<?php
/**
 * @file
 * Fix duplicate users.
 */

// Query assumes one org per duplicated user.
$query = <<<SQL
select
  u.uid, u.mail, u.access,
  o.field_tap_organization_tid,
  m.sourceid1
from
  {users} u
    inner join {profile} p on u.uid=p.uid
    inner join {field_data_field_tap_organization} o on p.pid=o.entity_id
    inner join {migrate_map_ldrorg} m on o.field_tap_organization_tid=m.destid1
where
  mail in (
    select mail from users group by mail having count(mail) > 1
  )
order by
  mail, access desc
SQL;

$stmt = db_query($query);

$user_updates = array();
$user_deletes = array();
$prev_mail = '';
$user_to_save = 0;

while ($result = $stmt->fetchAssoc()) {
  $uid = $result['uid'];
  $mail = $result['mail'];
  if ($mail != $prev_mail) {
    $prev_mail = $mail;
    $user_to_save = $uid;
    $user_updates[$user_to_save] = array();
    $user_updates[$user_to_save][] = $result['sourceid1'];
  }
  else {
    $user_updates[$user_to_save][] = $result['sourceid1'];
    $user_deletes[] = $uid;
  }
}

// Update thy user with orgs of doppleganger.
$uids = array_keys($user_updates);
$users = user_load_multiple($uids);
foreach ($users as $uid => $user) {
  $org_ids = implode(',', $user_updates[$uid]);
  pads_org_assign_orgs_to_user($org_ids, $user);
}

// Delete dup users.
user_delete_multiple($user_deletes);

