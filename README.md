PARCC-TAP
=========
PARCC Teacher Admin Portal (TAP) is part of a larger assessment deliver system (ADS), and is dependent on other ADS components such as LDR.

DEV REQUIREMENTS
----------------
TAP is based on the Drupal platform, and is built to run on top of PHP 5.5. To install and run tests, you will need:

* Composer [Install Composer](https://getcomposer.org/doc/00-intro.md)
* Drush [GitHub repo](https://github.com/drush-ops/drush)
* A url and credentials to an [LDR](https://github.com/Breakthrough-Technologies/PARCC-LDR) instance.

INSTALLATION
------------
You can use `drush si` with config options to setup TAP. We assume the database is created and is accessible. Replace '{...}' with appropriate values.

```
drush si
  pads
  install_pads_ldr_client.ldr_uri={http://myldrurl}
  install_pads_ldr_client.ldr_username={myldr_username}
  install_pads_ldr_client.ldr_password={myldr_password}
  pads_install_choose_vendor_form.pads_vendor={parcc or isbe}
  --db-url="mysql://{dbuser}:{dbpass}@{host}/{database}"
  --account-name={admin}
  --account-pass={correct horse battery staple}
  --account-mail={admin@example.com}
  --site-name="ADS Teach"
  --site-mail={teach@example.com}
  --yes
```

**LDR Configuration**
TAP requires an LDR instance to operate. Credentials and additional
details can be found at Breakthrough Technologies Central Desktop. Given
you are a user with access, you can access the document
[here](https://breaktech.centraldesktop.com/p/aQAAAAACZ9Sw).

TEST SETUP
----------
In the /tests folders, run `composer install` to install all the dependencies. Then copy the file `default-behat.yml` to `behat.yml`. In `behat.yml`, edit the base_url value to the url of your local install.

To run tests without SSO enabled, be sure to use `--tags="~sso"`.

CONFIGURATION
-------------
There are components in the system that are dependent on the "private://" scheme. The default location from the docroot is ../private_files. You will need to create this directory with appropriate permissions.

### SSO
SSO is disabled by default, but can be enabled. See https://breaktech.atlassian.net/wiki/x/NgClAQ for more details.

VENDOR SETUP
------------
The ADS Teach application can be setup with multiple vendors with multisite. The multisite configuration expects a `sites/vendor` parent folder for the vendor. For example, to setup ISBE as a vendor you would need to do the following:
* Create or edit docroot/sites/sites.php to point the vendor url to the vendor/isbe directory
```
<?php
$sites['isbe-ads.dev'] = 'vendor/isbe';
// Replace 'isbe-ads.dev' with the real site url.
```
* Create the docroot/sites/vendor/isbe folder
* Copy docroot/sites/default/default.settings.php to docroot/sites/vendor/isbe/settings.php
* Create the docroot/sites/vendor/isbe/files directory and set permissions to 777
* Run the install profile
* Revert features to set ISBE vars.


More Information
----------------

There is a list of developer notes listed [here](https://gist.github.com/burnsjeremy/1881b4472ce78fe0143d) that could be useful for other developers.
