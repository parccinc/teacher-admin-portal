<?php
/**
 * @file
 * Theme functions.
 */

/**
 * Implements hook_css_alter().
 */
function ads_teach_css_alter(&$css) {
  // Exclude specified CSS files from theme.
  $excludes = ads_teach_get_theme_info(NULL, 'exclude][css');

  // Run through $excludes and empty that from css array.
  if (!empty($excludes)) {
    $css = array_diff_key($css, drupal_map_assoc($excludes));
  }
}

/**
 * Implements hook_preprocess_html().
 */
function ads_teach_preprocess_html(&$variables) {
  // Add Google Fonts Open Sans.
  // TODO: After new AS work is done we need to go back and trim off any of,
  // The weights that are not going to be used. You should be able to search,
  // For 'font-weight:' to find out which of these font weights are used.
  drupal_add_css(
    'https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,800|Open+Sans+Condensed:300,700',
    array('type' => 'external', 'group' => CSS_THEME)
  );
}
/**
 * Implements theme_preprocess_page().
 */
function ads_teach_preprocess_page(&$variables) {
  if (!empty($variables['logo']) && empty($variables['linked_logo'])) {
    $variables['linked_logo'] = l($variables['logo_img'], '<front>', array(
      'attributes' => array(
        'rel' => 'home',
        'title' => strip_tags($variables['site_name']) . ' ' . t('Home'),
      ),
      'html' => TRUE,
    ));
  }
  // Add modal Zurb style for cTools to use, we use modals on almost all pages,
  // In the TAP system so this is why the settings are added here.
  drupal_add_js(array(
    'zurb-modal-style' => array(
      'modalSize' => array(
        'type' => 'scale',
        'width' => 0.75,
        'height' => 0.75,
      ),
      'modalTheme' => 'ZurbCToolsModalDialog',
      'throbberTheme' => 'ZurbCToolsModalThrobber',
      // Not sure on the backdrop here but this should work.
      'modalOptions' => array('opacity' => .55, 'background' => '#333'),
      // Animation fadeIn like zurb reveal.
      'animation' => 'fadeIn',
      // Moving this to fast as it seems more inline with Zurb.
      'animationSpeed' => 'fast',
      // Font icon here.
      'closeText' => '<i class="icon-close-circle"></i>',
      // No Image, closeImage just needs to be empty here for font icon above.
      'closeImage' => '',
      // We don't want loadingText, Zurb doesn't have that.
      'loadingText' => '',
      // Zurb doesn't have a throbber either.
      'throbber' => '',
    ),
  ), 'setting');
}

/**
 * Theme function to add Zurb classes to buttons.
 *
 * @param mixed $variables
 *   Array of theme variables.
 *
 * @return string
 *   The HTML of a button.
 *
 * @TODO - We need to go through these buttons and use theme wrappers instead of,
 * Having all of this in this button function.
 */
function ads_teach_button($variables) {
  $element = $variables['element'];
  $element_id = array_key_exists('#id', $element) ? $element['#id'] : '';
  // Add Zurb classes to file field with cardinality.
  if (
    (strpos($element_id, 'edit-field-file-und-') === 0 && strpos($element_id, 'remove-button') !== FALSE) ||
    (strpos($element_id, 'pads-org-field-widget-') === 0 && strpos($element_id, 'remove') !== FALSE)
  ) {
    $variables['element']['#attributes']['class'][] = 'tiny';
    $variables['element']['#attributes']['class'][] = 'round';
    $variables['element']['#attributes']['class'][] = 'alert';
  }
  // Add Zurb classes to the plupload upload button.
  elseif (
    (strpos($element_id, 'filefield-plupload-upload-button') !== FALSE) ||
    (strpos($element_id, 'edit-profile-pads-batchuser-organization-field-state-und-add-actions-associate') === 0)
  ) {
    $variables['element']['#attributes']['class'][] = 'tiny';
    $variables['element']['#attributes']['class'][] = 'round';
  }
  elseif (strpos($element_id, 'edit-field-file-und-0-upload-button') === 0) {
    return theme_button($variables);
  }
  else {
    switch ($element_id) {
      // This will take care of all edit-submit's since there may be more,
      // Than one on a page, edit-submit--3 as an example.
      case strstr($element_id, 'edit-submit'):
      case 'edit-delete':
      case 'edit-print':
      case 'edit-preview-changes':
        // Can't remove the classes from ajax submit buttons.
        if ($element_id !== 'edit-submit-parcc-secure-files') {
          unset($variables['element']['#attributes']['class']);
        }
        $variables['element']['#attributes']['class'][] = 'small';
        $variables['element']['#attributes']['class'][] = 'round';
        break;

      // Buttons on import class complete '/class/import/complete'
      case 'edit-back':
      case 'edit-class':
      case 'edit-export':
        unset($variables['element']['#attributes']['class']);
        $variables['element']['#attributes']['class'][] = 'small';
        $variables['element']['#attributes']['class'][] = 'round';
        break;

      case 'edit-reset':
        $element['#attributes']['class'][] = 'postfix';
        element_set_attributes($element, array('id', 'name', 'value', 'type'));
        // Add icon to button.
        $value = $element['#value'];
        $value = theme('icon', array('bundle' => 'foundation', 'icon' => 'fi-refresh')) . ' ' . $value;
        return '<button' . drupal_attributes($element['#attributes']) . '>' . $value . '</button>';

      case 'pads-org-widget-add':
      case 'addfile-button':
      case 'edit-submit-parcc-secure-files':
      case 'edit-field-file-und-0-filefield-plupload-upload-button':
      case 'edit-profile-pads-tap-organization-field-tap-organization-und-hierarchical-select-dropbox-add':
        $variables['element']['#attributes']['class'][] = 'tiny';
        $variables['element']['#attributes']['class'][] = 'round';
        break;

      case 'edit-field-file-und-0-remove-button':
        $variables['element']['#attributes']['class'][] = 'tiny';
        $variables['element']['#attributes']['class'][] = 'round';
        $variables['element']['#attributes']['class'][] = 'alert';
        break;

      case 'edit-reset':
      case 'pads-org-widget-cancel':
        $variables['element']['#attributes']['class'][] = 'tiny';
        $variables['element']['#attributes']['class'][] = 'round';
        $variables['element']['#attributes']['class'][] = 'secondary';
        break;

      // A true zurb whitelist.
      case 'edit-acl-view-delete-button':
      case 'edit-acl-view-add-button':
        return theme_button($variables);

      default:
        break;

    }
  }

  return zurb_foundation_button($variables);
}

/**
 * Theme wrapper function for submit button.
 *
 * @param mixed $variables
 *   Array of theme variables.
 *
 * @return string
 *   The HTML of a button.
 */
function ads_teach_button__tap_submit(&$variables) {
  $variables['element']['#attributes']['class'][] = 'small';
  $variables['element']['#attributes']['class'][] = 'round';
  return ads_teach_button($variables);
}

/**
 * Theme wrapper function for cancel button.
 *
 * @param mixed $variables
 *   Array of theme variables.
 *
 * @return string
 *   The HTML of a button.
 */
function ads_teach_button__tap_cancel(&$variables) {
  $variables['element']['#attributes']['class'][] = 'small';
  $variables['element']['#attributes']['class'][] = 'round';
  $variables['element']['#attributes']['class'][] = 'secondary';
  return ads_teach_button($variables);
}

/**
 * Theme wrapper function for submit button as a link.
 *
 * @param mixed $variables
 *   Array of theme variables.
 *
 * @return string
 *   The HTML of a link.
 */
function ads_teach_link__tap_submit(&$variables) {
  $variables['options']['attributes']['class'][] = 'button';
  $variables['options']['attributes']['class'][] = 'small';
  $variables['options']['attributes']['class'][] = 'round';
  return theme_link($variables);
}

/**
 * Theme wrapper function for cancel button link.
 *
 * @param mixed $variables
 *   Array of theme variables.
 *
 * @return string
 *   The HTML of a link.
 */
function ads_teach_link__tap_cancel(&$variables) {
  $variables['options']['attributes']['class'][] = 'button';
  $variables['options']['attributes']['class'][] = 'small';
  $variables['options']['attributes']['class'][] = 'round';
  $variables['options']['attributes']['class'][] = 'secondary';
  return theme_link($variables);
}

/**
 * Implements hook_preprocess_HOOK().
 */
function ads_teach_preprocess_textfield(&$variables) {
  // Add "placeholder" attribute to textfields.
  if (!empty($variables['element']['#placeholder'])) {
    $variables['element']['#attributes']['placeholder'] = $variables['element']['#placeholder'];
  }
}

/**
 * Implements theme_status_messages().
 *
 * Overrides base Zurb template alerts. Would be nice to put in theme settings.
 */
function ads_teach_status_messages($variables) {
  return theme_status_messages($variables);
}

/**
 * Implements hook_js_alter().
 *
 * Views bulk operations has a conflict with jquery v. 1.9.
 * Swap out the default vbo js file with our fixed version.
 */
function ads_teach_js_alter(&$javascript) {
  $vbo_js_path = drupal_get_path('module', 'views_bulk_operations') . '/js/views_bulk_operations.js';
  if (isset($javascript[$vbo_js_path])) {
    $path = drupal_get_path('theme', 'ads_teach') . '/js/ads_teach_views_bulk_operations.js';
    $javascript[$vbo_js_path]['data'] = $path;
  }

  // Move datatable search label to placeholder.
  if (module_exists('datatables')) {
    $js_settings = isset($javascript['settings']['data']) ?
      $javascript['settings']['data'] : array();
    foreach ($js_settings as $key => $settings) {
      if (array_key_exists('datatables', $settings)) {
        $datatables = $settings['datatables'];
        foreach ($datatables as $dtid => $datatable) {
          if (array_key_exists('oLanguage', $datatable) && $datatable['oLanguage']['sSearch'] == 'Search') {
            $javascript['settings']['data'][$key]['datatables'][$dtid]['oLanguage']['sSearchPlaceholder'] = t('Filter all columns');
            $javascript['settings']['data'][$key]['datatables'][$dtid]['oLanguage']['sSearch'] = '';
          }
        }
      }

    }
  }
}

/**
 * Implements theme_menu_local_tasks().
 */
function ads_teach_menu_local_tasks(&$variables) {
  $output = '';

  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $variables['primary']['#prefix'] .= '<ul class="button-group round">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<ul class="button-group round secondary">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }

  return $output;
}

/**
 * Implements theme_menu_local_task().
 */
function ads_teach_menu_local_task(&$variables) {
  $link = $variables['element']['#link'];
  $link_text = $link['title'];

  $active_link = !empty($variables['element']['#active']);
  $li_class = $active_link ? ' class="active"' : '';

  // Add section tab styling.
  $link['localized_options']['attributes']['class'] = $active_link ?
    array('tiny', 'button') : array('tiny', 'button');

  if ($active_link) {
    // Add text to indicate active tab for non-visual users.
    $active = '<span class="element-invisible">' . t('(active tab)') . '</span>';

    // If the link does not contain HTML already, check_plain() it now.
    // After we set 'html'=TRUE the link will not be sanitized by l().
    if (empty($link['localized_options']['html'])) {
      $link['title'] = check_plain($link['title']);
    }
    $link['localized_options']['html'] = TRUE;
    $link_text = t('!local-task-title!active', array(
      '!local-task-title' => $link['title'],
      '!active' => $active,
    ));
  }

  $output = '';
  $output .= '<li' . $li_class . '>';
  $output .= l($link_text, $link['href'], $link['localized_options']);
  $output .= "</li>\n";
  return $output;
}

/**
 * Alternate theme function for file_widget_multiple.
 *
 * Code is copied from theme_file_widget_multiple, and the operations column
 * is removed if attached to the secure_file node form. Table sticky headers
 * are set to FALSE.
 *
 * @param mixed $variables
 *   Theme variables.
 *
 * @return string
 *   Rendered Html output.
 *
 * @throws Exception
 *   Throws exception.
 */
function ads_teach_file_widget_multiple($variables) {
  $element = $variables['element'];
  if (!(
    array_key_exists(0, $element) &&
    array_key_exists('#bundle', $element[0]) &&
    $element[0]['#bundle'] == 'secure_file'
  )
  ) {
    return theme_file_widget_multiple($variables);
  }

  // Special ID and classes for draggable tables.
  $weight_class = $element['#id'] . '-weight';
  $table_id = $element['#id'] . '-table';

  // Build up a table of applicable fields.
  $headers = array();
  $headers[] = t('File information');
  if ($element['#display_field']) {
    $headers[] = array(
      'data' => t('Display'),
      'class' => array('checkbox'),
    );
  }
  $headers[] = t('Weight');

  // Get our list of widgets in order (needed when the form comes back after
  // preview or failed validation).
  $widgets = array();
  foreach (element_children($element) as $key) {
    $widgets[] = &$element[$key];
  }
  usort($widgets, '_field_sort_items_value_helper');

  $rows = array();
  foreach ($widgets as $key => &$widget) {
    // Save the uploading row for last.
    if ($widget['#file'] == FALSE) {
      $widget['#title'] = $element['#file_upload_title'];
      $widget['#description'] = $element['#file_upload_description'];
      continue;
    }

    // Delay rendering of the buttons, so that they can be rendered later in the
    // "operations" column.
    $operations_elements = array();
    foreach (element_children($widget) as $sub_key) {
      if (isset($widget[$sub_key]['#type']) && $widget[$sub_key]['#type'] == 'submit') {
        hide($widget[$sub_key]);
        $operations_elements[] = &$widget[$sub_key];
      }
    }

    // Delay rendering of the "Display" option and the weight selector, so that
    // each can be rendered later in its own column.
    if ($element['#display_field']) {
      hide($widget['display']);
    }
    hide($widget['_weight']);

    // Render everything else together in a column, without the normal wrappers.
    $widget['#theme_wrappers'] = array();
    $information = drupal_render($widget);

    // Render the previously hidden elements, using render() instead of
    // drupal_render(), to undo the earlier hide().
    $operations = '';
    foreach ($operations_elements as $operation_element) {
      $operations .= render($operation_element);
    }
    $display = '';
    if ($element['#display_field']) {
      unset($widget['display']['#title']);
      $display = array(
        'data' => render($widget['display']),
        'class' => array('checkbox'),
      );
    }
    $widget['_weight']['#attributes']['class'] = array($weight_class);
    $weight = render($widget['_weight']);

    // Arrange the row with all of the rendered columns.
    $row = array();
    $row[] = $information;
    if ($element['#display_field']) {
      $row[] = $display;
    }
    $row[] = $weight;
    $rows[] = array(
      'data' => $row,
      'class' => isset($widget['#attributes']['class']) ? array_merge($widget['#attributes']['class'], array('draggable')) : array('draggable'),
    );
  }

  drupal_add_tabledrag($table_id, 'order', 'sibling', $weight_class);

  $output = '';
  $output = empty($rows) ? '' : theme('table', array(
    'header' => $headers,
    'rows' => $rows,
    'attributes' => array('id' => $table_id),
    'sticky' => FALSE,
  ));
  $output .= drupal_render_children($element);
  return $output;
}

/**
 * Implements hook_library_alter().
 */
function ads_teach_library_alter(&$module_libraries, $module) {
  if ($module == 'datatables') {
    // Add JavaScript for Zurb styling.
    $module_libraries[$module]['js'][drupal_get_path('theme', 'ads_teach') . '/datatable/dataTables.foundation.js'] = array();
    // Add JavaScript for ADS implementations.
    $module_libraries[$module]['js'][drupal_get_path('theme', 'ads_teach') . '/datatable/pads_dt.js'] = array();
    // Replace CSS with Zurb styling css.
    $module_libraries[$module]['css'] = array(
      drupal_get_path('theme', 'ads_teach') . '/datatable/dataTables.foundation.css' => array(),
    );
  }
}

/**
 * Implements hook_form_alter().
 */
function ads_teach_form_alter(&$form, &$form_state, $form_id) {
  // Set empty label for all VBO checkboxes to use for styling.
  if (isset($form['views_bulk_operations'])) {
    $children = element_children($form['views_bulk_operations']);
    foreach ($children as $child) {
      if ($form['views_bulk_operations'][$child]['#type'] == 'checkbox') {
        $form['views_bulk_operations'][$child]['#title'] = ' ';
      }
    }
  }
}

/**
 * Implements hook_views_bulk_operations_form_alter().
 */
function ads_teach_views_bulk_operations_form_alter(&$form, &$form_state, $vbo) {
  if ($form_state['step'] == 'views_form_views_form') {
    // Alter the first step of the VBO form (the selection page).
    // Button styling.
    $form['select']['submit']['#attributes']['class'][] = 'tiny';
    $form['select']['submit']['#attributes']['class'][] = 'round';
  }
  elseif ($form_state['step'] == 'views_bulk_operations_config_form') {
    // Alter the configuration step of the VBO form.
    $form_id = array_key_exists('#form_id', $form) ? $form['#form_id'] : '';
    switch ($form_id) {
      case 'views_form_tap_user_actions_panel_pane_user_actions':
      case 'views_form_pads_students_panel_pane_student_actions':
        $form['actions']['submit']['#attributes']['class'][] = 'tiny';
        $form['actions']['submit']['#attributes']['class'][] = 'round';

        $path = array_key_exists('cancel_path', $form_state) ?
          $form_state['cancel_path'] : current_path();
        // Make a cancel button.
        $cancel_button = array(
          '#theme' => 'link',
          '#text' => t('Cancel'),
          '#path' => $path,
          '#options' => array(
            'html' => FALSE,
            'attributes' => array(
              'class' => array(
                'button',
                'small',
                'round',
              ),
            ),
          ),
        );
        $form['actions']['submit']['#suffix'] = drupal_render($cancel_button);
        break;

    }
  }
  elseif ($form_state['step'] == 'views_bulk_operations_confirm_form') {
    // Alter the confirmation step of the VBO form.
    // Make cancel a button on confirm forms.
    $form['actions']['cancel']['#attributes']['class'][] = 'button';
    $form['actions']['cancel']['#attributes']['class'][] = 'small';
    $form['actions']['cancel']['#attributes']['class'][] = 'round';
  }
}

/**
 * Overrides DataTables theme function.
 *
 * Adds default Search label as a placeholder.
 *
 * @param mixed $variables
 *   Datatable theme variables.
 *
 * @return string
 *   Html output.
 */
function ads_teach_datatable($variables) {
  $attributes = &$variables['attributes'];
  $datatable_options = $attributes && array_key_exists('datatable_options', $attributes) ?
    $attributes['datatable_options'] : array();

  if (!$datatable_options || !array_key_exists('oLanguage', $datatable_options)) {
    $attributes['datatable_options']['oLanguage']['sSearch'] = '';
    $attributes['datatable_options']['oLanguage']['sSearchPlaceholder'] = t('Filter all columns');
  }
  return theme_datatable($variables);
}

/**
 * Returns HTML for a table with radio buttons or checkboxes.
 *
 * @see theme_tableselect
 */
function ads_teach_tableselect($variables) {
  $element = $variables['element'];
  $rows = array();
  $header = $element['#header'];
  if (!empty($element['#options'])) {
    // Generate a table row for each selectable item in #options.
    foreach (element_children($element) as $key) {
      $row = array();

      $row['data'] = array();

      if (isset($element['#options'][$key]['#attributes'])) {
        $row += $element['#options'][$key]['#attributes'];
      }

      // Add label to checkbox for styling.
      if (empty($element[$key]['#title'])) {
        $element[$key]['#title'] = ' ';
      }

      // Render the checkbox / radio element.
      $row['data'][] = drupal_render($element[$key]);

      // As theme_table only maps header and row columns by order, create the.
      // correct order by iterating over the header fields.
      foreach ($element['#header'] as $fieldname => $title) {
        $row['data'][] = $element['#options'][$key][$fieldname];
      }
      $rows[] = $row;
    }

    // Add an empty header or a "Select all" checkbox to provide room for the,
    // Checkboxes/radios in the first table column.
    if ($element['#js_select']) {
      // Add a "Select all" checkbox.
      drupal_add_js('misc/tableselect.js');
      array_unshift($header, array('class' => array('select-all')));
    }
    else {
      // Add an empty header when radio buttons are displayed or a "Select all",
      // checkbox is not desired.
      array_unshift($header, '');
    }
  }

  return theme(
    'datatable',
    array(
      'header' => $header,
      'rows' => $rows,
      'empty' => $element['#empty'],
      'attributes' => $element['#attributes'],
    )
  );
}

/**
 * Implements theme_dashboard_boxes().
 */
function ads_teach_dashboard_boxes($variables) {
  $boxes = $variables['boxes'];
  $columns = empty($boxes) ? '' : 12 / count($boxes);

  $output = '<div class="boxes-wrapper">';
  foreach ($boxes as $id => $box) {
    $output .= '<div class="box-' . $id . ' medium-' . $columns . ' columns">';
    $output .= theme('dashboard_box', $box);
    $output .= '</div>';
  }
  $output .= '</div>';

  return $output;
}

/**
 * Return information from the .info file of a theme (and possible base themes).
 *
 * @param string $theme_key
 *   The machine name of the theme.
 * @param string $key
 *   The key name of the item to return from the .info file. This value can
 *   include "][" to automatically attempt to traverse any arrays.
 * @param bool $base_themes
 *   Recursively search base themes, defaults to TRUE.
 *
 * @return string|array|false
 *   A string or array depending on the type of value and if a base theme also
 *   contains the same $key, FALSE if no $key is found.
 */
function ads_teach_get_theme_info($theme_key = NULL, $key = NULL, $base_themes = TRUE) {
  // If no $theme_key is given, use the current theme if we can determine it.
  if (!isset($theme_key)) {
    $theme_key = !empty($GLOBALS['theme_key']) ? $GLOBALS['theme_key'] : FALSE;
  }
  if ($theme_key) {
    $themes = list_themes();
    if (!empty($themes[$theme_key])) {
      $theme = $themes[$theme_key];
      // If a key name was specified, return just that array.
      if ($key) {
        $value = FALSE;
        // Recursively add base theme values.
        if ($base_themes && isset($theme->base_themes)) {
          foreach (array_keys($theme->base_themes) as $base_theme) {
            $value = ads_teach_get_theme_info($base_theme, $key);
          }
        }
        if (!empty($themes[$theme_key])) {
          $info = $themes[$theme_key]->info;
          // Allow array traversal.
          $keys = explode('][', $key);
          foreach ($keys as $parent) {
            if (isset($info[$parent])) {
              $info = $info[$parent];
            }
            else {
              $info = FALSE;
            }
          }
          if (is_array($value)) {
            if (!empty($info)) {
              if (!is_array($info)) {
                $info = array($info);
              }
              $value = drupal_array_merge_deep($value, $info);
            }
          }
          else {
            if (!empty($info)) {
              if (empty($value)) {
                $value = $info;
              }
              else {
                if (!is_array($value)) {
                  $value = array($value);
                }
                if (!is_array($info)) {
                  $info = array($info);
                }
                $value = drupal_array_merge_deep($value, $info);
              }
            }
          }
        }
        return $value;
      }
      // If no info $key was specified, just return the entire info array.
      return $theme->info;
    }
  }
  return FALSE;
}

/**
 * Implements hook_form_element().
 */
function ads_teach_form_element($variables) {

  $element = &$variables['element'];
  // This is to remove the wrapper around the form item of the search box.
  // It is used on the secure files page.
  if (!empty($element['#id']) && !empty($element['#name'])) {
    if ($element['#id'] === 'edit-terms' && $element['#name'] === 'terms') {
      // Return just the input element without wrappers.
      return $element['#children'];
    }
  }

  return theme_form_element($variables);
}

/**
 * Implements hook_icon_render_hooks().
 */
function ads_teach_icon_render_hooks() {
  $hooks['ads_teach'] = array(
    'file' => 'icons.inc',
    'path' => drupal_get_path('theme', 'ads_teach') . '/includes',
  );
  return $hooks;
}

/**
 * Implements theme_links() with foundations dropdown button markup.
 *
 * Formats links for Dropdown Button.
 *
 * @param mixed $variables
 *   An associative array containing:
 *   - label
 *     - Dropdown button label.
 *   - links
 *     - An array of menu links.
 *   - attributes (optional)
 *     - class: Array of additional classes like large, alert, round
 *     - data-dropdown: Custom dropdown id.
 *
 * @return string
 *   Rendered HTML for dropdown button.
 *
 * @see http://foundation.zurb.com/docs/components/dropdown-buttons.html
 */
function ads_teach_links__ads_dropdown($variables) {
  if (empty($variables['attributes']['class'])) {
    $variables['attributes']['class'] = array();
  }

  $variables['attributes']['class'][] = 'dropdown';
  $variables['attributes']['class'][] = 'button';
  $variables['attributes']['class'][] = 'split';
  $variables['attributes']['class'][] = 'secondary';
  $variables['attributes']['class'][] = 'tiny';
  $variables['attributes']['class'][] = 'round';

  if (!isset($variables['attributes']['data-dropdown'])) {
    $ddb_id = drupal_html_id('ddb');
    $variables['attributes']['data-dropdown'] = $ddb_id;
    $variables['attributes']['aria-controls'] = $ddb_id;
    $variables['attributes']['aria-expanded'] = 'false';
  }

  if (!isset($variables['label'])) {
    $label = t('Dropdown button');
    if (array_key_exists('heading', $variables)) {
      $label = is_array($variables['heading']) && array_key_exists('text', $variables['heading']) ?
        $variables['heading']['text'] : $variables['heading'];
    }
    $variables['label'] = $label;
  }

  $output = '<a href="#"' . drupal_attributes($variables['attributes']) . '>' . $variables['label'] . '</a>';

  $links_variables = array(
    'attributes' => array(
      'id' => $ddb_id,
      'class' => array('f-dropdown'),
      'aria-hidden' => 'true',
    ),
    'heading' => FALSE,
    'links' => $variables['links'],
  );
  $output .= theme_links($links_variables);
  return $output;
}

/**
 * Implements hook_preprocess_HOOK().
 */
function ads_teach_preprocess_homebox(&$variables) {
  global $user;
  $account = user_load($user->uid);

  // Username for dashboard header.
  $variables['username'] = check_plain(format_username($account));

  // Pads Org Context Selector.
  $variables['selector'] = pads_organization_context_selector();
}

/**
 * Implements hook_preprocess_link().
 */
function ads_teach_preprocess_link(&$variables) {
  // Make link for Organizations with icons.
  if ($variables['path'] === 'organizations') {
    $variables['options']['html'] = TRUE;
    // Old link text.
    $link_text = $variables['text'];
    // Make icon link.
    $variables['text'] = '<span class="icon icon-organization" aria-hidden="true"></span><div class="title">' . $link_text . '</div>';
  }
}
