(function ($, Drupal) {

  Drupal.behaviors.parcc = {
    attach: function(context, settings) {
      // If hierarchical select kicked off behaviors, and chosen exists, then
      // we properly hide the chosenized selects that chosen doesn't because
      // of hs.
      if (jQuery(context) && jQuery(context).hasClass('form-type-hierarchical-select')) {
        if (settings.hasOwnProperty('chosen')) {
          var chosen = $(context).find('.hierarchical-select-wrapper .hierarchical-select .selects .chosen-processed');
          if ($(chosen).length > 0) {
            var hs_id;
            $(chosen).each(function (index, item) {
              // If it's a select chosen processed item, hide it.
              if ($(item).is('select')) {
                $(item).addClass('element-invisible');
                hs_id = $(item).attr('id');
              }

              // If item is a div, make sure there is a corresponding select.
              if ($(item).is('div')) {
                if ($(item).prev().is('select')) {
                  $(item).attr('id', hs_id);
                  $('#' + hs_id).each(function () {
                    $(this).change(function () {
                      // Find the next chosen-container and kill it.
                      var killme = $(this).nextAll('.chosen-container').not('#' + $(this).attr('id'));
                      if ($(killme).length > 0) {
                        $(killme).remove();
                      }
                    });
                  });
                }
                else {
                  $(item).remove();
                }
              }
            });
          }
        }
      }
      // Function: sideNav() controls the sidebar menu visibility toggle.
      function sideNav() {
        if ($(window).width() < 769) {
          $('.off-canvas-wrap').removeClass('move-right');
          $('.left-off-canvas-toggle').show();
        } else {
          $('.off-canvas-wrap').addClass('move-right');
          $('.left-off-canvas-toggle').hide();
        }
      }
      // If the page is resized we need to run sideNav() to check sidebar menu.
      $(window).resize(function() {
        sideNav();
      });

    }
  };

  // Drupal Zurb Accordion Open with URL Hash.
  Drupal.behaviors.parcc_zurb_accordion = {
    attach: function(context, settings) {
      var hash;
      // window hash detection.
      hash = window.location.hash;

      if (hash !== '') {
        // If hash is present open that accordion.
        $('[data-accordion] [href="' + hash + '"]').trigger('click.fndtn.accordion');
      }
    }
  };

})(jQuery, Drupal);

// Update, cTools JS Modal themeable.
Drupal.theme.prototype.ZurbCToolsModalDialog = function () {
  var html = '';
  html += '  <div id="ctools-modal">';
  html += '    <div class="ctools-modal-content">';
  html += '      <div class="modal-header">';
  html += '        <a class="close-reveal-modal close" aria-label="Close">';
  html +=            Drupal.CTools.Modal.currentSettings.closeText + Drupal.CTools.Modal.currentSettings.closeImage;
  html += '        </a>';
  html += '        <h2 id="modal-title" class="modal-title"> </h2>';
  html += '      </div>';
  html += '      <div id="modal-content" class="modal-content">';
  html += '      </div>';
  html += '    </div>';
  html += '  </div>';

  return html;
};

// Update, cTools JS Modal Throbber themeable.
Drupal.theme.prototype.ZurbCToolsModalThrobber = function () {
  var html = '';
  html += '  <div id="modal-throbber">';
  html += '    <div class="modal-throbber-wrapper">';
  html +=        Drupal.CTools.Modal.currentSettings.throbber;
  html += '    </div>';
  html += '  </div>';

  return html;
};

(function ($) {

  Drupal.behaviors.ads_teach_more_links = {
    attach: function (context, settings) {
      $(".more-list", context).each(function () {
        var moreList = $('ul', this);
        $('a', this).click(function (event) {
          event.preventDefault();
          $(moreList).removeClass('element-invisible');
        });
      });
    }
  }

})(jQuery);
