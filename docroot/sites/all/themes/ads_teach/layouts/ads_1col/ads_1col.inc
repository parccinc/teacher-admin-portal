<?php
/**
 * @file
 * Ctools layout include file.
 */

/**
 * Implements hook_panels_layouts().
 */
function ads_teach_ads_1col_panels_layouts() {
  $items['ads_1col'] = array(
    'title' => t('ads: 1 column row'),
    'category' => t('ads: 1'),
    'icon' => 'ads_1col.png',
    'theme' => 'ads_1col',
    'regions' => array(
      'middle_grid' => t('Custom grid row in Main column'),
      'middle' => t('Main column'),
    ),
  );
  return $items;
}
