(function ($) {

  Drupal.behaviors.padsDataTablesEvents = {

    attach: function (context, settings) {

      /**
       * The Drupal datatables modules stores each datatable object on the page
       * in Drupal.settings. This jQuery .each method iterates through each
       * datatable object on the page and binds the draw.dt event.
       * 
       * See https://datatables.net/reference/event/draw for more details on
       * draw.dt.
       * 
       * When the table is redrawn, the Drupal.attachBehaviors function is
       * called to attach behaviors to the updated table content.
       */

      $.each(settings.datatables, function (selector) {
        $(selector, context).once('draw-dt', function () {
          $(selector).on('draw.dt', function (e) {
            Drupal.attachBehaviors(selector, settings);
          });
        });
      });
    }
  };

})(jQuery);