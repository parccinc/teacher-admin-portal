<header role="banner" id="header" class="l-header hide-for-print">

  <?php if ($top_bar): ?>
    <!--.top-bar -->
    <?php if ($top_bar_classes): ?>
      <div class="<?php print $top_bar_classes; ?>">
    <?php endif; ?>
    <nav class="top-bar" data-topbar <?php print $top_bar_options; ?>>
      <div class="medium-1 columns">
        <ul class="title-area">
          <li class="name">
            <h1><?php print $linked_logo ? $linked_logo : $linked_site_name; ?></h1>
          </li>
          <li class="toggle-topbar menu-icon"><a
              href="#"><span></span></a>
          </li>
        </ul>
      </div>
      <div class="medium-4 columns">
        <div class="site-name">
          <?php print $site_name; ?>
        </div>
      </div>
      <div class="medium-3 columns">
        <div class="user-navigation">
          <a href=""
            data-dropdown="user-navigation-dropdown"
            aria-controls="user-navigation-dropdown"
            aria-expanded="false"
            class="secondary-navigation-dropdown-button user-navigation-dropdown-button"
            title="Profile"
            alt="Profile">User</a>
          <?php print theme('links__system_secondary_menu', array('links' => $secondary_menu, 'attributes' => array('id' => 'user-navigation-dropdown', 'class' => 'f-dropdown', 'data-dropdown-content' => '', 'aria-hidden' => 'true', 'tabindex' => '-1'))); ?>
        </div>
      </div>

    </nav>
    <?php if ($top_bar_classes): ?>
      </div>
    <?php endif; ?>
    <!--/.top-bar -->
  <?php endif; ?>

  <!-- Title, slogan and menu -->
  <?php if ($alt_header): ?>
    <section class="row <?php print $alt_header_classes; ?>">

      <?php if ($linked_logo): print $linked_logo; endif; ?>

      <?php if ($site_name): ?>
        <?php if ($title): ?>
          <div id="site-name" class="element-invisible">
            <strong>
              <a href="<?php print $front_page; ?>"
                 title="<?php print t('Home'); ?>"
                 rel="home"><span><?php print $site_name; ?></span></a>
            </strong>
          </div>
        <?php else: /* Use h1 when the content title is empty */ { ?>
          <h1 id="site-name">
            <a href="<?php print $front_page; ?>"
               title="<?php print t('Home'); ?>"
               rel="home"><span><?php print $site_name; ?></span></a>
          </h1>
        <?php } endif; ?>
      <?php endif; ?>

      <?php if ($site_slogan): ?>
        <h2 title="<?php print $site_slogan; ?>"
            class="site-slogan"><?php print $site_slogan; ?></h2>
      <?php endif; ?>

      <?php if ($alt_main_menu): ?>
        <nav id="main-menu" class="navigation" role="navigation">
          <?php print ($alt_main_menu); ?>
        </nav> <!-- /#main-menu -->
      <?php endif; ?>

      <?php if ($alt_secondary_menu): ?>
        <nav id="secondary-menu" class="navigation" role="navigation">
          <?php print $alt_secondary_menu; ?>
        </nav> <!-- /#secondary-menu -->
      <?php endif; ?>

    </section>
  <?php endif; ?>
  <!-- End title, slogan and menu -->

  <?php if (!empty($page['header'])): ?>
    <!--.l-header-region -->
    <section class="l-header-region row">
      <div class="large-12 columns">
        <?php print render($page['header']); ?>
      </div>
    </section>
    <!--/.l-header-region -->
  <?php endif; ?>

</header>
<!--/.l-header -->
