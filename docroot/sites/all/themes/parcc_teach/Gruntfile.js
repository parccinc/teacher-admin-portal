module.exports = function(grunt) {
  "use strict";

  var theme_name = 'parcc_teach';

  var global_vars = {
    theme_name: theme_name,
    theme_css: 'css',
    theme_scss: 'scss'
  }

  grunt.initConfig({
    global_vars: global_vars,
    pkg: grunt.file.readJSON('package.json'),
    // Need to setup a config file for dev/prod to ease development.
    compass: {                  // Task
      dist: {                   // Target
        options: {              // Target options
          sassDir: '<%= global_vars.theme_scss %>',
          cssDir: '<%= global_vars.theme_css %>',
          sourcemap: true,
        }
      }
    },
    // Minify foundation JS files.
    // uglify: {
    //   min_target: {
    //     files: {
    //       'js/foundation.min.js': ['js/foundation/foundation.js', 'js/foundation/foundation.*.js']
    //     }
    //   }
    // },

    copy: {
      dist: {
        files: [
          // Getting rid of these until I can update the foundation version, compiling ourselves with grunt uglify.
          // {expand:true, cwd: 'bower_components/foundation/js', src: ['foundation/*.js'], dest: 'js/', filter: 'isFile'},
          // {expand:true, cwd: 'bower_components/foundation/', src: ['foundation.min.js'], dest: 'js/', filter: 'isFile'},
          {expand:true, cwd: 'bower_components/foundation/js/vendor', src: ['fastclick.js'], dest: 'js/vendor', filter: 'isFile'},
          {expand:true, cwd: 'bower_components/foundation/js/vendor', src: ['jquery.cookie.js'], dest: 'js/vendor', filter: 'isFile'},
          {expand:true, cwd: 'bower_components/foundation/js/vendor', src: ['modernizr.js'], dest: 'js/vendor', filter: 'isFile'},
          {expand:true, cwd: 'bower_components/foundation/scss/foundation/components', src: '**/*.scss', dest: 'scss/vendor/foundation/components', filter: 'isFile'},
          {expand:true, cwd: 'bower_components/foundation/scss/foundation', src: '_functions.scss', dest: 'scss/vendor/foundation', filter: 'isFile'},
        ]
      }
    },

    watch: {
      grunt: { files: ['Gruntfile.js'] },

      sass: {
        files: '<%= global_vars.theme_scss %>/**/*.scss',
        tasks: ['compass'],
        options: {
          livereload: true
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.registerTask('build', ['compass', 'copy']);
  grunt.registerTask('default', ['build', 'watch']);
}
