<?php

/**
 * @file
 * Zurb accordian view template, relies on view to provide most classes needed.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */

// <ul class="accordion" data-accordion>
//   <li class="accordion-navigation">
//     <a href="#panel1a">Accordion 1</a>
//     <div id="panel1a" class="content active">
//       Panel 1. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
//     </div>
//   </li>
//   <li class="accordion-navigation">
//     <a href="#panel2a">Accordion 2</a>
//     <div id="panel2a" class="content">
//       Panel 2. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
//     </div>
//   </li>
//   <li class="accordion-navigation">
//     <a href="#panel3a">Accordion 3</a>
//     <div id="panel3a" class="content">
//       Panel 3. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
//     </div>
//   </li>
// </ul>
?>
<?php print $wrapper_prefix; ?>
  <?php if (!empty($title)) : ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>
  <<?php print $options['type']; ?> class="accordion" data-accordion>
    <?php foreach ($rows as $id => $row): ?>
      <li class="<?php print $classes_array[$id]; ?>"><?php print $row; ?></li>
    <?php endforeach; ?>
  <?php print $list_type_suffix; ?>
<?php print $wrapper_suffix; ?>
