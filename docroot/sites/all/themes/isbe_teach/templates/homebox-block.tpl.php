<?php
/**
 * @file
 * File homebox-block.tpl.php
 * Default theme implementation each homebox block.
 */

?>



<div id="homebox-block-<?php print $block->key; ?>" class="<?php print $block->homebox_classes ?> clearfix block block-<?php print $block->module ?>">

  <hr>

  <div class="homebox-portlet-inner">
    <h2 class="portlet-header">
      <?php if ($block->closable): ?>
        <a class="portlet-icon portlet-close"></a>
      <?php endif; ?>
      <a class="portlet-inactive-icon icon-crosshair"></a>
      <!-- <a class="portlet-icon portlet-maximize"></a> -->
      <a class="portlet-icon portlet-minus"></a>
      <?php if ($page->settings['color'] || isset($block->edit_form)): ?>
        <a class="icon portlet-icon portlet-settings"></a>
      <?php endif; ?>
      <?php
        // Create tool tips per block in Homebox dashboard.
          $tooltip_title = $block->key;
          $tooltip_header = $block->key . '_header';
          $tooltip_header_text = variable_get($tooltip_header);
          $tooltip_text = variable_get($tooltip_title);
          $tooltip_string = '<span id="' . $tooltip_title . '" data-tooltip aria-haspopup="true" class="has-tip tip-top icon dashboard-help icon-help" title="<b>' . $tooltip_header_text .'</b></br>' . $tooltip_text . '"></span>';
      ?>
      <span class="portlet-title" ><?php print $block->subject ?><?php print $tooltip_string ?></span>
    </h2>
    <div class="portlet-config">
      <?php if ($page->settings['color']): ?>
        <div class="clearfix"><div class="homebox-colors">
          <span class="homebox-color-message"><?php print t('Select a color') . ':'; ?></span>
          <?php for ($i = 0; $i < HOMEBOX_NUMBER_OF_COLOURS; $i++): ?>
            <span class="homebox-color-selector" style="background-color: <?php print $page->settings['colors'][$i] ?>;">&nbsp;</span>
          <?php endfor ?>
        </div></div>
      <?php endif; ?>
      <?php if (isset($block->edit_form)): print $block->edit_form; endif; ?>
    </div>
     <div class="portlet-content content"><?php if (is_string($block->content)){print $block->content;} else {print drupal_render($block->content);} ?></div>
  </div>
</div>
