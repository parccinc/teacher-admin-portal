<!--.page -->
<div role="document" id="page" class="page">
<!-- #wrap -->
<div id="wrap">
  <!--.l-header region -->
  <?php include path_to_theme() . '/templates/includes/header.inc'; ?>
  <!-- #main -->
  <div id="main">

    <?php if (!empty($page['featured'])): ?>
      <!--/.featured -->
      <section class="l-featured row">
        <div class="large-12 columns">
          <?php print render($page['featured']); ?>
        </div>
      </section>
      <!--/.l-featured -->
    <?php endif; ?>

    <?php if (!empty($page['help'])): ?>
      <!--/.l-help -->
      <section class="l-help row">
        <div class="large-12 columns">
          <?php print render($page['help']); ?>
        </div>
      </section>
      <!--/.l-help -->
    <?php endif; ?>

    <main role="main" class="row l-main">

      <div class="off-canvas-wrap move-right" data-offcanvas>
        <div class="inner-wrap">

          <a class="left-off-canvas-toggle" href="#"><i class="fi-list"></i></a>

          <!-- Off Canvas Menu -->
          <aside class="left-off-canvas-menu hide-for-print">
            <div class="icon-bar vertical five-up">
              <?php print $top_bar_main_menu; ?>
            </div>
          </aside>


      <div class="<?php print $main_grid; ?> main columns">
        <?php if (!empty($page['highlighted'])): ?>
          <div class="highlight panel callout">
            <?php print render($page['highlighted']); ?>
          </div>
        <?php endif; ?>

        <a id="main-content"></a>

        <?php
          // Hiding breadcrumbs for now as they are not in any mockups.
          //if ($breadcrumb): print $breadcrumb; endif;
        ?>

        <?php if ($title && !$is_front): ?>
          <?php print render($title_prefix); ?>
          <h1 id="page-title" class="title"><?php print $title; ?></h1>
          <?php print render($title_suffix); ?>
        <?php endif; ?>

        <?php if ($messages && !$zurb_foundation_messages_modal): ?>
          <!--/.l-messages -->
          <section class="l-messages row">
            <div class="large-12 columns">
              <?php if ($messages): print $messages; endif; ?>
            </div>
          </section>
          <!--/.l-messages -->
        <?php endif; ?>

        <?php if (!empty($tabs)): ?>
          <?php print render($tabs); ?>
          <?php if (!empty($tabs2)): print render($tabs2); endif; ?>
        <?php endif; ?>

        <?php if ($action_links): ?>
          <ul class="action-links">
            <?php print render($action_links); ?>
          </ul>
        <?php endif; ?>

        <?php print render($page['content']); ?>
      </div>
      <!--/.main region -->

      <?php if (!empty($page['sidebar_first'])): ?>
        <aside role="complementary"
               class="<?php print $sidebar_first_grid; ?> sidebar-first columns sidebar">
          <?php print render($page['sidebar_first']); ?>
        </aside>
      <?php endif; ?>

      <?php if (!empty($page['sidebar_second'])): ?>
        <aside role="complementary"
               class="<?php print $sidebar_sec_grid; ?> sidebar-second columns sidebar">
          <?php print render($page['sidebar_second']); ?>
        </aside>
      <?php endif; ?>
      <!-- close the off-canvas menu -->
    <a class="exit-off-canvas"></a>

        </div>
      </div>
    </main>
    <!--/.main-->
    <?php if (!empty($page['triptych_first']) || !empty($page['triptych_middle']) || !empty($page['triptych_last'])): ?>
      <!--.triptych-->
      <section class="l-triptych row">
        <div class="triptych-first large-4 columns">
          <?php print render($page['triptych_first']); ?>
        </div>
        <div class="triptych-middle large-4 columns">
          <?php print render($page['triptych_middle']); ?>
        </div>
        <div class="triptych-last large-4 columns">
          <?php print render($page['triptych_last']); ?>
        </div>
      </section>
      <!--/.triptych -->
    <?php endif; ?>
  </div>
  <!-- /#main -->
</div>
<!-- /#wrap -->
<div id="footer">
  <?php if (!empty($page['footer_firstcolumn']) || !empty($page['footer_secondcolumn']) || !empty($page['footer_thirdcolumn']) || !empty($page['footer_fourthcolumn'])): ?>
    <!--.footer-columns -->
    <section class="row l-footer-columns">
      <?php if (!empty($page['footer_firstcolumn'])): ?>
        <div class="footer-first large-3 columns">
          <?php print render($page['footer_firstcolumn']); ?>
        </div>
      <?php endif; ?>
      <?php if (!empty($page['footer_secondcolumn'])): ?>
        <div class="footer-second large-3 columns">
          <?php print render($page['footer_secondcolumn']); ?>
        </div>
      <?php endif; ?>
      <?php if (!empty($page['footer_thirdcolumn'])): ?>
        <div class="footer-third large-3 columns">
          <?php print render($page['footer_thirdcolumn']); ?>
        </div>
      <?php endif; ?>
      <?php if (!empty($page['footer_fourthcolumn'])): ?>
        <div class="footer-fourth large-3 columns">
          <?php print render($page['footer_fourthcolumn']); ?>
        </div>
      <?php endif; ?>
    </section>
    <!--/.footer-columns-->
  <?php endif; ?>

  <!--.l-footer-->
  <!-- <footer class="l-footer row" role="contentinfo">
    <?php if (!empty($page['footer'])): ?>
      <div class="footer large-12 columns">
        <?php print render($page['footer']); ?>
      </div>
    <?php endif; ?>

    <?php if ($site_name) : ?>
      <div class="copyright large-12 columns">
        &copy; <?php print date('Y') . ' ' . check_plain($site_name) . ' ' . t('All rights reserved.'); ?>
      </div>
    <?php endif; ?>
  </footer> -->
  <!--/.footer-->
</div>
<!--/#footer-->
<?php if ($messages && $zurb_foundation_messages_modal): print $messages; endif; ?>
</div>
<!--/.page -->
