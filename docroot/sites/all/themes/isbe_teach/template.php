<?php
/**
 * @file
 * Theme functions.
 */

/**
 * Implements theme_fieldset().
 *
 * Changes title of a profile2 fieldset.
 *
 * Why change a fieldset title here and not in a form alter? Because I am too
 * lazy to add weight to a module so it executes after profile2.
 */
function isbe_teach_fieldset($variables) {
  $element = $variables['element'];
  $element_id = array_key_exists('#id', $element) ? $element['#id'] : '';
  switch ($element_id) {
    case 'edit-profile-pads-organization':
      $variables['element']['#title'] = t('Please indicate your affiliation');
      break;

    default:

  }
  return theme_fieldset($variables);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function isbe_teach_form_views_exposed_form_alter(&$form, $form_state) {
  switch ($form_state['view']->name) {
    case 'parcc_secure_files':
      // Add the placeholder text to the secure file keyword search.
      if (!empty($form['#info']['filter-combine']['description'])) {
        $form['terms']['#placeholder'] = $form['#info']['filter-combine']['description'];
        unset($form['#info']['filter-combine']['description']);
      }
      break;

  }
}

/**
 * Theme the menu links from batch_management.
 *
 * @param array $variables
 *   The variables.
 *
 * @return string
 *   Html.
 */
function isbe_teach_menu_link__menu_batch_management(&$variables) {
  $options['attributes']['class'][] = 'button';
  $options['attributes']['class'][] = 'radius';
  $options['attributes']['class'][] = 'secondary';
  $options['attributes']['class'][] = 'tiny';

  $options += $variables['element']['#localized_options'];

  $element = $variables['element'];
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $options);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Theme the menu tree for batch_management.
 *
 * @param array $variables
 *   The variables.
 *
 * @return string
 *   Html.
 */
function isbe_teach_menu_tree__menu_batch_management(&$variables) {
  return '<ul class="menu button-group">' . $variables['tree'] . '</ul>';
}

/**
 * Implements hook_preprocess_HOOK().
 */
function isbe_teach_preprocess_homebox(&$variables) {
  global $user;
  $account = user_load($user->uid);

  // Username for dashboard header.
  $variables['username'] = check_plain(format_username($account));

  // Pads Org Context Selector.
  $variables['selector'] = pads_organization_context_selector();
}

