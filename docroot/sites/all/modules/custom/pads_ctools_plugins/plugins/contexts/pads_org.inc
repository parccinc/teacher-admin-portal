<?php
/**
 * @file
 * Ctools context type for Pads Organization.
 */

$plugin = array(
  'title' => t("Pads Organization"),
  'description' => t('A single "pads_org" context.'),
  'context' => 'pads_ctools_plugins_context_create_pads_org',
  'context name' => 'pads_org',
  'edit form' => 'pads_ctools_plugins_pads_org_context_edit_form',
  'keyword' => 'pads_org',
  'convert list' => 'pads_org_convert_list',
  'convert' => 'pads_org_convert',
);

/**
 * Create pads_org context from URL.
 *
 * @param bool $empty
 *   If true, just return an empty context.
 * @param array|string $data
 *   If from settings form, an array as from a form. If from argument, a string.
 * @param bool $conf
 *   TRUE if $data from admin configuration, FALSE if data from URL arg.
 *
 * @return array
 *   a Context object/
 */
function pads_ctools_plugins_context_create_pads_org($empty, $data = NULL, $conf = FALSE) {
  $context = new ctools_context('pads_org');
  $context->plugin = 'pads_org';

  if ($empty) {
    return $context;
  }

  if ($conf) {
    if (!empty($data['source']) && $data['source'] == 'user_context_select') {
      $data = pads_organization_get_selected_org();
    }
  }

  if ($data && is_numeric($data)) {
    $context->data = pads_org_load($data);
  }

  return $context;
}

/**
 * Provide a list of sub-keywords.
 *
 * This is used to provide keywords from the context for use in a content type,
 * pane, etc.
 */
function pads_org_convert_list() {
  return array(
    'organizationId' => t('Organization id'),
    'organizationType' => t('Organization type (level)'),
    'stateOrganizationId' => t('State organization id'),
    'parentOrganizationId' => t('Parent organization id'),
    'organizationIdentifier' => t('Organization identifier (code)'),
    'organizationName' => t('Organization name'),
    'studentCount' => t('Student count'),
    'childCount' => t('Child count'),
    'termId' => t('Term id of term representing Org'),
    'accessibleClasses' => t('User accessible classes'),
  );
}

/**
 * Convert context into string to be used as keyword by content types, etc.
 *
 * @param object $context
 *   The ctools context.
 * @param string $type
 *   A keyword type.
 *
 * @return mixed
 *   The value of the keyword from the context, or NULL.
 */
function pads_org_convert($context, $type) {
  $value = NULL;
  switch ($type) {
    case 'organizationId':
      $value = $context->data['organizationId'];
      break;

    case 'organizationType':
      $value = $context->data['organizationType'];
      break;

    case 'stateOrganizationId':
      if ($org = pads_org_load($context->data['stateOrganizationId'])) {
        $value = $org['organizationName'];
      }
      break;

    case 'parentOrganizationId':
      if ($org = pads_org_load($context->data['parentOrganizationId'])) {
        $value = $org['organizationName'];
      }
      break;

    case 'organizationIdentifier':
      $value = $context->data['organizationIdentifier'];
      break;

    case 'organizationName':
      $value = $context->data['organizationName'];
      break;

    case 'termId':
      $term = pads_org_term_from_org($context->data['organizationId']);
      $value = $term->tid;
      break;

    case 'accessibleClasses':
      $values = array();
      if ($context->data['organizationType'] == PADS_ORGANIZATION_LEVEL_SCHOOL) {
        $user_classes = pads_classes_get_user_classes();
        if (!empty($user_classes)) {
          $user_classes = array_keys($user_classes);
        }
        $org_classes = pads_classes_get_by_org($context->data['organizationId']);
        if (array_key_exists('classes', $org_classes)) {
          foreach ($org_classes['classes'] as $class) {
            $cid = $class['classId'];
            if (in_array($cid, $user_classes)) {
              $values[] = $cid;
            }

          }
        }
      }
      $value = implode('+', $values);
      break;

  }
  return $value;
}

/**
 * Edit form for configuring an arbitrarily assigned pads_org context.
 */
function pads_ctools_plugins_pads_org_context_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['source'] = array(
    '#title' => t('Source'),
    '#type' => 'select',
    '#options' => array(
      'user_context_select' => t('User selected'),
      // Add more as needed.
    ),
    '#required' => TRUE,
    '#default_value' => isset($conf['source']) ? $conf['source'] : NULL,
  );

  return $form;
}

/**
 * Submit callback for pads_ctools_plugins_pads_org_context_edit_form().
 */
function pads_ctools_plugins_pads_org_context_edit_form_submit($form, &$form_state) {
  $form_state['conf']['source'] = $form_state['values']['source'];
}
