<?php

/**
 * @file
 * Ctools context type for Pads Class.
 */

$plugin = array(
  'title' => t("Pads Class"),
  'description' => t('A single "pads_class" context.'),
  'context' => 'pads_ctools_plugins_context_create_pads_class',
  'context name' => 'pads_class',
  'edit form' => 'pads_ctools_plugins_pads_class_context_edit_form',
  'keyword' => 'pads_class',
  'convert list' => 'pads_class_convert_list',
  'convert' => 'pads_class_convert',
);

/**
 * Create a pads_class context.
 */
function pads_ctools_plugins_context_create_pads_class($empty, $data = NULL, $conf = FALSE) {
  $context = new ctools_context('pads_class');
  $context->plugin = 'pads_class';

  if ($empty) {
    return $context;
  }

  if ($conf) {
    $data = $conf['class_id'];
  }

  if ($data && is_numeric($data)) {
    $context->data = pads_classes_get($data);
  }

  return $context;
}

/**
 * Form to create a pads_class context.
 */
function pads_ctools_plugins_pads_class_context_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['class_id'] = array(
    '#title' => t('Class ID'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => isset($conf['class_id']) ? $conf['class_id'] : NULL,
  );

  return $form;
}

/**
 * Submit callback for pads_class context create form.
 */
function pads_ctools_plugins_pads_class_context_edit_form_submit($form, &$form_state) {
  $form_state['conf']['class_id'] = $form_state['values']['class_id'];
}

/**
 * Provide a list of tokens provided by the context.
 *
 * @return array
 */
function pads_class_convert_list() {
  return array(
    'classId' => t('Class ID'),
    'organizationId' => t('Organization ID'),
    'classIdentifier' => t('Class Identifier'),
    'sectionNumber' => t('Section number'),
    'gradeLevel' => t('Grade'),
  );
}

/**
 * Convert tokens into strings.
 *
 * @param stdClass $context
 * @param string $type
 *
 * @return string
 *   The token's value, or NULL if not available.
 */
function pads_class_convert($context, $type) {
  if (empty($context->data)) {
    return NULL;
  }

  if (!isset($context->data[$type])) {
    return NULL;
  }

  return $context->data[$type];
}
