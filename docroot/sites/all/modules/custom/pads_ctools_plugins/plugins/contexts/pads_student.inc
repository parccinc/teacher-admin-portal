<?php
/**
 * @file
 * Ctools context type for Pads Student.
 */

$plugin = array(
  'title' => t("Pads Student"),
  'description' => t('A single "pads_student" context.'),
  'context' => 'pads_student_ctools_context_create',
  'context name' => 'pads_student',
  'settings form' => 'pads_student_settings_form',
  'keyword' => 'pads_student',
  'convert list' => 'pads_student_convert_list',
  'convert' => 'pads_student_convert',
  'placeholder form' => array(
    '#type' => 'textfield',
    '#description' => t('Enter some data to represent this "pads_student".'),
  ),
);

/**
 * Create pads_student context from URL.
 *
 * @param bool $empty
 *   If true, just return an empty context.
 * @param array|string $data
 *   If from settings form, an array as from a form. If from argument, a string.
 * @param bool $conf
 *   TRUE if $data from admin configuration, FALSE if data from URL arg.
 *
 * @return array
 *   a Context object/
 */
function pads_student_ctools_context_create($empty, $data = NULL, $conf = FALSE) {
  $context = new ctools_context('pads_student');
  $context->plugin = 'pads_student';

  if ($empty) {
    return $context;
  }

  if ($data && is_numeric($data)) {
    $context->data = pads_student_load($data);
  }
  return $context;
}

/**
 * Provide a list of sub-keywords.
 *
 * This is used to provide keywords from the context for use in a content type,
 * pane, etc.
 */
function pads_student_convert_list() {
  return array(
    'studentRecordId' => t('Student record id'),
    'studentId' => t('Student id'),
    'enrollOrgId' => t('Enrollment organization id'),
    'stateIdentifier' => t('State identifier'),
    'name' => t('name'),
    'gradeLevel' => t('Grade'),
    // Todo: class ids and test assignment ids.
  );
}

/**
 * Convert context into string to be used as keyword by content types, etc.
 *
 * @param object $context
 *   The ctools context.
 * @param string $type
 *   A keyword type.
 *
 * @return mixed
 *   The value of the keyword from the context, or NULL.
 */
function pads_student_convert($context, $type) {
  $value = NULL;
  switch ($type) {
    case 'name':
      $value = pads_students_formatname($context->data);
      break;

    default:
      $value = array_key_exists($type, $context->data) ? $context->data[$type] : '';
      break;

  }
  return $value;
}
