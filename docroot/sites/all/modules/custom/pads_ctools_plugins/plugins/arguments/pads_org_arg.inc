<?php

/**
 * @file
 * Argument handler for Pads Organization.
 */

$plugin = array(
  'title' => t("Pads Organization"),
  'keyword' => 'pads_org',
  'description' => t('Creates Organization from arg.'),
  'context' => 'pads_org_arg_context',
  'settings form' => 'pads_org_arg_settings_form',
  'placeholder form' => array(
    '#type' => 'textfield',
    '#description' => t('Enter the pads_org arg'),
  ),
  'path placeholder to_arg' => TRUE,
  'path placeholder' => 'pads_org_ctools_placeholder',
);

/**
 * Determines path placeholder for pads_org.
 *
 * @param array $argument
 *   Ctools subtask argument array.
 *
 * @return string
 *   Path placeholder.
 */
function pads_org_ctools_placeholder($argument) {
  if (array_key_exists('orgLevel', $argument['settings']) && $argument['settings']['orgLevel'] == 'lowest') {
    return '%ctools_pads_org_school';
  }
  return '%pm_arg';
}

/**
 * Settings form for pads_org ctools argument.
 *
 * @param array $form
 *   Drupal form array.
 *
 * @param array $form_state
 *   Drupal form_state array.
 *
 * @param array $conf
 *   The saved configuration.
 */
function pads_org_arg_settings_form(&$form, $form_state, $conf) {
  $options = array(
    'highest' => t('Find users first State level Org or below'),
    'lowest' => t('Find users first school level Org'),
  );
  $form['settings']['orgLevel'] = array(
    '#type' => 'radios',
    '#title' => t('Level'),
    '#description' => t('If context is not provided, determine users Org at chosen level.'),
    '#options' => $options,
    '#default_value' => array_key_exists('orgLevel', $conf) ? $conf['orgLevel'] : 'highest',
  );
}

/**
 * Get the pads_org context using the arg.
 *
 * @param null|string $arg
 *   The argument.
 * @param array $conf
 *   Not used here.
 * @param bool $empty
 *   TRUE for generic, unfilled context.
 *
 * @return mixed
 *   FALSE if arg is empty, NULL if no context can be constructed, or $context
 *   object.
 */
function pads_org_arg_context($arg = NULL, $conf = NULL, $empty = FALSE) {
  // If $empty == TRUE it wants a generic, unfilled context.
  if ($empty) {
    return ctools_context_create_empty('pads_org');
  }
  if (empty($arg)) {
    return FALSE;
  }
  return ctools_context_create('pads_org', $arg);
}
