<?php
/**
 * @file
 * Argument handler for Pads Student.
 */

$plugin = array(
  'title' => t("Pads Student"),
  'keyword' => 'pads_student',
  'description' => t('Creates Student from arg.'),
  'context' => 'pads_student_arg_context',
  'placeholder form' => array(
    '#type' => 'textfield',
    '#description' => t('Enter the pads_student arg'),
  ),
);

/**
 * Get the pads_student context using the arg.
 *
 * @param null|string $arg
 *   The argument.
 * @param array $conf
 *   Not used here.
 * @param bool $empty
 *   TRUE for generic, unfilled context.
 *
 * @return mixed
 *   FALSE if arg is empty, NULL if no context can be constructed, or $context
 *   object.
 */
function pads_student_arg_context($arg = NULL, $conf = NULL, $empty = FALSE) {
  // If $empty == TRUE it wants a generic, unfilled context.
  if ($empty) {
    return ctools_context_create_empty('pads_student');
  }
  if (empty($arg)) {
    return FALSE;
  }
  return ctools_context_create('pads_student', $arg);
}
