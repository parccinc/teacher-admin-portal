<?php

/**
 * @file
 * Argument handler for Pads Class.
 */

$plugin = array(
  'title' => t("Pads Class"),
  'keyword' => 'pads_class',
  'description' => t('Creates Class from arg.'),
  'context' => 'pads_class_arg_context',
);

/**
 * Get the pads_class context using the arg.
 *
 * @param null|string $arg
 *   The argument.
 * @param array $conf
 *   Not used here.
 * @param bool $empty
 *   TRUE for generic, unfilled context.
 *
 * @return mixed
 *   FALSE if arg is empty, NULL if no context can be constructed, or $context
 *   object.
 */
function pads_class_arg_context($arg = NULL, $conf = NULL, $empty = FALSE) {
  if ($empty) {
    return ctools_context_create_empty('pads_class');
  }
  if (empty($arg)) {
    return FALSE;
  }
  return ctools_context_create('pads_class', $arg);
}
