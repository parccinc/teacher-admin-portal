<?php
/**
 * @file
 * Displays datatable of an Orgs classes.
 */

$plugin = array(
  'title' => t('Operation table for an Orgs classes'),
  'content_types' => 'class_by_org_table',
  'render callback' => 'class_by_org_table_render',
  'required context' => new ctools_context_required(t('Pads Organization'), 'pads_org'),
  'category' => array(t('PADS Organization Widgets'), -9),
);

/**
 * Render callback for class_by_org_table ctools content type.
 *
 * @param string $subtype
 *   Ctools subtype
 * @param array $conf
 *   Values from settings form.
 * @param array $args
 *   Argument array.
 * @param ctools_context $context
 *   Ctools context.
 *
 * @return stdClass
 *   Renderable block.
 */
function class_by_org_table_render($subtype, $conf, $args, $context) {
  $org = $context->data;
  $output = drupal_get_form('classes_select_table', $org);
  $block = new stdClass();
  $block->content = drupal_render($output);
  return $block;
}

/**
 * Helper function to retrieve the classes table.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 * @param array $org
 *   Ldr Org.
 *
 * @return array
 *   Form render array.
 */
function classes_select_table($form, &$form_state, $org) {
  $form = pads_classes_tableselect_form($form, $form_state, $org);

  // This functionality is not currently defined and is commented out until
  // future stories are written.

  /*
  if ($form['classes']['table']['#type'] == 'tableselect') {
    $form['classes']['operations'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('filters')),
    );
    $options = array(
      'pads_assign_users_to_classes' => t('Assign users to classes'),
      'pads_assign_students_to_classes' => t('Assign students to classes'),
    );
    $form['classes']['operations']['pads_class_operations'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('container-inline')),
      'operations' => array(
        '#type' => 'select',
        '#options' => $options,
        '#empty_option' => t('- Actions -'),
      ),
      'actions' => array(
        'submit' => array(
          '#type' => 'submit',
          '#value' => t('Next'),
          '#attributes' => array(
            'disabled' => 'disabled',
          ),
        ),
      ),
    );
  }
  */
  return $form;
}
