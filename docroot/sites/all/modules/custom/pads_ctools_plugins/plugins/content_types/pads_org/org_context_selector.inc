<?php

$plugin = array(
  'title' => t('Organization context selector'),
  'single' => TRUE,
  'defaults' => array(),
  'category' => t('PADS'),
);

function pads_ctools_plugins_org_context_selector_content_type_render() {
  $block = new stdClass();

  $block->title = t('Select Organization');
  $block->content = pads_organization_context_selector();

  return $block;
}
