<?php
/**
 * @file
 * Displays inline list of Classes.
 */

$plugin = array(
  'title' => t('Class jump menu for test assignments'),
  'content_types' => 'testassignments_class_jump_menu',
  'render callback' => 'testassignments_class_jump_menu_render',
  'required context' => new ctools_context_required(t('Pads Organization'), 'pads_org'),
  'category' => array(t('PADS Organization Widgets'), -9),
);

function testassignments_class_jump_menu_render($subtype, $conf, $args, $context) {
  $org = $context->data;
  // Abort if Org is higher than district level.
  if ($org['organizationType'] < PADS_ORGANIZATION_LEVEL_DISTRICT) {
    return array();
  }

  $block = new stdClass();

  ctools_include('jump-menu');
  $jump_form = drupal_get_form('testassignments_class_jump_menu_form', $org);
  $output['jump_form'] = $jump_form;

  $block->content = drupal_render($output);
  return $block;
}

function testassignments_class_jump_menu_form($form, &$form_state, $org) {
  $classes = pads_classes_get_user_classes();
  $jump_select = array();
  foreach ($classes as $class_id => $class) {
    $org_id = $class['organizationId'];
    if ($org['organizationId'] == $org_id) {
      $url = "manage/{$org_id}/tests/assignments/{$class_id}";
      $jump_select[url($url)] = pads_classes_format_name($class);
    }
  }
  asort($jump_select);
  $options = array(
    'title' => t('For class'),
    'default_value' => url(current_path()),
    'choose' => FALSE,
  );
  $form = ctools_jump_menu($form, $form_state, $jump_select, $options);
  return $form;
}
