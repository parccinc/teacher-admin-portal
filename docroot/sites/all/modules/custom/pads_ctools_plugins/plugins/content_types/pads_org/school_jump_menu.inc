<?php
/**
 * @file
 * Displays inline list of Org parents.
 */

$plugin = array(
  'title' => t('School jump menu'),
  'content_types' => 'school_jump_menu',
  'render callback' => 'school_jump_menu_render',
  'edit form' => 'school_jump_menu_edit_form',
  'required context' => new ctools_context_required(t('Pads Organization'), 'pads_org'),
  'category' => array(t('PADS Organization Widgets'), -9),
);

function school_jump_menu_render($subtype, $conf, $args, $context) {
  $org = $context->data;
  // Abort if Org is higher than district level.
  if ($org['organizationType'] < PADS_ORGANIZATION_LEVEL_DISTRICT) {
    return array();
  }

  $block = new stdClass();
  $contexts = array($context);
  $conf['path'] = ctools_context_keyword_substitute($conf['path'], array(), $contexts);

  ctools_include('jump-menu');
  $jump_form = drupal_get_form('school_jump_menu_form', $conf, $org);
  $output['jump_form'] = $jump_form;

  $block->content = drupal_render($output);
  return $block;
}

function school_jump_menu_form($form, &$form_state, $conf, $org) {
  $label = t('%label', array(
    '%label' => $conf['label'],
  ));
  $path = rtrim($conf['path'], '/\\');
  if (strpos($path, '%') === FALSE) {
    $path = $path . '/%';
  }

  // Find the district level org.
  $district_org = $org['organizationType'] == PADS_ORGANIZATION_LEVEL_DISTRICT ?
    $org : pads_org_load($org['parentOrganizationId']);

  // Get the schools of the district.
  $schools = pads_org_get_children($district_org['organizationId']);

  $jump_select = array();
  // Find schools user has access to.
  foreach ($schools as $school_org_id => $school) {
    if (pads_org_access($school)) {
      $jump_select[url(str_replace('%', $school_org_id, $path))] = $school['organizationName'];
    }
  }

  // Removed label -Choose- since page will always have school context. Fixes
  // PARADS 3312.
  $options = array(
    'title' => $label,
    'choose' => '',
  );
  if ($org['organizationType'] == PADS_ORGANIZATION_LEVEL_SCHOOL) {
    $options['default_value'] = url(str_replace('%', $org['organizationId'], $path));
  }

  $form = ctools_jump_menu($form, $form_state, $jump_select, $options);
  return $form;
}

/**
 * 'Edit form' callback for the content type.
 */
function school_jump_menu_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Jump menu label'),
    '#description' => 'Example: Showing classes for',
    '#size' => 50,
    '#default_value' => !empty($conf['label']) ? $conf['label'] : '',
  );
  $form['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Jump path'),
    '#description' => 'The path to jump to when school is selected, where % is the jump Org id. If no "%", then School Org id is automatically appended.',
    '#size' => 50,
    '#default_value' => !empty($conf['path']) ? $conf['path'] : '',
  );
  return $form;
}

/**
 * Content type config form submit callback.
 */
function school_jump_menu_edit_form_submit($form, &$form_state) {
  foreach (array('label', 'path') as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}
