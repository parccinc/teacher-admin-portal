<?php


/**
 * @file
 * Displays inline list of Org parents.
 */

$plugin = array(
  'title' => t('List of org parent names'),
  'content_types' => 'parent_names',
  'edit form' => 'parent_names_edit_form',
  'render callback' => 'parent_names_render',
  'description' => t('List of parent names similar to breadrumbs.'),
  'required context' => new ctools_context_required(t('Pads Organization'), 'pads_org'),
  'category' => array(t('PADS Organization Widgets'), -9),
);

function parent_names_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $form['item_list_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Item list class'),
    '#description' => t('Comma separated list of class names to apply to ul wrapper.'),
    '#size' => 50,
    '#default_value' => !empty($conf['item_list_class']) ? $conf['item_list_class'] : '',
  );
  return $form;
}

function parent_names_edit_form_submit($form, &$form_state) {
  foreach (array('item_list_class') as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}


function parent_names_render($subtype, $conf, $args, $context) {
  $org = $context->data;
  $org_id = $org['organizationId'];
  $block = new stdClass();

  // Show the state and district as unlinked breadcrumbs.
  $parent_orgs = pads_org_get_parents($org_id);
  ksort($parent_orgs);

  // Eat the root level Organization.
  array_shift($parent_orgs);
  $org_items = array();
  foreach ($parent_orgs as $parent_org_id => $parent_org) {
    $org_items[$parent_org_id] = $parent_org['organizationName'];
  }

  // If the current Organization is at the district level, add to list.
  if ($org['organizationType'] == PADS_ORGANIZATION_LEVEL_DISTRICT) {
    $org_items[$org_id] = $org['organizationName'];
  }

  $output = array(
    '#theme' => 'item_list',
    '#items' => $org_items,
  );
  if (array_key_exists('item_list_class', $conf)) {
    $classes = explode(',', $conf['item_list_class']);
    $output['#attributes']['class'] = $classes;
  }

  $block->content = drupal_render($output);
  return $block;
}
