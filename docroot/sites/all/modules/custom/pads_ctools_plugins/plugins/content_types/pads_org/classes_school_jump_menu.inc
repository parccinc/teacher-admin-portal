<?php


/**
 * @file
 * Displays inline list of Org parents.
 */

$plugin = array(
  'title' => t('School jump menu for classes'),
  'content_types' => 'classes_school_jump_menu',
  'render callback' => 'classes_school_jump_menu_render',
  'required context' => new ctools_context_required(t('Pads Organization'), 'pads_org'),
  'category' => array(t('PADS Organization Widgets'), -9),
);

function classes_school_jump_menu_render($subtype, $conf, $args, $context) {
  $org = $context->data;
  // Abort if Org is higher than district level.
  if ($org['organizationType'] < PADS_ORGANIZATION_LEVEL_DISTRICT) {
    return array();
  }

  $block = new stdClass();

  ctools_include('jump-menu');
  $jump_form = drupal_get_form('classes_school_jump_menu_form', $org);
  $output['jump_form'] = $jump_form;

  $block->content = drupal_render($output);
  return $block;
}

function classes_school_jump_menu_form($form, &$form_state, $org) {

  // Find the district level org.
  $district_org = $org['organizationType'] == PADS_ORGANIZATION_LEVEL_DISTRICT ?
    $org : pads_org_load($org['parentOrganizationId']);

  // Get the schools of the district.
  $schools = pads_org_get_children($district_org['organizationId']);

  $jump_select = array();
  // Find schools user has access to.
  foreach ($schools as $school_org_id => $school) {
    if (pads_org_access($school)) {
      $jump_select[url("classes/{$school_org_id}")] = $school['organizationName'];
    }
  }

  $options = array(
    'title' => t('Showing classes for'),
  );
  if ($org['organizationType'] == PADS_ORGANIZATION_LEVEL_SCHOOL) {
    $options['default_value'] = url("classes/{$org['organizationId']}");
  }

  $form = ctools_jump_menu($form, $form_state, $jump_select, $options);
  return $form;
}
