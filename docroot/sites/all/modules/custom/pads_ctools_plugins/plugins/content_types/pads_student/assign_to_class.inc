<?php
/**
 * @file
 * Ctools content type plugin for assigning student to class via VBO.
 */

$plugin = array(
  'title' => t('Assign student to class'),
  'description' => t('A pads_student view VBO submit of the student in context to assign to class.'),
  'content_types' => 'pads_student_content_type',
  'render callback' => 'student_assign_class_render',
  'required context' => array(
    new ctools_context_required(t('Pads Organization'), 'pads_org'),
    new ctools_context_required(t('Pads Student'), 'pads_student'),
  ),
  'category' => array(t('PADS Student Widgets'), -9),
);

/**
 * Render callback to trigger pads_student VBO.
 *
 * @param string $subtype
 *   Subtype name.
 * @param array $conf
 *   Array of config options.
 * @param array $args
 *   Array of url argument.
 * @param array $context
 *   Array of ctools_contexts for pads_org and pads_student.
 *
 * @return object
 *   Block object to render.
 */
function student_assign_class_render($subtype, $conf, $args, $context) {
  // We are expecting two contexts of an Org and a Student. Abort otherwise.
  if (!is_array($context) && count($context) != 2) {
    return NULL;
  }
  $org = $context[0]->data;
  $student = $context[1]->data;

  // Build the pads_student view to get the VBO field.
  $view = views_get_view('pads_students');
  $args = array($org['organizationId']);
  $view->set_arguments($args);
  $display_id = 'panel_pane_student_actions';
  $view->build($display_id);

  // Reset the work done by the pager.
  $view->query->set_limit(NULL);
  $view->query->set_offset(NULL);

  // Find the VBO field.
  $vbo = _views_bulk_operations_get_field($view);
  $view->execute();

  // Find the selected operation.
  $operations = $vbo->get_selected_operations();
  $operation_id = 'action::pads_classes_assign_students_to_classes';
  if (!isset($operations[$operation_id])) {
    return NULL;
  }
  $operation = views_bulk_operations_get_operation($operation_id, $vbo->get_entity_type(), $vbo->get_operation_options($operation_id));

  // Select the new student in the table row.
  $rows = array();
  $current = 1;
  foreach ($view->result as $row_index => $result) {
    if ($result->studentRecordId == $student['studentRecordId']) {

      $rows[$row_index] = array(
        'entity_id' => $vbo->get_value($result),
        'views_row' => array(),
        'position' => array(
          'current' => $current++,
          'total' => $view->total_rows,
        ),
      );
    }
  }

  // If config form submitted, have vbo create the batch.
  if ($_POST && array_key_exists('form_id', $_POST)) {
    $operation->formOptions = array('cids' => $_POST['table']);
    $selection = array();
    foreach ($rows as $row_index => $row) {
      $selection[$row_index] = $row['entity_id'];
    }
    views_bulk_operations_execute($vbo, $operation, $selection);
    batch_process('students/' . $org['organizationId']);
  }

  // Programatically submit the VBO form to load config.
  $form_id = 'views_form_pads_students_panel_pane_student_actions';
  $output = $view->preview($display_id, $args);
  $form_state = array();
  $form_state['build_info']['args'][] = $view;
  $form_state['build_info']['args'][] = $output;
  $form_state['values'] = array(
    'show_form_elements' => TRUE,
    'views_bulk_operations' => $rows,
    'submit' => 'Save',
    'select_all' => '',
    'operation' => $operation_id,
    'op' => 'Next',
  );
  drupal_form_submit($form_id, $form_state, $view, $output);

  // Rebuild submitted form and output it.
  $form_state['cancel_path'] = "students/{$org['organizationId']}";
  $form = drupal_rebuild_form($form_id, $form_state);
  $block = new stdClass();
  $block->title = '';
  $block->content = $form;
  return $block;
}
