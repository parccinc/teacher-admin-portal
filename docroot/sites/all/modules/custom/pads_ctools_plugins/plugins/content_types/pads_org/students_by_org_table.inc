<?php
/**
 * @file
 * Displays datatable of an Orgs students..
 */

$plugin = array(
  'title' => t('Operation table for an Orgs students'),
  'content_types' => 'students_by_org_table',
  'render callback' => 'students_by_org_table_render',
  'required context' => new ctools_context_required(t('Pads Organization'), 'pads_org'),
  'category' => array(t('PADS Organization Widgets'), -9),
);

function students_by_org_table_render($subtype, $conf, $args, $context) {
  $org = $context->data;
  $output = drupal_get_form('students_select_table', $org);
  $block = new stdClass();
  $block->content = drupal_render($output);
  return $block;
}

function students_select_table($form, &$form_state, $org) {
  $form = pads_students_tableselect_form($form, $form_state, $org);

  // Todo: Add operations.
  if ($form['students']['table']['#type'] == 'tableselect') {
    $form['students']['operations'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('filters')),
    );
    $options = array();
    $options['pads_assign_students_to_classes'] = t('Assign to Class');
    $form['students']['operations']['pads_students_operations'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('container-inline')),
      'operations' => array(
        '#type' => 'select',
        '#options' => $options,
        '#empty_option' => t('- Actions -'),
      ),
      'actions' => array(
        'submit' => array(
          '#type' => 'submit',
          '#value' => t('Next'),
          '#attributes' => array(
            'disabled' => 'disabled',
          ),
        ),
      ),
    );
  }

  return $form;
}
