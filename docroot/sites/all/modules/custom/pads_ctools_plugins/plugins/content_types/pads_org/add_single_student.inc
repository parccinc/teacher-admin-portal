<?php
/**
 * @file
 * Ctools content type plugin for add_single_student button.
 */

$plugin = array(
  'title' => t('Add Single Student Button'),
  'description' => t('Displays a button on the Students Tab to add a student.'),
  'content_types' => 'add_single_student',
  'render callback' => 'add_single_student_render',
  'required context' => new ctools_context_required(t('Pads Organization'), 'pads_org'),
  'edit form' => 'add_single_student_edit_form',
  'category' => array(t('PADS Organization Widgets'), -9),
);

/**
 * Render callback function.
 *
 * @param $subtype
 * @param $conf
 * @param $args
 * @param $context
 *
 * @return stdClass
 */
function add_single_student_render($subtype, $conf, $args, $context) {
  $org = $context->data;
  $label = array_key_exists('label', $conf) ? $conf['label'] :
    t('Add Single Student');
  $classes = array_key_exists('classes', $conf) ?
    explode(' ', $conf['classes']) : array();

  $options = array(
    'attributes' => array(
      'class' => $classes,
    ),
    'query' => drupal_get_destination(),
  );
  $content = l($label, "student/add/{$org['organizationId']}", $options);

  $block = new stdClass();
  $block->content = $content;
  return $block;
}

/**
 * Settings form for the content type.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 *
 * @return array
 *   Form render array.
 */
function add_single_student_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Button label'),
    '#size' => 50,
    '#default_value' => !empty($conf['label']) ? $conf['label'] : '',
  );
  $form['classes'] = array(
    '#type' => 'textfield',
    '#title' => t('Button classes'),
    '#description' => 'Input classes to apply to the button.',
    '#size' => 50,
    '#default_value' => !empty($conf['classes']) ? $conf['classes'] : '',
  );
  return $form;
}

/**
 * Content type config form submit callback.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 */
function add_single_student_edit_form_submit($form, &$form_state) {
  foreach (array('label', 'classes') as $key) {
    $value = $form_state['values'][$key];
    if ($key == 'label') {
      $value = filter_xss_admin($value);
    }
    $form_state['conf'][$key] = $value;
  }
}
