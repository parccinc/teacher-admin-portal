<?php
/**
 * @file
 * A user login page form for batchusers.
 */

$plugin = array(
  'title' => t('Profile2 Regpath user login form'),
  'single' => TRUE,
  'content_types' => array('profile2_regpath_userlogin'),
  'render callback' => 'profile2_regpath_userlogin_render',
  'defaults' => array(),
  'edit form' => 'profile2_regpath_userlogin_edit_form',
  'category' => array(t('PADS'), -9),
);

function profile2_regpath_userlogin_edit_form($form, &$form_state) {
  $options = array();
  $reg_paths = profile2_regpath_get_reg_paths();
  foreach ($reg_paths as $key => $reg_path) {
    $options[$reg_path->path] = $reg_path->path;
  }

  $form['regpath'] = array(
    '#type' => 'select',
    '#title' => t('Registration path'),
    '#options' => $options,
    '#required' => TRUE,
  );

  $conf = array_key_exists('conf', $form_state) ? $form_state['conf'] : array();
  if (array_key_exists('regpath', $conf) && !empty($conf['regpath'])) {
    $form['regpath']['#default_value'] = $conf['regpath'];
  }

  return $form;
}

function profile2_regpath_userlogin_edit_form_submit($form, &$form_state) {
  foreach (array('regpath') as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Run-time rendering of the body of the block.
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time.
 * @param $args
 * @param $context
 *   Context - in this case we don't have any.
 *
 * @return
 *   An object with at least title and content members.
 */
function profile2_regpath_userlogin_render($subtype, $conf, $args, $context) {
  $block = new stdClass();

  $path = $conf['regpath'];
  $profiles = profile2_regpath_get_profiles($path);
  $regpath = reset($profiles);

  $content = array();

  module_load_include('inc', 'profile2_regpath', 'profile2_regpath');
  $content['form'] = _profile2_regpath_user_login($regpath);

  $path = $regpath->path;
  $items = array();
  if (variable_get('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL)) {
    $items[] = l(t('Create new account'), "$path/register", array('attributes' => array('title' => t('Create a new user account.'))));
  }
  $items[] = l(t('Request new password'), "$path/password", array('attributes' => array('title' => t('Request new password via e-mail.'))));
  $content['links'] = array('#markup' => theme('item_list', array('items' => $items)));

  $block->title = drupal_get_title();
  $block->content = $content;

  return $block;

}
