<?php
/**
 * @file
 * Ctools content type to display text and button is ISBE.
 */

$plugin = array(
  'title' => t('ISBE Management Button'),
  'description' => t('Display text and button to ISBE Management tool.'),
  'single' => TRUE,
  'required context' => array(
    new ctools_context_required(t('User'), 'user'),
    new ctools_context_required(t('Organization'), 'pads_org'),
  ),
  'category' => t('PADS'),
);

/**
 * Render text and button.
 *
 * @param string $subtype
 *   Ctools subtype.
 * @param array $conf
 *   Values from settings form.
 * @param array $args
 *   Argument array.
 * @param object $context
 *   Ctools context.
 *
 * @return \stdClass
 *   Renderable block.
 */
function pads_ctools_plugins_isbe_button_content_type_render($subtype, $conf, $args, $context) {
  $user = $context[0]->data;
  $org = $context[1]->data;

  if (!$user || !$org || !module_exists('pads_isbe_client')) {
    return NULL;
  }

  if (!pads_ctools_plugins_isbe_mgmt_access($user)) {
    return NULL;
  }

  $description = array_key_exists('description', $conf) ? $conf['description'] :
    '';
  $label = array_key_exists('label', $conf) ? $conf['label'] :
    t('ISBE Management');
  $classes = array_key_exists('classes', $conf) ?
    explode(' ', $conf['classes']) : array();

  $content = '<div class="description">' . $description . '</div>';
  $classes = implode(' ', array_unique(array_map('drupal_html_class', $classes)));

  ctools_include('ajax');
  ctools_include('modal');
  ctools_modal_add_js();

  $content .= ctools_modal_text_button($label, 'isbe/management/nojs', $label, $classes);

  $block = new stdClass();
  $block->content = $content;
  return $block;
}

/**
 * Settings form for the content type.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 *
 * @return array
 *   Form render array.
 */
function pads_ctools_plugins_isbe_button_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => !empty($conf['description']) ? $conf['description'] : '',
  );
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Button label'),
    '#size' => 50,
    '#default_value' => !empty($conf['label']) ? $conf['label'] : '',
  );
  $form['classes'] = array(
    '#type' => 'textfield',
    '#title' => t('Button classes'),
    '#description' => 'Input classes to apply to the button.',
    '#size' => 50,
    '#default_value' => !empty($conf['classes']) ? $conf['classes'] : '',
  );
  return $form;
}

/**
 * Content type config form submit callback.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 */
function pads_ctools_plugins_isbe_button_content_type_edit_form_submit($form, &$form_state) {
  foreach (array('description', 'label', 'classes') as $key) {
    $value = $form_state['values'][$key];
    if ($key == 'description' || $key == 'label') {
      $value = filter_xss_admin($value);
    }
    $form_state['conf'][$key] = $value;
  }
}
