<?php
/**
 * @file
 * Defines ctools content_type plugin for add file button in modal.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Upload Secure file button'),
  'description' => t('Button that launches a modal window for node/add/secure_file'),

  // 'single' => TRUE means has no subtypes.
  'single' => TRUE,
  // Constructor.
  'content_types' => array('addfile'),
  // Name of a function which will render the block.
  'render callback' => 'addfile_render',
  // The default context.
  'defaults' => array(),

  'edit form' => 'addfile_edit_form',

  // Presents a block which is used in the preview of the data.
  // In Panels this is the preview pane shown on the panels building page.
  'admin info' => 'pads_ctools_plugins_addfile_admin_info',

  'category' => array(t('PADS'), -9),
);

/**
 * Preview callback.
 *
 * @param $subtype
 * @param $conf
 * @param null $context
 * @return stdClass
 */
function pads_ctools_plugins_addfile_admin_info($subtype, $conf, $context = NULL) {
  $block = addfile_render($subtype, $conf, array("Example"), $context);
  return $block;
}

/**
 * Run-time rendering of the body of the block.
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time.
 * @param $args
 * @param $context
 *   Context - in this case we don't have any.
 *
 * @return stdClass
 *   An object with at least title and content members.
 */
function addfile_render($subtype, $conf, $args, $context) {
  // Include the CTools tools that we need.
  ctools_include('ajax');
  ctools_include('modal');

  // Add CTools' javascript to the page.
  ctools_modal_add_js();

  $button_form = _addfile_ajax_button_form($conf);

  $block = new stdClass();

  // The title actually used in rendering.
  $block->title = '';
  $block->content = drupal_render($button_form);
  return $block;
}

/**
 * Provide a form for an ajax modal button.
 */
function _addfile_ajax_button_form($conf) {
  $form = array();

  if (array_key_exists('button_desc', $conf) && !empty($conf['button_desc'])) {
    if (array_key_exists('button_url', $conf) && !empty($conf['button_url'])) {
      if (user_access('import batch users')) {
        // Build link out.
        $link_text = t('@button_text', array('@button_text' => $conf['button_text']));
        $link_href = $conf['button_url'];
        $options = array (
          'attributes' => array (
            'class' => array($conf['button_class']),
          ),
        );
        // Build out button here to make markup match theme.
        $form['buttons']['other_button'] = array(
          '#markup' => l($link_text, $link_href, $options),
        );
        $form['buttons']['other_button']['#prefix'] = '<div class="button-container">';
        $form['buttons']['other_button']['#suffix'] = '</div>';
      }
    }
    $button_label = array_key_exists('button_label', $conf) ? $conf['button_label'] : ' + ' . t('Upload File');
    $form['buttons']['ajax_button'] = array(
      '#type' => 'button',
      '#value' => $button_label,
      '#attributes' => array('class' => array('ctools-use-modal', 'ctools-modal-zurb-modal-style')),
      '#id' => 'addfile-button',
    );
    $form['buttons']['ajax_button']['#prefix'] = '<div class="button-container">';
    $form['buttons']['ajax_button']['#suffix'] = '</div>';
    $form['buttons']['url'] = array(
      '#type' => 'hidden',
      // The name of the class is the #id of $form['ajax_button'] with "-url"
      // suffix.
      '#attributes' => array('class' => array('addfile-button-url')),
      '#value' => url('secure-file/add/nojs'),
    );
    if (array_key_exists('css_button_label', $conf)) {
      $form['buttons']['#prefix'] = "<div class=\"{$conf['css_button_label']}\">";
      $form['buttons']['#suffix'] = '</div>';
    }
    $form['desc'] = array(
      '#type' => 'item',
      '#markup' => $conf['button_desc'],
    );
    if (array_key_exists('css_button_desc', $conf)) {
      $form['desc']['#prefix'] = "<div class=\"{$conf['css_button_desc']}\">";
      $form['desc']['#suffix'] = '</div>';
    }
  }

  return $form;
}

/**
 * 'Edit form' callback for the content type.
 *
 * This example just returns a form; validation and submission are standard drupal
 * Note that if we had not provided an entry for this in hook_content_types,
 * this could have had the default name
 * ctools_plugin_example_no_context_content_type_edit_form.
 */
function addfile_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $form['button_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Button Text'),
    '#description' => '',
    '#size' => 50,
    '#default_value' => !empty($conf['button_text']) ? $conf['button_text'] : t('Import Users'),
  );
  $form['button_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Button URL'),
    '#description' => '',
    '#size' => 50,
    '#default_value' => !empty($conf['button_url']) ? $conf['button_url'] : '/admin/user/import',
  );
  $form['button_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Button Classes'),
    '#description' => '',
    '#size' => 50,
    '#default_value' => !empty($conf['button_class']) ? $conf['button_class'] : 'button radius tiny',
  );
  $form['button_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Button label'),
    '#description' => '',
    '#size' => 50,
    '#default_value' => !empty($conf['button_label']) ? $conf['button_label'] : ' + ' . t('Upload File'),
  );
  $form['css_button_label'] = array(
    '#type' => 'textfield',
    '#title' => t('CSS classes'),
    '#description' => t('CSS classes for button label container.'),
    '#default_value' => !empty($conf['css_button_label']) ? $conf['css_button_label'] : '',
  );

  $form['button_desc'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#description' => t('Include an optional description to display before the button.'),
    '#default_value' => !empty($conf['button_desc']) ? $conf['button_desc'] : '',
  );
  $form['css_button_desc'] = array(
    '#type' => 'textfield',
    '#title' => t('CSS classes'),
    '#description' => t('CSS classes for button description container.'),
    '#default_value' => !empty($conf['css_button_desc']) ? $conf['css_button_desc'] : '',
  );
  return $form;
}

function addfile_edit_form_submit($form, &$form_state) {
  foreach (array(
    'button_text', 'button_url', 'button_class', 'button_label', 'button_desc', 'css_button_label', 'css_button_desc'
  ) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}
