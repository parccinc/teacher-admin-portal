<?php
/**
 * @file
 * Dashboard box/card functionality and markup cTools Content Type file.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Dashboard boxes'),
  'description' => t("Dashboard boxes for Users, Orgs, Students."),
  'required context' => new ctools_context_required(t('User'), 'user'),
  'edit form' => 'dashboard_boxes_edit_form',
  'category' => array(t('PADS'), -9),
);

/**
 * Dashboard box render callback.
 *
 * @param string|NULL $subtype
 *   Content type subtype.
 * @param array $conf
 *   Config array.
 * @param array $args
 *   Pane arguments.
 * @param ctools_context_required $context
 *   User context.
 *
 * @return object
 *   Drupal block object.
 *
 * @throws Exception
 *   Exception.
 */
function pads_ctools_plugins_dashboard_boxes_content_type_render($subtype, $conf, $args, $context) {
  // $user = clone $context->data;
  // Get currently selected org.
  $current_org = pads_organization_get_selected_org();

  $block = new stdClass();
  // Check if the user has an org.
  if ($current_org) {

    // Start dashboard by orgs array to store which boxes get rendered.
    $dashboard_by_orgs = array();

    // Load org data.
    $org = pads_org_load($current_org);

    // No Org, abort.
    if (!$org) {
      return NULL;
    }
    $block->title = '';

    $boxes = array();
    $level = $org['organizationType'];
    $markup = "<div class=\"{$conf['row_class']}\" {$conf['row_attributes']}>";

    // Organization box.
    if (user_access('view organizations') && $org['organizationType'] < PADS_ORGANIZATION_LEVEL_SCHOOL) {
      $boxes[] = dashboard_box('organization', $org, $conf);
    }

    // User box.
    if (user_access('view users')) {
      $boxes[] = dashboard_box('user', $org, $conf);
    }

    // Student box for school level.
    // Double check if the "view any student" is actually needed here?
    if (
      user_access('view students') || user_access('view any student') &&
      in_array($level, array(PADS_ORGANIZATION_LEVEL_SCHOOL, PADS_ORGANIZATION_LEVEL_DISTRICT))
    ) {
      $boxes[] = dashboard_box('student', $org, $conf);
    }

    // Classes by Organization box, for school and district level users with
    // view any class in Organization permission.
    if (
      user_access('view any class') &&
      $org['organizationType'] >= PADS_ORGANIZATION_LEVEL_DISTRICT
    ) {
      $boxes[] = dashboard_box('class', $org, $conf);
    }

    // Get user classes to verify access to classes. Only test admins and,
    // Only test admins with assigned classes. Per BA this can show with a,
    // Zero value until assigned classes happen.
    $user_classes = pads_classes_get_user_classes();

    if (
      user_access('view classes') &&
      $org['organizationType'] >= PADS_ORGANIZATION_LEVEL_SCHOOL &&
      // Checking if the user has classes.
      $user_classes
    ) {
      $boxes[] = dashboard_box('assigned_class', $org, $conf);
    }

    if (
      user_access('manage tests in class') &&
      $org['organizationType'] >= PADS_ORGANIZATION_LEVEL_SCHOOL &&
      // Checking if the user has classes.
      $user_classes
    ) {
      $boxes[] = dashboard_box('test_assignments', $org, $conf);
    }

    $markup .= theme('dashboard_boxes', array(
      'boxes' => $boxes,
    ));

    // End row tag.
    $markup .= "</div>";

    $dashboard_by_orgs[] = array(
      'boxes' => array(
        '#type' => 'item',
        '#markup' => $markup,
      ),
    );
  }
  // Since we are checking for current org above this can throw an error,
  // I want to check for this error and make sure that we take care of that,
  // Situation, which I think it's the batch users redirect not happening.
  if (isset($dashboard_by_orgs)) {
    $block->content = drupal_render($dashboard_by_orgs);
  }
  else {
    // When user has No Current Orgs, this really shouldn't show for anyone,
    // But certain test cases will show this until the page is refreshed.
    $block->content = '<h5>No Current Organizations</h5>';
  }
  return $block;
}

/**
 * Gets a dashboard box renderable array.
 *
 * @param string $type
 *   Type of box; organization, user, student, or class.
 * @param mixed $org
 *   Organization array.
 * @param mixed $conf
 *   Ctools content type config.
 *
 * @return array
 *   Dashboard box renderable array for type.
 */
function dashboard_box($type, $org, $conf) {
  $box = array();
  $org_id = $org['organizationId'];

  switch ($type) {
    case 'organization':
      $levels = pads_org_levels(FALSE, TRUE);
      $level = $levels[$org['organizationType'] + 1];
      $child_count = l($org['childCount'], "organizations/$org_id", array(
        'attributes' => array(
          'id' => 'org-' . $org_id . '-count',
          'title' => t('My !level', array('!level' => $level)),
        ),
      ));
      $dashboard_card_path = "organizations/$org_id";
      $org_actions = '';
      if (user_access('import organizations')) {
        $org_actions = l(
          '<span class="icon icon-add-filled" aria-hidden="true"></span>',
          'organizations/import', array(
            'html' => TRUE,
            'attributes' => array(
              'id' => 'org-' . $org_id . '-action-add',
              'title' => t('Import !level', array('!level' => $level)),
            ),
          )
        );
      }

      $box = array(
        'title' => $level,
        'count' => $child_count,
        'actions' => $org_actions,
        'ul_wrapper_class' => $conf['ul_wrapper_class'],
        'ul_class' => $conf['ul_class'],
        'ul_attributes' => $conf['ul_attributes'],
        'li_title_class' => $conf['li_title_class'],
        'li_number_class' => $conf['li_number_class'],
        'li_action_class' => $conf['li_action_class'],
        'dashboard_icon' => 'icon-organization',
        'dashboard_card_path' => $dashboard_card_path,
      );
      break;

    case 'user':
      $user_count = l(pads_org_user_count($org_id), "tapusers/{$org_id}", array(
        'attributes' => array(
          'id' => 'user-' . $org_id . '-count',
          'title' => t('My Users'),
        ),
      ));
      $dashboard_card_path = "tapusers/$org_id";
      $user_actions = '';
      if (user_access('import users in organization')) {
        $user_actions = l(
          '<span class="icon icon-add-filled" aria-hidden="true"></span>',
          'tapusers/import', array(
            'html' => TRUE,
            'attributes' => array(
              'id' => 'user-' . $org_id . '-action-add',
              'title' => t('Import Users'),
            ),
          )
        );
      }

      $box = array(
        'title' => t('Users'),
        'count' => $user_count,
        'actions' => $user_actions,
        'ul_wrapper_class' => $conf['ul_wrapper_class'],
        'ul_class' => $conf['ul_class'],
        'ul_attributes' => $conf['ul_attributes'],
        'li_title_class' => $conf['li_title_class'],
        'li_number_class' => $conf['li_number_class'],
        'li_action_class' => $conf['li_action_class'],
        'dashboard_icon' => 'icon-user',
        'dashboard_card_path' => $dashboard_card_path,
      );
      break;

    case 'student':
      $student_actions = '';
      $dashboard_card_path = '';

      // Student count, this is different depending on what the user can see.
      if (user_access('view any student')) {
        $student_count = ($org = pads_org_load($org_id)) ? $org['studentCount'] : 0;
      }
      else {
        $classes = pads_classes_get_user_classes();
        $students = array();
        foreach ($classes as $class) {
          if ($class['organizationId'] == $org_id) {
            $class_students = pads_students_get_by_class($class['classId']);
            if (!array_key_exists('error', $class_students)) {
              foreach ($class_students as $class_student) {
                $students[$class_student['studentRecordId']] = $class_student;
              }
            }
          }
        }
        $student_count = count($students);
      }

      // Determine student link based on permissions.
      if (user_access('view any student')) {
        $dashboard_card_path = "students/$org_id";
      }
      elseif (user_access('view students')) {
        $dashboard_card_path = "manage/$org_id/students";
      }

      $student_count = l($student_count, $dashboard_card_path, array(
        'attributes' => array(
          'id' => 'students-' . $org_id . '-count',
          'title' => t('My Students'),
        ),
      ));

      // Student actions.
      if (user_access('import students')) {
        $student_actions = l(
          '<span class="icon icon-add-filled" aria-hidden="true"></span>',
          'students/import', array(
            'html' => TRUE,
            'attributes' => array(
              'id' => 'students-' . $org_id . '-action-add',
              'title' => t('Import Students'),
            ),
          )
        );
      }

      $box = array(
        'title' => t('Students'),
        'count' => $student_count,
        'actions' => $student_actions,
        'ul_wrapper_class' => $conf['ul_wrapper_class'],
        'ul_class' => $conf['ul_class'],
        'ul_attributes' => $conf['ul_attributes'],
        'li_title_class' => $conf['li_title_class'],
        'li_number_class' => $conf['li_number_class'],
        'li_action_class' => $conf['li_action_class'],
        'dashboard_icon' => 'icon-students',
        'dashboard_card_path' => $dashboard_card_path,
      );
      break;

    case 'class':
      $class_count = l(pads_classes_get_count($org_id), "classes/{$org_id}", array(
        'attributes' => array(
          'id' => 'classes-' . $org_id . '-count',
          'title' => t('My Classes'),
        ),
      ));
      $dashboard_card_path = "classes/{$org_id}";
      $class_actions = '';
      if (user_access('import classes')) {
        $class_actions = l(
          '<span class="icon icon-add-filled" aria-hidden="true"></span>',
          'class/import', array(
            'html' => TRUE,
            'attributes' => array(
              'id' => 'classes-' . $org_id . '-action-add',
              'title' => t('Import Classes'),
            ),
          )
        );
      }
      $box = array(
        'title' => t('Classes'),
        'count' => $class_count,
        'actions' => $class_actions,
        'ul_wrapper_class' => $conf['ul_wrapper_class'],
        'ul_class' => $conf['ul_class'],
        'ul_attributes' => $conf['ul_attributes'],
        'li_title_class' => $conf['li_title_class'],
        'li_number_class' => $conf['li_number_class'],
        'li_action_class' => $conf['li_action_class'],
        'dashboard_icon' => 'icon-classes',
        'dashboard_card_path' => $dashboard_card_path,
      );
      break;

    case 'test_assignments':
      // Get user classes test assignments by current org.
      $query = array(
        'organizationId' => $org_id,
      );
      $test_assignment = pads_tests_get_test_assignments($query);

      // Get user class assignment count.
      $user_test_assignments = count($test_assignment);
      $class_count = l($user_test_assignments, "manage/{$org_id}/tests/assignments", array(
        'attributes' => array(
          'id' => 'tests-' . $org_id . '-count',
          'title' => t('Test Assignments'),
        ),
      ));
      $dashboard_card_path = "manage/{$org_id}/tests/view";
      // Assign test modal.
      $test_actions = '';
      if (user_access('assign to class')) {
        $test_actions = l(
          '<span class="icon icon-add-filled" aria-hidden="true"></span>',
          "class/nojs/noclass/assigntest/{$org_id}", array(
            'html' => TRUE,
            'attributes' => array(
              'id' => 'tests-' . $org_id . '-action-add',
              'class' => array(
                'ctools-use-modal',
                'ctools-modal-zurb-modal-style',
              ),
              'title' => t('Assign Test'),
            ),
          )
        );
        ctools_include('modal');
        ctools_modal_add_js();
      }

      $box = array(
        'title' => t('Assigned Tests'),
        'count' => $class_count,
        'actions' => $test_actions,
        'ul_wrapper_class' => $conf['ul_wrapper_class'],
        'ul_class' => $conf['ul_class'],
        'ul_attributes' => $conf['ul_attributes'],
        'li_title_class' => $conf['li_title_class'],
        'li_number_class' => $conf['li_number_class'],
        'li_action_class' => $conf['li_action_class'],
        'dashboard_icon' => 'icon-assign-tests',
        'dashboard_card_path' => $dashboard_card_path,
      );
      break;

    case 'assigned_class':
      // Get user class assignments.
      $org_user_classes = pads_classes_get_user_classes_by_org($account = NULL, $org_id);
      $org_user_classes_count = count($org_user_classes);
      $class_count = l($org_user_classes_count, "manage/{$org_id}/classes", array(
        'attributes' => array(
          'id' => 'classes-' . $org_id . '-count',
          'title' => t('My Classes'),
        ),
      ));
      $dashboard_card_path = "manage/{$org_id}/classes";
      // No actions for classes for test admin.
      $class_actions = '';

      $box = array(
        'title' => t('Classes'),
        'count' => $class_count,
        'actions' => $class_actions,
        'ul_wrapper_class' => $conf['ul_wrapper_class'],
        'ul_class' => $conf['ul_class'],
        'ul_attributes' => $conf['ul_attributes'],
        'li_title_class' => $conf['li_title_class'],
        'li_number_class' => $conf['li_number_class'],
        'li_action_class' => $conf['li_action_class'],
        'dashboard_icon' => 'icon-classes',
        'dashboard_card_path' => $dashboard_card_path,
      );
      break;

    default:
      break;

  }

  return $box;
}

/**
 * Edit form for dashboard box content type.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 *
 * @return array
 *   Drupal form array.
 */
function dashboard_boxes_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $form['row_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Row class'),
    '#description' => '',
    '#size' => 50,
    '#default_value' => !empty($conf['row_class']) ? $conf['row_class'] : '',
  );
  $form['row_attributes'] = array(
    '#type' => 'textfield',
    '#title' => t('Row attributes'),
    '#description' => '',
    '#size' => 50,
    '#default_value' => !empty($conf['row_attributes']) ? $conf['row_attributes'] : '',
  );
  $form['ul_wrapper_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Div class wrapper around ul'),
    '#description' => '',
    '#default_value' => !empty($conf['ul_wrapper_class']) ? $conf['ul_wrapper_class'] : '',
  );
  $form['ul_class'] = array(
    '#type' => 'textfield',
    '#title' => t('UL class'),
    '#description' => '',
    '#default_value' => !empty($conf['ul_class']) ? $conf['ul_class'] : '',
  );
  $form['ul_attributes'] = array(
    '#type' => 'textfield',
    '#title' => t('UL attributes'),
    '#description' => '',
    '#default_value' => !empty($conf['ul_attributes']) ? $conf['ul_attributes'] : '',
  );

  $form['li_title_class'] = array(
    '#type' => 'textfield',
    '#title' => t('LI title class'),
    '#description' => t('LI class to display dashboard box title.'),
    '#default_value' => !empty($conf['li_title_class']) ? $conf['li_title_class'] : '',
  );
  $form['li_number_class'] = array(
    '#type' => 'textfield',
    '#title' => t('LI number class'),
    '#description' => t('LI class to display total number.'),
    '#default_value' => !empty($conf['li_number_class']) ? $conf['li_number_class'] : '',
  );
  $form['li_action_class'] = array(
    '#type' => 'textfield',
    '#title' => t('LI action class'),
    '#description' => t('LI class to around action buttons.'),
    '#default_value' => !empty($conf['li_action_class']) ? $conf['li_action_class'] : '',
  );
  return $form;
}

/**
 * Content type config form submit callback.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 */
function dashboard_boxes_edit_form_submit($form, &$form_state) {
  $keys = array(
    'row_class',
    'row_attributes',
    'ul_wrapper_class',
    'ul_class',
    'ul_attributes',
    'li_title_class',
    'li_number_class',
    'li_action_class',
  );
  foreach ($keys as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}
