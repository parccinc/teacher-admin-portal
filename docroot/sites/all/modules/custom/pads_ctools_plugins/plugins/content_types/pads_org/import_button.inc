<?php
/**
 * @file
 * Ctools content type to display button to import students page.
 */

$plugin = array(
  'title' => t('Import Button'),
  'description' => t('Button to link to an import page.'),
  'single' => TRUE,
  'category' => t('PADS'),
);

/**
 * Render text and button.
 *
 * @param string $subtype
 *   Ctools subtype.
 * @param array $conf
 *   Values from settings form.
 * @param array $args
 *   Argument array.
 * @param object $context
 *   Ctools context.
 *
 * @return \stdClass
 *   Renderable block.
 */
function pads_ctools_plugins_import_button_content_type_render($subtype, $conf, $args, $context) {

  $label = array_key_exists('label', $conf) ? $conf['label'] :
    t('Upload Students');
  $classes = array_key_exists('classes', $conf) ?
    explode(' ', $conf['classes']) : array();
  $path = array_key_exists('path', $conf) ? $conf['path'] : '';

  $options = array(
    'attributes' => array(
      'class' => $classes,
    ),
  );
  $content = l($label, $path, $options);

  $block = new stdClass();
  $block->content = $content;
  return $block;

}

/**
 * Settings form for the content type.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 *
 * @return array
 *   Form render array.
 */
function pads_ctools_plugins_import_button_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Button label'),
    '#size' => 50,
    '#default_value' => !empty($conf['label']) ? $conf['label'] : '',
  );
  $form['classes'] = array(
    '#type' => 'textfield',
    '#title' => t('Button classes'),
    '#description' => 'Input classes to apply to the button.',
    '#size' => 50,
    '#default_value' => !empty($conf['classes']) ? $conf['classes'] : '',
  );
  $form['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Import url'),
    '#description' => 'Input url to import page (i.e. students/import).',
    '#size' => 50,
    '#default_value' => !empty($conf['path']) ? $conf['path'] : '',
  );
  return $form;
}

/**
 * Content type config form submit callback.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 */
function pads_ctools_plugins_import_button_content_type_edit_form_submit($form, &$form_state) {
  foreach (array('label', 'classes', 'path') as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}
