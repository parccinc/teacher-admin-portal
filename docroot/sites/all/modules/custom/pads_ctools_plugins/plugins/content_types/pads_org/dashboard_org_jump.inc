<?php
/**
 * @file
 * Displays inline list of Org parents for Dashboard.
 */

$plugin = array(
  'title' => t('PADS Org jump menu'),
  'render callback' => 'dashboard_org_jump_render',
  'required context' => array(
    new ctools_context_required(t('User'), 'user'),
  ),
  'category' => array(t('PADS Organization Widgets'), -10),
  'defaults' => array(),
);

/**
 *
 */
function dashboard_org_jump_render($subtype, $conf, $args, $context) {
  $user = '';
  if (!isset ($_SESSION['currentPADSOrg'])) {
    $_SESSION['currentPADSOrg'] = '';
  }
  $org = pads_organization_get_user_orgs($user);

  $block = new stdClass();
  ctools_include('ajax');
  $org_jump_form = drupal_get_form('dashboard_org_jump_form', $conf, $org);
  $output['org_jump_form'] = $org_jump_form;

  $block->content = drupal_render($output);
  $block->title = 'Dasboard Org Swicher';
  $block->id = 'dashboard_org_jump_block';
  return $block;
}

/**
 *
 */
function dashboard_org_jump_form($form, &$form_state, $conf, $org) {

  $jump_select = array();
  // Find schools user has access to.
  foreach ($org as $org_id) {
    if (pads_org_access($org_id)) {
      $org_key = $org_id['organizationId'];
      $jump_select[$org_key] = $org_id['organizationName'];
    }
  }

  ctools_include('ajax');
  $form['org_switcher'] = array(
    '#type' => 'select',
    '#title' => '',
    '#options' => $jump_select,
    '#default_value' => $_SESSION['currentPADSOrg'],
    '#ajax' => array(
      'event' => 'change',
      'callback' => 'dashboard_org_jump_form_ajax_callback',
    ),
  );

  return $form;
}


/**
 * 'Edit form' callback for the content type.
 */
function dashboard_org_jump_edit_form($form, &$form_state) {

  return $form;
}

/**
 * Ajax form submit callback.
 */
function dashboard_org_jump_form_ajax_callback($form, &$form_state) {
  $_SESSION['currentPADSOrg'] = $form['org_switcher']['#value'];

}
