<?php

/**
 * @file
 * Plugin to provide an relationship handler for a user's default class.
 */

$plugin = array(
  'title' => t('User\'s Default PADS class'),
  'keyword' => 'user_default_class',
  'description' => t('Provides the user\'s default class as a context.'),
  'required context' => array(
    new ctools_context_required(t('User'), 'user'),
    new ctools_context_optional(t('Organization'), 'pads_org'),
  ),
  'context' => 'pads_user_default_class_context',
);

/**
 * Return a new pads_class context based on user context.
 */
function pads_user_default_class_context($context) {
  if (!$uid = $context[0]->data->uid) {
    return ctools_context_create_empty('pads_class');
  }

  if (!$account = user_load($uid)) {
    return ctools_context_create_empty('pads_class');
  }

  $org = !empty($context[1]->data) ? $context[1]->data : NULL;
  if ($class = pads_classes_user_default_class($account, $org, TRUE)) {
    return ctools_context_create('pads_class', $class['classId']);
  }

  return ctools_context_create_empty('pads_class');
}
