<?php
/**
 * @file
 * Ctools access plugin check if user has classes assigned to them.
 */

$plugin = array(
  'title' => t("Pads user has classes"),
  'description' => t('Verify user has classes assigned to them.'),
  'callback' => 'pads_user_has_classes_access',
  'required context' => new ctools_context_required(t('User'), 'user'),
  'summary' => 'pads_user_has_classes_summary',
);

/**
 * Check for access.
 */
function pads_user_has_classes_access($conf, $context) {
  // As far as I know there should always be a context at this point, but this
  // is safe.
  if (empty($context) || empty($context->data)) {
    return FALSE;
  }

  return !empty(pads_classes_get_user_classes($context->data));
}

/**
 * Summary for pads_user_has_classes access plugin.
 *
 * @param null|array $settings
 *   Settings array, if exists.
 * @param ctools_context $context
 *   Ctools context object.
 *
 * @return string
 *   The summary.
 */
function pads_user_has_classes_summary($settings, $context) {
  return t('@identifier has classes assigned to them.', array(
    '@identifier' => $context->get_identifier(),
  ));
}
