<?php
/**
 * @file
 * Ctools access plugin to verify users assigned Org levels.
 */

$plugin = array(
  'title' => t("ADS user Organization levels"),
  'description' => t('Verify user assigned Organization levels.'),
  'callback' => 'pads_user_org_level_access',
  'required context' => new ctools_context_required(t('User'), 'user'),
  'settings form' => 'pads_user_org_level_settings',
  'summary' => 'pads_user_org_level_summary',
);

/**
 * Access plugin settings form.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 * @param array|NULL $conf
 *   Plugin config options, if available.
 *
 * @return array
 *   Drupal form array.
 */
function pads_user_org_level_settings(&$form, &$form_state, $conf) {
  $levels = pads_org_levels();
  $form['settings']['#type'] = 'container';
  $form['settings']['#attributes']['style'] = "clear: both;";

  $value = ($conf && !empty($conf['levels'])) ? $conf['levels'] : array();
  if (!is_array($value)) {
    $value = array($value);
  }

  $form['settings']['levels'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Level access.'),
    '#description' => t('Select Organization levels to verify against all user-assigned Orgs.'),
    '#required' => TRUE,
    '#options' => $levels,
    '#default_value' => $value,
  );
  return $form;
}

/**
 * Access plugin callback.
 *
 * @param array $conf
 *   Config options.
 * @param ctools_context_required $context
 *   Ctools context for user.
 *
 * @return bool
 *   TRUE if user has Orgs in desired level, FALSE otherwise.
 */
function pads_user_org_level_access($conf, $context) {
  // As far as I know there should always be a context at this point, but this
  // is safe.
  if (empty($context) || !count($context->data)) {
    return FALSE;
  }
  $user = $context->data;
  $user_orgs = pads_organization_get_user_orgs($user);
  if (empty($user_orgs)) {
    return FALSE;
  }

  // Convert to an array if needed.
  $level = $conf['levels'];
  if (!is_array($level)) {
    $level = array($level);
  }
  $level = array_filter($level);

  // Verify level.
  foreach ($user_orgs as $user_org) {
    $user_org_level = array_key_exists('organizationType', $user_org) ?
      $user_org['organizationType'] : 0;
    if (!in_array($user_org_level, $level)) {
      return FALSE;
    }
  }
  return TRUE;
}

/**
 * Summary for pads_user_org_level access plugin.
 *
 * @param null|array $settings
 *   Settings array, if exists.
 * @param ctools_context $context
 *   Ctools context object.
 *
 * @return string
 *   The summary.
 */
function pads_user_org_level_summary($settings, $context) {
  $levels = pads_org_levels();

  $level = $settings['levels'];
  if (!is_array($level)) {
    $level = array($level);
  }
  $level = array_filter($level);
  $level = array_combine($level, $level);
  foreach ($level as $key => $val) {
    $level[$key] = $levels[$val];
  }
  $level = implode(', ', $level);

  return t('@identifier is assigned Orgs at the @level level.', array(
    '@identifier' => $context->get_identifier(),
    '@level' => $level,
  ));
}
