<?php
/**
 * @file
 * Ctools access plugin if VBO form is present.
 */

/**
 * VBO Access Plugin.
 *
 * Plugins are described by creating a $plugin array which will
 * be used by the system that includes the file.
 */
$plugin = array(
  'title' => t('VBO: VBO Access'),
  'description' => t('Controls access by checking for VBO confirm form'),
  'callback' => 'pads_vbo_access_ctools_access_check',
  'summary' => 'pads_vbo_access_ctools_summary',
);

/**
 * Custom callback defined by 'callback' in the $plugin array.
 *
 * Check for access.
 */
function pads_vbo_access_ctools_access_check($conf, $context) {
  // Check to see if our vars are set.
  if (isset($_POST['views_bulk_operations'])) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Provide a summary description.
 */
function pads_vbo_access_ctools_summary($conf, $context) {
  return t('Will not show on VBO confirm form');
}
