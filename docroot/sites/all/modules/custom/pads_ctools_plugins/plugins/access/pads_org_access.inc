<?php
/**
 * @file
 * Plugin to provide access control/visibility of pads_org argument (in URL).
 */

$plugin = array(
  'title' => t("Pads Org access"),
  'description' => t('Verify access to pads_org argument.'),
  'callback' => 'pads_org_ctools_access',
  'required context' => new ctools_context_required(t('Pads Organization'), 'pads_org'),
  'settings form' => 'pads_org_ctools_access_settings',
  'summary' => 'pads_org_access_summary',
);

/**
 * Check for access.
 */
function pads_org_ctools_access($conf, $context) {
  // As far as I know there should always be a context at this point, but this
  // is safe.
  if (empty($context) || empty($context->data)) {
    return FALSE;
  }

  if (is_array($context->data) && array_key_exists('organizationId', $context->data)) {
    // Verify level.
    if (
      $conf && array_key_exists('levels', $conf) && !empty($conf['levels']) &&
      !in_array($context->data['organizationType'], $conf['levels'])
    ) {
      if (!empty($conf['message'])) {
        drupal_set_message($conf['message'], 'warning');
      }
      return FALSE;
    }
    return pads_org_access($context->data);
  }
  return FALSE;
}

function pads_org_ctools_access_settings(&$form, &$form_state, $conf) {
  $levels = pads_org_levels(TRUE);
  $form['settings']['#type'] ='container';
  $form['settings']['#attributes']['style'] = "clear: both;";
  $form['settings']['levels'] = array(
    '#type' => 'select',
    '#title' => t('Level access.'),
    '#description' => t('Leave blank to allow access at any level. Otherwise select levels to grant access.'),
    '#options' => $levels,
    '#multiple' => TRUE,
    '#default_value' => ($conf && !empty($conf['levels'])) ? $conf['levels'] : array(),
  );
  $form['settings']['message'] = array(
    '#type' => 'textfield',
    '#title' => t('Message to display if level access check fails'),
    '#default_value' => ($conf && !empty($conf['message'])) ? $conf['message'] : '',
  );
  return $form;
}

/**
 * Smart description of Org access summary.
 *
 * @param array $access
 *   Array of config options.
 * @param ctools_context $contexts
 *   The pads_org ctools_context object.
 *
 * @return string
 *   Description of access summary.
 */
function pads_org_access_summary($access, ctools_context $contexts) {
  $summaries = array();
  $summary = 'User has access to view Organization';
  if (array_key_exists('children', $access) && $access['children']) {
    $summary .= ' or one of its immediate children';
  }
  $summaries[] = t('!summary', array('!summary' => $summary));

  if (array_key_exists('levels', $access) && !empty($access['levels'])) {
    $summary = ' AND Org level is in (';
    $levels = pads_org_levels(TRUE);
    foreach ($access['levels'] as $level) {
      $summary .= $levels[$level] . ', ';
    }
    $summary = substr($summary, 0, -2) . ')';
    $summaries[] = t('!summary', array('!summary' => $summary));
  }

  return implode(' ', $summaries);
}
