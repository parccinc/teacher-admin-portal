<?php
/**
 * @file
 * Access plugin for pads_student ctools_context.
 */

$plugin = array(
  'title' => t("Pads Student access"),
  'description' => t('Verify access to pads_student argument.'),
  'callback' => 'pads_student_ctools_access',
  'required context' => new ctools_context_required(t('Pads Student'), 'pads_student'),
  'settings form' => 'pads_student_ctools_access_settings',
  'summary' => 'pads_student_access_summary',
);

/**
 * Check for access.
 */
function pads_student_ctools_access($conf, $context) {
  // As far as I know there should always be a context at this point, but this
  // is safe.
  if (empty($context) || empty($context->data)) {
    return FALSE;
  }

  if (is_array($context->data) && array_key_exists('studentRecordId', $context->data)) {
    return pads_student_access($context->data);
  }
  return FALSE;
}

/**
 * Smart description of Student access summary.
 *
 * @param array $access
 *   Array of config options
 * @param ctools_context $contexts
 *   The pads_student ctools_context object.
 *
 * @return string
 *   Description of access summary.
 */
function pads_student_access_summary($access, $contexts) {
  $summaries = array();
  $summary = 'User has access to view Student';
  $summaries[] = $summary;
  return implode(' ', $summaries);
}
