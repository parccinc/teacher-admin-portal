<?php
/**
 * @file
 * Plugin to provide access control/visibility based on vendor.
 */

$plugin = array(
  'title' => t('Vendor access'),
  'description' => t('Control access by vendor.'),
  'callback' => 'vendor_access_callback',
  'settings form' => 'vendor_access_edit_form',
  'summary' => 'vendor_access_summary',
);

/**
 * The conf_path() value for each vendor.
 *
 * @return array
 *   Array keys are config path, values are vendor names.
 */
function vendor_access_get_conf_paths() {
  return array(
    'sites/default' => 'PARCC',
    'sites/vendor/isbe' => 'ISBE',
  );
}

/**
 * Settings form for the vendor access.
 */
function vendor_access_edit_form(&$form, &$form_state, $conf) {
  $form['settings']['vendor_path'] = array(
    '#type' => 'select',
    '#title' => t('Vendor.'),
    '#description' => t('Select vendor'),
    '#required' => TRUE,
    '#options' => vendor_access_get_conf_paths(),
    '#default_value' => ($conf && !empty($conf['vendor_path'])) ? $conf['vendor_path'] : NULL,
  );
  return $form;
}

/**
 * Check for access.
 */
function vendor_access_callback($conf, $context) {
  $vendor_path = $conf['vendor_path'];
  return conf_path() == $vendor_path;
}

/**
 * Provide a summary description based upon the checked roles.
 */
function vendor_access_summary($conf, $context) {
  $conf_paths = vendor_access_get_conf_paths();
  return t('Vendor is !vendor', array(
    '!vendor' => $conf_paths[$conf['vendor_path']],
  ));
}
