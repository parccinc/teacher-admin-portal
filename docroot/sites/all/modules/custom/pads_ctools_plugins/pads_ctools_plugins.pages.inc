<?php
/**
 * @file
 * Handles modal node form operations.
 */

/**
 * Callback to present the node add form in ctools modal.
 *
 * @param string $type
 *   Node type being created.
 *
 * @param null $js
 *   If Javascript is not support, $js is NULL.
 */
function modal_node_add($type, $js = NULL) {

  // Fall back if $js is not set.
  if (!$js) {
    return node_add($type);
  }

  ctools_include('modal');
  ctools_include('ajax');

  global $user;

  $types = node_type_get_types();
  $node = (object) array(
    'uid' => $user->uid,
    'name' => (isset($user->name) ? $user->name : ''),
    'type' => $type,
    'language' => LANGUAGE_NONE,
  );

  $title = t('Create @name', array('@name' => $type == "secure_file" ? 'Batch' : $types[$type]->name));

  $form_state = array(
    'title' => $title,
    'ajax' => TRUE,
    'build_info' => array(
      'args' => array($node),
    ),
  );

  ctools_form_include_file($form_state, drupal_get_path('module', 'node') . '/node.pages.inc');
  $output = ctools_modal_form_wrapper($type . '_node_form', $form_state);

  if (!empty($form_state['executed'])) {
    $output = array();
    // If node submit, reload page.
    $triggering_element_id = array_key_exists('#id', $form_state['triggering_element']) ?
      $form_state['triggering_element']['#id'] : '';
    if (strpos($triggering_element_id, 'edit-submit') === 0) {
      $output[] = ctools_ajax_command_reload();
    }
    else {
      $output = array();
    }
  }
  print ajax_render($output);
  if (!empty($output)) {
    drupal_exit();
  }
}

/**
 * Page callback to service ISBE Management page.
 *
 * @param $ajax
 *   Indicates an AJAX interaction.
 *
 * @return array
 *   Either a renderable array of page content for non-AJAX interactions, or
 *   CTools modal commands.
 */
function pads_ctools_plugins_isbe_mgmt_page($ajax) {

  $src = pads_ctools_plugins_isbe_mgmt_src();
  // @TODO: implement rendering via theme().
  $output['iframe']['#markup'] = '<iframe src="' . $src . '" style="min-height: 500px; width: 100%" height="1030"></iframe>';

  if ($ajax) {
    ctools_include('modal');
    ctools_include('ajax');
    $output = render($output);
    ctools_modal_render('ISBE Management', $output);
    drupal_exit();
  }

  return $output;
}
