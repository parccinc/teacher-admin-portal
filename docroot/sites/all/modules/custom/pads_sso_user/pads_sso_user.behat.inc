<?php
/**
 * @file
 * Step definitions relating to LDAP users.
 */

use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Drupal\DrupalExtension\Context\DrupalSubContextBase;
use Drupal\DrupalExtension\Context\DrupalSubContextInterface;

/**
 * Class PadsLdapContext.
 */
class PadsLdapContext extends DrupalSubContextBase implements DrupalSubContextInterface {

  protected $contexts = array();

  /**
   * Store behat contexts.
   *
   * @param \Behat\Behat\Hook\Scope\BeforeScenarioScope $scope
   *   Behat scenario scope.
   *
   * @BeforeScenario
   */
  public function setContexts(BeforeScenarioScope $scope) {
    $this->contexts['mink'] = $scope->getEnvironment()
      ->getContext('PadsMinkContext');
  }

  /**
   * Assert LDAP value for user.
   *
   * @param string $mail
   *   User email address in LDAP.
   * @param string $attribute
   *   LDAP attribute to lookup.
   * @param string $value
   *   LDAP value to verify.
   *
   * @throws \Exception
   *
   * @Then the LDAP user :mail should have a :attribute value of :value
   */
  public function assertLdapUserShouldHasValue($mail, $attribute, $value) {
    $mail = $this->contexts['mink']->fixStepArgument($mail);
    $mail = str_replace('+', ldap_escape('+'), $mail);
    $dn = "uid=$mail,ou=People,dc=uat,dc=parcconline,dc=org";
    $vals = _pads_sso_user_get_ldap_property($dn, $attribute);
    if (!in_array($value, $vals)) {
      throw new \Exception(sprintf('User "%s" did not have a "%s" value of "%s". Instead, it was "%s".',
        $mail, $attribute, $value, implode(',', $vals)));
    }
  }

}
