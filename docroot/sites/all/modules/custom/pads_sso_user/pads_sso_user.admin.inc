<?php
/**
 * @file
 * Administrative functionality for dmrs_sso_user.
 */

/**
 * Admin config form.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 *
 * @return array
 *   Form render array.
 */
function pads_sso_user_settings($form, &$form_state) {
  $servers = ldap_servers_get_servers();
  $server_options = array();
  foreach($servers as $server_name => $server) {
    $server_options[$server_name] = $server->name;
  }
  $form['pads_sso_user_selected_ldap_server'] = array(
    '#type' => 'select',
    '#options' => $server_options,
    '#title' => t('Active LDAP Server'),
    '#description' => t('LDAP Server being used'),
//    '#default_value' => variable_get('pads_sso_user_selected_ldap_server', ''),
  );
  return system_settings_form($form);
}
