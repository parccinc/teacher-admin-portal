<?php
/**
 * @file
 * Admin related functions.
 */

/**
 * Page callback to run import.
 *
 * @param string $machine_name
 *   The machine name of the migration to import.
 */
function pads_migrate_org_run_import($machine_name) {
  module_load_include('inc', 'migrate_ui', 'migrate_ui.pages');
  $form_state = array(
    'values' => array(
      'migrations' => array(
        $machine_name => $machine_name,
      ),
      'operation' => 'import_immediate',
      'update' => 1,
      'force' => 0,
      'limit' => array(
        'value' => '',
        'unit' => 'items',
      ),
      'submit' => 'Execute',
    ),
    'build_info' => array(
      'args' => array('pads_org'),
    ),
  );
  drupal_form_submit('migrate_ui_migrate_group', $form_state);
  migrate_ui_migrate_submit(NULL, $form_state);
  batch_process('admin/reports/status');
}
