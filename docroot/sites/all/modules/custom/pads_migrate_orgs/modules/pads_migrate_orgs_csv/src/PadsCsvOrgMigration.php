<?php

/**
 * @file
 * Contains \Drupal\pads_migrate_orgs\PadsCsvOrgMigration.
 */

namespace Drupal\pads_migrate_orgs\CsvOrg;

use Drupal\pads_migrate_orgs\PadsTermOrgMigration;
use MigrateException;
use MigrateSourceCSV;
use MigrationBase;

class PadsCsvOrgMigration extends PadsTermOrgMigration {

  /**
   * {@inheritdoc}
   */
  public function __construct($arguments = array()) {
    parent::__construct($arguments);

    $this->description = t('Migrate organizations from Csv to taxonomy terms');

    $uri = variable_get('pads_migrate_orgs_csv_uri', 'private://pads_migrate_orgs_csv/data.csv');
    $path = drupal_realpath($uri);
    if (array_key_exists('path', $arguments)) {
      $path = $arguments['path'];
    }

    if (!$path) {
      throw new MigrateException(t('The uri %uri does not exist.', array(
        '%uri' => $uri,
      )), MigrationBase::MESSAGE_ERROR);
    }
    $csvcolumns = parent::getColumns();
    $options = array(
      'header_rows' => TRUE,
    );
    $fields = array(
      'organizationId' => array(t('Organization id')),
      'parentOrganizationId' => array(t('Parent organization id')),
      'organizationName' => array(t('Organization name')),
    );

    $this->source = new MigrateSourceCSV($path, $csvcolumns, $options, $fields);
  }
}
