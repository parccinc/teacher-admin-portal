<?php
/**
 * @file
 * Contains \Drupal\pads_migrate_orgs\DbOrg\PadsDbOrgMigration.
 */

namespace Drupal\pads_migrate_orgs\DbOrg;

use Database;
use DatabaseConnection;
use Drupal\pads_migrate_orgs\PadsTermOrgMigration;
use MigrateSourceSQL;

class PadsDbOrgMigration extends PadsTermOrgMigration {
  protected $ldrDatabase;

  /**
   * {@inheritdoc}
   */
  public function __construct($arguments = array()) {
    $info = array();
    if (array_key_exists('info', $arguments)) {
      $info = $arguments['info'];
      unset($arguments['info']);
    }

    if (!array_key_exists('machine_name', $arguments)) {
      $arguments['machine_name'] = 'DbOrg';
    }
    parent::__construct($arguments);

    $this->ldrDatabase = new LdrDatabase($info);

    $fields = parent::getFields();
    $query = $this->ldrDatabase->getLdrConnection()
      ->select('organization', 'o')->fields('o', array_keys($fields));

    $count_query = $query->countQuery();

    $this->source = new MigrateSourceSQL($query, $fields, $count_query, array(
      'batch_size' => 1000,
    ));
  }
}

class LdrDatabase extends Database {
  protected $key;
  protected $target;
  protected $info;

  /**
   * LdrDatabase constructor.
   *
   * @param array $info
   *   The database connection information, as it would be defined in
   *   settings.php. Note that the structure of this array will depend on the
   *   database driver it is connecting to.
   */
  public function __construct($info = array()) {
    $this->key = variable_get('pads_migrate_orgs_ldr_db_key', 'default');
    $this->target = variable_get('pads_migrate_orgs_ldr_db_key', 'ldr');
    $defaults = array(
      'driver' => variable_get('pads_migrate_orgs_ldr_db_driver', 'mysql'),
      'database' => variable_get('pads_migrate_orgs_ldr_db_name', ''),
      'username' => variable_get('pads_migrate_orgs_ldr_db_username', ''),
      'password' => variable_get('pads_migrate_orgs_ldr_db_password', ''),
      'host' => variable_get('pads_migrate_orgs_ldr_db_host', 'localhost'),
      'port' => variable_get('pads_migrate_orgs_ldr_db_port', '3306'),
    );

    $this->info = $info + $defaults;
    parent::addConnectionInfo($this->key, $this->target, $this->info);
  }

  /**
   * Get the Ldr database connection.
   *
   * @return DatabaseConnection
   *   The corresponding connection object.
   */
  public function getLdrConnection() {
    return Database::getConnection($this->target, $this->key);
  }
}
