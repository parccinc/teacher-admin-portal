<?php
/**
 * @file
 * Install, update, and uninstall functions for the Foo Bar module.
 * Contains \Fully\Qualified\Namespace\And\NameOfTheClass.
 */

/**
 * Implements hook_migrate_api().
 */
function pads_migrate_orgs_db_migrate_api() {
  $api = array(
    'api' => 2,
    'migrations' => array(
      'DbOrg' => array(
        'class_name' => '\Drupal\pads_migrate_orgs\DbOrg\PadsDbOrgMigration',
        'group_name' => 'pads_org',
      ),
    ),

  );
  return $api;
}
