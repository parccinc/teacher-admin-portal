<?php
/**
 * @file
 * Install, update, and uninstall functions for the Foo Bar module.
 * Contains \Fully\Qualified\Namespace\And\NameOfTheClass.
 */

/**
 * Implements hook_migrate_api().
 */
function pads_migrate_orgs_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'pads_org' => array(
        'title' => t('Migrate Organizations'),
      ),
    ),
    'migrations' => array(
      'LdrOrg' => array(
        'class_name' => '\Drupal\pads_migrate_orgs\PadsLdrOrgMigration',
        'group_name' => 'pads_org',
      ),
    ),
  );
  return $api;
}
