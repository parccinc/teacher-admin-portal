<?php
/**
 * @file
 * Contains \Drupal\pads_migrate_orgs\PadsTermOrgMigration.
 */

namespace Drupal\pads_migrate_orgs;

use MigrateDestinationTerm;
use MigrateSQLMap;
use Migration;

abstract class PadsTermOrgMigration extends Migration {
  protected $columns;
  protected $disableMailSystem;

  /**
   * {@inheritdoc}
   */
  public function __construct($arguments = array()) {
    $this->disableMailSystem
      = array_key_exists('disableMailSystem', $arguments) ?
      $arguments['disableMailSystem'] : FALSE;
    parent::__construct($arguments);

    $bundle = array_key_exists('bundle', $arguments) ?
      $arguments['bundle'] : 'organization';

    self::$displayFunction = 'pads_migrate_set_message';

    $this->columns = array(
      0 => array('organizationId', t('Organization id')),
      1 => array('organizationType', t('Organization type (level)')),
      2 => array('stateOrganizationId', t('State organization id')),
      3 => array('parentOrganizationId', t('Parent organization id')),
      4 => array('organizationIdentifier', t('Organization identifier (code)')),
      5 => array('organizationName', t('Organization name')),
      8 => array('studentCount', t('Student count')),
      9 => array('childCount', t('Child count')),
    );

    $options = array(
      'allow_duplicate_terms' => TRUE,
    );
    $this->destination = new MigrateDestinationTerm($bundle, $options);

    $this->map = pads_migrate_orgs_get_migrate_map();

    $this->addFieldMapping('name', 'organizationName');
    $this->addFieldMapping('description', 'info');
    $this->addFieldMapping('parent', 'parentOrganizationId')
      ->sourceMigration($this->machineName)
      ->callbacks(array($this, 'mapParent'));
  }

  /**
   * Get columns.
   *
   * @return array
   *   Keys are integers. values are array(field name, description).
   */
  public function getColumns() {
    return $this->columns;
  }

  /**
   * Get fields.
   *
   * @return array
   *   Keys are field names, values are descriptions. Use to override the
   *   default descriptions, or to add additional source fields which the
   *   migration will add via other means (e.g., prepareRow()).
   */
  public function getFields() {
    $fields = array();
    foreach ($this->columns as $index => $column) {
      $fields[$column[0]] = $column[1];
    }
    return $fields;
  }

  /**
   * Maps the source parentOrganizationId with the created taxonomy term tid.
   *
   * @param int $value
   *   The parentOrganizationId value
   *
   * @return int
   *   The tid representing the parent Organization.
   */
  public function mapParent($value) {
    $parent_row = $this->getMap()->getRowByDestination(array($value));
    return $parent_row['destid1'];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow($row) {
    parent::prepareRow($row);
  }

  /**
   * Returns the MigrateSQLMap.
   *
   * @return MigrateSQLMap
   *   The Map object.
   */
  public function getMap() {
    return $this->map;
  }

  /**
   * {@inheritdoc}
   */
  public function disableMailSystem() {
    if ($this->disableMailSystem) {
      parent::disableMailSystem();
    }
  }
}
