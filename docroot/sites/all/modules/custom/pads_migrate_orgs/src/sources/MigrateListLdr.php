<?php
/**
 * @file
 * Install, update, and uninstall functions for the Foo Bar module.
 * Contains \Fully\Qualified\Namespace\And\NameOfTheClass.
 */

namespace Drupal\pads_migrate_orgs\sources;

use MigrateList;

class MigrateListLdr extends MigrateList {

  protected $idList;
  protected $count;

  /**
   * MigrateListLdr constructor.
   *
   * @param array $id_list
   *   Array of Organization Ids.
   * @param int $count
   *   The total number of Organizations.
   */
  public function __construct($id_list, $count) {
    parent::__construct();
    $this->idList = $id_list;
    $this->count = $count;
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    return variable_get('ldr_uri');
  }

  /**
   * {@inheritdoc}
   */
  public function getIdList() {
    return $this->idList;
  }

  /**
   * {@inheritdoc}
   */
  public function computeCount() {
    return $this->count;
  }
}
