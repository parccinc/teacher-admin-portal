<?php
/**
 * @file
 * Install, update, and uninstall functions for the Foo Bar module.
 * Contains \Fully\Qualified\Namespace\And\NameOfTheClass.
 */

namespace Drupal\pads_migrate_orgs\sources;

use MigrateItem;

class MigrateItemLdr extends MigrateItem {

  protected $items;

  /**
   * MigrateItemLdr constructor.
   *
   * @param array $items
   *   List of Organizations, keyed by Organization id.
   */
  public function __construct($items) {
    parent::__construct();
    $this->items = $items;
  }

  /**
   * Implementors are expected to return an object representing a source item.
   *
   * @param int $id
   *   The Organization id to retrieve from the list.
   *
   * @return object
   *   A stdClass object representing Organization.
   */
  public function getItem($id) {
    $item = $this->items[$id];
    $item = (object) $item;
    return $item;
  }
}
