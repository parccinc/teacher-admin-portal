<?php

/**
 * @file
 * Contains \Drupal\pads_migrate_orgs\PadsLdrOrgMigration.
 */

namespace Drupal\pads_migrate_orgs;

use Drupal\pads_migrate_orgs\sources\MigrateItemLdr;
use Drupal\pads_migrate_orgs\sources\MigrateListLdr;
use MigrateSourceList;

class PadsLdrOrgMigration extends PadsTermOrgMigration {

  /**
   * {@inheritdoc}
   */
  public function __construct($arguments = array()) {
    parent::__construct($arguments);

    $this->description = t('Migrate organizations from Ldr to taxonomy terms');

    $data = array();
    $response = pads_ldr_client_call('getAllOrganizations');
    $organizations = array_key_exists('organizations', $response) ?
      $response['organizations'] : array();
    foreach ($organizations as $organization) {
      $data[$organization['organizationId']] = $organization;
    }
    $count = array_key_exists('totalCount', $response) ?
      $response['totalCount'] : count($data);
    $list = new MigrateListLdr(array_keys($data), $count);
    $item = new MigrateItemLdr($data);

    $fields = parent::getFields();
    $options = array(
      // 'cache_counts' => TRUE,
    );
    $this->source = new MigrateSourceList($list, $item, $fields, $options);

  }
}
