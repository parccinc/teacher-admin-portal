<?php

/**
 * @file
 * Factory class for WdNodeWrapper.
 */
class PadsWrapperFactory {

  /**
   * Creates or retrieves a type of WdEntityWrapper.
   *
   * @param string $entity_type
   *   The entity type.
   * @param mixed $entity
   *   The entity or entity_id to retrieve, or array of values to create.
   * @param string $bundle
   *   The bundle.
   *
   * @return PadsTapOrganizationProfile2Wrapper|PadsBatchuserOrganizationProfile2Wrapper|PadsDmrsProfile2Wrapper|SecureFileNodeWrapper|WdEntityWrapper|WdNodeWrapper
   *   An instance of WdEntityWrapper
   */
  public static function getInstance($entity_type, $entity, $bundle) {
    composer_manager_register_autoloader();
    switch ($entity_type) {
      case 'node':
        switch ($bundle) {
          case 'secure_file':
            $wrapper = is_array($entity) ?
              SecureFileNodeWrapper::create($entity) :
              $wrapper = new SecureFileNodeWrapper($entity);
            break;

          default:
            $wrapper = new WdNodeWrapper($entity);
        }
        break;

      case 'profile2':
        switch ($bundle) {
          case 'pads_batchuser_organization':
            $wrapper = is_array($entity) ?
              PadsBatchuserOrganizationProfile2Wrapper::create($entity) :
              new PadsBatchuserOrganizationProfile2Wrapper($entity_type, $entity);
            break;

          case 'pads_tap_organization':
            $wrapper = is_array($entity) ?
              PadsTapOrganizationProfile2Wrapper::create($entity) :
              new PadsTapOrganizationProfile2Wrapper($entity_type, $entity);
            break;

          case 'dmrs_profile':
            $wrapper = is_array($entity) ?
              PadsDmrsProfile2Wrapper::create($entity) :
              new PadsDmrsProfile2Wrapper($entity_type, $entity);
            break;

          default:
            $wrapper = new WdEntityWrapper($entity_type, $entity);
        }
    }

    return $wrapper;
  }
}
