<?php
/**
 * @file
 * class PadsBatchuserOrganizationProfile2Wrapper
 */

class PadsDmrsProfile2Wrapper extends WdEntityWrapper {

  public function __construct($entity_type, $entity) {
    parent::__construct($entity_type, $entity);
  }

  /**
   * Create a new pads_organization profile2.
   *
   * @param array $values
   * @param string $language
   * @return PadsBatchuserOrganizationProfile2Wrapper
   */
  public static function create($values = array(), $language = LANGUAGE_NONE) {
    // $values += array('bundle' => self::$bundle);
    $entity = profile2_create($values);
    return new PadsDmrsProfile2Wrapper('profile2', $entity);
  }

  /**
   * Retrieves field_state_code
   *
   * @return mixed
   */
  public function getStateCode() {
    return $this->get('field_state_code');
  }

  /**
   * Sets field_state_code
   *
   * @param $value
   *
   * @return $this
   */
  public function setStateCode($value) {
    $this->set('field_state_code', $value);
    return $this;
  }

  /**
   * Retrieves field_staffid
   *
   * @return mixed
   */
  public function getStaffID() {
    return $this->get('field_staffid');
  }

  /**
   * Sets field_memberof
   *
   * @param $value
   *
   * @return $this
   */
  public function setMemberOf($value) {
    $this->set('field_memberof', $value);
    return $this;
  }

  /**
   * Retrieves field_memberof
   *
   * @return mixed
   */
  public function getMemberOf() {
    return $this->get('field_memberof');
  }

  /**
   * Sets field_staffid
   *
   * @param $value
   *
   * @return $this
   */
  public function setStaffID($value) {
    $this->set('field_staffid', $value);
    return $this;
  }

}
