<?php
/**
 * @file
 * class PadsTapOrganizationProfile2Wrapper
 */

use Drupal\pads_organization\OrganizationFactory;

class PadsTapOrganizationProfile2Wrapper extends WdEntityWrapper {

  private static $bundle = 'pads_tap_organization';

  /**
   * Create a new pads_tap_organization profile2.
   *
   * @param array $values
   * @param string $language
   *
   * @return PadsTapOrganizationProfile2Wrapper
   */
  public static function create($values = array(), $language = LANGUAGE_NONE) {
    $entity = profile2_create($values);
    return new PadsTapOrganizationProfile2Wrapper('profile2', $entity);
  }

  /**
   * Retrieves field_tap_organization.
   *
   * @return mixed
   */
  public function getTapOrganization() {
    return $this->get('field_tap_organization');
  }

  /**
   * Sets field_tap_organization.
   *
   * @param array $value
   *   Array of Organization terms.
   *
   * @return $this
   */
  public function setTapOrganization($value) {
    $this->set('field_tap_organization', $value);
    return $this;
  }

  /**
   * Gets array of Organization objects from field_tap_organization terms.
   *
   * @return array
   *   The Organization id is the key and the Organization array is the value.
   *   Returns empty array if none are found.
   */
  public function getOrgs() {
    $orgs = array();
    $field_tap_organizations = $this->getTapOrganization();
    foreach ($field_tap_organizations as $term) {
      if ($term) {
        $org = OrganizationFactory::getTermLdrOrganizationInstance()
          ->getOrgFromTerm($term);
        $orgs[$org['organizationId']] = $org;
      }
    }
    return $orgs;
  }

}
