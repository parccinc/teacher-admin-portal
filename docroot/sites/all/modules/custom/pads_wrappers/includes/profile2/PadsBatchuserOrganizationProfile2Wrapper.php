<?php
/**
 * @file
 * class PadsBatchuserOrganizationProfile2Wrapper
 */

class PadsBatchuserOrganizationProfile2Wrapper extends WdEntityWrapper {

  public function __construct($entity_type, $entity) {
    parent::__construct($entity_type, $entity);
  }

  /**
   * Create a new pads_organization profile2.
   *
   * @param array $values
   * @param string $language
   * @return PadsBatchuserOrganizationProfile2Wrapper
   */
  public static function create($values = array(), $language = LANGUAGE_NONE) {
    // $values += array('bundle' => self::$bundle);
    $entity = profile2_create($values);
    return new PadsBatchuserOrganizationProfile2Wrapper('profile2', $entity);
  }

  /**
   * Retrieves field_affiliation_type
   *
   * @return mixed
   */
  public function getAffiliationType() {
    return $this->get('field_affiliation_type');
  }

  /**
   * Sets field_affiliation_type
   *
   * @param $value
   *
   * @return $this
   */
  public function setAffiliationType($value) {
    $this->set('field_affiliation_type', $value);
    return $this;
  }

  /**
   * Retrieves field_state
   *
   * @return mixed
   */
  public function getState() {
    return $this->get('field_state');
  }

  /**
   * Sets field_state
   *
   * @param $value
   *
   * @return $this
   */
  public function setState($value) {
    $this->set('field_state', $value);
    return $this;
  }

  /**
   * Retrieves field_vendor
   *
   * @return mixed
   */
  public function getVendor() {
    return $this->get('field_vendor');
  }

  /**
   * Sets field_vendor
   *
   * @param $value
   *
   * @return $this
   */
  public function setVendor($value) {
    $this->set('field_vendor', $value);
    return $this;
  }

}
