<?php
/**
 * @file
 * Defines class SecureFileNodeWrapper.
 */

class SecureFileNodeWrapper extends WdNodeWrapper {

  private static $bundle = 'secure_file';

  public static $scheme = 'private://';
  public static $directory = 'secure-files/archives';

  /**
   * Create a new secure_file node.
   *
   * @param array $values
   *   An array of values to set, keyed by property name.
   *
   * @param string $language
   *   Default entity language.
   *
   * @return SecureFileNodeWrapper
   *   The SecureFileNodeWrapper created.
   */
  public static function create($values = array(), $language = LANGUAGE_NONE) {
    $values += array('bundle' => self::$bundle);
    $entity_wrapper = parent::create($values, $language);
    return new SecureFileNodeWrapper($entity_wrapper->value());
  }

  /**
   * Retrieves field_file.
   *
   * @return mixed
   */
  public function getFile() {
    return $this->get('field_file');
  }

  /**
   * Sets field_file.
   *
   * @param $value
   *
   * @return $this
   */
  public function setFile($value) {
    $this->set('field_file', $value);
    return $this;
  }

  /**
   * Retrieves field_file as a URL.
   *
   * @param bool $absolute
   *   Whether to return an absolute URL or not
   *
   * @return string[]
   */
  public function getFileUrl($absolute = FALSE) {
    $files = $this->get('field_file');
    if (!empty($files)) {
      foreach ($files as $i => $file) {
        $files[$i] = url(file_create_url($file['uri']), array('absolute' => $absolute));
      }
    }
    return $files;
  }


  /**
   * Retrieves field_delete_time.
   *
   * @return mixed
   */
  public function getDeleteTime() {
    return $this->get('field_delete_time');
  }

  /**
   * Sets field_delete_time.
   *
   * @param $value
   *
   * @return $this
   */
  public function setDeleteTime($value) {
    $this->set('field_delete_time', $value);
    return $this;
  }

  /**
   * Retrieves field_batch_description.
   *
   * @return mixed
   */
  public function getBatchDescription($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_batch_description', $format, $markup_format);
  }

  /**
   * Sets field_batch_description.
   *
   * @param $value
   * @param null $format
   * @return $this
   */
  public function setBatchDescription($value, $format = NULL) {
    $this->setText('field_batch_description', $value, $format);
    return $this;
  }

  /**
   * Returns the final save location of the zip file.
   *
   * @return string
   *   The uri of the archived files in the node.
   */
  private function uri() {
    return "{$this::$scheme}{$this::$directory}/{$this->filename()}";
  }

  /**
   * Create the ZIP file on disk.
   *
   * @param bool $overwrite
   *   Optional field that when FALSE will not recreate the zip if it already
   *   exists.
   *
   * @return string
   *   The uri of the zip file. Returns NULL if file exists and overwrite
   *   is FALSE.
   */
  public function generateZip($overwrite = TRUE) {
    // If file exists and overwrite is FALSE, return URI.
    $uri = $this->uri();
    if (!$overwrite && file_exists($uri)) {
      return $uri;
    }

    $lib = libraries_load('pclzip');
    if (!$lib) {
      // Error.
      return;
    }

    $temp_uri = 'temporary://' . $this->filename();
    $dirname = drupal_dirname($temp_uri);
    file_prepare_directory($dirname, FILE_CREATE_DIRECTORY);
    $zip_file = file_unmanaged_save_data('', $temp_uri);

    // Add in the files to be ZIP'd up.
    ini_set('max_execution_time', 240);
    $archive = new PclZip($zip_file);
    $files = file_load_multiple($this->fids());
    $filelist = array();
    foreach ($files as $file) {
      $filename = drupal_realpath($file->uri);
      if (file_exists($filename)) {
        $filelist[] = $filename;
      }
    }
    $archive->add(implode(',', $filelist), PCLZIP_OPT_REMOVE_ALL_PATH);

    // Move into permanent storage.
    $dirname = drupal_dirname($uri);
    file_prepare_directory($dirname, FILE_CREATE_DIRECTORY);
    $zip_file = file_unmanaged_move($zip_file, $uri, FILE_EXISTS_REPLACE);

    return $zip_file;
  }

  /**
   * Generate the filename used for the cached ZIP file.
   */
  public function filename() {
    // Leverage pathauto's function for sanitizing path names.
    module_load_include('inc', 'pathauto', 'pathauto');

    // Use title and nid to ensure uniqueness.
    $sanitized = pathauto_cleanstring($this->getTitle() . '-' . $this->getId());

    $filename = "{$sanitized}.zip";

    return $filename;
  }

  /**
   * The file ids of the files attached to node.
   *
   * @return array
   *   The array of fids.
   */
  protected function fids() {
    $fids = array();
    $files = $this->getFile();
    foreach ($files as $file) {
      $fids[] = $file['fid'];
    }
    return $fids;
  }

  /**
   * Deletes the node, and associated Zip file.
   */
  public function delete() {
    file_unmanaged_delete($this->uri());
    node_delete($this->getNid());
  }
}
