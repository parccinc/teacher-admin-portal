<?php
/**
 * @file
 * Factory class for WdNodeWrapper.
 */

class PadsNodeWrapperFactory {

  /**
   * Returns WdNodeWrapper object.
   *
   * @param stdClass|int $node
   *   The nid or node.
   * @param string $type
   *   The node bundle.
   *
   * @return SecureFileNodeWrapper|WdNodeWrapper
   *   Returns the WdNodeWrapper instance.
   */
  public static function create($node, $type = 'secure_file') {
    switch ($type) {
      case 'secure_file':
        $wrapper = new SecureFileNodeWrapper($node);
        break;

      default:
        $wrapper = new WdNodeWrapper($node);
    }

    return $wrapper;
  }
}
