<?php
/**
 * @file
 * Administrative functionality for dmrs_sso_user.
 */

/**
 * Admin config form.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 *
 * @return array
 *   Form render array.
 */
function dmrs_sso_user_settings($form, &$form_state) {
  $form['dmrs_sso_user_amplify_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Amplify URL'),
    '#description' => t('URL for Amplify DMRS'),
    '#default_value' => variable_get('dmrs_sso_user_amplify_url', ''),
  );
  return system_settings_form($form);
}
