<?php
/**
 * @file
 * Handling of DMRS single sign on Amplify pages.
 */

/**
 * Page callback to send user to DMRS system.
 */
function dmrs_sso_user_redirect_to_amplify() {
  $amplify_url = variable_get('dmrs_sso_user_amplify_url', '');
  drupal_goto($amplify_url);
}
