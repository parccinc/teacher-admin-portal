<?php

/**
 * @file
 * Handling of DMRS single sign on.
 */

/**
 * Implements hook_menu().
 */
function dmrs_sso_user_menu() {
  $items = array();
  $items['dmrsusers/amplify'] = array(
    'title callback' => 'dmrs_link_title',
    'page callback' => 'dmrs_sso_user_redirect_to_amplify',
    'page arguments' => array(),
    'access arguments' => array('access dmrs'),
    'file' => 'dmrs_sso_user.pages.inc',
    'menu_name' => 'main-menu',
    'weight' => 2,
  );
  $items['admin/config/system/dmrs_sso_user'] = array(
    'title' => 'DMRS Amplify Settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('dmrs_sso_user_settings'),
    'access arguments' => array('administer dmrs settings'),
    'file' => 'dmrs_sso_user.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function dmrs_sso_user_permission() {
  return array(
    'import new dmrs users' => array(
      'title' => t('Import new DMRS users'),
      'description' => t('Import new DMRS users'),
    ),
    'administer dmrs users' => array(
      'title' => t('Administer DMRS Users'),
      'description' => t('Administer DMRS Users'),
    ),
    'administer dmrs' => array(
      'title' => t('Administer DMRS'),
      'description' => t('Change Amplify URL'),
    ),
    'access dmrs' => array(
      'title' => t('Access DMRS'),
      'description' => t('Access DMRS'),
    ),
  );
}

/**
 * Title callback to determine name of DMRS menu link.
 */
function dmrs_link_title() {
  $title = variable_get('dmrs_sso_user_dmrs_link_title', t('DMRS'));
  return $title;
}

function _dmrs_sso_user_member_of($role, $state, $district, $institution) {
  $pipes = array();
  $pipes = array_pad($pipes, 17, '');

  $pipes[1] = $role;
  $pipes[7] = $state;
  $pipes[11] = $district;
  $pipes[15] = $institution;

  $member_of = '|' . implode($pipes, '|') . '|';
  return $member_of;
}

/**
 * @param object $user
 *   Drupal user object.
 * @param string $staff_id
 *   DMRS staffid value.
 * @param string $state_code
 *   Two digit state code.
 * @param string $district
 *   DMRS district value.
 * @param string $institution
 *   DMRS institution value.
 * @param string $general
 *   DMRS permission.
 * @param string $pii
 *   DMRS permission.
 * @param string $roster
 *   DMRS permission.
 * @param string $sf_extract
 *   DMRS permission.
 * @param string $rf_extract
 *   DMRS permission.
 * @param string $pf_extract
 *   DMRS permission.
 * @param string $psrd_extract
 *   DMRS permission.
 * @param string $cds_extract
 *   DMRS permission.
 * @param string $pii_analytics
 *   DMRS permission.
 *
 * @return array|bool|mixed
 *   User profile.
 */
function _dmrs_sso_user_create_dmrs_profile($user, $staff_id, $state_code,
                                            $district, $institution, $general,
                                            $pii, $roster, $sf_extract,
                                            $rf_extract, $pf_extract,
                                            $psrd_extract, $cds_extract,
                                            $pii_analytics) {

  // Retrieve users profile, if exists.
  $type = 'dmrs_profile';
  $profile = profile2_load_by_user($user, $type);
  if (!$profile) {
    $profile = array(
      'type' => $type,
      'user' => $user->uid,
    );
  }

  $member_of = array();

  if ($general) {
    $member_of[] = _dmrs_sso_user_member_of('GENERAL', $state_code, $district, $institution);
  }
  if ($pii) {
    $member_of[] = _dmrs_sso_user_member_of('PII', $state_code, $district, $institution);
  }
  if ($roster) {
    $member_of[] = _dmrs_sso_user_member_of('ROSTER', $state_code, $district, $institution);
  }
  if ($sf_extract) {
    $member_of[] = _dmrs_sso_user_member_of('SF_EXTRACT', $state_code, $district, $institution);
  }
  if ($rf_extract) {
    $member_of[] = _dmrs_sso_user_member_of('RF_EXTRACT', $state_code, $district, $institution);
  }
  if ($pf_extract) {
    $member_of[] = _dmrs_sso_user_member_of('PF_EXTRACT', $state_code, $district, $institution);
  }
  if ($psrd_extract) {
    $member_of[] = _dmrs_sso_user_member_of('PSRD_EXTRACT', $state_code, $district, $institution);
  }
  if ($cds_extract) {
    $member_of[] = _dmrs_sso_user_member_of('CDS_EXTRACT', $state_code, $district, $institution);
  }
  if ($pii_analytics) {
    $member_of[] = _dmrs_sso_user_member_of('PII_ANALYTICS', $state_code, $district, $institution);
  }

  $pw = PadsWrapperFactory::getInstance('profile2', $profile, $type);
  $pw->setStaffID($staff_id);
  $pw->setStateCode($state_code);
  $pw->setMemberOf($member_of);
  $pw->save();

  return $profile;
}
