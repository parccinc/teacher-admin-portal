<?php
/**
 * @file
 * Contains \Drupal\pads_ldr_client\LdrClient.
 */

namespace Drupal\pads_ldr_client;

use Drupal\pads_client\PadsClient;
use GuzzleHttp\Collection;
use GuzzleHttp\Event\BeforeEvent;

composer_manager_register_autoloader();

/**
 * Class LdrClient.
 *
 * @package Drupal\pads_ldr_client
 */
class LdrClient extends PadsClient {

  const TOKEN_PREFIX = 'ldr_token_';

  /**
   * {@inheritdoc}
   */
  public function __construct(array $config) {
    parent::__construct($config);
  }

  /**
   * {@inheritdoc}
   */
  protected function handleHttpClientOptions(Collection $config) {
    parent::handleHttpClientOptions($config);
    $http_client = $config['http_client'];
    // Poor man's authorization until LDR supports real auth methods.
    $http_client->getEmitter()->on('before', function (BeforeEvent $e) {
      $request = $e->getRequest();

      // Request path.
      $path = explode('/', $request->getPath());

      // URI for LDR session-token.
      $uri = explode('/', $this->getDescription()
        ->getOperation('session')
        ->getUri());

      // If request is not for token, then add token to request header.
      if ($uri && (array_pop($path) != array_pop($uri))) {
        $request->addHeader('token', $this::getToken());
      }
    });
    $config['http_client'] = $http_client;

  }

  /**
   * Gets the unique session key.
   *
   * @return string
   *   Returns ldr_token prefix plus the session id.
   */
  private function getSessionKey() {
    return $this::TOKEN_PREFIX . session_id();
  }

  /**
   * Retrieves session token form LDR.
   *
   * @param array $auth
   *   Allows callers to provide explicit authorization. Otherwise depends
   *   on pads default user. Expected keys are clientName, password as per
   *   https://breaktech.centraldesktop.com/p/aQAAAAACFNx-.
   *
   * @return string
   *   The session token, or empty string if not found.
   */
  private function getToken($auth = array()) {
    $token = '';
    $session_key = $this->getSessionKey();
    if (empty($auth) && isset($_SESSION) &&
      array_key_exists($session_key, $_SESSION)
    ) {
      $token = $_SESSION[$session_key];
    }
    else {
      $response = $this->session($auth);
      if (array_key_exists('token', $response)) {
        $token = $response['token'];
        $_SESSION[$session_key] = $token;
      }
    }
    return $token;
  }

  /**
   * {@inheritdoc}
   */
  public function handleError(&$message, $severity = WATCHDOG_NOTICE, $link = NULL) {
    $variables = array();
    if (is_array($message) && array_key_exists('error', $message) && array_key_exists('detail', $message)) {
      $variables = array(
        '%code' => $message['error'],
        '%detail' => $message['detail'],
      );

      $log_message = 'Error %code: %detail';
      if (!array_key_exists('show_detail', $message) || !$message['show_detail']) {
        $message['detail'] = variable_get('pads_ldr_client_generic_error',
          t("An error occurred during the request, sorry it didn't work out. We have the error code !code to help us troubleshoot.", array(
            '!code' => $message['error'],
            '%detail' => $message['detail'],
          )));
      }
    }
    else {
      $log_message = $message;
    }
    watchdog('Client', $log_message, $variables, $severity, $link);
  }

  /**
   * {@inheritdoc}
   */
  public function call($operation, array $args = []) {
    // Look for explicit authorization.
    if (array_key_exists('auth', $args)) {
      $this->getToken($args['auth']);
    }
    try {
      $result = parent::call($operation, $args);
    }
    catch (\Exception $e) {
      return array(
        'error' => t('Error code @code', array('@code' => $e->getCode())),
        'detail' => $e->getMessage(),
      );
    }
    if (array_key_exists('error', $result)) {
      $error_code = intval($result['error']);
      switch ($error_code) {
        // Session token not found (2008), or expired (2009).
        case 2008:
        case 2009:
          // Get new token and try again.
          $session_key = $this->getSessionKey();
          unset($_SESSION[$session_key]);
          return $this->call($operation, $args);

        // Adds client user if they do not exist in Ldr.
        case 2023:
          // If we are here we can assume clientUserId should be part of
          // the operation arguments.
          $detail = array_key_exists('detail', $result) ? $result['detail'] : '';
          if (strpos($detail, 'clientUserId') !== FALSE) {
            $client_user_id = array_key_exists('clientUserId', $args) ?
              $args['clientUserId'] : pads_ldr_client_user_id();
            $account = user_load($client_user_id);
            $user_orgs = pads_organization_get_user_orgs($account);
            $user_org_ids = array_keys($user_orgs);
            $organization_id = !empty($user_orgs) ? reset($user_org_ids) : FALSE;
            if ($client_user_id && $organization_id) {
              $ldr_create_user_response = pads_ldr_client_call('user', [
                'clientUserId' => $client_user_id,
                'organizationId' => $organization_id,
              ]);
              if (!array_key_exists('error', $ldr_create_user_response)) {
                $args['clientUserId'] = $client_user_id;
                return $this->call($operation, $args);
              }
            }
          }
          $this->handleError($result);
          break;

        // These error codes set show_detail to TRUE which will display the
        // LDR response error detail message to the user.
        case 2014:
        case 2016:
        case 2019:
        case 2041:
        case 2056:
          $result['show_detail'] = TRUE;
          $this->handleError($result);
          break;

        default:
          $this->handleError($result);
      }

    }
    return $result;
  }

}
