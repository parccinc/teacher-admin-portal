<?php
/**
 * @file
 * Contains \LdrClientFactory.
 */

namespace Drupal\pads_ldr_client;

class LdrClientFactory {

  /**
   * Returns instance of Ldr Client.
   *
   * @return array|\Drupal\pads_ldr_client\LdrClient
   *   LdrClient instance.
   */
  public static function getInstance() {
    $client = &drupal_static(__CLASS__ . '::' . __FUNCTION__);
    if (!isset($client) || empty($client)) {
      $config = array(
        'description_path' => __DIR__ . '/ldr-api.php',
      );
      $client = new LdrClient($config);

    }
    return $client;
  }
}
