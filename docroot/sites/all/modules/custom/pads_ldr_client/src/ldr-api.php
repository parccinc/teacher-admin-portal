<?php
/**
 * @file
 * Define LDR Service description data.
 */

$base_url = variable_get('ldr_uri');
return [
  'baseUrl' => rtrim($base_url, '/\\') . '/',
  'defaults' => [
    'curl' => [],
  ],
  'operations' => [
    // Ldr session and user operations.
    'user' => [
      'httpMethod' => 'POST',
      'uri' => 'ldr_svcs/user',
      'responseModel' => 'Resource',
      'parameters' => [
        'clientUserId' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'json',
        ],
        'organizationId' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'json',
        ],
      ],
    ],
    'session' => [
      'httpMethod' => 'POST',
      'uri' => 'ldr_svcs/session-token',
      'responseModel' => 'Resource',
      'parameters' => [
        'clientName' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'json',
          'default' => variable_get('ldr_username'),
        ],
        'password' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'json',
          'default' => pads_ldr_client_get_password(),
        ],
      ],
    ],
    // Ldr organization operations.
    'organizations' => [
      'httpMethod' => 'GET',
      'uri' => 'ldr_svcs/organizations/{organizationId}',
      'responseModel' => 'Resource',
      'parameters' => [
        'organizationId' => [
          'required' => TRUE,
          'type' => 'number',
          'location' => 'uri',
          'default' => 1,
        ],
      ],
    ],
    'getAllOrganizations' => [
      'httpMethod' => 'GET',
      'uri' => 'ldr_svcs/organizations-all',
      'responseModel' => 'Resource',
    ],
    'organization' => [
      'httpMethod' => 'GET',
      'uri' => 'ldr_svcs/organization/{organizationId}',
      'responseModel' => 'Resource',
      'parameters' => [
        'organizationId' => [
          'required' => TRUE,
          'type' => 'number',
          'location' => 'uri',
        ],
      ],
    ],
    'organizationId' => [
      'httpMethod' => 'GET',
      'uri' => 'ldr_svcs/organization-id',
      'responseModel' => 'Resource',
      'parameters' => [
        'stateCode' => [
          'required' => FALSE,
          'type' => 'string',
          'location' => 'query',
        ],
        'organizationIdentifier' => [
          'required' => FALSE,
          'type' => 'string',
          'location' => 'query',
        ],
        'parentOrganizationId' => [
          'required' => FALSE,
          'type' => 'number',
          'location' => 'query',
        ],
        'organizationName' => [
          'required' => FALSE,
          'type' => 'string',
          'location' => 'query',
        ],
        'organizationPath' => [
          'required' => FALSE,
          'type' => 'string',
          'location' => 'query',
        ],
      ],
    ],
    'addOrganization' => [
      'httpMethod' => 'POST',
      'uri' => 'ldr_svcs/organization',
      'responseModel' => 'Resource',
      'parameters' => [
        'clientUserId' => [
          'required' => TRUE,
          'location' => 'json',
          'default' => pads_ldr_client_user_id(),
        ],
        'organizationName' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'json',
        ],
        'organizationType' => [
          'required' => TRUE,
          'type' => 'number',
          'location' => 'json',
        ],
        'parentStateCode' => [
          'required' => FALSE,
          'type' => 'string',
          'location' => 'json',
        ],
        'parentOrganizationIdentifier' => [
          'required' => FALSE,
          'type' => 'string',
          'location' => 'json',
        ],
        'parentOrganizationId' => [
          'required' => FALSE,
          'type' => 'number',
          'location' => 'json',
        ],
        'organizationIdentifier' => [
          'type' => 'string',
          'location' => 'json',
        ],
      ],
    ],
    'deleteOrganization' => [
      'httpMethod' => 'DELETE',
      'uri' => 'ldr_svcs/organization/{organizationId}',
      'responseModel' => 'Resource',
      'parameters' => [
        'clientUserId' => [
          'required' => TRUE,
          'location' => 'json',
          'default' => pads_ldr_client_user_id(),
        ],
        'organizationId' => [
          'required' => TRUE,
          'type' => 'number',
          'location' => 'uri',
        ],
      ],
    ],
    // Ldr class operations.
    'getClasses' => [
      'httpMethod' => 'GET',
      'uri' => 'ldr_svcs/classes/{organizationId}',
      'responseModel' => 'Resource',
      'parameters' => [
        'organizationId' => [
          'required' => TRUE,
          'type' => 'number',
          'location' => 'uri',
        ],
      ],
    ],
    'getClass' => [
      'httpMethod' => 'GET',
      'uri' => 'ldr_svcs/class/{classId}',
      'responseModel' => 'Resource',
      'parameters' => [
        'classId' => [
          'required' => TRUE,
          'type' => 'number',
          'location' => 'uri',
        ],
      ],
    ],
    'addClass' => [
      'httpMethod' => 'POST',
      'uri' => 'ldr_svcs/class',
      'responseModel' => 'Resource',
      'parameters' => [
        'clientUserId' => [
          'required' => TRUE,
          'location' => 'json',
          'default' => pads_ldr_client_user_id(),
        ],
        'organizationId' => [
          'required' => FALSE,
          'type' => 'number',
          'location' => 'json',
        ],
        'classIdentifier' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'json',
        ],
        'stateCode' => [
          'required' => FALSE,
          'type' => 'string',
          'location' => 'json',
        ],
        'organizationIdentifier' => [
          'required' => FALSE,
          'type' => 'string',
          'location' => 'json',
        ],
        'gradeLevel' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'json',
        ],
        'sectionNumber' => [
          'type' => 'string',
          'location' => 'json',
        ],
      ],
    ],
    'deleteClass' => [
      'httpMethod' => 'DELETE',
      'uri' => 'ldr_svcs/class/{classId}',
      'responseModel' => 'Resource',
      'parameters' => [
        'clientUserId' => [
          'required' => TRUE,
          'location' => 'json',
          'default' => pads_ldr_client_user_id(),
        ],
        'classId' => [
          'required' => TRUE,
          'type' => 'number',
          'location' => 'uri',
        ],
      ],
    ],
    // Ldr student operations.
    'addStudentRecord' => [
      'httpMethod' => 'POST',
      'uri' => 'ldr_svcs/student-record',
      'responseModel' => 'Resource',
      'parameters' => [
        'clientUserId' => [
          'required' => TRUE,
          'location' => 'json',
          'default' => pads_ldr_client_user_id(),
        ],
        // Bug in ldr?
        'testingOrgId' => [
          'required' => TRUE,
          'location' => 'json',
          'default' => 1,
        ],
      ],
      'additionalParameters' => [
        'location' => 'json',
      ],
    ],
    'addStudentRecords' => [
      'httpMethod' => 'POST',
      'uri' => 'ldr_svcs/student-records',
      'responseModel' => 'Resource',
      'parameters' => [
        'clientUserId' => [
          'required' => TRUE,
          'location' => 'json',
          'default' => pads_ldr_client_user_id(),
        ],
      ],
      'additionalParameters' => [
        'location' => 'json',
      ],
    ],
    'updateStudentRecord' => [
      'httpMethod' => 'PUT',
      'uri' => 'ldr_svcs/student-record/{studentRecordId}',
      'responseModel' => 'Resource',
      'parameters' => [
        'studentRecordId' => [
          'required' => TRUE,
          'type' => 'number',
          'location' => 'uri',
        ],
        'clientUserId' => [
          'required' => TRUE,
          'location' => 'json',
          'default' => pads_ldr_client_user_id(),
        ],
      ],
      'additionalParameters' => [
        'location' => 'json',
      ],
    ],
    'getStudentRecords' => [
      'httpMethod' => 'GET',
      'uri' => 'ldr_svcs/student-records/{organizationId}',
      'responseModel' => 'Resource',
      'parameters' => [
        'organizationId' => [
          'required' => TRUE,
          'type' => 'number',
          'location' => 'uri',
        ],
        'gradeLevel' => [
          'required' => FALSE,
          'location' => 'query',
        ],
        'hasClasses' => [
          'required' => FALSE,
          'location' => 'query',
        ],
      ],
    ],
    'deleteStudentRecord' => [
      'httpMethod' => 'DELETE',
      'uri' => 'ldr_svcs/student-record/{studentRecordId}',
      'responseModel' => 'Resource',
      'parameters' => [
        'studentRecordId' => [
          'required' => TRUE,
          'type' => 'number',
          'location' => 'uri',
        ],
        'clientUserId' => [
          'required' => TRUE,
          'location' => 'json',
          'default' => pads_ldr_client_user_id(),
        ],
      ],
    ],
    'deleteStudentRecords' => [
      'httpMethod' => 'DELETE',
      'uri' => 'ldr_svcs/student-records/{organizationId}',
      'responseModel' => 'Resource',
      'parameters' => [
        'organizationId' => [
          'required' => TRUE,
          'type' => 'number',
          'location' => 'uri',
        ],
        'clientUserId' => [
          'required' => TRUE,
          'location' => 'json',
          'default' => pads_ldr_client_user_id(),
        ],
      ],
    ],
    'deleteClassStudents' => [
      'httpMethod' => 'DELETE',
      'uri' => 'ldr_svcs/class-students/{classId}',
      'responseModel' => 'Resource',
      'parameters' => [
        'classId' => [
          'required' => TRUE,
          'type' => 'number',
          'location' => 'uri',
        ],
        'studentRecordIds' => [
          'required' => TRUE,
          'type' => 'array',
          'location' => 'json',
        ],
        'clientUserId' => [
          'required' => TRUE,
          'location' => 'json',
          'default' => pads_ldr_client_user_id(),
        ],
      ],
    ],
    'deleteStudentClassAssignments' => [
      'httpMethod' => 'DELETE',
      'uri' => 'ldr_svcs/class-students',
      'responseModel' => 'Resource',
      'parameters' => [
        'clientUserId' => [
          'required' => TRUE,
          'location' => 'json',
          'default' => pads_ldr_client_user_id(),
        ],
        'organizationId' => [
          'type' => 'string',
          'location' => 'json',
        ],
        'classId' => [
          'type' => 'string',
          'location' => 'json',
        ],
      ],
    ],
    'getEmbeddedScoreReport' => [
      'httpMethod' => 'GET',
      'uri' => 'ldr_svcs/embedded-score-report/{embeddedScoreReportId}',
      'responseModel' => 'Resource',
      'parameters' => [
        'embeddedScoreReportId' => [
          'required' => TRUE,
          'type' => 'number',
          'location' => 'uri',
        ],
      ],
    ],
    'getStudentRecordsByClass' => [
      'httpMethod' => 'GET',
      'uri' => 'ldr_svcs/class-students/{classId}',
      'responseModel' => 'Resource',
      'parameters' => [
        'classId' => [
          'required' => TRUE,
          'type' => 'number',
          'location' => 'uri',
        ],
      ],
    ],
    'getStudentRecord' => [
      'httpMethod' => 'GET',
      'uri' => 'ldr_svcs/student-record/{studentRecordId}',
      'responseModel' => 'Resource',
      'parameters' => [
        'studentRecordId' => [
          'required' => TRUE,
          'type' => 'number',
          'location' => 'uri',
        ],
        'optionalStateData' => [
          'required' => FALSE,
          'location' => 'query',
        ],
      ],
    ],
    'getStudentClasses' => [
      'httpMethod' => 'GET',
      'uri' => 'ldr_svcs/student-classes/{studentRecordId}',
      'responseModel' => 'Resource',
      'parameters' => [
        'studentRecordId' => [
          'required' => TRUE,
          'type' => 'number',
          'location' => 'uri',
        ],
      ],
    ],
    'createClassStudents' => [
      'httpMethod' => 'POST',
      'uri' => 'ldr_svcs/class-students/{classId}',
      'responseModel' => 'Resource',
      'parameters' => [
        'clientUserId' => [
          'required' => TRUE,
          'location' => 'json',
          'default' => pads_ldr_client_user_id(),
        ],
        'classId' => [
          'required' => TRUE,
          'type' => 'number',
          'location' => 'uri',
        ],
        'studentRecordIds' => [
          'required' => TRUE,
          'type' => 'array',
          'location' => 'json',
        ],
      ],
    ],
    // Test operations.
    'getTestAssignments' => [
      'httpMethod' => 'GET',
      'uri' => 'ldr_svcs/test-assignments',
      'responseModel' => 'Resource',
      'parameters' => [
        'testAssignmentId' => [
          'location' => 'query',
        ],
        'studentRecordId' => [
          'location' => 'query',
        ],
        'organizationId' => [
          'location' => 'query',
        ],
        'classIds' => [
          'location' => 'query',
        ],
        'studentGradeLevel' => [
          'location' => 'query',
        ],
        'testBatterySubject' => [
          'location' => 'query',
        ],
        'testBatteryGrade' => [
          'location' => 'query',
        ],
        'testBatteryId' => [
          'location' => 'query',
        ],
        'testAssignmentStatus' => [
          'location' => 'query',
        ],
        'testKeysOnly' => [
          'location' => 'query',
        ],
      ],
    ],
    'getTestBatterySubjects' => [
      'httpMethod' => 'GET',
      'uri' => 'ldr_svcs/test-battery-subjects',
      'responseModel' => 'Resource',
      'parameters' => [
        'testBatteryGrade' => [
          'required' => TRUE,
          'location' => 'query',
        ],
      ],
    ],
    'getTestBattery' => [
      'httpMethod' => 'GET',
      'uri' => 'ldr_svcs/test-battery/{testBatteryId}',
      'responseModel' => 'Resource',
      'parameters' => [
        'clientUserId' => [
          'required' => TRUE,
          'location' => 'json',
          'default' => pads_ldr_client_user_id(),
        ],
        'testBatteryId' => [
          'required' => TRUE,
          'type' => 'number',
          'location' => 'uri',
        ],
      ],
    ],
    'getTestBatteries' => [
      'httpMethod' => 'GET',
      'uri' => 'ldr_svcs/test-batteries',
      'responseModel' => 'Resource',
      'parameters' => [
        'testBatteryPermissions' => [
          'location' => 'query',
          'required' => TRUE,
          'default' => user_access('view restricted tests') ? 'Restricted' : 'Non-Restricted',
        ],
        'testBatterySubject' => [
          'location' => 'query',
        ],
        'testBatteryGrade' => [
          'location' => 'query',
        ],
        'testBatteryProgram' => [
          'location' => 'query',
        ],
        'testBatterySecurity' => [
          'location' => 'query',
        ],
        'testBatteryScoring' => [
          'location' => 'query',
        ],
      ],
    ],
    'postTestAssignment' => [
      'httpMethod' => 'POST',
      'uri' => 'ldr_svcs/test-assignment',
      'responseModel' => 'Resource',
      'parameters' => [
        'clientUserId' => [
          'required' => TRUE,
          'location' => 'json',
          'default' => pads_ldr_client_user_id(),
        ],
        'studentRecordId' => [
          'required' => TRUE,
          'location' => 'json',
        ],
        'testBatteryId' => [
          'required' => TRUE,
          'location' => 'json',
        ],
        'instructionStatus' => [
          'required' => FALSE,
          'location' => 'json',
        ],
        'enableLineReader' => [
          'required' => FALSE,
          'location' => 'json',
        ],
        'enableTextToSpeech' => [
          'required' => FALSE,
          'location' => 'json',
        ],
      ],
    ],
    'postTestAssignments' => [
      'httpMethod' => 'POST',
      'uri' => 'ldr_svcs/test-assignments',
      'responseModel' => 'Resource',
      'parameters' => [
        'clientUserId' => [
          'required' => TRUE,
          'location' => 'json',
          'default' => pads_ldr_client_user_id(),
        ],
        'classId' => [
          'required' => TRUE,
          'location' => 'json',
        ],
        'testBatteryId' => [
          'required' => TRUE,
          'location' => 'json',
        ],
        'allOrNone' => [
          'required' => FALSE,
          'location' => 'json',
        ],
      ],
    ],
    'unlockTest' => [
      'httpMethod' => 'PUT',
      'uri' => 'ldr_svcs/unlock-test/{testAssignmentId}',
      'responseModel' => 'Resource',
      'parameters' => [
        'clientUserId' => [
          'required' => TRUE,
          'location' => 'json',
          'default' => pads_ldr_client_user_id(),
        ],
        'testAssignmentId' => [
          'required' => TRUE,
          'location' => 'uri',
        ],
      ],
    ],
    'updateTestAssignment' => [
      'httpMethod' => 'PUT',
      'uri' => 'ldr_svcs/test-assignment/{testAssignmentId}',
      'responseModel' => 'Resource',
      'parameters' => [
        'testAssignmentId' => [
          'required' => TRUE,
          'location' => 'uri',
        ],
        'clientUserId' => [
          'required' => TRUE,
          'location' => 'json',
          'default' => pads_ldr_client_user_id(),
        ],
        'testAssignmentStatus' => [
          'required' => FALSE,
          'location' => 'json',
        ],
        'instructionStatus' => [
          'required' => FALSE,
          'location' => 'json',
        ],
        'enableLineReader' => [
          'required' => FALSE,
          'location' => 'json',
        ],
        'enableTextToSpeech' => [
          'required' => FALSE,
          'location' => 'json',
        ],
      ],
    ],
  ],
  'models' => [
    'Resource' => [
      'type' => 'object',
      'additionalProperties' => [
        'location' => 'json',
      ],
    ],
  ],
];
