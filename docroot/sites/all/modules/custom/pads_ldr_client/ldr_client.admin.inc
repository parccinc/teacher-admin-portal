<?php
/**
 * @file
 * Form methods for LDR Client config form.
 */

/**
 * Form callback for LDR client.
 *
 * @param array $form
 *   An empty form object.
 * @param array $form_state
 *   The form_state object.
 *
 * @return array
 *   The systems setting form.
 */
function ldr_client_admin_form($form, &$form_state) {
  $form = array();
  $form['ldr_uri'] = array(
    '#type' => 'textfield',
    '#title' => 'URI',
    '#description' => 'URI location of LDR without trailing slash',
    '#default_value' => variable_get('ldr_uri'),
  );
  $form['credentials'] = array(
    '#type' => 'container',
    '#attributes' => array(),
  );
  $form['credentials']['ldr_username'] = array(
    '#type' => 'textfield',
    '#title' => 'Username',
    '#description' => 'LDR username.',
    '#default_value' => variable_get('ldr_username'),
  );
  $form['credentials']['ldr_password'] = array(
    '#type' => 'password',
    '#title' => 'Password',
    '#description' => 'LDR password. Leave blank to keep unchanged.',
  );
  return system_settings_form($form);
}

/**
 * Validate LDR client admin settings form.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form_state array.
 */
function ldr_client_admin_form_validate($form, &$form_state) {
  $password = $form_state['values']['ldr_password'];
  if (empty($password)) {
    form_set_value($form['credentials']['ldr_password'], variable_get('ldr_password'), $form_state);
  }
  else {
    if (module_exists('encrypt')) {
      $password = encrypt($password);
    }
    form_set_value($form['credentials']['ldr_password'], $password, $form_state);
  }
}
