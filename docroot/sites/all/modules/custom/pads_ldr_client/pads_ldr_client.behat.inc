<?php
/**
 * @file
 * Contains \LdrContext.
 */

use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Behat\Hook\Scope\AfterScenarioScope;
use Behat\Gherkin\Node\TableNode;
use Drupal\DrupalDriverManager;
use Drupal\DrupalExtension\Context\DrupalSubContextBase;
use Drupal\DrupalExtension\Context\DrupalSubContextInterface;
use Parcc\Ldr\Driver;

class LdrContext extends DrupalSubcontextBase implements DrupalSubContextInterface {

  /** @var Parcc\Ldr\Driver */
  protected $driver;
  protected $ldrData = array();

  protected $contexts;
  protected $response;

  /**
   * {@inheritdoc}
   */
  public function __construct(DrupalDriverManager $drupal) {
    parent::__construct($drupal);
    $this->response = array();
  }

  /**
   * @BeforeScenario
   *
   * @param \Behat\Behat\Hook\Scope\BeforeScenarioScope $scope
   */
  public function prepare(BeforeScenarioScope $scope) {
    $ldrPath = $scope->getEnvironment()->getSuite()->getSetting('ldr_path');
    $this->driver = Driver::factory(realpath($ldrPath));
    $this->contexts['mink'] = $scope->getEnvironment()->getContext('PadsMinkContext');

    $this->ldrData['orgs'] = array();
    $this->ldrData['classes'] = array();
    $this->ldrData['class_assignments'] = array();
    $this->ldrData['students'] = array();
    $this->ldrData['tests'] = array();
    $this->ldrData['assignments'] = array();
    $this->ldrData['score_reports'] = array();
  }

  /**
   * @BeforeScenario
   */
  public function resetLdr() {
    $this->driver->reset();
  }

  /**
   * @When I call the Ldr service :operation with JSON args :args
   */
  public function iCallTheLdrServiceWithJsonArgs($operation, $args) {
    $args = $this->getContext('PadsMinkContext')->fixStepArgument($args);
    $args = json_decode($args, TRUE);
    $this->response = pads_ldr_client_call($operation, $args);
  }

  /**
   * @Then The text in element :locator contains Ldr response :key
   */
  public function theTextInElementContainsLdrResponse($locator, $key) {
    $element = $this->assertSession()->elementExists('css', $locator);
    $text = $element->getText();

    if (!array_key_exists($key, $this->response)) {
      throw new \Exception(sprintf('Ldr response does not contain %s', $key));
    }
    if (strpos($text, $this->response[$key]) === FALSE) {
      throw new \Exception('These are not the values you are looking for.');
    }
  }

  /**
   * @Then /^I should see "(?P<property>[^"]*)" in Ldr response$/
   */
  public function assertLdrResponse($property) {
    $response = $this->response;
    if (!array_key_exists($property, $response)) {
      // If the response is not empty, check the first element for the property.
      if (!empty($response)) {
        $first_element = reset($response);
        if (is_array($first_element) && array_key_exists($property, $first_element)) {
          return;
        }
      }
      throw new \Exception("The LDR response does not contain '$property'.");
    }
  }

  /**
   * @Then /^I should see "(?P<select>[^"]*)" options with name matching LDR property "(?P<name>[^"]*)" and value matching LDR property "(?P<value>[^"]*)"$/
   */
  public function assertSelectOptions($select, $name, $value) {
    $select = $this->fixStepArgument($select);
    $name = $this->fixStepArgument($name);
    $value = $this->fixStepArgument($value);

    $response = $this->response;
    foreach ($response as $response_item) {
      if (!array_key_exists($name, $response_item)) {
        throw new \Exception("$name property not found in LDR response");
      }
      // Selects all options in select field that match the name/value pair
      // from LDR response. Doesn't return a value, but throws an exception
      // if value not found.
      $this->getSession()
        ->getPage()
        ->selectFieldOption($select, $response_item[$name]);
    }
  }

  /**
   * @When I should find key|value pair :arg1 in the :arg2 property of Ldr response
   */
  public function iShouldFindKeyValuePairInThePropertyOfLdrResponse($arg1, $arg2) {
    if (strpos($arg1, '|') === FALSE) {
      throw new \Exception("Expected a '|' in the key|value argument.");
    }
    $key_value = explode('|', $arg1);
    $key = $key_value[0];
    $value = $key_value[1];
    $value = $this->getContext('PadsMinkContext')->fixStepArgument($value);

    $this->assertLdrResponse($arg2);
    $results = $this->response[$arg2];
    foreach ($results as $result) {
      if (array_key_exists($key, $result) && $result[$key] == $value) {
        return;
      }
    }
    throw new \Exception("$arg1 not found in $arg2 key of Ldr response.");

  }

  /**
   * Log in as someone if not already so Ldr doesn't soil itself.
   */
  static public function ldrClientUserLogIn() {
    if (user_is_anonymous()) {
      global $user;
      $user = user_load(1);
    }
  }

  /**
   * Creates organizations and migrates them into ADS.
   *
   * Given LDR organizations:
   *   | name   | type     | identifier | parent     |
   *   | XXD6   | district | 6000       | XX         |
   *   | XXD6S1 | school   | 6001       | [org:XXD6] |
   *   | XXD6S2 | school   | 6002       | [org:XXD6] |
   *
   * Parent org may be referenced by ID (preferred) or by name looking one level
   * up from asserted org.
   *
   * @Given LDR organizations:
   *
   * @param \Behat\Gherkin\Node\TableNode $table
   */
  public function assertOrganizations(TableNode $table) {
    $data = $table->getHash();

    foreach ($data as $row) {
      $row = $this->fixRow($row);

      $org = array();

      // Transform data into LDR-ish data.
      $org['organizationName'] = trim($row['name']);
      switch ($row['type']) {
        case 'state':
          $org['organizationType'] = 2;
          break;
        case 'district':
          $org['organizationType'] = 3;
          break;
        case 'school':
          $org['organizationType'] = 4;
          break;
        default:
          $org['organizationType'] = 0;
      }
      $org['organizationIdentifier'] = $row['identifier'];
      $org['parentOrganizationId'] = $row['parent'];
      $org = $this->driver->addOrganization($org);

      // States aren't handled by this assertOrganizations. We need to add the
      // token for the parent state.
      if ($row['type'] == 'district') {
        $this->contexts['mink']->addToken("[org:{$row['parent']}]", $org->parentOrganizationId);
      }

      $this->contexts['mink']->addToken("[org:{$org->organizationName}]", $org->organizationId);
      $this->ldrData['orgs'][$org->organizationId] = $org;
    }

    // Do migrate to get em into Drupal.
    drupal_static_reset();
    pads_organization_process_import();
  }

  /**
   * Creates classes and migrates them into ADS.
   *
   * "ref" is may be an arbitrary value in case the class later needs to be
   * referenced by ID.
   *
   * Given LDR class orgs:
   *   | name    | section | org          | grade | ref  |
   *   | Algebra |         | [org:XXD6S1] | 10    |      |
   *   | English | Lit 1   | [org:XXD6S1] | 10    |      |
   *   | Math    |         | [org:XXD6S1] | 6     | math |
   *
   * @Given LDR class orgs:
   *
   * @param \Behat\Gherkin\Node\TableNode $table
   */
  public function assertClasses(TableNode $table) {
    $data = $table->getHash();

    $properties = array(
      'name' => 'classIdentifier',
      'section' => 'sectionNumber',
      'org' => 'organizationId',
      'grade' => 'gradeLevel',
    );

    foreach ($data as $row) {
      $class = $this->fixRow($row);
      $class = $this->transposeProperties($class, $properties);
      $class = $this->driver->addClass($class);
      $ref = (!empty($class->ref)) ? $class->ref : $class->classId;
      $this->contexts['mink']->addToken("[class:{$ref}]", $class->classId);
      $this->ldrData['classes'][$class->classId] = $class;
    }
  }

  /**
   * Creates assignments between classes and students.
   *
   * Given LDR class assignments:
   *   | student | class |
   *
   * @Given LDR class assignments:
   *
   * @param \Behat\Gherkin\Node\TableNode $table
   */
  public function assertClassAssignment(TableNode $table) {
    $data = $table->getHash();

    $properties = array(
      'student' => 'studentRecordId',
      'class' => 'classId',
    );

    foreach ($data as $row) {
      $assignment = $this->fixRow($row);
      $assignment = $this->transposeProperties($assignment, $properties);
      $assignment = $this->driver->addClassAssignment($assignment);
      $ref = (!empty($assignment->ref)) ? $assignment->ref : $assignment->studentClassId;
      $this->contexts['mink']->addToken("[class_assign:{$ref}]", $assignment->studentClassId);
      $this->ldrData['class_assignments'][$assignment->studentClassId] = $assignment;
    }
  }

  /**
   * Creates students.
   *
   * Given LDR students:
   *   | firstName | lastName | enrollOrgId  | dateOfBirth | stateIdentifier | gender | gradeLevel | classes      |
   *   | Marnie    | Hummel   | [org:XXD6S1] | 2005-01-01  | IL              | F      | 3          | [class:math] |
   *
   * "classes" is an optional, comma-separated list of class IDs.
   *
   * Additional attributes for race, disability, state options not supported at
   * this time.
   *
   * @Given LDR students:
   *
   * @param \Behat\Gherkin\Node\TableNode $table
   */
  public function assertStudents(TableNode $table) {
    $data = $table->getHash();

    foreach ($data as $row) {
      $student = $this->fixRow($row);
      $student = $this->driver->addStudent($student);
      $this->contexts['mink']->addToken("[student:{$student->firstName} {$student->lastName}]", $student->studentRecordId);
      $this->ldrData['students'][$student->studentRecordId] = $student;

      // Assign student to classes if provided.
      if (!empty($student->classes)) {
        $classes = explode(',', $student->classes);
        foreach ($classes as $class) {
          $params = array(
            'studentRecordId' => $student->studentRecordId,
            'classId' => $class,
          );
          $assignment = $this->driver->addClassAssignment($params);
          $this->ldrData['class_assignments'][$assignment->studentClassId] = $assignment;
        }
      }

    }
  }

  /**
   * Check for the existence of a student.
   *
   * @Then there should be a student :student_id in state :state
   *
   * @param string $identifier
   * @param string $state
   *
   * @throws Exception
   */
  public function assertStudentExists($identifier, $state) {
    if (!$student = $this->driver->getStudentByIdentifier($identifier, $state)) {
      throw new Exception(sprintf('Student "%s" for state "%s" not found.', $identifier, $state));
    }
  }

  /**
   * Check for the non-existence of a student.
   *
   * @Then there should not be a student :student_id in state :state
   *
   * @param string $identifier
   * @param string $state
   *
   * @throws Exception
   */
  public function assertStudentNotExists($identifier, $state) {
    if ($student = $this->driver->getStudentByIdentifier($identifier, $state)) {
      throw new Exception(sprintf('Student "%s" for state "%s" exists, but should not.', $identifier, $state));
    }
  }

  /**
   * @Given students :student_record_ids are assigned to :class_ids
   */
  public function studentsAreAssignedTo($student_record_ids, $class_ids) {
    $class_ids = explode(',', $class_ids);
    $student_record_ids = explode(',', $student_record_ids);
    foreach ($class_ids as $class_id) {
      $class_id = $this->contexts['mink']->fixStepArgument($class_id);
      foreach ($student_record_ids as $student_record_id) {
        $fixed_student_record_id = $this->contexts['mink']->fixStepArgument($student_record_id);
        $params = array(
          'studentRecordId' => $fixed_student_record_id,
          'classId' => $class_id,
        );
        $assignment = $this->driver->addClassAssignment($params);
        $this->ldrData['class_assignments'][$assignment->studentClassId] = $assignment;
      }
    }
  }

  /**
   * Creates tests and a form for each test.
   *
   * Given LDR tests:
   *   | name       | subject | grade | program               | security | permissions | scoring   | description                 |
   *   | Behat test | ELA     | 3     | Diagnostic Assessment | Secure   | Restricted  | Immediate | Test administered for Behat |
   *
   * @Given LDR tests:
   *
   * @param \Behat\Gherkin\Node\TableNode $table
   */
  public function assertTests(TableNode $table) {
    $data = $table->getHash();

    $properties = array(
      'name' => 'testBatteryName',
      'subject' => 'testBatterySubject',
      'grade' => 'testBatteryGrade',
      'program' => 'testBatteryProgram',
      'security' => 'testBatterySecurity',
      'permissions' => 'testBatteryPermissions',
      'scoring' => 'testBatteryScoring',
      'description' => 'testBatteryDescription',
    );

    foreach ($data as $row) {
      $test = $this->fixRow($row);
      $test = $this->transposeProperties($test, $properties);
      $test = $this->driver->addTest($test);
      $this->contexts['mink']->addToken("[test:{$test->testBatteryName}]", $test->testBatteryId);
      $this->ldrData['tests'][$test->testBatteryId] = $test;
    }
  }

  /**
   * Assigns tests to students.
   *
   * Status defaults to "Scheduled" and "ref" is may be an arbitrary value in
   * case the assignment later needs to be referenced by ID.
   *
   * Given LDR test assignments:
   *   | student                 | test              | status | score_report | ref    |
   *   | [student:Marnie Hummel] | [test:Behat test] | Paused | sr-marnie    | 123abc |
   *
   * @Given LDR test assignments:
   *
   * @param \Behat\Gherkin\Node\TableNode $table
   */
  public function assertTestAssignments(TableNode $table) {
    $data = $table->getHash();

    $properties = array(
      'student' => 'studentRecordId',
      'test' => 'testBatteryId',
      'status' => 'testAssignmentStatus',
      'score_report' => 'embeddedScoreReportId',
    );

    foreach ($data as $row) {
      $assignment = $this->fixRow($row);
      $assignment = $this->transposeProperties($assignment, $properties);
      $assignment = $this->driver->addTestAssignment($assignment);
      $ref = !empty($assignment->ref) ? $assignment->ref : $assignment->testAssignmentId;
      $this->contexts['mink']->addToken("[asn:$ref]", $assignment->testAssignmentId);
      $this->ldrData['assignments'][$assignment->testAssignmentId] = $assignment;
    }
  }

  /**
   * Add score reports.
   *
   * "ref" is may be an arbitrary value by which the score report may later be
   * referenced. If ref is omitted, the score report may be referenced by its
   * embeddedScoreReportId.
   *
   * Score report file is the name of a file within the test_content directory.
   * The file contents should be base64 encoded.
   *
   * In the case where line has both score_report and score_report_file set,
   * score_report will take precedence due to the behavior of the
   * Parcc\Ldr\ScoreReport class' handling of the properties in the validate()
   * method.
   *
   * Given LDR score reports:
   *   | score_report          | score_report_file      | ref      |
   *   | Wow, you did awful.   |                        | sr-danny |
   *   | This kid is a genius. |                        | sr-billy |
   *   |                       | score_report_tammy.txt | sr-tammy |
   *
   * @Given LDR score reports:
   *
   * @param \Behat\Gherkin\Node\TableNode $table
   */
  function assertScoreReports(TableNode $table) {
    $data = $table->getHash();

    foreach ($data as $row) {
      $score = $this->fixRow($row);

      if (!empty($score['score_report_file'])) {
        $score['embeddedScoreReport'] = $this->readFile($score['score_report_file']);
      }

      $score = $this->driver->addScoreReport($score);
      $ref = !empty($score->ref) ? $score->ref : $score->embeddedScoreReportId;
      $this->contexts['mink']->addToken("[sr:$ref]", $score->embeddedScoreReportId);
      $this->ldrData['score_reports'][$score->embeddedScoreReportId] = $score;
    }
  }

  /**
   * Fix arguments on a row of data.
   *
   * @param array $row
   *  Raw row array.
   *
   * @return array
   *   Fixed row array.
   */
  protected function fixRow($row) {
    foreach ($row as $key => $value) {
      $row[$key] = $this->contexts['mink']->fixStepArgument($value);
    }
    return $row;
  }

  /**
   * Transpose properties to new names needed.
   *
   * @param array $row
   *   The raw row data.
   * @param array $properties
   *   Properties to transpose, keys are existing names and values are new
   *   names.
   *
   * @return array
   */
  protected function transposeProperties($row, $properties) {
    foreach ($row as $key => $val) {
      if (!empty($properties[$key])) {
        $row[$properties[$key]] = $val;
      }
    }
    return $row;
  }

  /**
   * Deletes classes created by test activity, rather than by Given steps.
   */
  protected function deleteNonGivenClasses() {
    foreach ($this->ldrData['orgs'] as $org_id => $org) {
      $org_classes = pads_classes_get_by_org($org_id);
      if (array_key_exists('classes', $org_classes)) {
        foreach ($org_classes['classes'] as $class) {
          $this->driver->deleteClass($class['classId']);
        }
      }
    }
  }

  /**
   * Reads the contents of a file.
   *
   * @param $path
   *   The path of the file
   * @return string
   * @throws \Exception
   */
  protected function readFile($path) {

    if ($files_dir = $this->getMinkParameter('files_path')) {
      $files_dir = rtrim($files_dir, DIRECTORY_SEPARATOR);
      $path = $files_dir . DIRECTORY_SEPARATOR . $path;
    } else {
      throw new \Exception('No mink files_path parameter set.');
    }

    if (!is_readable($path) || !is_file($path)) {
      throw new \Exception(sprintf('Unable to read file "%s"', $path));
    }

    return file_get_contents($path);
  }

}
