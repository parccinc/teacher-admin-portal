<?php
/**
 * @file
 * Contains \pads_tests_views_handler_field_operation_student_assigntest.
 */

class pads_tests_views_handler_field_operation_student_assigntest extends views_handler_field {

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    // If user does not have access to assign test to student, abort.
    if (!user_access('assign test to student')) {
      return NULL;
    }
    $student_record_id = $this->get_value($values);
    $class_id = $values->classId;

    // Needed to trigger the modal.
    ctools_include('modal');
    ctools_modal_add_js();

    $options = array();
    $options['attributes']['class'][] = 'ctools-use-modal';
    $options['attributes']['class'][] = 'ctools-modal-zurb-modal-style';
    return l($this->options['label'], "students/nojs/$student_record_id/assigntest/$class_id", $options);
  }
}
