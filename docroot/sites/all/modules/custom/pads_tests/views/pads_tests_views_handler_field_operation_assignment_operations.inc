<?php
/**
 * @file
 * Contains \pads_tests_views_handler_field_operation_assignment_operations.
 */

use Drupal\pads\tests\TestFactory;

class pads_tests_views_handler_field_operation_assignment_operations extends views_handler_field {

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    $test_assignment_id = $this->get_value($values);
    $test_factory = TestFactory::getInstance();

    $output = array();
    $operations = array();

    ctools_include('modal');
    ctools_modal_add_js();

    $test_assignment = (array) $values;
    if ($test_factory->isUnlockable($test_assignment)) {
      $options = array();
      $options['query'] = drupal_get_destination();
      $options['attributes']['class'][] = 'ctools-use-modal';
      $options['attributes']['class'][] = 'ctools-modal-zurb-modal-style';
      $operations[] = l(t('Unlock Test'), "test-assignment/nojs/$test_assignment_id/unlock", $options);
    }

    if ($test_factory->isCancelable($test_assignment)) {
      $options = array();
      $options['query'] = drupal_get_destination();
      $options['attributes']['class'][] = 'ctools-use-modal';
      $options['attributes']['class'][] = 'ctools-modal-zurb-modal-style';
      $operations[] = l(t('Cancel Test'), "test-assignment/nojs/$test_assignment_id/cancel", $options);
    }

    $output['operations']['#markup'] = implode('&emsp;', $operations);

    return $output;
  }

}
