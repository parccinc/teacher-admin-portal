<?php
/**
 * @file
 * Views functionality for tests.
 */

/**
 * Implements hook_views_data().
 */
function pads_tests_views_data() {
  $data = array();
  $data['pads_test_assignments']['table']['group'] = t('Pads Test Assignments');
  $data['pads_test_assignments']['table']['base'] = array(
    'title' => t('Pads Test Assignments'),
    'help' => t('Query Pads Test Assignments.'),
    'field' => 'testAssignmentId',
    'query class' => 'pads_tests_views_query',
  );
  $data['pads_test_assignments']['testAssignmentId'] = array(
    'title' => t('Test assignment id'),
    'help' => t('Ldr assigned test assignment id'),
    'field' => array(
      'handler' => 'pads_tests_views_handler_field_test_assignments',
      'click sortable' => TRUE,
    ),
  );
  $data['pads_test_assignments']['testAssignmentStatus'] = array(
    'title' => t('Status'),
    'help' => t('Test assignment status'),
    'field' => array(
      'handler' => 'pads_tests_views_handler_field_test_assignments',
      'click sortable' => TRUE,
    ),
  );
  $data['pads_test_assignments']['classId'] = array(
    'title' => t('Class id'),
    'help' => t('Class id of student in test assignment'),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );
  $data['pads_test_assignments']['className'] = array(
    'title' => t('Class name'),
    'help' => t('Class name'),
    'field' => array(
      'handler' => 'pads_tests_views_handler_field_test_assignments',
      'click sortable' => TRUE,
    ),
  );
  $data['pads_test_assignments']['classStudentCount'] = array(
    'title' => t('Class student count'),
    'help' => t('Number of students in class'),
    'field' => array(
      'handler' => 'pads_tests_views_handler_field_test_assignments',
      'click sortable' => TRUE,
    ),
  );
  $data['pads_test_assignments']['batteryAssignmentCount'] = array(
    'title' => t('Battery assignment count'),
    'help' => t('Number of students assigned to battery'),
    'field' => array(
      'handler' => 'pads_tests_views_handler_field_test_assignments',
      'click sortable' => TRUE,
    ),
  );
  $data['pads_test_assignments']['enableLineReader'] = array(
    'title' => t('Accommodation Enable Line Reader'),
    'help' => t('Accommodation Enable Line Reader'),
    'field' => array(
      'handler' => 'pads_tests_views_handler_field_operation_assignment_checkbox',
      'click sortable' => TRUE,
    ),
  );
  $data['pads_test_assignments']['enableTextToSpeech'] = array(
    'title' => t('Accommodation Enable Text to Speech'),
    'help' => t('Accommodation Enable Text to Speech'),
    'field' => array(
      'handler' => 'pads_tests_views_handler_field_operation_assignment_checkbox',
      'click sortable' => TRUE,
    ),
  );
  $data['pads_test_assignments']['studentFirstName'] = array(
    'title' => t('Student First Name'),
    'help' => t('Student first name'),
    'field' => array(
      'handler' => 'pads_tests_views_handler_field_test_assignments',
      'click sortable' => TRUE,
    ),
  );
  $data['pads_test_assignments']['studentLastName'] = array(
    'title' => t('Student Last Name'),
    'help' => t('Student last name'),
    'field' => array(
      'handler' => 'pads_tests_views_handler_field_test_assignments',
      'click sortable' => TRUE,
    ),
  );
  $data['pads_test_assignments']['studentRecordId'] = array(
    'title' => t('Student Record id'),
    'help' => t('Student record id of student assigned to test'),
    'field' => array(
      'handler' => 'pads_tests_views_handler_field_test_assignments',
      'click sortable' => TRUE,
    ),
  );
  $data['pads_test_assignments']['studentStateIdentifier'] = array(
    'title' => t('Student State Identifier'),
    'help' => t('State Identifier of student assigned to test'),
    'field' => array(
      'handler' => 'pads_tests_views_handler_field_test_assignments',
      'click sortable' => TRUE,
    ),
  );
  $data['pads_test_assignments']['testBatteryName'] = array(
    'title' => t('Battery Name'),
    'help' => t('Test battery name'),
    'field' => array(
      'handler' => 'pads_tests_views_handler_field_test_assignments',
      'click sortable' => TRUE,
    ),
  );
  $data['pads_test_assignments']['testBatterySubject'] = array(
    'title' => t('Battery Subject'),
    'help' => t('Test battery subject'),
    'field' => array(
      'handler' => 'pads_tests_views_handler_field_test_assignments',
      'click sortable' => TRUE,
    ),
  );
  $data['pads_test_assignments']['testBatteryGrade'] = array(
    'title' => t('Battery Grade'),
    'help' => t('Test battery grade'),
    'field' => array(
      'handler' => 'pads_tests_views_handler_field_test_assignments',
      'click sortable' => TRUE,
    ),
  );
  $data['pads_test_assignments']['testBatteryForm'] = array(
    'title' => t('Form'),
    'help' => t('Test battery form'),
    'field' => array(
      'handler' => 'pads_tests_views_handler_field_test_assignments',
      'click sortable' => TRUE,
    ),
  );

  $data['pads_test_assignments']['classTestKey'] = array(
    'title' => t('Access codes for a class'),
    'group' => t('Pads Test Assignments'),
    'help' => t('Provide a link to print class test assignment keys.'),
    'real field' => 'classId',
    'field' => array(
      'handler' => 'pads_tests_views_handler_field_operation_class_testkeys',
    ),
  );

  // pads_tests_views_handler_field_operation_assignment_operations
  $data['pads_test_assignments']['operations'] = array(
    'title' => t('Operations'),
    'help' => t('Operations that may be performed on the test assignment'),
    'real field' => 'testAssignmentId',
    'field' => array(
      'handler' => 'pads_tests_views_handler_field_operation_assignment_operations',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function pads_tests_views_data_alter(&$data) {
  $data['pads_students']['studentTestKey'] = array(
    'title' => t('Student access code'),
    'group' => t('Student test operations'),
    'help' => t('Provide a link to print student test assignment keys.'),
    'real field' => 'studentRecordId',
    'field' => array(
      'handler' => 'pads_tests_views_handler_field_operation_student_testkey',
    ),
  );

  $data['pads_students']['assignTest'] = array(
    'title' => t('Assign test'),
    'group' => t('Student test operations'),
    'help' => t('Create test assignment for student.'),
    'real field' => 'studentRecordId',
    'field' => array(
      'handler' => 'pads_tests_views_handler_field_operation_student_assigntest',
    ),
  );

  // PARADS-4188 View/Edit Test Assignment.
  $data['pads_students']['editTestAssignment'] = array(
    'title' => t('Access codes for a class'),
    'group' => t('Student test operations'),
    'help' => t('Provide a link to view/edit test assignments.'),
    'real field' => 'studentRecordId',
    'field' => array(
      'handler' => 'pads_tests_views_handler_field_operation_student_testassignment',
    ),
  );
}

/**
 * Implements hook_views_plugins().
 */
function pads_tests_views_plugins() {
  return array(
    'query' => array(
      'pads_tests_views_query' => array(
        'title' => t('SQL Query'),
        'help' => t('Query will be generated and run using the Drupal database API.'),
        'handler' => 'pads_tests_views_query',
      ),
    ),
  );
}
