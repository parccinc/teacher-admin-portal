<?php
/**
 * @file
 * Install, update, and uninstall functions for the Foo Bar module.
 * Contains \Fully\Qualified\Namespace\And\NameOfTheClass.
 */

class views_handler_field_testassignments extends views_handler_field {

  protected $testAssignments;

  /**
   * {@inheritdoc}
   */
  public function init(&$view, &$options) {
    $this->testAssignments = array();
    parent::init($view, $options);
  }

  /**
   * Uses classes to retrieve student test assignments.
   */
  public function query() {
    // Query must be an instance of pads_students_views_query.
    $class_ids = $this->query->getClassIds();
    if (!empty($class_ids)) {
      $class_ids = implode(',', array_keys($class_ids));
    }

    $this->testAssignments = $this->getTestAssignments($class_ids);
    parent::query();
  }

  /**
   * Statically save test assignments for class implementors.
   *
   * @param string $class_ids
   *   Comma-separated list of class ids.
   *
   * @return array
   *   Array of test assignments keyed by studentRecordId.
   */
  public function getTestAssignments($class_ids) {
    $test_assignments = &drupal_static(__FUNCTION__ . ":" . drupal_json_encode($class_ids));
    if (!$test_assignments) {
      $test_assignments = array();
      $query = array(
        'classIds' => $class_ids,
      );
      $ldr_test_assignments = pads_tests_get_test_assignments($query);
      if ($ldr_test_assignments && !array_key_exists('error', $ldr_test_assignments)) {
        foreach ($ldr_test_assignments as $ldr_test_assignment) {
          $test_assignments[$ldr_test_assignment['studentRecordId']][] = $ldr_test_assignment;
        }
      }
    }
    return $test_assignments;
  }

  /**
   * Retrieve test assignments for a student.
   *
   * @param int $student_record_id
   *   The student_record_id from Ldr.
   *
   * @return array
   *   Test assignments as returned by Ldr's GET test-assignments.
   */
  public function getTestAssignmentsForStudent($student_record_id) {
    if (array_key_exists($student_record_id, $this->testAssignments)) {
      return $this->testAssignments[$student_record_id];
    }
    return array();
  }

}
