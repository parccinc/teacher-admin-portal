<?php
/**
 * @file
 * Contains \pads_tests_views_handler_field_operation_student_testkey.
 */

class pads_tests_views_handler_field_operation_student_testassignment extends views_handler_field_testassignments {

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    $student_record_id = $this->get_value($values);
    $test_assignments = $this->getTestAssignmentsForStudent($student_record_id);

    $links = array();
    $test = new \Drupal\pads\tests\LdrTest();

    $query['destination'] = current_path();
    foreach ($test_assignments as $test_assignment) {
      if ($test->isEditable($test_assignment)) {
        $link = array(
          'title' => $test_assignment['testBatteryName'],
          // TODO: Retrieve test assignment by testAssignmentId when LDR has that
          // available.
          'href' => "testassignment/nojs/$student_record_id/{$test_assignment['testBatteryId']}",
          'query' => $query,
          'attributes' => array(
            'class' => array('ctools-use-modal', 'ctools-modal-zurb-modal-style'),
          ),
        );
        $links[] = $link;
      }
    }

    if (empty($links)) {
      return '';
    }

    $test_key_dropdown = array(
      '#theme' => 'links__ads_dropdown',
      '#links' => $links,
      '#heading' => $this->options['label'],
    );

    // Needed to trigger the modal.
    ctools_include('modal');
    ctools_modal_add_js();
    return drupal_render($test_key_dropdown);
  }

}
