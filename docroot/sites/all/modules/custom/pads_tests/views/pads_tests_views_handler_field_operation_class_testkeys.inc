<?php
/**
 * @file
 * Contains \pads_tests_views_handler_field_operation_class_testkeys.
 */

class pads_tests_views_handler_field_operation_class_testkeys extends views_handler_field {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->field_alias = $this->real_field;
  }

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    $class_id = $this->get_value($values);
    $battery_name = $values->testBatteryName;
    $test_battery_id = $values->testBatteryId;

    // Needed to trigger the modal.
    ctools_include('modal');
    ctools_modal_add_js();

    $options = array();
    $options['attributes']['class'][] = 'ctools-use-modal';
    $options['attributes']['class'][] = 'ctools-modal-zurb-modal-style';

    $text = !empty($this->options['label']) ? $this->options['label'] : $battery_name;
    return l($text, "tests/nojs/key/classes/$class_id/$test_battery_id", $options);

  }

}
