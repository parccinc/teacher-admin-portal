<?php
/**
 * @file
 * Contains \pads_tests_views_handler_field_operation_class_testkeys.
 */

class pads_tests_views_handler_field_operation_assignment_checkbox extends views_handler_field {

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    $value = $this->get_value($values) == 'Y';
    return pads_tests_accommodations_link((array) $values, $this->real_field, $value);
  }
}
