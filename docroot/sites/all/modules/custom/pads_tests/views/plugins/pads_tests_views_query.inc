<?php
/**
 * @file
 * Contains \pads_tests_views_query.
 */

class pads_tests_views_query extends views_plugin_query_default {

  protected $classIds;

  /**
   * {@inheritdoc}
   */
  public function init($base_table, $base_field, $options) {
    $this->classIds = array();
    parent::init($base_table, $base_field, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function query($get_count = FALSE) {
  }

  /**
   * {@inheritdoc}
   */
  public function placeholder($base = 'views') {
  }

  /**
   * {@inheritdoc}
   */
  public function add_where_expression($group, $snippet, $args = array()) {
    // Defined context filters are either Organization ids or Class ids.
    // If the snippet is the Class id "where" clause, set $classIds. Otherwise
    // we are querying by Organization id.
    if ($snippet == 'pads_test_assignments.classId =  ') {
      $this->classIds[] = reset($args);
    }
    // Check for multiple class ids.
    elseif ($snippet == "pads_test_assignments.classId IN() ") {
      foreach ($args as $class_ids) {
        foreach ($class_ids as $class_id) {
          $this->classIds[] = $class_id;
        }
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function add_field($table, $field, $alias = '', $params = array()) {
    // Make sure an alias is assigned.
    $alias = $alias ? $alias : $field;

    // PostgreSQL truncates aliases to 63 characters. See
    // http://drupal.org/node/571548.

    // We limit the length of the original alias up to 60 characters to get a
    // unique alias later if its have duplicates.
    $alias = substr($alias, 0, 60);

    // Create a field info array.
    $field_info = array(
      'field' => $field,
      'table' => $table,
      'alias' => $alias,
    ) + $params;

    // Test to see if the field is actually the same or not. Due to differing
    // parameters changing the aggregation function, we need to do some
    // automatic alias collision detection.
    $base = $alias;
    $counter = 0;
    while (!empty($this->fields[$alias]) && $this->fields[$alias] != $field_info) {
      $field_info['alias'] = $alias = $base . '_' . ++$counter;
    }

    if (empty($this->fields[$alias])) {
      $this->fields[$alias] = $field_info;
    }

    // Keep track of all aliases used.
    $this->field_aliases[$table][$field] = $alias;

    return $alias;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(&$view) {
    // We expect class ids in views argument.
    foreach ($this->classIds as $class_id) {
      // Retrieve test assignments for this class id from ldr.
      $args['classIds'] = $class_id;
      $test_assignments = pads_tests_get_test_assignments($args);

      // Retrieve class info from Ldr.
      $class = pads_classes_get($class_id);
      // Find number of students in class.
      $class_students = pads_students_get_by_class($class_id);
      $student_count = (!$class_students || array_key_exists('error', $class_students)) ? 0 : count($class_students);
      $class['studentCount'] = format_plural($student_count, '1 student', '@student_count students', array(
        '@student_count' => $student_count,
      ));

      // If there are no test assignments, make an empty test assignment record.
      if (!$test_assignments || array_key_exists('error', $test_assignments)) {
        if ($this->options['include_empty_classes']) {
          $test_assignments = array(
            $class_id => array(
              'testBatteryName' => 'empty' . $class_id,
              'testBatterySubject' => 'empty' . $class_id,
              'testBatteryGrade' => 'empty' . $class_id,
              'testAssignmentId' => 0,
              'testAssignmentStatus' => 'No Results',
              'batteryAssignmentCount' => '0',
              'classId' => $class_id,
              'className' => pads_classes_format_name($class),
              'classStudentCount' => $class['studentCount'],
            ),
          );
        }
        else {
          continue;
        }
      }

      // Todo: Find a better way to group test assignment counts.
      $results = array();
      foreach ($test_assignments as $test_assignment) {
        $name = $test_assignment['testBatteryName'];
        $subject = $test_assignment['testBatterySubject'];
        $grade = $test_assignment['testBatteryGrade'];
        if ($this->options['aggregate_by_test']) {
          $key = "{$name}_{$subject}_{$grade}";
        }
        else {
          $key = $test_assignment['testAssignmentId'];
        }
        $count = array_key_exists($key, $results) ? $results[$key]['batteryAssignmentCount'] + 1 : 1;
        $test_assignment['batteryAssignmentCount'] = $count;
        $test_assignment['classId'] = $class_id;
        $test_assignment['className'] = pads_classes_format_name($class);
        $test_assignment['classStudentCount'] = $class['studentCount'];
        $results[$key] = $test_assignment;
      }

      foreach ($results as $result) {
        $view->result[] = (object) $result;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['aggregate_by_test'] = array(
      'default' => FALSE,
      'translatable' => FALSE,
      'bool' => TRUE,
    );
    $options['include_empty_classes'] = array(
      'default' => FALSE,
      'translatable' => FALSE,
      'bool' => TRUE,
    );
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['aggregate_by_test'] = array(
      '#title' => t('Aggregate Test Assignments By Test Name'),
      '#description' => t('Displays the aggregated rows of the test assignments, grouping by name, subject, and grade.'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['aggregate_by_test']),
    );
    $form['include_empty_classes'] = array(
      '#title' => t('Include grouping rows for empty classes'),
      '#description' => t('Includes rows for classes with no test assignments so they can be displayed in groups'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['include_empty_classes']),
    );

  }
}
