<?php
/**
 * @file
 * Contains \pads_classes_views_handler_field_class_identifier.
 */

class pads_tests_views_handler_field_test_assignments extends views_handler_field {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->field_alias = $this->real_field;
  }

  public function render($values) {
    $render = parent::render($values);

    // Special handling for testAssignmentStatus field.
    if ($this->real_field == 'testAssignmentStatus') {

      // Only for submitted tests with score reports.
      if ($values->testAssignmentStatus == 'Submitted' && !empty($values->embeddedScoreReportId)) {
        $options['attributes']['target'] = 'blank';
        $render .= '<br />' . l(t('View Score Report'), "test-assignment/{$values->testAssignmentId}/report", $options);
      }
    }

    return $render;
  }

}
