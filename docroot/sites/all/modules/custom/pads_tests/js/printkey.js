(function ( $ ) {

  $.fn.printkey = function( key ) {

    function closePrint () {
      document.body.removeChild(this.__container__);
    }

    function setPrint () {
      this.contentWindow.__container__ = this;
      this.contentWindow.onbeforeunload = closePrint;
      this.contentWindow.onafterprint = closePrint;

      var printed = this.contentWindow.document.execCommand('print', false, null);
      if (!printed) {
        this.contentWindow.print();
      }
    }

    var iframe = document.createElement('iframe');
    iframe.style.visibility = "hidden";
    iframe.style.position = "fixed";
    iframe.style.right = "0";
    iframe.style.bottom = "0";
    document.body.appendChild(iframe);

    iframe.onload = setPrint;
    iframe.contentWindow.document.open();
    iframe.contentWindow.document.write(key);
    iframe.contentWindow.document.close();
  };

}( jQuery ));
