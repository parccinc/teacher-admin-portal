(function ($) {

    Drupal.ajax.prototype.commands.meow = function (ajax, response) {
        $.meow({message: response.message});
    };

})(jQuery);
