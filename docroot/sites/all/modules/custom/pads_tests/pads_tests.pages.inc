<?php
/**
 * @file
 * Page functionality for the pads_tests module.
 */

use Drupal\pads\tests\TestFactory;

/**
 * Modal callback to show test assignment form.
 *
 * @param bool $js
 *   TRUE if JavaScript is enabled.
 * @param int $student
 *   Ldr student record id.
 * @param int $test_battery_id
 *   Ldr test battery id.
 *
 * @return array|mixed
 *   Array of AJAX commands.
 */
function test_assignment_callback($js, $student, $test_battery_id) {

  $query = array(
    'studentRecordId' => $student['studentRecordId'],
    'testBatteryId' => $test_battery_id,
  );
  $test_assignment = pads_tests_get_test_assignments($query);
  $test_assignment = !array_key_exists('error', $test_assignment) ?
    reset($test_assignment) : FALSE;

  if (!$js) {
    return drupal_get_form('test_assignment_form', $student, $test_assignment);
  }

  ctools_include('modal');
  ctools_include('ajax');

  $form_state = array(
    'title' => t('Test Assignment'),
    'ajax' => TRUE,
    'build_info' => array(
      'args' => array(
        $student,
        $test_assignment,
      ),
    ),
  );
  $output = ctools_modal_form_wrapper('test_assignment_form', $form_state);
  if (!empty($form_state['executed'])) {
    // We'll just overwrite the form output if it was successful.
    $commands = array();

    $op = $form_state['triggering_element']['#name'];
    if ($op == 'save') {
      $commands[] = ctools_ajax_command_reload();
    }
    else {
      $commands[] = ctools_modal_command_dismiss();
    }
    $output = $commands;
  }
  print ajax_render($output);
  drupal_exit();
}

/**
 * Test assignment view/edit form.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 *
 * @return array
 *   Form render array.
 */
function test_assignment_form($form, &$form_state) {
  $student = $form_state['build_info']['args'][0];
  $test_assignment = $form_state['build_info']['args'][1];

  $test = TestFactory::getInstance();
  $form_state['storage']['accommodations_locked'] = !$test->canUpdateAccommodations($test_assignment['testAssignmentId']);

  test_assignment_info_form($form, $form_state, $student, $test_assignment);

  test_assignment_accommodations_form($form, $form_state, $test_assignment);

  test_assignment_timing_form($form, $form_state, $test_assignment);

  $form['actions'] = array('#type' => 'actions');

  if (empty($form_state['storage']['accommodations_locked'])) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
      '#name' => 'save',
      '#theme_wrappers' => array('button__tap_submit'),
    );
  }

  $form['close'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#name' => 'close',
    '#theme_wrappers' => array('button__tap_cancel'),
  );

  return $form;
}

/**
 * Test assignment form submit.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 */
function test_assignment_form_submit($form, &$form_state) {
  $op = $form_state['triggering_element']['#name'];
  if ($op == 'save') {
    $test_assignment = $form_state['build_info']['args'][1];
    $instruction_status = $form_state['values']['instructionStatus'];
    $enable_text_to_speech = $form_state['values']['accommodations']['text_to_speech'];
    $enable_line_reader = $form_state['values']['accommodations']['line_reader'];
    // Convert checkbox accommodations into LDR variables.
    $enable_text_to_speech = !is_numeric($enable_text_to_speech) ? 'Y' : 'N';
    $enable_line_reader = !is_numeric($enable_line_reader) ? 'Y' : 'N';
    $result = TestFactory::getInstance()
      ->updateTestAssignment($test_assignment['testAssignmentId'], $instruction_status, $enable_line_reader, $enable_text_to_speech);
    if (!array_key_exists('errors', $result)) {
      drupal_set_message(t('Test assignment updated.'));
    }
    else {
      drupal_set_message(t('Sorry, the test assignment update did not work.'), 'error');
    }
  }
}

/**
 * Page callback for modal access code form.
 *
 * @param bool $js
 *   TRUE if JavaScript is enabled.
 * @param int $class_id
 *   Ldr class Id.
 * @param int $test_battery_id
 *   Test battery id.
 * @param bool $print
 *   Indicates if the print operation is being performed.
 *
 * @return array|mixed
 *   Drupal render array or Drupal ajax commands.
 */
function test_key_class($js, $class_id, $test_battery_id, $print = FALSE) {

  $class = array(
    'classIds' => $class_id,
    'testBatteryId' => $test_battery_id,
  );

  if ($print) {
    $output = pads_tests_generate_class_keys($class, $js);
    if (!$js) {
      return $output;
    }
    else {
      ctools_include('ajax');
      ctools_include('modal');
      $commands[] = ajax_command_invoke('window', 'printkey', array($output));
      $commands[] = ctools_modal_command_dismiss();
      $output = $commands;
      print ajax_render($output);
      drupal_exit();
    }
  }

  if (!$js) {
    return drupal_get_form('test_key_class_form', $class);
  }

  ctools_include('modal');
  ctools_include('ajax');

  $form_state = array(
    'title' => t('Print access codes for a class'),
    'ajax' => TRUE,
    'build_info' => array(
      'args' => array(
        $class,
      ),
    ),
  );

  $output = ctools_modal_form_wrapper('test_key_class_form', $form_state);
  print ajax_render($output);
  drupal_exit();
}

/**
 * Produces class access code.
 *
 * @param array $query
 *   Ldr parameters to retrieve class test assignments.
 * @param bool $js
 *   TRUE if JavaScript is supported.
 *
 * @return string
 *   Html output.
 */
function pads_tests_generate_class_keys($query, $js) {

  $test_assignments = pads_tests_get_test_assignments($query);
  $class = pads_classes_get($query['classIds']);
  $class_name = pads_classes_format_name($class);

  $exclude_statii = array(
    'Canceled',
    'Submitted',
  );
  $rows = array();

  // Create the table rows.
  foreach ($test_assignments as $test_assignment) {
    if (array_search($test_assignment['testAssignmentStatus'], $exclude_statii) === FALSE) {
      $row = '';
      // Add HR to JS (printed) version.
      if ($js) {
        $row .= '<tr><td colspan="3"><hr /></td></tr>';
      }
      $row .= '<tr>';
      $row .= "<td>{$test_assignment['studentLastName']}, {$test_assignment['studentFirstName']}</td>";
      $row .= "<td>{$test_assignment['studentStateIdentifier']}</td>";
      $row .= "<td>{$test_assignment['testKey']}</td>";
      $row .= '</tr>';
      $rows[] = $row;
    }
  }

  $rows = implode(PHP_EOL, $rows);

  // Slight difference in how we present the class and test info between JS and
  // non-JS rendering.
  if (!$js) {
    $params['@class'] = $class_name;
    $params['@test'] = $test_assignments[0]['testBatteryName'];
    $caption = t('Class Name: @class', $params) . '<br />' . t('Test Name: @test', $params);
    $caption = '<caption>' . $caption . '</caption>';
    $head = '<tr><th align="left">Last Name, First Name</th><th align="left">Student ID</th><th align="left">Access Code</th></tr>';
    $head = '<thead>' . $head . '</thead>';
  } else {
    $caption = '';
    $head = "<tr><th align=\"right\">Class Name:</th><th align=\"left\" colspan=\"2\">$class_name</th></tr>";
    $head .= "<tr><th align=\"right\">Test Name:</th><th align=\"left\" colspan=\"2\">{$test_assignments[0]['testBatteryName']}</th></tr>";
    $head .= '<tr><td colspan="3">&nbsp;</td></tr>';
    $head .= '<tr><th align="left">Last Name, First Name</th><th align="left">Student ID</th><th align="left">Access Code</th></tr>';
    $head = '<thead>' . $head . '</thead>';
  }

  $output = <<<HTML
<style type="text/css">
    table { width: 100% }
    thead { display: table-header-group }
    td { page-break-inside: avoid }
</style>
<table cellpadding="3" cellspacing="1">
    $caption
    $head
  <tbody>
    $rows
  </tbody>
</table>
HTML;

  return $output;
}

/**
 * Form to confirm print access codes for a class.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 *
 * @return array
 *   Form renderable array.
 */
function test_key_class_form($form, &$form_state) {
  $query = $form_state['build_info']['args'][0];
  $form['#attached']['js'][] = drupal_get_path('module', 'pads_tests') . '/js/printkey.js';

  $test_assignment = pads_tests_get_test_assignments($query);
  $class = pads_classes_get($query['classIds']);
  $class_name = pads_classes_format_name($class);
  $test_name = $test_assignment[0]['testBatteryName'];
  $form['class'] = array(
    '#type' => 'textfield',
    '#title' => t('Class'),
    '#default_value' => $class_name,
    '#disabled' => 'disabled',
  );

  $form['test'] = array(
    '#type' => 'fieldset',
    '#title' => t('Test Battery Info'),
    'name' => array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#default_value' => $test_name,
      '#disabled' => 'disabled',
    ),
    'subject' => array(
      '#type' => 'textfield',
      '#title' => t('Grade'),
      '#default_value' => $test_assignment[0]['testBatteryGrade'],
      '#disabled' => 'disabled',
    ),
    'grade' => array(
      '#type' => 'textfield',
      '#title' => t('Subject'),
      '#default_value' => $test_assignment[0]['testBatterySubject'],
      '#disabled' => 'disabled',
    ),
  );
  $form['print'] = array(
    '#theme' => 'link__tap_submit',
    '#path' => "tests/nojs/key/classes/{$query['classIds']}/{$query['testBatteryId']}/print",
    '#text' => t('Print access code'),
  );
  $form['print']['#options']['html'] = FALSE;
  $form['print']['#options']['attributes']['class'][] = 'use-ajax';
  $form['print']['#options']['attributes']['class'][] = 'button';

  return $form;
}

/**
 * Page callback for modal access code form.
 *
 * @param bool $js
 *   TRUE if JavaScript is enabled.
 * @param array $student
 *   Ldr student object.
 * @param int $test_assignment
 *   Ldr Test assignment object.
 * @param bool $print
 *   Indicates if the print operation is being performed.
 *
 * @return array|mixed
 *   Drupal render array or Drupal ajax commands.
 */
function test_key_student($js, $student, $test_assignment, $print = FALSE) {

  if ($print) {
    $output = pads_tests_generate_test_assignment_keys($test_assignment, $js);
    if (!$js) {
      return $output;
    }
    else {
      ctools_include('ajax');
      ctools_include('modal');
      $commands[] = ajax_command_invoke('window', 'printkey', array($output));
      $commands[] = ctools_modal_command_dismiss();
      $output = $commands;
      print ajax_render($output);
      drupal_exit();
    }
  }

  if (!$js) {
    return drupal_get_form('test_key_student_form', $student, $test_assignment);
  }

  ctools_include('modal');
  ctools_include('ajax');

  $form_state = array(
    'title' => t('Print student access code'),
    'ajax' => TRUE,
    'build_info' => array(
      'args' => array(
        $student,
        $test_assignment,
      ),
    ),
  );
  $output = ctools_modal_form_wrapper('test_key_student_form', $form_state, $student, $test_assignment);
  print ajax_render($output);
  drupal_exit();
}

/**
 * Produces individual access code.
 *
 * @param array $test_assignment
 *   Ldr test_assignment array.
 * @param bool $js
 *   TRUE if JavaScript is enabled.
 *
 * @return string
 *   Html output.
 */
function pads_tests_generate_test_assignment_keys($test_assignment, $js) {
  $rows = array();

  if (!$js) {
    $output['#theme'] = 'table';

    $rows[] = array(t('Test Name:'), $test_assignment['testBatteryName']);
    $rows[] = array(t('Student Name:'), $test_assignment['studentLastName'] . ', ' . $test_assignment['studentFirstName']);
    $rows[] = array(t('Student Id:'), $test_assignment['studentStateIdentifier']);
    $rows[] = array(t('Access Code:'), $test_assignment['testKey']);

    $output['#rows'] = $rows;

    return render($output);
  }

  $output = "<td>Test Name:</td><td>{$test_assignment['testBatteryName']}</td>";
  $output .= "<tr><td>Student Name:</td><td>{$test_assignment['studentLastName']}, {$test_assignment['studentFirstName']}</td></tr>";
  $output .= "<tr><td>Student Id:</td><td>{$test_assignment['studentStateIdentifier']}</td></tr>";
  $output .= "<tr><td>Access Code:</td><td>{$test_assignment['testKey']}</td></tr>";

  $output = '<tbody>' . $output . '</tbody>';
  $output = '<table cellpadding="3" cellspacing="1">' . $output . '</table>';
  return $output;
}

/**
 * Form to confirm access code.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 * @param array $student
 *   Ldr student object.
 * @param int $test_assignment
 *   Ldr Test assignment object.
 *
 * @return array
 *   Form renderable array.
 */
function test_key_student_form($form, &$form_state, $student, $test_assignment) {
  $form['#attached']['js'][] = drupal_get_path('module', 'pads_tests') . '/js/printkey.js';
  test_assignment_info_form($form, $form_state, $student, $test_assignment);

  $form['print'] = array(
    '#theme' => 'link__tap_submit',
    '#path' => "tests/nojs/key/student/{$test_assignment['studentRecordId']}/{$test_assignment['testAssignmentId']}/print",
    '#text' => t('Print access code'),
  );
  $form['print']['#options']['html'] = FALSE;
  $form['print']['#options']['attributes']['class'][] = 'use-ajax';
  $form['print']['#options']['attributes']['class'][] = 'button';

  return $form;
}

/**
 * Page callback test assignment operations.
 */
function pads_tests_test_op_page($js, $test_assignment, $op, $confirm = NULL) {

  switch ($op) {
    case 'cancel':
      $output = pads_tests_cancel_test_page($js, $test_assignment, $confirm);
      break;

    case 'unlock':
      $output = pads_tests_unlock_test_page($js, $test_assignment, $confirm);
      break;

    default:
      $output = NULL;
  }

  return $output;
}

/**
 * Page callback for Cancel Test operation.
 *
 * Performs cancel operation and redirects/reloads the page. Displays
 * confirmation page/CTools modal when necessary.
 */
function pads_tests_cancel_test_page($js, $test_assignment, $confirm = NULL) {
  drupal_set_title(t('Cancel Test'));

  $student = pads_ldr_client_call('getStudentRecord', array('studentRecordId' => $test_assignment['studentRecordId']));
  $args['%student'] = "{$student['firstName']} {$student['lastName']}";
  $args['%test'] = $test_assignment['testBatteryName'];

  $params = drupal_get_query_parameters();
  $destination = !empty($params['destination']) ? $params['destination'] : '';
  $output = array();

  if ($confirm != 'confirm') {
    $output['text']['#markup'] = t('Note: Please make sure that the student is logged out of the test if the status is In Progress before canceling the test. Are you sure you want to cancel %test for %student', $args);
    $output['text']['#prefix'] = '<p>';
    $output['text']['#suffix'] = '</p>';

    $output['actions']['#type'] = 'actions';

    $options = array();
    $options['html'] = FALSE;
    $options['attributes']['class'][] = 'use-ajax';
    if ($destination) {
      $options['query']['destination'] = $destination;
    }
    $output['actions']['confirm']['#theme'] = 'link__tap_submit';
    $output['actions']['confirm']['#text'] = t('Yes, cancel the test');
    $output['actions']['confirm']['#path'] = current_path() . '/confirm';
    $output['actions']['confirm']['#options'] = $options;

    $options = array();
    $options['html'] = FALSE;
    $options['attributes']['class'][] = 'ctools-close-modal';
    $output['actions']['cancel']['#theme'] = 'link__tap_cancel';
    $output['actions']['cancel']['#text'] = t('No, do not cancel the test');
    $output['actions']['cancel']['#path'] = $destination;
    $output['actions']['cancel']['#options'] = $options;
  }

  // Do the cancel.
  if ($confirm == 'confirm') {
    $params['testAssignmentId'] = $test_assignment['testAssignmentId'];
    $params['testAssignmentStatus'] = 'Canceled';
    $result = pads_ldr_client_call('updateTestAssignment', $params);
    if (empty($result['result']) || $result['result'] != 'success') {
      drupal_set_message(t("Unable to cancel student's test."), 'error');
    }
    else {
      drupal_set_message(t("%test has been canceled for %student.", $args));
    }
  }

  // Handle AJAX exchanges.
  if ($js) {
    if ($output) {
      ctools_include('modal');
      ctools_modal_render(t('Cancel Test'), drupal_render($output));
    }
    else {
      ctools_include('ajax');
      $commands[] = ctools_ajax_command_reload();
      $page = array('#type' => 'ajax', '#commands' => $commands);
      ajax_deliver($page);
    }

    drupal_exit();
  }

  // If there is markup to output, send it.
  if ($output) {
    return $output;
  }

  // Redirect.
  drupal_goto($destination);
}

/**
 * Page callback for Unlock Test operation.
 *
 * Performs unlock operation and redirects/reloads the page. Displays
 * confirmation page/CTools modal when necessary.
 */
function pads_tests_unlock_test_page($js, $test_assignment, $confirm = NULL) {
  drupal_set_title(t('Unlock Test'));

  $params = drupal_get_query_parameters();
  $destination = !empty($params['destination']) ? $params['destination'] : '';
  $output = array();

  // Produce confirmation text when needed.
  $test_factory = TestFactory::getInstance();
  if ($test_factory->isUnlockable($test_assignment) && $confirm != 'confirm') {

    $markup = variable_get('pads_tests_unlock_confirmation', pads_tests_default_unlock_confirmation());
    $output['text']['#markup'] = check_markup($markup, 'filtered_html');
    $output['text']['#prefix'] = '<p>';
    $output['text']['#suffix'] = '</p>';

    $output['actions']['#type'] = 'actions';

    $options = array();
    $options['html'] = FALSE;
    if ($js) {
      $options['attributes']['class'][] = 'use-ajax';
    }
    if ($destination) {
      $options['query']['destination'] = $destination;
    }
    $output['actions']['confirm']['#theme'] = 'link__tap_submit';
    $output['actions']['confirm']['#text'] = t('Yes, unlock test');
    $output['actions']['confirm']['#path'] = current_path() . '/confirm';
    $output['actions']['confirm']['#options'] = $options;

    $options = array();
    $options['html'] = FALSE;
    $options['attributes']['class'][] = 'ctools-close-modal';
    $output['actions']['cancel']['#theme'] = 'link__tap_cancel';
    $output['actions']['cancel']['#text'] = t('No, do not unlock test');
    $output['actions']['cancel']['#path'] = $destination;
    $output['actions']['cancel']['#options'] = $options;
  }

  // Do the unlocking.
  if (!$output) {
    $result = pads_ldr_client_call('unlockTest', array('testAssignmentId' => $test_assignment['testAssignmentId']));
    if (empty($result['result']) || $result['result'] != 'success') {
      drupal_set_message(t("Unable to unlock the student's test."), 'error');
    }
    else {
      drupal_set_message(t("The student's test was successfully unlocked."));
    }
  }

  // Handle AJAX exchanges.
  if ($js) {
    if ($output) {
      ctools_include('modal');
      ctools_modal_render(t('Unlock Test'), drupal_render($output));
    }
    else {
      ctools_include('ajax');
      $commands[] = ctools_ajax_command_reload();
      $page = array('#type' => 'ajax', '#commands' => $commands);
      ajax_deliver($page);
    }

    drupal_exit();
  }

  // If there is markup to output, send it.
  if ($output) {
    return $output;
  }

  // Redirect.
  drupal_goto($destination);
}

/**
 * Page callback for rendering a test assignment's score report.
 */
function pads_tests_score_report_page($test_assignment) {
  drupal_set_title(t('View Score Report'));
  $output = array();

  $score = pads_ldr_client_call('getEmbeddedScoreReport', array('embeddedScoreReportId' => $test_assignment['embeddedScoreReportId']));

  if (!empty($score['embeddedScoreReport'])) {
    $score = base64_decode($score['embeddedScoreReport']);
    $score = pads_tests_filter_score_report($score);
  }
  else {
    $score = NULL;
  }

  if (!$score) {
    $score = t('There was an issue retrieving the test score report.');
  }

  $output['score']['#markup'] = $score;

  ctools_include('modal');
  ctools_modal_add_js();

  return $output;
}

/**
 * Page callback for setting test assignment accommodations.
 *
 * @param array $test_assignment
 * @param boolean $js
 * @param string $accommodation
 * @param integer $value
 *
 * @return mixed
 *   String or renderable array for non-JS, AJAX commands for JS.
 */
function pads_tests_accommodations_page($test_assignment, $js, $accommodation, $value) {
  $test_assignment_id = $test_assignment['testAssignmentId'];
  $accommodations = array('enableLineReader', 'enableTextToSpeech');
  $valid = in_array($accommodation, $accommodations);
  $ldr_value = $value ? 'Y' : 'N';

  $success = FALSE;
  if ($valid) {
    $enableLineReader = NULL;
    $enableTextToSpeech = NULL;
    ${$accommodation} = $ldr_value;

    $result = TestFactory::getInstance()
      ->updateTestAssignment($test_assignment_id, NULL, $enableLineReader, $enableTextToSpeech);
    $success = !empty($result['result']) && $result['result'] == 'success';
  }

  if (!$js) {
    if (!$valid) {
      drupal_set_message(t('Invalid accommodation.'), 'error');
    } elseif (!$success) {
      drupal_set_message(t('Unable to update accommodation.'), 'error');
    } else {
      drupal_set_message(t('Accommodation updated.'));
    }

    $destination = drupal_get_destination();
    if (!$destination) {
      $destination = '<front>';
    }
    drupal_goto($destination);
  }

  // JS request.
  $commands = array();

  $selector = '.' . drupal_html_class("accommodation-checkbox-{$test_assignment_id}-{$accommodation}");
  $html = pads_tests_accommodations_link($test_assignment, $accommodation, $value);
  $commands[] = ajax_command_replace($selector, $html);
  $commands[] = array('command' => 'meow', 'message' => t('Test assignment updated'));

  libraries_load('meow');
  drupal_add_js(drupal_get_path('module', 'pads_tests') . '/js/meow.js');

  print ajax_render($commands);
  drupal_exit();
}
