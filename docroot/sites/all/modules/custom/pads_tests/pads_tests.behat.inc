<?php
/**
 * @file
 * Behat steps relating to pads_tests.
 */

use Behat\Gherkin\Node\TableNode;
use Drupal\DrupalExtension\Context\DrupalSubContextBase;
use Drupal\DrupalExtension\Context\DrupalSubContextInterface;

class TestsContext extends DrupalSubcontextBase implements DrupalSubContextInterface {

  /**
   * Calls fixStepArgument from PadsMinkContext.
   *
   * @param string $arg
   *   The arg to "fix".
   *
   * @return mixed
   *   Fixed arg.
   */
  protected function fixStepArgument($arg) {
    $padsmink_context = $this->getContext('PadsMinkContext');
    return $padsmink_context->fixStepArgument($arg);
  }

  /**
   * @Given Test Battery class assignments:
   *
   * Provide test battery assignment data in the following format:
   * | testBatteryId | classId |
   * | 1             | 123     |
   * | 2             | 124     |
   */
  public function testBatteryClassAssignments(TableNode $table) {
    foreach ($table->getHash() as $test_assignment) {
      foreach ($test_assignment as $key => $value) {
        $test_assignment[$key] = $this->fixStepArgument($value);
      }
      // Tell Ldr it's okay to trust me.
      $test_assignment['clientUserId'] = 1;
      $response = pads_ldr_client_call('postTestAssignments', $test_assignment);
      if (array_key_exists('error', $response)) {
        throw new \Exception(sprintf('Unable to create test assignments for battery %d, class %d', $test_assignment['testBatteryId'], $test_assignment['classId']));
      }
    }
  }

  /**
   * @When I set the :accommodation accommodation for :test_assignment
   */
  public function assertSetAccommodation($accommodation, $test_assignment) {
    $test_assignment = $this->getContext('PadsMinkContext')->fixStepArgument($test_assignment);

    // Interact with the first one.
    $class = drupal_html_class("accommodation-checkbox-$test_assignment-$accommodation");
    if (!$element = $this->getSession()->getPage()->find('css', "a.$class")) {
      throw new Exception(sprintf('Did not find accommodation "%s" link for "%s" on "%s".', $accommodation, $test_assignment, $this->getSession()->getCurrentUrl()));
    }

    if ($element->hasClass('accommodation-checkbox-set')) {
      throw new Exception(sprintf('Accommodation "%s" for "%s" already set on "%s".', $accommodation, $test_assignment, $this->getSession()->getCurrentUrl()));
    }

    $element->click();
  }

  /**
   * @Then the :accommodation accommodation for :test_assignment should be set
   */
  public function assertAccommodationSet($accommodation, $test_assignment) {
    $test_assignment = $this->getContext('PadsMinkContext')->fixStepArgument($test_assignment);

    // Check the status of all.
    $class = drupal_html_class("accommodation-checkbox-$test_assignment-$accommodation");
    if (!$elements = $this->getSession()->getPage()->findAll('css', "a.$class")) {
      throw new Exception(sprintf('Did not find accommodation "%s" link for "%s" on "%s".', $accommodation, $test_assignment, $this->getSession()->getCurrentUrl()));
    }

    foreach ($elements as $element) {
      /** @var \Behat\Mink\Element\NodeElement $element */
      if (!$element->hasClass('accommodation-checkbox-set')) {
        throw new Exception(sprintf('Accommodation "%s" for "%s" not set on "%s".', $accommodation, $test_assignment, $this->getSession()->getCurrentUrl()));
      }
    }
  }

  /**
   * @When I unset the :accommodation accommodation for :test_assignment
   */
  public function assertUnsetAccommodation($accommodation, $test_assignment) {
    $test_assignment = $this->getContext('PadsMinkContext')->fixStepArgument($test_assignment);

    // Interact with the first one.
    $class = drupal_html_class("accommodation-checkbox-$test_assignment-$accommodation");
    if (!$element = $this->getSession()->getPage()->find('css', "a.$class")) {
      throw new Exception(sprintf('Did not find accommodation "%s" link for "%s" on "%s".', $accommodation, $test_assignment, $this->getSession()->getCurrentUrl()));
    }

    if (!$element->hasClass('accommodation-checkbox-set')) {
      throw new Exception(sprintf('Accommodation "%s" for "%s" not set on "%s".', $accommodation, $test_assignment, $this->getSession()->getCurrentUrl()));
    }

    $element->click();
  }

  /**
   * @Then the :accommodation accommodation for :test_assignment should not be set
   */
  public function assertAccommodationNotSet($accommodation, $test_assignment) {
    $test_assignment = $this->getContext('PadsMinkContext')->fixStepArgument($test_assignment);

    // Check the status of all.
    $class = drupal_html_class("accommodation-checkbox-$test_assignment-$accommodation");
    if (!$elements = $this->getSession()->getPage()->findAll('css', "a.$class")) {
      throw new Exception(sprintf('Did not find accommodation "%s" link for "%s" on "%s".', $accommodation, $test_assignment, $this->getSession()->getCurrentUrl()));
    }

    foreach ($elements as $element) {
      /** @var \Behat\Mink\Element\NodeElement $element */
      if ($element->hasClass('accommodation-checkbox-set')) {
        throw new Exception(sprintf('Accommodation "%s" for "%s" set on "%s".', $accommodation, $test_assignment, $this->getSession()->getCurrentUrl()));
      }
    }
  }

  /**
   * @Then the :accommodation accommodation for :test_assignment should be enabled
   */
  public function assertAccommodationEnabled($accommodation, $test_assignment) {
    $test_assignment = $this->getContext('PadsMinkContext')->fixStepArgument($test_assignment);

    $class = drupal_html_class("accommodation-checkbox-$test_assignment-$accommodation");
    if (!$elements = $this->getSession()->getPage()->findAll('css', ".$class")) {
      throw new Exception(sprintf('Did not find accommodation "%s" for "%s" on "%s".', $accommodation, $test_assignment, $this->getSession()->getCurrentUrl()));
    }

    foreach ($elements as $element) {
      /** @var \Behat\Mink\Element\NodeElement $element */
      if ($element->getTagName() != 'a') {
        throw new Exception(sprintf('Accommodation "%s" for "%s" disabled on "%s".', $accommodation, $test_assignment, $this->getSession()->getCurrentUrl()));
      }
    }
  }

  /**
   * @Then the :accommodation accommodation for :test_assignment should be disabled
   */
  public function assertAccommodationDisabled($accommodation, $test_assignment) {
    $test_assignment = $this->getContext('PadsMinkContext')->fixStepArgument($test_assignment);

    $class = drupal_html_class("accommodation-checkbox-$test_assignment-$accommodation");
    if (!$elements = $this->getSession()->getPage()->findAll('css', ".$class")) {
      throw new Exception(sprintf('Did not find accommodation "%s" for "%s" on "%s".', $accommodation, $test_assignment, $this->getSession()->getCurrentUrl()));
    }

    foreach ($elements as $element) {
      /** @var \Behat\Mink\Element\NodeElement $element */
      if ($element->getTagName() == 'a') {
        throw new Exception(sprintf('Accommodation "%s" for "%s" not disabled on "%s".', $accommodation, $test_assignment, $this->getSession()->getCurrentUrl()));
      }
    }
  }

  /**
   * Ensure accommodation link is set
   *
   * This is intended for use when we do not have access to the test assignment
   * ID, likely because LDR automatically created it when the student was
   * created.
   *
   * @param string $test
   * @param string $student
   * @param string $accommodation
   *
   * @throws \Exception
   *
   * @Then I see the :test test assigned to :student has the :accommodation accommodation set
   */
  public function assertTestAccommodationSet($test, $student, $accommodation) {
    $links = $this->findAccommodationByName($test, $student, $accommodation);

    if (!$links) {
      throw new Exception(sprintf('Did not find link for accommodation "%s" for student "%s" and test "%s"', $accommodation, $student, $test));
    }

    foreach ($links as $link) {
      /** @var \Behat\Mink\Element\NodeElement $link */
      if (!$link->hasClass('accommodation-checkbox-set')) {
        $args[] = $accommodation;
        $args[] = $student;
        $args[] = $test;
        throw new Exception(sprintf('Accommodation "%s" for student "%s" and test "%s" is not set', $accommodation, $student, $test));
      }
    }
  }

  /**
   * Ensure accommodation link is not set
   *
   * This is intended for use when we do not have access to the test assignment
   * ID, likely because LDR automatically created it when the student was
   * created.
   *
   * @param string $test
   * @param string $student
   * @param string $accommodation
   *
   * @throws \Exception
   *
   * @Then I see the :test test assigned to :student has the :accommodation accommodation not set
   */
  public function assertTestAccommodationNotSet($test, $student, $accommodation) {
    $links = $this->findAccommodationByName($test, $student, $accommodation);

    if (!$links) {
      $args[] = $accommodation;
      $args[] = $student;
      $args[] = $test;
      throw new Exception(sprintf('Did not find link for accommodation "%s" for student "%s" and test "%s"', $accommodation, $student, $test));
    }

    foreach ($links as $link) {
      /** @var \Behat\Mink\Element\NodeElement $link */
      if ($link->hasClass('accommodation-checkbox-set')) {
        $args[] = $accommodation;
        $args[] = $student;
        $args[] = $test;
        throw new Exception(sprintf('Accommodation "%s" for student "%s" and test "%s" is set', $accommodation, $student, $test));
      }
    }
  }

  /**
   * Ensure accommodation link is enabled
   *
   * This is intended for use when we do not have access to the test assignment
   * ID, likely because LDR automatically created it when the student was
   * created.
   *
   * @param string $test
   * @param string $student
   * @param string $accommodation
   *
   * @throws \Exception
   *
   * @Then I see the :test test assigned to :student has the :accommodation accommodation enabled
   */
  public function assertTestAccommodationEnabled($test, $student, $accommodation) {
    $links = $this->findAccommodationByName($test, $student, $accommodation);

    if (!$links) {
      $args[] = $accommodation;
      $args[] = $student;
      $args[] = $test;
      throw new Exception(sprintf('Did not find link for accommodation "%s" for student "%s" and test "%s"', $accommodation, $student, $test));
    }

    foreach ($links as $link) {
      /** @var \Behat\Mink\Element\NodeElement $link */
      if ($link->getTagName() != 'a') {
        $args[] = $accommodation;
        $args[] = $student;
        $args[] = $test;
        throw new Exception(sprintf('Accommodation "%s" for student "%s" and test "%s" is not enabled', $accommodation, $student, $test));
      }
    }
  }

  /**
   * Ensure accommodation link is disabled
   *
   * This is intended for use when we do not have access to the test assignment
   * ID, likely because LDR automatically created it when the student was
   * created.
   *
   * @param string $test
   * @param string $student
   * @param string $accommodation
   *
   * @throws \Exception
   *
   * @Then I see the :test test assigned to :student has the :accommodation accommodation disabled
   */
  public function assertTestAccommodationDisabled($test, $student, $accommodation) {
    $links = $this->findAccommodationByName($test, $student, $accommodation);

    if (!$links) {
      $args[] = $accommodation;
      $args[] = $student;
      $args[] = $test;
      throw new Exception(sprintf('Did not find link for accommodation "%s" for student "%s" and test "%s"', $accommodation, $student, $test));
    }

    foreach ($links as $link) {
      /** @var \Behat\Mink\Element\NodeElement $link */
      if ($link->getTagName() == 'a') {
        $args[] = $accommodation;
        $args[] = $student;
        $args[] = $test;
        throw new Exception(sprintf('Accommodation "%s" for student "%s" and test "%s" is not disabled', $accommodation, $student, $test));
      }
    }
  }

  /**
   * Find accommodation link(s) by test name, student name, and accommodation.
   *
   * @param string $testName
   * @param string $studentName
   * @param string $accommodation
   *
   * @return array[NodeElement]
   */
  protected function findAccommodationByName($testName, $studentName, $accommodation) {
    $page = $this->getSession()->getPage();

    $matches = array();
    $accommodation = drupal_html_class($accommodation);
    $pattern = "/accommodation-checkbox-[0-9]*-$accommodation/";

    foreach ($page->findAll('css', 'tr') as $row) {
      /** @var \Behat\Mink\Element\NodeElement $row */
      $text = $row->getText();
      if (strpos($text, $testName) !== FALSE && strpos($text, $studentName) !== FALSE) {
        // We've got the row, find the link/text.
        foreach ($row->findAll('css', '.accommodation-checkbox') as $link) {
          /** @var \Behat\Mink\Element\NodeElement $link */
          if (preg_match($pattern, $link->getAttribute('class'))) {
            $matches[] = $link;
          }
        }
      }
    }

    return $matches;
  }

}
