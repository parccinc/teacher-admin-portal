<?php
/**
 * @file
 * Theme functions for pads_tests.
 */

/**
 * Ctools dropdown of test assignments.
 *
 * @param array $variables
 *   Theme variables.
 *
 * @return string
 *   Html output.
 */
function theme_test_key_dropdown($variables) {
  $student_id = $variables['student_id'];
  $test_assignments = $variables['test_assignments'];
  $heading = $variables['heading'];

  ctools_include('jump-menu');

  $form_state = array();
  $select = array();
  $options = array();
  if ($heading != '') {
    $options['choose'] = $heading;
  }
  foreach ($test_assignments as $test_assignment) {
    $jump_url = url("tests/nojs/key/student/$student_id/{$test_assignment['testAssignmentId']}");
    $select[$jump_url] = $test_assignment['testBatteryName'];
  }
  $form = ctools_jump_menu(array(), $form_state, $select, $options);
  return drupal_render($form);
}
