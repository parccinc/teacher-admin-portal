<?php
/**
 * @file
 * Contains \Drupal\Pads\Test\LdrTest.
 */

namespace Drupal\pads\tests;

class LdrTest implements TestInterface {

  /**
   * {@inheritdoc}
   */
  public function getTestBattery($test_battery_id) {
    return pads_ldr_client_call('getTestBattery', array(
      'testBatteryId' => $test_battery_id,
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function getTestBatteries($query) {
    return pads_ldr_client_call('getTestBatteries', $query);
  }

  /**
   * {@inheritdoc}
   */
  public function getTestBatterySubjects($test_battery_grade) {
    return pads_ldr_client_call('getTestBatterySubjects', array(
      'testBatteryGrade' => $test_battery_grade,
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function addTestAssignment($student_record_id, $test_battery_id, $timing_value, $enable_line_reader, $enable_text_to_speech) {
    return pads_ldr_client_call('postTestAssignment', array(
      'studentRecordId' => $student_record_id,
      'testBatteryId' => $test_battery_id,
      'instructionStatus' => $timing_value,
      'enableLineReader' => $enable_line_reader,
      'enableTextToSpeech' => $enable_text_to_speech,
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function updateTestAssignment($test_assignment_id, $instruction_status = NULL, $enable_line_reader = NULL, $enable_text_to_speech = NULL) {
    $options = array(
      'testAssignmentId' => $test_assignment_id,
    );
    if ($instruction_status !== NULL) {
      $options['instructionStatus'] = $instruction_status;
    }
    if ($enable_line_reader !== NULL) {
      $options['enableLineReader'] = $enable_line_reader;
    }
    if ($enable_text_to_speech !== NULL) {
      $options['enableTextToSpeech'] = $enable_text_to_speech;
    }
    return pads_ldr_client_call('updateTestAssignment', $options);
  }

  /**
   * {@inheritdoc}
   */
  public function addTestAssignments($class_id, $test_battery_id, $all_or_none = 'Y') {
    return pads_ldr_client_call('postTestAssignments', array(
      'classId' => $class_id,
      'testBatteryId' => $test_battery_id,
      'allOrNone' => $all_or_none,
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function getTestAssignment($test_assignment_id) {
    if (is_array($test_assignment_id)) {
      return $test_assignment_id;
    }
    $query = array(
      'testAssignmentId' => $test_assignment_id,
    );
    $test_assignments = $this->getTestAssignments($query);
    return !empty($test_assignments) ? reset($test_assignments) : array();
  }

  /**
   * {@inheritdoc}
   */
  public function getTestAssignments($query) {
    return pads_ldr_client_call('getTestAssignments', $query);
  }

  /**
   * {@inheritdoc}
   */
  public function hasScoreReport($test_assignment_id) {
    if ($test_assignment_id) {
      $test_assignment = $this->getTestAssignment($test_assignment_id);
      if (array_key_exists('testAssignmentStatus', $test_assignment)) {

        // Submitted and has a score report.
        if ($test_assignment['testAssignmentStatus'] == 'Submitted' && !empty($test_assignment['embeddedScoreReportId'])) {
          return TRUE;
        }

      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isCancelable($test_assignment_id) {
    if ($test_assignment_id) {
      $test_assignment = $this->getTestAssignment($test_assignment_id);
      if (array_key_exists('testAssignmentStatus', $test_assignment)) {
        $cancelable = array('Scheduled', 'InProgress', 'Paused');
        if (in_array($test_assignment['testAssignmentStatus'], $cancelable)) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isUnlockable($test_assignment_id) {
    if ($test_assignment_id) {
      $test_assignment = $this->getTestAssignment($test_assignment_id);
      if (array_key_exists('testAssignmentStatus', $test_assignment)) {
        $unlockable = array('Scheduled', 'InProgress', 'Paused');
        if (in_array($test_assignment['testAssignmentStatus'], $unlockable)) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function canUpdateAccommodations($test_assignment_id) {
    return $this->isEditable($test_assignment_id);
  }

  /**
   * {@inheritdoc}
   */
  public function isEditable($test_assignment_id) {
    if ($test_assignment_id) {
      $test_assignment = $this->getTestAssignment($test_assignment_id);
      if (array_key_exists('testAssignmentStatus', $test_assignment)) {
        $can_accommodate = array('Scheduled', 'InProgress', 'Paused');
        if (in_array($test_assignment['testAssignmentStatus'], $can_accommodate)) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

}
