<?php
/**
 * @file
 * Contains \Drupal\Pads\Test\TestInterface.
 */

namespace Drupal\pads\tests;

interface TestInterface {

  /**
   * Create test assignment.
   *
   * @param int $student_record_id
   *   Ldr student record id to assign test to.
   * @param int $test_battery_id
   *   Ldr test battery id to assign to student.
   * @param int $timing_value
   *   Ldr pre or post instruction timing.
   * @param string $enable_line_reader
   *   Ldr enableLineReader set to Y if true, not required.
   * @param string $enable_text_to_speech
   *   Ldr enableTextToSpeech set to Y if true, not required.
   *
   * @see https://breaktech.centraldesktop.com/p/aQAAAAACYtaY
   *
   * @return array
   *   Results from Ldr.
   */
  public function addTestAssignment($student_record_id, $test_battery_id, $timing_value, $enable_line_reader, $enable_text_to_speech);

  /**
   * Create test assignment.
   *
   * @param int $class_id
   *   Ldr class id.
   * @param int $test_battery_id
   *   Ldr test battery id to assign to student.
   * @param string $all_or_none
   *   Default to Y which will not assign tests to students in class if test is
   *   already assigned to one or more students in class. A value of N will
   *   assign tests to other students even if there are previous assignments
   *   in class.
   *
   * @see https://breaktech.centraldesktop.com/p/aQAAAAACZDUT
   *
   * @return array
   *   Results from Ldr.
   */
  public function addTestAssignments($class_id, $test_battery_id, $all_or_none = 'Y');

  /**
   * Get test assignments.
   *
   * @param array $query
   *   Query string parameters for Ldr test-assignment call.
   *
   * @see https://breaktech.centraldesktop.com/p/aQAAAAACY0du
   *
   * @return array
   *   Ldr output.
   */
  public function getTestAssignments($query);

  /**
   * Get test assignment by Ldr test assignment id.
   *
   * @param int|array $test_assignment_id
   *   Ldr test assignment id or test assignment array.
   *
   * @return array
   *   Ldr output.
   */
  public function getTestAssignment($test_assignment_id);

  /**
   * Update test assignment.
   *
   * @param int $test_assignment_id
   *   Ldr test assignment id.
   * @param int $instruction_status
   *   Integer representing status.
   * @param string $enable_linereader
   *   Ldr enableLineReader set to Y if true, not required.
   * @param string $enable_texttospeech
   *   Ldr enableTextToSpeech set to Y if true, not required.
   *
   * @see https://breaktech.centraldesktop.com/p/aQAAAAACfgxw
   *
   * @return array
   *   Ldr output.
   */
  public function updateTestAssignment($test_assignment_id, $instruction_status, $enable_linereader, $enable_texttospeech);

  /**
   * Get test batteries.
   *
   * See below for query string parameters and result output.
   *
   * @param array $query
   *   Query string parameters.
   *
   * @see https://breaktech.centraldesktop.com/p/aQAAAAACYLEf
   *
   * @return array
   *   Results from Ldr.
   */
  public function getTestBatteries($query);

  /**
   * Get test battery.
   *
   * @param int $test_battery_id
   *   Ldr test battery id.
   *
   * @see https://breaktech.centraldesktop.com/p/aQAAAAACYLEg
   *
   * @return array
   *   Results from Ldr.
   */
  public function getTestBattery($test_battery_id);

  /**
   * Get test battery subjects.
   *
   * @param string $test_battery_grade
   *   Ldr test battery grade.
   *
   * @see https://breaktech.centraldesktop.com/p/aQAAAAACbG8S
   *
   * @return array
   *   Results from Ldr.
   */
  public function getTestBatterySubjects($test_battery_grade);

  /**
   * Checks if test has a score report.
   *
   * @param int|array $test_assignment_id
   *   Ldr test assignment id or test assignment array.
   *
   * @return bool
   *   TRUE if test has a score report, FALSE otherwise.
   */
  public function hasScoreReport($test_assignment_id);

  /**
   * Checks if test can be canceled.
   *
   * @param int|array $test_assignment_id
   *   Ldr test assignment id or test assignment array.
   *
   * @return bool
   *   TRUE if test can be canceled, FALSE otherwise.
   */
  public function isCancelable($test_assignment_id);

  /**
   * Checks if test can be unlocked.
   *
   * @param int|array $test_assignment_id
   *   Ldr test assignment id or test assignment array.
   *
   * @return bool
   *   TRUE if test can be canceled, FALSE otherwise.
   */
  public function isUnlockable($test_assignment_id);

  /**
   * Checks if a test assignment can have accommodations modified.
   *
   * @param int|array $test_assignment_id
   *   Ldr test assignment id or test assignment array.
   *
   * @return bool
   *   TRUE if accommodations can be modified, FALSE otherwise.
   */
  public function canUpdateAccommodations($test_assignment_id);

  /**
   * Checks if a test assignment may be edited.
   *
   * @param int|array $test_assignment_id
   *   Ldr test assignment id or test assignment array.
   *
   * @return bool
   *   TRUE if test may be edited, FALSE otherwise.
   */
  public function isEditable($test_assignment_id);

}
