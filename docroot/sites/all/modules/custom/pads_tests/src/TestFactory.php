<?php
/**
 * @file
 * Contains \Drupal\Pads\Test\TestFactory.
 */

namespace Drupal\pads\tests;

use Drupal\isbe\tests\IsbeLdrTest;

/**
 * Class TestFactory.
 *
 * @package Drupal\pads\tests
 */
class TestFactory {

  /**
   * Get LdrTest instance.
   *
   * @return LdrTest
   *   LdrTest instance.
   */
  public static function getInstance() {
    $instance = &drupal_static(__CLASS__ . '.' . __FUNCTION__);
    if (!$instance) {
      switch (pads_get_vendor()) {
        case 'isbe':
          $instance = new IsbeLdrTest();
          break;

        default:
          $instance = new LdrTest();

      }

    }
    return $instance;
  }

}
