# README

This module provides an endpoint for a webservice which may be utilized by ISBE
to perform rostering activities, ie. adding students and classes.

## Contents

  - Endpoint
  - Authentication
  - Data format
  - Queue processing and confirmation
  - Logging

## Endpoint

The webservice may be queried via a POST request to `isbe/rostering`.

Requests to the webservice need to be authenticated and provide data in an 
acceptable format in order to be processed. See below for more information.

After a successful request, a HTTP response with code 200 and a message of "ok"
will be issued.

## Authentication

Requests to the rostering endpoint must be made using valid credentials for a
user of the system with the ISBE Services role.
 
Authentication is performed by providing an `Authorization` header in the 
request, with a value computed by base64 encoding a string containing the 
username and password separated by a colon.

**Example:**

```
$auth = "$username:$password";
$auth = base64_encode($auth);
```

If the data does not meet these specifications, a HTTP response with code 403 
and a description of the error will be issued.

## Data format

The endpoint accepts data as a JSON encoded payload of a POST request. The data
must contain the following data in the specified formats:
 
 - Payload must JSON decode to a valid object.
 - Payload must contain a numeric batchId attribute, a unique numeric value to 
   allow the consumer of the webservice to track the state of the request.
 - Payload may contain a 'class' attribute. If present it must be of type array
   and contain zero or more objects containing class data. All class data will 
   be accepted by the webservice and invalid class data will be reported during
   confirmation.
 - Payload may contain a 'student' attribute. If present it must be of type 
   array and contain zero or more objects containing student data. All student 
   data will be accepted by the webservice and invalid student data will be 
   reported during confirmation.

If the data does not meet these specifications, a HTTP response with code 400 
and a description of the error will be issued.

## Queue processing

When the initial rostering webservice request is made, the data is parsed and
queued for processing at a later time.

By default the queue will be processed for up to 15 seconds each time core cron 
is invoked. This value may be changed by adjusting the 
`pads_isbe_services_rostering_queue_time` Drupal variable.

Alternatively the queue may be processed via drush:

`drush roster-queue --time=45`

The time paramater is optional and defaults to the value configured for 
`pads_isbe_services_rostering_queue_time`. A crontab triggered command running
every minute and processing for ~45 seconds is recommended.

## Confirmation

The file operation queued for each request sends a confirmation of the 
processing to ISBE. This data is essentially identical to the original request 
and each class and student contains a `recordStatusCode` attribute to indicate 
success (1) or failure (2).

## Logging

A logging mechanism is available to capture results of the rostering webservice.
The level of logging may be set by manipulating the 
`pads_isbe_services_log_level` Drupal variable. This is a bitwise mask of the
following options:

 - 0001 Enable logging
 - 0010 Log all results
 - 0100 Log detailed information
 - 1000 Log confirmation data
