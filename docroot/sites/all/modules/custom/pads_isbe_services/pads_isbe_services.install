<?php
/**
 * @file
 * Pads ISBE Services installation code.
 */

/**
 * Implements hook_schema().
 */
function pads_isbe_services_schema() {
  $schema['pads_isbe_rostering'] = array(
    'description' => 'ISBE rostering data.',
    'fields' => array(
      'batch_id' => array(
        'description' => 'Batch ID.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'batch_increment' => array(
        'description' => 'Batch Increment.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'line' => array(
        'description' => 'Line number.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'type' => array(
        'description' => 'Type of ISBE rostering data.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
      'data' => array(
        'description' => 'The raw line data.',
        'type' => 'blob',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('batch_id', 'batch_increment', 'type', 'line'),
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function pads_isbe_services_install() {
  _roles_and_permissions();
}

/**
 * Implements hook_uninstall().
 */
function pads_isbe_services_uninstall() {
  variable_del('pads_isbe_services_3des_enabled');
  variable_del('pads_isbe_services_validate_student');
}

/**
 * Add batch_increment handling to PADS ISBE Services rostering.
 */
function pads_isbe_services_update_7000() {
  $spec = array(
    'description' => 'Batch Increment.',
    'type' => 'int',
    'unsigned' => TRUE,
    'not null' => TRUE,
  );
  db_add_field('pads_isbe_rostering', 'batch_increment', $spec);

  db_drop_primary_key('pads_isbe_rostering');
  db_add_primary_key('pads_isbe_rostering', array(
    'batch_id',
    'batch_increment',
    'type',
    'line',
  ));
}

/**
 * Create ISBE Service role if not exist and assign permissions.
 */
function pads_isbe_services_update_7001() {
  _roles_and_permissions();
}

/**
 * Create Role with permissions for service user.
 */
function _roles_and_permissions() {
  // Verify role does not exist.
  $role = user_role_load_by_name(PADS_ISBE_SERVICES_ROLE_NAME);
  if (!$role) {
    // Create new role.
    $role = new stdClass();
    $role->name = PADS_ISBE_SERVICES_ROLE_NAME;
    user_role_save($role);
  }
  // Add permissions to role.
  $permissions = array(
    'view organizations',
    'administer students',
    'add student to any class',
    'access isbe services',
  );
  user_role_grant_permissions($role->rid, $permissions);
}
