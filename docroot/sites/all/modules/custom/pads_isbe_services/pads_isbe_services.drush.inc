<?php

/**
 * @file
 * Pads ISBE Services Drush integration.
 */

/**
 * Implements hook_drush_command().
 */
function pads_isbe_services_drush_command() {
  $items['pads-isbe-services-process-rostering'] = array(
    'description' => 'Process queued ISBE rostering operations.',
    'options' => array(
      'time' => 'Time to spend processing the queue.',
    ),
    'aliases' => array('roster-queue'),
  );

  $items['pads-isbe-services-roster-user'] = array(
    'description' => 'Create user with ISBE Service role.',
    'options' => array(
      'name' => 'User name, defaults to isbe_user.',
      'pass' => 'User password.',
    ),
    'aliases' => array('roster-user'),
  );

  return $items;
}

/**
 * Drush command callback for processing ISBE rostering queue.
 */
function drush_pads_isbe_services_process_rostering() {
  $queue_name = 'pads_isbe_services_rostering';

  $time = drush_get_option('time');
  if (!$time) {
    $time = variable_get('pads_isbe_services_rostering_queue_time', PADS_ISBE_SERVICES_QUEUE_PROCESS_DEFAULT);
  }

  if (!is_numeric($time) || $time <= 0) {
    drush_log(dt('Queue processing time "@time" is not valid.', array('@time' => $time)), 'error');
    return;
  }

  $queue_info = module_invoke_all('cron_queue_info');
  drupal_alter('cron_queue_info', $queue_info);

  if (empty($queue_info[$queue_name])) {
    drush_log(dt('Unable to locate queue "@queue".', array('@queue' => $queue_name)), 'error');
    return;
  }

  $info = $queue_info[$queue_name];
  $callback = $info['worker callback'];

  /** @var DrupalQueueInterface $queue */
  $queue = DrupalQueue::get($queue_name);
  $queue->createQueue();

  drush_log(dt('Processing queue.'), 'ok');

  timer_start(__FUNCTION__);
  $timer_val = 0;
  $items = 0;
  while (($item = $queue->claimItem()) && $timer_val < $time) {
    try {
      $items++;
      call_user_func($callback, $item->data);
      $queue->deleteItem($item);
    }
    catch (Exception $e) {
      // No op.
    }
    $timer_val = timer_read(__FUNCTION__) / 1000;
  }

  $params['!items'] = $items;
  $params['!timer_val'] = $timer_val;
  $params['!remain'] = $queue->numberOfItems();
  drush_log(dt('Processed !items items in !timer_val seconds, !remain items remaining.', $params), 'ok');
}

/**
 * Drush command callback for roster-user.
 *
 * @throws \Exception
 */
function drush_pads_isbe_services_roster_user() {
  $name = drush_get_option('name');
  if (!$name) {
    $name = 'isbe_user';
  }
  $pass = drush_get_option('pass');
  if (!$pass) {
    $pass = user_password();
  }
  if (!user_load_by_mail($name)) {
    // Create user for ISBE Services.
    $edit = array();
    $edit['name'] = $name;
    $edit['pass'] = $pass;
    $edit['status'] = 1;

    // Add ISBE Services to role.
    $role = user_role_load_by_name(PADS_ISBE_SERVICES_ROLE_NAME);
    if ($role && $rid = $role->rid) {
      $edit['roles'] = array($rid => $rid);
    }
    else {
      drush_log(dt('Unable to create !role role.', array(
        '!role' => PADS_ISBE_SERVICES_ROLE_NAME,
      )), 'warning');
    }

    $account = user_save(drupal_anonymous_user(), $edit);
    if (!$account) {
      drush_log(dt('Unable to create user with mail !mail.', array(
        '!mail' => $name,
      )), 'warning');
    }
    else {
      // Assign user to Org.
      pads_org_assign_orgs_to_user('1', $account);

      // Output authorization for service call.
      $authorization = "$name:$pass";
      $iv_size = mcrypt_get_iv_size(MCRYPT_3DES, MCRYPT_MODE_CBC);
      $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
      $key = variable_get('pads_isbe_client_3des_key', '');
      $token = base64_encode($iv . mcrypt_encrypt(MCRYPT_3DES, $key, $authorization, MCRYPT_MODE_CBC, $iv));
      drush_log("Authorization: $token", "ok");
    }
  }
}
