<?php

/**
 * @file
 * Pads ISBE Services page callbacks.
 */

/**
 * Callback for rostering endpoint.
 */
function pads_isbe_services_roster_endpoint() {
  $response = new stdClass();

  if (!$account = pads_isbe_services_valid_auth_token()) {
    $response->code = 403;
    return $response;
  }

  // Verify data is legit?
  $data = file_get_contents("php://input");

  $data = json_decode($data);

  if ($data === NULL) {
    $response->code = 400;
    $response->description = 'Unable to decode contents.';
    return $response;
  }

  if (!is_object($data)) {
    $response->code = 400;
    $response->description = 'JSON contents invalid.';
    return $response;
  }

  if (!isset($data->batchId) || !is_int($data->batchId) || $data->batchId <= 0) {
    $response->code = 400;
    $response->description = 'batchId attribute invalid.';
    return $response;
  }

  if (!isset($data->batchIncrement)) {
    $data->batchIncrement = 0;
  }

  if (!is_int($data->batchIncrement) || $data->batchIncrement < 0) {
    $response->code = 400;
    $response->description = 'batchIncrement attribute invalid.';
    return $response;
  }

  /** @var DrupalQueueInterface $queue */
  $queue = DrupalQueue::get('pads_isbe_services_rostering');

  $line = 1;
  // Queue classes for creation.
  if (!empty($data->class)) {
    if (!is_array($data->class)) {
      $response->code = 400;
      $response->description = 'Class data invalid.';
    }
    foreach ($data->class as $item) {
      $queue_item['batch_id'] = $data->batchId;
      $queue_item['batch_increment'] = $data->batchIncrement;
      $queue_item['type'] = 'class';
      $queue_item['line'] = $line++;
      $queue_item['uid'] = $account->uid;
      $queue_item['data'] = $item;
      $queue->createItem($queue_item);
    }
  }

  $line = 1;
  $students = array();
  // Queue students for creation.
  if (!empty($data->student)) {
    if (!is_array($data->student)) {
      $response->code = 400;
      $response->description = 'Student data invalid.';
    }
    foreach ($data->student as $item) {
      $queue_item['batch_id'] = $data->batchId;
      $queue_item['batch_increment'] = $data->batchIncrement;
      $queue_item['type'] = 'student';
      $queue_item['uid'] = $account->uid;

      // Student add, make a list and batch it later on.
      if (isset($item->actionCode) && ($item->actionCode == 1)) {
        $students[$line++] = $item;
      } else {
        // Non-add operations.
        $queue_item['line'] = $line++;
        $queue_item['data'] = $item;
        $queue->createItem($queue_item);
      }
    }
  }
  // Batch the student adds.
  $keys = array_keys($students);
  $batch_size = variable_get('isbe_services_rostering_student_add_batch', 100);
  while (!empty($students)) {
    $queue_item['data'] = array();
    $queue_item['data']['actionCode'] = 1;
    $queue_item['data']['students'] = array();
    while (count($queue_item['data']['students']) < $batch_size && !empty($students)) {
      $queue_item['data']['students'][array_shift($keys)] = array_shift($students);
    }
    $queue->createItem($queue_item);
  }

  $queue_item = array();

  // Add confirmation as final item in queued series.
  $queue_item['type'] = 'confirm';
  $queue_item['uid'] = $account->uid;
  $queue_item['data']['batch_id'] = $data->batchId;
  $queue_item['data']['batch_increment'] = $data->batchIncrement;
  $queue->createItem($queue_item);

  $response->code = 200;
  $response->message = 'ok';

  return $response;
}
