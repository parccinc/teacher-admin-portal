<?php
/**
 * @file
 * Contains /PadsIsbeServicesContext.
 */

use Behat\Gherkin\Node\PyStringNode;
use Behat\Mink\Exception\UnsupportedDriverActionException;
use Drupal\DrupalExtension\Context\DrupalSubContextBase;
use Drupal\pads_organization\OrganizationFactory;

/**
 * Class PadsIsbeServicesContext.
 */
class PadsIsbeServicesContext extends DrupalSubContextBase {

  const SERVICE_ROLE = 'ISBE Services';
  protected $token;
  protected $users = array();
  protected $vars = array();

  /**
   * Creates a user and sets the access token for the test suite.
   *
   * @Given I am using the ISBE services authorization token for :name
   *
   * @param $name
   *   User's name.
   */
  public function assertValidToken($name) {
    if (!isset($this->users[$name])) {
      $user = new stdClass();
      $user->name = $name;
      $user->pass = $this->getRandom()->name();
      $this->userCreate($user);
      $this->getDriver()->userAddRole($user, $this::SERVICE_ROLE);
      $this->users[$name] = $user;

      // Add org.
      foreach (OrganizationFactory::getInstance()->getOrgChildren() as $state) {
        if ($state['organizationName'] == 'XX') {
          pads_org_assign_orgs_to_user($state['organizationId'], $user);
        }
      }

    }

    $authorization = "$name:{$this->users[$name]->pass}";
    if (variable_get('pads_isbe_services_3des_enabled', TRUE)) {
      $iv_size = mcrypt_get_iv_size(MCRYPT_3DES, MCRYPT_MODE_CBC);
      $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
      $key = variable_get('pads_isbe_client_3des_key', '');
      $this->token = base64_encode($iv . mcrypt_encrypt(MCRYPT_3DES, $key, $authorization, MCRYPT_MODE_CBC, $iv));
    }
    else {
      $this->token = base64_encode($authorization);
    }
  }

  /**
   * Remove users after each scenario.
   *
   * @AfterContext
   */
  public function cleanUp() {
    foreach ($this->users as $user) {
      $this->getDriver()->userDelete($user);
    }
  }

  /**
   * Call the rostering endpoint.
   *
   * @Given I call the ISBE rostering endpoint with:
   *
   * @param PyStringNode $data
   */
  public function assertRosteringData(PyStringNode $data) {
    /** @var PadsMinkContext $mink */
    $mink = $this->getContext('PadsMinkContext');
    $data = $mink->fixStepArgument($data->getRaw());
    $this->request('POST', '/isbe/rostering', $data);
  }

  /**
   * Perform a request.
   *
   * @param string $type
   *   POST or GET.
   * @param $url
   *   The path/url of the request.
   * @param null $data
   *   Data to include in the request body.
   *
   * @throws \Behat\Mink\Exception\UnsupportedDriverActionException
   */
  public function request($type, $url, $data = NULL) {
    $driver = $this->getSession()->getDriver();

    if (!method_exists($driver, 'getClient')) {
      throw new UnsupportedDriverActionException('No "getClient" method available on driver in %s.', $driver);
    }

    $server = array();
    /** @var \Goutte\Client $client */
    $client = $driver->getClient();
    if ($this->token) {
      $client->setHeader('Authorization', $this->token);
    }
    $client->request($type, $url, array(), array(), $server, $data);
  }

  /**
   * @BeforeScenario
   */
  public function clearRosteringLogs() {
    db_query('TRUNCATE TABLE {pads_isbe_rostering}');
    $dispatch = &drupal_static('pads_isbe_services_rostering_confirm_dispatch');
    $dispatch = FALSE;

    file_unmanaged_delete_recursive('temporary://pads-isbe-batch');
  }

  /**
   * @AfterScenario
   */
  public function resetDispatch() {
    drupal_static_reset('pads_isbe_services_rostering_confirm_dispatch');
  }

  /**
   * @Then the ISBE rostering confirmation message for batch :batchId should be:
   * @Then the ISBE rostering confirmation message for batch :batchId increment :batchIncrement should be:
   *
   * @param int $batchId
   * @param \Behat\Gherkin\Node\PyStringNode $data
   * @throws \Exception
   */
  public function assertConfirmation($batchId, PyStringNode $data, $batchIncrement = 0) {
    $confirmation = pads_isbe_services_rostering_confirm($batchId, $batchIncrement);
    $confirmation = trim(json_encode($confirmation, JSON_PRETTY_PRINT));

    // Normalize the data from the test step.
    $data = trim(json_encode(json_decode($data->getRaw()), JSON_PRETTY_PRINT));

    // Uncomment to dump these to files.
    //file_put_contents('conf.txt', $confirmation);
    //file_put_contents('data.txt', $data);

    if ($confirmation != $data) {
      throw new Exception(sprintf('ISBE rostering confirmation not as expected. Found %s', $confirmation));
    }
  }

  /**
   * @Then the ISBE rostering confirmation for student :stateIdentifier in batch :batchId should have a recordStatusCode of :code
   * @Then the ISBE rostering confirmation for student :stateIdentifier in batch :batchId increment :batchIncrement should have a recordStatusCode of :code
   *
   * @param string $stateIdentifier
   * @param int $batchId
   * @param int $code
   * @param int $batchIncrement
   *
   * @throws \Exception
   */
  public function assertRosterConfirmationStudentStatus($stateIdentifier, $batchId, $code, $batchIncrement = 0) {
    $confirmation = $this->findStudentConfirmation($batchId, $batchIncrement, $stateIdentifier);

    if ($confirmation->recordStatusCode != $code) {
      throw new Exception(sprintf('Student confirmation recordStatusCode for "%s" is "%s", expected "%s".', $stateIdentifier, $confirmation->recordStatusCode, $code));
    }
  }

  /**
   * @Then the ISBE rostering confirmation for student :stateIdentifier in batch :batchId should have a recordStatusDescription of :desc
   * @Then the ISBE rostering confirmation for student :stateIdentifier in batch :batchId increment :batchIncrement should have a recordStatusDescription of :desc
   *
   * @param string $stateIdentifier
   * @param int $batchId
   * @param int $desc
   * @param int $batchIncrement
   *
   * @throws \Exception
   */
  public function assertRosterConfirmationStudentDesc($stateIdentifier, $batchId, $desc, $batchIncrement = 0) {
    /** @var PadsMinkContext $mink */
    $mink = $this->getContext('PadsMinkContext');
    $desc = $mink->fixStepArgument($desc);

    $confirmation = $this->findStudentConfirmation($batchId, $batchIncrement, $stateIdentifier);

    if ($confirmation->recordStatusDescription != $desc) {
      throw new Exception(sprintf('Student confirmation recordStatusDescription for "%s" is "%s", expected "%s".', $stateIdentifier, $confirmation->recordStatusDescription, $desc));
    }
  }

  /**
   * Extracts a student's confirmation from an entire rostering confirmation.
   *
   * @param $batchId
   * @param int $batchIncrement
   * @param $stateIdentifier
   *
   * @return stdClass
   * @throws \Exception
   */
  protected function findStudentConfirmation($batchId, $batchIncrement, $stateIdentifier) {
    $confirmation = pads_isbe_services_rostering_confirm($batchId, $batchIncrement);

    if (empty($confirmation->student)) {
      throw new Exception('No student confirmation data.');
    }

    foreach ($confirmation->student as $student) {
      if ($student->stateIdentifier == $stateIdentifier) {
        return $student;
      }
    }

    throw new Exception(sprintf('Student confirmation for "%s" not found.', $stateIdentifier));
  }

  /**
   * Find an ISBE BTAddTestingStudents request for a student_id.
   *
   * Request must have occurred with pads_isbe_services_debug enabled.
   *
   * @Then an ISBE BTAddTestingStudents request for :student_id was made
   *
   * @param string $student_id
   *
   * @return stdClass
   *
   * @throws \Exception
   */
  public function assertStudentBatch($student_id) {
    $directory = 'temporary://pads-isbe-batch';
    file_prepare_directory($directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
    $filename = $directory . "/$student_id";
    if (!is_readable($filename)) {
      throw new Exception(sprintf('No ISBE BTAddTestingStudents request for "%s".', $student_id));
    }
    return json_decode(file_get_contents($filename));
  }

  /**
   * @Then the ISBE BTAddTestingStudents request for :student_id contains :param of :value
   *
   * @param $student_id
   * @param $param
   * @param $value
   *
   * @throws \Exception
   */
  public function assertStudentBatchContains($student_id, $param, $value) {
    /** @var PadsMinkContext $mink */
    $mink = $this->getContext('PadsMinkContext');
    $value = $mink->fixStepArgument($value);

    $request = $this->assertStudentBatch($student_id);
    $batch = drupal_json_decode($request->batch);
    if (!isset($batch['student'][0][$param])) {
      throw new Exception(sprintf('ISBE BTAddTestingStudents request for "%s" does not contain parameter "%s".', $student_id, $param));
    }

    if ($batch['student'][0][$param] != $value) {
      throw new Exception(sprintf('ISBE BTAddTestingStudents request for "%s" parameter "%s" is "%s", expected "%s".', $student_id, $param, $request['batch'][$param], $value));
    }
  }

  /**
   * @When ISBE validate student is :status
   */
  public function setValidateStudent($status) {
    if (!isset($this->vars['pads_isbe_services_validate_student'])) {
      $this->vars['pads_isbe_services_validate_student'] = variable_get('pads_isbe_services_validate_student');
    }

    if ($status == 'enabled') {
      variable_set('pads_isbe_services_validate_student', TRUE);
    }
    else {
      variable_del('pads_isbe_services_validate_student');
    }
  }

  /**
   * @Given ISBE services debug is :status
   */
  public function setDebug($status) {
    if (!isset($this->vars['pads_isbe_services_debug'])) {
      $this->vars['pads_isbe_services_debug'] = variable_get('pads_isbe_services_debug');
    }

    if ($status == 'enabled') {
      variable_set('pads_isbe_services_debug', TRUE);
    }
    else {
      variable_del('pads_isbe_services_debug');
    }
  }

  /**
   * @AfterScenario
   */
  public function reset() {
    foreach ($this->vars as $name => $value) {
      if ($value === NULL) {
        variable_del($name);
      }
      else {
        variable_set($name, $value);
      }
    }

    $this->vars = array();
  }

}
