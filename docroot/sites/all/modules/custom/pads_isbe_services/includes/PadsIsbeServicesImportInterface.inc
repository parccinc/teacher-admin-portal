<?php
/**
 * @file
 * Contains PadsIsbeServicesImportInterface interface.
 */

/**
 * Interface PadsIsbeServicesImportInterface.
 */
interface PadsIsbeServicesImportInterface {

  /**
   * Import data.
   *
   * @param array $data
   *   An array of data to import.
   *
   * @return \stdClass
   *   The record object created by the import.
   */
  public function import($data);


  /**
   * Remove data.
   *
   * @param array $data
   *   An array of data identifying the thing to remove.
   *
   * @return \stdClass
   *   Data pertaining to the removal.
   */
  public function remove($data);

}
