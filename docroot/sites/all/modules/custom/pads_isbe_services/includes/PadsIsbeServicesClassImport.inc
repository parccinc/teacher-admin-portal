<?php
/**
 * @file
 * Contains /PadsIsbeServicesClassImport.
 */

/**
 * Class PadsIsbeServicesClassImport.
 */
class PadsIsbeServicesClassImport extends PadsImportClassFile implements PadsIsbeServicesImportInterface {

  /**
   * {@inheritdoc}
   */
  public function import($data) {
    $data['line'][0] = $data['organizationIdentifier'];
    $data['line'][1] = $data['classIdentifier'];
    $data['line'][2] = $data['gradeLevel'];
    $data['line'][3] = $data['sectionNumber'];
    $data['line_no'] = 1;

    $record = $this->parseLine($data);
    $this->validateRecord($record, $validate);
    $this->generateEntity($record);

    return $record;
  }

  /**
   * {@inheritdoc}
   */
  public function remove($data) {
    // TODO: implement this method.
  }

}
