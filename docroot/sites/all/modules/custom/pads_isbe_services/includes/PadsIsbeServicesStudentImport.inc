<?php
/**
 * @file
 * Contains \PadsIsbeServicesStudentImport.
 */

/**
 * Class PadsIsbeServicesStudentImport.
 */
class PadsIsbeServicesStudentImport extends PadsImportStudentFile implements PadsIsbeServicesImportInterface {

  /**
   * {@inheritdoc}
   */
  public function import($data) {
    $records = array();

    foreach ($data as $line_no => $row) {
      $row = (array) $row;
      $row['line'] = array();
      foreach ($this->headers as $pos => $name) {
        if (isset($row[$name])) {
          $row['line'][$pos] = $row[$name];
        }
      }
      $row['line_no'] = $line_no;

      $record = $this->parseLine($row);
      $record->errors = array();
      $this->validateRecord($record, $validate);
      $records[$line_no] = $record;
    }

    $this->validateRecords($records, $validate);

    return $records;
  }

  /**
   * {@inheritdoc}
   */
  public function remove($data) {
    $record = new stdClass();

    // Find the state.
    if (!$state_org = $this->userStateOrg()) {
      $record->errors['no_state'] = 'Unable to determine state organization for service user.';
      return $record;
    }

    // Find the enrolled org.
    if (!$org_id = pads_org_get_id($state_org['organizationName'], $data['enrollSchoolIdentifier'])) {
      $record->errors['no_org'] = 'Unable to determine enrolled organization.';
      return $record;
    }

    // Get all students for the org, find the matching student.
    $students = pads_students_get($org_id);
    if (empty($students['studentRecords'])) {
      $students['studentRecords'] = array();
    }

    // Attempt to look up studentRecordId from enrollSchoolIdentifier and
    // stateIdentifier parameters.
    $student_record_id = NULL;
    foreach ($students['studentRecords'] as $student_record) {
      if ($student_record['stateIdentifier'] == $data['stateIdentifier']) {
        // Get eeem.
        $student_record_id = $student_record['studentRecordId'];
        break;
      }
    }

    if (!$student_record_id) {
      $record->errors['no_student'] = sprintf('Student record not found for stateIdentifier %s', $data['stateIdentifier']);
      return $record;
    }

    // Check for test assignments.
    $validate_student = variable_get('pads_isbe_services_validate_student', FALSE);
    if ($validate_student) {
      $test_assignments = pads_tests_get_test_assignments(array('studentRecordId' => $student_record_id));
      if ($test = reset($test_assignments)) {
        $record->errors['assigned_tests'] = sprintf('Student record %s already has an active test assignment for test battery %s', $data['stateIdentifier'], $test['testBatteryId']);
        return $record;
      }
      // Remove class assignments if found.
      $classes = pads_students_get_classes($student_record_id);
      if (!empty($classes['error'])) {
        $classes = array();
      }
      foreach ($classes as $class) {
        pads_classes_remove_students($class['classId'], array($student_record_id));
      }
    }

    $result = pads_students_delete($student_record_id);
    if (!empty($result['error'])) {
      $record->errors[$result['error']] = $result['detail'];
    }

    return $record;
  }

}
