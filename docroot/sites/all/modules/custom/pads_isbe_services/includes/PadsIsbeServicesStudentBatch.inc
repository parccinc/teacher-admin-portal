<?php
/**
 * @file
 * Contains \PadsIsbeServicesStudentBatch.
 */

/**
 * Class PadsIsbeServicesStudentBatch.
 */
class PadsIsbeServicesStudentBatch {

  /** @var array $student */
  protected $student;

  /**
   * PadsIsbeServicesStudentBatch constructor.
   *
   * @param array $student
   */
  public function __construct($student = array()) {
    $org_id = array_key_exists('enrollOrgId', $student) ?
      $student['enrollOrgId'] : '';
    $this->student = $this->defaultStudent($org_id);
    if ($student) {
      $this->setValues($student);
    }
  }

  /**
   * Set values onto the student batch record.
   *
   * @param string $name
   * @param string $value
   */
  public function setValue($name, $value) {
    if (array_key_exists($name, $this->student)) {
      $this->student[$name] = $value;
    }
  }

  /**
   * Set values onto the student batch record.
   *
   * @param array $values
   */
  public function setValues($values) {
    foreach ($values as $name => $value) {
      $this->setValue($name, $value);
    }
  }

  /**
   * Get the student batch values.
   *
   * @return array
   */
  public function getValues() {
    return $this->student;
  }

  /**
   * Submit an individual student batch record.
   */
  public function submit() {
    $batch = new stdClass();
    $batch->batchId = 0;
    $batch->batchIncrement = 0;
    $batch->class = array();
    $batch->student = array(
      (object) $this->getValues(),
    );

    $args['batch'] = drupal_json_encode($batch);
    $success = FALSE;
    if (variable_get('pads_isbe_services_debug')) {
      $student_id = $this->student['stateIdentifier'];
      $filename = 'temporary://pads-isbe-batch/' . $student_id;
      $dir = dirname($filename);
      file_prepare_directory($dir, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      file_unmanaged_save_data(json_encode($args), $filename, FILE_EXISTS_REPLACE);
    }
    else {
      $token = pads_isbe_client_get_token('Bearer ');
      $args['Authorization'] = $token;
      try {
        pads_isbe_client_call('bTAddTestingStudents', $args);
        $success = TRUE;
      }
      catch (Exception $e) {
        // No op.
      }
    }

    $level = variable_get('pads_isbe_services_log_level', PADS_ISBE_SERVICES_LOG_FAILURE);
    if (!$success || ($level && PADS_ISBE_SERVICES_LOG_ALL)) {
      $message = '<pre>' . json_encode($args, JSON_PRETTY_PRINT) . '</pre>';
      watchdog('pads_isbe_student_submit', $message);
    }
  }

  /**
   * Default student values.
   *
   * @return array
   */
  public static function defaultStudent($org_id = '') {
    $enroll_school_identifier = '';
    if (!empty($org_id) && $org = pads_org_load($org_id)) {
      $enroll_school_identifier = $org['organizationIdentifier'];
    }
    $student = array(
      'enrollOrgId' => $org_id,
      'enrollSchoolIdentifier' => $enroll_school_identifier,
      'stateIdentifier' => '',
      'lastName' => '',
      'firstName' => '',
      'middleName' => '',
      'dateOfBirth' => '',
      'gender' => '',
      'gradeLevel' => '',
      'raceAA' => '',
      'raceAN' => '',
      'raceAS' => '',
      'raceHL' => '',
      'raceHP' => '',
      'raceWH' => '',
      'statusDIS' => '',
      'statusECO' => '',
      'statusELL' => '',
      'statusGAT' => '',
      'statusLEP' => '',
      'statusMIG' => '',
      'disabilityType' => '',
      'optionalStateData1' => '',
      'optionalStateData2' => '',
      'optionalStateData3' => '',
      'optionalStateData4' => '',
      'optionalStateData5' => '',
      'optionalStateData6' => '',
      'optionalStateData7' => '',
      'optionalStateData8' => '',
      'classAssignment1' => '',
      'classAssignment2' => '',
      'classAssignment3' => '',
      'classAssignment4' => '',
      'classAssignment5' => '',
      'classAssignment6' => '',
      'classAssignment7' => '',
      'classAssignment8' => '',
      'classAssignment9' => '',
      'classAssignment10' => '',
      'actionCode' => 1,
      'recordStatusCode' => 1,
    );

    return $student;
  }

}
