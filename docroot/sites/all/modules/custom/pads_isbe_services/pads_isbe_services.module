<?php
/**
 * @file
 * Pads ISBE Services module code.
 */

// Core queue processing time.
use Drupal\pads_organization\OrganizationFactory;

define('PADS_ISBE_SERVICES_QUEUE_PROCESS_DEFAULT', 15);

// No logging.
define('PADS_ISBE_SERVICES_LOG_DISABLED', 0b0000);

// Log failures.
define('PADS_ISBE_SERVICES_LOG_FAILURE', 0b0001);

// Log all results.
define('PADS_ISBE_SERVICES_LOG_ALL', 0b0010);

// Log detailed data and responses.
define('PADS_ISBE_SERVICES_LOG_DETAIL', 0b0100);

// Log confirmation response sent to ISBE.
define('PADS_ISBE_SERVICES_LOG_CONFIRM', 0b1000);

// User role to assign user ISBE service authenticates with.
define('PADS_ISBE_SERVICES_ROLE_NAME', 'ISBE Services');

/**
 * Implements hook_menu().
 */
function pads_isbe_services_menu() {
  $items['isbe/rostering'] = array(
    'type' => MENU_CALLBACK,
    'page callback' => 'pads_isbe_services_roster_endpoint',
    'access callback' => TRUE,
    'delivery callback' => 'pads_isbe_services_deliver_response',
    'file' => 'pads_isbe_services.pages.inc',
  );

  return $items;
}

/**
 * Determine if an access token is valid.
 *
 * @param string $token
 *   A token to analyze, optional. If not provided, the token in the request's
 *   "Authorization" header will be used.
 *
 * @return \stdClass|bool
 *   The user account if access is granted, otherwise FALSE.
 */
function pads_isbe_services_valid_auth_token($token = NULL) {
  if (!$token) {
    if (empty($_SERVER['HTTP_AUTHORIZATION'])) {
      // No token provided.
      return FALSE;
    }
    $token = $_SERVER['HTTP_AUTHORIZATION'];
  }

  if (!$data = base64_decode($token)) {
    // Unable to decode.
    return FALSE;
  }

  if (variable_get('pads_isbe_services_3des_enabled', TRUE)) {
    $iv_size = mcrypt_get_iv_size(MCRYPT_3DES, MCRYPT_MODE_CBC);
    $key = variable_get('pads_isbe_client_3des_key', '');
    $iv = substr($data, 0, $iv_size);
    $data = str_replace($iv, "", $data);
    $data = mcrypt_decrypt(MCRYPT_3DES, $key, $data, MCRYPT_MODE_CBC, $iv);
  }

  $user = explode(':', $data, 2);
  if (count($user) != 2) {
    return FALSE;
  }
  list($name, $pass) = $user;

  // Failed authentication.
  if (!$uid = user_authenticate($name, trim($pass))) {
    return FALSE;
  }

  $account = user_load($uid);
  return user_access('access isbe services', $account) ? $account : FALSE;
}

/**
 * Delivery callback for providing JSON responses.
 *
 * @param object $response
 *   Acknowledgement response.
 *
 * @throws \Exception
 */
function pads_isbe_services_deliver_response($response) {

  if (!is_object($response) || !isset($response->code)) {
    throw new Exception('Invalid response value in ' . __FUNCTION__ . '.');
  }

  switch ($response->code) {
    case 400:
      drupal_add_http_header('Status', '400 Bad Request');
      $response->message = 'Bad request';
      break;

    case 403:
      drupal_add_http_header('Status', '403 Forbidden');
      $response->message = 'Access denied';
      break;

    case 404:
      drupal_add_http_header('Status', '404 Not Found');
      $response->message = 'Not found';
      break;
  }

  drupal_json_output($response);
}

/**
 * Implements hook_permission().
 */
function pads_isbe_services_permission() {
  $perms['access isbe services'] = array(
    'title' => t('Access ISBE services'),
    'description' => t('Access the endpoints for ISBE integration.'),
  );

  return $perms;
}

/**
 * Implements hook_cron_queue_info().
 */
function pads_isbe_services_cron_queue_info() {
  $queues['pads_isbe_services_rostering'] = array(
    'worker callback' => '_pads_isbe_services_rostering_worker',
    'time' => variable_get('pads_isbe_services_rostering_queue_time', PADS_ISBE_SERVICES_QUEUE_PROCESS_DEFAULT),
  );

  return $queues;
}

/**
 * Worker callback to process rostering queue.
 *
 * @param array $item
 *   Queue item.
 */
function _pads_isbe_services_rostering_worker($item) {
  // Got something weird, give up.
  if (!is_array($item) || !isset($item['type'])) {
    return;
  }

  $account = user_load($item['uid']);
  pads_ldr_client_user_id($item['uid']);

  switch ($item['type']) {
    case 'class':
      $data = (array) $item['data'];
      $i = new PadsIsbeServicesClassImport($account);
      $item['record'] = $i->import($data);
      pads_isbe_services_log($item);
      break;

    case 'student':
      // Individual student operations.
      $data = (array) $item['data'];
      $data['actionCode'] = isset($data['actionCode']) ? $data['actionCode'] : NULL;
      $i = new PadsIsbeServicesStudentImport($account);
      switch ($data['actionCode']) {
        case 1:
          $records = $i->import($data['students']);
          foreach ($records as $line => $record) {
            $item['data'] = $data['students'][$line];
            $item['line'] = $line;
            $item['record'] = $record;
            if (!isset($item['record']->errors)) {
              $item['record']->errors = array();
            }
            if (!empty($records->errors[$line])) {
              $item['record']->errors = array_merge($item['record']->errors, $records->errors[$line]);
            }
            pads_isbe_services_log($item);
          }
          break;

        case 2:
          // Delete.
          $data['actionCode'] = isset($data['actionCode']) ? $data['actionCode'] : NULL;
          $item['record'] = $i->remove($data);
          pads_isbe_services_log($item);
          break;

      }
      break;

    case 'confirm':
      $data = (array) $item['data'];
      $increment  = isset($data['batch_increment']) ? $data['batch_increment'] : 0;
      pads_isbe_services_rostering_confirm($data['batch_id'], $increment);
      break;

  }
}

/**
 * Send a confirmation to ISBE that the rostering action has completed.
 *
 * @param int $batch_id
 *   The batchId value from the initial ISBE rostering submission.
 * @param int $increment
 *   Batch increment value, defaults to 0.
 *
 * @return object
 *   The confirmation sent to ISBE.
 */
function pads_isbe_services_rostering_confirm($batch_id, $increment = 0) {
  $dispatch = drupal_static(__FUNCTION__ . '_dispatch', TRUE);

  $confirmation = new stdClass();
  $confirmation->batchId = $batch_id;
  $confirmation->batchIncrement = $increment;

  $query = 'SELECT type, data FROM {pads_isbe_rostering} WHERE batch_id = ? AND batch_increment = ? ORDER BY type, line';
  $result = db_query($query, array($batch_id, $increment));
  while ($row = $result->fetch()) {
    $confirmation->{$row->type}[] = json_decode($row->data);
  }

  $level = variable_get('pads_isbe_services_log_level', PADS_ISBE_SERVICES_LOG_FAILURE);
  if ($level & PADS_ISBE_SERVICES_LOG_CONFIRM) {
    $message = '<pre>' . print_r($confirmation, TRUE) . '</pre>';
    watchdog(__FUNCTION__, $message);
  }

  // Send that sucker.
  if ($dispatch) {
    $token = pads_isbe_client_get_token('Bearer ');
    $args['Authorization'] = $token;
    $args['batch'] = json_encode($confirmation);
    $msg_args['@batch'] = $batch_id;
    $msg_args['@increment'] = $increment;

    try {
      pads_isbe_client_call('processVendorResponse', $args);
      db_query('DELETE FROM {pads_isbe_rostering} WHERE batch_id = ? AND batch_increment = ?', array($batch_id, $increment));
      if ($level & PADS_ISBE_SERVICES_LOG_ALL) {
        watchdog(__FUNCTION__, 'ProcessVendorResponse successful for batch @batch, increment @increment.', array('@batch' => $batch_id));
      }
    }
    catch (Exception $e) {
      if ($level & PADS_ISBE_SERVICES_LOG_FAILURE) {
        watchdog(__FUNCTION__, 'ProcessVendorResponse failed for batch @batch, increment @increment.', $msg_args);
      }
    }
  }

  // Clear Organization cache to update student count.
  OrganizationFactory::getInstance()->cacheRebuild();

  return $confirmation;
}

/**
 * Log attempts to create classes and students.
 *
 * This code is dual-purpose, it writes the results to the pads_isbe_rostering
 * table for later use in a confirmation send to ISBE as well as records data to
 * the watchdog logging mechanism.
 *
 * The level of watchdog logging may be set by manipulating the
 * pads_isbe_services_log_level variable. See the following constants for more
 * detail:
 *
 * PADS_ISBE_SERVICES_LOG_DISABLED - No logging
 * PADS_ISBE_SERVICES_LOG_FAILURE  - Failing requests only
 * PADS_ISBE_SERVICES_LOG_ALL      - All requests
 * PADS_ISBE_SERVICES_LOG_DETAIL   - Include request details
 * PADS_ISBE_SERVICES_LOG_CONFIRM  - Log the confirmation sent to ISBE
 *
 * @param array $item
 *   The processed item. This should include batch_id, data, line, record, and
 *   type keys.
 */
function pads_isbe_services_log($item) {

  // How'd things go?
  $result = empty($item['record']->errors);

  $data = new stdClass();
  $data->batch_id = $item['batch_id'];
  $data->batch_increment = $item['batch_increment'];
  $data->type = $item['type'];
  $data->line = $item['line'];

  // Add result info to $item['data'].
  if ($result) {
    $item['data']->recordStatusCode = 1;
  }
  else {
    $item['data']->recordStatusCode = 2;
    $item['data']->recordStatusDescription = implode(', ', array_unique($item['record']->errors));
  }

  $data->data = json_encode($item['data']);
  drupal_write_record('pads_isbe_rostering', $data);

  $level = variable_get('pads_isbe_services_log_level', PADS_ISBE_SERVICES_LOG_FAILURE);

  // No logging.
  if ($level == PADS_ISBE_SERVICES_LOG_DISABLED) {
    return;
  }

  // Configured to log only failures.
  if ($result && !($level & PADS_ISBE_SERVICES_LOG_ALL)) {
    return;
  }

  $params['@type'] = $item['type'];

  // Summary.
  if ($result) {
    $severity = WATCHDOG_INFO;
    $params['@result'] = t('successful');
  }
  else {
    $severity = WATCHDOG_ERROR;
    $params['@result'] = t('failed');
  }
  $message[] = 'Rostering for @type @result.';

  // Detailed info.
  if ($level & PADS_ISBE_SERVICES_LOG_DETAIL) {
    $message[] = 'Data: !data';
    $params['!data'] = '<pre>' . check_plain(print_r($item, TRUE)) . '</pre>';
  }

  $message = implode('<br />', $message);
  watchdog('pads_isbe_services', $message, $params, $severity);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function pads_isbe_services_form_student_add_form_alter(&$form) {
  // Set "composite ID".
  $form['demographics']['stateIdentifier']['#element_validate'][] = 'pads_isbe_services_student_form_state_identifier_validate';

  // Send student data to ISBE.
  $form['#submit'][] = 'pads_isbe_services_student_add_form_submit';
}

/**
 * Element validate callback for stateIdentifier on student_add_form.
 *
 * Create the "composite ID" and set it onto the student as the stateIdentifier.
 */
function pads_isbe_services_student_form_state_identifier_validate(&$element, &$form_state) {
  // If empty, ignore to allow #required validation to apply.
  if (empty($element['#value'])) {
    return;
  }

  if (!$org = pads_org_load($form_state['values']['enrollOrgId'])) {
    return;
  }

  $composite_id = $element['#value'] . '_' . $org['organizationIdentifier'];
  form_set_value($element, $composite_id, $form_state);
}

/**
 * Submit callback for student_add_form.
 */
function pads_isbe_services_student_add_form_submit(&$form, &$form_state) {
  $student = $form_state['build_info']['args'][0];
  $b = new PadsIsbeServicesStudentBatch($student);

  // Set the class assignment if it's in there.
  if (!empty($form_state['values']['classAssignment1'])) {
    $b->setValue('classAssignment1', $form_state['values']['classAssignment1']);
  }

  $b->submit();
}
