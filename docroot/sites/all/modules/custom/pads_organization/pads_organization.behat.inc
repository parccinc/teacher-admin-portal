<?php
/**
 * @file
 * Contains \OrganizationContext.
 */

use Behat\Behat\Hook\Scope\BeforeStepScope;
use Behat\Gherkin\Node\TableNode;
use Drupal\DrupalExtension\Context\DrupalSubContextBase;
use Drupal\DrupalExtension\Context\DrupalSubContextInterface;
use Drupal\DrupalExtension\Hook\Scope\EntityScope;

class OrganizationContext extends DrupalSubcontextBase implements DrupalSubContextInterface {

  protected $xxOrgId;

  /**
   * Log in as someone if not already so Ldr doesn't soil itself.
   */
  protected function ldrClientUserLogIn() {
    if (user_is_anonymous()) {
      global $user;
      $user = user_load(1);
    }
  }

  /**
   * Makes sures that the state XX exists, and creates one otherwise.
   *
   * For example, deletes Orgs that were not deleted by LdrDriver because a
   * Student was manually added, or an import was done, or some Org-related
   * entity created through non LdrDriver steps. This method is not capable of
   * deleting tests or test assignments.
   *
   * @BeforeScenario @ldr_orgs
   *
   * @Todo: work with LdrDriver to find these type of Org items.
   */
  public function beforeLdrOrgTests() {
    $xx_exists = FALSE;
    $org_states = pads_org_get_children(1);
    foreach ($org_states as $org_id => $org) {
      if ($org['organizationName'] == 'XX') {
        $this->xxOrgId = $org_id;
        if ($org['childCount'] > 0) {
          $this->xxCleanUp();
        }
        $xx_exists = TRUE;
        break;
      }
    }
    if (!$xx_exists) {
      // Create the Org.
      // @see https://breaktech.centraldesktop.com/p/aQAAAAACIOlZ
      $org = array(
        'organizationName' => 'XX',
        'organizationType' => PADS_ORGANIZATION_LEVEL_STATE,
        'parentOrganizationId' => 1,
        'clientUserId' => 1,
      );
      $result = pads_org_add($org);
      if (!array_key_exists('organizationId', $result)) {
        throw new \Exception('Unable to create Ldr State Organization XX');
      }
      $this->xxOrgId = $result['organizationId'];
    }

    $this->addTokenToMink('[org:XX]', $this->xxOrgId);
  }

  /**
   * Used to clean up Orgs not able to be deleted with LdrDriver.
   *
   * @AfterScenario @ldr_cleanup
   */
  public function ldrCleanUp() {
    // Find XX.
    if (!$this->xxOrgId) {
      $org_states = pads_org_get_children(1);
      foreach ($org_states as $org_id => $org) {
        if ($org['organizationName'] == 'XX') {
          $this->xxOrgId = $org_id;
          if ($org['childCount'] > 0) {
            $this->xxCleanUp();
          }
          break;
        }
      }
    }
  }

  /**
   * @AfterScenario @ldr_orgs
   */
  public function xxCleanUp() {
    $xx_children = pads_org_get_subset($this->xxOrgId);
    if (!empty($xx_children)) {
      // Common settings.
      $ldr_args = array(
        'clientUserId' => 1,
        'organizationId' => $this->xxOrgId,
        'auth' => array(
          'clientName' => variable_get('test_ldr_username'),
          'password' => variable_get('test_ldr_password'),
        ),
      );

      // Delete any student class assignments within XX.
      $response = pads_ldr_client_call('deleteStudentClassAssignments', $ldr_args);

      // Delete all students in XX.
      $response = pads_ldr_client_call('deleteStudentRecords', $ldr_args);

      // Delete all classes in XX.
      $response = pads_ldr_client_call('getClasses', $ldr_args);
      if ($response && array_key_exists('classes', $response)) {
        foreach ($response['classes'] as $class) {
          pads_ldr_client_call('deleteClass', array(
            'classId' => $class['classId'],
            'clientUserId' => 1,
          ));
        }
      }

      // Delete the child Orgs.
      foreach($xx_children as $xx_child) {
        $org_ids[] = $xx_child['organizationId'];
      }
      arsort($org_ids);
      $this->ldrClientUserLogIn();

      $options = array(
        'migrate' => \Drupal\pads_organization\TermLdrOrganization::OPTION_MIGRATE_NOW,
      );
      pads_org_delete_multiple($org_ids, $options);
    }
  }

  /**
   * Getter method for xxOrgId.
   *
   * @return int
   *   The Org id for state XX.
   */
  public function getXxOrgId() {
    return $this->xxOrgId;
  }

  /**
   * Creates multiple users.
   *
   * Provide user data in the following format:
   *
   * | name     | mail         | roles        | organizations |
   * | user foo | foo@bar.com  | role1, role2 | org1, org2
   *
   * @Given TAP users:
   */
  public function tapUsers(TableNode $users_table) {
    $drupal_context = $this->getContext('\Drupal\DrupalExtension\Context\DrupalContext');
    $drupal_context->createUsers($users_table);
    $users = $drupal_context->users;
    foreach ($users_table->getHash() as $user_hash) {
      $user_name = $user_hash['name'];
      $user = $users[$user_name];

      // Assign Orgs.
      $user_hash['organizations'] = $this->fixStepArgument($user_hash['organizations']);
      pads_org_assign_orgs_to_user($user_hash['organizations'], $user);

      // Assign name field type of field_full_name from name.
      $name = explode(' ', $user->name);
      if (!empty($name)) {
        $given = array_shift($name);
        $family = !empty($name) ? implode(' ', $name) : '';

        $field_full_name = array(
          'given' => $given,
          'family' => $family,
        );
        $user = user_load($user->uid);
        $uw = entity_metadata_wrapper('user', $user);
        $uw->field_full_name = $field_full_name;
        $uw->save();
      }
    }
  }

  /**
   * @Given I am assigned organizations :org_ids
   */
  public function iAmAssignedOrganizations($org_ids) {
    $user = $this->getUser();
    pads_org_assign_orgs_to_user($org_ids, $user);
  }

  /**
   * @Given I am assigned the organization :org_id
   */
  public function iAmAssignedTheOrganization($org_id) {
    $org_id = $this->fixStepArgument($org_id);
    $this->iAmAssignedOrganizations($org_id);
  }

  /**
   * @Then the user with email address :mail should be assigned to organization(s) :orgs
   */
  function assertUserAssignedOrgs($mail, $orgs) {
    $mail = $this->fixStepArgument($mail);
    $orgs = $this->fixStepArgument($orgs);

    if (!$account = user_load_by_mail($mail)) {
      throw new Exception(sprintf('No user with email address "%s" found.', $mail));
    }

    $user_orgs = array_keys(pads_organization_get_user_orgs($account));

    $orgs = array_map('trim', explode(',', $orgs));

    $diff = array_diff($orgs, $user_orgs);

    if ($diff) {
      throw new Exception(sprintf('User should be assigned orgs "%s" but is not.', implode(', ', $orgs)));
    }
  }

  /**
   * Calls fixStepArgument from PadsMinkContext.
   *
   * @param string $arg
   *   The arg to "fix".
   *
   * @return mixed
   *   Fixed arg.
   */
  protected function fixStepArgument($arg) {
    $padsmink_context = $this->getContext('PadsMinkContext');
    return $padsmink_context->fixStepArgument($arg);
  }

  // Dashboard steps.
  /**
   * @Then I should see a :type block for :org_ids with clickable count and actions :buttons
   */
  public function iShouldSeeATypeBlockForWithClickableCountAndActions($type, $org_ids, $buttons) {
    $org_ids = $this->fixStepArgument($org_ids);
    $org_ids = explode(',', $org_ids);
    $orgs = pads_org_load_multiple($org_ids);

    // The :type, i.e. Districts, appears as a dashboard heading.
    $header_element = 'div.' . strtolower($type) . ' .card-title h6';
    $mink_context = $this->getContext('PadsMinkContext');
    $mink_context->assertElementContainsText($header_element, $type);

    // Checks that count is correct.
    switch ($type) {
      case 'States':
      case 'Districts':
      case 'Schools:':
        foreach ($orgs as $org) {
          $count = $org['childCount'];
          $mink_context->assertLinkVisible($count);
        }
        break;

      case 'users':
        foreach ($orgs as $org) {
          $count = pads_org_user_count($org['organizationId']);
          $mink_context->assertLinkVisible($count);
        }
        break;
    }

    // Check for action links.
    $buttons = explode(',', $buttons);
    foreach ($buttons as $button) {
      $mink_context->assertLinkVisible($button);
    }
  }

  /**
   * @BeforeStep /^(?:there is )?a file named "([^"]*)" with:$/
   */
  public function beforeFileCreate(BeforeStepScope $scope) {
    $feature_context = $this->getContext('FeatureContext');
    $token = array(
      '[xx-orgid]' => $this->xxOrgId,
    );
    $feature_context->addTokens($token);
  }

  /**
   * Adds token in PadsMinkContext for use with fixStepArgument.
   *
   * @param string $token
   *   Token value.
   * @param mixed $value
   *   The value to replace $token.
   */
  protected function addTokenToMink($token, $value) {
    $mink_context = $this->getContext('PadsMinkContext');
    $mink_context->addToken($token, $value);
  }

  /**
   * @Given XX Orgs:
   *
   * Provide Org data in the following format:
   * | organizationName | organizationType | parentOrganizationId | organizationIdentifier |
   * | XX District 1    | 3                | [org:XX]             | 1                      |
   * | School XX1a      | 4                | [org:XX District 1]  | 2                      |
   */
  public function xxOrgs(TableNode $table) {
    foreach ($table->getHash() as $org) {
      foreach ($org as $key => $value) {
        $org[$key] = $this->fixStepArgument($value);
      }
      // Tell Ldr it's okay to trust me.
      $org['clientUserId'] = 1;

      $options = array(
        'migrate' => \Drupal\pads_organization\TermLdrOrganization::OPTION_MIGRATE_OFF,
      );

      $response = pads_org_add($org, $options);
      if ($response && array_key_exists('organizationId', $response)) {
        $this->orgs[$org['organizationName']] = $response['organizationId'];
        $this->addTokenToMink("[org:{$org['organizationName']}]", $response['organizationId']);
      }
    }
    drupal_static_reset();
    pads_organization_process_import();
  }

  /**
   * @Given XX classes:
   *
   * Provide Class data in the following format:
   *| organizationId    | classIdentifier | gradeLevel | sectionNumber |
   * | [org:School XX1a] | Math            | 1          | math1         |
   * | [org:School XX1a] | English         | 1          | english       |
   */
  public function xxClasses(TableNode $table) {
    foreach ($table->getHash() as $class) {
      foreach ($class as $key => $value) {
        $class[$key] = $this->fixStepArgument($value);
      }
      // Tell Ldr it's okay to trust me.
      $class['clientUserId'] = 1;
      $response = pads_class_add($class);
      if ($response && array_key_exists('classId', $response)) {
        $this->classes[$class['classIdentifier']] = $response['classId'];
        $this->addTokenToMink("[class:{$class['classIdentifier']}]", $response['classId']);
      }
    }
  }

  /**
   * @Given XX Students:
   *
   * Provide Student data in the following format:
   * | enrollOrgId       | stateIdentifier | lastName | firstName | dateOfBirth | gender | gradeLevel |
   * | [org:School XX1a] | XXD1a001        | Skolnick | Louis     | 2001-02-03  | M      | 03         |
   * | [org:School XX1a] | XXD1a0002       | Lowell   | Gilbert   | 2004-05-06  | M      | 03         |
   */
  public function xxStudents(TableNode $table) {
    foreach ($table->getHash() as $student) {
      foreach ($student as $key => $value) {
        $student[$key] = $this->fixStepArgument($value);
      }
      // Tell Ldr it's okay to trust me.
      $student['clientUserId'] = 1;
      $response = pads_students_add($student);
      if ($response && array_key_exists('studentRecordId', $response)) {
        $this->students[$student['stateIdentifier']] = $response['studentRecordId'];
        $this->addTokenToMink("[student:{$student['stateIdentifier']}]", $response['studentRecordId']);
      }
    }
  }

}
