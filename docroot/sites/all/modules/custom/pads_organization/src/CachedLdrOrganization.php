<?php
/**
 * @file
 * Contains class \Drupal\pads_organization\CachedLdrOrganization.
 */

namespace Drupal\pads_organization;


class CachedLdrOrganization extends LdrOrganization {

  /**
   * {@inheritdoc}
   */
  public function cacheRebuild() {
    cache_clear_all('cache_pads_organizations', 'cache');
  }

  /**
   * Get the Organization hierarchy.
   *
   * @return \SplFixedArray
   *   SplFixedArray of all Organizations.
   */
  public function getOrganizationHierarchy() {
    $hierarchy = &drupal_static(__FUNCTION__);
    if (isset($hierarchy)) {
      return isset($hierarchy->data) ? $hierarchy->data : $hierarchy;
    }
    $hierarchy = cache_get('cache_pads_organizations');
    if ($hierarchy && isset($hierarchy->data)) {
      return $hierarchy->data;
    }

    $hierarchy = array();
    // Start with the national level Organization.
    $root_org_id = 1;
    $hierarchy[$root_org_id] = parent::getOrg($root_org_id);
    $hierarchy[$root_org_id]['parents'] = array();

    $response = parent::getOrgChildren();
    if (!empty($response)) {
      $this->hierarchyHelper($hierarchy, $response, PADS_ORGANIZATION_LEVEL_STATE, $root_org_id);
      $hierarchy = \SplFixedArray::fromArray($hierarchy);
      cache_set('cache_pads_organizations', $hierarchy);
    }

    /* Recursively delete all CSV files stores in private://organization-data.
    $uri = 'private://organization-data';
    file_unmanaged_delete_recursive($uri);
    */

    return $hierarchy;
  }

  /**
   * Recursive function to parse the LDR GetEducationalHierarchy result.
   *
   * @param array $hierarchy
   *   Array of organizations keyed by org_id.
   * @param array $data
   *   Array of organization data as returned by LDR.
   * @param int $level
   *   The level of the organizations being parsed.
   * @param null $parent
   *   The org_id of the parent organization.
   */
  private function hierarchyHelper(&$hierarchy, $data, $level = PADS_ORGANIZATION_LEVEL_STATE, $parent = NULL) {

    // This is how we look up the children.
    $keys = array(
      PADS_ORGANIZATION_LEVEL_STATE => 'districts',
      PADS_ORGANIZATION_LEVEL_DISTRICT => 'schools',
      PADS_ORGANIZATION_LEVEL_SCHOOL => 'classes',
    );

    foreach ($data as $org) {
      // Add the organization into the array.
      $org_id = $org['organizationId'];
      $org['parents'] = array();
      $org['children'] = array();
      $hierarchy[$org_id] = $org;

      // If parent data exists, add it in.
      if ($parent) {
        $hierarchy[$org_id]['parents'] = array_merge(array($parent), $hierarchy[$parent]['parents']);
        $hierarchy[$parent]['children'][] = $org_id;
      }

      // Recurse to the next level if possible.
      if (array_key_exists($level, $keys) && $org['childCount'] > 0) {
        $child_data = parent::getOrgChildren($org_id);
        if (!empty($child_data)) {
          $this->hierarchyHelper($hierarchy, $child_data, $level + 1, $org_id);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getOrg($org_id) {
    $orgs = $this->getOrgMultiple([$org_id]);
    return empty($orgs) ? FALSE : reset($orgs);
  }

  /**
   * {@inheritdoc}
   */
  public function getOrgChildren($parent_org_id = 1) {
    $org = $this->getOrg($parent_org_id);
    return $this->getOrgMultiple($org['children']);
  }

  /**
   * {@inheritdoc}
   */
  public function getUserCount($org_id, $children = TRUE) {
    // Make sure nothing weird with the org, gracefully fail with 0.
    if (!$org = $this->getOrg($org_id)) {
      return 0;
    }

    // Attempt to retrieve from persistent cache.
    $cid = "user_count:" . ($children ? 'child:' : '') . $org_id;
    if ($data = cache_get($cid, 'cache_pads_hierarchy_counts')) {
      return $data->data;
    }

    // Determine which orgs we're analyzing.
    $org_ids = ($children) ? array_keys(pads_org_get_subset($org_id)) : array();
    $org_ids[] = $org_id;

    // Big, semi-ugly query to avoid instantiating an enormous amount of profile
    // and user entities.
    $query = "SELECT COUNT(DISTINCT pid) FROM {profile} AS p
    INNER JOIN {field_data_field_state} AS o ON o.entity_type = ? AND o.entity_id = p.pid
    INNER JOIN {users} AS u ON p.uid = u.uid
    WHERE u.status <> 0 AND p.type = ?";
    $args = array('profile2', 'pads_organization');

    // Add to WHERE clause for filtering org assignments.
    if (count($org_ids) == 1) {
      // Exactly 1 org, query directly.
      $query .= " AND o.field_state_value = ?";
      $args[] = reset($org_ids);
    }
    else {
      // Multiple orgs, use IN clause.
      // Not using PDO args here to avoid large array operations.
      $placeholders = implode(', ', $org_ids);
      $query .= " AND o.field_state_value IN ($placeholders)";
    }

    $result = db_query($query, $args)->fetchField();
    cache_set($cid, $result, 'cache_pads_hierarchy_counts', CACHE_PERMANENT);
    return $result;
  }

  /**
   * Get the immediate parent or parents of the passed in Organization.
   *
   * @param int $org_id
   *   The Organization id to retrieve parents for.
   * @param bool $all_parents
   *   Defaults to TRUE, which returns all parents. Otherwise just the immediate
   *   parent is returned.
   *
   * @return array
   *   An array of the parent Organization ids.
   */
  public function getParents($org_id, $all_parents = TRUE) {
    $org = $this->getOrg($org_id);
    $parents = $org['parents'];
    if (!empty($parents)) {
      return $all_parents ? $parents : array_pop($parents);
    }
    return $parents;
  }
}
