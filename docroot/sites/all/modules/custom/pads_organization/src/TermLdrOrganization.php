<?php
/**
 * @file
 * Install, update, and uninstall functions for the Foo Bar module.
 * Contains \Fully\Qualified\Namespace\And\NameOfTheClass.
 */

namespace Drupal\pads_organization;


class TermLdrOrganization extends LdrOrganization {

  const OPTION_MIGRATE_NOW = 1;
  const OPTION_MIGRATE_SHUTDOWN = 2;
  const OPTION_MIGRATE_OFF = 1;

  protected $vocabulary;
  protected $registerImportOnShutdown;
  protected $registerRollbackOnShutdown;

  /**
   * TermLdrOrganization constructor.
   */
  public function __construct() {
    $this->vocabulary = taxonomy_vocabulary_machine_name_load('Organization');
    $this->registerImportOnShutdown = $this->registerRollbackOnShutdown = TRUE;
  }

  /**
   * Get the Organization hierarchy.
   *
   * @return \SplFixedArray
   *   SplFixedArray of all Organizations.
   */
  public function getOrganizationHierarchy() {
    $hierarchy = &drupal_static(__FUNCTION__);
    if (isset($hierarchy) && !empty($hierarchy)) {
      return isset($hierarchy->data) ? $hierarchy->data : $hierarchy;
    }
    $hierarchy = cache_get('cache_pads_organization_terms');
    if ($hierarchy && isset($hierarchy->data)) {
      return $hierarchy->data;
    }
    $hierarchy = parent::getOrganizationHierarchy();
    cache_set('cache_pads_organization_terms', $hierarchy);
    return $hierarchy;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrgChildren($parent_org_id = 1) {
    $child_orgs = array();
    $org = $this->getOrg($parent_org_id);
    if ($org['childCount'] == 0) {
      return $child_orgs;
    }

    $term = $this->getTermFromOrg($parent_org_id);
    // If the term or term children can't be found, resort to Ldr.
    if (!$term || empty($child_terms = taxonomy_get_children($term->tid))) {
      return parent::getOrgChildren($parent_org_id);
    }

    foreach ($child_terms as $term) {
      if ($child_org = $this->getOrgFromTerm($term)) {
        $child_orgs[$child_org['organizationId']] = $child_org;
      };
    }
    return $child_orgs;
  }

  /**
   * Get the immediate parent or parents of the passed in Organization.
   *
   * @param int $org_id
   *   The Organization id to retrieve parents for.
   * @param bool $all_parents
   *   Defaults to TRUE, which returns all parents. Otherwise just the immediate
   *   parent is returned.
   *
   * @return array
   *   An array of the parent Organization ids
   */
  public function getParents($org_id, $all_parents = TRUE) {
    $parents = array();
    if ($term = $this->getTermFromOrg($org_id)) {
      $tid = $term->tid;
      $parent_terms = $all_parents ? taxonomy_get_parents_all($tid) : taxonomy_get_parents($tid);

      foreach ($parent_terms as $term) {
        $org_from_term = $this->getOrgFromTerm($term);
        // Do not include parent source Organization.
        if ($org_id != $org_from_term['organizationId']) {
          $parents[$org_from_term['organizationId']] = $org_from_term;
        }
      }
    }

    return $parents;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserCount($org_id, $children = TRUE) {

    if (!$org = $this->getOrg($org_id)) {
      return 0;
    };
    $term = $this->getTermFromOrg($org_id);
    if (!$term) {
      watchdog('pads_organization', 'Unable to find term from org_id %org_id', array(
        '%org_id' => $org_id,
      ));
      return 0;
    }
    $tids = array();
    $tids[] = $term->tid;
    if ($children) {
      $tree = taxonomy_get_tree($this->vocabulary->vid, $term->tid);
      foreach ($tree as $term) {
        $tids[] = $term->tid;
      }
    }

    $offset = 0;
    $length = count($tids);
    $results = array();
    while ($offset < $length) {
      $in = array_slice($tids, $offset, 499);

      $efq = new \EntityFieldQuery();
      $efq->entityCondition('entity_type', 'profile2')
        ->entityCondition('bundle', 'pads_tap_organization')
        ->fieldCondition('field_tap_organization', 'tid', $in, 'IN');
      $result = $efq->execute();
      if (!empty($result) && array_key_exists('profile2', $result)) {
        $results += $result['profile2'];
      }
      $offset += 499;
    }
    return count($results);

  }

  /**
   * {@inheritdoc}
   */
  public function getOrg($org_id) {
    $hierarchy = $this->getOrganizationHierarchy();
    if ($hierarchy && $hierarchy->offsetExists($org_id)) {
      return $hierarchy->offsetGet($org_id);
    }
    $org = parent::getOrg($org_id);
    if (!$org) {
      watchdog('TermLdrOrganization', 'Organization id %org_id is not in the hierarchy', array(
        '%org_id' => $org_id,
      ));
      return array();
    }
    // If we are here we need to clear the cache.
    cache_clear_all('cache_pads_organization_terms', 'cache', TRUE);
    // Kill the static cache.
    drupal_static_reset('getOrganizationHierarchy');
    return $org;
  }

  /**
   * Returns Organization from Migrate mapping.
   *
   * Finds the taxonomy term associated with an Organization from the Migrate
   * mapping table. An organization array is built and returned.
   *
   * @param int $source_id
   *   The organization id.
   *
   * @return array
   *   The organization array
   */
  public function getTermFromOrg($source_id) {
    $term = &drupal_static(__FUNCTION__ . ':' . $source_id, FALSE);
    if (!$term) {
      $map = pads_migrate_orgs_get_map_rows('sourceid1');
      if (array_key_exists($source_id, $map)) {
        $term = taxonomy_term_load($map[$source_id]);
      }
    }
    return $term;
  }

  /**
   * Returns the Organization represented by the term.
   *
   * @param object $term
   *   The taxonomy term.
   *
   * @return array
   *   The organization array.
   */
  public function getOrgFromTerm($term) {
    if (!$term || !isset($term->tid)) {
      return FALSE;
    }
    $tid = $term->tid;
    $org = &drupal_static(__FUNCTION__ . ':' . $tid, FALSE);
    if (!$org) {
      $map = pads_migrate_orgs_get_map_rows('destid1');
      if (array_key_exists($tid, $map)) {
        $org_id = $map[$tid];
        $org = $this->getOrg($org_id);
      }
    }
    return $org;
  }

  /**
   * Updates the hierarchy with passed in Organizations.
   *
   * @param array $orgs
   *   Associative array with Organization id as the key, Organization array
   *   as value.
   * @param bool $reset
   *   Defaults to TRUE and will update the cache.
   *
   * @return array
   *   An array similar to the passed in $orgs array, but with updated term
   *   data.
   */
  protected function updateOrgs($orgs, $reset = TRUE) {
    $updated_orgs = array();
    $hierarchy = $this->getOrganizationHierarchy();
    foreach ($orgs as $org_id => $fields) {
      $org = parent::updateOrg($org_id, $fields);
      $term = $this->getTermFromOrg($org_id);
      $term->name = $org['organizationName'];
      taxonomy_term_save($term);
      $updated_orgs[$org_id] = $org;

      if ($reset) {
        $hierarchy->offsetSet($org_id, $org);
      }
    }

    if ($reset) {
      $this->updateHierarchyCache($hierarchy);
    }
    return $updated_orgs;
  }

  /**
   * {@inheritdoc}
   */
  public function updateOrg($org_id, $fields) {
    $orgs = array(
      $org_id => $fields,
    );
    $updated_orgs = $this->updateOrgs($orgs);
    return array_key_exists($org_id, $updated_orgs) ?
      $updated_orgs[$org_id] : array();
  }

  /**
   * Updates hierarchy cache with new hierarchy data.
   *
   * @param \SplFixedArray $hierarchy
   *   The hierarchy with updated info.
   */
  protected function updateHierarchyCache($hierarchy) {
    cache_set('cache_pads_organization_terms', $hierarchy);

    // Kill the static cache.
    drupal_static_reset('getOrganizationHierarchy');
  }

  /**
   * {@inheritdoc}
   */
  public function addOrg($org, $options = array()) {
    $org = parent::addOrg($org);
    if (!empty($org) && array_key_exists('organizationId', $org)) {
      $defaults = array(
        'migrate' => $this::OPTION_MIGRATE_SHUTDOWN,
      );
      $options = $options + $defaults;
      switch ($options['migrate']) {
        case $this::OPTION_MIGRATE_NOW:
          drupal_static_reset();
          pads_organization_process_import();
          break;

        case $this::OPTION_MIGRATE_SHUTDOWN:
          $this->processImportOnShutdown();
          break;

      }
    }
    return $org;
  }

  /**
   * {@inheritdoc}
   */
  public function cacheRebuild() {
    cache_clear_all('cache_pads_organization_terms', 'cache');
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOrgs($org_ids) {
    $result = parent::deleteOrgs($org_ids);
    if ($result && !empty($result['success'])) {
      $idlist = implode(',', $result['success']);
      $options = array(
        'idlist' => $idlist,
      );
      $this->processRollbackOnShutdown($options);
    }
    return $result;
  }

  /**
   * Exists for import.
   *
   * If we are adding a lot of Organizations then process
   * the import to create the Terms on shutdown.
   *
   * @param array $options
   *   Options to pass into Migrate::processImport. Defaults to empty array.
   * @param string $function
   *   Name of the shutdown function to register. Defaults to
   *  'pads_organization_process_import'.
   */
  protected function processImportOnShutdown($options = array(), $function = 'pads_organization_process_import') {
    if ($this->registerImportOnShutdown) {
      drupal_register_shutdown_function($function, $options);
      $this->registerImportOnShutdown = FALSE;
    }
  }

  /**
   * Exists for import.
   *
   * If we are deleting a lot of Organizations then rollback
   * the import to create the Terms on shutdown.
   *
   * @param array $options
   *   Options to pass into Migrate::processImport. Defaults to empty array.
   * @param string $function
   *   Name of the shutdown function to register. Defaults to
   *  'pads_organization_process_rollback'.
   */
  protected function processRollbackOnShutdown($options = array(), $function = 'pads_organization_process_rollback') {
    if ($this->registerRollbackOnShutdown) {
      drupal_register_shutdown_function($function, $options);
      $this->registerRollbackOnShutdown = FALSE;
    }
  }
}
