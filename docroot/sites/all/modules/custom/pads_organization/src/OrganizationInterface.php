<?php
/**
 * @file
 * Contains interface \Drupal\pads_organization\OrganizationInterface.
 */

namespace Drupal\pads_organization;

interface OrganizationInterface {

  /**
   * Get the Organization hierarchy.
   *
   * @return \SplFixedArray
   *   SplFixedArray of all Organizations.
   */
  public function getOrganizationHierarchy();

  /**
   * Get Organization object.
   *
   * @param int $org_id
   *   The Organization id of the org to retrieve.
   *
   * @return array
   *   The Organization array, or FALSE if not found.
   */
  public function getOrg($org_id);

  /**
   * Get Organization ID from state code and organization identifier.
   *
   * @param string $state_code
   *   The state code of the org to query.
   * @param string $org_identifier
   *   The org identifier of the org to query.
   *
   * @return mixed
   *   The Organization Id, or FALSE if not found.
   */
  public function getOrgId($state_code, $org_identifier);

  /**
   * Get an array of Organizations.
   *
   * @param array $org_ids
   *   An array of Organization ids to retrieve.
   *
   * @return array
   *   The array of Organization array objects.
   */
  public function getOrgMultiple($org_ids);

  /**
   * Get the children of the passed in Organization.
   *
   * @param int $parent_org_id
   *   Optionally pass in the parent Organization id. Default to national level.
   *
   * @return array
   *   An array of the children Organization array objects.
   */
  public function getOrgChildren($parent_org_id = 1);

  /**
   * Get the number of users assigned to the passed in Organization.
   *
   * @param int $org_id
   *   The Organization id.
   * @param bool $children
   *   Include the count of users for child organizations. Defaults to TRUE.
   *
   * @return int
   *   The count of users within the Organization.
   */
  public function getUserCount($org_id, $children = TRUE);

  /**
   * Get the immediate parent or parents of the passed in Organization.
   *
   * @param int $org_id
   *   The Organization id to retrieve parents for.
   * @param bool $all_parents
   *   Defaults to TRUE, which returns all parents. Otherwise just the immediate
   *   parent is returned.
   *
   * @return array
   *   An array of the parent Organization ids.
   */
  public function getParents($org_id, $all_parents = TRUE);

  /**
   * Update values in Organization.
   *
   * @param int $org_id
   *   The Organization id that needs updating.
   * @param array $fields
   *   An associative array with the key values being the Organization
   *   properties and the values being the new value.
   *
   * @return array
   *   The updated Organization.
   */
  public function updateOrg($org_id, $fields);

  /**
   * Add Organization.
   *
   * @param array $org
   *   New Organization values.
   * @param array $options
   *   Allow for interface implementations to have options.
   *
   * @return array
   *   The new Organization.
   */
  public function addOrg($org, $options = array());

  /**
   * Delete Organization.
   *
   * @param int $org_id
   *   The Organization id.
   *
   * @return array
   *   Response from Ldr.
   */

  /**
   * Named after the Drupal 8 drush command.
   *
   * Clears the Organization hierarchy cache, if it exists.
   */
  public function cacheRebuild();

  public function deleteOrg($org_id);
  /**
   * Delete Organizations.
   *
   * @param array $org_ids
   *   Array of Organization ids.
   *
   * @return array
   *   Response from Ldr.
   */
  public function deleteOrgs($org_ids);

  /**
   * Find Organizations.
   *
   * Uses Ldrs GET organization-id.
   * @see https://breaktech.centraldesktop.com/p/aQAAAAACIgda
   *
   * @param array $params
   *   Ldr search parameters.
   *
   * @return array
   *   Ldr response. If valid, an array of Org ids found.
   */
  public function findOrgs($params);
}
