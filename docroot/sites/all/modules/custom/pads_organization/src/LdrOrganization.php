<?php
/**
 * @file
 * Contains \Drupal\pads_organization\LdrOrganization.
 */

namespace Drupal\pads_organization;

abstract class LdrOrganization implements OrganizationInterface {

  /**
   * {@inheritdoc}
   */
  public function getOrg($org_id) {
    $response = pads_ldr_client_call('organization', array(
      'organizationId' => $org_id,
    ));
    $org = array_key_exists('organizationId', $response) ? $response : FALSE;
    return $org;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrgId($state_code, $org_identifier) {
    // Query by state code, and org identifier.
    $response = pads_ldr_client_call('organizationId', array(
      'stateCode' => $state_code,
      'organizationIdentifier' => $org_identifier,
    ));
    $org = array_key_exists('organizationId', $response) ? $response['organizationId'] : FALSE;
    if ($org) {
      return $org;
    }

    // Look for a state directly.
    $response = pads_ldr_client_call('organizationId', array(
      'organizationPath' => PADS_ORGANIZATION_ROOT_NAME . '_' . $state_code,
    ));
    $org = array_key_exists('organizationId', $response) ? $response['organizationId'] : FALSE;

    return $org;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrganizationHierarchy() {
    $hierarchy = array();
    $response = pads_ldr_client_call('getAllOrganizations');
    $organizations = array_key_exists('organizations', $response) ?
      $response['organizations'] : array();
    foreach ($organizations as $organization) {
      $hierarchy[$organization['organizationId']] = $organization;
    }
    if (!empty($hierarchy)) {
      $hierarchy = \SplFixedArray::fromArray($hierarchy);
    }
    else {
      $hierarchy = new \SplFixedArray();
    }
    return $hierarchy;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrgChildren($parent_org_id = 1) {
    $children = array();
    $response = pads_ldr_client_call('organizations', array(
      'organizationId' => $parent_org_id,
    ));
    if (!array_key_exists('error', $response)) {
      $children = $response['organizations'];
    }
    return $children;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrgMultiple($org_ids) {
    $orgs = array();
    $hierarchy = $this->getOrganizationHierarchy();
    if (!empty($hierarchy)) {
      foreach ($org_ids as $org_id) {
        if ($hierarchy->offsetExists($org_id)) {
          $orgs[$org_id] = $hierarchy[$org_id];
        }
      }
    }
    return $orgs;
  }

  /**
   * {@inheritdoc}
   */
  public function updateOrg($org_id, $fields) {
    $org = $this->getOrg($org_id);
    foreach ($fields as $key => $value) {
      $org[$key] = $value;
    }
    // Todo: Update ldr.
    return $org;
  }

  /**
   * {@inheritdoc}
   */
  public function addOrg($org, $options = array()) {
    if (!array_key_exists('clientUserId', $org)) {
      $org['clientUserId'] = $GLOBALS['user']->uid;
    }
    return pads_ldr_client_call('addOrganization', $org);
  }

  /**
   * {@inheritdoc}
   */
  public function cacheRebuild() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOrg($org_id) {
    $org_ids = array($org_id);
    $this->deleteOrgs($org_ids);
  }

  /**
   * Delete Orgs and add success counts to result.
   *
   * @param array $org_ids
   *   Array of Organization ids.
   *
   * @return array
   *   Array with a success key and a results key. The success key value is an
   *   array of Organization ids that were successfully deleted. The results
   *   key value is an array of the Ldr output regardless of success, keyed by
   *   Organization id.
   */
  public function deleteOrgs($org_ids) {
    $results = array(
      'success' => array(),
    );
    foreach ($org_ids as $org_id) {
      $args = array(
        'organizationId' => $org_id,
        'clientUserId' => $GLOBALS['user']->uid,
      );
      $result = pads_ldr_client_call('deleteOrganization', $args);
      if (array_key_exists('result', $result) && $result['result'] == 'success') {
        $results['success'][] = $org_id;
      }
      $results['results'][$org_id] = $result;
    }
    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function findOrgs($params) {
    return pads_ldr_client_call('organizationId', $params);
  }

}
