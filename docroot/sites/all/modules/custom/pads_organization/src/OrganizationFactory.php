<?php
/**
 * @file
 * Contains \Drupal\pads_organization\OrganizationFatory.
 */

namespace Drupal\pads_organization;

class OrganizationFactory {

  /**
   * Returns default instance of OrganizationInterface.
   *
   * @return OrganizationInterface
   *   The instance.
   */
  public static function getInstance() {
    return OrganizationFactory::getTermLdrOrganizationInstance();
  }

  /**
   * Get TermLdrOrganization instance.
   *
   * @return TermLdrOrganization
   *   The instance.
   */
  public static function getTermLdrOrganizationInstance() {
    $ldr_organization = &drupal_static(__CLASS__ . __FUNCTION__ . 'TermLdrOrganization');
    if (!isset($ldr_organization) || empty($ldr_organization)) {
      $ldr_organization = new TermLdrOrganization();
    }
    return $ldr_organization;
  }

  /**
   * Get CachedLdrOrganization instance.
   *
   * @return CachedLdrOrganization
   *   The instance.
   */
  public static function getCachedLdrOrganizationInstance() {
    $ldr_organization = &drupal_static(__CLASS__ . __FUNCTION__ . 'CachedLdrOrganization');
    if (!isset($ldr_organization) || empty($ldr_organization)) {
      $ldr_organization = new CachedLdrOrganization();
    }
    return $ldr_organization;
  }

}
