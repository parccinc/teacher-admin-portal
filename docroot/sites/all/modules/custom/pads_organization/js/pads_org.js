/**
 * Set pads_org_org_select's element.form attribute.
 *
 * This bit of JS is needed to set the element.form property on the outer
 * div of the pads_org_org_select form element. This property is populated
 * by default for DOM elements that accept input and is needed for Drupal's AJAX
 * JS to process the pads_org_org_select's div properly.
 */

(function ($) {

  Drupal.behaviors.padsOrg = {
    attach: function (context, settings) {
      // Attach the form to the select element.
      $(".pads-org-org-select", context).each(function () {
        Drupal.ajax[this.id].form = $(this).closest('form');
      });

      // Initialize things in the widget.
      $(".pads-org-widget", context).each(function () {
        var widget = this;

        var wrapper = $(".pads-org-widget-add-wrapper.hidden").hide().removeClass("hidden");
        // Show and hide the add organizations wrapper.
        $(".pads-org-widget-add", widget).click(function (event) {
          event.preventDefault();
          $(wrapper).show('slow');
        });
        $(".pads-org-widget-cancel", widget).click(function (event) {
          event.preventDefault();
          $(wrapper).hide('slow');
        });

        $("input.pads-org-widget-remove", widget).each(function () {
          Drupal.ajax[this.id].options.beforeSubmit = function () {
            return confirm("Are you sure you want to remove this organization?");
          };
        });
      });

      // Jump behavior on org context selection.
      $("form.org-context-select", context).each(function() {
        var form = this;
        $("button", this).hide();
        $("select", this).change(function () {
          $(form).submit();
          }
        );
      });
    }
  };

})(jQuery);
