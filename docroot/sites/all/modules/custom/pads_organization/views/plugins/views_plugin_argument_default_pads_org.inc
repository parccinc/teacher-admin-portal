<?php
/**
 * @file
 * Contains /views_plugin_argument_default_pads_org.
 */

class views_plugin_argument_default_pads_org extends views_plugin_argument_default {

  /**
   * {@inheritdoc}
   */
  public function get_argument() {
    $orgs = pads_organization_get_user_orgs();
    $org_ids = array_keys($orgs);
    if ($this->options['object_type'] == 'term') {
      $tids = array();
      foreach ($org_ids as $org_id) {
        $term = pads_org_term_from_org($org_id);
        if ($term) {
          $tids[] = $term->tid;
        }
      }
      return implode('+', $tids);
    }
    return implode('+', $org_ids);
  }

  /**
   * {@inheritdoc}
   */
  public function option_definition() {
    return ViewsPluginPadsOrgDefaults::optionDefinition();
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    ViewsPluginPadsOrgDefaults::optionsForm($form, $form_state, $this->options);
  }
}
