<?php
/**
 * @file
 * Contains \ViewsPluginPadsOrgDefault.
 */

class ViewsPluginPadsOrgDefaults {

  protected static $options;

  /**
   * Setting options for new control plugin.
   *
   * @return array
   *   Config options for plugin.
   */
  public static function optionDefinition() {
    self::$options = array(
      'object_type' => 'term',
    );
    return self::$options;
  }

  /**
   * Provide a form to edit options for this plugin.
   *
   * @param array $form
   *   Drupal form array.
   *
   * @param array $form_state
   *   Drupal form_state array.
   */
  public static function optionsForm(&$form, &$form_state, $options) {
    $form['object_type'] = array(
      '#type' => 'select',
      '#title' => t('Object representing Org'),
      '#options' => array(
        'term' => t('Term'),
        'org' => t('Org'),
      ),
      '#default_value' => $options['object_type'],
    );
  }
}
