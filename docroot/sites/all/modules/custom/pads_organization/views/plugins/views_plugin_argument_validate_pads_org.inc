<?php
/**
 * @file
 * Contains the 'PADS Organization' argument validator plugin.
 */

class views_plugin_argument_validate_pads_org extends views_plugin_argument_validate {

  /**
   * {@inheritdoc}
   */
  public function init(&$view, &$argument, $options) {
    parent::init($view, $argument, $options);
    composer_manager_register_autoloader();
  }

  /**
   * {@inheritdoc}
   */
  public function option_definition() {
    return ViewsPluginPadsOrgDefaults::optionDefinition();
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    ViewsPluginPadsOrgDefaults::optionsForm($form, $form_state, $this->options);
  }

  /**
   * {@inheritdoc}
   */
  public function validate_argument($arg) {
    $args = views_break_phrase($arg);
    $object_ids = $args->value;

    if ($this->options['object_type'] == 'term') {
      $access_callback = 'pads_org_term_access';
      $objects = taxonomy_term_load_multiple($object_ids);
    }
    else {
      $access_callback = 'pads_org_access';
      $objects = pads_org_load_multiple($object_ids);
    }

    if (!$objects || empty($objects)) {
      return FALSE;
    }
    foreach ($objects as $object) {
      if (!call_user_func($access_callback, $object)) {
        return FALSE;
      }
    }
    return TRUE;
  }

}
