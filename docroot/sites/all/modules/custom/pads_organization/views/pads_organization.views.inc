<?php
/**
 * @file
 * View hooks for pads_organization.
 */

/**
 * Implements hook_views_plugins().
 */
function pads_organization_views_plugins() {

  $plugins = array(
    'argument default' => array(
      'pads_org' => array(
        'title' => t('PADS Organizations from logged in user'),
        'handler' => 'views_plugin_argument_default_pads_org',
      ),
    ),
    'argument validator' => array(
      'pads_org' => array(
        'title' => t('PADS Organization'),
        'handler' => 'views_plugin_argument_validate_pads_org',
      ),
    ),
  );
  return $plugins;
}
