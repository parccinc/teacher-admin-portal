<?php
/**
 * @file
 * Contains theme implementation for organization field.
 */

/**
 * Default theme implementation for pads_org_org_select's element wrapper.
 *
 * This is needed to get an ID on the element's wrapper so it may be replaced
 * via AJAX.
 *
 * @see theme_form_element()
 */
function theme_pads_org_org_form_element(&$variables) {
  $element = &$variables['element'];

  $attributes['id'] = $element['#id'] . '-wrapper';
  $attributes['class'][] = 'pads-org-org-select-wrapper';

  // Start with the outer wrapper.
  $output = '<div' . drupal_attributes($attributes) . '>' . "\n";

  // Add the label.
  $output .= ' ' . theme('form_element_label', $variables);

  $prefix = isset($element['#field_prefix']) ? '<span class="field-prefix">' . $element['#field_prefix'] . '</span> ' : '';
  $suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix">' . $element['#field_suffix'] . '</span>' : '';

  // Add the actual form element and other children.
  $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";

  // Add the description.
  if (!empty($element['#description'])) {
    $output .= '<div class="description">' . $element['#description'] . "</div>\n";
  }

  // Close the outer wrapper.
  $output .= "</div>\n";

  return $output;
}

/**
 * Default theme implementation for pads_user_profile_org_select form element.
 */
function theme_pads_org_org_select(&$variables) {
  $element = $variables['element'];

  // Grab the hierarchy info and selected organization.
  $levels = pads_org_levels();

  // Build markup for the select boxes, try each level if the level has no
  // available options then skip it.
  $output = '';
  foreach ($levels as $depth => $level) {
    if ($options = pads_org_options($depth, $element['#value'])) {

      $element = $variables['element'];
      $element['#name'] .= "[$depth]";
      element_set_attributes($element, array('name'));

      // If the selected value is not an "all" or empty value, accept the first option.
      $selected_org_id = !in_array($element['#value'], ['', 'all']) ? $element['#value'] : key($options);
      $selected_org = pads_org_load($selected_org_id);
      if ($depth > $selected_org['level']) {
        $keys = array_keys($options);
        $element['#value'] = reset($keys);
      }

      $options = pads_org_render_options($options, $element['#value']);
      $output .= "<label for=\"{$element['#name']}\">$level</label>";
      $output .= "<select" . drupal_attributes($element['#attributes']) . '>' . $options . '</select>';
    }
  }

  // Wrap it all in a div that the JS can target for the change event.
  $output = "<div id=\"{$variables['element']['#id']}\" class=\"pads-org-org-select\">$output</div>";

  return $output;
}

/**
 * Default theme implementation for the pads_org_org field widget.
 */
function theme_pads_org_org_select_widget(&$variables) {
  $element = $variables['element'];

  $hierarchy = pads_organization_get_hierarchy();
  $levels = pads_org_levels(TRUE);

  $output = array(
    '#type' => 'fieldset',
    '#id' => $element['#id'],
  );

  if ($element['#value']) {
    $output['table'] = array(
      '#theme' => 'table',
      '#header' => array(t('Org ID'), t('Org Name'), t('Org Level'), t('Actions')),
      '#empty' => t('No organizations.'),
      '#caption' => t('The following table lists the organization(s) whose student information is available to the user. Click the Add Organization button to associate more organizations to the user.'),
    );

    // alter form elements if user does not have rights to edit organizations field
    if (!$element['#interactive_access']) {
      $output['table']['#header'] = array(t('Org ID'), t('Org Name'), t('Org Level'), '');
      $output['table']['#caption'] = t('The following table lists the organization(s) whose student information is available to you.');
    }

    // Build the table data.
    foreach ($element['#value'] as $delta => $item) {
      $row = array(
        $item,
        check_plain($hierarchy[$item]['name']),
        $levels[$hierarchy[$item]['level']],
        drupal_render($element['remove'][$delta]),
      );
      $output['table']['#rows'][] = $row;
    }

    $output['add'] = array(
      '#type' => 'button',
      '#value' => t('Add More Organizations'),
      '#access' => $element['#interactive_access'],
    );
    $output['add']['#attributes']['class'] = array('pads-org-widget-add');
    $output['add']['#id'] = 'pads-org-widget-add';
    $element['add']['actions']['cancel'] = array(
      '#id' => 'pads-org-widget-cancel',
      '#type' => 'button',
      '#value' => t('Cancel'),
    );
    $element['add']['actions']['cancel']['#attributes']['class'] = array('pads-org-widget-cancel');
  }

  $output['children'] = array(
    '#markup' => drupal_render_children($element),
  );

  $output['#attributes']['class'][] = 'pads-org-widget';
  $output['#attached']['js'][] = drupal_get_path('module', 'pads_organization') . '/js/pads_org.js';
  return render($output);
}
