<?php
/**
 * @file
 * Page handling for pads_organizations module.
 */

/**
 * Page callback for default organization page.
 *
 * @param bool $list_children
 *   If TRUE will display the users first Orgs children. Otherwise just displays
 *   the Org. Defaults to FALSE.
 *
 * @return array
 *   Return value from pads_organization().
 */
function pads_my_organizations($list_children = TRUE) {
  $org = FALSE;
  if ($org_id = pads_organization_get_selected_org()) {
    $org = pads_org_load($org_id);
  }

  // This should never happen, but just in case.
  if (!$org) {
    return MENU_ACCESS_DENIED;
  }

  return pads_organizations($org, $list_children);
}

/**
 * My Organizations page callback.
 *
 * @param array $org
 *   Organization array.
 * @param bool $list_children
 *   Optional argument to display only the children of $org. Defaults to TRUE.
 *
 * @return array
 *   A lovely Drupal render array.
 */
function pads_organizations($org, $list_children = TRUE) {
  if (empty($org)) {
    return pads_my_organizations($list_children);
  }
  $build = array();
  $level = pads_org_levels();
  $levels = pads_org_levels(FALSE, TRUE);

  $orgs_to_show = $list_children ? pads_org_get_children($org['organizationId']) : array($org);

  $view_level = $list_children ? $org['organizationType'] + 1 : $org['organizationType'];
  $view_child_level = $view_level + 1;

  $header = array(
    'organizationId' => array(
      'data' => array_key_exists($view_level, $level) ? "{$level[$view_level]} ID" : 'Org ID',
    ),
    'organizationName' => array(
      'data' => array_key_exists($view_level, $level) ? $level[$view_level] : 'Name',
    ),
    'organizationIdentifier' => array(
      'data' => t('Code'),
    ),
    'childLevel' => array(
      'data' => array_key_exists($view_child_level, $levels) ? $levels[$view_child_level] : NULL,
    ),
    'users' => array(
      'data' => t('Users'),
    ),
    'studentCount' => array(
      'data' => t('Students'),
    ),
  );

  // Do not show identifier for state or national level orgs.
  if ($view_level <= PADS_ORGANIZATION_LEVEL_STATE) {
    unset($header['identifier']);
  }

  // Hide user and student counts if user does not have access.
  if (!user_access('view users')) {
    unset($header['users']);
  }
  if (!user_access('view students') && !user_access('view any student')) {
    unset($header['studentCount']);
  }
  // At school level, only show childLevel or user column if user has access.
  if ($view_level == PADS_ORGANIZATION_LEVEL_SCHOOL && !user_access('view any class')) {
    unset($header['childLevel']);
  }

  $rows = array();
  if (!empty($orgs_to_show)) {
    foreach ($orgs_to_show as $org) {
      $rows[] = pads_organization_build_row($org, $header);
    }
  }

  $build['table'] = array(
    '#theme' => empty($rows) ? 'table' : 'datatable',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('There are no organizations.'),
  );

  return $build;
}

/**
 * My States page callback.
 *
 * @param array $org
 *   Organization array.
 * @param bool $list_children
 *   Optional argument to display only the children of $org. Defaults to TRUE.
 *
 * @return array
 *   A lovely Drupal render array.
 */
function pads_states($org, $list_children = TRUE) {
  // Load the org b/c it is coming across as integer here.
  $org = pads_org_load($org);
  $build = array();
  $level = pads_org_levels();
  $levels = pads_org_levels(FALSE, TRUE);

  $orgs_to_show = $list_children ? pads_org_get_children($org['organizationId']) : array($org);

  $view_level = $list_children ? $org['organizationType'] + 1 : $org['organizationType'];
  $view_child_level = $view_level + 1;

  $header = array(
    'organizationId' => array(
      'data' => array_key_exists($view_level, $level) ? "{$level[$view_level]} ID" : 'Org ID',
    ),
    'organizationName' => array(
      'data' => array_key_exists($view_level, $level) ? $level[$view_level] : 'Name',
    ),
    'organizationIdentifier' => array(
      'data' => t('Code'),
    ),
    'childLevel' => array(
      'data' => array_key_exists($view_child_level, $levels) ? $levels[$view_child_level] : NULL,
    ),
    'users' => array(
      'data' => t('Users'),
    ),
    'studentCount' => array(
      'data' => t('Students'),
    ),
  );

  // Do not show identifier for state or national level orgs.
  if ($view_level <= PADS_ORGANIZATION_LEVEL_STATE) {
    unset($header['identifier']);
  }

  // Hide user and student counts if user does not have access.
  if (!user_access('view users')) {
    unset($header['users']);
  }
  if (!user_access('view students')) {
    unset($header['studentCount']);
  }
  // At school level, only show childLevel or user column if user has access.
  if ($view_level == PADS_ORGANIZATION_LEVEL_SCHOOL && !user_access('view classes')) {
    unset($header['childLevel']);
  }

  $rows = array();
  if (!empty($orgs_to_show)) {
    foreach ($orgs_to_show as $org) {
      $rows[] = pads_organization_build_row($org, $header);
    }
  }

  $build['table'] = array(
    '#theme' => empty($rows) ? 'table' : 'datatable',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('There are no organizations.'),
  );

  return $build;
}

/**
 * My Districts page callback.
 *
 * @param array $org
 *   Organization array.
 * @param bool $list_children
 *   Optional argument to display only the children of $org. Defaults to TRUE.
 *
 * @return array
 *   A lovely Drupal render array.
 */
function pads_districts($org, $list_children = TRUE) {
  // Load the org b/c it is coming across as integer here.
  $org = pads_org_load($org);
  $build = array();
  $level = pads_org_levels();
  $levels = pads_org_levels(FALSE, TRUE);

  $orgs_to_show = $list_children ? pads_org_get_children($org['organizationId']) : array($org);

  $view_level = PADS_ORGANIZATION_LEVEL_DISTRICT;
  $view_child_level = $view_level + 1;

  $header = array(
    'organizationId' => array(
      'data' => array_key_exists($view_level, $level) ? "{$level[$view_level]} ID" : 'Org ID',
    ),
    'organizationName' => array(
      'data' => array_key_exists($view_level, $level) ? $level[$view_level] : 'Name',
    ),
    'organizationIdentifier' => array(
      'data' => t('Code'),
    ),
    'childLevel' => array(
      'data' => array_key_exists($view_child_level, $levels) ? $levels[$view_child_level] : NULL,
    ),
    'users' => array(
      'data' => t('Users'),
    ),
    'studentCount' => array(
      'data' => t('Students'),
    ),
  );

  // Do not show identifier for state or national level orgs.
  if ($view_level <= PADS_ORGANIZATION_LEVEL_STATE) {
    unset($header['identifier']);
  }

  // Hide user and student counts if user does not have access.
  if (!user_access('view users')) {
    unset($header['users']);
  }
  if (!user_access('view students')) {
    unset($header['studentCount']);
  }
  // At school level, only show childLevel or user column if user has access.
  if ($view_level == PADS_ORGANIZATION_LEVEL_SCHOOL && !user_access('view classes')) {
    unset($header['childLevel']);
  }

  $rows = array();
  if (!empty($orgs_to_show)) {
    foreach ($orgs_to_show as $org) {
      $rows[] = pads_organization_build_row($org, $header);
    }
  }

  $build['table'] = array(
    '#theme' => empty($rows) ? 'table' : 'datatable',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('There are no organizations.'),
  );

  return $build;
}

/**
 * My Schools page callback.
 *
 * @param array $org
 *   Organization array.
 * @param bool $list_children
 *   Optional argument to display only the children of $org. Defaults to TRUE.
 *
 * @return array
 *   A lovely Drupal render array.
 */
function pads_schools($org, $list_children = TRUE) {
  // Load the org b/c it is coming across as integer here.
  $org = pads_org_load($org);
  $build = array();
  $level = pads_org_levels();
  $levels = pads_org_levels(FALSE, TRUE);

  $orgs_to_show = $list_children ? pads_org_get_children($org['organizationId']) : array($org);

  $view_level = PADS_ORGANIZATION_LEVEL_SCHOOL;
  $view_child_level = $view_level + 1;

  $header = array(
    'organizationId' => array(
      'data' => array_key_exists($view_level, $level) ? "{$level[$view_level]} ID" : 'Org ID',
    ),
    'organizationName' => array(
      'data' => array_key_exists($view_level, $level) ? $level[$view_level] : 'Name',
    ),
    'organizationIdentifier' => array(
      'data' => t('Code'),
    ),
    'childLevel' => array(
      'data' => array_key_exists($view_child_level, $levels) ? $levels[$view_child_level] : NULL,
    ),
    'users' => array(
      'data' => t('Users'),
    ),
    'studentCount' => array(
      'data' => t('Students'),
    ),
  );

  // Do not show identifier for state or national level orgs.
  if ($view_level <= PADS_ORGANIZATION_LEVEL_STATE) {
    unset($header['identifier']);
  }

  // Hide user and student counts if user does not have access.
  if (!user_access('view users')) {
    unset($header['users']);
  }
  if (!user_access('view students')) {
    unset($header['studentCount']);
  }
  // At school level, only show childLevel or user column if user has access.
  if ($view_level == PADS_ORGANIZATION_LEVEL_SCHOOL && !user_access('view classes')) {
    unset($header['childLevel']);
  }

  $rows = array();
  if (!empty($orgs_to_show)) {
    foreach ($orgs_to_show as $org) {
      $rows[] = pads_organization_build_child_row($org, $header);
    }
  }

  $build['table'] = array(
    '#theme' => empty($rows) ? 'table' : 'datatable',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('There are no organizations.'),
  );

  return $build;
}

/**
 * Helper function to build a row in the organizations table.
 *
 * @param array $org
 *   The fully loaded organization object.
 * @param array $header
 *   Table header array expected by theme_table variables argument.
 *
 * @return array
 *   A row array as expected by theme_table variables argument.
 */
function pads_organization_build_row($org, $header) {
  $row = array();
  $header_keys = array_keys($header);
  $org_id = $org['organizationId'];
  foreach ($header_keys as $column) {
    switch ($column) {
      case 'childLevel':
        // If Org is at the school level, find the count of classroom and link
        // to the classes page.
        if ($org['organizationType'] == PADS_ORGANIZATION_LEVEL_SCHOOL) {
          $row[$header[$column]['data']] = l(pads_classes_get_count($org_id), "classes/{$org_id}", array(
            'attributes' => array(
              'id' => 'classes-' . $org_id . '-count',
            ),
          ));
        }
        else {
          $row[$header[$column]['data']] = l($org['childCount'], "organizations/$org_id");
        }

        break;

      case 'studentCount':
        $student_count = $org[$column];
        if ($student_count && $org['organizationType'] > PADS_ORGANIZATION_LEVEL_DISTRICT && user_access('view students')) {
          $row[$column] = l($student_count, "students/$org_id");
        }
        else {
          $row[$column] = $student_count;
        }
        break;

      case 'users':
        $user_count = pads_org_user_count($org_id);
        if (user_access('view users') && $user_count && $org['organizationType'] >= PADS_ORGANIZATION_LEVEL_SCHOOL) {
          $row[$column] = l($user_count, "tapusers/$org_id");
        }
        else {
          $row[$column] = $user_count;
        }
        break;

      default:
        $row[$column] = array_key_exists($column, $org) ? $org[$column] : '';

    }
  }
  return $row;
}

/**
 * Helper function to build a row in the states,districts,schools table.
 *
 * @param array $org
 *   The fully loaded organization object.
 * @param array $header
 *   Table header array expected by theme_table variables argument.
 *
 * @return array
 *   A row array as expected by theme_table variables argument.
 */
function pads_organization_build_child_row($org, $header) {
  $row = array();
  $header_keys = array_keys($header);
  $org_id = $org['organizationId'];
  foreach ($header_keys as $column) {
    switch ($column) {
      case 'childLevel':
        // If Org is at the school level, find the count of classroom and link
        // to the classes page.
        if ($org['organizationType'] == PADS_ORGANIZATION_LEVEL_SCHOOL) {
          $row[$header[$column]['data']] = l(pads_classes_get_count($org_id), "classes/{$org_id}", array(
            'attributes' => array(
              'id' => 'classes-' . $org_id . '-count',
            ),
          ));
        }
        else {
          $row[$header[$column]['data']] = l($org['childCount'], "organizations/$org_id");
        }

        break;

      case 'studentCount':
        $student_count = $org[$column];
        if ($student_count && $org['organizationType'] >= PADS_ORGANIZATION_LEVEL_DISTRICT) {
          $row[$column] = l($student_count, "students/$org_id");
        }
        else {
          $row[$column] = $student_count;
        }
        break;

      case 'users':
        $user_count = pads_org_user_count($org_id);
        if (user_access('view users') && $user_count && $org['organizationType'] >= PADS_ORGANIZATION_LEVEL_SCHOOL) {
          $row[$column] = l($user_count, "tapusers/$org_id");
        }
        else {
          $row[$column] = $user_count;
        }
        break;

      default:
        $row[$column] = array_key_exists($column, $org) ? $org[$column] : '';

    }
  }
  return $row;
}

/**
 * Page callback for providing redirect for the dashboard.
 */
function pads_organization_dashboard_page() {
  if (!user_access('view organizations')) {
    drupal_goto('files');
  }

  $params = func_get_args();
  return call_user_func_array('homebox_build', $params);
}
