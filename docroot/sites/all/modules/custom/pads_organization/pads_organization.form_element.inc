<?php
/**
 * @file
 * Contains organization form element functionality.
 */

/**
 * Implements hook_element_info().
 */
function pads_organization_element_info() {
  $types['pads_org_org_select'] = array(
    '#input' => TRUE,
    '#process' => array(
      'form_process_pads_org_org_select',
      'ajax_process_form',
    ),
    '#theme' => 'pads_org_org_select',
    '#theme_wrappers' => array('pads_org_org_form_element'),
    '#tree' => TRUE,
    '#ajax' => array(
      'callback' => 'pads_org_org_select_ajax',
      'event' => 'change',
      'method' => 'replace',
      // PARADS-1375 Remove progress indicator for now, we just are
      // selecting State level orgs.
      'progress' => 'none',
    ),
  );

  return $types;
}

/**
 * Process pads_org_org_select form element.
 */
function form_process_pads_org_org_select(&$element, &$form_state) {
  // If the form was submitted, look for the pieces to determine the value.
  if ($form_state['process_input']) {
    $hierarchy = pads_organization_get_hierarchy();
    $value = NULL;
    foreach (array_keys(pads_org_levels(TRUE)) as $level) {
      if (!empty($element['#value'][$level])) {
        $item = $element['#value'][$level];
        $parent = (!empty($hierarchy[$item]['parents'][0])) ? $hierarchy[$item]['parents'][0] : NULL;
        if (!$value || ($parent == $value && $hierarchy[$parent]['level'] > $level - 2)) {
          $value = $item;
        }
      }
    }
    $element['#value'] = $value;
    form_set_value($element, $value, $form_state);
  }

  // Allow implementing form to set wrapper, fallback to the wrapper of this
  // form element.
  if (empty($element['#ajax']['wrapper'])) {
    $element['#ajax']['wrapper'] = $element['#id'] . '-wrapper';
  }

  // Add in the JS needed for the element, weight it low so it's added in after
  // Drupal's core JS is loaded but before the AJAX JS proccessing occurs.
  $element['#attached']['js'][] = drupal_get_path('module', 'pads_organization') . '/js/pads_org.js';
  return $element;
}

/**
 * Determines options available in pads_org_org_select form element.
 *
 * The logic is a little tricky, so here's what we're looking for, both of the
 * following need to be present.
 *
 * Choice is available per user's org assignment (any):
 *  - User's has an org assignment which is a parent of the choice.
 *  - Choice matches one of the user's org assignments.
 *  - Choice is a parent of any of the user's org assignments.
 *
 * Choice is available per the selected orgs (any):
 *  - The parent of the item is in the "selected chain".
 *
 * @param int $level
 *   The level for which options are being presented.
 * @param int $value
 *   The org select element's current value.
 *
 * @return array
 *   An array of organizations keyed by ID which are available in the select.
 */
function pads_org_options($level, $value = NULL) {
  $options = array();

  $hierarchy = pads_organization_get_hierarchy();

  // Get the org for the user.
  $orgs = pads_organization_get_user_orgs();

  $org = (!empty($orgs)) ? reset($orgs) : FALSE;

  // Default the selected value to the user's organization.
  $value = (!$value) && $org ? $org : $value;

  /* Ensure the user's org and the selected value are present in the hierarchy.
  if (!array_key_exists($org, $hierarchy) || !array_key_exists($value, $hierarchy)) {
    return $options;
  }
  */

  // Grab the org data into handy variables.
  $selected_org = $selected_chain = FALSE;
  if ($value) {
    $selected_org = $hierarchy[$value];
    $selected_chain = $selected_org['parents'];
    $selected_chain[] = $value;
  }

  $parents = array();
  foreach ($orgs as $item) {
    $parents = array_merge($parents, $hierarchy[$item]['parents']);
  }
  $parents = array_values(array_unique($parents));

  // Add in any org that matches the logic explained above.
  foreach ($hierarchy as $id => $item) {
    if ($item['level'] == $level) {
      // Available per user's org assignment.
      // For now, all orgs are available.
      $in_org = TRUE ? TRUE : in_array($id, $orgs) ||
        in_array($id, $parents) ||
        array_intersect($orgs, $item['parents']);
      // Available per selected items.
      $in_selection = ($level == PADS_ORGANIZATION_LEVEL_STATE) || (!empty($item['parents']) && in_array($item['parents'][0], $selected_chain));
      if ($in_org && $in_selection) {
        $options[$id] = $item['name'];
      }
    }
  }

  // Alpha sort.
  asort($options);

  // Add in the "All" option if available.
  // This occurs when the user has an org that is a parent to this level.
  if ($options) {
    // Take the first option to analyze it's parents.
    $keys = array_keys($options);
    $key = reset($keys);
    if (array_intersect($hierarchy[$key]['parents'], $orgs)) {
      $parent_id = $hierarchy[$key]['parents'][0];
      $options = array($parent_id => t('All')) + $options;
    }
  }

  if ($level == PADS_ORGANIZATION_LEVEL_STATE) {
    $none = ['' => t('- Select a state -')];
    $options = $none + $options;
  }

  return $options;
}

/**
 * Renders markup for options in pads_org_org_select form elements.
 *
 * @param array $options
 *   Keyed array of element options.
 * @param string $value
 *   The form element value.
 *
 * @return string
 *   Returns HTML output.
 */
function pads_org_render_options($options, $value) {
  $hierarchy = pads_organization_get_hierarchy();
  $output = '';

  // Default the selected value to the user's organization.
  $org = pads_organization_get_user_orgs();
  $org = reset($org);

  $value = (!$value) ? $org : $value;
  $selected_org = ($org && array_key_exists($value, $hierarchy)) ? $hierarchy[$value] : reset($hierarchy);

  // Iterate over all available options.
  foreach ($options as $val => $option) {
    $selected = '';
    if (is_numeric($val)) {
      $org = $hierarchy[$val];
      // If the option val matches the form element's value or the value is a
      // parent of the form element's value, select the option.
      if ($val == $value || (in_array($val, $selected_org['parents']) && $selected_org['level'] != $org['level'])) {
        $selected = ' selected="selected"';
      }
    }
    $output .= '<option value="' . check_plain($val) . '"' . $selected . '>' . check_plain($option) . '</option>';
  }
  return $output;
}

/**
 * AJAX callback for handling changes to pads_org_org_select form element.
 *
 * @param array $form
 *   The form object.
 * @param array $form_state
 *   The form state object.
 *
 * @return mixed
 *   The form element to redraw.
 * @throws Exception
 */
function pads_org_org_select_ajax(&$form, &$form_state) {
  // Pluck the pads_org_org_select element out of the form and send it back.
  $element = drupal_array_get_nested_value($form, $form_state['triggering_element']['#array_parents']);
  $output['messages']['#markup'] = theme('status_messages');
  $output['messages']['#weight'] = -1000;
  $output['element'] = $element;
  return $output;
}
