<?php
/**
 * @file
 * Contains organizationfield functionality.
 */

/**
 * Implements hook_field_widget_info().
 */
function pads_organization_field_widget_info() {
  return array(
    'pads_org' => array(
      'label' => t('System organization selection'),
      'description' => t('Hierarchial drop-down lists for selecting an organization.'),
      'field types' => array('list_text'),
      'settings' => array(),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function pads_organization_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  // Widget type: pads_org_org
  $element['#value'] = array();

  // If the form is being re-generated, utilize values from $form_state.
  if (!empty($form_state['triggering_element']) && $form_state['triggering_element']['#field_name'] == $element['#field_name']) {
    $trigger = $form_state['triggering_element'];
    $parents = $trigger['#parents'];
    $depth = ($trigger['#op'] == 'add_org') ? 3 : 2;
    $parents = array_splice($parents, 0, count($parents) - $depth);
    $items = drupal_array_get_nested_value($form_state['values'], $parents);
  }

  // Build values into field type structure.
  foreach ($items as $delta => $item) {
    $element['#value'][$delta] = $item['value'];
  }

  pads_org_field_widget_build_element($element, $form_state);
  return $element;
}

/**
 * Adds the structure of the widget's form elements.
 *
 * This routine does the work to add the form elements that make up the entire
 * widget element.
 *
 * @param array $element
 *   An array representing the element. The only thing needed is for
 *   $element['value'] to be set to an array of values keyed by deltas.
 */
function pads_org_field_widget_build_element(&$element) {

  global $user;

  /* Determine if user has access to manipulate the values in this field.
  if (!empty($element['#entity']->uid) && $user->uid == $element['#entity']->uid) {
    $element['#interactive_access'] = user_access('pads_organization edit own org field');
  }
  else {
    $element['#interactive_access'] = user_access('pads_organization edit org field');
  }
  */
  $element['#interactive_access'] = TRUE;

  if (empty($element['#id'])) {
    $element['#id'] = drupal_html_id('pads-org-field-widget');
  }

  foreach ($element['#value'] as $delta => $value) {
    $element['remove'][$delta] = array(
      '#id' => drupal_html_id($element['#id'] . '-remove-' . $delta),
      '#type' => 'button',
      '#value' => t('Remove'),
      '#op' => 'remove_org',
      '#field_name' => $element['#field_name'],
      '#delta' => $delta,
      '#name' => 'remove_' . $delta,
      '#ajax' => array(
        'callback' => 'pads_org_field_widget_ajax',
        'wrapper' => $element['#id'],
      ),
    );
    $element['remove'][$delta]['#attributes']['class'][] = 'pads-org-widget-remove';
  }

  // Interface to add new organizations.
  $class = 'pads-org-widget-add-wrapper';
  if (count($element['#value'])) {
    $class .= ' hidden';
  }
  $element['add'] = array(
    '#prefix' => '<div class="' . $class . '">',
    '#suffix' => '</div>',
    '#access' => $element['#interactive_access'],
  );
  $element['add']['org'] = array(
    '#type' => 'pads_org_org_select',
    '#field_name' => $element['#field_name'],
    '#op' => 'select_org',
    '#title' => t('Add an Organization'),
  );
  $element['add']['actions'] = array(
    '#type' => 'actions',
    '#weight' => 100,
  );
  $element['add']['actions']['associate'] = array(
    '#type' => 'button',
    '#value' => t('Associate Selected Organization'),
    '#op' => 'add_org',
    '#field_name' => $element['#field_name'],
    '#ajax' => array(
      'callback' => 'pads_org_field_widget_ajax',
      'wrapper' => $element['#id'],
    ),
  );
  $element['add']['actions']['associate']['#attributes']['class'][] = 'pads-org-widget-associate';
  $element['add']['actions']['associate']['#states'] = array(
    'disabled' => array(
      ':input[name="profile_pads_organization[field_state][und][add][org][2]"]' => array(
        'value' => '',
      ),
    ),
  );

  // Remove button only available if user has access to edit field.
  $element['remove']['#access'] = $element['#interactive_access'];

  $element += array(
    '#element_validate' => array('pads_org_field_widget_validate'),
    '#theme' => 'pads_org_org_select_widget',
  );
}

/**
 * Element validation routine for the PADS Organization form widget.
 *
 * The main function of this code is to analyze which button was clicked,
 * perform the appropriate changes to the element's value, and set the value
 * into $form_state.
 */
function pads_org_field_widget_validate($element, &$form_state) {

  $values = $element['#value'];

  // Handle adjustments made via add and remove actions.
  if (!empty($form_state['triggering_element']) && array_key_exists('#field_name', $form_state['triggering_element']) && $form_state['triggering_element']['#field_name'] == $element['#field_name']) {
    $trigger = $form_state['triggering_element'];
    if (!empty($trigger['#op'])) {
      switch ($trigger['#op']) {
        case 'add_org':
          $values[] = $element['add']['org']['#value'];
          break;

        case 'remove_org':
          unset($values[$trigger['#delta']]);
          break;
      }
    }
  }

  // Build the submitted values into values the field storage can handle.
  $values = array_values(array_unique($values));
  foreach ($values as &$value) {
    $value = array('value' => $value);
  }

  // We are changing the form value here, but the required check has already
  // been performed so we perform the check again here.
  if (
    ($form_state['submitted'] && empty($values) && $element['#required']) ||
    (isset($trigger) && array_key_exists('#op', $trigger) && $trigger['#op'] == 'add_org') && (empty($value) or !($value['value']))
  ) {
    $t = get_t();
    form_error($element, $t('!name field is required.', array('!name' => $element['#title'])));
  }
  form_set_value($element, $values, $form_state);
}

/**
 * AJAX callback for servicing the multi-org widget.
 *
 * Analyze the "triggering element" and send the entire field element back to
 * the browser.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form state object.
 *
 * @return mixed
 *   The form element to redraw.
 * @throws Exception
 */
function pads_org_field_widget_ajax(&$form, &$form_state) {
  $trigger = $form_state['triggering_element'];

  // Grab the element and values from the form.
  $array_parents = $trigger['#array_parents'];
  // Need to go one level further up if this was an "add" operation.
  $depth = ($trigger['#op'] == 'add_org') ? 3 : 2;
  $array_parents = array_splice($array_parents, 0, count($array_parents) - $depth);
  $element = drupal_array_get_nested_value($form, $array_parents);

  // Reset the org select element to default on add operation.
  if ($trigger['#op'] == 'add_org' && array_key_exists('#default_value', $element['add']['org'])) {
    $element['add']['org']['#value'] = $element['add']['org']['#default_value'];
  }

  // Add in status messages. Only for debugging, can be removed.
  $output['messages']['#markup'] = theme('status_messages');
  $output['messages']['#weight'] = -1000;
  $output['element'] = $element;

  return $output;
}

/**
 * Implements hook_field_formatter_info().
 */
function pads_organization_field_formatter_info() {
  return array(
    'field_org_name' => array(
      'label' => t('Organization Name'),
      'field types' => array('list_text'),
    ),
    'field_org_code' => array(
      'label' => t('Organization Code'),
      'field types' => array('list_text'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function pads_organization_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  switch ($display['type']) {
    case 'field_org_name':
      foreach ($items as $delta => $item) {
        $org_id = $item['value'];
        $org = pads_org_load($org_id);
        if ($org) {
          $element[$delta] = array('#markup' => $org['organizationName']);
        }
      }
      break;

    case 'field_org_code':
      foreach ($items as $delta => $item) {
        $org_id = $item['value'];
        $org = pads_org_load($org_id);
        if ($org) {
          $element[$delta] = array('#markup' => $org['organizationIdentifier']);
        }
      }
      break;
  }
  return $element;
}
