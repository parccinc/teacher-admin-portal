<?php

/**
 * @file
 * Pads Organization hierarchical select hooks.
 */

/**
 * Implements hook_hierarchical_select_children().
 *
 * See hs_taxonomy_hierarchical_select_children() for default implementation.
 */
function pads_organization_hierarchical_select_children($parent, $params) {
  $factory = \Drupal\pads_organization\OrganizationFactory::getTermLdrOrganizationInstance();

  // Gather user org assignment info.
  $user_org_ids = array_keys(pads_organization_get_user_orgs());

  // Gather selection option terms and organization.
  $terms = taxonomy_get_children($parent, $params['vid']);
  foreach ($terms as $tid => $term) {
    // Variable used to avoid additional checks after access is granted.
    $access = FALSE;
    $term_org = $factory->getOrgFromTerm($term);

    // Access by permission.
    if (user_access('assign any organization')) {
      $access = TRUE;
    }

    // Direct Match.
    if (!$access && in_array($term_org['organizationId'], $user_org_ids)) {
      $access = TRUE;
    }

    // Term is a parent of user org.
    if (!$access && pads_organization_user_has_child_org($term_org)) {
      $access = TRUE;
    }

    // User org is a parent of term.
    if (!$access && pads_organization_user_has_parent_org($term_org)) {
      $access = TRUE;
    }

    // If user does not have access, remove.
    if (!$access) {
      unset($terms[$tid]);
    }
  }


  return _hs_taxonomy_hierarchical_select_terms_to_options($terms);
}


/**
 * Implements hook_hierarchical_select_valid_item().
 */
function pads_organization_hierarchical_select_valid_item($item, $params) {
  return hs_taxonomy_hierarchical_select_valid_item($item, $params);
}

/**
 * Implements hook_hierarchical_select_create_item().
 */
function pads_organization_hierarchical_select_create_item($label, $parent, $params) {
  return hs_taxonomy_hierarchical_select_create_item($label, $parent, $params);
}

/**
 * Implements hook_hierarchical_select_lineage().
 */
function pads_organization_hierarchical_select_lineage($item, $params) {
  return hs_taxonomy_hierarchical_select_lineage($item, $params);
}

/**
 * Implements hook_hierarchical_select_item_get_label().
 */
function pads_organization_hierarchical_select_item_get_label($item, $params) {
  return hs_taxonomy_hierarchical_select_item_get_label($item, $params);
}

/**
 * Implements hook_hierarchical_select_root_level().
 */
function pads_organization_hierarchical_select_root_level($params) {
  return hs_taxonomy_hierarchical_select_root_level($params);
}

/**
 * Implements hook_hierarchical_select_params().
 */
function pads_organization_hierarchical_select_params() {
  return hs_taxonomy_hierarchical_select_params();
}
