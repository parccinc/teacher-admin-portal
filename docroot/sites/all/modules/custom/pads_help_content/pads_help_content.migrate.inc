<?php
/**
 * @file
 * PADS Help Content migrate definitions.
 */

/**
 * Impelements hook_migrate_api().
 */
function pads_help_content_migrate_api() {
  return array(
    'api' => 2,
    'groups' => array(
      'help' => array(
        'title' => t('PADS Help Content Migration'),
      ),
    ),
    'migrations' => array(
      'PadsHelpContent' => array(
        'class_name' => 'PadsHelpContentMigration',
        'group_name' => 'help',
      ),
    ),
  );
}
