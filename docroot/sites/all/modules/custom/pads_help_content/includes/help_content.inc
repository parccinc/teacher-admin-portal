<?php
/**
 * @file
 * Migration for TAP help content.
 */

class PadsHelpContentMigration extends Migration {

  /**
   * {@inheritdoc}
   */
  public function __construct($arguments) {
    parent::__construct($arguments);

    // Not sure why this setting is needed now but it is.
    ini_set('auto_detect_line_endings', TRUE);

    $this->description = t('Import original TAP Help Content.');
    $options = array(
      'header_rows' => 1,
      'embedded_newlines' => TRUE,
    );
    $this->source = new MigrateSourceCSV(drupal_get_path('module', 'pads_help_content') . '/includes/data/help_content.csv', array(), $options);
    $this->destination = new MigrateDestinationNode('help');

    $this->map = new MigrateSQLMap(
      $this->machineName,
      array(
        'title' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
          'description' => 'Help Content Title',
          'alias' => 'm',
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );

    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('body', 'body');
    $this->addFieldMapping('body:format')
      ->defaultValue('filtered_html');
    $this->addFieldMapping('field_short_tagline', 'field_short_tagline');
    $this->addFieldMapping('field_accordion_header', 'field_accordion_header');
    $this->addFieldMapping('field_display_on_front_page', 'field_display_on_front_page');
    $this->addFieldMapping('field_help_icon:icon', 'field_help_icon');
    $this->addFieldMapping('field_help_icon', 'field_help_icon_bundle');
    $this->addFieldMapping('log')
      ->defaultValue('Imported by PADS Help Content migration.')
      ->description(t('Default Help Content for TAP Help Nodes.'));

    // List of destination fields that we have no intention of migrating.
    $this->addUnmigratedDestinations(array(
      'body:summary',
      'uid',
      'created',
      'changed',
      'status',
      'promote',
      'sticky',
      'revision',
      'language',
      'tnid',
      'translate',
      'revision_uid',
      'is_new',
      'path',
      'pathauto',
      'comment',
    ));

  }
}
