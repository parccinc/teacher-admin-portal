<?php
/**
 * @file
 * Defines PadsHelpContentContext.
 */

use Drupal\DrupalExtension\Context\DrupalSubContextBase;
use Drupal\DrupalExtension\Context\DrupalSubContextInterface;

class PadsHelpContentContext extends DrupalSubContextBase implements DrupalSubContextInterface {

  /**
   * Gets Drupal\DrupalExtension\Context\DrushContext.
   *
   * @return Drupal\DrupalExtension\Context\DrushContext
   *   An instance of the DrushContext.
   */
  protected function getDrushContext() {
    return $this->getContext('Drupal\DrupalExtension\Context\DrushContext');
  }
  /**
   * Imports the help content.
   *
   * @Given the help content has been imported
   */
  public function assertHelpContent() {
    $drush_context = $this->getDrushContext();
    $drush_context->assertDrushCommand('mreg');
    $drush_context->assertDrushCommandWithArgument('mi', '--update PadsHelpContent');

    // The drush mi command returns no output on success.
    $output = $drush_context->readDrushOutput();
    if (!empty($output)) {
      throw new \Exception(sprintf("The last drush command output was not empty.\nInstead, it was:\n\n%s'", $output));
    }
  }

}
