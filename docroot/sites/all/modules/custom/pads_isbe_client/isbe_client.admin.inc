<?php
/**
 * @file
 * Form methods for ISBE Client config form.
 */

/**
 * Form callback for ISBE client.
 *
 * @param array $form
 *   An empty form object.
 * @param array $form_state
 *   The form_state object.
 *
 * @return array
 *   The systems setting form.
 */
function isbe_client_admin_form($form, &$form_state) {
  $form = array();
  $form['isbe'] = array(
    '#type' => 'fieldset',
    '#title' => t('ISBE Settings'),
  );
  $form['isbe']['pads_isbe_client_service_api_uri'] = array(
    '#type' => 'textfield',
    '#title' => 'ISBE Services API',
    '#description' => 'URI location of ISBE without trailing slash',
    '#default_value' => variable_get('pads_isbe_client_service_api_uri'),
  );
  $form['isbe']['pads_isbe_client_service_ui_uri'] = array(
    '#type' => 'textfield',
    '#title' => 'ISBE Services UI',
    '#description' => 'URI location of ISBE Services UI without trailing slash',
    '#default_value' => variable_get('pads_isbe_client_service_ui_uri'),
  );
  $form['pads'] = array(
    '#type' => 'fieldset',
    '#title' => t('ADS Teach Settings'),
  );
  $form['pads']['pads_isbe_client_vendor_id'] = array(
    '#type' => 'textfield',
    '#title' => 'Vendor ID',
    '#description' => 'ISBE-assigned Vendor ID',
    '#default_value' => variable_get('pads_isbe_client_vendor_id'),
  );
  $form['pads']['pads_isbe_client_3des_key'] = array(
    '#type' => 'textfield',
    '#title' => '3DES Key',
    '#description' => 'ISBE-assigned 3DES key',
    '#default_value' => variable_get('pads_isbe_client_3des_key'),
  );
  $form['pads']['pads_isbe_client_username'] = array(
    '#type' => 'textfield',
    '#title' => 'Username',
    '#description' => 'ISBE username.',
    '#default_value' => variable_get('pads_isbe_client_username'),
  );
  $form['pads']['pads_isbe_client_password'] = array(
    '#type' => 'password',
    '#title' => 'Password',
    '#description' => 'ISBE password. Leave blank to keep unchanged.',
  );
  $form['pads']['pads_isbe_client_scope'] = array(
    '#type' => 'textfield',
    '#title' => 'Scope',
    '#description' => 'ISBE scope value.',
    '#default_value' => variable_get('pads_isbe_client_scope', 'assessments'),
  );
  return system_settings_form($form);
}

/**
 * Validate ISBE client admin settings form.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form_state array.
 */
function isbe_client_admin_form_validate($form, &$form_state) {
  $password = $form_state['values']['pads_isbe_client_password'];
  if (empty($password)) {
    form_set_value($form['pads']['pads_isbe_client_password'], variable_get('pads_isbe_client_password'), $form_state);
  }
  else {
    if (module_exists('encrypt')) {
      $password = encrypt($password);
    }
    form_set_value($form['pads']['pads_isbe_client_password'], $password, $form_state);
  }
}
