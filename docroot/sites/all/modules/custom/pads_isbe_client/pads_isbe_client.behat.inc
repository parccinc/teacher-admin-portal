<?php
/**
 * @file
 * Contains \IsbeContext.
 */

use Drupal\DrupalDriverManager;
use Drupal\DrupalExtension\Context\DrupalSubContextBase;
use Drupal\DrupalExtension\Context\DrupalSubContextInterface;

/**
 * Class IsbeContext.
 */
class IsbeContext extends DrupalSubcontextBase implements DrupalSubContextInterface {

  /**
   * PadsMinkContext context.
   *
   * @var \PadsMinkContext
   */
  protected $minkContext;

  /**
   * {@inheritdoc}
   */
  public function __construct(DrupalDriverManager $drupal) {
    parent::__construct($drupal);
    if ($drupal->getEnvironment()) {
      $this->minkContext = $this->getContext('PadsMinkContext');
    }
  }

  /**
   * Verifies we are on the ISBE Management page.
   *
   * @Then I should be on the ISBE Management page
   */
  public function assertIsbeManagementPage() {
    $url = $this->getSession()->getCurrentUrl();
    $isbe_url = variable_get('pads_isbe_client_service_ui_uri', '');

    $url_parsed = parse_url($url);
    $isbe_url_parsed = parse_url($isbe_url);

    if ($url_parsed['host'] != $isbe_url_parsed['host']) {
      throw new \Exception(sprintf('Expecting %s, found %s', $isbe_url, $url));
    }
  }

  /**
   * Returns iframe src value.
   *
   * @param string $selector
   *   Selector engine name.
   * @param string $locator
   *   Selector locator.
   *
   * @return string
   *   Iframe src value.
   *
   * @throws \Exception
   */
  protected function getIframeUrl($selector, $locator) {
    if (!$iframe = $this->getSession()->getPage()->find($selector, $locator)) {
      throw new Exception('Not able to find ISBE Management iframe.');
    }

    return $iframe->getAttribute('src');
  }

  /**
   * @Then the iframe source is set to ISBE Management
   */
  public function assertIframeSource() {
    $url = $this->getIframeUrl('css', 'iframe');
    $isbe_url = variable_get('pads_isbe_client_service_ui_uri', '');

    $url_parsed = parse_url($url);
    $isbe_url_parsed = parse_url($isbe_url);

    if ($url_parsed['host'] != $isbe_url_parsed['host']) {
      throw new \Exception(sprintf('Expecting %s, found %s', $isbe_url, $url));
    }
  }

  /**
   * @Then the decrypted query string should contain :value for :property
   */
  public function assertDecryptedValue($value, $property) {
    // Get the query string.
    $url = $this->getIframeUrl('css', 'iframe');
    $pattern = '/(?:^[^?#]*\?([^#]*).*$)?.*/';
    preg_match($pattern, $url, $matches);
    if (!$matches || empty($matches[1])) {
      throw new \Exception(sprintf('Expecting a query string for url %s, found none.', $url));
    }
    $querystring = $matches[1];
    $data = drupal_get_query_array($querystring);
    if (!array_key_exists('userParams', $data)) {
      throw new \Exception(sprintf('Expecting userParams in querystring, found %s', $querystring));
    }
    $data = base64_decode($data['userParams']);

    if (variable_get('pads_isbe_services_3des_enabled', TRUE)) {
      $iv_size = mcrypt_get_iv_size(MCRYPT_3DES, MCRYPT_MODE_CBC);
      $key = variable_get('pads_isbe_client_3des_key', '');
      $iv = substr($data, 0, $iv_size);
      $data = str_replace($iv, "", $data);
      $data = mcrypt_decrypt(MCRYPT_3DES, $key, $data, MCRYPT_MODE_CBC, $iv);
    }
    $data = drupal_get_query_array($data);
    if (!array_key_exists($property, $data)) {
      throw new \Exception(sprintf('Expecting %s in querystring but did not find it.', $property));
    }
    if ($data[$property] != $value) {
      throw new \Exception(sprintf('Expecting %s in %s, found %s.', $value, $property, $data[$property]));
    }
  }

}
