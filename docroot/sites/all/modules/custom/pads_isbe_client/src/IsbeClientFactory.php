<?php
/**
 * @file
 * Contains \IsbeClientFactory.
 */

namespace Drupal\pads_isbe_client;

class IsbeClientFactory {

  /**
   * Returns instance of IsbeClient.
   *
   * @return array|\Drupal\pads_isbe_client\IsbeClient
   *   IsbeClient instance.
   */
  public static function getInstance() {
    $client = &drupal_static(__CLASS__ . '::' . __FUNCTION__);
    if (!isset($client) || empty($client)) {
      $config = array(
        'description_path' => __DIR__ . '/isbe-api.php',
      );
      $client = new IsbeClient($config);

    }
    return $client;
  }

}
