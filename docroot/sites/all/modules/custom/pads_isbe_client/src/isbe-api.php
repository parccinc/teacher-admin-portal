<?php
/**
 * @file
 * Define Isbe Service description data.
 */

$base_url = variable_get('pads_isbe_client_service_api_uri');
return [
  'baseUrl' => rtrim($base_url, '/\\') . '/',
  'defaults' => [
    'curl' => [],
  ],
  'operations' => [
    // Access operations.
    'getAccessToken' => [
      'httpMethod' => 'POST',
      'uri' => 'isbeassessmentservices/api/vendor/GetAccessToken',
      'responseModel' => 'TokenResource',
      'parameters' => [
        'userName' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'json',
          'default' => variable_get('pads_isbe_client_username'),
        ],
        'password' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'json',
          'default' => variable_get('pads_isbe_client_password'),
        ],
        'scope' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'json',
          'default' => variable_get('pads_isbe_client_scope', 'assessments'),
        ],
      ],
    ],

    // Process operations.
    'bTAddTestingStudents' => [
      'httpMethod' => 'POST',
      'uri' => 'isbeassessmentservices/api/vendor/BTAddTestingStudents',
      'responseModel' => 'Resource',
      'parameters' => [
        'Authorization' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'header',
          // 'default' => pads_isbe_client_get_token(),
        ],
        'vendorId' => [
          'required' => TRUE,
          'type' => 'number',
          'location' => 'json',
          'default' => variable_get('pads_isbe_client_vendorid', 1),
        ],
        'batch' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'json',
        ],
      ],
    ],

    'processVendorResponse' => [
      'httpMethod' => 'POST',
      'uri' => 'isbeassessmentservices/api/vendor/ProcessVendorResponse',
      'responseModel' => 'Resource',
      'parameters' => [
        'Authorization' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'header',
          // 'default' => pads_isbe_client_get_token(),
        ],
        'vendorId' => [
          'required' => TRUE,
          'type' => 'number',
          'location' => 'json',
          'default' => variable_get('pads_isbe_client_vendorid', 1),
        ],
        'batch' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'json',
        ],
      ],
    ],
  ],
  'models' => [
    'Resource' => [
      'type' => 'object',
      'additionalProperties' => [
        'location' => 'json',
      ],
    ],
    'TokenResource' => [
      'type' => 'array',
      'location' => 'json',
      'name' => 'token',
      'items' => [
        'type' => 'object',
        'additionalProperties' => [
          'type' => 'string',
        ],
      ],
    ],
  ],
];
