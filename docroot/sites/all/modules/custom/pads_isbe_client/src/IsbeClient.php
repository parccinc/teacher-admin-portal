<?php
/**
 * @file
 * Contains \Drupal\pads_isbe_client\IsbeClient.
 */

namespace Drupal\pads_isbe_client;

use Drupal\pads_client\PadsClient;

composer_manager_register_autoloader();

/**
 * Class IsbeClient.
 *
 * @package Drupal\pads_isbe_client
 */
class IsbeClient extends PadsClient {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $config) {
    parent::__construct($config);
  }

  /**
   * {@inheritdoc}
   */
  public function call($operation, array $args = []) {
    return parent::call($operation, $args);
  }
}
