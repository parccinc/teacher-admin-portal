<?php
/**
 * @file
 * PADS tap users views functionality.
 */

function pads_tap_users_views_data_alter(&$data) {
  $data['users']['pads_tap_role'] = array(
    'real field' => 'uid',
    'field' => array(
      'title' => t('TAP user role'),
      'help' => t('Displays name of first TAP role found'),
      'handler' => 'pads_tap_users_views_handler_field_pads_tap_role',
      'click sortable' => TRUE,
    ),
  );

  // Allow edit user link in views if user has 'edit user' access and assigned
  // a parent Organization of editee.
  $data['users']['edit_node']['field']['handler'] = 'pads_views_handler_field_user_link_edit';
}
