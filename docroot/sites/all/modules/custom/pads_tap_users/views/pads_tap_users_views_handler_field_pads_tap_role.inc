<?php
/**
 * @file
 * Contains \pads_tap_users_views_handler_field_pads_tap_role.
 */

class pads_tap_users_views_handler_field_pads_tap_role extends views_handler_field {

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    $uid = $this->get_value($values);
    $user = user_load($uid);
    $tap_roles = pads_tap_roles();

    $user_tap_roles = array();
    foreach ($tap_roles as $tap_role_id => $tap_role_name) {
      if (user_has_role($tap_role_id, $user)) {
        $user_tap_roles[] = $tap_role_name;
      }
    }
    return implode(', ', $user_tap_roles);
  }
}
