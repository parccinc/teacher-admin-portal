<?php
/**
 * @file
 * Ctools access plugin for user class assignments.
 */

$plugin = array(
  'title' => t("PADS: Access to assign user class"),
  'description' => t('Control access based upon a node belonging to a book.'),
  'callback' => 'pads_tap_users_assign_classes_access_check',
  'default' => array(),
  'settings form' => 'pads_tap_users_assign_classes_access_check_settings',
  'summary' => 'pads_tap_users_assign_classes_access_check_settings_summary',
  'required context' => array(
    new ctools_context_required(t('Accessing user'), 'user'),
    new ctools_context_required(t('Accessed user'), 'user'),
  ),
);

/**
 * Access callback with logic to determine if the user may assign classes.
 */
function pads_tap_users_assign_classes_access_check($conf, $context) {
  $accessor = $context[0]->data;
  $accessee = $context[1]->data;

  // Same user, no good.
  if ($accessor->uid == $accessee->uid) {
    return FALSE;
  }

  // Access by organization assignment.
  if (!tap_user_edit_access($accessee, $accessor)) {
    return FALSE;
  }

  // User has no classes.
  if (!$classes = pads_classes_get_user_classes($accessee)) {
    return FALSE;
  }

  return TRUE;
}

/**
 * Settings form.
 *
 * This is needed to provide context selection in Panels UI.
 */
function pads_tap_users_assign_classes_access_check_settings($form, &$form_state, $conf) {
  return $form;
}

/**
 * Summary for pads_tap_users_assign_classes_access_check access plugin.
 *
 * @param null|array $conf
 *   Settings array, if exists.
 * @param object $context
 *   Ctools context object.
 *
 * @return string
 *   The summary.
 */
function pads_tap_users_assign_classes_access_check_settings_summary($conf, $context) {
  $accessor = $context[0];
  $accessee = $context[1];
  return t('@identifier may access @identifier2', array('@identifier' => $accessor->identifier, '@identifier2' => $accessee->identifier));
}
