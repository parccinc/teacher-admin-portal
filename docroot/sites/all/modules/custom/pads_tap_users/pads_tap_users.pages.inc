<?php
/**
 * @file
 * Page functions for the pads_tap_users module.
 */

/**
 * Page callback for parent tapusers.
 *
 * Redirects to tapusers/%org_id serviced through Page Manager.
 */
function pads_tap_users_callback() {
  $user_orgs = pads_organization_get_user_orgs();
  if (empty($user_orgs) || !$org = reset($user_orgs)) {
    drupal_set_message(t('Unable to determine user Organizations'), 'error');
    drupal_goto('<front>');
  }
  drupal_goto("tapusers/{$org['organizationId']}");
}

/**
 * Page callback for password reset request confirmation page.
 */
function reset_requested_page() {
  $markup = variable_get('pads_tap_users_reset_password_message', t('The email link will expire after 24 hours, and nothing will happen if the link is not used.'));
  return array('#markup' => '<p>' . $markup . '</p>');
}
