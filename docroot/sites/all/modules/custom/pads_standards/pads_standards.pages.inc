<?php

/**
 * @file
 * PADS Standards page callbacks.
 */

function pads_domain_cluster_view_page($standard, $js) {
  $w = entity_metadata_wrapper('node', $standard);

  $domain_cluster = $w->field_domain->value() . '-' . $w->field_cluster_number->value();
  drupal_set_title($domain_cluster);

  $output = node_view_multiple(array($standard->nid => $standard), 'full');

  $fields = array(
    'field_domain',
    'field_domain_text',
    'field_cluster_number',
    'field_cluster_name',
  );
  $output = array_intersect_key($output['nodes'][$standard->nid], array_flip($fields));

  if ($js) {
    ctools_include('modal');
    ctools_modal_render($domain_cluster, drupal_render($output));
    drupal_exit();
  }

  return $output;
}

/**
 * Page callback for rendering a standard.
 */
function pads_standards_view_page($standard, $js) {
  drupal_set_title($standard->title);

  $output = node_view_multiple(array($standard->nid => $standard), 'full');

  // If field_evidence_statement_text is empty, it's a math fluency standard and
  // we want to display the evidence statement label as "Content Standard".
  if (empty($standard->field_evidence_statement_text)) {
    $standard_nid = isset($standard->nid) ? $standard->nid : 0;
    if (array_key_exists($standard_nid, $output['nodes'])) {
      $output['nodes'][$standard_nid]['field_evidence_statement']['#title']
        = t('Content Standard');
    }
  }

  $fields = array(
    'field_domain',
    'field_domain_text',
    'field_cluster_number',
    'field_cluster_name',
    'field_evidence_statement',
    'field_evidence_statement_text',
    'field_fluency_skill_area',
    'field_item_strand',
    'field_item_strand_text',
  );
  $output = array_intersect_key($output['nodes'][$standard->nid], array_flip($fields));

  if ($js) {
    ctools_include('modal');
    ctools_modal_render($standard->title, drupal_render($output));
    drupal_exit();
  }

  return $output;
}
