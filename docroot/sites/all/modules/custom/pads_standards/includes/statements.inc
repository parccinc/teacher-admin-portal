<?php
/**
 * @file
 * Migration for evidence statement data.
 */

/**
 * Class PadsStandardsMigration.
 */
class PadsStandardsMigration extends Migration {

  /**
   * {@inheritdoc}
   */
  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->description = t('Import evidence statement data from the Pearson CC in a csv.');
    $path = dirname(__FILE__) . '/data';
    $options = array(
      'embedded_newlines' => TRUE,
      'header_rows' => TRUE,
    );
    $this->source = new StandardsMigrateSource($path . '/PARCC_MathComp.csv', array(), $options);
    $this->destination = new MigrateDestinationNode('pads_standards');

    $this->map = new MigrateSQLMap(
      $this->machineName,
      array(
        'standardID' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
          'description' => 'Evidence Statement ID',
          'alias' => 'm',
        ),
        'itemStrand' => array(
          'type' => 'varchar',
          'length' => 255,
          'default' => '',
          'description' => 'Math Fluency Item Strand',
          'alias' => 'n',
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );

    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('field_domain', 'domain');
    $this->addFieldMapping('field_domain_text', 'domainText');
    $this->addFieldMapping('field_cluster_number', 'clusterNumber');
    $this->addFieldMapping('field_cluster_name', 'clusterTestName');
    $this->addFieldMapping('field_evidence_statement', 'standardID');
    $this->addFieldMapping('field_evidence_statement_text', 'evidenceStatementText');
    $this->addFieldMapping('field_fluency_skill_area', 'fluencySkillArea');
    $this->addFieldMapping('field_item_strand', 'itemStrand');
    $this->addFieldMapping('field_item_strand_text', 'itemStrandText');
    $this->addFieldMapping('log')
      ->defaultValue('Imported by PADS statement migration.')
      ->description(t('Adding content from a csv.'));

    // List of destination fields that we have no intention of migrating.
    $this->addUnmigratedDestinations(array(
      'uid',
      'created',
      'changed',
      'status',
      'promote',
      'sticky',
      'revision',
      'language',
      'tnid',
      'translate',
      'revision_uid',
      'is_new',
      'path',
      'comment',
    ));

    $this->addUnmigratedSources(array(
      'clusterTestName',
    ));
  }

}

/**
 * Class StandardsMigrateSource.
 */
class StandardsMigrateSource extends MigrateSourceCSV {

  /**
   * {@inheritdoc}
   */
  public function __construct($path, array $csvcolumns = array(), array $options = array(), array $fields = array()) {
    $fields['title'] = t('Uses itemStrand if exists, otherwise standardID.');
    parent::__construct($path, $csvcolumns, $options, $fields);
  }

}
