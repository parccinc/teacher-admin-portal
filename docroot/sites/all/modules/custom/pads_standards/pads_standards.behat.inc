<?php
/**
 * @file
 * Defines PadsStandardsContext.
 */

use Drupal\DrupalExtension\Context\DrupalSubContextBase;

class PadsStandardsContext extends DrupalSubContextBase {

  /**
   * Gets Drupal\DrupalExtension\Context\DrushContext.
   *
   * @return Drupal\DrupalExtension\Context\DrushContext
   *   An instance of the DrushContext.
   */
  protected function getDrushContext() {
    return $this->getContext('Drupal\DrupalExtension\Context\DrushContext');
  }

  /**
   * Imports the standards content.
   *
   * @Given the standards have been imported
   */
  public function assertStandardsImported() {
    $drush_context = $this->getDrushContext();
    $drush_context->assertDrushCommand('mreg');
    $drush_context->assertDrushCommandWithArgument('mi', '--update PadsStandards');
    // The drush mi command returns no output on success.
    $output = $drush_context->readDrushOutput();
    if (!empty($output)) {
      throw new \Exception(sprintf("The last drush command output was not empty.\nInstead, it was:\n\n%s'", $output));
    }
  }

  /**
   * @When I am viewing the standard :standardID
   */
  public function assertViewingStandard($standard_id) {
    if (!$standard = pads_standard_load($standard_id)) {
      throw new Exception(sprintf('Unable to load standard "%s".', $standard_id));
    }

    drupal_lookup_path('wipe');
    $path = '/' . drupal_get_path_alias("node/{$standard->nid}");
    $this->getSession()->visit($path);
  }

  /**
   * @Then I should be on standard :standardID
   */
  public function assertOnStandard($standard_id) {
    if (!$standard = pads_standard_load($standard_id)) {
      throw new Exception(sprintf('Unable to load standard "%s".', $standard_id));
    }

    drupal_lookup_path('wipe');
    $path = '/' . drupal_get_path_alias("node/{$standard->nid}");
    $this->assertSession()->addressEquals($this->locatePath($path));
  }

  /**
   * @Then I should be on the modal for standard :standardID
   */
  public function assertOnStandardModal($standard_id) {
    drupal_lookup_path('wipe');
    $path = '/' . drupal_get_path_alias("standards/evidence/{$standard_id}/nojs/view");
    $this->assertSession()->addressEquals($this->locatePath($path));
  }

  /**
   * @Then I should be on the modal for domain cluster :domain_cluster
   */
  public function assertOnDomainClusterModal($domain_cluster) {
    drupal_lookup_path('wipe');
    $path = '/' . drupal_get_path_alias("standards/domain/{$domain_cluster}/nojs/view");
    $this->assertSession()->addressEquals($this->locatePath($path));
  }

}
