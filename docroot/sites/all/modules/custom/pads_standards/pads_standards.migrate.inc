<?php
/**
 * @file
 * PADS Standards migrate definitions.
 */

/**
 * Impelements hook_migrate_api().
 */
function pads_standards_migrate_api() {
  return array(
    'api' => 2,
    'groups' => array(
      'standards' => array(
        'title' => t('PADS Standards Migration'),
      ),
    ),
    'migrations' => array(
      'PadsStandards' => array(
        'class_name' => 'PadsStandardsMigration',
        'group_name' => 'standards',
      ),
    ),
  );
}
