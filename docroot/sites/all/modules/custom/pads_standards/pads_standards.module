<?php

/**
 * @file
 * Code for the PADS Standards module.
 */

/**
 * Implements hook_menu().
 */
function pads_standards_menu() {
  $items['standards/domain/%pads_domain_cluster/%ctools_js/view'] = array(
    'title' => 'View Score Report',
    'page callback' => 'pads_domain_cluster_view_page',
    'page arguments' => array(2, 3),
    'access callback' => 'node_access',
    'access arguments' => array('view', 2),
    'file' => 'pads_standards.pages.inc',
  );

  $items['standards/evidence/%pads_standard/%ctools_js/view'] = array(
    'title' => 'View Score Report',
    'page callback' => 'pads_standards_view_page',
    'page arguments' => array(2, 3),
    'access callback' => 'node_access',
    'access arguments' => array('view', 2),
    'file' => 'pads_standards.pages.inc',
  );

  return $items;
}

/**
 * Load domain/cluster by identifier.
 *
 * @param string $domain_cluster
 *   The domain/cluster identifier.
 *
 * @return object
 *   The first matching standard node, NULL if unable to find a corresponding
 *   node.
 */
function pads_domain_cluster_load($domain_cluster) {
  $domain_cluster = explode('-', $domain_cluster, 2);
  if (count($domain_cluster) < 2) {
    return;
  }

  list($domain, $cluster) = $domain_cluster;

  $q = new EfqHelper('node');
  $q->propertyCondition('type', 'pads_standards')
    ->fieldCondition('field_cluster_number', 'value', $cluster)
    ->fieldCondition('field_domain', 'value', $domain);
  return $q->first();
}

/**
 * Load a standard by ID.
 *
 * @param string $standard_id
 *   The standard's ID.
 *
 * @return object
 *   The standard node, NULL if unable to find corresponding node.
 */
function pads_standard_load($standard_id) {
  $standard_id = urldecode($standard_id);
  $q = new EfqHelper('node');
  $q->propertyCondition('type', 'pads_standards')
    ->propertyCondition('title', $standard_id);
  return $q->first();
}
