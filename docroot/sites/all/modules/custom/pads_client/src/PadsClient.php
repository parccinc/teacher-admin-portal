<?php
/**
 * @file
 * Contains \Drupal\pads_client\PadsClient.
 */

namespace Drupal\pads_client;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Collection;
use GuzzleHttp\Command\Guzzle\Description;
use GuzzleHttp\Command\Guzzle\GuzzleClient;

/**
 * Class PadsClient.
 *
 * @package Drupal\pads_client
 */
abstract class PadsClient extends GuzzleClient implements PadsClientInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $config = array()) {
    // Create a collection object with defaults and required params set.
    $data = Collection::fromConfig($config, array(
      'http_client_options' => array(
        'defaults' => array(
          'verify' => FALSE,
        ),
      ),
    ));
    $config = array_key_exists('data', $data) ? $data['data'] : $data;

    $this->handleHttpClientOptions($config);
    $this->handleDescriptionOptions($config);

    parent::__construct(
      $config['http_client'],
      $config['description'],
      $config->toArray()
    );

  }

  /**
   * Defines the client options for the HTTP Client.
   *
   * @param Collection $config
   *   The config object.
   */
  protected function handleHttpClientOptions(Collection $config) {
    // If http_client was injected, do nothing.
    if ($config['http_client']) {
      return;
    }
    $http_client_config = $config['http_client_options'] ?: array();
    $client = new HttpClient($http_client_config);
    $config['http_client'] = $client;
  }

  /**
   * Loads Guzzle Service Description.
   *
   * @param \GuzzleHttp\Collection $config
   *   The config object.
   */
  protected function handleDescriptionOptions(Collection $config) {
    // If service description was injected, do nothing.
    if ($config['description']) {
      return;
    }

    // Load service description data.
    $path = $config['description_path'];
    $data = file_exists($path) ? include $path : NULL;
    if (!is_array($data)) {
      throw new \InvalidArgumentException('Invalid description');
    }

    $config['description'] = new Description($data);
  }

  /**
   * {@inheritdoc}
   */
  public function call($operation, array $args = []) {
    return $this->$operation($args);
  }

}
