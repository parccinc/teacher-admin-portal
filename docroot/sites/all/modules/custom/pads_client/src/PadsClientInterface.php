<?php
/**
 * @file
 * Contains \Drupal\pads_client\PadsClientInterface.
 */

namespace Drupal\pads_client;

/**
 * Interface PadsClientInterface.
 */
interface PadsClientInterface {
  /**
   * Make an HTTP request to operation defined in service description data.
   *
   * @param string $operation
   *   Name of operation.
   * @param array $args
   *   Operation arguments.
   *
   * @return mixed
   *   The response from the operation call.
   */
  public function call($operation, array $args = []);

}
