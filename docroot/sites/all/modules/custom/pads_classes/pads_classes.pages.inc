<?php
/**
 * @file
 * Page functionality for pads_classes module.
 */

/**
 * Page callback for modal assign test to class page.
 *
 * @param bool $js
 *   TRUE if JavaScript enabled, FALSE otherwise.
 * @param mixed $class
 *   Loaded Ldr class.
 * @param mixed $org_id
 *   Current Org ID.
 *
 * @return array
 *   Renderable array or AJAX commands with abrubt exit.
 */
function pads_classes_assign_to_test($js, $class, $org_id = FALSE) {
  if (!$js) {
    return drupal_get_form('pads_classes_assign_to_test_form', $class, $org_id);
  }

  ctools_include('modal');
  ctools_include('ajax');

  $form_state = array(
    'title' => t('Assign to Test'),
    'ajax' => TRUE,
    'build_info' => array(
      'args' => array(
        $class,
        $org_id,
      ),
    ),
  );

  $output = ctools_modal_form_wrapper('pads_classes_assign_to_test_form', $form_state);
  if (!empty($form_state['executed'])) {
    // We'll just overwrite the form output if it was successful.
    $commands = array();
    if ($form_state['redirect']) {
      $commands[] = ctools_ajax_command_redirect($form_state['redirect']);
    }
    else {
      $commands[] = ctools_modal_command_dismiss();
    }

    $output = $commands;

  }
  print ajax_render($output);
  drupal_exit();
}

/**
 * Page callback for assign test to class page.
 *
 * @param mixed $form
 *   Drupal form array.
 * @param mixed $form_state
 *   Drupal form_state array.
 * @param mixed $class
 *   Ldr class object.
 *
 * @return array|string
 *   Drupal render array.
 */
function pads_classes_assign_to_test_form($form, &$form_state, $class, $org_id) {
  // Ensure we have the correct parameters.
  if (!$class) {
    drupal_set_message(t('Unable to determine class.'), 'error');
    return array();
  }

  // Check to see if from dashboard so user can choose class.
  if ($class == "no_class") {
    if (!$org_id) {
      drupal_set_message(t('Unable to determine org id.'), 'error');
      return array();
    }

    // Get classes to select.
    $user_classes = pads_classes_get_user_classes_by_org($account = NULL, $org_id);

    // Build out Class Options for user.
    $class_options = array();
    foreach ($user_classes as $class) {
      $class_options[$class['classId']] = $class['classIdentifier'] . ' - ' . $class['sectionNumber'];
    }

    // Set Class Id from choices, used in submit callback.
    $form['classId'] = array(
      '#type' => 'select',
      '#title' => t('Class - Section Identifier'),
      '#options' => $class_options,
      '#disabled' => FALSE,
    );

  }
  else {

    // Class Identifier field.
    $form['classIdentifier'] = array(
      '#type' => 'textfield',
      '#title' => t('Class Identifier'),
      '#default_value' => $class['classIdentifier'],
      '#disabled' => TRUE,
    );
    // Class Section number field.
    $form['sectionNumber'] = array(
      '#type' => 'textfield',
      '#title' => t('Section Number'),
      '#default_value' => $class['sectionNumber'],
      '#disabled' => TRUE,
    );
    // Class Id to use in submit callback.
    $form['classId'] = array(
      '#type' => 'hidden',
      '#value' => $class['classId'],
    );

  }

  // Submit form and pass to test assignment form.
  $submit_callback = array('pads_classes_assign_test');
  pads_tests_assignment_form($form, $form_state, $submit_callback, $class['gradeLevel']);
  return $form;
}
