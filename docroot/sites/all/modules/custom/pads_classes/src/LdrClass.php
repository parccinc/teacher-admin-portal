<?php
/**
 * @file
 * Install, update, and uninstall functions for the Foo Bar module.
 * Contains \Fully\Qualified\Namespace\And\NameOfTheClass.
 */

namespace Drupal\pads\classes;

use Database;
use Exception;

class LdrClass implements ClassInterface {

  /**
   * {@inheritdoc}
   */
  public function getClasses($org_id) {
    return pads_ldr_client_call('getClasses', array(
      'organizationId' => $org_id,
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function getClass($class_id) {
    return pads_ldr_client_call('getClass', array(
      'classId' => $class_id,
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function addClass($class) {
    return pads_ldr_client_call('addClass', $class);
  }

  /**
   * {@inheritdoc}
   */
  public function assignStudents($class_id, $student_record_ids) {
    return pads_ldr_client_call('createClassStudents', array(
      'classId' => $class_id,
      'studentRecordIds' => $student_record_ids,
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function removeStudents($class_id, $student_record_ids) {
    return pads_ldr_client_call('deleteClassStudents', array(
      'classId' => $class_id,
      'studentRecordIds' => $student_record_ids,
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function assignUser($user, $class_ids) {
    $account = isset($user->uid) ? $user : user_load($user);
    $user_cids = pads_classes_get_user_class_ids($account);
    $query = db_insert(PADS_TAP_USERS_USER_CLASSES_TABLE)
      ->fields(array('uid', 'cid', 'oid'));
    foreach ($class_ids as $class_id) {
      if (in_array($class_id, $user_cids)) {
        continue;
      }
      $class = $this->getClass($class_id);

      $query->values(array(
        'uid' => $account->uid,
        'cid' => $class_id,
        'oid' => $class && array_key_exists('organizationId', $class) ?
          $class['organizationId'] : 0,
      ));
    }

    $return_value = NULL;
    try {
      $return_value = $query->execute();
    }
    catch (Exception $e) {
      $code = $e->getCode();
      switch ($code) {
        case 23000:
          // Record exists, move on.
          $return_value = 0;
          break;

        default:
          watchdog(PADS_TAP_USERS_USER_CLASSES_TABLE, 'db_insert failed. Message = %message, query= %query',
            array(
              '%message' => $e->getMessage(),
              '%query' => $e->query_string,
            ), WATCHDOG_ERROR);
          break;

      }

    }
    return $return_value;
  }

  /**
   * {@inheritdoc}
   */
  public function unassignUser($user, $class_ids) {
    $account = isset($user->uid) ? $user : user_load($user);
    $options = array(
      'return' => Database::RETURN_AFFECTED,
    );
    $rows_affected = db_delete(PADS_TAP_USERS_USER_CLASSES_TABLE, $options)
      ->condition('cid', $class_ids)
      ->condition('uid', $account->uid)
      ->execute();
    return $rows_affected;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteClass($class_id) {
    return pads_ldr_client_call('deleteClass', array(
      'classId' => $class_id,
    ));
  }
}
