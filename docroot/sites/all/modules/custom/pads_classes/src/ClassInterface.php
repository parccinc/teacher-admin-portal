<?php
/**
 * @file
 * Install, update, and uninstall functions for the Foo Bar module.
 * Contains \Fully\Qualified\Namespace\And\NameOfTheClass.
 */

namespace Drupal\pads\classes;

interface ClassInterface {

  /**
   * Retrieve Ldr Classes from an Organization.
   *
   * @param int $org_id
   *   The organization id.
   *
   * @return array
   *   An array of Classes that are in the Organization.
   */
  public function getClasses($org_id);

  /**
   * Retrieve Ldr Class from Class id.
   *
   * @param int $class_id
   *   The Class id.
   *
   * @return array
   *   Ldr Class array.
   */
  public function getClass($class_id);

  /**
   * Post Class record.
   *
   * @param array $class
   *   Class object as expected by Ldr.
   *
   * @return array
   *   Ldr response.
   */
  public function addClass($class);

  /**
   * Assign one or more students to a class.
   *
   * @see https://breaktech.centraldesktop.com/p/aQAAAAACNwdS
   *
   * @param int $class_id
   *   Class id.
   * @param array $student_record_ids
   *   Student record ids.
   *
   * @return array
   *   Ldr response.
   */
  public function assignStudents($class_id, $student_record_ids);

  /**
   * Remove one or more students from a class.
   *
   * @see https://breaktech.imeetcentral.com/parcc/doc/37474966/w-LDRDELETEclassstudentsclassId
   *
   * @param int $class_id
   *   Class id.
   * @param array $student_record_ids
   *   Student record ids.
   *
   * @return array
   *   Ldr response.
   */
  public function removeStudents($class_id, $student_record_ids);

  /**
   * Assigns an Ldr Class to a TAP user.
   *
   * @param object|int $user
   *   User account, will accept uid.
   * @param array $class_ids
   *   Array of Ldr Class ids to assign.
   *
   * @return \DatabaseStatementInterface|int|null
   *   The last insert ID of the query, if one exists. If the query
   *   was given multiple sets of values to insert, the return value is
   *   undefined. If no fields are specified, this method will do nothing and
   *   return NULL. That makes it safe to use in multi-insert loops.
   */
  public function assignUser($user, $class_ids);

  /**
   * Unassigned Ldr Class from TAP user.
   *
   * @param object|int $user
   *   User account, will accept uid.
   * @param array $class_ids
   *   Array of Ldr Class ids to unassign.
   *
   * @return int|null
   *   The number of rows affected, or NULL if there was a problem.
   */
  public function unassignUser($user, $class_ids);

  /**
   * Delete Ldr Class.
   *
   * @param int $class_id
   *   Ldr Class id.
   *
   * @return array
   *   Ldr response.
   */
  public function deleteClass($class_id);
}
