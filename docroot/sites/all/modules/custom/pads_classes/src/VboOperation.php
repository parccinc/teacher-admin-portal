<?php

/**
 * @file
 * Drupal\pads\classes\VboOperation class.
 */

namespace Drupal\pads\classes;

class VboOperation extends \ViewsBulkOperationsAction {

  /**
   * @inheritdoc
   */
  public function logResult($entity, $result, &$log) {
    // Set a couple bits into $log the first time through.
    if (!isset($log['classes'])) {
      $log['classes'] = array_fill_keys(array_filter($this->formOptions['cids']), array());
      $log['operation'] = explode('::', $this->operationId);
      $log['operation'] = $log['operation'][1];
    }

    // Iterate over the classes, set appropriate info into $log to identify
    // errors and successes.
    foreach (array_keys($log['classes']) as $cid) {
      if (!empty($result[$log['operation']][$cid])) {
        // Error.
        $log['classes'][$cid]['error'][] = $entity['studentRecordId'];
      } else {
        // Success.
        $log['classes'][$cid]['success'][] = $entity['studentRecordId'];
      }
    }
  }

  /**
   * @inheritdoc
   */
  public function renderLog($log) {
    $output = array();

    $classes = $log['classes'];

    foreach ($classes as $cid => $result) {
      $class = pads_classes_get($cid);
      $class_name = $class['classIdentifier'];
      if (!empty($class['sectionNumber'])) {
        $class_name .= '-' . $class['sectionNumber'];
      }

      if (!empty($result['success'])) {
        // Record successful assignments.
        $params['@count'] = count($result['success']);
        $params['@plural'] = format_plural($params['@count'], t('student'), t('students'));
        $params['@class'] = $class_name;
        $output[] = t('@count @plural successfully assigned to Class @class.', $params);
      }
      if (!empty($result['error'])) {
        // Record unsuccessful assignments.
        $params['@count'] = count($result['error']);
        $params['@plural'] = format_plural($params['@count'], t('student'), t('students'));
        $params['@class'] = $class_name;
        $message = t('@count @plural unsuccessfully assigned to Class @class.', $params);

        // Add messaging to identify failed student class assignments when mixed
        // success/failure.
        if (!empty($result['success'])) {
          // Fakey "collapsible fieldset" for "More Details".
          $more = l(t('More Details'), '');

          // List of students.
          $list = array();
          foreach ($result['error'] as $student_id) {
            $student = pads_student_load($student_id);
            $params['@student'] = pads_students_formatname($student);
            $list[] = t('Unable to assign @student to Class @class.', $params);
          }
          $attributes['class'][] = 'element-invisible';
          $list = theme('item_list', array('items' => $list, 'attributes' => $attributes));
          $message .= '<div class="more-list">' . $more . $list . '</div>';
        }

        $output[] = $message;
      }
    }

    // Add completion message.
    foreach ($log as $item) {
      if (isset($item['type']) && $item['type'] == 'complete') {
        $output[] = $item['message'];
      }
    }

    return $output;
  }

}
