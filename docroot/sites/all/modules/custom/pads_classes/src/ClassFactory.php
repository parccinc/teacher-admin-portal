<?php
/**
 * @file
 * Install, update, and uninstall functions for the Foo Bar module.
 * Contains \Fully\Qualified\Namespace\And\NameOfTheClass.
 */

namespace Drupal\pads\classes;


class ClassFactory {

  public static $gradeSortOrder = array(
    '01' => 1,
    '02' => 2,
    '03' => 3,
    '04' => 4,
    '05' => 5,
    '06' => 6,
    '07' => 7,
    '08' => 8,
    '09' => 9,
    '10' => 10,
    '11' => 11,
    '12' => 12,
    '13' => 13,
    'IT' => 14,
    'KG' => 15,
    'Other' => 16,
    'OutOfSchool' => 17,
    'PK' => 18,
    'PR' => 19,
    'PS' => 20,
    'TK' => 21,
    'UG' => 22,
  );

  /**
   * Retrieve an implementation instance of ClassInterface.
   *
   * @return LdrClass
   *   An instance of LdrClass.
   */
  public static function getInstance() {
    $instance = &drupal_static(__CLASS__ . '.' . __FUNCTION__);
    if (!$instance) {
      $instance = new LdrClass();
    }
    return $instance;
  }
}