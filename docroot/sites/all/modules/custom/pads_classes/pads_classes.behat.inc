<?php
/**
 * @file
 * Contains \ClassContext.
 */

use Behat\Gherkin\Node\TableNode;
use Drupal\DrupalExtension\Context\DrupalSubContextBase;
use Drupal\DrupalExtension\Context\DrupalSubContextInterface;

class ClassContext extends DrupalSubcontextBase implements DrupalSubContextInterface {

  /**
   * @Given LDR classes:
   *
   * Provide Class data in the following format:
   *  | organizationId | classIdentifier      | gradeLevel | sectionNumber    |
   *  | 1234           | Discrete Mathematics | 1          | r2d3-[timestamp] |
   *  | 1234           | Integral Calculus    | 1          | r2d3-[timestamp] |
   *  | 1234           | Dinosaur Roaring     | 1          | r2d3-[timestamp] |
   */
  public function ldrClasses(TableNode $table) {
    $mink_context = $this->getContext('PadsMinkContext');
    foreach ($table->getHash() as $class) {
      foreach ($class as $key => $value) {
        $class[$key] = $mink_context->fixStepArgument($value);
      }
      // Tell Ldr it's okay to trust me.
      $class['clientUserId'] = 1;
      pads_class_add($class);
    }
  }

  /**
   * Calls fixStepArgument from PadsMinkContext.
   *
   * @param string $arg
   *   The arg to "fix".
   *
   * @return mixed
   *   Fixed arg.
   */
  protected function fixStepArgument($arg) {
    $padsmink_context = $this->getContext('PadsMinkContext');
    return $padsmink_context->fixStepArgument($arg);
  }

  /**
   * @Given I am assigned classes :class_ids
   */
  public function iAmAssignedClasses($class_ids) {
    $user = $this->getUser();
    $class_ids = explode(',', $class_ids);
    $fixed_class_ids = array();
    foreach ($class_ids as $class_id) {
      $fixed_class_ids[] = $this->fixStepArgument($class_id);
    }
    if (NULL === (pads_tap_users_assign_to_classes($user, $fixed_class_ids))) {
      throw new \Exception(sprintf('Class assignment did not happen for %s', implode(',', $class_ids)));
    }
  }

  /**
   * @Given user :user is assigned classes :class_ids
   */
  public function assertAssignedClasses($user, $class_ids) {
    if (!$account = user_load_by_name($user)) {
      throw new Exception(sprintf('Unable to find user "%s".', $user));
    }

    $class_ids = explode(',', $class_ids);
    $fixed_class_ids = array();
    foreach ($class_ids as $class_id) {
      $fixed_class_ids[] = $this->fixStepArgument($class_id);
    }
    if (NULL === (pads_tap_users_assign_to_classes($account, $fixed_class_ids))) {
      throw new \Exception(sprintf('Class assignment did not happen for user %s, class %s', $user, implode(',', $class_ids)));
    }
  }

  /**
   * Assert class count for user.
   *
   * @param string $user
   *   Name of user.
   * @param int $count
   *   Class count.
   *
   * @throws \Exception
   *
   * @Then user :user is assigned :count classes
   */
  public function assertUserClassAssignmentCount($user, $count) {
    $account = user_load_by_name($user);
    $cids = pads_classes_get_user_class_ids($account);
    $actual = count($cids);
    if ($actual != $count) {
      throw new \Exception(sprintf('Expecting %s has %d class(es), found %d', $user, $count, $actual));
    }
  }

  /**
   * @When I am assigned the classes :classIdentifiers from Organization :org_id
   *
   * @param string $class_identifiers
   *   Comma separated string of classIdentifiers
   *
   * @param int $org_id
   *   Organization id where the classes belong.
   *
   * @throws \Exception
   */
  public function iAmAssignedTheClasses($class_identifiers, $org_id) {
    $padsmink_context = $this->getContext('PadsMinkContext');
    $org_id = $padsmink_context->fixStepArgument($org_id);
    // The user to assign classes to.
    $user = $this->getUser();

    // Retrieve all classes in Organization.
    $classes = pads_classes_get_by_org($org_id);
    $classes = array_key_exists('classes', $classes) ?
      $classes['classes'] : array();

    // Match the classes.
    $class_identifiers = explode(',', $class_identifiers);

    // The array to store the class ids that match classIdentifier.
    $cids = $this->getCidsFromClassIdentifier($classes, $class_identifiers, $org_id);
    if (empty($cids)) {
      throw new \Exception(sprintf('No %s class(es) found in %d', implode(',', $class_identifiers), $org_id));
    }
    if (NULL === (pads_tap_users_assign_to_classes($user, $cids))) {
      throw new \Exception(sprintf('Class assignment did not happen for %s', implode(',', $class_identifiers)));
    }
  }


  /**
   * @Given I assign the students :state_identifiers to :class_identifiers
   */
  public function iAssignTheStudentsTo($state_identifiers, $class_identifiers) {
    $state_identifiers = explode(',', $state_identifiers);
    $class_identifiers = explode(',', $class_identifiers);

    $user = $this->getUser();
    // Find classes by classIdentifier.
    $classes = pads_classes_get_user_classes($user);

    // Store the class ids we want.
    $cids = $this->getCidsFromClassIdentifier($classes, $class_identifiers);

    // If we have found Class ids, then look for students.
    if (!empty($cids)) {
      // Get the students in the users Organizations.
      $orgs = pads_organization_get_user_orgs($user);
      $students = array();
      foreach ($orgs as $org_id => $org) {
        $response = pads_students_get($org_id);
        if ($response && array_key_exists('studentRecords', $response)) {
          foreach ($response['studentRecords'] as $student_record) {
            $state_identifier = $student_record['stateIdentifier'];
            if (in_array($state_identifier, $state_identifiers)) {
              $students[] = $student_record['studentRecordId'];
            }
          }
        }
      }
      if (!empty($students)) {
        foreach ($cids as $cid) {
          pads_ldr_client_call('createClassStudents', array(
            'clientUserId' => $user->uid,
            'classId' => $cid,
            'studentRecordIds' => $students,
          ));
        }
      }
    }
  }

  /**
   * Common method to find Class ids from classIdentifier field.
   *
   * @param array $classes
   *   Array of Classes.
   *
   * @param array $class_identifiers
   *   Array of classIdentifiers.
   *
   * @param int $user_org_id
   *   Defaults to 0 for no check, otherwise will verify Class from
   *   classIdentifier is in the Users Org.
   *
   * @return array
   *   Array of Class ids.
   */
  protected function getCidsFromClassIdentifier($classes, $class_identifiers, $user_org_id = 0) {
    $cids = array();
    foreach ($class_identifiers as $class_identifier) {
      $class_identifier = trim($class_identifier);
      foreach ($classes as $class) {
        if ($user_org_id && $class['organizationId'] != $user_org_id) {
          continue;
        }
        if ($class['classIdentifier'] == $class_identifier) {
          $cids[] = $class['classId'];
        }
      }
    }
    return $cids;
  }

  /**
   * @Then I should see the :field test battery subjects for grade :grade
   */
  public function iShouldSeeTheTestBatterySubjectsForGrade($field, $grade) {
    // Find the form field.
    $feature_context = $this->getContext('FeatureContext');
    $element = $feature_context->assertSession()->fieldExists($field);
    $subjects = pads_tests_get_subjects_by_grade($grade);
    foreach ($subjects as $subject) {
      $element->selectOption($subject);
    }
  }

  /**
   * @Then the :field form element should contain test-battery options from JSON args :args
   */
  public function theFormElementShouldContainsOptionsFromWithJsonArgs($field, $args) {
    // Retrieve the form field element.
    $feature_context = $this->getContext('FeatureContext');
    $element = $feature_context->assertSession()->fieldExists($field);

    // Get the test batteries.
    $args = json_decode($args, TRUE);
    $batteries = pads_tests_get_batteries_as_form_options($args);
    foreach ($batteries as $battery_id => $battery) {
      $option = $element->find('css', "option[value=\"$battery_id\"]");
      if (!$option) {
        throw new \Exception(sprintf('Expecting to find option with value attribute of %s.', $battery_id));
      }
      if ($option->getText() !== $battery) {
        throw new \Exception(sprintf('Expecting option value %s to have the label %s, but it does not.', $battery_id, $battery_id));
      }
    }
  }

  /**
   * @Then user :user should be assigned to classes :class_ids
   */
  function assertUserAssignedClass($name, $class_ids) {
    $class_ids = $this->getContext('PadsMinkContext')
      ->fixStepArgument($class_ids);
    $class_ids = array_map('trim', explode(',', $class_ids));

    if (!$account = user_load_by_name($name)) {
      throw new Exception(sprintf('Unable to find user "%s".', $name));
    }

    $user_class_ids = pads_classes_get_user_class_ids($account);

    foreach ($class_ids as $class_id) {
      if (!in_array($class_id, $user_class_ids)) {
        throw new Exception(sprintf('User "%s" is not assigned to class "%s".', $name, $class_id));
      }
    }
  }

  /**
   * @Then user :user should not be assigned to classes :class_ids
   */
  function assertUserNotAssignedClass($name, $class_ids) {
    $class_ids = $this->getContext('PadsMinkContext')
      ->fixStepArgument($class_ids);
    $class_ids = array_map('trim', explode(',', $class_ids));

    if (!$account = user_load_by_name($name)) {
      throw new Exception(sprintf('Unable to find user "%s".', $name));
    }

    $user_class_ids = pads_classes_get_user_class_ids($account);

    foreach ($class_ids as $class_id) {
      if (in_array($class_id, $user_class_ids)) {
        throw new Exception(sprintf('User "%s" is assigned to class "%s" but should not be.', $name, $class_id));
      }
    }
  }

  /**
   * @Then the class :class should be selected
   */
  function assertClassSelected($class) {
    $class = $this->getContext('PadsMinkContext')->fixStepArgument($class);

    $page = $this->getSession()->getPage();
    if (!$option = $page->find('css', '.pane-testassignments-class-jump-menu option[selected="selected"]')) {
      throw new Exception(sprintf('No selection found for test assignment jump menu on "%s".'), $this->getSession()
        ->getCurrentUrl());
    }

    $prefix = url('manage/4/tests/assignments');
    $val = str_replace($prefix . '/', '', $option->getAttribute('value'));
    if ($class != $val) {
      throw new Exception('Class "%s" selected, expected "%s".', $val, $class);
    }
  }
}
