<?php
/**
 * @file
 * Views functionality for pads_classes.
 */

/**
 * Implements hook_views_data().
 */
function pads_classes_views_data() {
  $data = array();
  $data['pads_user_classes'] = array(
    'table' => array(
      'group' => t('Pads Classes'),
      'base' => array(
        'field' => 'cid',
        'title' => t('Pads Classes'),
        'help' => t('Classes that are assigned to users.'),
      ),
      'join' => array(
        'user' => array(
          'left_field' => 'uid',
          'field' => 'uid',
        ),
      ),
    ),
    'cid' => array(
      'title' => t('Ldr Class id'),
      'help' => t('Class id'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
    'oid' => array(
      'title' => t('Organization id'),
      'help' => t('Class Organization id'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
    'classIdentifier' => array(
      'real field' => 'cid',
      'field' => array(
        'title' => t('Class name'),
        'help' => t('Retrieves class identifier from Ldr'),
        'handler' => 'pads_classes_views_handler_field_classes',
        'click sortable' => TRUE,
      ),
    ),
    'sectionNumber' => array(
      'real field' => 'cid',
      'field' => array(
        'title' => t('Section number'),
        'help' => t('Retrieves class section number from Ldr'),
        'handler' => 'pads_classes_views_handler_field_classes',
        'click sortable' => TRUE,
      ),
    ),
    'gradeLevel' => array(
      'real field' => 'cid',
      'field' => array(
        'title' => t('Grade level'),
        'help' => t('Retrieves class grade level from Ldr'),
        'handler' => 'pads_classes_views_handler_field_classes',
        'click sortable' => TRUE,
      ),
    ),
    'uid' => array(
      'title' => t('Uid'),
      'relationship' => array(
        'base' => 'users',
        'base field' => 'uid',
        'handler' => 'views_handler_relationship',
        'label' => t('Users'),
        'title' => t('User'),
        'help' => t('The user assigned to Class.'),
      ),
    ),
    'assignTest' => array(
      'title' => t('Assign test'),
      'group' => t('Class test operations'),
      'help' => t('Create test assignment for all students in class.'),
      'real field' => 'cid',
      'field' => array(
        'handler' => 'pads_classes_views_handler_field_operation_assigntest',
      ),
    ),
    'addStudent' => array(
      'title' => t('Add student'),
      'group' => t('Class test operations'),
      'help' => t('Create a student and add to the class.'),
      'real field' => 'cid',
      'field' => array(
        'handler' => 'pads_classes_views_handler_field_operation_addstudent',
      ),
    ),
  );
  return $data;
}

/**
 * Implements hook_views_plugins().
 */
function pads_classes_views_plugins() {

  $plugins = array(
    'argument default' => array(
      'pads_class' => array(
        'title' => t('PADS Classes from logged in user'),
        'handler' => 'views_plugin_argument_default_pads_class',
      ),
    ),
    'argument validator' => array(
      'pads_class' => array(
        'title' => t('PADS Class assigned to user'),
        'handler' => 'views_plugin_argument_validate_pads_class',
      ),
    ),
  );
  return $plugins;
}
