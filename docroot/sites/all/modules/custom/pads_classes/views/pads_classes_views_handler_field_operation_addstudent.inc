<?php
/**
 * @file
 * Contains \pads_classes_views_handler_field_operation_addstudent.
 */

class pads_classes_views_handler_field_operation_addstudent extends views_handler_field {

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    $class_id = $this->get_value($values);
    $class = pads_classes_get($class_id);
    if (!array_key_exists('organizationId', $class)) {
      return NULL;
    }
    $org_id = $class['organizationId'];
    $text = !empty($this->options['label']) ? $this->options['label'] : t('Add Student');
    $query = drupal_get_destination();
    $query['cid'] = $class_id;
    $options = array(
      'query' => $query,
    );
    return l($text, "student/add/$org_id", $options);
  }
}
