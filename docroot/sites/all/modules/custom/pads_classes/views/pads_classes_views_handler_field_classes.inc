<?php
/**
 * @file
 * Contains \pads_classes_views_handler_field_class_identifier.
 */

class pads_classes_views_handler_field_classes extends views_handler_field {

  protected $field;

  function init(&$view, &$options) {
    $this->field = array_key_exists('field', $options) ? $options['field'] : '';
    parent::init($view, $options);
  }

  function get_value($values, $field = NULL) {
    $cid = $values->cid;
    if (!$field) {
      $field = $this->field;
    }
    $class = pads_classes_get($cid);
    return array_key_exists($field, $class) ? $class[$field] : '';
  }
}
