<?php

/**
 * @file
 * Contains /views_plugin_argument_default_pads_class.
 */
class views_plugin_argument_default_pads_class extends views_plugin_argument_default {

  /**
   * {@inheritdoc}
   */
  public function get_argument() {
    $classes = pads_classes_get_user_classes();
    if (!$classes || empty($classes)) {
      return NULL;
    }
    $cids = array_keys($classes);
    return implode('+', $cids);
  }
}
