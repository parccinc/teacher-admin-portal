<?php

/**
 * @file
 * Contains /views_plugin_argument_validate_pads_class.
 */

class views_plugin_argument_validate_pads_class extends views_plugin_argument_validate {

  /**
   * {@inheritdoc}
   */
  public function init(&$view, &$argument, $options) {
    parent::init($view, $argument, $options);
    composer_manager_register_autoloader();
  }

  /**
   * Pads Class context validator.
   *
   * @param array|string $arg
   *   If $arg is an array, it likely was created by the 'argument default'
   *   plugin. The keys are the class ids, and the values are an array of the
   *   Class object as returned by Ldr. If String, the context is a '+'
   *   separated string of Class ids.
   *
   * @return bool
   *   True if it validates.
   */
  public function validate_argument($arg) {
    if (!$arg) {
      return FALSE;
    }
    // If the user has access to view class, no need to validate.
    if (user_access('view any class')) {
      return TRUE;
    }

    if (is_array($arg)) {
      $cids = array_keys($arg);
      $arg = implode('+', $cids);
    }
    $args = views_break_phrase($arg);
    $object_ids = $args->value;

    // Get the Users classes to validate user has rights to view Class
    // represented by $arg.
    $classes = pads_classes_get_user_classes();
    $cids = array_keys($classes);

    // If any of the args are not assigned to the user, validation fails.
    foreach ($object_ids as $id) {
      if (!in_array($id, $cids)) {
        return FALSE;
      }
    }

    // If we made it here, all is good.
    return TRUE;
  }

}
