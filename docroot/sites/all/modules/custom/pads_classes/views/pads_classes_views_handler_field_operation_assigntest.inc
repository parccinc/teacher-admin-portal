<?php
/**
 * @file
 * Contains \pads_classes_views_handler_field_operation_assigntest.
 */

/**
 * Class pads_classes_views_handler_field_operation_assigntest.
 */
class pads_classes_views_handler_field_operation_assigntest extends views_handler_field {

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    // If user does not have access to assign test to class, abort.
    if (!user_access('assign to class')) {
      return NULL;
    }

    $class_id = $this->get_value($values);

    // Needed to trigger the modal.
    ctools_include('modal');
    ctools_modal_add_js();

    $options = array();
    $options['attributes']['class'][] = 'ctools-use-modal';
    $options['attributes']['class'][] = 'ctools-modal-zurb-modal-style';
    return l($this->options['label'], "class/nojs/$class_id/assigntest", $options);
  }

}
