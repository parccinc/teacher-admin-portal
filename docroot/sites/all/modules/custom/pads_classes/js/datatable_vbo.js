(function ($) {

    Drupal.behaviors.pads_classes_vbo = {
        attach: function (context, settings) {
            if (typeof settings.datatables === "undefined")
                return;

            // Iterate over datatables defined in JS settings.
            for (var selector in settings.datatables) {
                if (Drupal.datatables.hasOwnProperty(selector)) {
                    var table = $(selector);
                    var form = table.parents('form')[0];

                    // Gather all the checkboxes in the original table.
                    var boxes = Drupal.pads_datatable.getBoxes(selector);

                    // Handle multi-page form submit.
                    $('button', form).click(function (event) {
                        event.preventDefault();
                        $('[data-vbo="hidden"]', form).remove();
                        boxes.each(function () {
                            if (this.checked) {
                                Drupal.pads_datatable.setForm(form, this.name, this.value);
                            }
                        });
                        form.submit();
                    });

                    // Attach the select all functionality to the checkboxes.
                    $('.vbo-table-select-all', form).click(function (event) {
                        // Remove default VBO select all pages control.
                        $('tr.views-table-row-select-all').remove();
                        // Insert the select all pages control.
                        $('tbody', table).prepend(Drupal.theme('padsClassesSelectAll', table));
                        $('.views-table-row-select-all button', table).click(function (event) {
                            $('tr.views-table-row-select-all').remove();
                            Drupal.pads_datatable.selectAll(table);
                        });
                    });
                    // Attach select all functionality to tableselect checkboxes.
                    $('.select-all', form).click(function (event) {
                        // Insert the select all pages control.
                        $('tbody', table).prepend(Drupal.theme('padsClassesSelectAll', table));
                        $('.views-table-row-select-all button', table).click(function (event) {
                            $('tr.views-table-row-select-all').remove();
                            Drupal.pads_datatable.selectAll(table);
                        });
                    });
                }
            }
        }
    };

    Drupal.pads_datatable = Drupal.pads_datatable || {};

    /**
     * Select all boxes in the table.
     *
     * @param table
     */
    Drupal.pads_datatable.selectAll = function (table) {
        Drupal.pads_datatable.getBoxes(table.selector).each(function () {
            this.checked = true;
        });
    }

    /**
     * Gets the checkboxes from the datatable.
     *
     * These are not necessarily visible in the DOM, so this it necessary in
     * order to access "hidden" rows.
     *
     * @param table
     *   The table.
     */
    Drupal.pads_datatable.getBoxes = function(selector) {
        return (typeof Drupal.datatables[selector].$ == "function") ?
          Drupal.datatables[selector].$('input[type="checkbox"]') :
        {};
    }

    /**
     * Set a value onto a form.
     *
     * @param form
     *   The form element.
     * @param name
     *   The name of the value to be set.
     * @param value
     *   The value being set.
     */
    Drupal.pads_datatable.setForm = function(form, name, value) {
        var element = document.createElement('input');
        element.type = 'hidden';
        element.name = name;
        element.value = value;
        element.setAttribute('data-vbo', 'hidden');
        form.appendChild(element);
    }

    /**
     * Drupal theme function for rendering the "select all pages" control.
     */
    Drupal.theme.prototype.padsClassesSelectAll = function (table) {
        var oTable = Drupal.datatables[table.selector].DataTable();
        var cols = oTable.columns().nodes().length;;
        var rows = oTable.page.info();
        rows = rows.end - rows.start;

        var output = '<tr class="views-table-row-select-all">';
        output += '<td colspan="' +  cols + '">';
        output += 'Selected <strong>' + rows + ' rows</strong> in this page. ';
        output += '<button class="small round form-submit" name="op" value="Select all  rows in this view." type="button">Select all  rows in this view.</button>';
        output += '</td>';
        output += '</tr>';
        return output;
    };

})(jQuery);
