(function ($) {

  Drupal.behaviors.padsDataTables = {
    attach: function (context, settings) {
      $.each(settings.datatables, function (selector) {
        $(selector, context).once('pads_datatables', function() {
          var settings = Drupal.settings.datatables[selector];
          settings.drawCallback = function ( settings ) {
              var api = this.api();
              var rows = api.rows( {page:'current'} ).nodes();
              var last=null;

              api.column(3, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                  $(rows).eq( i ).before(
                    '<tr role="row" class="group"><th colspan="4">Grade '+group+'</th></tr>'
                  );

                  last = group;
                }
              } );
            }

          // Order by the grouping
          $(selector).on( 'click', 'tr.group', function () {
            var table = Drupal.datatables[selector].DataTable();
            var currentOrder = table.order()[0];
            if ( currentOrder[0] === 0 && currentOrder[1] === 'asc' ) {
              table.order( [ 3, 'desc' ] ).draw();
            }
            else {
              table.order( [ 3, 'asc' ] ).draw();
            }
          } );
        });
      });
    }
  }
})(jQuery, Drupal);
