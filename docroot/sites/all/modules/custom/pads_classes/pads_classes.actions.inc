<?php
/**
 * @file
 * Functionality for actions performed on class assignments.
 */

use Drupal\pads\classes\ClassFactory;

/**
 * Implements hook_action_info().
 */
function pads_classes_action_info() {
  return array(
    'pads_classes_assign_users_to_classes' => array(
      'type' => 'user',
      'label' => t('Assign to Class'),
      'behavior' => array('views_property'),
      'configurable' => TRUE,
      'vbo_configurable' => FALSE,
      'triggers' => array('any'),
    ),
    'pads_classes_assign_students_to_classes' => array(
      'type' => 'pads_students',
      'label' => t('Assign to Class'),
      'behavior' => array('views_property'),
      'configurable' => TRUE,
      'vbo_configurable' => FALSE,
      'triggers' => array('any'),
      'operation_class' => 'Drupal\pads\classes\VboOperation',
    ),
    'pads_classes_unassign_user_from_classes' => array(
      'type' => 'user',
      'label' => t('Unassign Class'),
      'behavior' => array('changes_property'),
      'configurable' => FALSE,
      'vbo_configurable' => FALSE,
      'triggers' => array('any'),
      'aggregate' => TRUE,
      'pass rows' => TRUE,
    ),
  );
}

/**
 * VBO Config form for assigning users to classes.
 *
 * @param array $context
 *   VBO context.
 * @param array $form_state
 *   Drupal form_state array.
 *
 * @return array
 *   Renderable form array.
 *
 * @throws Exception
 */
function pads_classes_assign_users_to_classes_form($context, &$form_state) {
  $form = array();

  // Tell user that we are selecting classes to assign to the selected users.
  $selection = array_key_exists('selection', $form_state) ?
    $form_state['selection'] : array();

  $users = user_load_multiple($selection);
  $usernames = array();
  foreach ($users as $user) {
    $usernames[] = format_username($user);
  }

  $count = format_plural(count($users), 'user', '@count users');
  $username_markup = theme('item_list', array(
    'items' => $usernames,
    'title' => t('Select classes to assign the following <strong>!count</strong>:', array('!count' => $count)),
  ));

  $form['info'] = array(
    '#type' => 'item',
    '#markup' => $username_markup,
  );

  // Get the term from view arguments.
  $args = $context['view']->args;
  if (!empty($args)) {
    // Get classes from Organization.
    $tid = reset($args);
    $org = pads_org_org_from_term($tid);
    $form = pads_classes_tableselect_form($form, $form_state, $org);
  }
  return $form;
}

/**
 * VBO Config form for assigning students to classes.
 *
 * @param array $context
 *   VBO context.
 * @param array $form_state
 *   Drupal form_state array.
 *
 * @return array
 *   Renderable form array.
 *
 * @throws Exception
 */
function pads_classes_assign_students_to_classes_form($context, &$form_state) {
  $form = array();

  // Tell user that we are selecting classes to assign to the selected students.
  $selection = array_key_exists('selection', $form_state) ?
    $form_state['selection'] : array();

  $students = array();
  foreach ($selection as $student_id) {
    $student = pads_student_load($student_id);
    $students[] = pads_students_formatname($student);
  }

  $count = format_plural(count($students), 'student', '@count students');
  $student_markup = theme('item_list', array(
    'items' => $students,
    'title' => t('Select classes to assign the following <strong>!count</strong>:', array('!count' => $count)),
  ));

  $form['info'] = array(
    '#type' => 'item',
    '#markup' => $student_markup,
  );

  // Get Org view arguments.
  $orgs = pads_org_load(reset($context['view']->args));
  if (!empty($orgs)) {
    $form = pads_classes_tableselect_form($form, $form_state, $orgs);
  }
  return $form;
}

/**
 * VBO assign users to classes config form validation.
 *
 * @param array $form
 *   Drupal form.
 * @param array $form_state
 *   Drupal form_state.
 */
function pads_classes_assign_users_to_classes_validate($form, &$form_state) {
  if (
    !array_key_exists('table', $form_state['values']) ||
    empty($selected_rows = array_filter($form_state['values']['table']))
  ) {
    form_set_error('table', 'Please choose at least one class.');
  }
}

/**
 * VBO assign users to classes config form validation.
 *
 * @param array $form
 *   Drupal form.
 * @param array $form_state
 *   Drupal from_state.
 */
function pads_classes_assign_students_to_classes_validate($form, &$form_state) {
  pads_classes_assign_users_to_classes_validate($form, $form_state);
}

/**
 * VBO assign users to classes config form submission..
 *
 * @param array $form
 *   Drupal form.
 * @param array $form_state
 *   Drupal from_state.
 *
 * @return array
 *   Context array to pass to action handler.
 */
function pads_classes_assign_users_to_classes_submit($form, &$form_state) {
  return array(
    'cids' => array_filter($form_state['values']['table'], function ($cid) {
      return ($cid);
    }),
  );
}

/**
 * VBO assign student to classes config form submission..
 *
 * @param array $form
 *   Drupal form.
 * @param array $form_state
 *   Drupal from_state.
 *
 * @return array
 *   Context array to pass to action handler.
 */
function pads_classes_assign_students_to_classes_submit($form, &$form_state) {
  return pads_classes_assign_users_to_classes_submit($form, $form_state);
}

/**
 * Action handler to assign users to classes.
 *
 * @param object $account
 *   User object.
 * @param array $context
 *   VBO context returned from config form submission.
 */
function pads_classes_assign_users_to_classes($account, $context) {
  $cids = $context['cids'];
  if (NULL === (pads_tap_users_assign_to_classes($account, $cids))) {
    drupal_set_message(t('Unable to assign classes to @user', array(
      '@user' => format_username($account),
    )), 'error');
  }
}

/**
 * Action handler to unassign class from users.
 *
 * @param object $account
 *   User object.
 * @param array $context
 *   VBO context returned from config form submission.
 */
function pads_classes_unassign_user_from_classes($user, $context) {
  $user = reset($user);
  $cids = array();
  $rows = $context['rows'];
  foreach ($rows as $row) {
    $cids[] = $row->cid;
  }
  if ($affected_rows = ClassFactory::getInstance()
    ->unassignUser($user, $cids)
  ) {
    $msg_classes_removed = format_plural($affected_rows, '1 class', '@count classes');
    drupal_set_message(t('@classes_removed removed for @user', array(
      '@classes_removed' => $msg_classes_removed,
      '@user' => format_username($user),
    )));
  }
  else {
    drupal_set_message(t('Unable to unassign class(es) from @user', array(
      '@user' => format_username($user),
    )), 'error');
  }

  // Redirect to user page if destination not set.
  if (!array_key_exists('destination', $_GET)) {
    $_GET['destination'] = "user/{$user->uid}";
  }
}

/**
 * Action handler to assign students to classes.
 *
 * @param array $student
 *   Student array..
 * @param array $context
 *   VBO context returned from config form submission.
 *
 * @return array
 *   An array of errors, keyed by class cid.
 */
function pads_classes_assign_students_to_classes($student, $context) {
  $cids = $context['cids'];
  $errors = array();
  foreach ($cids as $cid) {
    $response = pads_ldr_client_call('createClassStudents', array(
      'classId' => $cid,
      'studentRecordIds' => array($student['studentRecordId']),
    ));
    if (array_key_exists('errors', $response)) {
      foreach ($response['errors'] as $student_record_id => $error) {
        $errors[$cid] = $error['detail'];
      }
    }
  }

  return $errors;
}

/**
 * Implements hook_views_bulk_operations_form_alter().
 */
function pads_classes_views_bulk_operations_form_alter(&$form, &$form_state, $vbo) {
  $step = $form_state['step'];
  $form_id = array_key_exists('form_id', $form_state['build_info']) ?
    $form_state['build_info']['form_id'] : FALSE;

  if ($form_id) {
    switch ($step) {
      case 'views_form_views_form':

        switch ($form_id) {
          case 'views_form_tap_user_actions_panel_pane_user_actions':
          case 'views_form_pads_students_panel_pane_student_actions':
            // No Fieldset, change empty option to Actions.
            $form['select']['#type'] = t('Container');
            $form['select']['operation']['#options'][0] = t('- Actions -');

            // Action button label to 'next'.
            $form['select']['submit']['#value'] = t('Next');
            break;

        }
        break;

      case 'views_bulk_operations_config_form':
        // Alter the first step of the VBO form (the selection page).
        break;

      case 'views_bulk_operations_confirm_form':
        // Alter the confirmation step of the VBO form.
        break;

    }
  }
}
