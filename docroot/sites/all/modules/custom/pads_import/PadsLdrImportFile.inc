<?php
/**
 * @file
 * Class PadsLdrImportFile
 */

/**
 * Abstract class built for using Ldr for validation, turning the Import
 * process from a two step process (validation and creation) into one.
 */
abstract class PadsLdrImportFile extends PadsImportFile {

  protected $idlist;

  /**
   * PadsLdrImportFile constructor.
   */
  public function __construct($account) {
    parent::__construct($account);
    $this->idlist = array();
  }

  /**
   * {@inheritdoc}
   */
  public function parseLines($lines, &$validate) {
    $records = array();
    // Parse and validate individually.
    foreach ($lines as $data) {
      // Skip empty lines.
      if (!$data['line'][0]) {
        continue;
      }
      $record = $this->parseLine($data);
      $this->validateRecord($record, $validate);
      $records[$record->lineNo] = $record;
    }
    // Validate the set/subset.
    $this->validateRecords($records, $validate);
    // Save each record.
    foreach ($records as $record) {
      $dest = empty($record->errors) ? 'complete' : 'error';
      $this->saveRecord($record, $dest);
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function validateRecord($record, &$validate) {
    // Let Ldr do validation.
    $record = $this->generateEntity($record);
  }

  /**
   * {@inheritdoc}
   */
  protected function validateRecords($records, &$validate) {
    $this->generateEntities(array_keys($records), $validate);
  }

  /**
   * {@inheritdoc}
   */
  public function generateEntities($record_nos, &$validate) {
    if (!empty($this->idlist)) {
      // Do something classy.
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postValidation() {
    $this->writeStatus('complete');
  }

}
