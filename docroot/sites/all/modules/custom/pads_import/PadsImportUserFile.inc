<?php
/**
 * @file
 * Class \PadsImportUserFile
 */

class PadsImportUserFile extends PadsImportFile {

  // Declare the upload type.
  protected $type = 'user';
  protected $roles;

  /**
   * El constructor.
   */
  public function __construct($account) {
    $user_roles = user_roles();
    foreach ($user_roles as $rid => $role) {
      $role = strtolower($role);
      $this->roles[$role] = $rid;
    }
    parent::__construct($account);
  }

  /**
   * {@inheritdoc}
   */
  protected function parseLine($data) {
    $record = new stdClass();
    $record->line = $data['line'];
    $record->lineNo = $data['line_no'];

    // Org IDs may be input as a space or comma-separated value.
    $org_ids = array_map('trim', array_filter(explode(',', $data['line'][2])));
    $orgs = pads_org_load_multiple($org_ids);
    // If the org id is invalid, then orgs will be empty.
    if (!empty($org_ids) && empty($orgs)) {
      // Put in a fake org that will fail validation.
      $orgs[-1] = $org_ids;
    }
    $record->orgs = $orgs;

    // Convert role from text to rid.
    $record->roles = array();
    $role = trim(strtolower($data['line'][1]));
    if (!empty($this->roles[$role])) {
      $record->role = $this->roles[$role];
    }

    $record->mail = trim($data['line'][0]);
    $record->vendor = trim($data['line'][3]);

    return $record;
  }

  /**
   * {@inheritdoc}
   */
  protected function validateRecord($record, &$validate) {

    // Validate org.
    if (count($record->orgs) == 0) {
      // $record->errors['org_req'] = t('Org ID: Value is required.');
    }
    else {
      foreach ($record->orgs as $org_id => $org) {
        // Is the ord id valid?
        if ($org_id == -1) {
          $record->errors['org_valid'] = t('%org is not a valid Organization ID.', array(
            '%org' => implode(',', $org),
          ));
        }
      }
    }

    // Role checks.
    if (empty($record->role)) {
      $role_valid_message = !empty($record->line[1]) ? t('Role: %role is not a valid role.', array(
        '%role' => $record->line[1],
      )) : t('Role: Value is required.');
      $record->errors['role_valid'] = $role_valid_message;
    }

    // Email address.
    if (empty($record->mail)) {
      $record->errors['mail_req'] = t('Email address: Value is required.');
    }
    elseif (!valid_email_address($record->mail)) {
      $record->errors['mail_valid'] = t('Email address: Invalid.');
    }
    elseif (user_load_by_mail($record->mail)) {
      $record->errors['mail_dup'] = t('Email address: Duplicate user exists in the System.');
    }
    elseif (!empty($validate['emails'][$record->mail])) {
      $record->errors['mail_upload'] = t('Email address: Duplicate user exists in uploaded file.');
    }
    else {
      $validate['emails'][$record->mail] = TRUE;
    }

    // Records requires an org id or a vendor. Check to see that only one
    // exists.
    if (
      (empty($record->orgs) && empty($record->vendor)) ||
      (!empty($record->orgs) && !empty($record->vendor))
    ) {
      $record->errors['affiliation'] = t('Affiliation: There must be one value for either Org ID or Vendor.');
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function generateEntity($record) {

    if (module_exists('email_registration')) {
      // Prepending email_registration_ to the user name will kick off the
      // user save hooks from the email_registration module and safely provide
      // a unique username based on email.
      $name = $this->userName('email_registration_' . user_password());
    }
    else {
      $name = $this->userName($record->mail);
    }

    // Create the user entity.
    $data = array(
      'name' => $name,
      'mail' => $record->mail,
      'init' => $record->mail,
      'pass' => user_password(),
      'status' => TRUE,
    );
    $data['roles'][$record->role] = $record->role;
    $account = entity_create('user', array());
    user_save($account, $data);

    // Create the profile.
    // Determine profile to use based on $type value.
    $profile = ($this->type == 'user') ? 'pads_batchuser_organization' : 'pads_tapuser_organization';

    $data = array(
      'type' => $profile,
      'uid' => $account->uid,
    );
    $pw = PadsWrapperFactory::getInstance('profile2', $data, $profile);
    if (!empty($record->orgs)) {
      $pw->setState(array_keys($record->orgs));
    }
    if (!empty($record->vendor)) {
      $pw->setVendor($record->vendor);
    }
    $pw->save();

    _user_mail_notify('register_admin_created', $account);
  }

  /**
   * Creates a valid username out of an email address.
   *
   * Grabs the characters before '@' in an email address. Checks database
   * if username already exists and appends a digit to make unique. Subject
   * to a race condition.
   *
   * @param string $name
   *   A valid email address.
   *
   * @return mixed|string
   *   The valie username.
   */
  protected function userName($name) {
    $name = preg_replace('/[^\x{80}-\x{F7} a-zA-Z0-9@_.\'-]/', '', trim(strtolower($name)));

    $query = "SELECT name FROM {users} WHERE NAME LIKE :name";
    $args = array(':name' => $name . '%');
    $usernames = db_query($query, $args)->fetchCol();

    // Ensure username is unique.
    // TODO: This is open to a race condition.
    $addon = '';
    while (in_array($name . $addon, $usernames)) {
      $addon = (int) $addon + 1;
    }
    $name .= $addon;

    return $name;
  }

  /**
   * {@inheritdoc}
   */
  public function writeErrors() {
    global $user;

    // Load or create a new file entity.
    if ($file = $this->errorFile()) {
      return $file->uri;
    }

    $file = new stdClass();
    $file->uid = $user->uid;
    $file->uri = $this->errorUri;
    $file->filename = drupal_basename($file->uri);

    // Write the CSV.
    $fh = fopen($this->errorUri, 'w');
    $header = array(
      'Mail',
      'Role',
      'Role',
      'State ID',
      'Organization',
      'Error Message',
    );
    fputcsv($fh, $header);

    $errors = $this->recordFiles('error');

    foreach ($errors as $line_no) {
      $record = $this->loadRecord('error', $line_no);
      $line = $record->line;
      $line[] = implode(', ', (array) $record->errors);
      fputcsv($fh, $line);
    }
    fclose($fh);

    // Insert/update the db record.
    file_save($file);

    return $file->uri;
  }

}
