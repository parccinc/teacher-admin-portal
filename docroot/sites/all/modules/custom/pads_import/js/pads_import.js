(function ($) {

  Drupal.padsImport = Drupal.padsImport || {};

  Drupal.padsImport.filesAddedCallback = function (up, files) {
    // Remove any previous errors.
    $('.file-upload-js-error').remove();

    var files_added = true;
    if (up.settings.hasOwnProperty('cardinality')) {
      if (up.settings.cardinality != files.length) {
        for (key in files) {
          up.removeFile(files[key]);
        }
        var error = Drupal.formatPlural(up.settings.cardinality, 'Please select exactly 1 file', 'Please select exactly @num files');
        $(document).find('div.form-type-plupload.form-item-file').prepend('<div class="messages error file-upload-js-error" aria-live="polite">' + error + '</div>');
        files_added = false;
      }
    }
    if (files_added) {
      Drupal.plupload.filesAddedCallback(up, files);
    }
  }

  Drupal.behaviors.padsImportHideUploadButton = {
    attach : function (){
      $('.plupload_start').hide();
    }
  }

})(jQuery);