<?php

/**
 * @file
 * Contains \PadsImportFile.
 */
abstract class PadsImportFile {

  // The base location where a user's files will be saved.
  protected $basePath;

  // URI to a user's uploaded data file.
  protected $dataUri;
  protected $dataDir;

  // URI to a user's errored records file.
  protected $errorUri;
  protected $errorDir;

  // URI to a file we save to signify the upload is complete.
  protected $statusUri;

  // Raw data uploaded from the CSV.
  protected $lines;

  // An array of records read from the CSV.
  protected $records = NULL;
  protected $type = NULL;
  protected $account;

  /**
   * PadsImportFile constructor.
   *
   * @param stdClass $account
   *   The user account which is running the import.
   *
   * @throws Exception
   */
  public function __construct($account) {

    $this->account = $account;

    if (empty($this->type)) {
      throw new Exception(t('No file type defined.'));
    }

    $this->basePath = "private://pads-import/{$this->account->uid}/{$this->type}";
    $this->dataUri = "{$this->basePath}/data.csv";
    $this->dataDir = "{$this->basePath}/data";
    $this->errorUri = "{$this->basePath}/errors.csv";
    $this->errorDir = "{$this->basePath}/errors";
    $this->statusUri = "{$this->basePath}/status";
    $this->completeDir = "{$this->basePath}/complete";
  }

  /**
   * Reports the status of an upload.
   *
   * @return string
   *   A string representing the current status of the upload process. Possible
   *   values are:
   *
   *     "upload" - Waiting for an upload.
   *     "validate" - User is being prompted with validation information.
   *     "complete" - Creation process complete.
   */
  public function status() {
    if (!is_readable($this->statusUri)) {
      return 'upload';
    }
    return file_get_contents($this->statusUri);
  }

  /**
   * Moves an uploaded file into place.
   *
   * @param object $file
   *   File object that is to be moved into place.
   */
  public function upload($file) {
    // Delete any existing files.
    $this->clear();

    // Prepare the directory to save the file.
    $dir = dirname($this->dataUri);
    file_prepare_directory($dir, FILE_MODIFY_PERMISSIONS || FILE_CREATE_DIRECTORY);

    // Prepare the directories that will hold the data.
    foreach (array(
               $this->dataDir,
               $this->errorDir,
             ) as $dir) {
      file_prepare_directory($dir, FILE_MODIFY_PERMISSIONS | FILE_CREATE_DIRECTORY);
    }

    // Move the new file in.
    file_move($file, $this->dataUri, FILE_EXISTS_REPLACE);
  }

  /**
   * Clear all data files for a the upload.
   */
  public function clear() {
    if ($file = $this->lookupFile($this->dataUri)) {
      file_delete($file);
    }
    if (is_readable($this->dataDir)) {
      file_unmanaged_delete_recursive($this->dataDir);
    }
    if ($file = $this->lookupFile($this->errorUri)) {
      file_delete($file);
    }
    if (is_readable($this->errorDir)) {
      file_unmanaged_delete_recursive($this->errorDir);
    }
    if (is_readable($this->statusUri)) {
      file_unmanaged_delete($this->statusUri);
    }
    $this->deleteRecords();
  }

  /**
   * Provide a user's uploaded data file.
   */
  public function dataFile() {
    return $this->lookupFile($this->dataUri);
  }

  /**
   * Provide a user's errored records file.
   */
  public function errorFile() {
    return $this->lookupFile($this->errorUri);
  }

  /**
   * Parse data read from the CSV upload.
   *
   * This accepts multiple lines, each an array of data with the following info:
   *   - line: An array of CSV data.
   *   - line_no: An integer indicating which line to which it corresponds.
   *
   * @param array $lines
   *   An array of uploaded CSV data.
   * @param array $validate
   *   An array available for saving validation data.
   */
  public function parseLines($lines, &$validate) {
    $records = array();
    // Parse and validate individually.
    foreach ($lines as $data) {
      $record = $this->parseLine($data);
      $this->validateRecord($record, $validate);
      $records[$record->lineNo] = $record;
    }
    // Validate the set/subset.
    $this->validateRecords($records, $validate);
    // Save each record.
    foreach ($records as $record) {
      $this->saveRecord($record);
    }
  }

  /**
   * Parse a line as read from CSV into an record object.
   *
   * This is specific to the upload type and is to be implemented by each
   * subclass.
   *
   * @param array $data
   *   A keyed array containing data read from the upload.
   *   - line: An array of CSV data.
   *   - line_no: An integer indicating which line to which it corresponds.
   *
   * @return object
   *   An object representing data read from the uploaded file.
   */
  abstract protected function parseLine($data);

  /**
   * Gives subclasses the chance to build and output their error records.
   *
   * @param object $record
   *   The error record
   *
   * @return string
   *   The error message(s) as a string.
   */
  public function parseError($record) {

  }

  /**
   * Validates a set of uploaded records.
   *
   * This will be unused except when implemented by a subclass.
   *
   * @param array $records
   *   An array of parsed CSV records.
   * @param array $validate
   *   An array available for saving validation data.
   */
  protected function validateRecords($records, &$validate) {
  }

  /**
   * Validate an uploaded record.
   *
   * This function will perform various checks on the uploaded record and any
   * errors found will be placed into the $record->errors array. The key is a
   * machine name and the value is a textual label visible to the user in the
   * validation and export screens as well as the error CSV.
   *
   * @param object $record
   *   A record parsed from the CSV ready for validation.
   * @param array $validate
   *   An array available for saving validation data.
   */
  abstract protected function validateRecord($record, &$validate);

  /**
   * Generate entities from uploaded record data.
   *
   * @param array $record_nos
   *   Record numbers corresponding to uploaded CSV data.
   * @param array $validate
   *   An array available for saving validation data.
   */
  public function generateEntities($record_nos, &$validate) {

    // Re-parse and re-validate the records.
    $records = array();
    foreach ($record_nos as $record_no) {
      if ($record = $this->loadRecord('valid', $record_no)) {
        $data = array(
          'line_no' => $record->lineNo,
          'line' => $record->line,
        );
        $record = $this->parseLine($data);
        $this->validateRecord($record, $validate);
        $records[] = $record;
      }
    }
    $this->validateRecords($records, $validate);

    // Iterate through the records, attempt to generatex and handle errors as
    // needed.
    foreach ($records as $record) {
      // If the record did not validate, move it to errored dir.
      if (!empty($record->errors)) {
        $this->saveRecord($record);
      }
      else {
        try {
          $this->generateEntity($record);
          $this->saveRecord($record, 'complete');
        } catch (Exception $e) {
          $this->saveRecord($record, 'error');
        }
      }
    }

  }

  /**
   * Generate an entity from uploaded record data.
   *
   * This is specific to each type of upload, to be implemented by child
   * classes.
   *
   * @param object $record
   *   The record object to be generated into an entity.
   */
  abstract protected function generateEntity($record);

  /**
   * Deletes pads_import records.
   *
   * @param null|string $dest
   *   Complete, error, or valid. NULL value deletes all.
   * @param int $line_number
   *   Line number to delete.
   *
   * @return int
   *   Number of affected records.
   */
  private function deleteRecords($dest = NULL, $line_number = 0) {
    $query = db_delete('pads_import_records')
      ->condition('uid', $this->account->uid)
      ->condition('type', $this->type);
    if ($dest) {
      $query->condition('dest', $dest);
    }
    if ($line_number > 0) {
      $query->condition('line_no', $line_number);
    }
    return $query->execute();
  }

  /**
   * Saves a parsed and validated record to disk.
   *
   * @param object $record
   *   The record object.
   *
   * @param null|string $dest
   *   Optional destination parameter.
   */
  protected function saveRecord($record, $dest = NULL) {
    if (!isset($dest)) {
      $dest = empty($record->errors) ? 'valid' : 'error';
    }

    $line_number = $record->lineNo;
    // Delete existing records.
    $this->deleteRecords($dest, $line_number);

    $data = drupal_json_encode($record);
    if (!$data) {
      $data = new \stdClass();
      $data->line = array();
      $data->lineNo = $record->lineNo;
      $data->errors = array(
        json_last_error() => json_last_error_msg(),
      );
      $data = drupal_json_encode($data);
    }
    db_insert('pads_import_records')
      ->fields(array('uid', 'type', 'line_no', 'dest', 'data'))
      ->values(array(
        $this->account->uid,
        $this->type,
        $line_number,
        $dest,
        $data,
      ))
      ->execute();
  }

  /**
   * Writes a data file containing status information.
   *
   * @param null|string $status
   *   The data to write to the file.
   */
  public function writeStatus($status = NULL) {
    file_put_contents($this->statusUri, $status);
  }

  /**
   * Provides a list of record files.
   *
   * @param string $type
   *   Specifies which type of records to return, "valid",  "error", or
   *   "complete".
   *
   * @return array
   *   An array of records. These are file names which correspond to line
   *   numbers from the original CSV.
   */
  public function recordFiles($type = 'valid') {
    $result = db_select('pads_import_records', 'r')
      ->fields('r', array('dest', 'line_no', 'data'))
      ->condition('r.dest', $type)
      ->condition('uid', $this->account->uid)
      ->condition('type', $this->type)
      ->execute();

    while ($record = $result->fetchAssoc()) {
      $this->records[$this->account->uid][$type][$record['line_no']] = $record['data'];
    }
    return isset($this->records[$this->account->uid][$type]) ?
      array_keys($this->records[$this->account->uid][$type]) :
      array();
  }

  /**
   * Loads the data file for an uploaded record.
   *
   * @param string $type
   *   Specifies which type of records to return, "valid" or "error".
   * @param int $num
   *   The number of the record. This is the line number from the original CSV.
   *
   * @return object
   *   The loaded record.
   */
  public function loadRecord($type, $num) {
    if (!isset($this->records[$this->account->uid][$type][$num])) {
      $this->recordFiles($type);
    }
    return json_decode($this->records[$this->account->uid][$type][$num]);
  }

  /**
   * Handle import file column header.
   *
   * @param resource $fh
   *   File handler resource.
   */
  public function handleColumnHeader($fh) {
    // Read first line and ignore it.
    fgetcsv($fh);
  }

  /**
   * Writes a CSV containing the upload's errored records.
   *
   * $return string
   *   A string indicating the URI of the exported error file.
   */
  public abstract function writeErrors();

  /**
   * The number of records to process in a single batch iteration.
   *
   * This defaults to 100, but can be set via variables named
   * pads_import_[upload_type]_batch_size.
   *
   * @return int
   *   Batch size.
   */
  public function batchSize() {
    $key = "pads_import_{$this->type}_batch_size";
    return variable_get($key, 100);
  }

  /**
   * Find a file by URI.
   *
   * @param string $uri
   *   URI of the file to find.
   *
   * @return object
   *   A fully-loaded file object of the file found at the specified URI.
   */
  public static function lookupFile($uri) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'file')
      ->propertyCondition('uri', $uri);
    $result = $query->execute();
    if (!empty($result['file'])) {
      $fid = array_keys($result['file']);
      $fid = reset($fid);
      return file_load($fid);
    }
  }

  /**
   * Instantiate PadsImportFile object based on type.
   *
   * @param string $type
   *   Users or students.
   *
   * @return PadsImportStudentFile|PadsImportUserFile
   *   The PadsImportFile object.
   */
  public static function getInstance($type, $account = NULL) {
    global $user;

    if (!$account) {
      $account = user_load($user->uid);
    }

    switch ($type) {
      case 'user':
        $file = new PadsImportUserFile($account);
        break;

      case 'tapuser':
        $file = new PadsImportTapUserFile($account);
        break;

      case 'student':
        $file = new PadsImportStudentFile($account);
        break;

      case 'organization':
        $file = new PadsImportOrganizationFile($account);
        break;

      case 'class':
        $file = new PadsImportClassFile($account);
        break;

      case 'dmrsuser':
        $file = new PadsImportDmrsUserFile($account);
        break;

      default:
        $file = FALSE;
    }

    return $file;
  }

  /**
   * Adds property to $record with a trimmed value.
   *
   * @param object $record
   *   The record object.
   * @param string $property
   *   The record property to set.
   * @param string $value
   *   The value to set the record property.
   * @param mixed $empty_value
   *   Value to set if $value is empty, defaults to NULL.
   */
  protected function getLine(&$record, $property, $value, $empty_value = NULL) {
    $clean_value = trim($value);
    $record->$property = strlen($clean_value) != 0 ? $clean_value : $empty_value;
  }

  /**
   * Do something post validation batch run.
   */
  public function postValidation() {

  }

  /**
   * A user's state org.
   */
  protected function userStateOrg() {
    $states = pads_org_load_user_states($this->account);
    if ($states && count($states) == 1) {
      return reset($states);
    }
  }

}
