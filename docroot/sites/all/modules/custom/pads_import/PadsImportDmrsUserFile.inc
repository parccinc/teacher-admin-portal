<?php
/**
 * @file
 * Contains \PadsImportDmrsUserFile.
 */

/**
 * Class PadsImportDmrsUserFile.
 */
class PadsImportDmrsUserFile extends PadsImportFile {

  protected $type = 'dmrsuser';
  protected $roles;

  /**
   * {@inheritdoc}
   */
  public function __construct(\stdClass $account) {
    parent::__construct($account);
    $roles = user_roles();
    $tap_roles = array('dmrs user');
    foreach ($roles as $rid => $role) {
      if (in_array($role, $tap_roles)) {
        $this->roles[$role] = $rid;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function handleColumnHeader($fh) {
    if ($line = fgetcsv($fh)) {
      // Verify 16 columns.
      if (count($line) != 16) {
        $template_link = l(t('DMRS User Template'), 'pads-import/download/dmrsuser_template.csv');
        drupal_set_message(t('Invalid DMRS User template. To ensure a successful upload, make sure your CSV file is in !template_link format.', array(
          '!template_link' => $template_link,
        )), 'warning');
        // Puts file pointer at end of file.
        fseek($fh, 0, SEEK_END);
      }
    }
  }

  /**
   * Like php's intval, but checks only for 0 or 1 and returns NULL otherwise.
   *
   * @param string $var
   *   Variable to check for a 1 or 0.
   *
   * @return string|NULL
   *   Returns value if allowed, NULL otherwise.
   */
  protected function permissionvalue($var) {
    $allowed_values = array('0', '1');
    $value = in_array($var, $allowed_values) ? $var : NULL;
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  protected function parseLine($data) {
    $record = new stdClass();
    $record->line = $data['line'];
    $record->lineNo = $data['line_no'];

    $this->getLine($record, 'firstName', $data['line'][0]);
    $this->getLine($record, 'lastName', $data['line'][1]);
    $this->getLine($record, 'email', $data['line'][2]);
    $this->getLine($record, 'stateID', $data['line'][3]);
    $this->getLine($record, 'districtID', $data['line'][4]);
    $this->getLine($record, 'institutionID', $data['line'][5]);
    $this->getLine($record, 'general', $this->permissionvalue($data['line'][6]));
    $this->getLine($record, 'pii', $this->permissionvalue($data['line'][7]));
    $this->getLine($record, 'roster', $this->permissionvalue($data['line'][8]));
    $this->getLine($record, 'sfExtract', $this->permissionvalue($data['line'][9]));
    $this->getLine($record, 'rfExtract', $this->permissionvalue($data['line'][10]));
    $this->getLine($record, 'pfExtract', $this->permissionvalue($data['line'][11]));
    $this->getLine($record, 'psrdExtract', $this->permissionvalue($data['line'][12]));
    $this->getLine($record, 'cdsExtract', $this->permissionvalue($data['line'][13]));
    $this->getLine($record, 'piiAnalytics', $this->permissionvalue($data['line'][14]));
    $this->getLine($record, 'staffID', $data['line'][15]);

    $record->organizations = array();
    $identifier = trim($record->stateID);
    $org_id = pads_org_get_id($record->stateID, $identifier);
    $record->organizations[$identifier] = $org_id ?: $identifier;

    return $record;
  }

  /**
   * {@inheritdoc}
   */
  protected function validateRecord($record, &$validate) {

    // ADS-1329 All permissions are required.
    $required_fields = array(
      'firstName' => 'First name',
      'lastName' => 'Last name',
      'email' => 'E-mail',
      'stateID' => 'State ID',
      'general' => 'General',
      'pii' => 'PII',
      'roster' => 'Roster',
      'sfExtract' => 'SF_Extract',
      'rfExtract' => 'RF_Extract',
      'pfExtract' => 'PF_Extract',
      'psrdExtract' => 'PRSD_Extract',
      'cdsExtract' => 'CDS_Extract',
      'piiAnalytics' => 'PII_Analytics',
    );
    foreach ($required_fields as $required_field => $field_name) {
      if (!isset($record->$required_field) || trim($record->$required_field) == '') {
        $record->errors["{$required_field}_required"] = t('Column %field is required.', array(
          '%field' => $field_name,
        ));
      }
    }

    // Validate email.
    if (!isset($record->errors) || !array_key_exists('email_required', $record->errors)) {
      $email = $record->email;
      if (!valid_email_address($email)) {
        $record->errors['email_valid'] = t('%email is not a valid email address.', array(
          '%email' => $email,
        ));
      }
      // Check email does not exist.
      if (user_load_by_mail($email)) {
        $record->errors['email_exists'] = t('%email already exists in the system.', array(
          '%email' => $email,
        ));
      }
    }

    if (array_key_exists('emails', $validate) && array_search($email, $validate['emails']) !== FALSE) {
      $record->errors['email_exists'] = t('%email already exists in the file.', array(
        '%email' => $email,
      ));
    }

    $validate['emails'][] = $email;

    // Do we have at least one permission?
    $permission_fields = array(
      'general',
      'pii',
      'roster',
      'sfExtract',
      'rfExtract',
      'pfExtract',
      'psrdExtract',
      'cdsExtract',
      'piiAnalytics',
    );
    $has_permission = FALSE;
    foreach ($permission_fields as $permission_field) {
      if (isset($record->$permission_field) && $record->$permission_field == 1) {
        $has_permission = TRUE;
        break;
      }
    }
    if (!$has_permission) {
      $record->errors["permission_required"] = t('You must enable at least one permission.');
    }

    // Valid Organization?
    if (!empty($record->organizations)) {
      foreach ($record->organizations as $org_id) {
        if (!$org = pads_org_load($org_id)) {
          $record->errors['org_valid'][] = t('%org_id is not a valid Organization id.', array(
            '%org_id' => $org_id,
          ));
        }
        elseif (!pads_org_access($org)) {
          $record->errors['org_valid'][] = t('The Organization id %org_id is not within your associated Organizations.', array(
            '%org_id' => $org_id,
          ));
        }
      }
    }
  }

  /**
   * Generate an entity from uploaded record data.
   *
   * This is specific to each type of upload, to be implemented by child
   * classes.
   *
   * @param object $record
   *   The record object to be generated into an entity.
   */
  protected function generateEntity($record) {
    // Create user.
    $name = "{$record->firstName}.{$record->lastName}";
    if (module_exists('email_registration')) {
      // Prepending email_registration_ to the user name will kick off the
      // user save hooks from the email_registration module and safely provide
      // a unique username based on email.
      $name = 'email_registration_' . user_password();
    }

    // Create the user entity.
    $data = array(
      'name' => $name,
      'mail' => $record->email,
      'init' => $record->email,
      'pass' => user_password(),
      'status' => TRUE,
    );
    $roles = array('dmrs user');
    $rids = $user_roles = array();
    foreach ($roles as $role) {
      $rid = $this->roles[$role];
      $rids[$rid] = $rid;
    }
    $data['roles'] = $rids;

    // ADS-524 Gotta put the name values up here so LDAP provisioning happens.
    $data['field_full_name']['und'][0]['given'] = $record->firstName;
    $data['field_full_name']['und'][0]['family'] = $record->lastName;

    $account = entity_create('user', array());
    user_save($account, $data);

    _dmrs_sso_user_create_dmrs_profile($account,
      $record->staffID,
      $record->stateID,
      $record->districtID,
      $record->institutionID,
      $record->general,
      $record->pii,
      $record->roster,
      $record->sfExtract,
      $record->rfExtract,
      $record->pfExtract,
      $record->psrdExtract,
      $record->cdsExtract,
      $record->piiAnalytics);

    $orgs = implode(',', $record->organizations);
    pads_org_assign_orgs_to_user($orgs, $account);

    _user_mail_notify('register_admin_created', $account);

    // Reload and resave to get all of our properties provisioned to LDAP.
    $account = user_load($account->uid);
    user_save($account);

  }

  /**
   * {@inheritdoc}
   */
  public function parseError($record) {
    // Abort if there are no errors.
    if (!isset($record->errors)) {
      return '';
    }
    $errors = $record->errors;
    $implode_me = array();
    foreach ($errors as $key => $value) {
      if (is_array($value)) {
        $implode_me = array_merge($implode_me, $value);
      }
      else {
        $implode_me[] = $value;
      }
    }
    $errors = implode(',', $implode_me);
    return $errors;
  }


  /**
   * Writes a CSV containing the upload's errored records.
   *
   * $return string
   *   A string indicating the URI of the exported error file.
   */
  public function writeErrors() {
    global $user;

    // Load or create a new file entity.
    if ($file = $this->errorFile()) {
      return $file->uri;
    }

    $file = new stdClass();
    $file->uid = $user->uid;
    $file->uri = $this->errorUri;
    $file->filename = drupal_basename($file->uri);

    // Write the CSV.
    $fh = fopen($this->errorUri, 'w');
    $header = array(
      'First name',
      'Last name',
      'Email',
      'State ID',
      'District ID',
      'Institution ID',
      'General',
      'PII',
      'Roster',
      'SF_Extract',
      'RF_Extract',
      'PF_Extract',
      'PSRD_Extract',
      'CDS_Extract',
      'PII_Analytics',
      'Staff ID',
      'Error',
    );
    fputcsv($fh, $header);

    $errors = $this->recordFiles('error');

    foreach ($errors as $line_no) {
      $record = $this->loadRecord('error', $line_no);
      $line = $record->line;
      $error = strip_tags($this->parseError($record));
      $line[] = $error;
      fputcsv($fh, $line);
    }
    fclose($fh);

    // Insert/update the db record.
    file_save($file);

    return $file->uri;
  }

}
