<?php
/**
 * @file
 * Pages & forms associated with the import workflow.
 */

/**
 * Page callback for uploading student and user data.
 *
 * @param string $type
 *   The expected values are 'user' for user import and 'student' for student
 *   import.
 *
 * @return array
 *   The form array.
 */
function tap_import_upload_page($type) {
  $args = func_get_args();
  // Be aggressive in deleting the files. No second chances.
  $file = padsImportFile::getInstance($type);
  if (count($args) != 2) {
    $file->clear();
  }
  elseif ($args[1] != 'complete') {
    $file->clear();
  }

  $text = t('Confirmation');
  if ($file->status() == 'complete') {
    switch ($type) {
      case 'student':
        $text = t('Upload Student Data Confirmation');
        break;

      case 'tapuser':
      case 'user':
        $text = t('Create User Accounts Confirmation');
        break;

      case 'organization':
        $text = t('Upload Organization Data Confirmation');
        break;

      case 'class':
        $text = t('Upload Class Data Confirmation');
        break;

      case 'dmrsuser':
        $text = t('Upload DMRS Users Confirmation');
        break;
    }

    drupal_set_title($text);
    return drupal_get_form('pads_import_complete_form', $file, $type);
  }

  // Waiting for a file upload.
  return drupal_get_form('pads_import_upload_form', $type);
}

/**
 * Import form.
 */
function pads_import_upload_form($form, &$form_state, $type = NULL) {

  $form['#file_type'] = $type;
  // Adds complete to the form action as a measure to prevent files form being
  // cleared.
  $action_path = current_path();
  $path_parts = explode('/', $action_path);
  if (array_pop($path_parts) != 'complete') {
    $action_path .= '/complete';
  }
  $form['#action'] = url($action_path);

  $filesizemax = 2048;
  $template_file = $template_text = $template_extra_markup = '';

  switch ($type) {
    case 'student':
      $template_file = '/student_template.csv';
      $template_text = t('Enrollment Template');
      break;

    case 'tapuser':
      $template_file = '/tapuser_template.csv';
      $template_text = t('Tap User Template');
      break;

    case 'user':
      $template_file = '/user_template.csv';
      $template_text = t('User Template');
      break;

    case 'organization':
      $template_file = '/organization_template.csv';
      $template_text = t('Organization Template');
      break;

    case 'class':
      $template_file = '/class_template.csv';
      $template_text = t('Class Template');
      break;

    case 'dmrsuser':
      $template_file = '/dmrsuser_template.csv';
      $template_text = t('DMRS User Template');
      $template_extra_markup = '<br><strong>' . t('Attention: The DMRS User Template has changed. Please make sure you are using the latest template.') . '</strong>';
      break;

  }

  $template_link = l($template_text, 'pads-import/download' . $template_file);
  $template_markup = t('To ensure a successful upload, make sure your CSV file is in !link format. <span class="small-instruct">(Click to download sample .csv format file.)</span>', array(
    '!link' => $template_link,
  ));
  $template_markup = $template_markup . $template_extra_markup;

  $form['contents']['notes'] = array(
    '#type' => 'item',
    '#markup' => $template_markup,
  );

  $form['contents']['file'] = array(
    '#type' => 'plupload',
    '#title' => t('Source File'),
    '#autoupload' => TRUE,
    '#autosubmit' => TRUE,
    '#submit_element' => '#edit-upload',
    '#upload_validators' => array(
      'file_validate_extensions' => array('csv'),
      'file_validate_size' => array($filesizemax * 1024),
    ),
    '#event_callbacks' => array(
      'FilesAdded' => 'Drupal.padsImport.filesAddedCallback',
    ),
    '#plupload_settings' => array(
      'runtimes' => 'html5,flash,silverlight,html4',
      // The cardinality setting is optional. If exists, js/pads_import.js
      // will display an error message if the number of files added to the
      // plupload widget via file drag and drop do not match the cardinality
      // value.
      'cardinality' => 1,
    ),
  );

  $form['#attached']['js'][] = drupal_get_path('module', 'pads_import') . '/js/pads_import.js';

  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 100,
  );
  $form['actions']['upload'] = array(
    '#type' => 'submit',
    '#value' => t('Start @type import', array(
      '@type' => $type,
    )),
    '#attributes' => array(
      'class' => array('element-invisible'),
    ),
  );

  return $form;
}

/**
 * Pads import form submit.
 */
function pads_import_upload_form_submit($form, &$form_state) {
  $type = $form['#file_type'];
  $plupload_file = $form_state['values']['file'][0];
  $plupload_name = $plupload_file['name'];
  $filename = drupal_realpath($plupload_file['tmppath']);
  // S3fsStreamWrapper does not support, so we check if FALSE.
  if (!$filename) {
    // Try and move the file to temporary storage.
    $import_temp_dir = PADS_IMPORT_TEMP_DIR;
    file_prepare_directory($import_temp_dir, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
    $temp_uri = $import_temp_dir . '/' . $plupload_name;
    $filename = file_unmanaged_copy($plupload_file['tmppath'], $temp_uri);
    if ($filename) {
      $filename = drupal_realpath($filename);
    }
  }
  if (!$filename) {
    drupal_set_message(t('Unable to read uploaded file.'), 'error');
    return;
  }
  $file = file_save_data(file_get_contents($filename), "private://{$plupload_name}");
  $import_file = PadsImportFile::getInstance($type);
  $import_file->upload($file);

  $operations = array();
  switch ($type) {
    case 'class':
    case 'organization':
    case 'student':
      $operations[] = array(
        'pads_import_validation_batch',
        array($form['#file_type']),
      );
      break;

    case 'tapuser':
    case 'user':
    case 'dmrsuser':
      $operations[] = array(
        'pads_import_validation_batch',
        array($form['#file_type']),
      );
      $operations[] = array(
        'pads_import_creation_batch',
        array($form['#file_type']),
      );
      break;

  }

  $batch = array(
    'title' => t('Processing Upload'),
    'operations' => $operations,
    'progress_message' => t('@elapsed elapsed, @estimate remaining.'),
    'file' => drupal_get_path('module', 'pads_import') . '/pads_import.batch.inc',
  );

  batch_set($batch);
}

/**
 * Pads import complete form.
 */
function pads_import_complete_form($form, &$form_state, $file, $type) {
  $form['#file'] = $file;

  $error_records = $file->recordFiles('error');
  $complete_records = $file->recordFiles('complete');

  switch ($type) {
    case 'tapuser':
      $type_label = t('user');
      break;
    case 'dmrsuser':
      $type_label = t('DMRS User');
      break;
    default:
      $type_label = $type;
      break;
  }

  $pads_type_plural = ($type_label == 'class') ? 'classes' : $type_label . 's';

  $back_button_text = $type != 'class' ? t('Back to @type import', array(
    '@type' => ucfirst($type_label),
  )) : t('Add More Classes');

  $num_completed_records = count($complete_records);
  $num_errors_records = count($error_records);
  $num_total_records = $num_completed_records + $num_errors_records;

  $pads_errorfunction = ($type == 'class') ? 'pads_import_error_' . 'classes' : 'pads_import_error_' . $type . 's';

  switch ($type) {
    case 'user':
      if ($num_completed_records) {
        $form['success'] = array(
          '#prefix' => '<p>',
          '#markup' => t('%completed users were successfully registered.', array(
            '%completed' => $num_completed_records,
          )),
          '#suffix' => '</p>',
        );

      }

      if (count($error_records)) {
        $form['errors'] = $pads_errorfunction($file, $error_records);
      }
      break;

    case 'class':
    case 'student':
    case 'tapuser':
    case 'organization':
    case 'dmrsuser':
      switch ($type) {
        case 'student':
          // Only school level can access student records.
          $markup = (pads_organization_get_user_org_level() == PADS_ORGANIZATION_LEVEL_SCHOOL ?
            t('You can access the new student records and complete their profile.') : '');
          break;
        case 'class':
          $markup = t('You can add more classes.');
          break;
        case 'dmrsuser':
          $markup = '';
          break;
        default:
          $markup = t('You can access the new !type_label records.', array(
            '!type_label' => $type_label,
          ));
          break;
      }
      $label = format_plural(count($error_records), $type_label, $pads_type_plural);
      if ($num_total_records) {
        // If there are no errors, show all good message.
        if ($num_errors_records == 0) {
          $form['info']['noerrors'] = array(
            '#type' => 'container',
            '#attributes' => array(
              'class' => array('noerrors'),
            ),
            '0' => array(
              '#type' => 'item',
              '#markup' => t('%completed out of %total %label were created successfully.', array(
                '%completed' => $num_completed_records,
                '%total' => $num_total_records,
                '%label' => $label,
              )),
            ),
            '1' => array(
              '#type' => 'item',
              '#markup' => $markup,
            ),
          );
        }
        // There are some errors.
        else {
          drupal_set_title(t('Upload !type data confirmation with exceptions', array(
            '!type' => ucfirst($type_label),
          )));

          // Present error messaging referring to the proper template for the
          // upload.
          $params = array();
          switch ($type) {
            case 'organization':
              $params['!link'] = l('Organization Template', 'pads-import/download/organization_template.csv');
              break;
            case 'class':
              $params['!link'] = l('Class Template', 'pads-import/download/class_template.csv');
              break;
            case 'student':
              $params['!link'] = l('Enrollment Template', 'pads-import/download/student_template.csv');
              break;
            case 'tapuser':
              $params['!link'] = l('Tap User Template', 'pads-import/download/tapuser_template.csv');
              break;
            case 'dmrsuser':
              $params['!link'] = l('DMRS User Template', 'pads-import/download/dmrsuser_template.csv');
              break;
            default:
              $params['!link'] = check_plain($type);
          }
          $error_msg = t('Some records could not be uploaded. To ensure a successful upload, make sure your CSV file is in !link format. Please see the details below.', $params);
          $form['info']['errors'][] = array(
            '#type' => 'item',
            '#markup' => '<div class="messages warning">' . $error_msg . '</div>',
          );
          $form['info']['errors'][] = array(
            '#type' => 'container',
            '#attributes' => array(
              'class' => array('errors'),
            ),
            '0' => array(
              '#type' => 'item',
              '#markup' => t('Total records in file: %total :: %completed %label imported successfully.', array(
                '%completed' => $num_completed_records,
                '%label' => $label,
                '%total' => $num_total_records,
              )),
            ),
          );
          $error_records_markup = format_plural(
            $num_errors_records,
            'There is %count erroneous record. You can download the record as a .csv file in order to make corrections and re-upload to TAP.',
            'There are %count erroneous records. You can download these records as a .csv file in order to make corrections and re-upload to TAP.',
            array('%count' => $num_errors_records)
          );
          $form['info']['errors'][] = array(
            '#type' => 'container',
            '#attributes' => array(
              'class' => array('errors'),
            ),
            '0' => array(
              '#type' => 'item',
              '#markup' => t('Rejected Records: %error_records', array(
                '%error_records' => $num_errors_records,
              )),
            ),
            '1' => array(
              '#type' => 'item',
              '#markup' => t('Rejected records are the ones that could not be created due to a data error.'),
            ),
            '2' => array(
              '#type' => 'item',
              '#markup' => $error_records_markup,
            ),
          );
        }
      }

      if (count($error_records)) {
        $error_table = $pads_errorfunction($file, $error_records);
        $form['errors']['table'] = $error_table;
      }
      break;

  }

  if ($num_errors_records > 0) {
    $form['actions']['export'] = array(
      '#type' => 'submit',
      '#value' => t('Download errors (.csv)'),
      '#submit' => array('pads_import_error_export'),
    );
  }
  $form['actions']['back'] = array(
    '#type' => 'submit',
    '#value' => $back_button_text,
    '#submit' => array('pads_import_complete_form_back'),
  );

  // Additional action items per type.
  switch ($type) {
    case 'class':
      $form['actions']['class'] = array(
        '#type' => 'submit',
        '#value' => t('View My Classes'),
        '#submit' => array('pads_import_view_classes'),
      );
      break;

  }

  return $form;
}

/**
 * Submit callback for "back" button on pads_import_complete_form.
 */
function pads_import_complete_form_back(&$form, &$form_state) {
  $form['#file']->clear();

  // Remove .../complete from the path.
  $form_action_parts = explode('/', ltrim($form['#action'], '\//'));
  array_pop($form_action_parts);
  $form_state['redirect'] = implode('/', $form_action_parts);
}

/**
 * Submit callback to go to classes page.
 *
 * @param array $form
 *   The submitted form.
 * @param array $form_state
 *   The form_state array.
 */
function pads_import_view_classes(&$form, &$form_state) {
  pads_import_complete_form_back($form, $form_state);
  $orgs = pads_organization_get_user_orgs();
  $form_state['redirect'] = 'classes/' . key($orgs);
}

/**
 * Generates the table data for erroneous users.
 *
 * @param object $file
 *   Drupal file object.
 * @param array $error_records
 *   Array of faulty records.
 *
 * @return array
 *   Drupal table array for rendering.
 */
function pads_import_error_users($file, $error_records) {

  $header = array(
    t('Email'),
    t('Role'),
    t('State ID'),
    t('Organization'),
    t('Error Message'),
  );

  $rows = array();
  foreach ($error_records as $record) {
    $record = $file->loadRecord('error', $record);
    $row = array();
    foreach ($record->line as $cell) {
      $row[] = check_plain($cell);
    }
    $errors = isset($record->errors) ? implode(', ', (array) $record->errors) : '';
    $errors = "<span class=\"error\">$errors</span>";
    $row[] = array(
      'data' => $errors,
      'class' => array('td-error'),
    );

    $rows[] = $row;
  }

  return import_error_table($header, $rows);
}

/**
 * Generates the table data for erroneous tap users.
 *
 * @param object $file
 *   Drupal file object.
 * @param array $error_records
 *   Array of faulty records.
 *
 * @return array
 *   Drupal table array for rendering.
 */
function pads_import_error_tapusers($file, $error_records) {

  $table_info = array(
    t('First name') => 0,
    t('Last name') => 1,
    t('Email') => 2,
    t('Roles') => 3,
    t('State Code') => 4,
    t('Organization Identifiers') => 5,
  );

  $rows = array();
  foreach ($error_records as $record) {
    $record = $file->loadRecord('error', $record);
    $line = $record->line;
    $row = array();

    foreach ($table_info as $label => $index) {
      $row[] = array_key_exists($index, $line) ? check_plain($line[$index]) : '';
    }
    $errors = $file->parseError($record);
    $errors = "<span class=\"error\">$errors</span>";
    $row[] = array(
      'data' => $errors,
      'class' => array('td-error'),
    );

    $rows[] = $row;
  }

  $header = array_keys($table_info);
  $header[] = t('Error');

  return import_error_table($header, $rows);

}

/**
 * Generates the table data for erroneous dmrs users.
 *
 * @param object $file
 *   Drupal file object.
 * @param array $error_records
 *   Array of faulty records.
 *
 * @return array
 *   Drupal table array for rendering.
 */
function pads_import_error_dmrsusers($file, $error_records) {


  $table_info = array(
    t('First name') => 0,
    t('Last name') => 1,
    t('Email') => 2,
    t('State ID') => 3,
    t('District ID') => 4,
    t('Institution ID') => 5,
    t('General') => 6,
    t('PII') => 7,
    t('Roster') => 8,
    t('SF_Extract') => 9,
    t('RF_Extract') => 10,
    t('PF_Extract') => 11,
    t('PSRD_Extract') => 12,
    t('CDS_Extract') => 13,
    t('PII_Analytics') => 14,
    t('Staff ID') => 15,
  );

  $rows = array();
  foreach ($error_records as $record) {
    $record = $file->loadRecord('error', $record);
    $line = $record->line;
    $row = array();

    foreach ($table_info as $label => $index) {
      $row[] = array_key_exists($index, $line) ? check_plain($line[$index]) : '';
    }
    $errors = $file->parseError($record);
    $errors = "<span class=\"error\">$errors</span>";
    $row[] = array(
      'data' => $errors,
      'class' => array('td-error'),
    );

    $rows[] = $row;
  }

  $header = array_keys($table_info);
  $header[] = t('Error');

  return import_error_table($header, $rows);

}

/**
 * Generates the table data for erroneous student entries.
 *
 * @param object $file
 *   Drupal file object.
 * @param array $error_records
 *   Array of faulty records.
 *
 * @return array
 *   Drupal table array for rendering.
 */
function pads_import_error_students($file, $error_records) {
  drupal_set_title('Upload Student Data Confirmation with Exceptions');
  // Column header value is the key, line index is the value.
  $table_info = array(
    t('Student Identifier') => 1,
    t('First Name') => 2,
    t('Last Name') => 3,
    t('DOB') => 5,
    t('Gender') => 6,
    t('Grade') => 7,
  );

  $rows = array();
  foreach ($error_records as $record) {
    $record = $file->loadRecord('error', $record);
    $line = $record->line;
    $row = array();
    foreach ($table_info as $label => $index) {
      $row[] = array_key_exists($index, $line) ? check_plain($line[$index]) : '';
    }
    $errors = implode(', ', (array) $record->errors);
    $errors = "<span class=\"error\">$errors</span>";
    $row[] = array(
      'data' => $errors,
      'class' => array('td-error'),
    );

    $rows[] = $row;
  }

  $header = array_keys($table_info);
  $header[] = t('Error');

  return import_error_table($header, $rows);
}

/**
 * Generates the table data for erroneous organization entries.
 *
 * @param object $file
 *   Drupal file object.
 * @param array $error_records
 *   Array of faulty records.
 *
 * @return array
 *   Drupal table array for rendering.
 */
function pads_import_error_organizations($file, $error_records) {
  drupal_set_title('Upload Organization Data Confirmation with Exceptions');
  // Column header value is the key, line index is the value.
  $table_info = array(
    t('Name') => 0,
    t('Type') => 1,
    t('State Code') => 2,
    t('Identifier') => 3,
  );

  $rows = array();
  foreach ($error_records as $record) {
    $record = $file->loadRecord('error', $record);
    $line = $record->line;
    $row = array();
    foreach ($table_info as $label => $index) {
      $row[] = array_key_exists($index, $line) ? check_plain($line[$index]) : '';
    }
    $errors = implode(', ', (array) $record->errors);
    $errors = "<span class=\"error\">$errors</span>";
    $row[] = array(
      'data' => $errors,
      'class' => array('td-error'),
    );

    $rows[] = $row;
  }

  $header = array_keys($table_info);
  $header[] = t('Error');

  return import_error_table($header, $rows);
}

/**
 * Generates the table data for erroneous class entries.
 *
 * @param object $file
 *   Drupal file object.
 * @param array $error_records
 *   Array of faulty records.
 *
 * @return array
 *   Drupal table array for rendering.
 */
function pads_import_error_classes($file, $error_records) {
  drupal_set_title('Upload Class Data Confirmation with Exceptions');
  // Column header value is the key, line index is the value.
  $table_info = array(
    t('Organization Identifier') => 0,
    t('Class Identifier') => 1,
    t('Grade Level') => 2,
    t('Section Number') => 3,
  );

  $rows = array();
  foreach ($error_records as $record) {
    $record = $file->loadRecord('error', $record);
    $line = $record->line;
    $row = array();
    foreach ($table_info as $label => $index) {
      $row[] = array_key_exists($index, $line) ? check_plain($line[$index]) : '';
    }
    $errors = implode(', ', (array) $record->errors);
    $errors = "<span class=\"error\">$errors</span>";
    $row[] = array(
      'data' => $errors,
      'class' => array('td-error'),
    );

    $rows[] = $row;
  }

  $header = array_keys($table_info);
  $header[] = t('Error');

  return import_error_table($header, $rows);
}

/**
 * Submit callback for export button on tap_uploads_upload_error_form.
 */
function pads_import_error_export($form, &$form_state) {
  // Generate the CSV of erroneous records and redirect user to that file.
  $file = $form['#file'];
  $form_state['redirect'] = file_create_url($file->writeErrors());
}

/**
 * Ensures a file download of import template.
 *
 * @param string $file
 *   Path to the template file.
 */
function pads_import_template_download($file) {
  $filename = basename($file);
  $headers = array(
    'Content-Type' => 'text/csv',
    'Content-Length' => filesize($file),
    'Content-Disposition' => 'attachment; filename="' . $filename . '"',
    'Expires' => 0,
    'Pragma' => 'no-cache',
  );

  // @see file_transfer.
  if (ob_get_level()) {
    ob_end_clean();
  }

  foreach ($headers as $name => $value) {
    drupal_add_http_header($name, $value);
  }
  drupal_send_headers();
  // Transfer file in 1024 byte chunks to save memory usage.
  if ($fd = fopen($file, 'rb')) {
    while (!feof($fd)) {
      print fread($fd, 1024);
    }
    fclose($fd);
  }
  else {
    drupal_not_found();
  }
  drupal_exit();
}

/**
 * Common procedure to output import error table.
 *
 * @param array $header
 *   Table header
 * @param array $rows
 *   Table rows.
 *
 * @return array
 *   Renderable theme array.
 */
function import_error_table($header, $rows) {
  return array(
    '#theme' => 'table',
    '#attributes' => array(
      'id' => 'table_import_errors',
    ),
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('There are no rejected records to display.'),
  );
}
