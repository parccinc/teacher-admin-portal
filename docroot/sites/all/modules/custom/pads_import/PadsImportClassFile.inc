<?php
/**
 * @file
 * Contains \PadsImportClassFile.
 */

use Drupal\pads_organization\OrganizationFactory;

class PadsImportClassFile extends PadsLdrImportFile {

  protected $type = 'class';

  /**
   * PadsImportOrganizationFile constructor.
   */
  public function __construct($account) {
    parent::__construct($account);
  }

  /**
   * {@inheritdoc}
   */
  protected function parseLine($data) {
    $record = new stdClass();
    $record->line = $data['line'];
    $record->lineNo = $data['line_no'];
    $record->clientUserId = $this->account->uid;

    // Expecting organizationId,classIdentifier,gradeLevel,sectionNumber.

    $this->getLine($record, 'organizationIdentifier', $data['line'][0]);
    $this->getLine($record, 'classIdentifier', $data['line'][1]);
    $this->getLine($record, 'gradeLevel', $data['line'][2]);
    $this->getLine($record, 'sectionNumber', $data['line'][3]);

    // We need to infer the state code from the user's organizations.
    if ($state_org = $this->userStateOrg()) {
      $this->getLine($record, 'stateCode', $state_org['organizationName']);
    }

    // Remove NULL values.
    $record = (object) array_filter((array) $record);

    return $record;

  }

  /**
   * {@inheritdoc}
   */
  protected function generateEntity($record) {
    $args = (array) $record;
    unset($args['lineNo']);
    unset($args['line']);

    // Verify access to Org the class would belong to.
    $org_access = FALSE;
    $state_code = array_key_exists('stateCode', $args) ?
      $args['stateCode'] : FALSE;
    $org_identifier = $args['organizationIdentifier'];
    if ($state_code) {
      $org_id = OrganizationFactory::getInstance()
        ->getOrgId($state_code, $org_identifier);
      if ($org_id && pads_org_access($org_id, 'view organizations', array(), $this->account)) {
        $org_access = TRUE;
      }
    }

    if (!$org_access) {
      // No access to class Organization.
      $record->errors[] = t('Access to class Organization is required');
    }
    else {
      $response = pads_ldr_client_call('addClass', $args);
      if (array_key_exists('error', $response)) {
        $record->errors[] = $response['detail'];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function writeErrors() {
    global $user;

    // Load or create a new file entity.
    if ($file = $this->errorFile()) {
      return $file->uri;
    }

    $file = new stdClass();
    $file->uid = $user->uid;
    $file->uri = $this->errorUri;
    $file->filename = drupal_basename($file->uri);

    // Write the CSV.
    $fh = fopen($this->errorUri, 'w');
    $header = array(
      'organizationIdentifier',
      'classIdentifier',
      'gradeLevel',
      'sectionNumber',
    );
    fputcsv($fh, $header);

    $errors = $this->recordFiles('error');

    foreach ($errors as $line_no) {
      $record = $this->loadRecord('error', $line_no);
      $line = $record->line;
      $line[] = implode(', ', (array) $record->errors);
      fputcsv($fh, $line);
    }
    fclose($fh);

    // Insert/update the db record.
    file_save($file);

    return $file->uri;
  }

}
