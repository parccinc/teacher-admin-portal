<?php
/**
 * @file
 * Contains \PadsImportOrganizationFile.
 */

use Drupal\pads_organization\OrganizationFactory;

class PadsImportOrganizationFile extends PadsLdrImportFile {

  protected $type = 'organization';

  /**
   * PadsImportOrganizationFile constructor.
   */
  public function __construct($account) {
    parent::__construct($account);
  }

  /**
   * {@inheritdoc}
   */
  protected function parseLine($data) {
    $record = new stdClass();
    $record->line = $data['line'];
    $record->lineNo = $data['line_no'];
    $record->clientUserId = $GLOBALS['user']->uid;

    $this->getLine($record, 'organizationName', $data['line'][0]);
    // Allow the file to specify the type by number or by text.
    $this->getLine($record, 'organizationType', $data['line'][1]);
    if (!is_numeric($record->organizationType)) {
      $levels = pads_org_levels();
      $lower_levels = array_map('drupal_strtolower', $levels);
      $record->organizationType = array_search(drupal_strtolower($record->organizationType), $lower_levels);
    }
    if ($record->organizationType == PADS_ORGANIZATION_LEVEL_DISTRICT) {
      // If the record is a district, the state code is in the file.
      $this->getLine($record, 'parentStateCode', $data['line'][2]);
    }
    else {
      // If the record isn't for a district, we need to infer the state code
      // from the user's organizations.
      $states = pads_org_load_user_states();
      if ($states && count($states) == 1) {
        $state_org = reset($states);
        $this->getLine($record, 'parentStateCode', $state_org['organizationName']);
      }
      $this->getLine($record, 'parentOrganizationIdentifier', $data['line'][2]);
    }
    $this->getLine($record, 'organizationIdentifier', $data['line'][3]);

    // Remove NULL values.
    $record = (object) array_filter((array) $record);

    return $record;
  }

  /**
   * {@inheritdoc}
   */
  protected function generateEntity($record) {
    $args = (array) $record;
    unset($args['lineNo']);
    unset($args['line']);

    // Validate that we have a valid org type.
    if (!$record->organizationType) {
      $record->errors[] = t('Valid organization type is required');
    }

    // Validate access to parent Org.
    $organization = OrganizationFactory::getInstance();
    $parent_access = FALSE;
    $level = $args['organizationType'];
    if ($level == PADS_ORGANIZATION_LEVEL_STATE) {
      // User must have access to root Org.
      $parent_access = pads_org_access(1);
    }
    elseif ($level == PADS_ORGANIZATION_LEVEL_DISTRICT) {
      // User must have access to state.
      $params = array(
        'organizationPath' => PADS_ORGANIZATION_ROOT_NAME . '_' . $args['parentStateCode'],
      );
      $orgs = $organization->findOrgs($params);
      if (!empty($orgs) && count($orgs) == 1) {
        $parent_access = pads_org_access(reset($orgs));
      }
    }
    else {
      $state_code = $args['parentStateCode'];
      $organization_identifier = $args['parentOrganizationIdentifier'];
      $parent_org = $organization->getOrgId($state_code, $organization_identifier);
      if ($parent_org && pads_org_access($parent_org)) {
        $parent_access = TRUE;
      }
    }

    if (!$parent_access) {
      $record->errors[] = t('Access to parent Organization is required');
    }
    else {
      // We have parent access, let's add the Org.
      $org = pads_org_add($args);

      if (array_key_exists('error', $org)) {
        $record->errors[] = $org['detail'];
      }
      elseif (!empty($org)) {
        $this->idlist[] = $org['organizationId'];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function writeErrors() {
    global $user;

    // Load or create a new file entity.
    if ($file = $this->errorFile()) {
      return $file->uri;
    }

    $file = new stdClass();
    $file->uid = $user->uid;
    $file->uri = $this->errorUri;
    $file->filename = drupal_basename($file->uri);

    // Write the CSV.
    $fh = fopen($this->errorUri, 'w');
    $header = array(
      'organizationName',
      'organizationType',
      'parentOrganizationIdentifier',
      'organizationIdentifier',
    );
    fputcsv($fh, $header);

    $errors = $this->recordFiles('error');

    foreach ($errors as $line_no) {
      $record = $this->loadRecord('error', $line_no);
      $line = $record->line;
      $line[] = implode(', ', (array) $record->errors);
      fputcsv($fh, $line);
    }
    fclose($fh);

    // Insert/update the db record.
    file_save($file);

    return $file->uri;
  }

  /**
   * Method that runs after batch process validation.
   *
   * Validation for Ldr objects happen during Ldr POST request, so this
   * method is called items have been created in Ldr.
   *
   * @throws Exception
   */
  public function postValidation() {
    parent::postValidation();
    if (!empty($this->idlist)) {
      // Do something classy here.
    }
  }
}
