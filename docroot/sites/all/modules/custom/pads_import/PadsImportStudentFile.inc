<?php
/**
 * @file
 * Contains \PadsImportStudentFile.
 */

use Drupal\pads\classes\ClassFactory;
use Drupal\pads\students\StudentFactory;
use Drupal\pads_organization\OrganizationFactory;

class PadsImportStudentFile extends PadsLdrImportFile {

  // Declare the upload type.
  protected $type = 'student';

  protected $headers = array(
    "enrollSchoolIdentifier",
    "stateIdentifier",
    "lastName",
    "firstName",
    "middleName",
    "dateOfBirth",
    "gender",
    "gradeLevel",
    "raceAA",
    "raceAN",
    "raceAS",
    "raceHL",
    "raceHP",
    "raceWH",
    "statusDIS",
    "statusECO",
    "statusELL",
    "statusGAT",
    "statusLEP",
    "statusMIG",
    "disabilityType",
    "optionalStateData1",
    "optionalStateData2",
    "optionalStateData3",
    "optionalStateData4",
    "optionalStateData5",
    "optionalStateData6",
    "optionalStateData7",
    "optionalStateData8",
    "classAssignment1",
    "classAssignment2",
    "classAssignment3",
    "classAssignment4",
    "classAssignment5",
    "classAssignment6",
    "classAssignment7",
    "classAssignment8",
    "classAssignment9",
    "classAssignment10",
  );

  /**
   * PadsImportStudentFile constructor.
   */
  public function __construct($account) {
    parent::__construct($account);
  }

  /**
   * {@inheritdoc}
   */
  protected function parseLine($data) {
    $record = new stdClass();
    $record->line = $data['line'];
    $record->lineNo = $data['line_no'];
    $record->clientUserId = $this->account->uid;

    foreach ($this->headers as $pos => $name) {
      $value = array_key_exists($pos, $data['line']) ? $data['line'][$pos] : '';
      $this->getLine($record, $name, $value);
    }

    if ($state = $this->userStateOrg()) {
      $record->enrollStateCode = $state['organizationName'];
    }

    $record = (object) array_filter((array) $record);

    return $record;
  }

  /**
   * {@inheritdoc}
   */
  protected function generateEntity($record) {
    // No op.
  }

  /**
   * {@inheritdoc}
   */
  public function writeErrors() {
    global $user;

    // Load or create a new file entity.
    if ($file = $this->errorFile()) {
      return $file->uri;
    }

    $file = new stdClass();
    $file->uid = $user->uid;
    $file->uri = $this->errorUri;
    $file->filename = drupal_basename($file->uri);

    // Write the CSV.
    $fh = fopen($this->errorUri, 'w');
    fputcsv($fh, $this->headers);

    $errors = $this->recordFiles('error');

    foreach ($errors as $line_no) {
      $record = $this->loadRecord('error', $line_no);
      $line = $record->line;
      $line[] = implode(', ', (array) $record->errors);
      fputcsv($fh, $line);
    }
    fclose($fh);

    // Insert/update the db record.
    file_save($file);

    return $file->uri;
  }

  /**
   * {@inheritdoc}
   */
  protected function validateRecords($records, &$validate) {
    $args = array();

    foreach ($records as $record) {
      if (!empty($record->errors)) {
        $this->saveRecord($record, 'error');
        continue;
      }

      $item_args = (array) $record;

      $offset = $item_args['lineNo'];

      unset($item_args['lineNo']);
      unset($item_args['line']);

      $args['students'][$offset] = $item_args;
    }

    $response = StudentFactory::getInstance()->addStudents($args);

    $errors = array_key_exists('errors', $response) ? $response['errors'] :
      array();
    foreach ($errors as $line => $error) {
      $records[$line]->errors[] = $error['detail'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postValidation() {
    parent::postValidation();
    OrganizationFactory::getInstance()->cacheRebuild();
  }

  /**
   * {@inheritdoc}
   */
  protected function validateRecord($record, &$validate) {
    // If either property is missing, we've got to error right away.
    if (empty($record->enrollStateCode) || empty($record->enrollSchoolIdentifier)) {
      $record->errors['org_access'] = t('Unable to determine upload state.');
    }

    // Make the LDR call and hang onto the resulting org throughout the upload.
    if (!isset($validate['orgs'][$record->enrollStateCode][$record->enrollSchoolIdentifier])) {
      $factory = OrganizationFactory::getInstance();
      $params = array(
        'organizationIdentifier' => $record->enrollSchoolIdentifier,
        'stateCode' => $record->enrollStateCode,
      );
      $orgs = $factory->findOrgs($params);
      $validate['orgs'][$record->enrollStateCode][$record->enrollSchoolIdentifier] = pads_org_load(reset($orgs));
    }

    // If we could not determine the organization, we've got to error.
    if (empty($validate['orgs'][$record->enrollStateCode][$record->enrollSchoolIdentifier])) {
      $record->errors['org_access'] = t('Unable to determine student Organization.');
    }

    // If the user does not have access, set the error.
    if (!pads_org_access($validate['orgs'][$record->enrollStateCode][$record->enrollSchoolIdentifier], 'view organizations', array(), $this->account)) {
      $record->errors['org_access'] = t('Access to student Organization is required.');
    }

    // Check class assignments for access.
    if (!user_access('add student to any class', $this->account)) {
      $user_class_ids = array_keys(pads_classes_get_user_classes());
      $org_id = $validate['orgs'][$record->enrollStateCode][$record->enrollSchoolIdentifier]['organizationId'];
      $classes = ClassFactory::getInstance()->getClasses($org_id);

      // Ldr accepts a value of either class sectionNumber or classIdentifier
      // which means we don't know which. We'll start by searching the
      // sectionNumber first for a match.
      $classes_by_section_number = array();
      $classes_by_class_identifier = array();

      foreach ($classes['classes'] as $class) {
        if (!empty($class['sectionNumber'])) {
          $classes_by_section_number[$class['sectionNumber']] = $class;
        }
        $classes_by_class_identifier[$class['classIdentifier']] = $class;
      }

      $class_assignment_properties = array(
        'classAssignment1', 'classAssignment2', 'classAssignment3',
        'classAssignment4', 'classAssignment5', 'classAssignment6',
        'classAssignment7', 'classAssignment8', 'classAssignment9',
        'classAssignment10');

      $error_message = t('Cannot assign a student to a class you are not assigned to.');
      foreach ($class_assignment_properties as $property) {
        if (isset($record->$property) && !empty(trim($record->$property))) {
          $class_assignment = $record->$property;
          if (in_array($class_assignment, array_keys($classes_by_section_number))
            && !in_array($classes_by_section_number[$class_assignment]['classId'], $user_class_ids)) {
            $record->errors['classassignments'] = $error_message;
            break;
          }
          elseif (in_array($class_assignment, array_keys($classes_by_class_identifier))
            && !in_array($classes_by_class_identifier[$class_assignment]['classId'], $user_class_ids)) {
            $record->errors['classassignments'] = $error_message;
            break;
          }

        }
      }
    }
  }

}
