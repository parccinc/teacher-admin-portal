<?php
/**
 * @file
 * Step definitions relating to importing users and students.
 */

use Behat\Gherkin\Node\PyStringNode;
use Drupal\DrupalExtension\Context\DrupalSubContextBase;
use Drupal\DrupalExtension\Context\DrupalSubContextInterface;

class ImportContext extends DrupalSubcontextBase implements DrupalSubContextInterface {

  protected $latestUid;

  /**
   * @beforeScenario @import
   */
  public function setLatestUid() {
    $efq = new EntityFieldQuery();
    $efq->entityCondition('entity_type', 'user')
      ->propertyOrderBy('uid', 'DESC')
      ->range(0, 1);
    $result = $efq->execute();
    $this->latestUid = reset($result['user'])->uid;
  }

  /**
   * @afterScenario @import
   */
  public function killEmAll() {
    $efq = new EntityFieldQuery();
    $efq->entityCondition('entity_type', 'user')
      ->propertyCondition('uid', $this->latestUid, '>');
    $result = $efq->execute();
    if (!empty($result) && array_key_exists('user', $result)) {
      $uids = array_keys($result['user']);
      user_delete_multiple($uids);
    }
  }

  /**
   * @Then my :type error export should contain:
   */
  public function assertErrorExportContains($type, PyStringNode $content) {

    if (!$user = $this->getUser()) {
      throw new Exception('User not logged in.');
    }

    if (!$account = user_load($user->uid)) {
      throw new Exception('Unable to load user.');
    }

    $file = PadsImportFile::getInstance($type, $account);

    if (!$data = $file->errorFile()) {
      throw new Exception('Unable to load error file.');
    }

    if (!is_readable($data->uri)) {
      throw new Exception(sprintf('Unable to read error file "%s".', $data->uri));
    }

    $file_contents = trim(file_get_contents($data->uri));
    $content = trim((string) $content);

    if ($file_contents != $content) {
      throw new Exception(sprintf('Content of error file "%s" is not as expected.', $data->uri));
    }
  }

}
