<?php
/**
 * @file
 * Contains \PadsImportTapUserFile.
 */

/**
 * Class PadsImportTapUserFile.
 */
class PadsImportTapUserFile extends PadsImportFile {

  protected $type = 'tapuser';
  protected $roles;

  /**
   * Initiate TAP user roles.
   *
   * @throws Exception
   */
  public function __construct($account) {
    parent::__construct($account);
    $roles = user_roles();
    $tap_roles = pads_get_tap_roles();
    foreach ($roles as $rid => $role) {
      if (in_array($role, $tap_roles)) {
        $this->roles[$role] = $rid;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function parseLine($data) {
    $record = new stdClass();
    $record->line = $data['line'];
    $record->lineNo = $data['line_no'];

    $this->getLine($record, 'firstName', $data['line'][0]);
    $this->getLine($record, 'lastName', $data['line'][1]);
    $this->getLine($record, 'email', $data['line'][2]);
    $this->getLine($record, 'roles', $data['line'][3]);
    $this->getLine($record, 'state_code', $data['line'][4]);
    $this->getLine($record, 'organizations_raw', !empty($data['line'][5]) ? $data['line'][5] : '');

    $record->org_identifiers = array_filter(explode(',', $record->organizations_raw));
    if (empty($record->org_identifiers)) {
      $record->org_identifiers = array('');
    }

    $record->organizations = array();
    foreach ($record->org_identifiers as $identifier) {
      $identifier = trim($identifier);
      if ($identifier) {
        $params = array(
          'stateCode' => $record->state_code,
          'organizationIdentifier' => $identifier,
        );
      }
      else {
        $params = array(
          'organizationPath' => PADS_ORGANIZATION_ROOT_NAME . '_' . $record->state_code,
        );
        $identifier = $record->state_code;
      }
      $org_ids = pads_org_find($params);
      $org_id = reset($org_ids);
      if ($org_id) {
        $record->organizations[$identifier] = $org_id;
      }

    }

    return $record;
  }

  /**
   * {@inheritdoc}
   */
  protected function validateRecords($records, &$validate) {
    $dupemails = array_key_exists('dupemails', $validate) ?
      $validate['dupemails'] : array();
    if (!empty($dupemails)) {
      foreach ($records as $record) {
        $email = $record->email;
        if (in_array($email, $dupemails)) {
          $record->errors['email_exists'] = t('%email already exists in the file.', array(
            '%email' => $email,
          ));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function validateRecord($record, &$validate) {
    $required_fields = array(
      'firstName',
      'lastName',
      'email',
      'roles',
    );
    foreach ($required_fields as $required_field) {
      if (!isset($record->$required_field) || trim($record->$required_field) == '') {
        $record->errors["{$required_field}_required"] = t('Column %field is required.', array(
          '%field' => $required_field,
        ));
      }
    }

    // Validate email.
    $email = $record->email;
    if (!isset($record->errors) || !array_key_exists('email_required', $record->errors)) {
      if (!valid_email_address($email)) {
        $record->errors['email_valid'] = t('%email is not a valid email address.', array(
          '%email' => $email,
        ));
      }
      // Check email does not exist.
      if (user_load_by_mail($email)) {
        $record->errors['email_exists'] = t('%email already exists in the system.', array(
          '%email' => $email,
        ));
      }
    }
    // Populate dupemails for validateRecords.
    if (array_key_exists('emails', $validate) && array_search($email, $validate['emails']) !== FALSE) {
      $validate['dupemails'][] = $email;
    }
    $validate['emails'][] = $email;

    // Valid TAP role?
    $user_roles = array();
    if (!isset($record->errors) || !array_key_exists('roles_required', $record->errors)) {
      $roles = explode(',', $record->roles);
      foreach ($roles as $role) {
        if (!array_key_exists($role, $this->roles)) {
          $record->errors['role_valid'][] = t('%role is not valid.', array(
            '%role' => $role,
          ));
        }
        else {
          $user_roles[$this->roles[$role]] = $role;
        }
      }
    }

    // Validate organization required, if not an org admin.
    if (!in_array('organization administrator', $user_roles)) {
      if (empty($record->organizations_raw)) {
        $args = array('%field' => 'organizations');
        $record->errors["organizations_required"] = t('Column %field is required.', $args);
      }
    }

    // Valid Organization?
    if (!empty($record->organizations)) {
      foreach ($record->organizations as $org_id) {
        if (!$org = pads_org_load($org_id)) {
          $record->errors['org_valid'][] = t('%org_id is not a valid Organization id.', array(
            '%org_id' => $org_id,
          ));
        }
        elseif (!pads_org_access($org)) {
          $record->errors['org_valid'][] = t('The Organization id %org_id is not within your associated Organizations.', array(
            '%org_id' => $org_id,
          ));
        }
      }
    }
    // No orgs.
    else {
      $record->errors['org_valid'][] = t('Organization with identifier %org_identifier in %state_code not found.', array(
        '%org_identifier' => implode(', ', $record->org_identifiers),
        '%state_code' => $record->state_code,
      ));
    }

    // Valid Organization based on role?
    if (!isset($record->errors['role_valid']) && !isset($record->errors['org_valid'])) {
      $message = pads_organization_validate_org_terms_level($record->organizations, $user_roles, FALSE);
      if ($message != '') {
        $record->errors['org_valid'][] = $message;
      }
    }
  }

  /**
   * Generate an entity from uploaded record data.
   *
   * This is specific to each type of upload, to be implemented by child
   * classes.
   *
   * @param object $record
   *   The record object to be generated into an entity.
   */
  protected function generateEntity($record) {
    // Create user.
    $name = "{$record->firstName}.{$record->lastName}";
    if (module_exists('email_registration')) {
      // Prepending email_registration_ to the user name will kick off the
      // user save hooks from the email_registration module and safely provide
      // a unique username based on email.
      $name = 'email_registration_' . user_password();
    }

    // Create the user entity.
    $data = array(
      'name' => $name,
      'mail' => $record->email,
      'init' => $record->email,
      'pass' => user_password(),
      'status' => TRUE,
    );
    $roles = explode(',', $record->roles);
    $rids = $user_roles = array();
    foreach ($roles as $role) {
      $rid = $this->roles[$role];
      $rids[$rid] = $rid;
    }
    $data['roles'] = $rids;

    // ADS-524 Gotta put the name values up here so LDAP provisioning happens.
    $data['field_full_name']['und'][0]['given'] = $record->firstName;
    $data['field_full_name']['und'][0]['family'] = $record->lastName;

    $account = entity_create('user', array());
    user_save($account, $data);

    $orgs = implode(',', $record->organizations);
    pads_org_assign_orgs_to_user($orgs, $account);

    _user_mail_notify('register_admin_created', $account);

    // Reload and resave to get all of our properties provisioned to LDAP.
    $account = user_load($account->uid);
    user_save($account);

  }

  /**
   * {@inheritdoc}
   */
  public function parseError($record) {
    // Abort if there are no errors.
    if (!isset($record->errors)) {
      return '';
    }
    $errors = $record->errors;
    $implode_me = array();
    foreach ($errors as $key => $value) {
      if (is_array($value)) {
        $implode_me = array_merge($implode_me, $value);
      }
      else {
        $implode_me[] = $value;
      }
    }
    $errors = implode(',', $implode_me);
    return $errors;
  }


  /**
   * Writes a CSV containing the upload's errored records.
   *
   * $return string
   *   A string indicating the URI of the exported error file.
   */
  public function writeErrors() {
    global $user;

    // Load or create a new file entity.
    if ($file = $this->errorFile()) {
      return $file->uri;
    }

    $file = new stdClass();
    $file->uid = $user->uid;
    $file->uri = $this->errorUri;
    $file->filename = drupal_basename($file->uri);

    // Write the CSV.
    $fh = fopen($this->errorUri, 'w');

    $header = array(
      'First name',
      'Last name',
      'Email',
      'Roles',
      'State Code',
      'Organization Identifiers',
      'Error',
    );
    fputcsv($fh, $header);

    $errors = $this->recordFiles('error');

    foreach ($errors as $line_no) {
      $record = $this->loadRecord('error', $line_no);
      $line = $record->line;
      $error = strip_tags($this->parseError($record));
      $line[] = $error;
      fputcsv($fh, $line);
    }
    fclose($fh);

    // Insert/update the db record.
    file_save($file);

    return $file->uri;
  }

}
