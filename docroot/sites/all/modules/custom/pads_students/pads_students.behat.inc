<?php
/**
 * @file
 * Contains \StudentContext.
 */

use Behat\Gherkin\Node\TableNode;
use Drupal\DrupalExtension\Context\DrupalSubContextBase;
use Drupal\DrupalExtension\Context\DrupalSubContextInterface;
use Drupal\pads\students\StudentFactory;

class StudentContext extends DrupalSubcontextBase implements DrupalSubContextInterface {

  /**
   * @Given Students:
   *
   * Provide student data in the following format:
   * | enrollOrgId | stateIdentifier | lastName | firstName | dateOfBirth | gender | gradeLevel |
   * | 1234        | IL-1230-01      | Skolnick | Louis     | 01-02-2003  | M      | 03         |
   * | 1234        | IL-1230-02      | Lowell   | Gilbert   | 04-05-2003  | M      | 03         |
   */
  public function students(TableNode $student_node) {

    // Tell Ldr we are the admin user.
    $client_user_id = 1;
    foreach ($student_node->getHash() as $student) {
      $student['clientUserId'] = $client_user_id;

      // Temporary stuff.
      $student['raceAA'] = 'N';
      $student['raceAN'] = 'N';
      $student['raceAS'] = 'N';
      $student['raceHL'] = 'N';
      $student['raceHP'] = 'N';
      $student['raceWH'] = 'N';
      $student['statusDIS'] = 'N';
      $student['statusECO'] = 'N';
      $student['statusELL'] = 'N';
      $student['statusGAT'] = 'N';
      $student['statusLEP'] = 'N';
      $student['statusMIG'] = 'N';
      $student['disabilityType'] = '';

      // We just make the call and do not check the response. If it fails,
      // we assume the students are already created.
      pads_students_add($student);
    }
  }

  /**
   * Calls fixStepArgument from PadsMinkContext.
   *
   * @param string $arg
   *   The arg to "fix".
   *
   * @return mixed
   *   Fixed arg.
   */
  protected function fixStepArgument($arg) {
    $padsmink_context = $this->getContext('PadsMinkContext');
    return $padsmink_context->fixStepArgument($arg);
  }

  /**
   * @Then student with stateIdentifier :stateIdentifier is assigned to class :cid
   */
  public function studentHasClass($state_identifier, $cid) {
    $cid = $this->fixStepArgument($cid);
    LdrContext::ldrClientUserLogIn();
    $students = StudentFactory::getInstance()->getStudentsByClass($cid);
    if ($students && !array_key_exists('error', $students)) {
      $student_found = FALSE;
      foreach ($students as $student) {
        if ($student['stateIdentifier'] == $state_identifier) {
          $student_found = TRUE;
          break;
        }
      }
      if (!$student_found) {
        throw new \Exception(sprintf('Expecting to find student %s in class %d, but did not.', $state_identifier, $cid));
      }
    }
    else {
      throw new \Exception(sprintf('No students found in class %d', $cid));
    }
  }

  /**
   * @Then student with stateIdentifier :stateIdentifier is not assigned to class :cid
   */
  public function StudentDoesNotHaveClass($state_identifier, $cid) {
    $return = FALSE;
    try {
      $this->studentHasClass($state_identifier, $cid);
    }
    catch (\Exception $e) {
      $return = TRUE;
    }
    if (!$return) {
      throw new \Exception(sprintf('Expecting to not find student %s in class %d, but I did.', $state_identifier, $cid));
    }
  }

  /**
   * @Then the newest student in org :org contains optional state data :values
   */
  public function newStudentOptionalData($org_id, $values) {
    $org_id = $this->fixStepArgument($org_id);
    $values = explode(',', $values);

    // Retrieve all students from Org id.
    $response = StudentFactory::getInstance()->getStudents($org_id);
    if (array_key_exists('studentRecords', $response) && !empty($response['studentRecords'])) {
      $student = reset($response['studentRecords']);
      $student_record_id = $student['studentRecordId'];

      // Call Ldrs GET student-record with optionalStateData parameter.
      $response = pads_ldr_client_call('getStudentRecord', array(
        'studentRecordId' => $student_record_id,
        'optionalStateData' => 'Y',
      ));

      // Verify optionalStateData values.
      if (!array_key_exists('error', $response)) {
        foreach ($values as $key => $value) {
          $value = trim($value);
          if (!empty($value)) {
            $index = $key + 1;
            $actual_value = $response["optionalStateData$index"];
            if ($actual_value != $value) {
              throw new \Exception(sprintf('Expecting %s, found %s', $value, $actual_value));
            }
          }

        }
      }
      else {
        throw new \Exception(sprintf('Could not retrieve student %d from org %d', $student_record_id, $org_id));
      }

    }
    else {
      throw new \Exception(sprintf('Expecting at least one student in org %d, found none', $org_id));
    }
  }
}
