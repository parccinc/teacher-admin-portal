<?php
/**
 * @file
 * Functionality relating to Student pages.
 */

use Drupal\pads\students\StudentFactory;

/**
 * Page callback for default students page.
 *
 * @return array
 *   Return value from pads_organization().
 */
function pads_my_students() {
  $user_orgs = pads_organization_get_user_orgs();
  // Pass the first org.
  $org = array_shift($user_orgs);
  drupal_goto("students/{$org['organizationId']}");
}

/**
 * Form callback for Student view page.
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form_state array.
 * @param array $student
 *   Student array from Ldr.
 *
 * @return string
 *   The rendered form.
 */
function pads_student_view($form, &$form_state, $student) {
  $form['demographics'] = array(
    '#type' => 'vertical_tabs',
  );

  $form['demographic_data'] = array(
    '#type' => 'fieldset',
    '#title' => t('Demographic Info'),
    '#collapsible' => FALSE,
    '#group' => 'demographics',
  );

  pads_student_demographics($form['demographics'], $student, 'view');
  return $form;
}

/**
 * Edit student page callback.
 *
 * @param array $student
 *   Student object from Ldr.
 *
 * @return array
 *   Drupal render array.
 */
function pads_student_edit($student) {
  drupal_set_title(t('Edit %firstName %lastName', array(
    '%firstName' => array_key_exists('firstName', $student) ? $student['firstName'] : '',
    '%lastName' => array_key_exists('lastName', $student) ? $student['lastName'] : '',
  )), PASS_THROUGH);
  return drupal_get_form('pads_student_edit_form', $student);
}

/**
 * Form callback for student edit form.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form_state array.
 * @param array $student
 *   The Student object from Ldr.
 *
 * @return array
 *   Drupal form render array.
 */
function pads_student_edit_form($form, &$form_state, $student) {

  $actions = array(
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save Student Data'),
    ),
  );

  $form['demographics'] = array(
    '#type' => 'vertical_tabs',
  );

  $form['demographic_data'] = array(
    '#type' => 'fieldset',
    '#title' => t('Demographic Info'),
    '#collapsible' => FALSE,
    '#group' => 'demographics',
  );

  pads_student_demographics($form['demographics'], $student, 'edit');

  $form['footer'] = $actions;
  return $form;
}

/**
 * Page callback to prepare and call add student form.
 *
 * @param array $org
 *   Ldr Org.
 *
 * @return array
 *   Form render array.
 */
function student_add_page($org) {
  $org_id = $org['organizationId'];
  $student = StudentFactory::getInstance()->scaffold($org_id);
  return drupal_get_form('student_add_form', $student);
}

/**
 * Shared form validation form student add and edit forms.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array
 * @param string $op
 *   Expected values are 'add' or 'edit'.
 */
function student_add_edit_form_validate($form, &$form_state, $op) {
  $student = $form_state['build_info']['args'][0];
  $values = $form_state['values'];

  // Make each ethnicity option its own property.
  foreach ($values['ethnicity'] as $key => $value) {
    $values[$key] = $value ? 'Y' : 'N';
  }
  unset($values['ethnicity']);

  // Remove unchanged values.
  if ($op == 'edit') {
    $values_keys = array_keys($values);
    foreach ($values_keys as $value_key) {
      if (
        array_key_exists($value_key, $student) &&
        $student[$value_key] == $values[$value_key]
      ) {
        unset($values[$value_key]);
      }
    }
  }

  if (array_key_exists('studentRecordId', $student)) {
    $values['studentRecordId'] = $student['studentRecordId'];
  }

  $org_id = array_key_exists('enrollOrgId', $values) ? $values['enrollOrgId'] : $student['enrollOrgId'];
  $org = pads_org_load($org_id);
  if (pads_org_access($org)) {
    // Make the call to Ldr to add student in validation so the Drupal form
    // knows if it fails.
    $operation = $op == 'edit' ? 'updateStudentRecord' : 'addStudentRecord';
    $response = pads_ldr_client_call($operation, $values);
    if (array_key_exists('error', $response)) {
      form_set_error('demographics', t('[%logid] %detail', array(
        '%logid' => $response['logid'],
        '%detail' => $response['detail'],
      )));
    }
    if ($op == 'add' && array_key_exists('studentRecordId', $response)) {
      $form_state['build_info']['args'][0] = pads_student_load($response['studentRecordId']);
    }
  }
}

/**
 * Form callback for add single student form.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form_state array.
 * @param array $student
 *   Ldr Student.
 *
 * @return array
 *   Drupal form render array.
 */
function student_add_form($form, &$form_state, $student) {

  $actions = array(
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save Student Data'),
    ),
  );

  $form['demographics'] = array(
    '#type' => 'vertical_tabs',
  );

  $form['demographic_data'] = array(
    '#type' => 'fieldset',
    '#title' => t('Demographic Info'),
    '#collapsible' => FALSE,
    '#group' => 'demographics',
  );

  $form['demographics']['enrollOrgId'] = array(
    '#type' => 'hidden',
    '#value' => $student['enrollOrgId'],
  );
  pads_student_demographics($form['demographics'], $student, 'add');

  $form['footer'] = $actions;
  return $form;
}

/**
 * Student add form validation.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form_state array.
 */
function student_add_form_validate($form, &$form_state) {
  student_add_edit_form_validate($form, $form_state, 'add');
}

/**
 * Student add form submit.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form_state array.
 */
function student_add_form_submit($form, &$form_state) {
  $student = $form_state['build_info']['args'][0];
  $org_id = $form_state['values']['enrollOrgId'];

  // Checks if new student has class assigned and user can assign test.
  $next_step = '';
  if (user_access('assign test to student')) {
    if (array_key_exists('cid', $form_state['values'])) {
      $class_id = $form_state['values']['cid'];
      // Needed to trigger the modal.
      ctools_include('modal');
      ctools_modal_add_js();

      $options['attributes']['class'][] = 'ctools-use-modal';
      $options['attributes']['class'][] = 'ctools-modal-zurb-modal-style';
      $student_record_id = $student['studentRecordId'];
      $link = l(t('here'), "students/nojs/$student_record_id/assigntest/$class_id", $options);
      $classortest = 'test';
    }
    else {
      $link = l(t('here'), "students/$org_id/{$student['studentRecordId']}");
      $classortest = 'class';
    }

    $next_step = t('Click !link to assign them to a %classortest.', array(
      '!link' => $link,
      '%classortest' => $classortest,
    ));
  }

  $message = t(
    'You have successfully added %name. !next_step',
    array(
      '%name' => pads_students_formatname($student),
      '!next_step' => $next_step,
    ));

  drupal_set_message($message);

  $form_state['redirect'] = "student/{$student['studentRecordId']}";
}

/**
 * Modal page callback to display assign test form for Student.
 *
 * @param bool $js
 *   TRUE if JavaScript is disabled, FALSE if the year is 1995.
 * @param array $student
 *   Ldr student array.
 * @param array $class
 *   Ldr class arary.
 *
 * @return array|mixed
 *   Drupal render array or JSON command array.
 */
function pads_students_assign_to_test($js, $student, $class) {
  if (!$js) {
    return drupal_get_form('pads_students_assign_to_test_form', $student, $class);
  }

  ctools_include('modal');
  ctools_include('ajax');

  $form_state = array(
    'title' => t('Assign to Test'),
    'ajax' => TRUE,
    'build_info' => array(
      'args' => array(
        $student,
        $class,
      ),
    ),
  );

  $output = ctools_modal_form_wrapper('pads_students_assign_to_test_form', $form_state);
  if (!empty($form_state['executed'])) {
    // We'll just overwrite the form output if it was successful.
    $commands = array();
    if ($form_state['redirect']) {
      $commands[] = ctools_ajax_command_redirect($form_state['redirect']);
    }
    else {
      $commands[] = ctools_modal_command_dismiss();
    }

    $output = $commands;

  }
  print ajax_render($output);
  drupal_exit();
}

/**
 * Page callback for assign test to student form.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 * @param array $student
 *   Ldr student object.
 * @param array $class
 *   Ldr class object.
 *
 * @return array|string
 *   Drupal render array.
 */
function pads_students_assign_to_test_form($form, &$form_state, $student, $class) {
  if (!$student) {
    drupal_set_message('Unable to determine student.', 'error');
    return array();
  }
  $form['header'] = array(
    '#type' => 'item',
    '#markup' => t('You are assigning a test to @studentName', array(
      '@studentName' => pads_students_formatname($student),
    )),
  );

  // Class Identifier field.
  $form['classIdentifier'] = array(
    '#type' => 'textfield',
    '#title' => t('Class Identifier'),
    '#default_value' => $class['classIdentifier'],
    '#disabled' => TRUE,
  );

  // Class Identifier field.
  $form['sectionNumber'] = array(
    '#type' => 'textfield',
    '#title' => t('Section Number'),
    '#default_value' => $class['sectionNumber'],
    '#disabled' => TRUE,
  );

  // Test accommodations form @see pads_tests.module for more information.
  test_assignment_accommodations_form($form, $form_state);

  $submit_callback = array('pads_students_assign_test');
  pads_tests_assignment_form($form, $form_state, $submit_callback, $class['gradeLevel']);
  return $form;
}

/**
 * Helper to retrieve value expected in a students classAssignment field.
 *
 * Converts a classId to either a sectionNumber if exists, otherwise
 * classIdentifier.
 *
 * @param array $class
 *   Ldr class array.
 *
 * @return string
 *   Class value expected in students classAssignment fields.
 */
function _pads_students_student_class_helper($class) {
  return array_key_exists('sectionNumber', $class) ? $class['sectionNumber'] : $class['classIdentifier'];
}

/**
 * Help function to retrieve student demographic data.
 *
 * @param array $form
 *   The form to populate.
 * @param array $student
 *   The student array.
 * @param string $op
 *   Currently supports values of 'view', 'edit', or 'add'.
 */
function pads_student_demographics(&$form, $student, $op) {
  $disabled = $op == 'view' ? TRUE : FALSE;

  $school_org = pads_org_load($student['enrollOrgId']);
  $district_org = pads_org_load($school_org['parentOrganizationId']);

  $form['stateIdentifier'] = array(
    '#type' => 'textfield',
    '#title' => t('State Student ID'),
    '#default_value' => $student['stateIdentifier'],
    '#disabled' => $disabled,
    '#required' => TRUE,
  );

  $form['localIdentifier'] = array(
    '#type' => 'textfield',
    '#title' => t('Local Student ID'),
    '#default_value' => $student['localIdentifier'],
    '#disabled' => $disabled,
  );

  if (in_array($op, array('view', 'add'))) {
    $form['district'] = array(
      '#type' => 'textfield',
      '#title' => t('District'),
      '#default_value' => $district_org['organizationName'],
      '#disabled' => TRUE,
    );

    $form['school'] = array(
      '#type' => 'textfield',
      '#title' => t('School'),
      '#default_value' => $school_org['organizationName'],
      '#disabled' => TRUE,
    );

    if ($op == 'add' && user_access('add student to class')) {
      // Check query string for class id.
      $query_parameters = drupal_get_query_parameters();
      if ($query_parameters && array_key_exists('cid', $query_parameters)) {
        $class = pads_classes_get($query_parameters['cid']);
        if (pads_classes_access($class)) {
          $form['classAssignment1'] = array(
            '#type' => 'hidden',
            '#value' => _pads_students_student_class_helper($class),
          );
          $form['cid'] = array(
            '#type' => 'hidden',
            '#value' => $class['classId'],
          );
        }
      }
      // No class id can be determined, so let's show a class dropdown if the
      // vendor is ISBE and the user has the 'view any class' permission (i.e.
      // an org administrator).
      elseif (pads_get_vendor() == 'isbe' && user_access('view any class')) {
        $class_options = array();
        $classes = pads_classes_get_by_org($student['enrollOrgId']);
        $classes = array_key_exists('classes', $classes) ? $classes['classes'] : array();
        foreach ($classes as $class) {
          $class_options[_pads_students_student_class_helper($class)] = pads_classes_format_name($class);
        }
        asort($class_options);
        if (!empty($class_options)) {
          $form['classAssignment1'] = array(
            '#type' => 'select',
            '#title' => t('Class'),
            '#options' => $class_options,
            '#required' => TRUE,
            '#attributes' => array(
              'required' => 'required',
            ),
          );
        }
      }
    }
  }

  $form['gradeLevel'] = array(
    '#type' => 'textfield',
    '#title' => t('Grade'),
    '#default_value' => $student['gradeLevel'],
    '#disabled' => $disabled,
    '#required' => TRUE,
  );

  $form['firstName'] = array(
    '#type' => 'textfield',
    '#title' => t('First Name'),
    '#default_value' => $student['firstName'],
    '#disabled' => $disabled,
    '#required' => TRUE,
  );

  $form['middleName'] = array(
    '#type' => 'textfield',
    '#title' => t('Middle Name'),
    '#default_value' => $student['middleName'],
    '#disabled' => $disabled,
  );

  $form['lastName'] = array(
    '#type' => 'textfield',
    '#title' => t('Last Name'),
    '#default_value' => $student['lastName'],
    '#disabled' => $disabled,
    '#required' => TRUE,
  );

  if (in_array($op, array('add', 'edit'))) {
    $form['gender'] = array(
      '#type' => 'radios',
      '#title' => t('Gender'),
      '#options' => array(
        'M' => 'Male',
        'F' => 'Female',
      ),
      '#default_value' => $student['gender'],
      '#required' => $op == 'add',
    );
  }
  else {
    $form['gender'] = array(
      '#type' => 'textfield',
      '#title' => t('Gender'),
      '#default_value' => $student['gender'],
      '#disabled' => $disabled,
      '#required' => TRUE,
    );
  }

  if (in_array($op, array('add', 'edit'))) {
    $form['dateOfBirth'] = array(
      '#type' => 'date_popup',
      '#title' => t('Date of Birth'),
      '#default_value' => $student['dateOfBirth'],
      '#date_type' => DATE_DATETIME,
      '#date_timezone' => date_default_timezone(),
      '#date_year_range' => '-20:+0',
      '#date_format' => 'Y-m-d',
      '#required' => TRUE,
    );
  }
  else {
    $form['dateOfBirth'] = array(
      '#type' => 'textfield',
      '#title' => t('Date of Birth'),
      '#default_value' => $student['dateOfBirth'],
      '#disabled' => $disabled,
    );
  }

  $ethnicity = array();
  $ethnicity_label = pads_student_ethnicity_labels();
  foreach (array_keys($ethnicity_label) as $key) {
    if (in_array($student[$key], array('y', 'Y'))) {
      $ethnicity[$key] = $ethnicity_label[$key];
    }
  }

  if (in_array($op, array('add', 'edit'))) {
    $form['ethnicity'] = array(
      '#title' => 'Ethnicity',
      '#type' => 'checkboxes',
      '#options' => $ethnicity_label,
      '#default_value' => array_keys($ethnicity),
    );
  }
  else {
    $form['ethnicity'] = array(
      '#title' => 'Ethnicity',
      '#type' => 'textarea',
      '#default_value' => implode(PHP_EOL, $ethnicity),
      '#disabled' => $disabled,
    );
  }
}

/**
 * Ethnicity labels.
 *
 * @return array
 *   Keys are the expected Ldr property, value is the label.
 */
function pads_student_ethnicity_labels() {
  return array(
    'raceAA' => 'Black or African American',
    'raceAN' => 'American Indian or Alaska Native',
    'raceAS' => 'Asian',
    'raceHL' => 'Hispanic or Latin',
    'raceHP' => 'Native Hawaiian or Other Pacific Islander',
    'raceWH' => 'White',
  );

}

/**
 * Validate method for pads_student_edit_form.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The form_state.
 */
function pads_student_edit_form_validate($form, &$form_state) {
  student_add_edit_form_validate($form, $form_state, 'edit');
}

/**
 * Submit function for pads_student_edit_form.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The form_state.
 */
function pads_student_edit_form_submit($form, &$form_state) {
  $student = $form_state['build_info']['args'][0];
  drupal_set_message(t('Student %stateIdentifier has been updated.', array(
    '%stateIdentifier' => $student['stateIdentifier'],
  )));
  $form_state['redirect'] = "student/{$student['studentRecordId']}";
}

/**
 * Form callback for the school ctools jump menu.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 * @param array $org
 *   The Organization array from Ldr.
 *
 * @return string
 *   Html output for ctools jump form.
 */
function pads_student_page_school_jump($form, &$form_state, $org) {

  // Find the district level org.
  $district_org = $org['organizationType'] == PADS_ORGANIZATION_LEVEL_DISTRICT ?
    $org : pads_org_load($org['parentOrganizationId']);

  // Get the schools of the district.
  $schools = pads_org_get_children($district_org['organizationId']);

  $jump_select = array();
  // Find schools user has access to.
  foreach ($schools as $school_org_id => $school) {
    if (pads_org_access($school)) {
      $jump_select[url("students/{$school_org_id}")] = $school['organizationName'];
    }
  }

  $options = array(
    'title' => t('Showing students for'),
  );
  if ($org['organizationType'] == PADS_ORGANIZATION_LEVEL_SCHOOL) {
    $options['default_value'] = url("students/{$org['organizationId']}");
  }

  $form = ctools_jump_menu($form, $form_state, $jump_select, $options);
  return $form;
}
