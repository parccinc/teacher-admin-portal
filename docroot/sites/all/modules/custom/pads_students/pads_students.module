<?php
/**
 * @file
 * Contains student functionality.
 */

use Drupal\pads\students\StudentFactory;

/**
 * Implements hook_menu().
 */
function pads_students_menu() {
  $items['students'] = array(
    'title' => 'My Students',
    'type' => MENU_CALLBACK,
    'page callback' => 'pads_my_students',
    'access callback' => 'pads_org_access',
    'access arguments' => array(
      FALSE,
      'view organizations',
      array(PADS_ORGANIZATION_LEVEL_SCHOOL),
    ),
    'file' => 'pads_students.pages.inc',
  );
  $items['student/%pads_student'] = array(
    'title' => 'Student Profile',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pads_student_view', 1),
    'access callback' => 'pads_student_access',
    'access arguments' => array(1),
    'file' => 'pads_students.pages.inc',
  );

  $items['student/%pads_student/view'] = array(
    'title' => 'View Student',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  $items['student/%pads_student/edit'] = array(
    'title' => 'Edit Student',
    'page callback' => 'pads_student_edit',
    'page arguments' => array(1),
    'access callback' => 'pads_student_access',
    'access arguments' => array(1, 'update'),
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    'file' => 'pads_students.pages.inc',
  );

  $items['student/add/%pads_org'] = array(
    'title' => 'Add Student',
    'page callback' => 'student_add_page',
    'page arguments' => array(2),
    'access callback' => 'student_add_access',
    'access arguments' => array(2, 'add students'),
    'type' => MENU_CALLBACK,
    'file' => 'pads_students.pages.inc',
  );

  // Test router items.
  $items['students/%ctools_js/%pads_student/assigntest/%class'] = array(
    'type' => MENU_CALLBACK,
    'title' => 'Assign to Test',
    'page callback' => 'pads_students_assign_to_test',
    'page arguments' => array(1, 2, 4),
    'access callback' => 'pads_student_assign_test_access',
    'access arguments' => array(2, 'assign test to student', 4),
    'file' => 'pads_students.pages.inc',
  );

  return $items;
}

/**
 * Add student access callback.
 *
 * @param array $org
 *   Ldr Org.
 * @param string $permission
 *   Access permission.
 *
 * @return bool
 *   TRUE if Org at school level, and user has permission.
 */
function student_add_access($org, $permission) {
  if (!pads_org_access($org) ||
    $org['organizationType'] != PADS_ORGANIZATION_LEVEL_SCHOOL
  ) {
    return FALSE;
  }

  return user_access($permission);
}

/**
 * Implements hook_views_api().
 */
function pads_students_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'pads_students') . '/views',
  );
}

/**
 * Implements hook_permission().
 */
function pads_students_permission() {
  return array(
    'view students' => array(
      'title' => t('View students in class'),
      'description' => t('Users with this permission will have access to view students within their assigned classes.'),
    ),
    'view any student' => array(
      'title' => t('View students in Org'),
      'description' => t('Users with this permission will have access to students within Organization if assigned a School level Org.'),
    ),
    'add students' => array(
      'title' => t('Add students'),
      'description' => t('Users with this permission will have access to add students within Organization.'),
    ),
    'add any student' => array(
      'title' => t('Add any student'),
      'description' => t('Users with this permission will have access to add students regardless of Organization.'),
    ),
    'edit students' => array(
      'title' => t('Edit students'),
      'description' => t('Users with this permission will have access to edit students within their assigned classes.'),
    ),
    'edit any student' => array(
      'title' => t('Edit any student'),
      'description' => t('Users with this permission will have access to edit students in Organization if assigned a school level Org.'),
    ),
    'administer students' => array(
      'title' => t('Administer students'),
      'descriptions' => t('Users with this permission will be able to access all student functions regardless of Org assignment.'),
      'restrict access' => TRUE,
    ),
    'administer students in org' => array(
      'title' => t('Administer students in Org'),
      'descriptions' => t('Users with this permission will be able to access all student functions within Org.'),
    ),
  );
}

/**
 * Implements hook_entity_info().
 */
function pads_students_entity_info() {
  return array(
    'pads_students' => array(
      'label' => t('Student record'),
      'module' => 'pads_students',
      'entity keys' => array(
        'id' => 'studentRecordId',
        'label' => 'Student record id',
      ),
      'base table' => NULL,
      'controller class' => 'PadsStudentsEntityController',
    ),
  );
}

/**
 * Returns a fully loaded Student object from Ldr.
 *
 * @param int $student_id
 *   The id of the student.
 *
 * @return array
 *   Student object from Ldr.
 */
function pads_student_load($student_id) {
  composer_manager_register_autoloader();
  return pads_ldr_client_call(
    'getStudentRecord',
    array(
      'studentRecordId' => $student_id,
    ));
}

/**
 * Access callback to student.
 *
 * @param array $student
 *   The student object.
 * @param string $op
 *   The operation to be performed on the student. Possible values are:
 *   - "view"
 *   - "update"
 *   - "delete"
 *   - "create"
 *
 * @return bool
 *   TRUE if access allowed, FALSE otherwise.
 */
function pads_student_access($student, $op = 'view') {
  if (!$student || array_key_exists('error', $student)) {
    return FALSE;
  }

  if (user_access('administer students')) {
    return TRUE;
  }

  // Check if the user has access to the requested operation.
  switch ($op) {
    case 'view':
      if (!user_access('view students') && !user_access('view any student')) {
        return FALSE;
      }
      break;

    case 'update':
      if (!user_access('edit students') && !user_access('edit any student')) {
        return FALSE;
      }
      break;

    case 'delete':
      return FALSE;

    case 'create':
      if (!user_access('add students') && !user_access('add any student')) {
        return FALSE;
      }
      break;

    default:
      return FALSE;

  }

  $org_id = array_key_exists('enrollOrgId', $student) ?
    $student['enrollOrgId'] : FALSE;
  $user_orgs = array_keys(pads_organization_get_user_orgs());
  if ($org_id && ($org = pads_org_load($org_id))) {
    // Direct org assignment.
    if (in_array($org_id, $user_orgs)) {
      return TRUE;
    }
    // Check for district user's schools.
    if (pads_organization_get_user_org_level() == PADS_ORGANIZATION_LEVEL_DISTRICT) {
      foreach ($user_orgs as $user_org_id) {
        $schools = array_keys(pads_org_get_children($user_org_id));
        if (in_array($org_id, $schools)) {
          return TRUE;
        }
      }
    }
  }

  return FALSE;
}

/**
 * Form submit for Student test assignment.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form_state array.
 */
function pads_students_assign_test($form, &$form_state) {
  $test_battery_id = $form_state['values']['battery'];
  $timing_value = $form_state['values']['instructionStatus'];
  $enable_text_to_speech = $form_state['values']['accommodations']['text_to_speech'];
  $enable_line_reader = $form_state['values']['accommodations']['line_reader'];
  // Convert checkbox accommodations into LDR variables.
  $enable_text_to_speech = !is_numeric($enable_text_to_speech) ? 'Y' : 'N';
  $enable_line_reader = !is_numeric($enable_line_reader) ? 'Y' : 'N';
  $student = $form_state['build_info']['args'][0];
  $class = $form_state['build_info']['args'][1];
  $battery = pads_tests_get_battery($test_battery_id);
  if (!is_numeric($test_battery_id)) {
    drupal_set_message('Unable to determine test battery for student assignment', 'error');
  }
  elseif (!$student) {
    drupal_set_message('Unable to determine student for test assignment', 'error');
  }
  else {
    // Create test assignments with student id.
    $response = pads_tests_add_test_assignment($student['studentRecordId'], $test_battery_id, $timing_value, $enable_line_reader, $enable_text_to_speech);
    if (array_key_exists('error', $response)) {
      $message = array_key_exists('detail', $response) ?
        $response['detail'] : t('An error occurred during test assignment');
      if ($response['error'] == 2001 || $response['error'] == 2082) {
        $message = $battery['testBatteryName'] . " has already been assigned to " . pads_students_formatname($student) . ".";
      }
      drupal_set_message($message, 'error');
    }
    else {
      drupal_set_message(t('@test has been assigned to @studentName', array(
        '@test' => $battery && array_key_exists('testBatteryName', $battery) ?
          $battery['testBatteryName'] : $test_battery_id,
        '@studentName' => pads_students_formatname($student),
      )));
    }
  }
  $form_state['redirect'] = "manage/{$class['organizationId']}/tests/assignments/{$class['classId']}";
}

/**
 * Checks access to student, permission, and class.
 *
 * @param array $student
 *   Ldr student object.
 * @param string $permission
 *   Permission to check.
 * @param array $class
 *   Ldr class array.
 *
 * @return bool
 *   TRUE if access, FALSE otherwise.
 */
function pads_student_assign_test_access($student, $permission, $class) {
  return pads_student_access($student) && user_access($permission)
  && pads_classes_access($class);
}

/**
 * Format Student name.
 *
 * @param array $student
 *   Ldr Student array.
 *
 * @return string
 *   Name of student.
 */
function pads_students_formatname($student) {
  return "{$student['firstName']} {$student['lastName']}";
}

/**
 * Returns the number of students for the given Organization.
 *
 * @param int $org_id
 *   The organization id.
 *
 * @return int
 *   Number of students within Organization.
 */
function pads_students_get_count($org_id) {
  $response = pads_students_get($org_id);
  return $response && array_key_exists('totalCount', $response) ?
    $response['totalCount'] : 0;
}

/**
 * Calls Ldr student-records/{org_id}.
 *
 * @see https://breaktech.centraldesktop.com/p/aQAAAAACM1Dl
 *
 * @param int $org_id
 *   The org id passed into the Ldr request.
 *
 * @return array
 *   Ldr response.
 */
function pads_students_get($org_id) {
  composer_manager_register_autoloader();
  return StudentFactory::getInstance()->getStudents($org_id);
}

/**
 * Calls Ldr class-students/{class_id}.
 *
 * @see https://breaktech.centraldesktop.com/p/aQAAAAACN2nj
 *
 * @param int $class_id
 *   The Class id passed into the Ldr request.
 *
 * @return array
 *   Ldr response.
 */
function pads_students_get_by_class($class_id) {
  composer_manager_register_autoloader();
  return StudentFactory::getInstance()->getStudentsByClass($class_id);
}

/**
 * Calls Ldr post /student-record.
 *
 * @see https://breaktech.centraldesktop.com/p/aQAAAAACMi7B
 *
 * @param array $student
 *   The student object. See link in function header description for more
 *   details on student object.
 *
 * @return array
 *   Ldr response.
 */
function pads_students_add($student) {
  composer_manager_register_autoloader();
  return StudentFactory::getInstance()->addStudent($student);
}

/**
 * Delete student in Ldr.
 *
 * @param int $student_id
 *   Student id to delete.
 *
 * @return array
 *   Ldr response.
 */
function pads_students_delete($student_id) {
  composer_manager_register_autoloader();
  return StudentFactory::getInstance()->deleteStudent($student_id);
}

/**
 * Delete student in Ldr.
 *
 * @param int $student_id
 *   Student id to delete.
 *
 * @return array
 *   Ldr response.
 */
function pads_students_get_classes($student_id) {
  composer_manager_register_autoloader();
  return StudentFactory::getInstance()->getStudentClasses($student_id);
}

/**
 * A DataTable of Ldr student with select checkboxes.
 *
 * @param array $form
 *   The form to add the students datatable to.
 * @param array $form_state
 *   The form_state array.
 * @param array $org
 *   Ldr Organization array.
 *
 * @return array
 *   Adds the key ['students']['table'] to $form, with the value of the
 *   renderable array form item.
 */
function pads_students_tableselect_form($form, &$form_state, $org) {
  // Get the students from Ldr.
  $response = pads_students_get($org['organizationId']);
  $students = array_key_exists('studentRecords', $response) ?
    $response['studentRecords'] : array();

  $header = array(
    'stateIdentifier' => t('State identifier'),
    'firstName' => t('First name'),
    'middleName' => t('Middle name'),
    'lastName' => t('Last name'),
    'gradeLevel' => t('Grade'),
  );

  $rows = array();

  foreach ($students as $student) {
    $rows[] = pads_students_build_row($student, $header);
  }

  $form['students'] = array(
    '#type' => 'container',
    '#attributes' => array(),
  );

  $attributes = array();
  if ($rows) {
    $form['students']['table'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $rows,
      '#attributes' => $attributes,
      '#weight' => 5,
    );
  }
  else {
    $empty_table = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => $attributes,
      '#empty' => t('No students assigned to !organizationName.', array(
        '!organizationName' => $org['organizationName'],
      )),
    );
    $form['students']['table'] = array(
      '#type' => 'item',
      '#markup' => drupal_render($empty_table),
    );
  };

  return $form;
}

/**
 * Helper function to build a row in the students table.
 *
 * @param array $student
 *   The fully loaded Student object.
 * @param array $header
 *   Table header array expected by theme_table variables argument.
 *
 * @return array
 *   A row array as expected by theme_table variables argument.
 */
function pads_students_build_row($student, $header) {
  $row = array();
  $header_keys = array_keys($header);
  foreach ($header_keys as $column) {
    switch ($column) {

      case 'stateIdentifier':
        $state_identifier = $student[$column];
        $row[$column] = l($state_identifier, "student/{$student['studentRecordId']}");
        break;

      default:
        $row[$column] = array_key_exists($column, $student) ? $student[$column] : '';

    }
  }
  return $row;
}

/**
 * Todo: Figure out a place for this.
 */
class PadsStudentsEntityController extends EntityAPIController {

  /**
   * {@inheritdoc}
   */
  public function load($ids = array(), $conditions = array()) {
    $entities = array();
    foreach ($ids as $id) {
      $entities[$id] = pads_student_load($id);
    }
    return $entities;
  }

}
