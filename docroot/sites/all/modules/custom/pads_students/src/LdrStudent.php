<?php
/**
 * @file
 * Install, update, and uninstall functions for the Foo Bar module.
 * Contains \Fully\Qualified\Namespace\And\NameOfTheClass.
 */

namespace Drupal\pads\students;

class LdrStudent implements StudentInterface {

  /**
   * {@inheritdoc}
   */
  public function getStudents($org_id) {
    $response = pads_ldr_client_call('getStudentRecords', array(
      'organizationId' => $org_id,
    ));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function getStudentsByClass($class_id) {
    $response = pads_ldr_client_call('getStudentRecordsByClass', array(
      'classId' => $class_id,
    ));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function addStudent($student) {
    return pads_ldr_client_call('addStudentRecord', $student);
  }

  /**
   * Delete student in Ldr.
   *
   * @param int $student_id
   *   Student id to delete.
   *
   * @return array
   *   Ldr response.
   */
  public function deleteStudent($student_id) {
    return pads_ldr_client_call('deleteStudentRecord', array(
      'studentRecordId' => $student_id,
    ));
  }

  /**
   * Get the classes for a student in Ldr.
   *
   * @param int $student_id
   *   Student id to query.
   *
   * @return array
   *   Ldr response.
   */
  public function getStudentClasses($student_id) {
    return pads_ldr_client_call('getStudentClasses', array(
      'studentRecordId' => $student_id,
    ));
  }

  /**
   * Post Student object to Ldr.
   *
   * @see https://breaktech.centraldesktop.com/p/aQAAAAACNaYu
   *
   * @param array $students
   *   Array of students.
   *
   * @return array
   *   Ldr response.
   */
  public function addStudents($students) {
    return pads_ldr_client_call('addStudentRecords', $students);
  }

  /**
   * {@inheritdoc}
   */
  public function scaffold($org_id) {
    $student = array(
      'enrollOrgId' => $org_id,
      'stateIdentifier' => NULL,
      'localIdentifier' => NULL,
      'gradeLevel' => NULL,
      'firstName' => NULL,
      'middleName' => NULL,
      'lastName' => NULL,
      'gender' => NULL,
      'dateOfBirth' => NULL,
      'ethnicity' => NULL,
    );
    foreach (pads_student_ethnicity_labels() as $key => $ethnicity_label) {
      $student[$key] = 'N';
    }
    return $student;
  }

}
