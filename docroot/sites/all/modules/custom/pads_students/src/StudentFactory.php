<?php
/**
 * @file
 * Install, update, and uninstall functions for the Foo Bar module.
 * Contains \Fully\Qualified\Namespace\And\NameOfTheClass.
 */

namespace Drupal\pads\students;


class StudentFactory {

  /**
   * Retrieve an implementation instance of StudentInterface.
   *
   * @return LdrStudent
   *   An instance of LdrStudent.
   */
  public static function getInstance() {
    $instance = &drupal_static(__CLASS__ . '.' . __FUNCTION__);
    if (!$instance) {
      $instance = new LdrStudent();
    }
    return $instance;
  }
}