<?php
/**
 * @file
 * Install, update, and uninstall functions for the Foo Bar module.
 * Contains \Fully\Qualified\Namespace\And\NameOfTheClass.
 */

namespace Drupal\pads\students;

interface StudentInterface {

  /**
   * Retrieve Ldr Students from an Organization.
   *
   * @param int $org_id
   *   The organization id.
   *
   * @return array
   *   An array of Students that are in the Organization.
   */
  public function getStudents($org_id);

  /**
   * Retrieve Ldr Students assigned to Class.
   *
   * @see https://breaktech.centraldesktop.com/p/aQAAAAACN2nj
   *
   * @param int $class_id
   *   Ldr Class id.
   *
   * @return array
   *   An array of Students that are assigned to Class.
   */
  public function getStudentsByClass($class_id);

  /**
   * Post Student object to Ldr.
   *
   * @see https://breaktech.centraldesktop.com/p/aQAAAAACMi7B
   *
   * @param array $student
   *   The student object. See link in function header description for more
   *   details on student object.
   *
   * @return array
   *   Ldr response.
   */
  public function addStudent($student);

  /**
   * Post Student object to Ldr.
   *
   * @see https://breaktech.centraldesktop.com/p/aQAAAAACNaYu
   *
   * @param array $students
   *   Array of students.
   *
   * @return array
   *   Ldr response.
   */
  public function addStudents($students);

  /**
   * Delete student in Ldr.
   *
   * @param int $student_id
   *   Student id to delete.
   *
   * @return array
   *   Ldr response.
   */
  public function deleteStudent($student_id);

  /**
   * Get the classes for a student in Ldr.
   *
   * @param int $student_id
   *   Student id to query.
   *
   * @return array
   *   Ldr response.
   */
  public function getStudentClasses($student_id);

  /**
   * Scaffold a new student array with fields expected by new/edit form.
   *
   * @param int $org_id
   *   New students enroll or testing Org id.
   *
   * @return array
   *   Student keys with NULL values except for enrollOrgId.
   */
  public function scaffold($org_id);
}
