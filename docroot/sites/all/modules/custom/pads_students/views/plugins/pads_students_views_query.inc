<?php
/**
 * @file
 * Contains \pads_students_views_query.
 */

class pads_students_views_query extends views_plugin_query_default {

  protected $orgIds;
  protected $classIds;

  /**
   * {@inheritdoc}
   */
  public function init($base_table, $base_field, $options) {
    $this->orgIds = array();
    $this->classIds = array();
    parent::init($base_table, $base_field, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function query($get_count = FALSE) {
  }

  /**
   * {@inheritdoc}
   */
  public function placeholder($base = 'views') {
  }

  /**
   * {@inheritdoc}
   */
  public function add_where_expression($group, $snippet, $args = array()) {
    // Defined context filters are either Organization ids or Class ids.
    // If the snippet is the Class id "where" clause, set $classIds. Otherwise
    // we are querying by Organization id.
    if ($snippet == 'pads_students.classId =  ') {
      $arg = reset($args);
      if (is_array($arg)) {
        $this->classIds = $arg;
      }
      else {
        $class = pads_classes_get($arg);
        if ($class && !array_key_exists('error', $class)) {
          $this->classIds[$class['classId']] = $class;
        }
      }
    }
    // Check for multiple class ids.
    elseif ($snippet == "pads_students.classId IN() ") {
      $class_ids = reset($args);
      foreach ($class_ids as $class_id) {
        $this->classIds[$class_id] = pads_classes_get($class_id);
      }
    }
    else {
      $this->orgIds = $args;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function add_field($table, $field, $alias = '', $params = array()) {
    // Make sure an alias is assigned.
    $alias = $alias ? $alias : $field;

    // PostgreSQL truncates aliases to 63 characters. See
    // http://drupal.org/node/571548.

    // We limit the length of the original alias up to 60 characters to get a
    // unique alias later if its have duplicates.
    $alias = substr($alias, 0, 60);

    // Create a field info array.
    $field_info = array(
        'field' => $field,
        'table' => $table,
        'alias' => $alias,
      ) + $params;

    // Test to see if the field is actually the same or not. Due to differing
    // parameters changing the aggregation function, we need to do some
    // automatic alias collision detection.
    $base = $alias;
    $counter = 0;
    while (!empty($this->fields[$alias]) && $this->fields[$alias] != $field_info) {
      $field_info['alias'] = $alias = $base . '_' . ++$counter;
    }

    if (empty($this->fields[$alias])) {
      $this->fields[$alias] = $field_info;
    }

    // Keep track of all aliases used.
    $this->field_aliases[$table][$field] = $alias;

    return $alias;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(&$view) {
    // Get students by Org id.
    foreach ($this->orgIds as $org_id) {
      if (is_numeric($org_id)) {
        $students = pads_students_get($org_id);
        $student_records = $students['studentRecords'];
        foreach ($student_records as $student) {
          $view->result[] = (object) $student;
        }
      }
    }

    // Get students by class.
    if (!empty($this->classIds)) {
      $results_by_class = array();
      foreach ($this->classIds as $class_id => $class) {
        $students = pads_students_get_by_class($class_id);
        foreach ($students as $student) {
          $result = $student;
          $result['classId'] = $class_id;
          $result['classIdentifier'] = $class['classIdentifier'];
          $result['sectionNumber'] = $class['sectionNumber'];

          $key = !empty($result['sectionNumber']) ? $result['classIdentifier'] . ' - ' . $result['sectionNumber'] : $class['classIdentifier'];
          $results_by_class[$key][] = (object) $result;
        }
      }

      // Sort by class name here, because we don't have a views sort handler
      // for an service response at this time.
      $class_identifiers = array_keys($results_by_class);
      asort($class_identifiers, SORT_NATURAL | SORT_STRING);

      foreach ($class_identifiers as $class_identifier) {
        $results = $results_by_class[$class_identifier];
        foreach ($results as $result) {
          $view->result[] = $result;
        }
      }
    }
  }

  /**
   * Getter function for $this->classIds.
   *
   * @return array
   *   Array of classIds.
   */
  public function getClassIds() {
    return $this->classIds;
  }
}
