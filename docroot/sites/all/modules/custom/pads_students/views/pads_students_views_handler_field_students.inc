<?php
/**
 * @file
 * Contains \pads_students_views_handler_field_students.
 */

class pads_students_views_handler_field_students extends views_handler_field {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->field_alias = $this->real_field;
  }
}
