<?php
/**
 * @file
 * Contains \pads_students_views_bulk_operations_handler_field_operations.
 */

class pads_students_views_bulk_operations_handler_field_operations extends views_bulk_operations_handler_field_operations {

  /**
   * {@inheritdoc}
   */
  public function get_entity_type() {
    if (!$entity_type = parent::get_entity_type()) {
      return 'pads_students';
    }
    return $entity_type;
  }

}