<?php
/**
 * @file
 * Views functionality for pads_students
 */

/**
 * Implements hook_views_plugins().
 */
function pads_students_views_plugins() {
  return array(
    'query' => array(
      'pads_students_views_query' => array(
        'title' => t('SQL Query'),
        'help' => t('Query will be generated and run using the Drupal database API.'),
        'handler' => 'pads_students_views_query',
      ),
    ),
  );
}

/**
 * Implements hook_views_data().
 */
function pads_students_views_data() {
  $data = array();
  $data['pads_students']['table']['group'] = t('Pads Students');
  $data['pads_students']['table']['base'] = array(
    'title' => t('Pads Students'),
    'help' => t('Query Pads Students.'),
    'field' => 'studentRecordId',
    'query class' => 'pads_students_views_query',
  );

  $data['pads_students']['studentRecordId'] = array(
    'title' => t('Student id'),
    'help' => t('Ldr assigned student id'),
    'field' => array(
      'handler' => 'pads_students_views_handler_field_students',
      'click sortable' => TRUE,
    ),
  );
  $data['pads_students']['enrollOrgId'] = array(
    'title' => t('Organization id'),
    'help' => t('Students enrollment Organization id'),
    'field' => array(
      'handler' => 'pads_students_views_handler_field_students',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );
  $data['pads_students']['firstName'] = array(
    'title' => t('First name'),
    'help' => t('Students first name'),
    'field' => array(
      'handler' => 'pads_students_views_handler_field_students',
      'click sortable' => TRUE,
    ),
  );
  $data['pads_students']['middleName'] = array(
    'title' => t('Middle name'),
    'help' => t('Students middle name'),
    'field' => array(
      'handler' => 'pads_students_views_handler_field_students',
      'click sortable' => TRUE,
    ),

  );
  $data['pads_students']['lastName'] = array(
    'title' => t('Last name'),
    'help' => t('Students last name'),
    'field' => array(
      'handler' => 'pads_students_views_handler_field_students',
      'click sortable' => TRUE,
    ),

  );
  $data['pads_students']['stateIdentifier'] = array(
    'title' => t('State identifier'),
    'help' => t('Student state-issued identifier'),
    'field' => array(
      'handler' => 'pads_students_views_handler_field_students',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['pads_students']['gradeLevel'] = array(
    'title' => t('Grade'),
    'help' => t('Students grade level'),
    'field' => array(
      'handler' => 'pads_students_views_handler_field_students',
      'click sortable' => TRUE,
    ),
  );
  $data['pads_students']['classId'] = array(
    'title' => t('Class id'),
    'help' => t('Class id student is assigned to'),
    'field' => array(
      'handler' => 'pads_students_views_handler_field_students',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );
  $data['pads_students']['classIdentifier'] = array(
    'title' => t('Class name'),
    'help' => t('Retrieves class identifier from Ldr'),
    'field' => array(
      'handler' => 'pads_students_views_handler_field_students',
    ),
  );
  $data['pads_students']['sectionNumber'] = array(
    'title' => t('Class section number'),
    'help' => t('Retrieves class section number from Ldr'),
    'field' => array(
      'handler' => 'pads_students_views_handler_field_students',
    ),
  );

  if (module_exists('views_bulk_operations')) {
    $data['pads_students']['views_bulk_operations'] = array(
      'title' => t('Pads Students'),
      'group' => t('Bulk operations'),
      'help' => t('Provide a checkbox to select the row for bulk operations.'),
      'real field' => 'studentRecordId',
      'field' => array(
        'handler' => 'pads_students_views_bulk_operations_handler_field_operations',
        'click sortable' => FALSE,
      ),
    );
  }

  return $data;
}
