<?php

/**
 * @file
 * PADS Secure File Rules integration.
 */

/**
 * Implements hook_rules_event_info().
 */
function pads_secure_file_rules_event_info() {
  $items = array(
    'pads_secure_file_share' => array(
      'label' => t('After sharing a secure file'),
      'group' => t('Secure Files'),
      'variables' => array(
        'node' => array(
          'type' => 'node',
          'label' => t('shared file'),
        ),
        'message' => array(
          'type' => 'message',
          'label' => t('message to dispatch'),
        ),
        'user' => array(
          'type' => 'user',
          'label' => t('shared user'),
        ),
      ),
    ),
  );

  return $items;
}
