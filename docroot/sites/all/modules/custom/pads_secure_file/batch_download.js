/**
 * @file Automatically trigger download of the VBO zip of files
 */
(function ($) {
  Drupal.behaviors.padsBatchDownloadAutostart = {
    attach: function (context) {
      $('a.vbo-download-link', context).once(
        function () {
          var target = $(this).attr('href');
          iframe = '<iframe height="1px" width="1px" src="' + target + '"></iframe>';
          $('body').append(iframe);
        }
      );
    }
  }
})(jQuery);
