<?php
/**
 * @file
 * Contains ctools modal delete functionality.
 */

/**
 * Page callback for secure file in ctools modal.
 *
 * @param bool $js
 *   FALSE if user is using Netscape 2.0.
 * @param stdclass $node
 *   The node object.
 *
 * @return array
 *   Array of Drupal ajax commands.
 *
 * @throws Exception
 */
function pads_secure_file_view_ajax($js, $node) {
  ctools_include('modal');
  ctools_include('ajax');

  // If node is empty, it no longer exists (likely deleted from cron).
  if (empty($node)) {
    drupal_set_message('You attempted to view an item that does not exist.', 'warning');
    $commands = array();
    $commands[] = ctools_ajax_command_reload();
    print ajax_render($commands);
    drupal_exit();
  }

  $output = node_page_view($node);

  // If JS is not set, return the output.
  if (!$js) {
    return $output;
  }

  if (is_array($output)) {
    $output = drupal_render($output);
  }

  $title = drupal_get_title();

  // If there are messages for the form, render them.
  if ($messages = theme('status_messages')) {
    $output = '<div class="messages">' . $messages . '</div>' . $output;
  }

  $commands = array();
  $commands[] = ctools_modal_command_display($title, $output);

  if (empty($commands)) {
    $commands[] = ctools_modal_command_loading();
    if (!empty($_GET['destination'])) {
      $commands[] = ctools_ajax_command_redirect($_GET['destination']);
    }
  }

  print ajax_render($commands);
  drupal_exit();
}

/**
 * Page callback to deliver node edit form via CTools AJAX/modal.
 *
 * @param boolean|null $js
 *   TRUE if Javascript is enabled, NULL otherwise.
 * @param object $node
 *   The node to edit.
 *
 * @return array
 *   Array of Drupal Ajax Commands.
 */
function pads_secure_file_edit_ajax($js, $node) {
  // Fall back if $js is not set.
  if (!$js) {
    module_load_include('inc', 'node', 'node.pages');
    return node_page_edit($node);
  }

  ctools_include('modal');
  ctools_include('ajax');

  $output = array();
  // If node is empty, it no longer exists (likely deleted from cron).
  if (empty($node)) {
    drupal_set_message('You attempted to edit a file that does not exist.', 'warning');
    $output[] = ctools_ajax_command_reload();
  }
  else {
    $type_name = node_type_get_name($node);
    $title = t('<em>Edit @type</em> @title', array(
      '@type' => $type_name,
      '@title' => $node->title,
    ));
    $form_state = array(
      'title' => $title,
      'ajax' => TRUE,
      'build_info' => array(
        'args' => array($node),
      ),
    );
    ctools_form_include_file($form_state, drupal_get_path('module', 'node') . '/node.pages.inc');
    $form_state['build_info']['args'] = array($node);

    // Todo: combine the below code with modal_node_add in pads_ctools_plugin.
    $output = ctools_modal_form_wrapper($node->type . '_node_form', $form_state);

    if (!empty($form_state['executed'])) {
      $output = array();
      // If node submit, reload page.
      $triggering_element_id = array_key_exists('#id', $form_state['triggering_element']) ?
        $form_state['triggering_element']['#id'] : '';
      if (strpos($triggering_element_id, 'edit-submit') === 0) {
        $output[] = ctools_ajax_command_reload();
      }
      else {
        $output = array();
      }
    }
  }

  print ajax_render($output);
  drupal_exit();
}

/**
 * Page callback to deliver node delete form via CTools AJAX/modal.
 *
 * @param bool $js
 *   Is incoming menu item js capable or not?
 * @param stdclass $node
 *   The node object.
 *
 * @return array
 *   Array of Drupal Ajax Commands.
 */
function pads_secure_file_delete_ajax($js, $node) {

  // Fall back if $js is not set.
  if (!$js) {
    return drupal_get_form('pads_secure_file_delete_form', $node);
  }

  ctools_include('modal');
  ctools_include('ajax');

  $output = array();
  // If node is empty, it no longer exists (likely deleted from cron).
  if (empty($node)) {
    drupal_set_message('You attempted to delete a file that does not exist.', 'warning');
    $output[] = ctools_ajax_command_reload();
  }
  else {
    $form_state = array(
      'title' => t('Delete Secure File'),
      'ajax' => TRUE,
    );
    $form_state['build_info']['args'] = array($node);

    $output = ctools_modal_form_wrapper('pads_secure_file_delete_form', $form_state);

    if (!empty($form_state['executed'])) {
      // On successful submission, reload the page.
      $output = array();
      $output[] = ctools_ajax_command_reload();
    }
  }

  print ajax_render($output);
  exit;
}

/**
 * AJAX callback for an autocomplete to select users by email address.
 *
 * @param string $str
 *   The current search criteria.
 *
 * @return array
 *   Associated array of search results.
 */
function pads_secure_file_user_ajax($str = '') {

  $query = "SELECT u.uid, mail FROM {users} AS u
    LEFT JOIN {users_roles} AS ur ON u.uid = ur.uid
    WHERE ur.rid is NULL AND u.status = ?
    AND u.mail LIKE ? LIMIT 10";

  $output = array();
  foreach (db_query($query, array(1, "%$str%")) as $row) {
    $output[$row->mail] = $row->mail;
  }

  return $output;
}
