//@ sourceURL=secure_file.js
(function ($) {

  Drupal.behaviors.secureFile = {

    attach: function (context, settings) {

      var stateChanged = function (up) {
        $(document).find('#secure-file-node-form').find(':submit').each(function(i) {
          if (up.state == plupload.STARTED) {
            $(this).attr('disabled', true);
          }
          else {
            $(this).attr('disabled', false);
          }
        });
      };

      var filesAdded = function (up, files) {
        var start_upload_button = $(document).find('#secure-file-node-form').find('.ajax-processed:submit');
        if (start_upload_button) {
          setTimeout(function(){start_upload_button.click();}, 100);
        }
        else {
          setTimeout(function(){up.start()}, 100);
        }

      };

      $('.form-item.form-type-plupload', context).each(function () {
        if ($(this).find('.plupload-element').length > 0) {
          $('.plupload-element').once('plupload-element', function (index) {
            var uploader = $(this).pluploadQueue();
            if (uploader) {
              uploader.bind('StateChanged', stateChanged, this);
              uploader.bind('FilesAdded', filesAdded, this);
            }
          });
        }
      });

      // PARADS-1420 Hides Start Upload button in favor of auto submit.
      $('.filefield-source-plupload .form-submit').hide();
    }
  }
})(jQuery);