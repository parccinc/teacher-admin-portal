/**
 * @file
 * Provides automatic upload initiation on managed file elements.
 */

(function ($) {

  /**
   * Attach behaviors to managed file element upload fields.
   */
  Drupal.behaviors.padsFileValidateAutoAttach = {
    attach: function (context) {
      $('.pads-auto-upload', context).once(function () {
        $this = $(this);
        $button = $this.next().hide();
        var element_id = this.id;
        var seen_id = element_id.match(/--[0-9]$/);
        if (seen_id) {
          // Bind the Drupal.file change event to this element
          seen_id = seen_id.shift();
          var field_file_id = '#' + element_id.substring((element_id.length - seen_id.length), -(seen_id.length));
          $(this).bind('change', {extensions: Drupal.settings.file.elements[field_file_id]}, Drupal.file.validateExtension);

          // Add class to messages so validateExtension can replace it.
          if ($('form.node-form.node-secure_file-form .messages').length > 0) {
            $('form.node-form.node-secure_file-form .messages').addClass('file-upload-js-error');
          }
        }
        $this.bind('change', {button: $button}, Drupal.behaviors.padsFileValidateAutoAttach.autoUpload);
      });
    },
    detach: function (context) {
      $('.pads-auto-upload', context).each(function () {
        $(this).unbind('change', Drupal.behaviors.padsFileValidateAutoAttach.autoUpload);
      });
    },
    autoUpload: function (event) {
      // Event will be cancelled if it fails Drupal.file.validateExtension.
      if (this.value && !event.isDefaultPrevented()) {
        // Remove any previous messages, press upload.
        $('form.node-form.node-secure_file-form .messages').remove();
        event.data.button.mousedown();
      }
    }
  };


})(jQuery);
