<?php
/**
 * @file
 * Contains \PadsSecureFileSubContext.
 */

use Behat\Gherkin\Node\TableNode as TableNode;
use Drupal\DrupalExtension\Context\DrupalSubContextBase;
use Drupal\DrupalExtension\Context\DrupalSubContextInterface;

class PadsSecureFileSubContext extends DrupalSubcontextBase implements DrupalSubContextInterface {

  /**
   * Creates Secure File content provided in the below form.
   *
   * | title    | author           | field_file            |
   * | pubatch1 | behat_parccuser  | behat1.csv,behat2.csv |
   * | pabatch1 | behat_parccadmin | behat.csv             |
   * | bubatch1 | behat_batchuser  | behat.csv             |
   * | ...      | ...              | ...                   |
   *
   * @Given secure file content:
   */
  public function createSecureFileNodes(TableNode $nodes_table) {
    $feature_context = $this->getContext('\FeatureContext');
    $drupal_context = $this->getContext('\Drupal\DrupalExtension\Context\DrupalContext');

    $type = 'secure_file';
    foreach ($nodes_table->getHash() as $node_hash) {
      $files = array();
      // Create the Drupal file class.
      $filenames = explode(',', $node_hash['field_file']);
      foreach ($filenames as $filename) {
        if ($file = $feature_context->getDrupalFileFromPath($filename)) {
          $file = (array) $file;
          // Convert to array and add columns expected by file field.
          $file['display'] = 1;
          $files[] = $file;
        } else {
          throw new Exception(sprintf('Error generating file from "%s".', $filename));
        }
      }

      if (!empty($node_hash['delete']) && $deleted = strtotime($node_hash['delete'])) {
        $node_hash['field_delete_time'] = $deleted;
      }

      // Create node.
      $node = (object) $node_hash;
      $node->type = $type;
      $saved = $drupal_context->nodeCreate($node);

      // Add file to Node.
      $w = entity_metadata_wrapper('node', $saved);
      $w->field_file = $files;
      $w->save();
    }
  }

  /**
   * @When /^I check the box for the row containing "([^"]*)"$/
   */
  public function iCheckTheBoxForTheRowContaining($text) {
    $rows = $this->getSession()
      ->getPage()
      ->findAll('css', 'tbody tr');
    if (!$rows) {
      throw new \Exception(sprintf('No rows found on the page %s', $this->getSession()
        ->getCurrentUrl()));
    }
    foreach ($rows as $row) {
      $html = $row->getHtml();
      if (strpos($row->getText(), $text) !== FALSE) {
        $element = $row->find('css', "input[type='checkbox']");
      }
    }
    if (isset($element)) {
      $element->check();
    }
    else {
      throw new \Exception(sprintf('No checkbox found in the row containing %s', $text));
    }
  }

  /**
   * @Then /^the link "([^"]*)" should contain "([^"]*)" in the path$/
   */
  public function theLinkShouldContainInThePath($link_text, $path_fragment) {
    $links = $this->getSession()
      ->getPage()
      ->findAll('css', 'a');
    foreach ($links as $link) {
      if (strpos($link->getText(), $link_text) !== FALSE) {
        $link_path = $link->getAttribute('href');
        if (strpos($link_path, $path_fragment) !== FALSE) {
          return;
        }
      }
    }
    throw new \Exception(sprintf('No link found containing %s', $path_fragment));
  }

  /**
   * Verifies text is in Message entity subject or body.
   *
   * @param string $bundle
   *   The Message entity bundle.
   * @param string $mail
   *   The email address of the 'to' user.
   * @param string $text
   *   The text to find in subject or body of Message.
   *
   * @return bool
   *   TRUE if passes, throws exception otherwise.
   *
   * @throws \Exception
   *
   * @Then the :arg1 message to :arg2 should contain the text :arg3
   */
  public function theMessageToShouldContainTheText($bundle, $mail, $text) {
    // Get the "to" user;
    $to_user = user_load_by_mail($mail);
    if (!$to_user) {
      throw new \Exception(sprintf('Did not find expected message to %s', $mail));
    }

    // Get Message entity by uid property.
    $efq = new EntityFieldQuery();
    $efq->entityCondition('entity_type', 'message')
      ->entityCondition('bundle', $bundle)
      ->propertyCondition('uid', $to_user->uid);
    $result = $efq->execute();

    // Iterate through message results.
    if (!empty($result) && array_key_exists('message', $result)) {
      $mids = array_keys($result['message']);
      $messages = entity_load('message', $mids);
      foreach ($messages as $message) {
        $mw = entity_metadata_wrapper('message', $message);

        // If $text is in body return TRUE.
        $fields_to_check = array(
          $mw->field_subject->value(),
          $mw->field_message_body->value(),
        );
        foreach ($fields_to_check as $field_to_check) {
          if ($field_to_check && strpos($field_to_check, $text) !== FALSE) {
            return TRUE;
          }
        }

      }
    }
    throw new \Exception('Did not find expected content in message body.');

  }

  /**
   * @Given secure file :title has no delete value set
   */
  public function assertSecureFileNoDeleteValue($title) {
    if (!$node = FeatureContext::findContent('secure_file', $title)) {
      throw new Exception(sprintf('Secure file "%s" does not exist.', $title));
    }

    $node->field_delete_time = array();
    node_save($node);
  }

}
