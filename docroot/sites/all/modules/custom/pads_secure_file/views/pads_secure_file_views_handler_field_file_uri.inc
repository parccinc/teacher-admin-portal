<?php
/**
 * @file
 * Contains class views_handler_field_secure_file_link_download.
 */

class pads_secure_file_views_handler_field_file_uri extends views_handler_field {

  public function option_definition() {
    $options = parent::option_definition();
    $options['text'] = array('default' => 'download', 'translatable' => TRUE);
    return $options;
  }

  public function options_form(&$form, &$form_state) {
    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text to display'),
      '#default_value' => $this->options['text'],
    );
    parent::options_form($form, $form_state);

    // The path is set by render_link function so don't allow to set it.
    $form['alter']['path'] = array('#access' => FALSE);
    $form['alter']['external'] = array('#access' => FALSE);
  }


  public function render_link($data, $values) {
    if ($data !== NULL && $data !== '') {
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = file_create_url($data);
      $text = $this->options['text'];
      return $text;
    }

    return $data;
  }

  public function render($values) {
    $value = $this->get_value($values);
    return $this->render_link($this->sanitize_value($value), $values);
  }

}
