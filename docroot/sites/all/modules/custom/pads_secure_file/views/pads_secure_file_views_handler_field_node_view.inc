<?php
/**
 * @file
 * Definition of pads_secure_file_views_handler_field_node_edit.
 */

class pads_secure_file_views_handler_field_node_view extends views_handler_field_entity {
  function render($values) {
    $node = $this->get_value($values);

    if (node_access('view', $node)) {
      // Needed to trigger the modal.
      ctools_include('modal');
      ctools_modal_add_js();

      $options = array();
      $options['query'] = drupal_get_destination();
      $options['attributes']['class'][] = 'ctools-use-modal tip-top ctools-modal-zurb-modal-style';
      $options['attributes']['id'][] = 'view_file_' . $node->nid;
      $options['attributes']['title'] = 'View File';
      $options['attributes']['data-tooltip'] = '';
      $options['attributes']['aria-haspopup'] = 'true';
      $options['attributes']['data-options'] = "append_to:html";
      $options['html'] = TRUE;
      return l('<i class="fi-eye"></i>', "pads/secure-file/nojs/{$node->nid}", $options);
    }
  }
}
