<?php
/**
 * @file
 * Class pads_secure_file_handler_area_empty_notification
 *
 * This handler sets a message for use when no results are found after
 * submitting and exposed form.
 */

class pads_secure_file_handler_area_empty_notification extends views_handler_area {

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {

    return !empty($this->view->exposed_raw_input) && !empty($_POST) ?
      t('No results were found.') : t('There are no files.');

  }

}
