<?php
/**
 * @file
 * Contains pads_secure_file_views_handler_field_node_share class.
 */

class pads_secure_file_views_handler_field_node_share extends views_handler_field_entity {

  public function render($values) {
    $node = $this->get_value($values);
    if (content_access_node_page_access($node)) {
      $options = array();
      $options['query'] = drupal_get_destination();
      $options['attributes']['class'][] = 'tip-top';
      $options['attributes']['title'] = 'Share File';
      $options['attributes']['id'][] = 'share_file_' . $node->nid;
      $options['attributes']['data-tooltip'] = '';
      $options['attributes']['aria-haspopup'] = 'true';
      $options['attributes']['data-options'] = "append_to:html";
      $options['html'] = TRUE;
      return l('<i class="fi-share"></i>', "pads/secure-file/{$node->nid}/share", $options);
    }
  }

}
