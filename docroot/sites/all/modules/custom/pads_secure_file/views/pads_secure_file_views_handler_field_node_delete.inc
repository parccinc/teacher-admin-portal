<?php
/**
 * @file
 * Contains pads_secure_file_views_handler_field_node_delete class.
 */

class pads_secure_file_views_handler_field_node_delete extends views_handler_field_entity {

  public function render($values) {
    $node = $this->get_value($values);

    if (node_access('delete', $node)) {
      // Needed to trigger the modal.
      ctools_include('modal');
      ctools_modal_add_js();

      $options = array();
      $options['query'] = drupal_get_destination();
      $options['attributes']['class'][] = 'ctools-use-modal tip-top ctools-modal-zurb-modal-style';
      $options['attributes']['id'][] = 'delete_file_' . $node->nid;
      $options['attributes']['title'] = 'Delete File';
      $options['attributes']['data-tooltip'] = '';
      $options['attributes']['aria-haspopup'] = 'true';
      $options['attributes']['data-options'] = "append_to:html";
      $options['html'] = TRUE;
      return l('<i class="fi-trash"></i>', "pads/secure-file/nojs/{$node->nid}/delete", $options);
    }
  }

}
