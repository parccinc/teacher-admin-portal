<?php
/**
 * @file
 * Contains pads_secure_file_views_handler_field_node_share class.
 */

class pads_secure_file_views_handler_field_node_download extends views_handler_field_entity {

  function render($values) {
    $node = $this->get_value($values);
    $output = '';
    if (node_access('view', $node)) {
      $w = PadsNodeWrapperFactory::create($node, 'secure_file');
      $files = $w->getFile();
      $file_count = count($files);
      if ($file_count == 1) {
        $file = reset($files);
        $options['attributes']['class'][] = 'tip-top';
        $options['attributes']['id'][] = 'download_file_' . $node->nid;
        $options['attributes']['title'] = 'Download File';
        $options['attributes']['data-tooltip'] = '';
        $options['attributes']['aria-haspopup'] = 'true';
        $options['attributes']['data-options'] = "append_to:html";
        $options['html'] = TRUE;
        $output = l('<i class="fi-download"></i>', file_create_url($file['uri']), $options);
      }
      elseif ($file_count > 1) {
        $options['attributes']['class'][] = 'tip-top';
        $options['attributes']['id'][] = 'download_files_{$node->nid}';
        $options['attributes']['title'] = "Download ($file_count) Files";
        $options['attributes']['data-tooltip'] = '';
        $options['attributes']['aria-haspopup'] = 'true';
        $options['attributes']['data-options'] = "append_to:html";
        $options['html'] = TRUE;
        $output = l('<i class="fi-download"></i>', file_create_url($w->generateZip(FALSE)), $options);
      }
    }
    return $output;
  }

}
