<?php
/**
 * @file
 * Install, update, and uninstall functions for the Foo Bar module.
 * Contains \Fully\Qualified\Namespace\And\NameOfTheClass.
 */

/**
 * Implements hook_views_data_alter().
 */
function pads_secure_file_views_data() {
  $data['views']['pads_secure_files_empty'] = array(
    'title' => t('Secure files empty'),
    'help' => t('Empty result notification for secure files listing.'),
    'area' => array(
      'handler' => 'pads_secure_file_handler_area_empty_notification',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function pads_secure_file_views_data_alter(&$data) {
  $data['node']['pads_secure_file_view'] = array(
    'real field' => 'nid',
    'field' => array(
      'title' => t('View node'),
      'help' => t('Provide link to view node in ctools modal.'),
      'handler' => 'pads_secure_file_views_handler_field_node_view',
    ),
  );

  $data['node']['pads_secure_file_edit'] = array(
    'real field' => 'nid',
    'field' => array(
      'title' => t('Edit link'),
      'help' => t('Provide link to edit file in ctools modal.'),
      'handler' => 'pads_secure_file_views_handler_field_node_edit',
    ),
  );

  $data['node']['pads_secure_file_delete'] = array(
    'real field' => 'nid',
    'field' => array(
      'title' => t('Delete link'),
      'help' => t('Provide link to delete file'),
      'handler' => 'pads_secure_file_views_handler_field_node_delete',
    ),
  );

  $data['node']['pads_secure_file_share'] = array(
    'real field' => 'nid',
    'field' => array(
      'title' => t('Share link'),
      'help' => t('Provide link to node access settings'),
      'handler' => 'pads_secure_file_views_handler_field_node_share',
    ),
  );

  $data['node']['pads_secure_file_download'] = array(
    'real field' => 'nid',
    'field' => array(
      'title' => t('Download link'),
      'help' => t('Provide link to download file(s).'),
      'handler' => 'pads_secure_file_views_handler_field_node_download',
    ),
  );

  // Custom pseudo-field to make the description available for use in Views'
  // combine filter.
  $data['field_data_field_file']['pads_desc'] = array(
    'real field' => 'field_file_description',
    'group' => t('Content'),
    'field' => array(
      'title' => t('Description'),
      'help' => t('Description of the file'),
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'title' => t('Description'),
      'help' => t('Description of the file'),
      'handler' => 'views_handler_sort',
    ),
  );

}
