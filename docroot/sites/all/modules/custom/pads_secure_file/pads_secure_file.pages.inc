<?php
/**
 * @file
 * Page callback and helper methods.
 */

/**
 * Page callback for share.
 *
 * Wraps the content access share in a way that we can do an "item no longer
 * exists" page rather than "not found".
 *
 * @param object $node
 *   The node to share.
 */
function pads_secure_file_share($node) {
  // If node is empty, it no longer exists (likely deleted from cron).
  if (empty($node)) {
    drupal_set_message('The batch you want to share no longer exists.', 'warning');
    drupal_goto();
  }
  else {
    $nid = $node->nid;
    // Drupal_goto() checks for destination so unset here and add to the new
    // path.
    unset($_GET['destination']);
    $options = array(
      'query' => array('destination' => 'files'),
    );

    // Send them to the access form.
    drupal_goto("node/$nid/access", $options);
  }
}

/**
 * Page callback for checking the existence of secure files.
 *
 * This is necessary because we want to provide custom 404 messages for secure
 * file downloads.
 */
function pads_secure_file_download_page() {
  $args = func_get_args();
  $scheme = array_shift($args);
  $args = array_merge(array('secure-files'), $args);
  $target = implode('/', $args);
  $uri = $scheme . '://' . $target;

  // Mimic Drupal's 404 functionality, except set our own messaging.
  if (!file_exists($uri)) {
    drupal_add_http_header('Status', '404 Not Found');
    watchdog('secure file not found', check_plain($_GET['q']), NULL, WATCHDOG_WARNING);
    drupal_set_title(t('File no longer exists'));
    return t('The file "@path" no longer exists.', array('@path' => request_uri()));
  }

  // If user is anonymous we assume session expired.
  if (user_is_anonymous()) {
    drupal_set_message(t('Your session has expired. Please log in.'), 'warning');
    drupal_goto();
  }

  // All good as far as we're concerned, pass on to core file_download function.
  $args = array_merge(array($scheme), $args);
  call_user_func_array('file_download', $args);
}
