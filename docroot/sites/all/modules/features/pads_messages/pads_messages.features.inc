<?php
/**
 * @file
 * pads_messages.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pads_messages_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_message_type().
 */
function pads_messages_default_message_type() {
  $items = array();
  $items['pads_secure_file_shared'] = entity_import('message_type', '{
    "name" : "pads_secure_file_shared",
    "description" : "Secure File Shared",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : {
      "token options" : { "clear" : 0 },
      "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" }
    },
    "language" : "",
    "arguments" : null,
    "message_text" : { "und" : [
        {
          "value" : "A file has been shared with you at [site:url].",
          "format" : "plain_text",
          "safe_value" : "\\u003Cp\\u003EA file has been shared with you at [site:url].\\u003C\\/p\\u003E\\n"
        },
        {
          "value" : "[message:user:mail],\\r\\n\\r\\nThe batch \\u003Ca href=\\u0022[message:field-secure-file-ref:url:absolute]\\u0022\\u003E[message:field-secure-file-ref:title]\\u003C\\/a\\u003E has been shared with you by [current-user:mail].\\r\\nIt will appear in your batch list the next time you log in.\\r\\n\\r\\n- [site:name] team",
          "format" : "full_html",
          "safe_value" : "\\u003Cp\\u003E[message:user:mail],\\u003C\\/p\\u003E\\n\\u003Cp\\u003EThe batch \\u003Ca href=\\u0022[message:field-secure-file-ref:url:absolute]\\u0022\\u003E[message:field-secure-file-ref:title]\\u003C\\/a\\u003E has been shared with you by [current-user:mail].\\u003Cbr \\/\\u003E\\nIt will appear in your batch list the next time you log in.\\u003C\\/p\\u003E\\n\\u003Cp\\u003E- [site:name] team\\u003C\\/p\\u003E\\n"
        }
      ]
    },
    "rdf_mapping" : []
  }');
  return $items;
}
