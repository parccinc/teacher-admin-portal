<?php
/**
 * @file
 * pads_roles_permissions.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function pads_roles_permissions_user_default_roles() {
  $roles = array();

  // Exported role: PARCC user.
  $roles['PARCC user'] = array(
    'name' => 'PARCC user',
    'weight' => 4,
  );

  // Exported role: TAP user.
  $roles['TAP user'] = array(
    'name' => 'TAP user',
    'weight' => 6,
  );

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 2,
  );

  // Exported role: ads administrator.
  $roles['ads administrator'] = array(
    'name' => 'ads administrator',
    'weight' => 3,
  );

  // Exported role: ads content editor.
  $roles['ads content editor'] = array(
    'name' => 'ads content editor',
    'weight' => 16,
  );

  // Exported role: ads user administrator.
  $roles['ads user administrator'] = array(
    'name' => 'ads user administrator',
    'weight' => 17,
  );

  // Exported role: batch user.
  $roles['batch user'] = array(
    'name' => 'batch user',
    'weight' => 5,
  );

  // Exported role: dmrs administrator.
  $roles['dmrs administrator'] = array(
    'name' => 'dmrs administrator',
    'weight' => 14,
  );

  // Exported role: dmrs user.
  $roles['dmrs user'] = array(
    'name' => 'dmrs user',
    'weight' => 15,
  );

  // Exported role: lea district test coordinator.
  $roles['lea district test coordinator'] = array(
    'name' => 'lea district test coordinator',
    'weight' => 9,
  );

  // Exported role: non-school institution test coordinator.
  $roles['non-school institution test coordinator'] = array(
    'name' => 'non-school institution test coordinator',
    'weight' => 10,
  );

  // Exported role: organization administrator.
  $roles['organization administrator'] = array(
    'name' => 'organization administrator',
    'weight' => 12,
  );

  // Exported role: school institution test coordinator.
  $roles['school institution test coordinator'] = array(
    'name' => 'school institution test coordinator',
    'weight' => 11,
  );

  // Exported role: state.
  $roles['state'] = array(
    'name' => 'state',
    'weight' => 8,
  );

  // Exported role: test administrator.
  $roles['test administrator'] = array(
    'name' => 'test administrator',
    'weight' => 7,
  );

  // Exported role: test form qc.
  $roles['test form qc'] = array(
    'name' => 'test form qc',
    'weight' => 13,
  );

  return $roles;
}
