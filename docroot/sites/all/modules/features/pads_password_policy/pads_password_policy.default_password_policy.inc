<?php
/**
 * @file
 * pads_password_policy.default_password_policy.inc
 */

/**
 * Implements hook_default_password_policy().
 */
function pads_password_policy_default_password_policy() {
  $export = array();

  $password_policy = new stdClass();
  $password_policy->disabled = FALSE; /* Edit this to true to make a default password_policy disabled initially */
  $password_policy->api_version = 1;
  $password_policy->name = 'PADS password policy';
  $password_policy->config = 'a:14:{s:10:"alpha_case";a:1:{s:10:"alpha_case";i:0;}s:11:"alpha_count";a:1:{s:11:"alpha_count";s:0:"";}s:9:"blacklist";a:1:{s:9:"blacklist";s:0:"";}s:10:"char_count";a:1:{s:10:"char_count";s:1:"5";}s:11:"consecutive";a:1:{s:22:"consecutive_char_count";s:0:"";}s:5:"delay";a:1:{s:5:"delay";s:0:"";}s:15:"drupal_strength";a:1:{s:15:"drupal_strength";s:0:"";}s:9:"int_count";a:1:{s:9:"int_count";s:0:"";}s:14:"past_passwords";a:1:{s:14:"past_passwords";s:1:"3";}s:12:"symbol_count";a:2:{s:12:"symbol_count";s:0:"";s:20:"symbol_count_symbols";s:30:"!@#$%^&*()_+=-|}{"?:><,./;\'\\[]";}s:8:"username";a:1:{s:8:"username";i:1;}s:7:"authmap";a:1:{s:11:"authmodules";a:0:{}}s:4:"role";a:1:{s:5:"roles";a:5:{i:2;s:1:"2";i:3;s:1:"3";i:5;s:1:"5";i:1;i:0;i:4;i:0;}}s:6:"expire";a:5:{s:12:"expire_limit";s:7:"90 days";s:22:"expire_warning_message";s:47:"Your Password has expired. Please change it now";s:25:"expire_warning_email_sent";s:8:"-14 days";s:28:"expire_warning_email_message";s:148:"Hello [user:mail],
Your password on [site:name] is currently set to expire at [password_expiration_date:short]. You can reset it at [user:edit-url]";s:28:"expire_warning_email_subject";s:71:"Password on [site:name] expires in [password_expiration_date:interval] ";}}';
  $export['PADS password policy'] = $password_policy;

  return $export;
}
