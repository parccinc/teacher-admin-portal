<?php
/**
 * @file
 * pads_homebox.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pads_homebox_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "panels_mini" && $api == "panels_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_homebox().
 */
function pads_homebox_homebox() {
  $homeboxes = array();

  $homeboxes['dashboard'] = array(
    'regions' => 1,
    'cache' => 0,
    'color' => 0,
    'colors' => array(
      0 => '#E4F0F8',
      1 => '#E4F0F8',
      2 => '#E4F0F8',
      3 => '#E4F0F8',
      4 => '#E4F0F8',
      5 => '#E4F0F8',
    ),
    'blocks' => array(
      'panels_mini_dashboard_cards' => array(
        'module' => 'panels_mini',
        'delta' => 'dashboard_cards',
        'region' => 1,
        'movable' => 1,
        'status' => 1,
        'open' => 1,
        'closable' => 0,
        'title' => '',
        'weight' => -14,
      ),
      'views_pads_dashboard_help_block-block' => array(
        'module' => 'views',
        'delta' => 'pads_dashboard_help_block-block',
        'region' => 1,
        'movable' => 1,
        'status' => 1,
        'open' => 1,
        'closable' => 0,
        'title' => 'Learn more about PARCC Diagnostics',
        'weight' => -14,
      ),
    ),
    'widths' => array(),
    'title' => 'Dashboard',
    'path' => 'dashboard',
    'menu' => 1,
    'enabled' => 1,
    'auto_save' => 1,
    'full' => 0,
    'roles' => array(
      0 => 'authenticated user',
    ),
  );


  return $homeboxes;
}
