<?php
/**
 * @file
 * pads_homebox.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function pads_homebox_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'dashboard_cards';
  $mini->category = 'TAP';
  $mini->admin_title = 'Dashboard Cards';
  $mini->admin_description = 'This is a min-panel of the Dashboard Cards so I can test to see if Homebox will work for what we want on the dashboard. ';
  $mini->requiredcontexts = array();
  $mini->contexts = array(
    0 => array(
      'identifier' => 'User',
      'keyword' => 'user',
      'name' => 'user',
      'type' => 'current',
      'uid' => '',
      'id' => 1,
    ),
    1 => array(
      'identifier' => 'Pads Organization',
      'keyword' => 'pads_org',
      'name' => 'pads_org',
      'source' => 'user_context_select',
      'id' => 1,
    ),
  );
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'foundation_1col';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Dashboard';
  $display->uuid = 'a8e1484d-d1c1-4ee0-bb80-7a29f51aef05';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-b66b9f56-a40f-4c37-bbe7-d7134947e820';
    $pane->panel = 'middle';
    $pane->type = 'dashboard_boxes';
    $pane->subtype = 'dashboard_boxes';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'row_class' => 'row',
      'row_attributes' => 'data-equalizer',
      'ul_wrapper_class' => '',
      'ul_class' => 'pricing-table',
      'ul_attributes' => 'data-equalizer-watch',
      'li_title_class' => 'title',
      'li_number_class' => 'price',
      'li_action_class' => 'cta-button tiny',
      'context' => 'context_user_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b66b9f56-a40f-4c37-bbe7-d7134947e820';
    $display->content['new-b66b9f56-a40f-4c37-bbe7-d7134947e820'] = $pane;
    $display->panels['middle'][0] = 'new-b66b9f56-a40f-4c37-bbe7-d7134947e820';
    $pane = new stdClass();
    $pane->pid = 'new-ba1368ab-b9d8-4f15-baab-ced995b8d3b1';
    $pane->panel = 'middle';
    $pane->type = 'isbe_button';
    $pane->subtype = 'isbe_button';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'pads_user_org_level',
          'settings' => array(
            'levels' => '2',
          ),
          'context' => 'requiredcontext_user_1',
          'not' => TRUE,
        ),
        1 => array(
          'name' => 'perm',
          'settings' => array(
            'perm' => 'edit any student',
          ),
          'context' => 'requiredcontext_user_1',
          'not' => FALSE,
        ),
        2 => array(
          'name' => 'vendor_access',
          'settings' => array(
            'vendor_path' => 'sites/vendor/isbe',
          ),
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'description' => 'To manage students, classes, and tests please press the button below',
      'label' => 'ISBE Management',
      'classes' => 'button tiny round',
      'context' => array(
        0 => 'requiredcontext_user_1',
        1 => 'requiredcontext_pads_org_1',
      ),
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'ba1368ab-b9d8-4f15-baab-ced995b8d3b1';
    $display->content['new-ba1368ab-b9d8-4f15-baab-ced995b8d3b1'] = $pane;
    $display->panels['middle'][1] = 'new-ba1368ab-b9d8-4f15-baab-ced995b8d3b1';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-b66b9f56-a40f-4c37-bbe7-d7134947e820';
  $mini->display = $display;
  $export['dashboard_cards'] = $mini;

  return $export;
}
