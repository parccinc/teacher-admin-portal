<?php
/**
 * @file
 * pads_content_types.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pads_content_types_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function pads_content_types_node_info() {
  $items = array(
    'help' => array(
      'name' => t('Help'),
      'base' => 'node_content',
      'description' => t('This will contain all help page content.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'pads_standards' => array(
      'name' => t('Pads Standards'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'secure_file' => array(
      'name' => t('Secure file'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Batch name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
