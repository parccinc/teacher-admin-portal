<?php
/**
 * @file
 * pads_rules.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function pads_rules_default_rules_configuration() {
  $items = array();
  $items['rules_anonymous_user_redirect'] = entity_import('rules_config', '{ "rules_anonymous_user_redirect" : {
      "LABEL" : "Anonymous user redirect",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "TAP" ],
      "REQUIRES" : [ "rules" ],
      "ON" : { "init" : [] },
      "IF" : [
        { "NOT user_has_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "2" : "2" } }
          }
        },
        { "NOT text_matches" : { "text" : [ "site:current-page:path" ], "match" : "batchuser\\/register" } },
        { "NOT text_matches" : { "text" : [ "site:current-page:path" ], "match" : "user\\/register" } },
        { "NOT text_matches" : {
            "text" : [ "site:current-page:path" ],
            "match" : "user",
            "operation" : "starts"
          }
        },
        { "NOT text_matches" : {
            "text" : [ "site:current-page:path" ],
            "match" : "node",
            "operation" : "starts"
          }
        },
        { "NOT text_matches" : {
            "text" : [ "site:current-page:path" ],
            "match" : "onelogin_saml",
            "operation" : "starts"
          }
        },
        { "NOT text_matches" : { "text" : [ "site:current-page:path" ], "match" : "reset-requested" } }
      ],
      "DO" : [
        { "redirect" : {
            "url" : "user\\/login?nextDestination=[site:current-page:path]",
            "force" : "0"
          }
        }
      ]
    }
  }');
  $items['rules_pads_secure_file_share'] = entity_import('rules_config', '{ "rules_pads_secure_file_share" : {
      "LABEL" : "Secure File Share",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "message_notify", "pads_secure_file" ],
      "ON" : { "pads_secure_file_share" : [] },
      "DO" : [
        { "message_notify_process" : {
            "message" : [ "message" ],
            "rendered_subject_field" : "field_subject",
            "rendered_body_field" : "field_message_body"
          }
        }
      ]
    }
  }');
  $items['rules_user_register_redirect_for_batchuser'] = entity_import('rules_config', '{ "rules_user_register_redirect_for_batchuser" : {
      "LABEL" : "User register redirect for batchuser",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "TAP" ],
      "REQUIRES" : [ "rules" ],
      "ON" : { "init" : [] },
      "IF" : [
        { "NOT user_has_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "2" : "2" } }
          }
        },
        { "text_matches" : { "text" : [ "site:current-page:path" ], "match" : "user\\/register" } },
        { "NOT text_matches" : {
            "text" : [ "site:current-page:path" ],
            "match" : "batchuser",
            "operation" : "starts"
          }
        }
      ],
      "DO" : [ { "redirect" : { "url" : "batchuser\\/register" } } ]
    }
  }');
  return $items;
}
