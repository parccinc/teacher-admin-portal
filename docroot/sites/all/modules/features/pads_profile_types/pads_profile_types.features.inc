<?php
/**
 * @file
 * pads_profile_types.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pads_profile_types_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "profile2_regpath" && $api == "profile2_regpath") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_profile2_type().
 */
function pads_profile_types_default_profile2_type() {
  $items = array();
  $items['dmrs_profile'] = entity_import('profile2_type', '{
    "userCategory" : true,
    "userView" : true,
    "type" : "dmrs_profile",
    "label" : "DMRS Profile",
    "weight" : "0",
    "data" : { "registration" : 0 },
    "rdf_mapping" : []
  }');
  $items['pads_batchuser_organization'] = entity_import('profile2_type', '{
    "userCategory" : true,
    "userView" : true,
    "type" : "pads_batchuser_organization",
    "label" : "Batchuser Organization",
    "weight" : "0",
    "data" : { "registration" : 0 },
    "rdf_mapping" : []
  }');
  $items['pads_tap_organization'] = entity_import('profile2_type', '{
    "userCategory" : true,
    "userView" : true,
    "type" : "pads_tap_organization",
    "label" : "PADS TAP Organization",
    "weight" : "0",
    "data" : { "registration" : 0 },
    "rdf_mapping" : []
  }');
  return $items;
}
