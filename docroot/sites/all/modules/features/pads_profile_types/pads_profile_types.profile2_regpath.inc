<?php
/**
 * @file
 * pads_profile_types.profile2_regpath.inc
 */

/**
 * Implements hook_default_profile2_regpath().
 */
function pads_profile_types_default_profile2_regpath() {
  $export = array();

  $regpath = new stdClass();
  $regpath->disabled = FALSE; /* Edit this to true to make a default regpath disabled initially */
  $regpath->api_version = 1;
  $regpath->profile_type = 'pads_batchuser_organization';
  $regpath->path = 'batchuser';
  $regpath->roles = 'a:11:{i:7;s:1:"7";i:3;i:0;i:4;i:0;i:5;i:0;i:6;i:0;i:13;i:0;i:12;i:0;i:8;i:0;i:9;i:0;i:11;i:0;i:10;i:0;}';
  $regpath->status = 1;
  $regpath->weight = 0;
  $regpath->misc = 'a:9:{s:9:"path_type";s:8:"separate";s:8:"tab_text";s:18:"Create new account";s:13:"fieldset_wrap";i:0;s:13:"custom_titles";i:1;s:11:"login_title";s:6:"Log in";s:14:"register_title";s:18:"Create new account";s:14:"password_title";s:20:"Request new password";s:20:"confirmation_display";i:0;s:20:"confirmation_message";s:0:"";}';
  $export['pads_batchuser_organization'] = $regpath;

  return $export;
}
