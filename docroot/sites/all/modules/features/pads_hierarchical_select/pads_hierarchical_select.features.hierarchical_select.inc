<?php
/**
 * @file
 * pads_hierarchical_select.features.hierarchical_select.inc
 */

/**
 * Implements hook_hierarchical_select_default_configs().
 */
function pads_hierarchical_select_hierarchical_select_default_configs() {
$configs = array();
$config = array(
  'config_id'       => 'taxonomy-field_tap_organization',
  'save_lineage'    => 0,
  'enforce_deepest' => 0,
  'entity_count'    => 0,
  'require_entity'  => 0,
  'resizable'       => 1,
  'level_labels' => array(
    'status' => 1,
    'labels' => array(
      0 => 'National',
      1 => 'State',
      2 => 'District',
      3 => 'School',
    ),
  ),
  'dropbox' => array(
    'status'    => 1,
    'title'     => 'Organizations',
    'limit'     => 0,
    'reset_hs'  => 0,
  ),
  'editability' => array(
    'status' => 0,
    'item_types' => array(
      0 => '',
      1 => '',
      2 => '',
      3 => '',
    ),
    'allowed_levels' => array(
      0 => 1,
      1 => 1,
      2 => 1,
      3 => 1,
    ),
    'allow_new_levels' => 0,
    'max_levels'       => 3,
  ),
);

$configs['taxonomy-field_tap_organization'] = $config;
return $configs;
}
