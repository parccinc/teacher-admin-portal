<?php
/**
 * @file
 * pads_menus.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function pads_menus_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_:organizations.
  $menu_links['main-menu_:organizations'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'organizations',
    'router_path' => 'organizations',
    'link_title' => '',
    'options' => array(
      'identifier' => 'main-menu_:organizations',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 0,
  );
  // Exported menu link: main-menu_admin-dmrs-users:dmrsusers/admin.
  $menu_links['main-menu_admin-dmrs-users:dmrsusers/admin'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'dmrsusers/admin',
    'router_path' => 'dmrsusers/admin',
    'link_title' => 'Admin DMRS Users',
    'options' => array(
      'icon' => array(
        'bundle' => 'tap',
        'icon' => 'icon-user',
        'position' => 'title_before',
        'breadcrumb' => 0,
        'title_wrapper' => 1,
        'title_wrapper_element' => 'div',
        'title_wrapper_class' => 'title',
      ),
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_admin-dmrs-users:dmrsusers/admin',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 2,
    'customized' => 1,
  );
  // Exported menu link: main-menu_classes:classes/%.
  $menu_links['main-menu_classes:classes/%'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'classes/%',
    'router_path' => 'classes/%',
    'link_title' => 'Classes',
    'options' => array(
      'icon' => array(
        'bundle' => 'tap',
        'icon' => 'icon-classes',
        'position' => 'title_before',
        'breadcrumb' => 0,
        'title_wrapper' => 1,
        'title_wrapper_element' => 'div',
        'title_wrapper_class' => 'title',
      ),
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_classes:classes/%',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: main-menu_classes:manage/%/classes.
  $menu_links['main-menu_classes:manage/%/classes'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'manage/%/classes',
    'router_path' => 'manage/%/classes',
    'link_title' => 'Classes',
    'options' => array(
      'icon' => array(
        'bundle' => 'tap',
        'icon' => 'icon-classes',
        'position' => 'title_before',
        'breadcrumb' => 0,
        'title_wrapper' => 1,
        'title_wrapper_element' => 'div',
        'title_wrapper_class' => 'title',
      ),
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_classes:manage/%/classes',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: main-menu_dashboard:dashboard.
  $menu_links['main-menu_dashboard:dashboard'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'dashboard',
    'router_path' => 'dashboard',
    'link_title' => 'Dashboard',
    'options' => array(
      'icon' => array(
        'bundle' => 'tap',
        'icon' => 'icon-dashboard',
        'position' => 'title_before',
        'breadcrumb' => 0,
        'title_wrapper' => 1,
        'title_wrapper_element' => 'div',
        'title_wrapper_class' => 'title',
      ),
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_dashboard:dashboard',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_dmrs:dmrsusers/amplify.
  $menu_links['main-menu_dmrs:dmrsusers/amplify'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'dmrsusers/amplify',
    'router_path' => 'dmrsusers/amplify',
    'link_title' => 'DMRS',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'icon' => array(
        'bundle' => 'tap',
        'icon' => 'icon-score-report',
        'position' => 'title_before',
        'breadcrumb' => 0,
        'title_wrapper' => 1,
        'title_wrapper_element' => 'div',
        'title_wrapper_class' => 'title',
      ),
      'identifier' => 'main-menu_dmrs:dmrsusers/amplify',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 2,
    'customized' => 1,
  );
  // Exported menu link: main-menu_help:help.
  $menu_links['main-menu_help:help'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'help',
    'router_path' => 'help',
    'link_title' => 'Help',
    'options' => array(
      'icon' => array(
        'bundle' => 'tap',
        'icon' => 'icon-help',
        'position' => 'title_before',
        'breadcrumb' => 0,
        'title_wrapper' => 1,
        'title_wrapper_element' => 'div',
        'title_wrapper_class' => 'title',
      ),
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_help:help',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 30,
    'customized' => 1,
  );
  // Exported menu link: main-menu_secure-files:files.
  $menu_links['main-menu_secure-files:files'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'files',
    'router_path' => 'files',
    'link_title' => 'Secure Files',
    'options' => array(
      'icon' => array(
        'bundle' => 'tap',
        'icon' => 'icon-secure-files',
        'position' => 'title_before',
        'breadcrumb' => 0,
        'title_wrapper' => 1,
        'title_wrapper_element' => 'div',
        'title_wrapper_class' => 'title',
      ),
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_secure-files:files',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 20,
    'customized' => 1,
  );
  // Exported menu link: main-menu_students:students/%.
  $menu_links['main-menu_students:students/%'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'students/%',
    'router_path' => 'students/%',
    'link_title' => 'Students',
    'options' => array(
      'icon' => array(
        'bundle' => 'tap',
        'icon' => 'icon-students',
        'position' => 'title_before',
        'breadcrumb' => 0,
        'title_wrapper' => 1,
        'title_wrapper_element' => 'div',
        'title_wrapper_class' => 'title',
      ),
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_students:students/%',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -40,
    'customized' => 1,
  );
  // Exported menu link: main-menu_tests:manage/%/tests.
  $menu_links['main-menu_tests:manage/%/tests'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'manage/%/tests',
    'router_path' => 'manage/%/tests',
    'link_title' => 'Tests',
    'options' => array(
      'icon' => array(
        'bundle' => 'tap',
        'icon' => 'icon-tests',
        'position' => 'title_before',
        'breadcrumb' => 0,
        'title_wrapper' => 1,
        'title_wrapper_element' => 'div',
        'title_wrapper_class' => 'title',
      ),
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_tests:manage/%/tests',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -10,
    'customized' => 1,
  );
  // Exported menu link: main-menu_upload-dmrs-users:dmrsusers/import.
  $menu_links['main-menu_upload-dmrs-users:dmrsusers/import'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'dmrsusers/import',
    'router_path' => 'dmrsusers/import',
    'link_title' => 'Upload DMRS Users',
    'options' => array(
      'icon' => array(
        'bundle' => 'tap',
        'icon' => 'icon-user',
        'position' => 'title_before',
        'breadcrumb' => 0,
        'title_wrapper' => 1,
        'title_wrapper_element' => 'div',
        'title_wrapper_class' => 'title',
      ),
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_upload-dmrs-users:dmrsusers/import',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: main-menu_users:tapusers/%.
  $menu_links['main-menu_users:tapusers/%'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'tapusers/%',
    'router_path' => 'tapusers/%',
    'link_title' => 'Users',
    'options' => array(
      'icon' => array(
        'bundle' => 'tap',
        'icon' => 'icon-user',
        'position' => 'title_before',
        'breadcrumb' => 0,
        'title_wrapper' => 1,
        'title_wrapper_element' => 'div',
        'title_wrapper_class' => 'title',
      ),
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_users:tapusers/%',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -20,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Admin DMRS Users');
  t('Classes');
  t('DMRS');
  t('Dashboard');
  t('Help');
  t('Secure Files');
  t('Students');
  t('Tests');
  t('Upload DMRS Users');
  t('Users');

  return $menu_links;
}
