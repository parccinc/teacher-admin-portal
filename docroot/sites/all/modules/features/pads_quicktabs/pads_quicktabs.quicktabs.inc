<?php
/**
 * @file
 * pads_quicktabs.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function pads_quicktabs_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'pads_quicktab_students';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Students';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'pads_students_by_class',
      'display' => 'default',
      'args' => '',
      'use_title' => 0,
      'title' => 'Students',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'pads_classes_assigned_to_users',
      'display' => 'default',
      'args' => '',
      'use_title' => 0,
      'title' => 'Classes',
      'weight' => '-99',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Classes');
  t('Students');

  $export['pads_quicktab_students'] = $quicktabs;

  return $export;
}
