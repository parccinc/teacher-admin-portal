<?php
/**
 * @file
 * Contains \Drupal\isbe\tests\IsbeTest.
 */

namespace Drupal\isbe\tests;

use Drupal\pads\tests\LdrTest;

/**
 * Class IsbeTest.
 *
 * @package Drupal\isbe\tests
 */
class IsbeLdrTest extends LdrTest {

  /**
   * {@inheritdoc}
   */
  public function canUpdateAccommodations($test_assignment_id) {
    return FALSE;
  }

}
