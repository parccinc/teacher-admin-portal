<?php

/**
 * @file
 * PADS generic installation code.
 */

/**
 * Generic theme installation.
 */
function pads_install_themes() {
  $themes = array(PADS_THEME);

  // Enable adminimal, if needed.
  if (PADS_ADMIN_THEME == 'adminimal') {
    $themes[] = PADS_ADMIN_THEME;
  }

  theme_enable($themes);
  variable_set('theme_default', PADS_THEME);
  variable_set('admin_theme', PADS_ADMIN_THEME);
  variable_set('node_admin_theme', TRUE);

  // Disable other themes.
  $disable = array_diff(array_keys(list_themes(TRUE)), $themes);
  theme_disable($disable);
}

/**
 * Generic administrative user installation.
 */
function pads_install_admin() {
  // Create an admin role if needed.
  if (!$admin_role = user_role_load_by_name('administrator')) {
    $admin_role = new stdClass();
    $admin_role->name = 'administrator';
    $admin_role->weight = 10;
    user_role_save($admin_role);
  }

  // We want 'administrator' as rid 3, so it's not we'll move the other role out
  // and the admin role in. Features will handle anything else.
  $third_role = user_role_load(3);
  if ($third_role->name != 'administrator') {
    $admin_role->name = 'pads_admin_placeholder';
    user_role_save($admin_role);
    $admin_role->name = $third_role->name;
    $third_role->name = 'administrator';
    user_role_save($third_role);
    user_role_save($admin_role);
  }

  // Create a default role for site administrators, with all available
  // permissions assigned.
  user_role_grant_permissions($third_role->rid, array_keys(module_invoke_all('permission')));
  // Set this as the administrator role.
  variable_set('user_admin_role', $third_role->rid);

  // Assign user 1 the "administrator" role.
  db_insert('users_roles')
    ->fields(array('uid' => 1, 'rid' => $third_role->rid))
    ->execute();
}

/**
 * Generic vendor install.
 */
function pads_install_vendor() {
  // No op.
}
