; pads make file for d.o. usage
core = "7.x"
api = "2"

; +++++ Modules +++++

projects[acl][version] = "1.1"
projects[acl][subdir] = "contrib"

projects[content_access][version] = "1.2-beta2"
projects[content_access][subdir] = "contrib"

projects[admin_menu][version] = "3.0-rc5"
projects[admin_menu][subdir] = "contrib"

projects[admin_views][version] = "1.5"
projects[admin_views][subdir] = "contrib"

projects[adminimal_admin_menu][version] = "1.6"
projects[adminimal_admin_menu][subdir] = "contrib"

projects[module_filter][version] = "2.0"
projects[module_filter][subdir] = "contrib"

projects[ctools][version] = "1.9"
projects[ctools][subdir] = "contrib"

projects[user_pages][version] = "1.0"
projects[user_pages][subdir] = "contrib"

projects[date][version] = "2.9"
projects[date][subdir] = "contrib"

projects[devel][version] = "1.5"
projects[devel][subdir] = "contrib"

projects[efq_helper][version] = "1.1"
projects[efq_helper][subdir] = "contrib"

projects[profiler_builder][version] = "1.2"
projects[profiler_builder][subdir] = "contrib"

projects[features][version] = "2.7"
projects[features][subdir] = "contrib"

projects[entityreference][version] = "1.1"
projects[entityreference][subdir] = "contrib"

projects[filefield_sources][version] = "1.10"
projects[filefield_sources][subdir] = "contrib"

projects[filefield_sources_plupload][version] = "1.1"
projects[filefield_sources_plupload][subdir] = "contrib"

projects[name][version] = "1.9"
projects[name][subdir] = "contrib"

projects[hierarchical_select][version] = "3.0-beta2"
projects[hierarchical_select][subdir] = "contrib"

projects[homebox][version] = "2.0-rc1"
projects[homebox][subdir] = "contrib"

projects[icon][version] = "1.0-beta6"
projects[icon][subdir] = "contrib"

projects[mailsystem][version] = "2.34"
projects[mailsystem][subdir] = "contrib"

projects[plupload][version] = "1.7"
projects[plupload][subdir] = "contrib"

projects[message][version] = "1.10"
projects[message][subdir] = "contrib"

projects[message_notify][version] = "2.5"
projects[message_notify][subdir] = "contrib"

projects[migrate][version] = "2.8"
projects[migrate][subdir] = "contrib"

projects[advanced_help][version] = "1.3"
projects[advanced_help][subdir] = "contrib"

projects[auto_nodetitle][version] = "1.0"
projects[auto_nodetitle][subdir] = "contrib"

projects[autologout][version] = "4.4"
projects[autologout][subdir] = "contrib"

projects[backup_migrate][version] = "3.1"
projects[backup_migrate][subdir] = "contrib"

projects[composer_manager][version] = "1.8"
projects[composer_manager][subdir] = "contrib"

projects[diff][version] = "3.2"
projects[diff][subdir] = "contrib"

projects[email_registration][version] = "1.3"
projects[email_registration][subdir] = "contrib"

projects[entity][version] = "1.6"
projects[entity][subdir] = "contrib"

projects[environment_indicator][version] = "2.8"
projects[environment_indicator][subdir] = "contrib"

projects[libraries][version] = "2.2"
projects[libraries][subdir] = "contrib"

projects[pathauto][version] = "1.3"
projects[pathauto][subdir] = "contrib"

projects[profile2][version] = "1.3"
projects[profile2][subdir] = "contrib"

projects[profile2_regpath][version] = "2.x-dev"
projects[profile2_regpath][subdir] = "contrib"

projects[quicktabs][version] = "3.x-dev"
projects[quicktabs][subdir] = "contrib"

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = "contrib"

projects[taxonomy_entity_index][version] = "1.0-beta7"
projects[taxonomy_entity_index][subdir] = "contrib"

projects[token][version] = "1.6"
projects[token][subdir] = "contrib"

projects[pads_help_content][version] = "1.0"
projects[pads_help_content][subdir] = "contrib"

projects[pads_standards][version] = "1.0"
projects[pads_standards][subdir] = "contrib"

projects[pads_wrappers][version] = "1.0"
projects[pads_wrappers][subdir] = "contrib"

projects[panels][version] = "3.5"
projects[panels][subdir] = "contrib"

projects[rules][version] = "2.9"
projects[rules][subdir] = "contrib"

projects[password_policy][version] = "2.0-alpha5"
projects[password_policy][subdir] = "contrib"

projects[honeypot][version] = "1.21"
projects[honeypot][subdir] = "contrib"

projects[jquery_update][version] = "2.7"
projects[jquery_update][subdir] = "contrib"

projects[datatables][version] = "0.1"
projects[datatables][subdir] = "contrib"

projects[views][version] = "3.13"
projects[views][subdir] = "contrib"

projects[views_bulk_operations][version] = "3.3"
projects[views_bulk_operations][subdir] = "contrib"

projects[views_data_export][version] = "3.0-beta9"
projects[views_data_export][subdir] = "contrib"

projects[wrappers_delight][version] = "1.2"
projects[wrappers_delight][subdir] = "contrib"

; +++++ Themes +++++

; adminimal_theme
projects[adminimal_theme][type] = "theme"
projects[adminimal_theme][version] = "1.23"
projects[adminimal_theme][subdir] = "contrib"

; zurb_foundation
projects[zurb_foundation][type] = "theme"
projects[zurb_foundation][version] = "5.0-rc6"
projects[zurb_foundation][subdir] = "contrib"

; +++++ Libraries +++++

; Plupload
libraries[plupload][directory_name] = "plupload"
libraries[plupload][type] = "library"
libraries[plupload][destination] = "libraries"
libraries[plupload][patch][] = "http://drupal.org/files/plupload-1_5_6-rm_examples-1903850-5.patch"
libraries[plupload][download][type] = "file"
libraries[plupload][download][url] = "http://plupload.com/downloads/plupload_1_5_6.zip"
