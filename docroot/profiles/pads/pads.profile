<?php

/**
 * @file
 * PADS installation profile.
 */

pads_include('profile');

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function pads_form_install_configure_form_alter(&$form) {
  $form['site_information']['site_name']['#default_value'] = PADS_SITE_NAME;
}

/**
 * Include code from additional files.
 *
 * Attempts to load vendor-specific and generic include files.
 */
function pads_include($file) {
  $vendor = pads_get_vendor();

  // Look for vendor specific include.
  $filename = __DIR__ . "/vendor/$vendor/$file.inc";
  if (is_readable($filename)) {
    require_once $filename;
  }

  // Look for generic include.
  $filename = __DIR__ . "/includes/$file.inc";
  if (is_readable($filename)) {
    require_once $filename;
  }
}

/**
 * Invokes vendor-specific functionality.
 */
function pads_vendor_invoke($op, $params = array()) {
  $func = "pads_{$op}_" . pads_get_vendor();
  if (function_exists($func)) {
    return call_user_func_array($func, $params);
  }

  $func = "pads_{$op}";
  return call_user_func_array($func, $params);
}

/**
 * Determine the vendor.
 *
 * The vendor is determined by the 'pads_vendor' variable, or if not set will
 * attempt to determine based on the location of the multi-site installation.
 * Default to 'parcc' when unable to determine.
 *
 * @return string
 *   Indicates vendor for which the system is operating. Defaults to "parcc".
 */
function pads_get_vendor() {
  if ($vendor = variable_get('pads_vendor')) {
    return $vendor;
  }

  switch (conf_path()) {
    case 'sites/vendor/isbe':
      $vendor = 'isbe';
      break;

    default:
      $vendor = 'parcc';
  }

  return $vendor;
}
