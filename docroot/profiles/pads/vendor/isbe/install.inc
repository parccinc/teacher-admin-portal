<?php

/**
 * @file
 * ISBE-specific install code.
 */

/**
 * ISBE vendor installation.
 */
function pads_install_vendor_isbe() {
  // Install extra modules, etc.
  $modules[] = 'isbe_tests';
  $modules[] = 'pads_isbe_client';
  $modules[] = 'pads_isbe_services';
  module_enable($modules);
}
