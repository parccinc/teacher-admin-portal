<?php

/**
 * @file
 * PARCC-specific installation profile code.
 */

// Clear session cookies on browser close.
ini_set('session.cookie_lifetime', variable_get('pads_session_cookie_lifetime', 0));

// Session inactivity timeout configuration handled via
// https://www.drupal.org/project/autologout.

define('PADS_SITE_NAME', 'ISBE Teach');

define('PADS_THEME', 'isbe_teach');
define('PADS_ADMIN_THEME', 'adminimal');
