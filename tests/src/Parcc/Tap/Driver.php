<?php

namespace Parcc\Tap;

class Driver {

  /**
   * Reset Drupal to a "clean" state.
   */
  public function reset() {
    $this->resetNodes();
    $this->resetOrgs();
    $this->resetUsers();
  }

  /**
   * Delete all nodes.
   */
  public function resetNodes() {
    $query = 'SELECT nid FROM {node} LIMIT 25';

    while ($nids = db_query($query)->fetchCol()) {
      node_delete_multiple($nids);
    }
  }

  /**
   * Delete all organization terms.
   *
   * Also clears migration mappings.
   */
  public function resetOrgs() {
    // Get the vocab.
    if (!$vocab = taxonomy_vocabulary_machine_name_load('organization')) {
      throw new \Exception('Unable to find "organization" voacbulary.');
    }

    $query = 'SELECT tid FROM {taxonomy_term_data}
      WHERE vid = ? LIMIT 25';
    $args = array($vocab->vid);

    while ($tids = db_query($query, $args)->fetchCol()) {
      while ($tid = array_shift($tids)) {
        taxonomy_term_delete($tid);
      }
    }

    foreach (migrate_migrations() as $migration) {
      $map = $migration->getMap();
      if ($map instanceof \MigrateSQLMap) {
        $table = $map->getMapTable();
        db_query("TRUNCATE TABLE {$table}");
      }
    }

  }

  /**
   * Removes all users except the super admin.
   */
  public function resetUsers() {
    $query = 'SELECT uid FROM {users} WHERE uid > ? LIMIT 25';
    $args = array(1);

    while ($uids = db_query($query, $args)->fetchCol()) {
      user_delete_multiple($uids);
    }
  }

}
