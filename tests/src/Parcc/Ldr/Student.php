<?php

namespace Parcc\Ldr;

class Student extends EntityBase {

  public static function load($logId, $db, $studentRecordId) {
    if ($data = dbGetStudentRecord($logId, $db,  $studentRecordId)) {
      return self::factory($logId, $db, $data);
    }
  }

  public static function loadByIdentifier($logId, $db, $stateOrgId, $stateIdentifier) {
    if ($studentRecordId = dbGetStudentRecordId_ByStateOrgId_ByStateIdentifier($logId, $db, $stateOrgId, $stateIdentifier)) {
      return self::load($logId, $db, $studentRecordId);
    }
  }

  /**
   * Saves a studentRecord object in Ldr, populates $this->studentRecordId.
   *
   * @throws \Exception
   */
  public function save() {
    $this->validate();

    if (empty($this->studentRecordId)) {
      $result = dbCreateStudentRecord($this->logId, $this->db, 1, $this->enrollOrgId,
        $this->stateOrgId, $this->stateIdentifier, $this->localIdentifier,
        '', $this->lastName, $this->firstName, $this->middleName, $this->dateOfBirth,
        $this->gender, '', $this->gradeLevel, '', '', '', '', '', '', '', '',
        '', '', '', '', '', '', '', '', '', '', '', '', array());
      $this->studentRecordId = is_array($result) && array_key_exists('studentRecordId', $result) ?
        $result['studentRecordId'] : $result;
    }
    else {
      dbUpdateStudentRecord($this->logId, $this->db, 1, $this->studentRecordId,
        $this->stateIdentifier, $this->localIdentifier, $this->lastName,
        $this->firstName, $this->middleName, $this->dateOfBirth, $this->gender,
        $this->gradeLevel, '', '', '', '', '', '', '', '', '', '', '', '', array());
    }
  }

  protected function validate() {
    // Ensure necessary properties are set.
    if (empty($this->enrollOrgId)) {
      throw new \Exception('LDR student missing enrollOrgId.');
    }

    if (!isset($this->dateOfBirth)) {
      throw new \Exception('LDR student missing dateOfBirth.');
    }

    if (empty($this->stateIdentifier)) {
      throw new \Exception('LDR student missing stateIdentifier.');
    }

    if (empty($this->gender)) {
      throw new \Exception('LDR student missing gender.');
    }

    if (empty($this->gradeLevel)) {
      throw new \Exception('LDR student missing gradeLevel.');
    }

    // Clean up and ensure a few properties are set.
    if (empty($this->stateOrgId)) {
      $org = Organization::load($this->logId, $this->db, $this->enrollOrgId);
      if (!$org) {
        throw new \Exception('Unable to determine student\'s state.');
      }
      $this->stateOrgId = $org->organizationId;
    }

    if (!isset($this->localIdentifier)) {
      $this->localIdentifier = '';
    }

    if (!isset($this->middleName)) {
      $this->middleName = '';
    }

    $this->dateOfBirth = date('Y-m-d', strtotime($this->dateOfBirth));
  }

  public function delete() {
    if (empty($this->studentRecordId)) {
      throw new \Exception('Unable to delete student with no studentRecordId.');
    }
    dbDeleteStudentRecord($this->logId, $this->db, 1, $this->studentRecordId);
  }
}
