<?php

namespace Parcc\Ldr;

abstract class TestBase extends EntityBase {

  protected function getNextId($table, $idCol) {
    $sql = "SELECT MAX($idCol) FROM $table";
    $stmt = $this->db->prepare($sql);
    $stmt->execute();
    return $stmt->fetchColumn() + 1;
  }

  protected function getId($value, $table, $idCol, $lookupCol) {
    $sql = "SELECT $idCol FROM $table WHERE $lookupCol = ? AND deleted = ?";
    $stmt = $this->db->prepare($sql);
    $stmt->execute(array($value, 'N'));
    return $stmt->fetchColumn();
  }

}
