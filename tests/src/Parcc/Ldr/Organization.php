<?php

namespace Parcc\Ldr;

class Organization extends EntityBase {

  public static function load($logId, $db, $organizationId) {
    try {
      $data = dbGetOrganization($logId, $db, $organizationId);
      return self::factory($logId, $db, $data);
    } catch (\Exception $e) {
      return NULL;
    }
  }

  public static function loadByName($logId, $db, $name) {
    $sql = "SELECT organizationId FROM organization WHERE organizationName = ? AND deleted = ?";
    $stmt = $db->prepare($sql);
    $stmt->execute(array($name, 'N'));
    $organizationId = $stmt->fetchColumn();
    return self::load($logId, $db, $organizationId);
  }

  public function save() {
    $this->validate();

    if (empty($this->organizationId)) {
      $this->organizationId = dbCreateOrganization($this->logId, $this->db, 1, $this->organizationName,
        $this->organizationType, $this->organizationIdentifier, '',  '',
        $this->parentOrganizationId, '');
    } else {
      dbUpdateOrganization($this->logId, $this->db, 1, $this->organizationId,
        $this->organizationName, $this->organizationIdentifier);
    }
  }

  protected function validate() {

    $parent = $this->parentOrganizationId;

    // Try to find the parent by ID.
    if (!self::load($this->logId, $this->db, $this->parentOrganizationId)) {
      // Look up by name next level up.
      $sql = "SELECT organizationId FROM organization WHERE organizationName = ?
        AND deleted = ? AND organizationType = ?";
      $stmt = $this->db->prepare($sql);
      $stmt->execute(array($this->parentOrganizationId, 'N', $this->organizationType - 1));
      $this->parentOrganizationId = $stmt->fetchColumn();
    }

    if (!$this->parentOrganizationId) {
      throw new \Exception(sprintf('Unable to find parent org "%s"', $parent));
    }

    if (!isset($this->organizationIdentifier)) {
      $this->organizationIdentifier = '';
    }
  }

  public function delete() {
    if (empty($this->organizationId)) {
      throw new \Exception('Unable to delete organization with no organizationId');
    }
    dbDeleteOrganization($this->logId, $this->db, 1, $this->organizationId);
  }

}
