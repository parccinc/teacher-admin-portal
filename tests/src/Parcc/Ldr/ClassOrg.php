<?php

namespace Parcc\Ldr;


class ClassOrg extends EntityBase {

  public static function load($logId, $db, $classId) {
    if ($data = dbGetClass($logId, $db, $classId)) {
      $class = self::factory($logId, $db, $data);
      return $class;
    }
  }

  public function save() {
    if (empty($this->classId)) {
      $this->classId = dbCreateClass($this->logId, $this->db, 1, $this->organizationId,
        $this->classIdentifier, $this->sectionNumber, $this->gradeLevel);
    } else {
      dbUpdateClass($this->logId, $this->db, 1, $this->classId,
        $this->classIdentifier, $this->sectionNumber, $this->gradeLevel);
    }

  }

  protected function validate() {
    if (!isset($this->sectionNumber)) {
      $this->sectionNumber = '';
    }

    $properties = array(
      'organizationId',
      'classIdentifier',
      'gradeLevel',
    );
    $this->validateRequired('class', $properties);
  }

  public function delete() {
    if (empty($this->classId)) {
      throw new \Exception('Unable to delete class with no classId');
    }
    dbDeleteClass($this->logId, $this->db, 1, $this->classId);
  }

}
