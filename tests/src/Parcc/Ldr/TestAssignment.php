<?php

namespace Parcc\Ldr;

class TestAssignment extends TestBase {

  public static function load($logId, $db, $testAssignmentId) {
    if ($data = self::loadAssignment($logId, $db, $testAssignmentId)) {
      $test = self::factory($logId, $db, $data);
      $test->new = FALSE;
      return $test;
    }
  }

  public static function loadAssignment($logId, $db, $testAssignmentId) {
    return dbGetTestAssignment_ById($logId, $db, $testAssignmentId);
  }

  public function save() {
    $this->validate();

    if (empty($this->testAssignmentId)) {
      $this->testAssignmentId = dbCreateTestAssignment($this->logId, $this->db,
        1, $this->studentRecordId, $this->testBatteryId, $this->testBatteryFormId, '');
    }

    // Have to re-save to set status, this makes me frown :-(.
    $assignment = $this->loadAssignment($this->logId, $this->db, $this->testAssignmentId);

    try {
      dbUpdateTestAssignment($this->logId, $this->db, 1, $this->testAssignmentId,
        $assignment, $this->testAssignmentStatus, '', '', '');
    } catch (\Exception $e) {
      // Ignore "no changes made" exceptions.
      if (!strpos($e->getMessage(), 'Update arguments are missing or match existing values')) {
        throw $e;
      }
    }

    // Add the embedded test assignment reference.
    $sql = "UPDATE test_assignment SET embeddedScoreReportId = ? WHERE testAssignmentId = ?";
    $stmt = $this->db->prepare($sql);
    $stmt->execute(array($this->embeddedScoreReportId, $this->testAssignmentId));
  }

  protected function validate() {
    if (empty($this->testAssignmentStatus)) {
      $this->testAssignmentStatus = 'Scheduled';
    }

    $properties = array(
      'studentRecordId',
      'testBatteryId',
    );
    $this->validateRequired('test_assignment', $properties);

    $this->testBatteryFormId = $this->getId($this->testBatteryId, 'test_battery_form', 'testBatteryFormId', 'testBatteryId');
    if (!$this->testBatteryFormId) {
      throw new \Exception(sprintf('LDR form for test battery "%s" not found.', $this->testBatteryId));
    }

    if (empty($this->embeddedScoreReportId)) {
      $this->embeddedScoreReportId = 0;
    }
  }

  public function delete() {
    $sql = "UPDATE test_assignment SET deleted = ? WHERE testAssignmentId = ?";
    $stmt = $this->db->prepare($sql);
    $stmt->execute(array('Y', $this->testAssignmentId));
  }
}
