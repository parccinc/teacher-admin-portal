<?php

/**
 * @file
 * LDR internal functions.
 */

define('TENANT_DIR', $this->path . '/ldr_svcs/tenant');

function generateIdentifier($length)
{
  $identifierCharSet = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ'; // Excludes 0, 1, I, and O
  $charSetMaxIndex = strlen($identifierCharSet) - 1;

  $identifier = "";
  while ($length-- > 0)
    $identifier .= substr($identifierCharSet, mt_rand(0, $charSetMaxIndex), 1);

  return $identifier;
}

function generateGUID_RFC4122v4()
{
  $uuidCharSet = '0123456789ABCDEF';
  $nibbleCharSet = '89AB';

  $uuid = '';
  for ($idx = 0; $idx < LENGTH_GUID_RFC4122; $idx++)
  {
    switch ($idx)
    {
      case 14:
        $uuid .= '4'; // RFC4122 version 4
        break;
      case 19:
        $uuid .= substr($nibbleCharSet, mt_rand(0, 3), 1);
        break;
      case 8:
      case 13:
      case 18:
      case 23:
        $uuid .= '-'; // RFC4122 version 4
        break;
      default:
        $uuid .= substr($uuidCharSet, mt_rand(0, 15), 1);
        break;
    }
  }

  return $uuid;
}

function logError($logId, $error)
{
  throw new Exception($error);
}

function adpCreateTestAssignment() {
  return NULL;
}

function adpUpdateTestAssignment() {
  return 'success';
}
