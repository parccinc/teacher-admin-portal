<?php

namespace Parcc\Ldr;

use ErrorLDR;
use PDO;

class Driver {

  protected static $driver;

  // LDR internals.
  protected $logId;
  protected $db;
  protected $path;

  /**
   * Driver constructor.
   *
   * @param string $path
   *   Path to LDR root.
   */
  private function __construct($path) {
    $this->path = $path;
    $this->bootstrap();
    $this->logId = \generateIdentifier(4);
    $this->db = $this->dbConnect($this->logId);
  }

  /**
   * Initializes an LDR driver.
   *
   * @param string $path
   *   Path to LDR root.
   *
   * @return \Parcc\Ldr\Driver
   *   Ldr Driver instance.
   */
  public static function factory($path) {
    if (!self::$driver) {
      self::$driver = new Driver($path);
    }

    return self::$driver;
  }

  /**
   * Bootstrap LDR codebase.
   */
  protected function bootstrap() {
    try {
      global $argMaxLength, $referenceIdColumns, $referenceValueColumns;
      include_once __DIR__ . '/ldr.php';
      include_once $this->path . '/ldr_common/definitions.php';
      $config_file = file_exists($this->path . '/ldr_svcs/tenant.php') ?
        $this->path . '/ldr_svcs/tenant.php' :
        $this->path . '/ldr_svcs/config.php';
      include_once $config_file;
      include_once $this->path . '/ldr_svcs/class.php';
      include_once $this->path . '/ldr_svcs/database.php';
      include_once $this->path . '/ldr_svcs/organization.php';
      include_once $this->path . '/ldr_svcs/redis.php';
      include_once $this->path . '/ldr_svcs/student.php';
      include_once $this->path . '/ldr_svcs/test_assignment.php';
      include_once $this->path . '/ldr_svcs/test_battery.php';
      include_once $this->path . '/ldr_svcs/user_audit.php';
      include_once $this->path . '/ldr_svcs/validation.php';
    }
    catch (\Exception $e) {
      throw new \Exception('Unable to bootstrap LDR.');
    }
  }

  /**
   * Add an organization.
   */
  public function addOrganization($params) {
    $org = Organization::factory($this->logId, $this->db, $params);
    $org->save();
    return $org;
  }

  /**
   * Add a class.
   */
  public function addClass($params) {
    $class = ClassOrg::factory($this->logId, $this->db, $params);
    $class->save();
    return $class;
  }

  /**
   * Delete a class that was created by a test and not scaffolded.
   */
  public function deleteClass($classId) {
    $class = ClassOrg::load($this->logId, $this->db, $classId);
    $class->delete();
  }

  /**
   * Add a class assignment.
   */
  public function addClassAssignment($params) {
    $assignment = ClassAssignment::factory($this->logId, $this->db, $params);
    $assignment->save();
    return $assignment;
  }

  /**
   * Add a student.
   */
  public function addStudent($params) {
    $student = Student::factory($this->logId, $this->db, $params);
    $student->save();
    return $student;
  }

  /**
   * Load a student by identifier and state.
   */
  public function getStudentByIdentifier($identifier, $state) {
    // Get organization by name.
    if (!$org = Organization::loadByName($this->logId, $this->db, $state)) {
      return;
    }
    $student = Student::loadByIdentifier($this->logId, $this->db, $org->organizationId, $identifier);
    return $student;
  }

  /**
   * Add a test.
   */
  public function addTest($params) {
    $test = Test::factory($this->logId, $this->db, $params);
    $test->save();
    return $test;
  }

  /**
   * Add a test assignment.
   */
  public function addTestAssignment($params) {
    $assignment = TestAssignment::factory($this->logId, $this->db, $params);
    $assignment->save();
    return $assignment;
  }

  /**
   * Add a score report.
   */
  public function addScoreReport($params) {
    $score = ScoreReport::factory($this->logId, $this->db, $params);
    $score->save();
    return $score;
  }

  /**
   * Resets LDR to a fresh, clean state.
   */
  public function reset() {
    $this->resetTests();
    $this->resetClasses();
    $this->resetStudents();
    $this->resetOrgs();
  }

  /**
   * Reset organizations.
   *
   * Removes all organizations, ensures root PARCC org and XX state org exist.
   */
  public function resetOrgs() {
    // Kill em all (annihilation).
    $this->resetTable('organization');

    // Insert root.
    $sql = "INSERT INTO organization
      (organizationId, organizationType, stateOrganizationId,
      parentOrganizationId, organizationName, nestedSetLeft, nestedSetRight)
      VALUES ( ? , ? , ? , ? , ? , ? , ? )";
    $stmt = $this->db->prepare($sql);
    $stmt->execute(array(
      ROOT_ORGANIZATION_ID,
      1,
      0,
      ROOT_PARENT_ORGANIZATION_ID,
      ROOT_ORGANIZATION_NAME,
      1,
      2
    ));
    $root = ROOT_ORGANIZATION_ID;

    // Insert XX state.
    $params['organizationName'] = 'XX';
    $params['organizationType'] = 2;
    $params['parentOrganizationId'] = $root;
    $xx = Organization::factory($this->logId, $this->db, $params);
    $xx->save();
  }

  /**
   * Removes classes and student class assignments.
   */
  public function resetClasses() {
    $this->resetTable('student_class');
    $this->resetTable('class');
  }

  /**
   * Removes all test data from LDR.
   */
  public function resetTests() {
    $this->resetTable('test_assignment');
    $this->resetTable('embedded_score_report');
    $this->resetTable('test_battery_form', FALSE);
    $this->resetTable('test_battery', FALSE);
  }

  /**
   * Removes all student data from LDR.
   */
  public function resetStudents() {
    // TODO: avoid deleting practice test user?
    $this->resetTable('student_record', FALSE);
    $this->resetTable('student');
  }

  /**
   * Utility function for clearing all data from a table.
   *
   * @param string $table
   *   The table to be reset.
   * @param bool $truncate
   *   Ideally we can TRUNCATE the table, but tables referenced by foreign keys
   *   TRUNCATE is not allowed. Setting this to FALSE performs a DELETE.
   */
  protected function resetTable($table, $truncate = TRUE) {
    if ($truncate) {
      $sql = 'TRUNCATE TABLE ' . $table;
    }
    else {
      $sql = 'DELETE FROM ' . $table;
    }

    $stmt = $this->db->prepare($sql);
    $stmt->execute();
  }

  /**
   * Prepare a PDO connection for LDR.
   *
   * @param string $log_id
   *   Four digit log identifier from Ldr.
   *
   * @return \PDO
   *   PDO object.
   */
  public function dbConnect($log_id) {

    // Open the database server connection.
    $db_hostname = DB_HOSTNAME;
    $dsn = "mysql:host=$db_hostname";
    $dsn .= defined('DB_PORT') && DB_PORT ? ';port=' . DB_PORT : '';
    try {
      $pdo = new \PDO($dsn, DB_USERNAME, DB_PASSWORD);
      $pdo->exec("SET NAMES utf8");
    }
    catch (\PDOException $error) {
      $error_ldr = new ErrorLDR($log_id, LDR_EC_DATABASE_CONNECT_FAILED, $error->getMessage());
      $error_ldr->logError(__FUNCTION__);
      $error_ldr->logMessage(__FUNCTION__, 'DSN = |' . $dsn . '|');
      return $error_ldr;
    }

    // Configure the connection to throw exceptions.
    if (!$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION)) {
      // Close the database connection.
      $pdo = NULL;
      $error_ldr = new ErrorLDR($log_id, LDR_EC_DATABASE_SET_ATTRIBUTE_FAILED,
        'Database connection configuration failed');
      $error_ldr->logError(__FUNCTION__);
      return $error_ldr;
    }

    // Get the database name.
    try {
      $db_name = DB_PRODUCTION;
      if ($db_name === FALSE) {
        $error_ldr = new ErrorLDR($log_id, LDR_EC_DATABASE_SELECTION_FAILED, LDR_ED_DATABASE_SELECTION_FAILED);
        $error_ldr->logError(__FUNCTION__);
        return $error_ldr;
      }
    }
    catch (\Exception $error) {
      $error_ldr = new ErrorLDR($log_id, LDR_EC_DATABASE_SELECTION_FAILED, $error->getMessage());
      $error_ldr->logError(__FUNCTION__);
      return $error_ldr;
    }

    // Select the LDR database.
    try {
      $pdo->exec("USE $db_name;");
    }
    catch (\PDOException $error) {
      // Close the database connection.
      $pdo = NULL;
      $error_ldr = new ErrorLDR($log_id, LDR_EC_USE_DATABASE_FAILED, $error->getMessage());
      $error_ldr->logError(__FUNCTION__);
      return $error_ldr;
    }

    // Return the connection.
    return $pdo;
  }

}
