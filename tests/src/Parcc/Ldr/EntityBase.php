<?php

namespace Parcc\Ldr;

abstract class EntityBase implements Entity {

  protected $logId;
  protected $db;

  public function __construct($logId, $db) {
    $this->logId = $logId;
    $this->db = $db;
  }

  public static function factory($logId, $db, $params = array()) {
    $class = get_called_class();
    $entity = new $class($logId, $db);
    foreach ($params as $name => $value) {
      $entity->$name = $value;
    }
    return $entity;
  }

  protected function validateRequired($type, $properties) {
    foreach ($properties as $name) {
      if (empty($this->$name)) {
        throw new \Exception(sprintf('LDR %s missing %s.', $type, $name));
      }
    }
  }

}
