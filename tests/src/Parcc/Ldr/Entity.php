<?php

namespace Parcc\Ldr;

interface Entity {

  /**
   * @param string $logId
   * @param \PDO $db
   */
  public function __construct($logId, $db);

  /**
   * @param string $logId
   * @param \PDO $db
   * @param int $id
   * @return Entity
   */
  public static function load($logId, $db, $id);

  public function save();

  public function delete();

  /**
   * @param string $logId
   * @param \PDO $db
   * @param array $params
   * @return Entity
   */
  public static function factory($logId, $db, $params = array());

}
