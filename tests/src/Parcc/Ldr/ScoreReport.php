<?php

namespace Parcc\Ldr;

class ScoreReport extends EntityBase {

  public static function load($logId, $db, $embeddedScoreReportId) {
    if ($data = self::loadScoreRport($logId, $db,  $embeddedScoreReportId)) {
      return self::factory($logId, $db, $data);
    }
  }

  protected static function loadScoreReport($logId, $db,  $embeddedScoreReportId) {
    $sql = "SELECT * FROM embedded_score_report WHERE deleted = ? AND embeddedScoreReportId = ?";
    $stmt = $db->prepare($sql);
    $stmt->execute(array('N', $embeddedScoreReportId));
    return $stmt->fetch(\PDO::FETCH_ASSOC);

  }

  public function save() {
    $this->validate();

    if (empty($this->embeddedScoreReportId)) {
      // New.
      $sql = "INSERT INTO embedded_score_report (embeddedScoreReport) VALUES (?)";
      $stmt = $this->db->prepare($sql);
      $stmt->execute(array($this->embeddedScoreReport));
      $this->embeddedScoreReportId = $this->db->lastInsertId();
    } else {
      // Existing.
      $sql = "UPDATE embedded_score_report
        SET embeddedScoreReport = ?
        WHERE embeddedScoreReportId = ?";
      $stmt = $this->db->prepare($sql);
      $stmt->execute(array($this->embeddedScoreReport, $this->embeddedScoreReportId));
    }

  }

  protected function validate() {

    if (isset($this->score_report)) {
      if (!empty($this->score_report)) {
        $this->embeddedScoreReport = base64_encode($this->score_report);
      }
      unset($this->score_report);
    }

    $properties = array('embeddedScoreReport');
    $this->validateRequired('score_report', $properties);
  }

  public function delete() {
    $sql = "UPDATE embedded_score_report SET deleted = ? WHERE embeddedScoreReportId = ?";
    $stmt = $this->db->prepare($sql);
    $stmt->execute(array('Y', $this->embeddedScoreReportId));
  }

}
