<?php

namespace Parcc\Ldr;

class Test extends TestBase {

  static $subjects = array();

  protected $new = TRUE;

  public static function load($logId, $db, $testBatteryId) {
    if ($data = dbGetTestBattery($logId, $db,  $testBatteryId)) {
      $test = self::factory($logId, $db, $data);
      $test->new = FALSE;
      return $test;
    }
  }

  public function save() {
    $this->validate();

    if ($this->new) {
      // Create.
      dbCreateTestBattery($this->logId, $this->db, $this->testBatteryId,
        $this->testBatteryName, $this->testBatterySubjectId,
        $this->testBatteryGradeId, $this->testBatteryProgramId,
        $this->testBatterySecurityId, $this->testBatteryPermissionsId,
        $this->testBatteryScoringId, $this->testBatteryDescription);
      $this->createForm();
    } else {
      // Update.
      dbUpdateTestBattery($this->logId, $this->db, $this->testBatteryId,
        $this->testBatteryName, $this->testBatterySubjectId,
        $this->testBatteryGradeId, $this->testBatteryProgramId,
        $this->testBatterySecurityId, $this->testBatteryPermissionsId,
        $this->testBatteryScoringId, $this->testBatteryDescription);
    }

    $this->new = FALSE;
  }

  protected function validate() {

    if (empty($this->testBatteryId)) {
      $this->testBatteryId = $this->getNextId('test_battery', 'testBatteryId');
    }

    if (!empty($this->testBatterySubject)) {
      if (!$this->testBatterySubjectId = $this->getSubject($this->testBatterySubject)) {
        throw new \Exception(sprintf('LDR test battery subject "%s" not found.', $this->testBatterySubject));
      }
    }

    if (!empty($this->testBatteryGrade)) {
      if (!$this->testBatteryGradeId = $this->getGrade($this->testBatteryGrade)) {
        throw new \Exception(sprintf('LDR test battery grade "%s" not found.', $this->testBatteryGrade));
      }
    }

    if (!empty($this->testBatteryProgram)) {
      if (!$this->testBatteryProgramId = $this->getProgram($this->testBatteryProgram)) {
        throw new \Exception(sprintf('LDR test battery program "%s" not found.', $this->testBatteryProgram));
      }
    }

    if (!empty($this->testBatterySecurity)) {
      if (!$this->testBatterySecurityId = $this->getSecurity($this->testBatterySecurity)) {
        throw new \Exception(sprintf('LDR test battery security "%s" not found.', $this->testBatterySecurity));
      }
    }

    if (!empty($this->testBatteryPermissions)) {
      if (!$this->testBatteryPermissionsId = $this->getPermissions($this->testBatteryPermissions)) {
        throw new \Exception(sprintf('LDR test battery permissions "%s" not found.', $this->testBatteryPermissions));
      }
    }

    if (!empty($this->testBatteryScoring)) {
      if (!$this->testBatteryScoringId = $this->getScoring($this->testBatteryScoring)) {
        throw new \Exception(sprintf('LDR test battery scoring "%s" not found.', $this->testBatteryScoring));
      }
    }

    $properties = array(
      'testBatteryName',
      'testBatteryDescription',
      'testBatterySubjectId',
      'testBatteryGradeId',
      'testBatteryProgramId',
      'testBatterySecurityId',
      'testBatteryPermissionsId',
      'testBatteryScoringId',
    );
    $this->validateRequired('test_battery', $properties);
  }

  protected function getSubject($subject) {
    return $this->getId($subject, 'test_battery_subject', 'testBatterySubjectId', 'testBatterySubject');
  }

  protected function getGrade($grade) {
    return $this->getId($grade, 'test_battery_grade', 'testBatteryGradeId', 'testBatteryGrade');
  }

  protected function getProgram($program) {
    return $this->getId($program, 'test_battery_program', 'testBatteryProgramId', 'testBatteryProgram');
  }

  protected function getSecurity($security) {
    return $this->getId($security, 'test_battery_security', 'testBatterySecurityId', 'testBatterySecurity');
  }

  protected function getPermissions($perms) {
    return $this->getId($perms, 'test_battery_permissions', 'testBatteryPermissionsId', 'testBatteryPermissions');
  }

  protected function getScoring($scoring) {
    return $this->getId($scoring, 'test_battery_scoring', 'testBatteryScoringId', 'testBatteryScoring');
  }

  public function delete() {
    // Delete assignments referencing the test.
    $sql = "UPDATE test_assignment SET deleted = ? WHERE testBatteryId = ?";
    $stmt = $this->db->prepare($sql);
    $stmt->execute(array('Y', $this->testBatteryId));

    // Delete the test.
    $sql = "UPDATE test_battery SET deleted = ? WHERE testBatteryId = ?";
    $stmt = $this->db->prepare($sql);
    $stmt->execute(array('Y', $this->testBatteryId));

    // Delete the form.
    $sql = "UPDATE test_battery_form SET deleted = ? WHERE testBatteryId = ?";
    $stmt = $this->db->prepare($sql);
    $stmt->execute(array('Y', $this->testBatteryId));
  }

  protected function createForm() {
     dbCreateTestBatteryForm($this->logId, $this->db, $this->testBatteryId,
       $this->getNextId('test_battery_form', 'testBatteryFormId'),
       $this->testBatteryName . ' Form');
  }

}
