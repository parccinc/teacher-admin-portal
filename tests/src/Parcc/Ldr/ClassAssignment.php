<?php
/**
 * @file
 * Contains \ClassAssignment.
 */

namespace Parcc\Ldr;

/**
 * Class ClassAssignment.
 *
 * @package Parcc\Ldr
 */
class ClassAssignment extends EntityBase {

  public static function load($logId, $db, $studentClassId) {

    $sql = "SELECT studentClassId FROM student_class
            WHERE studentClassId = ? AND deleted = ?";
    $stmt = $db->prepare($sql);
    $stmt->execute(array($studentClassId, 'N'));

    if ($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $assignment = self::factory($logId, $db, $data);
      return $assignment;
    }
  }

  /**
   * Saves student class assignment and populates $this->studentClassId.
   */
  public function save() {
    $this->validate();

    if (empty($this->studentClassId)) {
      $result = dbCreateStudentClass($this->logId, $this->db, 1,
        $this->classId, $this->studentRecordId);
    }
    else {
      // Doesn't seem to be LDR method to update.
      dbDeleteStudentClass($this->logId, $this->db, 1, $this->studentRecordId, $this->studentClassId);
      $result = $this->studentClassId = dbCreateStudentClass($this->logId, $this->db, 1,
        $this->classId, $this->studentRecordId);
    }
    $this->studentClassId = is_array($result) && array_key_exists('studentClassId', $result) ?
      $result['studentClassId'] : $result;
  }

  protected function validate() {
    $properties = array('classId', 'studentRecordId');
    $this->validateRequired('class_assignment', $properties);
  }

  public function delete() {
    dbDeleteStudentClass($this->logId, $this->db, 1, $this->studentRecordId, $this->studentClassId);
  }

}
