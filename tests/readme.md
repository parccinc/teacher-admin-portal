# Test

The test directory is used for behat tests.

The vendor folder is not included. Please run composer install from this directory.

Copy default-behat.yml to behat.yml and modify your environment information.

Note: This is not a directory for simpletest.

## Profiles

The default-behat.yml contains a default profile and a profile for ISBE. Profiles inherit from default, and the only entries in the ISBE profile are those that are different from the default profile (i.e. base_url). The profile looks for .feature files in the `vendors/isbe/features` folder. To run ISBE tests, use the command:
```
bin/behat --profile isbe
```

Note that to have multi-tenancy locally you need a local LDR instance that is version 1.7.7 or later. It also need to be configured to handle multiple tenants via its configuration. @Todo: link to documentation. In the meantime, may the force be with you.
