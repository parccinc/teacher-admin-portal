@api
Feature: As ISBE's rostering system
  I want to access TAP via an API
  So that I may perform rostering activities in TAP

  @ads-663 @ads-1279
  Scenario: Access Token and Basic Payload Checks
    Given I am an anonymous user
    When I call the ISBE rostering endpoint with:
    """
    abc
    """
    Then I should get a 403 HTTP response

    When I am using the ISBE services authorization token for "isbe_user"
    And I call the ISBE rostering endpoint with:
    """
    abc
    """
    Then I should get a 400 HTTP response

    When I call the ISBE rostering endpoint with:
    """
    {"batchId": 123, "batchIncrement": -1}
    """
    Then I should get a 400 HTTP response

    When I call the ISBE rostering endpoint with:
    """
    {"batchId": 123}
    """
    Then I should get a 200 HTTP response

  @ads-663 @ads-1279
  Scenario: Successfully Create Classrooms and Roster Students
    Given LDR organizations:
      | name   | type     | identifier | parent     |
      | XXD1   | district | 100        | XX         |
      | XXD1S1 | school   | 1001       | [org:XXD1] |
      | XXD1S2 | school   | 1002       | [org:XXD1] |
    And TAP users:
      | name    | pass  | mail         | roles              | organizations |
      | ta_d1s1 | behat | ta@bd1s1.edu | test administrator | [org:XXD1S1]  |
    And I am an anonymous user
    And I am using the ISBE services authorization token for "isbe_user"
    When I call the ISBE rostering endpoint with:
"""
{
  "batchId": 1,
  "batchIncrement": 987,
  "class": [{
    "organizationIdentifier": "1001",
    "classIdentifier": "Biology",
    "gradeLevel": "5",
    "sectionNumber": "03051A000"
  }, {
    "organizationIdentifier": "1001",
    "classIdentifier": "Biology - Advanced Studies",
    "gradeLevel": "5",
    "sectionNumber": "03052A000"
  }],
  "student": [{
    "enrollSchoolIdentifier": "1001",
    "stateIdentifier": "1000000",
    "lastName": "Simpson",
    "firstName": "Bart",
    "middleName": "",
    "dateOfBirth": "1979-04-01",
    "gender": "M",
    "gradeLevel": "5",
    "raceAA": "",
    "raceAN": "",
    "raceAS": "",
    "raceHL": "",
    "raceHP": "",
    "raceWH": "Y",
    "statusDIS": "",
    "statusECO": "",
    "statusELL": "",
    "statusGAT": "",
    "statusLEP": "",
    "statusMIG": "",
    "disabilityType": "",
    "optionalStateData1": "",
    "optionalStateData2": "",
    "optionalStateData3": "",
    "optionalStateData4": "",
    "optionalStateData5": "",
    "optionalStateData6": "",
    "optionalStateData7": "",
    "optionalStateData8": "",
    "classAssignment1": "03051A000",
    "classAssignment2": "",
    "classAssignment3": "",
    "classAssignment4": "",
    "classAssignment5": "",
    "classAssignment6": "",
    "classAssignment7": "",
    "classAssignment8": "",
    "classAssignment9": "",
    "classAssignment10": "",
    "actionCode": 1
  }, {
    "enrollSchoolIdentifier": "1001",
    "stateIdentifier": "1000001",
    "lastName": "Simpson",
    "firstName": "Lisa",
    "middleName": "",
    "dateOfBirth": "1981-09-28",
    "gender": "F",
    "gradeLevel": "5",
    "raceAA": "",
    "raceAN": "",
    "raceAS": "",
    "raceHL": "",
    "raceHP": "",
    "raceWH": "Y",
    "statusDIS": "",
    "statusECO": "",
    "statusELL": "",
    "statusGAT": "",
    "statusLEP": "",
    "statusMIG": "",
    "disabilityType": "",
    "optionalStateData1": "",
    "optionalStateData2": "",
    "optionalStateData3": "",
    "optionalStateData4": "",
    "optionalStateData5": "",
    "optionalStateData6": "",
    "optionalStateData7": "",
    "optionalStateData8": "",
    "classAssignment1": "03052A000",
    "classAssignment2": "",
    "classAssignment3": "",
    "classAssignment4": "",
    "classAssignment5": "",
    "classAssignment6": "",
    "classAssignment7": "",
    "classAssignment8": "",
    "classAssignment9": "",
    "classAssignment10": "",
    "actionCode": 1
  }]
}
    """
    Then I should get a 200 HTTP response

    When the "pads_isbe_services_rostering" queue has been processed
    And I am logged in as "ta_d1s1"
    And I am assigned the classes "Biology" from Organization "[org:XXD1S1]"
    And I go to "manage/[org:XXD1S1]/classes"
    Then I should see "Biology"
    And I should see "03051A000"

    When I go to "manage/[org:XXD1S1]/students"
    Then I should see "Bart" in the "1000000" row
    And I should see "Simpson" in the "1000000" row

    And the ISBE rostering confirmation message for batch 1 should be:
    """
{
    "batchId": "1",
    "batchIncrement": 0
}
    """

    And the ISBE rostering confirmation message for batch 1 increment 987 should be:
    """
{
    "batchId": "1",
    "batchIncrement": "987",
    "class": [
        {
            "organizationIdentifier": "1001",
            "classIdentifier": "Biology",
            "gradeLevel": "5",
            "sectionNumber": "03051A000",
            "recordStatusCode": 1
        },
        {
            "organizationIdentifier": "1001",
            "classIdentifier": "Biology - Advanced Studies",
            "gradeLevel": "5",
            "sectionNumber": "03052A000",
            "recordStatusCode": 1
        }
    ],
    "student": [
        {
            "enrollSchoolIdentifier": "1001",
            "stateIdentifier": "1000000",
            "lastName": "Simpson",
            "firstName": "Bart",
            "middleName": "",
            "dateOfBirth": "1979-04-01",
            "gender": "M",
            "gradeLevel": "5",
            "raceAA": "",
            "raceAN": "",
            "raceAS": "",
            "raceHL": "",
            "raceHP": "",
            "raceWH": "Y",
            "statusDIS": "",
            "statusECO": "",
            "statusELL": "",
            "statusGAT": "",
            "statusLEP": "",
            "statusMIG": "",
            "disabilityType": "",
            "optionalStateData1": "",
            "optionalStateData2": "",
            "optionalStateData3": "",
            "optionalStateData4": "",
            "optionalStateData5": "",
            "optionalStateData6": "",
            "optionalStateData7": "",
            "optionalStateData8": "",
            "classAssignment1": "03051A000",
            "classAssignment2": "",
            "classAssignment3": "",
            "classAssignment4": "",
            "classAssignment5": "",
            "classAssignment6": "",
            "classAssignment7": "",
            "classAssignment8": "",
            "classAssignment9": "",
            "classAssignment10": "",
            "actionCode": 1,
            "recordStatusCode": 1
        },
        {
            "enrollSchoolIdentifier": "1001",
            "stateIdentifier": "1000001",
            "lastName": "Simpson",
            "firstName": "Lisa",
            "middleName": "",
            "dateOfBirth": "1981-09-28",
            "gender": "F",
            "gradeLevel": "5",
            "raceAA": "",
            "raceAN": "",
            "raceAS": "",
            "raceHL": "",
            "raceHP": "",
            "raceWH": "Y",
            "statusDIS": "",
            "statusECO": "",
            "statusELL": "",
            "statusGAT": "",
            "statusLEP": "",
            "statusMIG": "",
            "disabilityType": "",
            "optionalStateData1": "",
            "optionalStateData2": "",
            "optionalStateData3": "",
            "optionalStateData4": "",
            "optionalStateData5": "",
            "optionalStateData6": "",
            "optionalStateData7": "",
            "optionalStateData8": "",
            "classAssignment1": "03052A000",
            "classAssignment2": "",
            "classAssignment3": "",
            "classAssignment4": "",
            "classAssignment5": "",
            "classAssignment6": "",
            "classAssignment7": "",
            "classAssignment8": "",
            "classAssignment9": "",
            "classAssignment10": "",
            "actionCode": 1,
            "recordStatusCode": 1
        }
    ]
}
"""

  @ads-665
  Scenario: Roster students to existing classes
    Given LDR organizations:
      | name   | type     | identifier | parent     |
      | XXD1   | district | 100        | XX         |
      | XXD1S1 | school   | 1001       | [org:XXD1] |
      | XXD1S2 | school   | 1002       | [org:XXD1] |
    And TAP users:
      | name    | pass  | mail         | roles              | organizations |
      | ta_d1s1 | behat | ta@bd1s1.edu | test administrator | [org:XXD1S1]  |
    And LDR class orgs:
      | name                       | section   | org          | grade | ref             |
      | Biology                    | 03051A000 | [org:XXD1S1] | 5     | biology         |
      | Biology - Advanced Studies | 03052A000 | [org:XXD1S1] | 5     | biologyadvanced |

    And I am an anonymous user
    And I am using the ISBE services authorization token for "isbe_user"
    When I call the ISBE rostering endpoint with:
"""
{
  "batchId": 1,
  "student": [{
    "enrollSchoolIdentifier": "1001",
    "stateIdentifier": "1000000",
    "lastName": "Simpson",
    "firstName": "Bart",
    "middleName": "",
    "dateOfBirth": "1979-04-01",
    "gender": "M",
    "gradeLevel": "5",
    "raceAA": "",
    "raceAN": "",
    "raceAS": "",
    "raceHL": "",
    "raceHP": "",
    "raceWH": "Y",
    "statusDIS": "",
    "statusECO": "",
    "statusELL": "",
    "statusGAT": "",
    "statusLEP": "",
    "statusMIG": "",
    "disabilityType": "",
    "optionalStateData1": "",
    "optionalStateData2": "",
    "optionalStateData3": "",
    "optionalStateData4": "",
    "optionalStateData5": "",
    "optionalStateData6": "",
    "optionalStateData7": "",
    "optionalStateData8": "",
    "classAssignment1": "03051A000",
    "classAssignment2": "",
    "classAssignment3": "",
    "classAssignment4": "",
    "classAssignment5": "",
    "classAssignment6": "",
    "classAssignment7": "",
    "classAssignment8": "",
    "classAssignment9": "",
    "classAssignment10": "",
    "actionCode": 1
  }, {
    "enrollSchoolIdentifier": "1001",
    "stateIdentifier": "1000001",
    "lastName": "Simpson",
    "firstName": "Lisa",
    "middleName": "",
    "dateOfBirth": "1981-09-28",
    "gender": "F",
    "gradeLevel": "5",
    "raceAA": "",
    "raceAN": "",
    "raceAS": "",
    "raceHL": "",
    "raceHP": "",
    "raceWH": "Y",
    "statusDIS": "",
    "statusECO": "",
    "statusELL": "",
    "statusGAT": "",
    "statusLEP": "",
    "statusMIG": "",
    "disabilityType": "",
    "optionalStateData1": "",
    "optionalStateData2": "",
    "optionalStateData3": "",
    "optionalStateData4": "",
    "optionalStateData5": "",
    "optionalStateData6": "",
    "optionalStateData7": "",
    "optionalStateData8": "",
    "classAssignment1": "03052A000",
    "classAssignment2": "",
    "classAssignment3": "",
    "classAssignment4": "",
    "classAssignment5": "",
    "classAssignment6": "",
    "classAssignment7": "",
    "classAssignment8": "",
    "classAssignment9": "",
    "classAssignment10": "",
    "actionCode": 1
  }]
}
    """
    Then I should get a 200 HTTP response

    When the "pads_isbe_services_rostering" queue has been processed
    And I am logged in as "ta_d1s1"
    And I am assigned classes "[class:biology]"
    And I go to "manage/[org:XXD1S1]/classes"
    Then I should see "Biology"
    And I should see "03051A000"
    When I go to "manage/[org:XXD1S1]/students"
    Then I should see "Bart" in the "1000000" row
    And I should see "Simpson" in the "1000000" row

  @ads-666 @ads-1218
  Scenario: Remove ISBE Student - Valid
    Given LDR organizations:
      | name          | type     | identifier | parent              |
      | XX District 1 | district | 1          | XX                  |
      | School XX1a   | school   | 2          | [org:XX District 1] |
    And TAP users:
      | name          | pass  | mail                | roles              | organizations     |
      | behat_tapuser | behat | tapuser@example.com | test administrator | [org:School XX1a] |
    And LDR class orgs:
      | name    | section  | org               | grade | ref     |
      | Math    | math1    | [org:School XX1a] | 3     | Math    |
      | English | english1 | [org:School XX1a] | 7     | English |
    And LDR students:
      | firstName | lastName | enrollOrgId       | dateOfBirth | gender | gradeLevel | stateIdentifier |
      | Louis     | Skolnick | [org:School XX1a] | 2001-02-03  | M      | 3          | XXD1A001        |
      | Gilbert   | Lowell   | [org:School XX1a] | 2004-05-06  | M      | 3          | XXD1A002        |
    And students "[student:Louis Skolnick]" are assigned to "[class:Math]"
    And students "[student:Gilbert Lowell]" are assigned to "[class:English]"

    And I am logged in as "behat_tapuser"
    And I am assigned classes "[class:Math],[class:English]"

    When I go to "manage/[org:School XX1a]/students"
    Then I should see "Skolnick"
    And I should see "Lowell"

    When I am using the ISBE services authorization token for "isbe_user"
    And ISBE validate student is enabled
    And I call the ISBE rostering endpoint with:
    """
{
  "batchId": 1,
  "student": [{
    "enrollSchoolIdentifier": "1",
    "stateIdentifier": "XXD1A001",
    "lastName": "Gilbert",
    "firstName": "Lowell",
    "middleName": "",
    "dateOfBirth": "2004-05-06",
    "gender": "M",
    "gradeLevel": "3",
    "raceAA": "",
    "raceAN": "",
    "raceAS": "",
    "raceHL": "",
    "raceHP": "",
    "raceWH": "Y",
    "statusDIS": "",
    "statusECO": "",
    "statusELL": "",
    "statusGAT": "",
    "statusLEP": "",
    "statusMIG": "",
    "disabilityType": "",
    "optionalStateData1": "",
    "optionalStateData2": "",
    "optionalStateData3": "",
    "optionalStateData4": "",
    "optionalStateData5": "",
    "optionalStateData6": "",
    "optionalStateData7": "",
    "optionalStateData8": "",
    "classAssignment1": "Math",
    "classAssignment2": "",
    "classAssignment3": "",
    "classAssignment4": "",
    "classAssignment5": "",
    "classAssignment6": "",
    "classAssignment7": "",
    "classAssignment8": "",
    "classAssignment9": "",
    "classAssignment10": "",
    "actionCode": 2
  }]
}
    """

    And the "pads_isbe_services_rostering" queue has been processed
    And I go to "manage/[org:School XX1a]/students"
    Then I should not see "Skolnick"
    And I should see "Lowell"
    And the ISBE rostering confirmation for student "XXD1A001" in batch 1 should have a recordStatusCode of 1

  @ads-666 @ads-1218
  Scenario: Remove ISBE Student - Invalid (test assigned)
    Given LDR organizations:
      | name          | type     | identifier | parent              |
      | XX District 1 | district | 1          | XX                  |
      | School XX1a   | school   | 2          | [org:XX District 1] |
    And TAP users:
      | name          | pass  | mail                | roles              | organizations     |
      | behat_tapuser | behat | tapuser@example.com | test administrator | [org:School XX1a] |
    And LDR class orgs:
      | name    | section  | org               | grade | ref     |
      | Math    | math1    | [org:School XX1a] | 3     | Math    |
      | English | english1 | [org:School XX1a] | 7     | English |
    And LDR students:
      | firstName | lastName | enrollOrgId       | dateOfBirth | gender | gradeLevel | stateIdentifier |
      | Louis     | Skolnick | [org:School XX1a] | 2001-02-03  | M      | 3          | XXD1A001        |
      | Gilbert   | Lowell   | [org:School XX1a] | 2004-05-06  | M      | 3          | XXD1A002        |
    And students "[student:Louis Skolnick]" are assigned to "[class:Math]"
    And students "[student:Gilbert Lowell]" are assigned to "[class:English]"

    And LDR tests:
      | name       | subject | grade | program               | security | permissions | scoring   | description                 |
      | Behat test | ELA     | 3     | Diagnostic Assessment | Secure   | Restricted  | Immediate | Test administered for Behat |
    And LDR test assignments:
      | student                  | test              | status    |
      | [student:Louis Skolnick] | [test:Behat test] | Scheduled |

    And I am logged in as "behat_tapuser"
    And I am assigned classes "[class:Math],[class:English]"

    When I go to "manage/[org:School XX1a]/students"
    Then I should see "Skolnick"
    And I should see "Lowell"

    When I am using the ISBE services authorization token for "isbe_user"
    And ISBE validate student is enabled
    And I call the ISBE rostering endpoint with:
    """
{
  "batchId": 1,
  "batchIncrement": 654,
  "student": [{
    "enrollSchoolIdentifier": "1",
    "stateIdentifier": "XXD1A001",
    "lastName": "Gilbert",
    "firstName": "Lowell",
    "middleName": "",
    "dateOfBirth": "2004-05-06",
    "gender": "M",
    "gradeLevel": "3",
    "raceAA": "",
    "raceAN": "",
    "raceAS": "",
    "raceHL": "",
    "raceHP": "",
    "raceWH": "Y",
    "statusDIS": "",
    "statusECO": "",
    "statusELL": "",
    "statusGAT": "",
    "statusLEP": "",
    "statusMIG": "",
    "disabilityType": "",
    "optionalStateData1": "",
    "optionalStateData2": "",
    "optionalStateData3": "",
    "optionalStateData4": "",
    "optionalStateData5": "",
    "optionalStateData6": "",
    "optionalStateData7": "",
    "optionalStateData8": "",
    "classAssignment1": "Math",
    "classAssignment2": "",
    "classAssignment3": "",
    "classAssignment4": "",
    "classAssignment5": "",
    "classAssignment6": "",
    "classAssignment7": "",
    "classAssignment8": "",
    "classAssignment9": "",
    "classAssignment10": "",
    "actionCode": 2
  },
  {
    "enrollSchoolIdentifier": "1",
    "stateIdentifier": "XXD1INVALID",
    "lastName": "Lisa",
    "firstName": "Simpson",
    "middleName": "",
    "dateOfBirth": "2004-03-02",
    "gender": "F",
    "gradeLevel": "3",
    "raceAA": "",
    "raceAN": "",
    "raceAS": "",
    "raceHL": "",
    "raceHP": "",
    "raceWH": "Y",
    "statusDIS": "",
    "statusECO": "",
    "statusELL": "",
    "statusGAT": "",
    "statusLEP": "",
    "statusMIG": "",
    "disabilityType": "",
    "optionalStateData1": "",
    "optionalStateData2": "",
    "optionalStateData3": "",
    "optionalStateData4": "",
    "optionalStateData5": "",
    "optionalStateData6": "",
    "optionalStateData7": "",
    "optionalStateData8": "",
    "classAssignment1": "Math",
    "classAssignment2": "",
    "classAssignment3": "",
    "classAssignment4": "",
    "classAssignment5": "",
    "classAssignment6": "",
    "classAssignment7": "",
    "classAssignment8": "",
    "classAssignment9": "",
    "classAssignment10": "",
    "actionCode": 2
  }]
}
    """

    And the "pads_isbe_services_rostering" queue has been processed
    And I go to "manage/[org:School XX1a]/students"
    Then I should see "Skolnick"
    And I should see "Lowell"
    And the ISBE rostering confirmation for student "XXD1A001" in batch 1 increment 654 should have a recordStatusCode of 2
    And the ISBE rostering confirmation for student "XXD1A001" in batch 1 increment 654 should have a recordStatusDescription of "Student record XXD1A001 already has an active test assignment for test battery [test:Behat test]"
    And the ISBE rostering confirmation for student "XXD1INVALID" in batch 1 increment 654 should have a recordStatusCode of 2
    And the ISBE rostering confirmation for student "XXD1INVALID" in batch 1 increment 654 should have a recordStatusDescription of "Student record not found for stateIdentifier XXD1INVALID"

  @counts @roster
  Scenario: Student count updates after roster
    Given LDR organizations:
      | name   | type     | identifier | parent     |
      | XXD1   | district | 100        | XX         |
      | XXD1S1 | school   | 1001       | [org:XXD1] |
    And TAP users:
      | name    | pass  | mail         | roles                      | organizations |
      | ta_d1s1 | behat | ta@bd1s1.edu | test administrator         | [org:XXD1S1]  |
      | oa_d1s1 | behat | oa@bd1s1.edu | organization administrator | [org:XXD1S1]  |
    And LDR class orgs:
      | name                       | section   | org          | grade | ref     |
      | Biology                    | 03051A000 | [org:XXD1S1] | 5     | biology |
      | Biology - Advanced Studies | 03052A000 | [org:XXD1S1] | 5     | advbio  |
    And LDR students:
      | firstName | lastName | enrollOrgId  | dateOfBirth | gender | gradeLevel | stateIdentifier | classes         |
      | Louis     | Skolnick | [org:XXD1S1] | 2005-01-01  | F      | 3          | IL4560001       | [class:biology] |
      | Gilbert   | Lowell   | [org:XXD1S1] | 2005-02-02  | M      | 3          | IL4560002       | [class:biology] |
      | Jeff      | Spicoli  | [org:XXD1S1] | 1980-01-01  | M      | 7          | IL4560003       | [class:advbio]  |

    #Org admin sees the 3 students in their org
    When I am logged in as "oa_d1s1"
    And I am on the homepage
    Then I should see "3" in the "#students-[org:XXD1S1]-count" element

    #Test admin sees 2 students in their class
    When I am logged in as "ta_d1s1"
    And I am assigned classes "[class:biology]"
    And I am on the homepage
    Then I should see "2" in the "#students-[org:XXD1S1]-count" element

    When I am using the ISBE services authorization token for "isbe_user"
    And I call the ISBE rostering endpoint with:
    """
{
  "batchId": 1,
  "batchIncrement": 100,
  "student": [
        {
            "enrollSchoolIdentifier": "1001",
            "stateIdentifier": "1000000",
            "lastName": "Simpson",
            "firstName": "Bart",
            "middleName": "",
            "dateOfBirth": "1979-04-01",
            "gender": "M",
            "gradeLevel": "5",
            "raceAA": "",
            "raceAN": "",
            "raceAS": "",
            "raceHL": "",
            "raceHP": "",
            "raceWH": "Y",
            "statusDIS": "",
            "statusECO": "",
            "statusELL": "",
            "statusGAT": "",
            "statusLEP": "",
            "statusMIG": "",
            "disabilityType": "",
            "optionalStateData1": "",
            "optionalStateData2": "",
            "optionalStateData3": "",
            "optionalStateData4": "",
            "optionalStateData5": "",
            "optionalStateData6": "",
            "optionalStateData7": "",
            "optionalStateData8": "",
            "classAssignment1": "03051A000",
            "classAssignment2": "",
            "classAssignment3": "",
            "classAssignment4": "",
            "classAssignment5": "",
            "classAssignment6": "",
            "classAssignment7": "",
            "classAssignment8": "",
            "classAssignment9": "",
            "classAssignment10": "",
            "actionCode": 1,
            "recordStatusCode": 1
        },
        {
            "enrollSchoolIdentifier": "1001",
            "stateIdentifier": "1000001",
            "lastName": "Simpson",
            "firstName": "Lisa",
            "middleName": "",
            "dateOfBirth": "1981-09-28",
            "gender": "F",
            "gradeLevel": "5",
            "raceAA": "",
            "raceAN": "",
            "raceAS": "",
            "raceHL": "",
            "raceHP": "",
            "raceWH": "Y",
            "statusDIS": "",
            "statusECO": "",
            "statusELL": "",
            "statusGAT": "",
            "statusLEP": "",
            "statusMIG": "",
            "disabilityType": "",
            "optionalStateData1": "",
            "optionalStateData2": "",
            "optionalStateData3": "",
            "optionalStateData4": "",
            "optionalStateData5": "",
            "optionalStateData6": "",
            "optionalStateData7": "",
            "optionalStateData8": "",
            "classAssignment1": "03052A000",
            "classAssignment2": "",
            "classAssignment3": "",
            "classAssignment4": "",
            "classAssignment5": "",
            "classAssignment6": "",
            "classAssignment7": "",
            "classAssignment8": "",
            "classAssignment9": "",
            "classAssignment10": "",
            "actionCode": 1,
            "recordStatusCode": 1
        }
    ]
}
"""
    When the "pads_isbe_services_rostering" queue has been processed

    #Org admin sees 5 students in org
    And I am logged in as "oa_d1s1"
    And I am on the homepage
    Then I should see "5" in the "#students-[org:XXD1S1]-count" element

    #Test admin sees 3 students in classes
    When I am logged in as "ta_d1s1"
    And I am assigned classes "[class:biology]"
    And I am on the homepage
    Then I should see "3" in the "#students-[org:XXD1S1]-count" element
