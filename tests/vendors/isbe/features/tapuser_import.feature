@api @tapuser @tapuser_import @import
Feature: TAP User import features.

  Background:
    Given LDR organizations:
      | name   | type     | identifier      | parent     |
      | XXD1   | district | 050160670020000 | XX         |
      | XXD1S1 | school   | 050160670021002 | [org:XXD1] |
    Given TAP users:
      | name                | pass  | mail                | roles                      | organizations |
      | Sir Patrick Stewart | behat | engage@makeitso.org | organization administrator | [org:XX]      |


  @ads-1535 @javascript
  Scenario: bad character import
    Given I am logged in as "Sir Patrick Stewart"
    When I import the file "tapuser_import_withinvalidchars.csv" at "tapusers/import"
    Then I should see "Malformed UTF-8 characters, possibly incorrectly encoded" in the "#table_import_errors tbody tr:nth-of-type(1) td:nth-of-type(7)" element
    And I should see "Malformed UTF-8 characters, possibly incorrectly encoded" in the "#table_import_errors tbody tr:nth-of-type(2) td:nth-of-type(7)" element
    And I should see "Malformed UTF-8 characters, possibly incorrectly encoded" in the "#table_import_errors tbody tr:nth-of-type(3) td:nth-of-type(7)" element
    And I should see "Malformed UTF-8 characters, possibly incorrectly encoded" in the "#table_import_errors tbody tr:nth-of-type(4) td:nth-of-type(7)" element
    And I should see "Organization with identifier 050160700000000 in XX not found." in the "#table_import_errors tbody tr:nth-of-type(5) td:nth-of-type(7)" element
    And I should see "Organization with identifier 050160700000000 in XX not found." in the "#table_import_errors tbody tr:nth-of-type(6) td:nth-of-type(7)" element
