@api @tests @assignments
Feature: Scenarios around test assignment functionality
  As a Test Administrator,
  I want to be able to select accommodations for the students in my class for the test I just assigned to them.

  Background:
    Given LDR organizations:
      | name   | type     | identifier | parent     |
      | XXD1   | district | 1          | XX         |
      | XXD1S1 | school   | 2          | [org:XXD1] |
    And LDR class orgs:
      | name             | section | org          | grade | ref        |
      | Science          | 101     | [org:XXD1S1] | 8     | Science    |
      | Advanced Science | 101     | [org:XXD1S1] | 8     | AdvScience |
    And LDR tests:
      | name                                | subject | grade | program               | security | permissions    | scoring   | description                 |
      | ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3  | ELA     | 8     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Test administered for Behat |
      | Math_Comp - MC.8.F.B.3 - MC.8.F.B.3 | Math    | 8     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Test administered for Behat |

    And LDR students:
      | firstName | lastName | enrollOrgId  | dateOfBirth | gender | gradeLevel | stateIdentifier | classes                            |
      | Louis     | Skolnick | [org:XXD1S1] | 2001-02-03  | M      | 3          | XXD1A001        | [class:Science]                    |
      | Lowell    | Gilbert  | [org:XXD1S1] | 2004-05-06  | M      | 3          | XXD1A002        | [class:Science]                    |
      | Sloth     | Fratelli | [org:XXD1S1] | 2001-02-03  | M      | 3          | XXD1A003        | [class:Science],[class:AdvScience] |
      | Mikey     | Astin    | [org:XXD1S1] | 2004-05-06  | M      | 3          | XXD1A004        | [class:AdvScience]                 |
      | Mouth     | Feldman  | [org:XXD1S1] | 2004-05-06  | M      | 3          | XXD1A005        | [class:AdvScience]                 |

    #Note that test assignments are created automatically when a student is
    #assigned to a class. The test battery grade must match the class grade,
    #and Ldr's tenant config file must have the following:
    #define('TEST_ASSIGNMENT_LINE_READER_DEFAULT', 'Y');
    #define('TEST_ASSIGNMENT_TTS_DEFAULT', 'Y');
    #define('AUTO_TEST_ASSIGNMENT', true);

    And TAP users:
      | name      | pass  | mail                  | roles                      | organizations |
      | testadmin | behat | testadmin@example.com | test administrator         | [org:XXD1S1]  |
      | orgadmin  | behat | orgadmin@example.com  | organization administrator | [org:XXD1S1]  |


  @ads-1527
  Scenario: same class info in different chools
    Given LDR organizations:
      | name   | type   | identifier | parent     |
      | XXD1S2 | school | 3          | [org:XXD1] |
      | XXD1S3 | school | 4          | [org:XXD1] |
    And LDR class orgs:
      | name    | section | org          | grade | ref      |
      | Science | 101     | [org:XXD1S2] | 8     | Science2 |
      | Science | 101     | [org:XXD1S3] | 8     | Science3 |
    And LDR students:
      | firstName | lastName | enrollOrgId  | dateOfBirth | gender | gradeLevel | stateIdentifier | classes          |
      | Bart      | Simpson  | [org:XXD1S2] | 2004-05-06  | M      | 8          | XXD1A006        | [class:Science2] |
      | Lisa      | Simpson  | [org:XXD1S3] | 2004-05-06  | M      | 8          | XXD1A007        | [class:Science3] |
    And I am logged in as a user with the "test administrator" role
    And I am assigned the organization "[org:XXD1S1],[org:XXD1S2],[org:XXD1S3]"
    And I am assigned classes "[class:Science],[class:AdvScience],[class:Science2],[class:Science3]"
    And I am on "manage/[org:XXD1S1]/tests/assignments"
    Then the "For class" select element should contain options "Advanced Science - 101,Science - 101"

    When I select "XXD1S2" from "Showing test assignments for"
    And I press "Go"
    Then the "For class" select element should contain options "Science - 101"
    And the "#datatable-1 > caption:nth-child(1)" element should contain "Class: Science - 101"
    And I should not see the text "There are no classes assigned to you"

    When I select "XXD1S3" from "Showing test assignments for"
    And I press "Go"
    Then the "For class" select element should contain options "Science - 101"
    And the "#datatable-1 > caption:nth-child(1)" element should contain "Class: Science - 101"
    And I should not see the text "There are no classes assigned to you"

  @ads-1359 @javascript
  Scenario: All classes displayed on the page
    Given I am logged in as "testadmin"
    And I am assigned classes "[class:Science]"
    And I am on the homepage
    And I click "My Students"
    And I wait 1 seconds
    And I click "View/Edit Test Assignments" in the "Skolnick" row
    When I click the "ul#ddb--2 li.first a" element
    And I wait for AJAX to finish

    #accomodations are checked and disabled.
    Then I see the 'input[name="accommodations[text_to_speech]"]' element with the "disabled" attribute set to "disabled" in the modal
    And I see the 'input[name="accommodations[text_to_speech]"]' element with the "checked" attribute set to "checked" in the modal

    And I see the 'input[name="accommodations[line_reader]"]' element with the "disabled" attribute set to "disabled" in the modal
    And I see the 'input[name="accommodations[line_reader]"]' element with the "checked" attribute set to "checked" in the modal

    #test assignment accommodation checkboxes also checked and disabled
    When I visit "manage/[org:XXD1S1]/tests/assignments"
    Then I wait 2 seconds

    Then I see the "ELA_Comp - MC.8.F.B.3" test assigned to "Louis Skolnick" has the "enableLineReader" accommodation set
    And I see the "ELA_Comp - MC.8.F.B.3" test assigned to "Louis Skolnick" has the "enableLineReader" accommodation disabled
    And I see the "ELA_Comp - MC.8.F.B.3" test assigned to "Louis Skolnick" has the "enableTextToSpeech" accommodation set
    And I see the "ELA_Comp - MC.8.F.B.3" test assigned to "Louis Skolnick" has the "enableTextToSpeech" accommodation disabled

    And I see the "Math_Comp - MC.8.F.B.3" test assigned to "Louis Skolnick" has the "enableLineReader" accommodation set
    And I see the "Math_Comp - MC.8.F.B.3" test assigned to "Louis Skolnick" has the "enableLineReader" accommodation disabled
    And I see the "Math_Comp - MC.8.F.B.3" test assigned to "Louis Skolnick" has the "enableTextToSpeech" accommodation set
    And I see the "Math_Comp - MC.8.F.B.3" test assigned to "Louis Skolnick" has the "enableTextToSpeech" accommodation disabled

    And I see the "ELA_Comp - MC.8.F.B.3" test assigned to "Lowell Gilbert" has the "enableLineReader" accommodation set
    And I see the "ELA_Comp - MC.8.F.B.3" test assigned to "Lowell Gilbert" has the "enableLineReader" accommodation disabled
    And I see the "ELA_Comp - MC.8.F.B.3" test assigned to "Lowell Gilbert" has the "enableTextToSpeech" accommodation set
    And I see the "ELA_Comp - MC.8.F.B.3" test assigned to "Lowell Gilbert" has the "enableTextToSpeech" accommodation disabled

    And I see the "Math_Comp - MC.8.F.B.3" test assigned to "Lowell Gilbert" has the "enableLineReader" accommodation set
    And I see the "Math_Comp - MC.8.F.B.3" test assigned to "Lowell Gilbert" has the "enableLineReader" accommodation disabled
    And I see the "Math_Comp - MC.8.F.B.3" test assigned to "Lowell Gilbert" has the "enableTextToSpeech" accommodation set
    And I see the "Math_Comp - MC.8.F.B.3" test assigned to "Lowell Gilbert" has the "enableTextToSpeech" accommodation disabled

    And I see the "ELA_Comp - MC.8.F.B.3" test assigned to "Sloth Fratelli" has the "enableLineReader" accommodation set
    And I see the "ELA_Comp - MC.8.F.B.3" test assigned to "Sloth Fratelli" has the "enableLineReader" accommodation disabled
    And I see the "ELA_Comp - MC.8.F.B.3" test assigned to "Sloth Fratelli" has the "enableTextToSpeech" accommodation set
    And I see the "ELA_Comp - MC.8.F.B.3" test assigned to "Sloth Fratelli" has the "enableTextToSpeech" accommodation disabled

    And I see the "Math_Comp - MC.8.F.B.3" test assigned to "Sloth Fratelli" has the "enableLineReader" accommodation set
    And I see the "Math_Comp - MC.8.F.B.3" test assigned to "Sloth Fratelli" has the "enableLineReader" accommodation disabled
    And I see the "Math_Comp - MC.8.F.B.3" test assigned to "Sloth Fratelli" has the "enableTextToSpeech" accommodation set
    And I see the "Math_Comp - MC.8.F.B.3" test assigned to "Sloth Fratelli" has the "enableTextToSpeech" accommodation disabled
