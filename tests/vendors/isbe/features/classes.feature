@api @classes
Feature: Scenarios around class behavior

  Background:
    Given LDR organizations:
      | name   | type     | identifier | parent     |
      | XXD1   | district | 1          | XX         |
      | XXD1S1 | school   | 2          | [org:XXD1] |

    And LDR class orgs:
      | name    | section            | org          | grade | ref      |
      | Biology | Biology - 14201F11 | [org:XXD1S1] | 9     | 14201F11 |
      | Biology | Biology - 14201L1  | [org:XXD1S1] | 9     | 14201L1  |
      | Biology | Biology - 14201N2  | [org:XXD1S1] | 9     | 14201N2  |
      | Biology | Biology - 14202C1  | [org:XXD1S1] | 9     | 14202C1  |
      | Biology | Biology - 14202C2  | [org:XXD1S1] | 9     | 14202C2  |
      | Biology | Biology - 14202C3  | [org:XXD1S1] | 9     | 14202C3  |
      | Biology | Biology - 14202F1  | [org:XXD1S1] | 9     | 14202F1  |
      | Biology | Biology - 14202F10 | [org:XXD1S1] | 9     | 14202F10 |
      | Biology | Biology - 14202F11 | [org:XXD1S1] | 9     | 14202F11 |
      | Biology | Biology - 14202F12 | [org:XXD1S1] | 9     | 14202F12 |
      | Biology | Biology - 14202F13 | [org:XXD1S1] | 9     | 14202F13 |
      | Biology | Biology - 14202F2  | [org:XXD1S1] | 9     | 14202F2  |
      | Biology | Biology - 14202F3  | [org:XXD1S1] | 9     | 14202F3  |
      | Biology | Biology - 14202F4  | [org:XXD1S1] | 9     | 14202F4  |
      | Biology | Biology - 14202F5  | [org:XXD1S1] | 9     | 14202F5  |
      | Biology | Biology - 14202F6  | [org:XXD1S1] | 9     | 14202F6  |
      | Biology | Biology - 14202F7  | [org:XXD1S1] | 9     | 14202F7  |
      | Biology | Biology - 14202F8  | [org:XXD1S1] | 9     | 14202F8  |
      | Biology | Biology - 14202F9  | [org:XXD1S1] | 9     | 14202F9  |
      | Biology | Biology - 14202H1  | [org:XXD1S1] | 12    | 14202H1  |
      | Biology | Biology - 14202L1  | [org:XXD1S1] | 9     | 14202L1  |
      | Biology | Biology - 14202N1  | [org:XXD1S1] | 10    | 14202N1  |
      | Biology | Biology - 14202N2  | [org:XXD1S1] | 10    | 14202N2  |
      | Biology | Biology - 14202N3  | [org:XXD1S1] | 9     | 14202N3  |
      | Biology | Biology - 14205F1  | [org:XXD1S1] | 9     | 14205F1  |
      | Biology | Biology - 14205F2  | [org:XXD1S1] | 9     | 14205F2  |
      | Biology | Biology - 14205F3  | [org:XXD1S1] | 9     | 14205F3  |
      | Biology | Biology - 14205F4  | [org:XXD1S1] | 9     | 14205F4  |
      | Biology | Biology - 315021   | [org:XXD1S1] | 9     | 315021   |
      | Biology | Biology - 315022   | [org:XXD1S1] | 9     | 315022   |

    And TAP users:
      | name                  | pass  | mail                     | roles                      | organizations |
      | mr_hand               | hand  | notinmyclass@example.com | test administrator         | [org:XXD1S1]  |
      | behat_school_orgadmin | behat | orgadmin@example.com     | organization administrator | [org:XXD1S1]  |


  @ads-1421 @datatables @javascript
  Scenario: Javascript error on class page.
      Given I am logged in as "behat_school_orgadmin"
      And I am at "classes/[org:XXD1S1]"

      #Use to fail due to unexpected modal dialog.
      When I page "next" on the "datatable-1" datatable
      Then I should see "Showing 11 to 20 of 30 entries"