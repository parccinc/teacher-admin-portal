@api @students
Feature: PADS student functionality - ISBE Customization

  Background:
    Given LDR organizations:
      | name   | type     | identifier | parent     |
      | XXD1   | district | 1          | XX         |
      | XXD1S1 | school   | 2          | [org:XXD1] |
      | XXD1S2 | school   | 3          | [org:XXD1] |
    And TAP users:
      | name                       | pass  | mail                       | roles                      | organizations             |
      | behat_testadmin            | behat | testadmin@example.com      | test administrator         | [org:XXD1S1]              |
      | mr_hand                    | hand  | notinmyclass@example.com   | test administrator         | [org:XXD1S1],[org:XXD1S2] |
      | behat_school_orgadmin      | behat | orgadmin@example.com       | organization administrator | [org:XXD1S1]              |
      | behat_multischool_orgadmin | behat | orgadmin_multi@example.com | organization administrator | [org:XXD1S1],[org:XXD1S2] |
    And LDR class orgs:
      | name    | section | org          | grade | ref  |
      | Math    | 101     | [org:XXD1S1] | 3     | math |
      | English | 201     | [org:XXD1S1] | 4     | eng  |
      | History | 301     | [org:XXD1S1] | 3     | his  |
      | Science | 401     | [org:XXD1S2] | 3     | sci  |
      | Gym     | 501     | [org:XXD1S2] | 3     | gym  |
    And LDR students:
      | firstName | lastName | enrollOrgId  | dateOfBirth | gender | gradeLevel | stateIdentifier | classes      |
      | Louis     | Skolnick | [org:XXD1S1] | 2005-01-01  | F      | 3          | IL4560001       | [class:math] |
      | Gilbert   | Lowell   | [org:XXD1S1] | 2005-02-02  | M      | 3          | IL4560002       | [class:eng]  |
      | Jeff      | Spicoli  | [org:XXD1S2] | 1980-01-01  | M      | 7          | IL4560003       | [class:sci]  |

  @ads-1380
  Scenario: PARCC District Org Admin
    # Dashboard, navigation item and student card with import action link.
    And I am logged in as a user with the "organization administrator" role
    And I am assigned the organization "[org:XXD1]"
    When I am on the homepage
    Then I should see the link "Students" in the "main_menu" region
    And I should not see a "div.students .card-actions a" element

    # Students page.
    When I click "Students" in the "main_menu" region
    # Add and upload buttons available.
    Then I should see the link "Add Single Student"
    And I should not see the link "Upload Students"
    # Filtering by school (default).
    And I should see the link "IL4560001"
    And I should see the link "IL4560002"
    And I should not see the link "IL4560003"
    # Filtering by school (select other option).
    When I select "XXD1S2" from "Showing students for"
    And I press "Go"
    Then I should not see the link "IL4560001"
    And I should not see the link "IL4560002"
    And I should see the link "IL4560003"
    # Able to add students.
    When I go to "student/add/4"
    Then I should get a 200 HTTP response
    # Able to view and edit all students in district.
    When I go to "student/[student:Louis Skolnick]"
    Then I should get a 200 HTTP response
    When I go to "student/[student:Louis Skolnick]/edit"
    Then I should get a 200 HTTP response
    When I go to "student/[student:Gilbert Lowell]"
    Then I should get a 200 HTTP response
    When I go to "student/[student:Gilbert Lowell]/edit"
    Then I should get a 200 HTTP response
    When I go to "student/[student:Jeff Spicoli]"
    Then I should get a 200 HTTP response
    When I go to "student/[student:Jeff Spicoli]/edit"
    Then I should get a 200 HTTP response
    # Access student import page.
    When I go to "students/import"
    Then I should get a 403 HTTP response

    # Schools Page, check that student counts are linked.
    When I click "Schools" in the "main_menu" region
    And I click 4 in the XXD1S1 row
    Then I am on "students/[org:XXD1S1]"
    When I click "Schools" in the "main_menu" region
    And I click 1 in the XXD1S2 row
    Then I am on "students/[org:XXD1S2]"
