@api
Feature: Scenarios relating to ISBE's ui in iframe.

  Background:
    Given LDR organizations:
      | name   | type     | identifier | parent     |
      | XXD1   | district | 100        | XX         |
      | XXD1S1 | school   | 060160860020000       | [org:XXD1] |
      | XXD1S2 | school   | 560990810020000       | [org:XXD1] |

  @ads-664
  Scenario Outline: Call to action on dashboard
    Given I am logged in as a user with the "<role>" role
    And I am assigned the organization "<org>"
    When I am on the homepage
    Then I <to see or not to see> see the text "To manage students, classes, and tests please press the button below"
    And I <to see or not to see> see the link "ISBE Management"
    Examples:
      | role                       | org          | to see or not to see |
      | organization administrator | [org:XX]     | should not           |
      | organization administrator | [org:XXD1]   | should               |
      | organization administrator | [org:XXD1S1] | should               |
      | test administrator         | [org:XXD1S1] | should not           |

  @ads-664 @ads-1360
  Scenario: User CHARPER@ROE46.NET assigned org identifier 1008 clicks isbe button.
    Given TAP users:
      | name    | pass  | mail              | roles                      | organizations |
      | charper | behat | CHARPER@ROE46.NET | organization administrator | [org:XXD1S1]  |
    And I am logged in as "charper"
    And I am on the homepage
    When I click "ISBE Management"
    Then I should be on "isbe/management/nojs"
    And I should see "ISBE Management" in the "#page-title" element
    And the decrypted query string should contain "060160860020000" for "EntityId"
    And the iframe source is set to ISBE Management

  @ads-1360 @javascript
  Scenario: Verify ISBE Managament modal opens
    Given TAP users:
      | name    | pass  | mail              | roles                      | organizations |
      | charper | behat | CHARPER@ROE46.NET | organization administrator | [org:XXD1S1]  |
    And I am logged in as "charper"
    And I am on the homepage
    When I click "ISBE Management"
    And I wait for AJAX to finish
    Then I should see "ISBE Management" in the "#modal-title" element
    And I should see an "#modal-content iframe" element
    And the iframe source is set to ISBE Management

  @ads-1302
  Scenario Outline: No import classes and students for org admins
    Given I am logged in as a user with the "<role>" role
    And I am assigned the organization "<org>"
    When I am on the homepage
    Then I should not see the link "Import Classes"
    And I should not see the link "Import Students"

    When I go to "students/import"
    Then I should get a 403 HTTP response

    When I go to "class/import"
    Then I should get a 403 HTTP response

    Examples:
      | role                       | org          |
      | organization administrator | [org:XXD1]   |
      | organization administrator | [org:XXD1S1] |

  @ads-1290
  Scenario: Add Upload Students button
    Given TAP users:
      | name    | pass  | mail         | roles                      | organizations |
      | ta_d1s1 | behat | ta@bd1s1.edu | test administrator         | [org:XXD1S1]  |
      | oa_d1s1 | behat | oa@bd1s1.edu | organization administrator | [org:XXD1S1]  |
    And LDR class orgs:
      | name                       | section   | org          | grade | ref             |
      | Biology                    | 03051A000 | [org:XXD1S1] | 5     | biology         |
      | Biology - Advanced Studies | 03052A000 | [org:XXD1S1] | 5     | biologyadvanced |
    And I am logged in as "ta_d1s1"
    And I am assigned classes "[class:biology]"
    And I am on the homepage
    When I click "My Students"
    Then I should not see the link "Upload Students"

    When I am logged in as "oa_d1s1"
    And I am on the homepage
    When I click "Students"
    Then I should not see the link "Upload Students"

  @ads-1291
  Scenario: Add Upload Classes button
    Given TAP users:
      | name    | pass  | mail         | roles                      | organizations |
      | ta_d1s1 | behat | ta@bd1s1.edu | test administrator         | [org:XXD1S1]  |
      | oa_d1s1 | behat | oa@bd1s1.edu | organization administrator | [org:XXD1S1]  |
    And LDR class orgs:
      | name                       | section   | org          | grade | ref             |
      | Biology                    | 03051A000 | [org:XXD1S1] | 5     | biology         |
      | Biology - Advanced Studies | 03052A000 | [org:XXD1S1] | 5     | biologyadvanced |
    And I am logged in as "oa_d1s1"

    And I am on the homepage
    When I click "My Classes"
    Then I should not see the link "Upload Classes"

  @ads-1362
  Scenario: Dashboard for school level users
    Given TAP users:
      | name    | pass  | mail         | roles                      | organizations |
      | ta_d1s1 | behat | ta@bd1s1.edu | test administrator         | [org:XXD1S1]  |
      | oa_d1s1 | behat | oa@bd1s1.edu | organization administrator | [org:XXD1S1]  |
    And LDR class orgs:
      | name                       | section   | org          | grade | ref             |
      | Biology                    | 03051A000 | [org:XXD1S1] | 5     | biology         |
      | Biology - Advanced Studies | 03052A000 | [org:XXD1S1] | 5     | biologyadvanced |
    Given I am logged in as "oa_d1s1"
    When I am on the homepage

    # I see the user block.
    Then I should see "Users" in the "div.users .card-title h6" element
    And I should see the link "user-[org:XXD1S1]-count"
    And I should see the link "Import Users"

    # I see the student block.
    When I am on the homepage
    Then I should see "Students" in the "div.students .card-title h6" element
    And I should see the link "students-[org:XXD1S1]-count"
    And I should not see the link "Import Students"
    #Correct student link.
    Then I see the "#mini-panel-dashboard_cards a[title='Students']" element with the "href" attribute set to "students/4" in the content

    # I see the classes block.
    When I am on the homepage
    And I should see "Classes" in the "div.classes .card-title h6" element
    And I should see an "#classes-[org:XXD1S1]-count" element
    And I should not see the link "Import Classes"

    #Test administrator sees correct student link.
    When I am logged in as "ta_d1s1"
    And I am on the homepage
    Then I see the "#mini-panel-dashboard_cards a[title='Students']" element with the "href" attribute set to "manage/4/students" in the content

