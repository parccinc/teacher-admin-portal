@api @students
Feature: As an ISBE Test Administrator
  I want to perform various operations on students in ADS
  So that student data is prepared for test taking

  Background:
    Given LDR organizations:
      | name   | type     | identifier | parent     |
      | XXD1   | district | 1          | XX         |
      | XXD1S1 | school   | SCHOOL2    | [org:XXD1] |

    And TAP users:
      | name                 | pass  | mail                      | roles                      | organizations |
      | John Kimball         | behat | itsnotatumor@1234.sd2.org | test administrator         | [org:XXD1S1]  |
      | Sir Patrick Stewart  | behat | makeitso@engage.net       | organization administrator | [org:XXD1]    |
      | Sir Patrick Stewart2 | behat | makeitso2@engage.net      | organization administrator | [org:XXD1S1]  |

    And LDR class orgs:
      | name                 | section       | org          | grade | ref      |
      | Discrete Mathematics | a-[timestamp] | [org:XXD1S1] | 2     | math     |
      | Integral Calculus    | b-[timestamp] | [org:XXD1S1] | 2     | calculus |
      | Dinosaur Roaring     | c-[timestamp] | [org:XXD1S1] | 1     | dinosaur |
      | Hair Braiding        | d-[timestamp] | [org:XXD1S1] | 1     | braiding |

  @ads-1508 @ads-1599
  Scenario Outline: Org admin has class dropdown when adding student
    Given I am logged in as "<name>"
    And I am at "students/[org:XXD1S1]"
    When I click "Add Single Student"
    Then the "classAssignment1" select element should contain options "- Select -,Dinosaur Roaring - c-[timestamp],Discrete Mathematics - a-[timestamp],Hair Braiding - d-[timestamp],Integral Calculus - b-[timestamp]"
    And I see the '#edit-classassignment1' element with the "required" attribute set to "required" in the content

    When I fill in the following:
      | Grade            | 7          |
      | First Name       | Ora        |
      | Last Name        | Schultz    |
      | Date             | 2003-03-08 |
      | State Student ID | Y2PHZE4P   |
    And I select the radio button "Female"
    And I select "Dinosaur Roaring - c-[timestamp]" from "classAssignment1"
    And I press "Save Student Data"
    Then I should see the success message "You have successfully added Ora Schultz"
    But I should not see the success message "Click here to assign them to a class"
    And student with stateIdentifier "Y2PHZE4P_SCHOOL2" is assigned to class "[class:dinosaur]"

    #No assign test operation in student table
    When I click "Students"
    Then I should not see "Assign Test" in the "Y2PHZE4P_SCHOOL2" row
    Examples:
      | name                 |
      | Sir Patrick Stewart  |
      | Sir Patrick Stewart2 |

  @ads-1158
  Scenario: Create a Student
    Given ISBE services debug is enabled
    And I am logged in as "John Kimball"
    And I am assigned classes "[class:dinosaur]"

    # Ensure composite ID generation does not interfere with requiredness of the
    # "State Student ID" field.
    When I go to "manage/[org:XXD1S1]/classes"
    And I click "Add Student"
    And I fill in the following:
      | Grade      | 7          |
      | First Name | Ora        |
      | Last Name  | Schultz    |
      | Date       | 2003-03-08 |
    And I select the radio button "Female"
    And I press "Save Student Data"
    Then I should see the error message "Required value stateIdentifier not provided"

    # Successful submission, check that the composite ID was generated.
    When I fill in "Y2PHZE4P" for "State Student ID"
    And I press "Save Student Data"
    Then I should see the success message "You have successfully added Ora Schultz"
    When I go to "manage/[org:XXD1S1]/students"
    Then I should see "Y2PHZE4P_SCHOOL2"
    And the ISBE BTAddTestingStudents request for "Y2PHZE4P_SCHOOL2" contains "classAssignment1" of "c-[timestamp]"

    # Edit the student and ensure that the composite ID was not computed again.
    When I click "Y2PHZE4P_SCHOOL2"
    And I click "Edit Student"
    And I fill in "Edna" for "First Name"
    And I press "Save Student Data"
    Then I should see the success message "Y2PHZE4P_SCHOOL2 has been updated."
    When I go to "manage/[org:XXD1S1]/students"
    Then I should see "Y2PHZE4P_SCHOOL2"
    And I should not see "Y2PHZE4P_SCHOOL2_SCHOOL2"

  @ads-1181 @ads-1508
  Scenario: Mesage when test admin adds student
    Given LDR students:
      | firstName | lastName | enrollOrgId  | dateOfBirth | gender | gradeLevel | stateIdentifier | classes          |
      | Louis     | Skolnick | [org:XXD1S1] | 2005-01-01  | F      | 3          | IL4560001       | [class:dinosaur] |
      | Gilbert   | Lowell   | [org:XXD1S1] | 2005-02-02  | M      | 3          | IL4560002       | [class:dinosaur] |
    And I am logged in as "John Kimball"
    And I am assigned classes "[class:dinosaur]"
    And I am at "manage/[org:XXD1S1]/classes"
    When I click "Add Student"
    Then I should not see an "#edit-classassignment1" element
    When I fill in the following:
      | Grade            | 7          |
      | First Name       | Ora        |
      | Last Name        | Schultz    |
      | Date             | 2003-03-08 |
      | State Student ID | Y2PHZE4P   |
    And I select the radio button "Female"
    When I press "Save Student Data"
    Then I should see the success message "You have successfully added Ora Schultz"
    But I should not see the success message "Click here to assign them to a test"

    #No assign test operation in class table
    And I should not see "Assign Test" in the "Dinosaur Roaring" row

    #No assign test operation in student table
    When I click "Students"
    Then I should not see "Assign Test" in the "IL4560001" row
    And I should not see "Assign Test" in the "IL4560002" row
