Feature: Drupal user registration deactivated

  @ads-1613
  Scenario: User registration disabled
    Given I am an anonymous user

    When I go to "user/register"
    Then I should get a 200 HTTP response
    And I should be on "user/login"

    When I go to "batchuser/register"
    Then I should get a 200 HTTP response
    And I should be on "user/login"
