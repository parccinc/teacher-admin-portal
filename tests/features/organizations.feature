@api @orgpages @organizations
Feature: TAP Organizations
  In order to manage Organizations
  Users should be able to
  view, edit, and add Organizations

  Background:
    Given LDR organizations:
      | name          | type     | identifier | parent              |
      | XX District 1 | district | 1          | XX                  |
      | School XX1a   | school   | 2          | [org:XX District 1] |

  @parads-1115
  Scenario: PARADS-1115 Access organizations page and see page title.
    Given I am logged in as a user with the "organization administrator" role
    And I am assigned the organization "[org:XX]"
    When I am on the homepage
    And I follow "My Districts"
    Then I see the heading "XX Districts"
    And the "table" element should contain "District ID"
    And the "table" element should contain "District"
    And the "table" element should contain "Code"
    And the "table" element should contain "Schools"
    And the "table" element should contain "Users"
    And the "table" element should contain "Students"

  @javascript @parads-2748
  Scenario: PARADS-2748 Consistent behavior for AC that says filter all columns
    Given I am logged in as a user with the "organization administrator" role
    And I am assigned the organization "[org:XX]"
    When I am on the homepage
    And I follow "My Districts"
    And I wait 1 seconds
    Then I should see "" in the 'input[type="search"]' element with the "placeholder" attribute set to "Filter all columns" in the "content" region

  @parads-3655 @parads-3780 @ads-1477
  Scenario Outline: Roles parcc user and parcc admin can not click on classrooms, but orgadmin can
    Given LDR class orgs:
      | name    | section  | org               | grade | ref     |
      | Math    | math1    | [org:School XX1a] | 3     | Math    |
      | English | english1 | [org:School XX1a] | 7     | English |
    And TAP users:
      | name       | pass  | mail                   | roles                      | organizations       |
      | parccuser  | behat | parccuser@example.com  | PARCC user                 | [org:XX District 1] |
      | parccadmin | behat | parccadmin@example.com | ads administrator          | [org:XX District 1] |
      | orgadmin   | behat | orgadmin@example.com   | organization administrator | [org:XX District 1] |

    And I am logged in as "<user name>"
    When I am at "organizations/[org:<org name>]"
    Then the xpath '//*[@id="datatable-1"]/thead/tr[1]' element <see users> contain "Users"
    And the xpath '//*[@id="datatable-1"]/thead/tr[1]' element <see students> contain "Students"
    And the xpath '//*[@id="datatable-1"]/thead/tr[1]' element <see classes> contain "Classrooms"
    And I should <classroom link> the link "classes-[org:<school org name>]-count"
    Examples:
      | user name  | org name      | see users  | see students | see classes | classroom link | school org name |
      | parccuser  | XX District 1 | should not | should not   | should not  | not see        | School XX1a     |
      | parccadmin | XX District 1 | should     | should not   | should not  | not see        | School XX1a     |
      | orgadmin   | XX District 1 | should     | should       | should      | see            | School XX1a     |

  @parads-3780
  Scenario: Classroom links to classes page
    And LDR class orgs:
      | name    | section  | org               | grade | ref     |
      | Math    | math1    | [org:School XX1a] | 3     | Math    |
      | English | english1 | [org:School XX1a] | 7     | English |
    And TAP users:
      | name     | mail                 | roles                      | organizations       |
      | orgadmin | orgadmin@example.com | organization administrator | [org:XX District 1] |

    And I am logged in as "orgadmin"
    When I am at "organizations/[org:XX District 1]"
    And I click the "#classes-[org:School XX1a]-count" element
    Then I should be on "classes/[org:School XX1a]"

  @parads-4878 @javascript
  Scenario:
    Given LDR organizations:
      | name          | type     | identifier | parent              |
      | School XX1b   | school   | 3          | [org:XX District 1] |
      | XX District 2 | district | 4          | XX                  |
      | School XX2a   | school   | 5          | [org:XX District 2] |
      | School XX2b   | school   | 6          | [org:XX District 2] |
    And TAP users:
      | name      | mail                  | roles                      | organizations       |
      | orgadmin  | orgadmin@example.com  | organization administrator | [org:XX District 1] |
      | testadmin | testadmin@example.com | test administrator         | [org:School XX1a]   |
    And I am logged in as "orgadmin"
    When I am editing the "pads_tap_organization" profile for "testadmin"
    And I select "PARCC" from "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][0]"
    And I wait for AJAX to finish
    And I select "XX" from "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][1]"
    And I wait for AJAX to finish

    # XX District 1 available, XX District 2 excluded (no access).
    Then the "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][2]" select element should contain options "XX District 1"
    And the "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][2]" select element should not contain options "XX District 2"

    When I select "XX District 1" from "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][2]"
    And I wait for AJAX to finish
    Then the "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][3]" select element should contain options "School XX1a,School XX1b"

    # Able to add child school.
    When I select "School XX1b" from "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][3]"
    And press "Add"
    And I wait for AJAX to finish
    Then I should see "School XX1b" in the ".field-name-field-tap-organization .dropbox" element
    When I press "Save"
    Then I should see "School XX1b" in the ".field-name-field-tap-organization .dropbox" element

    # Can't assign XX (state).
    And I select "PARCC" from "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][0]"
    And I wait for AJAX to finish
    And I select "XX" from "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][1]"
    And I wait for AJAX to finish
    When I press "Add"
    And I wait for AJAX to finish
    Then I should see the error message "You do not have access to XX."

  @parads-5305
  Scenario Outline: Organization pages referencing invalid org ids or text does not barf error messages.
    Given I am logged in as a user with the "<role>" role
    And I am assigned the organization "<org name>"
    When I visit "<url>"
    Then I should see 0 "div.messages.error" elements
    And I should see "<page title>" in the "#page-title" element
    Examples:
      | role                       | org name            | url               | page title             |
      | organization administrator | [org:XX]            | organizations/0   | XX Districts           |
      | organization administrator | [org:XX]            | organizations/foo | XX Districts           |
      | organization administrator | [org:XX District 1] | organizations/0   | XX District 1 Schools  |
      | organization administrator | [org:XX District 1] | organizations/foo | XX District 1 Schools  |
      | organization administrator | [org:School XX1a]   | organizations/0   | School XX1a Classrooms |
      | organization administrator | [org:School XX1a]   | organizations/foo | School XX1a Classrooms |
      | test administrator         | [org:School XX1a]   | organizations/0   | School XX1a Classrooms |
      | test administrator         | [org:School XX1a]   | organizations/foo | School XX1a Classrooms |


