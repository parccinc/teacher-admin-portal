@api @dmrsuser @dmrsuser_import @import @dmrs
Feature: ADS-642 A dmrs administrator imports invalid dmrs users
  As a dmrs administrator
  I want to know how many records had errors
  so that I can try 'em again.

  Background:
    Given LDR organizations:
      | name          | type     | identifier | parent              |
      | XX District 1 | district | 15515134   | XX                  |
      | School XX1a   | school   | 25515134   | [org:XX District 1] |
      | School XX1b   | school   | 25525134   | [org:XX District 1] |
    Given TAP users:
      | name                | mail                | pass  | roles              | organizations |
      | Sir Patrick Stewart | engage@makeitso.org | behat | dmrs administrator | [org:XX]      |

  @ads-639
  Scenario: A dmrs administrator logs in and sees link to user import
    Given I am logged in as "Sir Patrick Stewart"
    Then I should see the link "Upload DMRS User"

  @ads-639 @ads-1329
  Scenario: DMRS user import page components
    Given I am logged in as "Sir Patrick Stewart"
    When I click "Upload DMRS User"
    Then the url should match "dmrsusers/import"
    And I see the heading "Upload DMRS Users"
    And I see the text "To ensure a successful upload, make sure your CSV file is in DMRS User Template format. (Click to download sample .csv format file.)"
    And I see the text "Attention: The DMRS User Template has changed. Please make sure you are using the latest template."

  @ads-639 @ads-1329
  Scenario: Dmrs user template, with PII_Analytics
    Given I am logged in as "Sir Patrick Stewart"
    And I am on "dmrsuser/import"
    When I go to "/pads-import/download/dmrsuser_template.csv"
    Then the response should contain "First name,Last name,Email,State ID,District ID,Institution ID,General,PII,Roster,SF_Extract,RF_Extract,PF_Extract,PSRD_Extract,CDS_Extract,PII_Analytics,Staff ID"
    And the response should contain "Garrett,State,garrettcounty@mailinator.com,MD,,,1,0,1,1,1,1,1,1,1,"
    And the response should contain "Kent,District,kentcounty@mailinator.com,MD,11,,1,1,1,1,0,0,1,0,0,"
    And the response should contain "Harford,School,harfordcounty@mailinator.com,MD,11,265,1,1,1,0,0,1,0,0,1,CDavis Grade 4 ELA Class"

  @ads-642 @javascript
  Scenario: A dmrs administrator imports dmrs users with required records missing
    Given a file named "invalid_users.csv" with:
    """
First name,Last name,Email,State ID,District ID,Institution ID,General,PII,Roster,SF_Extract,RF_Extract,PF_Extract,PSRD_Extract,CDS_Extract,PII_Analytics,Staff ID
,State,nofirst[timestamp]@mailinator.com,XX,,,1,0,1,1,1,1,1,1,1
Kent,,nolast[timestamp]@mailinator.com,XX,11,,1,1,1,1,0,0,1,1,1
No,Mail,,XX,11,265,1,1,1,0,0,0,0,1,1,CDavis Grade 4 ELA Class
Harford,School,nostate[timestamp]@mailinator.com,,11,265,1,1,1,0,0,0,0,0,0,CDavis Grade 4 ELA Class
    """
    And I am logged in as "Sir Patrick Stewart"
    When I import the file "invalid_users.csv" at "dmrsusers/import"
    Then I see the heading "Upload DMRS User data confirmation with exceptions"
    And I see the text "Total records in file: 4 :: 0 DMRS Users imported successfully."
    And I see the text "Rejected Records: 4"
    And I should see the warning message "Some records could not be uploaded. To ensure a successful upload, make sure your CSV file is in DMRS User Template format. Please see the details below."
    And I should see the column headers in table "table_import_errors":
      | header         |
      | First name     |
      | Last name      |
      | Email          |
      | State ID       |
      | District ID    |
      | Institution ID |
      | General        |
      | PII            |
      | Roster         |
      | SF_Extract     |
      | RF_Extract     |
      | PF_Extract     |
      | PSRD_Extract   |
      | CDS_Extract    |
      | PII_Analytics  |
      | Staff ID       |
      | Error          |
    And I see the text "Column First name is required."
    And I see the text "Column Last name is required."
    And I see the text "Column E-mail is required."
    And I see the text "Column State ID is required."
    And I should see the button "Back to DMRS User import"

  @ads-642 @ads-1329 @javascript
  Scenario: All 0's for permissions columns
    Given a file named "invalid_users.csv" with:
    """
First name,Last name,Email,State ID,District ID,Institution ID,General,PII,Roster,SF_Extract,RF_Extract,PF_Extract,PSRD_Extract,CDS_Extract,PII_Analytics,Staff ID
Invalid,State,nofirst1[timestamp]@mailinator.com,XX,,,0,0,0,0,0,0,0,0,0
Valid1,State,nofirst2[timestamp]@mailinator.com,XX,,,0,1,0,0,0,0,0,0,0
Valid2,State,nofirst3[timestamp]@mailinator.com,XX,,,0,0,1,0,0,0,0,1,1
Valid3,State,nofirst4[timestamp]@mailinator.com,XX,,,0,0,0,1,0,0,0,1,1
Valid4,State,nofirst5[timestamp]@mailinator.com,XX,,,0,0,0,0,0,0,0,0,1
Valid5,State,nofirst6[timestamp]@mailinator.com,XX,,,0,0,0,0,0,1,0,1,1
Valid6,State,nofirst7[timestamp]@mailinator.com,XX,,,0,0,0,0,0,0,1,0,0
    """
    And I am logged in as "Sir Patrick Stewart"
    When I import the file "invalid_users.csv" at "dmrsusers/import"
    Then I see the heading "Upload DMRS User data confirmation with exceptions"
    And I see the text "Total records in file: 7 :: 6 DMRS User imported successfully."
    And I see the text "Rejected Records: 1"
    And I should see the warning message "Some records could not be uploaded. To ensure a successful upload, make sure your CSV file is in DMRS User Template format. Please see the details below."
    And I see the text "You must enable at least one permission."

  @ads-706 @javascript
  Scenario: Duplicate email addresses in same file
    Given a file named "invalid_users.csv" with:
    """
First name,Last name,Email,State ID,District ID,Institution ID,General,PII,Roster,SF_Extract,RF_Extract,PF_Extract,PSRD_Extract,CDS_Extract,PII_Analytics,Staff ID
Valid0,State,dup1[timestamp]@mailinator.com,XX,,,0,0,0,0,0,0,0,0,0
Valid1,State,dup1[timestamp]@mailinator.com,XX,,,0,1,0,0,0,0,0,0,0
Valid2,State,dup1[timestamp]@mailinator.com,XX,,,0,0,1,0,0,0,0,1,1
Valid3,State,dup1[timestamp]@mailinator.com,XX,,,0,0,0,1,0,0,0,1,1
Valid4,State,dup1[timestamp]@mailinator.com,XX,,,0,0,0,0,1,0,0,0,0
Valid5,State,dup1[timestamp]@mailinator.com,XX,,,0,0,0,0,0,1,0,1,1
Valid6,State,dup1[timestamp]@mailinator.com,XX,,,0,0,0,0,0,0,1,0,0
    """
    And I am logged in as "Sir Patrick Stewart"
    When I import the file "invalid_users.csv" at "dmrsusers/import"
    Then I see the heading "Upload DMRS User data confirmation with exceptions"
    And I see the text "Total records in file: 7 :: 0 DMRS Users imported successfully."
    And I see the text "Rejected Records: 7"
    And I should see the warning message "Some records could not be uploaded. To ensure a successful upload, make sure your CSV file is in DMRS User Template format. Please see the details below."
    And I see the text "You must enable at least one permission."
    And I see the text "dup1[timestamp]@mailinator.com already exists in the file."

  @ads-641 @javascript @ads-705 @ads-741 @ads-1329
  Scenario: A dmrs administrator imports all valid users
    Given a file named "valid_users.csv" with:
    """
First name,Last name,Email,State ID,District ID,Institution ID,General,PII,Roster,SF_Extract,RF_Extract,PF_Extract,PSRD_Extract,CDS_Extract,PII_Analytics,Staff ID
Valid1 First,State Last,valid1_[timestamp]@mailinator.com,XX,,,1,1,1,1,1,1,1,1,1,validid_1
Valid2 First,State Last,valid2_[timestamp]@mailinator.com,XX,,,0,0,1,0,0,0,0,1,1,validid_2
Valid3 First,State Last,valid3_[timestamp]@mailinator.com,XX,,,0,0,0,1,0,0,0,0,0,validid_3
Valid4 First,State Last,valid4_[timestamp]@mailinator.com,XX,,,0,0,0,0,1,0,0,1,1,validid_4
Valid5 First,State Last,valid5_[timestamp]@mailinator.com,XX,,,0,0,0,0,0,1,0,0,0,validid_5
Valid6 First,State Last,valid6_[timestamp]@mailinator.com,XX,,,0,0,0,0,0,0,1,1,1,validid_6
    """
    And I am logged in as "Sir Patrick Stewart"
    When I import the file "valid_users.csv" at "dmrsusers/import"
    Then I see the heading "Upload DMRS Users Confirmation"
    And I see the text "6 out of 6 DMRS Users were created successfully."
    And I should not see the text "You can access the new DMRS User records."
    And I should see the button "Back to DMRS User import"
    Then the user "valid1_[timestamp]@mailinator.com" should have the role "dmrs user"
    And I am editing the "dmrs_profile" profile for the user with the email "valid1_[timestamp]@mailinator.com"
    And the "State Code" field should contain "XX"
    And the "staffid" field should contain "validid_1"
    And the "edit-profile-dmrs-profile-field-memberof-und-0-value" field should contain "||GENERAL||||||XX||||||||||"
    And the "edit-profile-dmrs-profile-field-memberof-und-1-value" field should contain "||PII||||||XX||||||||||"
    And the "edit-profile-dmrs-profile-field-memberof-und-2-value" field should contain "||ROSTER||||||XX||||||||||"
    And the "edit-profile-dmrs-profile-field-memberof-und-3-value" field should contain "||SF_EXTRACT||||||XX||||||||||"
    And the "edit-profile-dmrs-profile-field-memberof-und-4-value" field should contain "||RF_EXTRACT||||||XX||||||||||"
    And the "edit-profile-dmrs-profile-field-memberof-und-5-value" field should contain "||PF_EXTRACT||||||XX||||||||||"
    And the "edit-profile-dmrs-profile-field-memberof-und-6-value" field should contain "||PSRD_EXTRACT||||||XX||||||||||"
    And the "edit-profile-dmrs-profile-field-memberof-und-7-value" field should contain "||CDS_EXTRACT||||||XX||||||||||"
    And the "edit-profile-dmrs-profile-field-memberof-und-8-value" field should contain "||PII_ANALYTICS||||||XX||||||||||"

  @amplify
  Scenario: dmrs administrator can't see Amplify link
    Given I am logged in as "Sir Patrick Stewart"
    And I should not see the exact link "DMRS"

  @ads-950
  Scenario: The menu link to dmrs says DMRS and not amplify
    Given I am logged in as a user with the "dmrs user" role
    And I am on the homepage
    Then I should see the link "DMRS"
    And I should not see the link "Amplify"

    When the "dmrs_sso_user_dmrs_link_title" variable is set to "some other vendor"
    #Todo: implement menu item reset step.
    #And I reload the page
    #Then I should see the link "some other vendor"
    #And I should not see the link "DMRS"

  @ads-774 @javascript
  Scenario: DMRS admins can only import dmrs users in their state
    Given LDR organizations:
      | name | type  | identifier | parent |
      | WI   | state | WI         | 1      |
      | IL   | state | IL         | 1      |
    And a file named "wrong_state_users.csv" with:
    """
First name,Last name,Email,State ID,District ID,Institution ID,General,PII,Roster,SF_Extract,RF_Extract,PF_Extract,PSRD_Extract,CDS_Extract,PII_Analytics,Staff ID
Valid,State,nofirst1[timestamp]@mailinator.com,XX,,,0,0,0,0,0,0,1,0,0,
Invalid1,State,nofirst2[timestamp]@mailinator.com,IL,,,0,1,0,0,0,0,0,0,0,
Invalid2,State,nofirst3[timestamp]@mailinator.com,IL,,,0,0,1,0,0,0,0,1,1,
Invalid3,State,nofirst4[timestamp]@mailinator.com,IL,,,0,0,0,1,0,0,0,1,1,
Invalid4,State,nofirst5[timestamp]@mailinator.com,WI,,,0,0,0,0,1,0,0,0,0,
Invalid5,State,nofirst6[timestamp]@mailinator.com,WI,,,0,0,0,0,0,1,0,1,1,
Invalid6,State,nofirst7[timestamp]@mailinator.com,MD,,,0,0,0,0,0,0,1,0,0,
    """
    And I am logged in as "Sir Patrick Stewart"
    When I import the file "wrong_state_users.csv" at "dmrsusers/import"
    Then I see the heading "Upload DMRS User data confirmation with exceptions"
    And I see the text "Total records in file: 7 :: 1 DMRS Users imported successfully."
    And I see the text "Rejected Records: 6"
    And I should see the warning message "Some records could not be uploaded. To ensure a successful upload, make sure your CSV file is in DMRS User Template format. Please see the details below."
    And I should see "is not within your associated Organizations" in the "Invalid1" row
    And I should see "is not within your associated Organizations" in the "Invalid2" row
    And I should see "is not within your associated Organizations" in the "Invalid3" row
    And I should see "is not within your associated Organizations" in the "Invalid4" row
    And I should see "is not within your associated Organizations" in the "Invalid5" row
    And I should see "is not a valid Organization id." in the "Invalid6" row

  @ads-774 @javascript
  Scenario: DMRS admin associated to "PARCC" can import dmrs users in multiple states
    Given LDR organizations:
      | name | type  | identifier | parent |
      | WI   | state | WI         | 1      |
      | IL   | state | IL         | 1      |
    And TAP users:
      | name             | mail                | pass  | roles              | organizations |
      | Sir Ian McKellen | gandalf@thegrey.org | behat | dmrs administrator | 1             |
    And a file named "wrong_state_users.csv" with:
    """
First name,Last name,Email,State ID,District ID,Institution ID,General,PII,Roster,SF_Extract,RF_Extract,PF_Extract,PSRD_Extract,CDS_Extract,PII_Analytics,Staff ID
Valid,State,nofirst1[timestamp]@mailinator.com,XX,,,0,0,0,0,0,0,1,0,0,
Invalid1,State,nofirst2[timestamp]@mailinator.com,IL,,,0,1,0,0,0,0,0,0,0,
Invalid2,State,nofirst3[timestamp]@mailinator.com,IL,,,0,0,1,0,0,0,0,1,1,
Invalid3,State,nofirst4[timestamp]@mailinator.com,IL,,,0,0,0,1,0,0,0,1,1,
Invalid4,State,nofirst5[timestamp]@mailinator.com,WI,,,0,0,0,0,1,0,0,0,0,
Invalid5,State,nofirst6[timestamp]@mailinator.com,WI,,,0,0,0,0,0,1,0,1,1,
Invalid6,State,nofirst7[timestamp]@mailinator.com,MD,,,0,0,0,0,0,0,1,0,0,
    """
    And I am logged in as "Sir Ian McKellen"
    When I import the file "wrong_state_users.csv" at "dmrsusers/import"
    Then I see the heading "Upload DMRS User data confirmation with exceptions"
    And I see the text "Total records in file: 7 :: 6 DMRS User imported successfully."
    And I see the text "Rejected Records: 1"
    And I should see the warning message "Some records could not be uploaded. To ensure a successful upload, make sure your CSV file is in DMRS User Template format. Please see the details below."
    And I should see "is not a valid Organization id." in the "Invalid6" row

  @ads-1401
  Scenario: DMRS admin with multiple states sees edit link
    Given LDR organizations:
      | name | type  | identifier | parent |
      | WI   | state | WI         | 1      |
      | IL   | state | IL         | 1      |
      | IA   | state | IA         | 1      |
    And TAP users:
      | name            | mail                 | pass  | roles              | organizations     |
      | Christopher Lee | saruman@thewhite.org | behat | dmrs administrator | [org:WI],[org:IL] |
      | iluser1         | iluser1@example.com  | behat | dmrs user          | [org:IL]          |
      | iluser2         | iluser2@example.com  | behat | dmrs user          | [org:IL]          |
      | iauser1         | iauser1@example.com  | behat | dmrs user          | [org:IA]          |
      | wiuser1         | wiuser1@example.com  | behat | dmrs user          | [org:WI]          |
      | wiuser2         | wiuser2@example.com  | behat | dmrs user          | [org:WI]          |


  #multi-state user sees edit link
    When I am logged in as "Christopher Lee"
    And I am on the homepage
    When I click "Admin DMRS Users"
    Then I should see 4 "table.views-table tbody tr" elements
    And I should see 4 "table.views-table tbody > tr > td.views-field.views-field-edit-node > a" elements
    And I should not see a "iauser1" row
    When I am on the edit page for user "iluser1"
    Then I should get a 200 HTTP response

    When I am editing the "dmrs_user" profile for "iluser1"
    Then I should get a 200 HTTP response

    When I am on the edit page for user "wiuser1"
    Then I should get a 200 HTTP response
    And I take an awesome screenshot
    When I am editing the "dmrs_user" profile for "wiuser1"
    Then I should get a 200 HTTP response
    When I am on the edit page for user "iauser1"
    Then I should get a 403 HTTP response
    When I am editing the "dmrs_user" profile for "iauser1"
    Then I should get a 403 HTTP response

  @ads-1329 @javascript
  Scenario: 1-4 Old template used
    Given a file named "oldtemplate.csv" with:
"""
First name,Last name,Email,State ID,District ID,Institution ID,General,PII,Roster,SF_Extract,RF_Extract,PF_Extract,PSRD_Extract,CDS_Extract,Staff ID
Valid1 First,State Last,valid1_[timestamp]@mailinator.com,XX,,,1,1,1,1,1,1,1,1,validid_1
Valid2 First,State Last,v2alid2_[timestamp]@mailinator.com,XX,,,0,0,1,0,0,0,0,1,validid_2
"""
    And I am logged in as "Sir Patrick Stewart"
    When I import the file "oldtemplate.csv" at "dmrsusers/import"
    Then I should see the warning message "Invalid DMRS User template. To ensure a successful upload, make sure your CSV file is in DMRS User Template format."
    And I should not see an "#table_import_errors" element
