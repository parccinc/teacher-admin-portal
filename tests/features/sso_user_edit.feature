@api @sso @user
Feature: ADS-526 User edit page
  As a user with any authenticated role
  I want to view but not change my email
  so that I can properly use the SSO system

  @ads-526
  Scenario: 1: My user edit page
    Given users:
      | name           | pass  | mail                 | roles      |
      | behat_ssoadmin | behat | ssoadmin@example.com | batch user |
    When I go to "user/local-login"
    And I fill in "E-mail or username" with "behat_ssoadmin"
    And I fill in "Password" with "behat"
    And I press "Log in"
    And I click "My account"
    And I click "Edit"
    Then the "E-mail address *" form element should be disabled

  @ads-682 @ads-699
  Scenario: Editing my own name should leave my roles in tact
    Given users:
      | name                      | pass  | mail                            | roles                                 |
      | behat_ssoadmin[timestamp] | behat | ssoadmin[timestamp]@example.com | test administrator,dmrs administrator |
    When I go to "user/local-login"
    And I fill in "E-mail or username" with "behat_ssoadmin[timestamp]"
    And I fill in "Password" with "behat"
    And I press "Log in"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "parccroles" value of "test administrator"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "parccroles" value of "dmrs administrator"
    And I click "My account"
    And I click "Edit"
    And I fill in "First name" with "Firsty"
    And I fill in "Last name" with "Lasty"
    And I fill in "Current password" with "behat"
    And I fill in "Password" with "abcd1234"
    And I fill in "Confirm password" with "abcd1234"
    And I press "Save"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "parccroles" value of "test administrator"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "parccroles" value of "dmrs administrator"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "givenname" value of "Firsty"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "sn" value of "Lasty"
    # ADS-699 states that repeatedly updating logging in and out changes the LDAP attributes
    And I click "Log out"
    When I go to "user/local-login"
    And I fill in "E-mail or username" with "behat_ssoadmin[timestamp]"
    And I fill in "Password" with "abcd1234"
    And I press "Log in"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "parccroles" value of "test administrator"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "parccroles" value of "dmrs administrator"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "givenname" value of "Firsty"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "sn" value of "Lasty"
    # Twice
    And I click "Log out"
    When I go to "user/local-login"
    And I fill in "E-mail or username" with "behat_ssoadmin[timestamp]"
    And I fill in "Password" with "abcd1234"
    And I press "Log in"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "parccroles" value of "test administrator"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "parccroles" value of "dmrs administrator"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "givenname" value of "Firsty"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "sn" value of "Lasty"
    # Three times
    And I click "Log out"
    When I go to "user/local-login"
    And I fill in "E-mail or username" with "behat_ssoadmin[timestamp]"
    And I fill in "Password" with "abcd1234"
    And I press "Log in"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "parccroles" value of "test administrator"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "parccroles" value of "dmrs administrator"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "givenname" value of "Firsty"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "sn" value of "Lasty"
    # Four times
    And I click "Log out"
    When I go to "user/local-login"
    And I fill in "E-mail or username" with "behat_ssoadmin[timestamp]"
    And I fill in "Password" with "abcd1234"
    And I press "Log in"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "parccroles" value of "test administrator"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "parccroles" value of "dmrs administrator"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "givenname" value of "Firsty"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "sn" value of "Lasty"
    # Five times
    And I click "Log out"
    When I go to "user/local-login"
    And I fill in "E-mail or username" with "behat_ssoadmin[timestamp]"
    And I fill in "Password" with "abcd1234"
    And I press "Log in"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "parccroles" value of "test administrator"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "parccroles" value of "dmrs administrator"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "givenname" value of "Firsty"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "sn" value of "Lasty"
    # Six times
    And I click "Log out"
    When I go to "user/local-login"
    And I fill in "E-mail or username" with "behat_ssoadmin[timestamp]"
    And I fill in "Password" with "abcd1234"
    And I press "Log in"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "parccroles" value of "test administrator"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "parccroles" value of "dmrs administrator"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "givenname" value of "Firsty"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "sn" value of "Lasty"
    # Seven times
    And I click "Log out"
    When I go to "user/local-login"
    And I fill in "E-mail or username" with "behat_ssoadmin[timestamp]"
    And I fill in "Password" with "abcd1234"
    And I press "Log in"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "parccroles" value of "test administrator"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "parccroles" value of "dmrs administrator"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "givenname" value of "Firsty"
    And the LDAP user "ssoadmin[timestamp]@example.com" should have a "sn" value of "Lasty"

  @ads-527
  Scenario: 1: Drupal administrator can not edit email
    Given users:
      | name           | pass  | mail                 | roles         |
      | behat_ssoadmin | behat | ssoadmin@example.com | administrator |
      | behat_ssobatch | behat | ssobatch@example.com | administrator |
    When I go to "user/local-login"
    And I fill in "E-mail or username" with "behat_ssoadmin"
    And I fill in "Password" with "behat"
    And I press "Log in"
    And I click "My account"
    And I click "Edit"
    Then the "E-mail address *" form element should be disabled

  @ads-633 @javascript
  Scenario: Saving profile updates LDAP on profile2 entries
    Given LDR organizations:
      | name          | type     | identifier | parent              |
      | XX District 1 | district | 15515134   | XX                  |
      | School XX1a   | school   | 25515134   | [org:XX District 1] |
      | School XX1b   | school   | 25525134   | [org:XX District 1] |
    And TAP users:
      | name                | mail                | pass  | roles         | organizations |
      | Sir Patrick Stewart | engage@makeitso.org | behat | administrator | [org:XX]      |
    And a file named "valid_users.csv" with:
    """
First name,Last name,Email,State ID,District ID,Institution ID,General,PII,Roster,SF_Extract,RF_Extract,PF_Extract,PSRD_Extract,CDS_Extract,PII_Extract,Staff ID
Valid1 First,State Last,valid633_[timestamp]@mailinator.com,XX,,,1,1,1,1,1,1,1,1,1,validid_1
    """
    And I am logged in as "Sir Patrick Stewart"
    And I import the file "valid_users.csv" at "dmrsusers/import"

    When I am editing the "pads_tap_organization" profile for the user with the email "valid633_[timestamp]@mailinator.com"

    #Add school org
    And I select "PARCC" from "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][0]"
    And I wait for AJAX to finish
    And I select "XX" from "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][1]"
    And I wait for AJAX to finish
    And I select "XX District 1" from "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][2]"
    And I wait for AJAX to finish
    And I select "School XX1a" from "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][3]"
    And I wait for AJAX to finish
    And press "Add"
    And I wait for AJAX to finish

        # Remove XX
    And I click "Remove"
    And I wait for AJAX to finish

    When I press "Save"
    Then the LDAP user "valid633_[timestamp]@mailinator.com" should have a "parccorgs" value of "PARCC_XX_XX District 1_School XX1a"

    # Add a second school, the profile should contain them both
    And I select "PARCC" from "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][0]"
    And I wait for AJAX to finish
    And I select "XX" from "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][1]"
    And I wait for AJAX to finish
    And I select "XX District 1" from "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][2]"
    And I wait for AJAX to finish
    And I select "School XX1b" from "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][3]"
    And I wait for AJAX to finish
    And press "Add"
    And I wait for AJAX to finish
    When I press "Save"
    And the LDAP user "valid633_[timestamp]@mailinator.com" should have a "parccorgs" value of "PARCC_XX_XX District 1_School XX1b"

    # TODO: ADS-633 - Removing the last org from a user's profile doesn't update LDAP
#    And the LDAP user "valid633_[timestamp]@mailinator.com" should have a "parccorgs" value of ""


  @ads-728 @javascript
  Scenario: Make Organization a required field
    Given LDR organizations:
      | name          | type     | identifier | parent              |
      | XX District 1 | district | 15515134   | XX                  |
      | School XX1a   | school   | 25515134   | [org:XX District 1] |
      | School XX1b   | school   | 25525134   | [org:XX District 1] |
    And TAP users:
      | name                | mail                    | pass  | roles         | organizations |
      | Sir Patrick Stewart | engage@makeitso.org     | behat | administrator | [org:XX]      |
      | Sir Ian McKellen    | magneto@evilmutants.org | behat | TAP user      |               |
    And I am logged in as "Sir Patrick Stewart"
    And I am editing the "pads_tap_organization" profile for the user with the email "magneto@evilmutants.org"
    When I press "Save"
    Then I should see the error message containing "TAP Organization field is required."
    And I select "PARCC" from "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][0]"
    And I wait for AJAX to finish
    And I select "XX" from "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][1]"
    And I wait for AJAX to finish
    And press "Add"
    And I wait for AJAX to finish
    When I press "Save"
    When I click "Remove"
    And I wait for AJAX to finish
    When I press "Save"
    Then I should see the error message containing "TAP Organization field is required."

  @ads-528
  Scenario: An organization administrator edits test administrator users name
    Given LDR organizations:
      | name   | type     | identifier | parent     |
      | XXD1   | district | 15515134   | XX         |
      | XXD1S1 | school   | 25515134   | [org:XXD1] |
    And users:
      | name                          | mail                                      | pass  | roles                      |
      | Sir Patrick Stewart           | behat_ssoengage[timestamp]@makeitso.org   | behat | organization administrator |
      | behat_ssotestadmin[timestamp] | behat_ssotestadmin[timestamp]@example.com | behat | test administrator         |

    #The test administrator will fill out his first and last name
    When I go to "user/local-login"
    And I fill in "E-mail or username" with "behat_ssotestadmin[timestamp]"
    And I fill in "Password" with "behat"
    And I press "Log in"
    And I am assigned the organization "[org:XXD1S1]"
    And I click "My account"
    And I click "Edit"
    And I fill in "First name" with "Eses"
    And I fill in "Last name" with "Oooh"
    And I press "Save"
    Then the LDAP user "behat_ssotestadmin[timestamp]@example.com" should have a "givenname" value of "Eses"
    And the LDAP user "behat_ssotestadmin[timestamp]@example.com" should have a "sn" value of "Oooh"

    #Now Jean-Luc changes the users name and it is reflected in LDAP
    When I am logged in as "Sir Patrick Stewart"
    And I am assigned the organization "[org:XX]"
    And I am on the edit page for user "behat_ssotestadmin[timestamp]"
    And I fill in "First name" with "Eses1"
    And I fill in "Last name" with "Oooh1"
    And I press "Save"
    Then the LDAP user "behat_ssotestadmin[timestamp]@example.com" should have a "givenname" value of "Eses1"
    And the LDAP user "behat_ssotestadmin[timestamp]@example.com" should have a "sn" value of "Oooh1"

  @ads-528 @javascript
    Scenario: Organization administrator edits org of test admin is reflected in ldap
    Given LDR organizations:
      | name   | type     | identifier | parent     |
      | XXD1   | district | 15515134   | XX         |
      | XXD1S1 | school   | 25515134   | [org:XXD1] |
      | XXD1S2 | school   | 25525134   | [org:XXD1] |
    And users:
      | name                          | mail                                      | pass  | roles                      |
      | Sir Patrick Stewart           | behat_ssoengage[timestamp]@makeitso.org   | behat | organization administrator |
      | behat_ssotestadmin[timestamp] | behat_ssotestadmin[timestamp]@example.com | behat | test administrator         |

    And I am logged in as "Sir Patrick Stewart"
    And I am assigned the organization "[org:XX]"
    And I am editing the "pads_tap_organization" profile for the user with the email "behat_ssotestadmin[timestamp]@example.com"
    And I select "PARCC" from "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][0]"
    And I wait for AJAX to finish
    And I select "XX" from "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][1]"
    And I wait for AJAX to finish
    And I select "XXD1" from "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][2]"
    And I wait for AJAX to finish
    And I select "XXD1S1" from "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][3]"
    And I wait for AJAX to finish
    And press "Add"
    And I wait for AJAX to finish
    When I press "Save"
    Then the LDAP user "behat_ssotestadmin[timestamp]@example.com" should have a "parccorgs" value of "PARCC_XX_XXD1_XXD1S1"
    # Add a second school, the profile should contain them both
    When I select "PARCC" from "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][0]"
    And I wait for AJAX to finish
    And I select "XX" from "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][1]"
    And I wait for AJAX to finish
    And I select "XXD1" from "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][2]"
    And I wait for AJAX to finish
    And I select "XXD1S2" from "profile_pads_tap_organization[field_tap_organization][und][hierarchical_select][selects][3]"
    And I wait for AJAX to finish
    And press "Add"
    And I wait for AJAX to finish
    And I press "Save"
    Then the LDAP user "behat_ssotestadmin[timestamp]@example.com" should have a "parccorgs" value of "PARCC_XX_XXD1_XXD1S1"
    And the LDAP user "behat_ssotestadmin[timestamp]@example.com" should have a "parccorgs" value of "PARCC_XX_XXD1_XXD1S2"