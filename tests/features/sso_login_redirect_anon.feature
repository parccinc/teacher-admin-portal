@api @user @sso @login
Feature: ADS-341 Redirect TAP default login page to SSO login page
  As an anonymous user
  I want to login to TAP via SSO
  so that I can access all ADS systems with one login

  Scenario: 1: Anonymous visits TAP
#    Given I am an anonymous user
    When I am on the homepage
    Then I should see the link "Forgot Password?"
    And I should see the text "Enter your e-mail address."
    And I should see the text "Enter the password that accompanies your e-mail."
    And I should see the button "Log in"
    And I should not see the text "User account"
    And I should not see the text "E-mail or username"
