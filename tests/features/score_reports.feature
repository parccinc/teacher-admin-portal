@api @tests @assignments @standards
Feature: Score Reports

  Background:
    Given LDR organizations:
      | name   | type     | identifier | parent     |
      | XXD6   | district | 6000       | XX         |
      | XXD6S1 | school   | 6001       | [org:XXD6] |
      | XXD6S2 | school   | 6002       | [org:XXD6] |
    And LDR class orgs:
      | name | section | org          | grade | ref  |
      | Math |         | [org:XXD6S1] | 3     | math |
    And LDR students:
      | firstName | lastName | enrollOrgId  | dateOfBirth | gender | gradeLevel | stateIdentifier | classes      |
      | Marnie    | Hummel   | [org:XXD6S1] | 2005-01-01  | F      | 3          | 4865MH          | [class:math] |
      | Abel      | Wade     | [org:XXD6S1] | 2005-02-02  | M      | 3          | 4865AW          | [class:math] |
      | Gordon    | Harris   | [org:XXD6S1] | 2005-03-03  | M      | 3          | 4865GH          | [class:math] |
      | Muriel    | Johnson  | [org:XXD6S1] | 2005-04-04  | F      | 3          | 4865MJ          | [class:math] |
      | Helen     | McKinney | [org:XXD6S1] | 2005-05-05  | F      | 3          | 4865HM          | [class:math] |
      | Jules     | Pepe     | [org:XXD6S1] | 2005-06-06  | M      | 3          | 4865JP          | [class:math] |
    And LDR tests:
      | name       | subject | grade | program               | security | permissions    | scoring   | description                 |
      | Behat test | ELA     | 3     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Test administered for Behat |
    And LDR score reports:
      | score_report | score_report_file   | ref    |
      | ABC          |                     | muriel |
      |              | score_report_18.txt | helen  |
    And LDR test assignments:
      | student                  | test              | status     | ref       | score_report |
      | [student:Marnie Hummel]  | [test:Behat test] | Scheduled  | ta-marnie |              |
      | [student:Abel Wade]      | [test:Behat test] | InProgress | ta-abel   |              |
      | [student:Gordon Harris]  | [test:Behat test] | Paused     | ta-gordon |              |
      | [student:Muriel Johnson] | [test:Behat test] | Canceled   | ta-muriel | [sr:muriel]  |
      | [student:Helen McKinney] | [test:Behat test] | Submitted  | ta-helen  | [sr:helen]   |
      | [student:Jules Pepe]     | [test:Behat test] | Submitted  | ta-jules  |              |
    And the standards have been imported
    And TAP users:
      | name             | pass  | mail                         | roles              | organizations |
      | ta_report_viewer | behat | ta_report_viewer@example.com | test administrator | [org:XXD6S1]  |

  @parads-3953 @parads-4627 @parads-5213 @ads-307
  Scenario: View Score Report Links
    Given I am logged in as "ta_report_viewer"
    And I am assigned classes "[class:math]"

    When I am on the homepage
    And I click "Tests"
    And I click "Test Assignments"

    # Verify "View Score Report" link visibility, PARADS-3953.
    Then I should see "Scheduled" in the "Marnie" row
    And I should not see "View Score Report" in the "Marnie" row

    And I should see "InProgress" in the "Abel" row
    And I should not see "View Score Report" in the "Abel" row

    And I should see "Paused" in the "Gordon" row
    And I should not see "View Score Report" in the "Gordon" row

    And I should see "Canceled" in the "Muriel" row
    And I should not see "View Score Report" in the "Muriel" row

    And I should see "Submitted" in the "Helen" row
    And I should see "View Score Report" in the "Helen" row

    And I should see "Submitted" in the "Jules" row
    And I should not see "View Score Report" in the "Jules" row

    # View the score report, PARADS-4627.
    When I click "View Score Report" in the "Helen" row
    Then I should be on "test-assignment/[asn:ta-helen]/report"
    And I should see "Math Comp MC.6.EE.C - Quantitative relationships between dependent and independent variables"

    # Verify links to standards, PARADS-4627.

    # Domain/cluster link, added in PARADS-5213.
    And I click "EE-6.EE.C"
    Then I should be on the modal for domain cluster "EE-6.EE.C"
    And I see the heading "EE-6.EE.C"
    And I should see "EE"
    And I should see "Expressions and Equations"
    And I should see "6.EE.C"
    And I should see "Represent and analyze quantitative relationships between dependent and independent variables."
    And I should not see "Evidence Statement:"
    And I should not see "Evidence Statement Text:"

    # Valid link, follow it to verify link rewrite worked.
    When I am on "test-assignment/[asn:ta-helen]/report"
    And I click "6.EE.9"
    Then I should be on the modal for standard "6.EE.9"
    And I see the heading "6.EE.9"
    And I should see "EE"
    And I should see "Expressions and Equations"
    And I should see "6.EE.C"
    And I should see "Represent and analyze quantitative relationships between dependent and independent variables."
    And I should see "6.EE.9"
    And I should see "Use variables to represent two quantities in a real-world problem that change in relationship to one another;"

    #ads-307 Non math-fluency score reports should say Evidence Statement and not Content Standard.
    And I should see "Evidence Statement"
    And I should not see "Content Standard"

    # Access control on score report view page callback.
    When I go to "test-assignment/[asn:ta-marnie]/report"
    Then I should get a "403" HTTP response

    When I go to "test-assignment/[asn:ta-abel]/report"
    Then I should get a "403" HTTP response

    When I go to "test-assignment/[asn:ta-gordon]/report"
    Then I should get a "403" HTTP response

    When I go to "test-assignment/[asn:ta-muriel]/report"
    Then I should get a "403" HTTP response

    When I go to "test-assignment/[asn:ta-jules]/report"
    Then I should get a "403" HTTP response

  @javascript @parads-4627 @parads-5213
  Scenario: View Score Report Links - Modals
    Given I am logged in as "ta_report_viewer"
    And I am assigned classes "[class:math]"

    When I am on "test-assignment/[asn:ta-helen]/report"
    And I click "EE-6.EE.C"
    And I wait for AJAX to finish
    Then I should see a ".ctools-modal-content" element
    And I should see "EE-6.EE.C" in the "#modal-title" element

    When I am on "test-assignment/[asn:ta-helen]/report"
    And I click "6.EE.9"
    And I wait for AJAX to finish
    Then I should see a ".ctools-modal-content" element
    And I should see "6.EE.9" in the "#modal-title" element

  @ads-694
  Scenario: Bug - Error Thrown on Score Report with Invalid Standard
    Given LDR score reports:
      | score_report                               | ref       |
      | <a href='standard:IN.VAL.ID'>IN.VAL.ID</a> | helen_bad |
    And LDR test assignments:
      | student                  | test              | status    | ref          | score_report   |
      | [student:Helen McKinney] | [test:Behat test] | Submitted | ta-helen_bad | [sr:helen_bad] |

    And I am logged in as "ta_report_viewer"
    And I am assigned classes "[class:math]"

    When I am on "test-assignment/[asn:ta-helen_bad]/report"
    Then I should not see "Notice: Undefined offset: 1 in pads_domain_cluster_load()"
    And I should see "IN.VAL.ID"
    And I should not see the link "IN.VAL.ID"

  @ads-307 @ads-917
  Scenario: Math fluency standards
    And LDR score reports:
      | score_report | score_report_file                | ref           |
      |              | score_report_math_fluency_13.txt | helen-fluency |
    And LDR test assignments:
      | student                  | test              | status    | ref              | score_report       |
      | [student:Helen McKinney] | [test:Behat test] | Submitted | ta-helen-fluency | [sr:helen-fluency] |

    And I am logged in as "ta_report_viewer"
    And I am assigned classes "[class:math]"
    And I am on "test-assignment/[asn:ta-helen-fluency]/report"
    When I click "6.3. Quotient (whole/decimal)"
    Then I see the heading "6.3. Quotient (whole/decimal)"
    And I should see "6.NS"
    And I should see "The Number System"

    #ads-917
    And I should not see "Evidence Statement"
    And I should see "Content Standard"

    And I should see "6.NS.B"
    And I should see "Compute fluently with multi-digit numbers and find common factors and multiples."
    And I should see "Fluency Skill Area"
    And I should see "Divide with multi-digit decimal numbers."
    And I should see "Subskill"
    And I should see "6.3. Quotient (whole/decimal)"
    And I should see "Subskill Text"
    And I should see "Quotient (whole/decimal)"
    And I should not see "Evidence Statement Text"
