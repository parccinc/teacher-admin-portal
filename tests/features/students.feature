@api @students
Feature: PADS student functionality

  Background:
    Given LDR organizations:
      | name   | type     | identifier | parent     |
      | XXD1   | district | 1          | XX         |
      | XXD1S1 | school   | 2          | [org:XXD1] |
      | XXD1S2 | school   | 3          | [org:XXD1] |
    And TAP users:
      | name                       | pass  | mail                       | roles                      | organizations             |
      | behat_testadmin            | behat | testadmin@example.com      | test administrator         | [org:XXD1S1]              |
      | mr_hand                    | hand  | notinmyclass@example.com   | test administrator         | [org:XXD1S1],[org:XXD1S2] |
      | behat_school_orgadmin      | behat | orgadmin@example.com       | organization administrator | [org:XXD1S1]              |
      | behat_multischool_orgadmin | behat | orgadmin_multi@example.com | organization administrator | [org:XXD1S1],[org:XXD1S2] |
    And LDR class orgs:
      | name    | section | org          | grade | ref  |
      | Math    | 101     | [org:XXD1S1] | 3     | math |
      | English | 201     | [org:XXD1S1] | 4     | eng  |
      | History | 301     | [org:XXD1S1] | 3     | his  |
      | Science | 401     | [org:XXD1S2] | 3     | sci  |
      | Gym     | 501     | [org:XXD1S2] | 3     | gym  |
    And LDR students:
      | firstName | lastName | enrollOrgId  | dateOfBirth | gender | gradeLevel | stateIdentifier | classes      |
      | Louis     | Skolnick | [org:XXD1S1] | 2005-01-01  | F      | 3          | IL4560001       | [class:math] |
      | Gilbert   | Lowell   | [org:XXD1S1] | 2005-02-02  | M      | 3          | IL4560002       | [class:eng]  |
      | Jeff      | Spicoli  | [org:XXD1S2] | 1980-01-01  | M      | 7          | IL4560003       | [class:sci]  |

  Scenario: Org administrators have jump menu of schools
    Given I am logged in as "behat_multischool_orgadmin"
    And I am on the homepage
    And I select "XXD1S2" from "Organization"
    And I press "Select"
    And I follow "students-[org:XXD1S2]-count"
    And I select "XXD1S2" from "Showing students for"
    And I press "Go"
    Then I should be on "students/[org:XXD1S2]"
    When I select "XXD1S1" from "edit-jump"
    And I press "Go"
    Then I should be on "students/[org:XXD1S1]"

  Scenario: I can click on the student identifier link on the students page
    Given I am logged in as "behat_testadmin"
    And I am assigned classes "[class:math],[class:eng]"
    When I click "My Students"
    When I click "IL4560001" in the "IL4560001" row
    Then I should see the heading "Student Profile"
    And the "stateIdentifier" field should contain "IL4560001"

  Scenario: I can edit or not edit student
    Given I am logged in as "behat_testadmin"
    And I am assigned classes "[class:math]"

    # Can edit math students.
    When I am at "student/[student:Louis Skolnick]"
    And I click "Edit Student"
    Then I should get a 200 HTTP response

    # Can not edit English students.
    When I am on "student/[student:Jeff Spicoli]"
    Then I should get a 403 HTTP response
    When I am on "student/[student:Jeff Spicoli]/edit"
    Then I should get a 403 HTTP response

  @parads-1116 @student
  Scenario: PARADS-1116 Student table has headers
    Given I am logged in as "behat_testadmin"
    And I am assigned classes "[class:math]"
    When I am on "students/[org:XXD1S1]"
    And I click "Students"
    Then the "th.views-field-stateIdentifier" element should contain "State identifier"
    And the "th.views-field-firstName" element should contain "First name"
    And the "th.views-field-middleName" element should contain "Middle name"
    And the "th.views-field-lastName" element should contain "Last name"
    And the "th.views-field-gradeLevel" element should contain "Grade"

  @javascript @parads-1116 @parads-3396
  Scenario Outline: Student table has sortable headers
    And LDR students:
      | firstName | lastName | enrollOrgId  | dateOfBirth | gender | gradeLevel | stateIdentifier | classes     |
      | Alpha     | Bravo    | [org:XXD1S1] | 2005-01-01  | F      | 3          | IL1230001       | [class:his] |
      | Charlie   | Delta    | [org:XXD1S1] | 2005-02-02  | M      | 3          | IL1230002       | [class:his] |

    And I am logged in as "behat_testadmin"
    And I am assigned classes "[class:his]"
    # Change to more of a natural flow b/c I was getting random 500 errors on this.
    # The reload the page is b/c of the assigned classes, it needs adjusting.
    And I am on the homepage
    And I reload the page
    And I click "Students"

    # Click on column header for default asc sort.
    When I click the "th.views-field-<column name>" element

    Then the xpath '//*[@id="datatable-1"]/tbody/tr[1]/td[<column>]' element should contain "<asc value>"
    And the xpath '//*[@id="datatable-1"]/tbody/tr[2]/td[<column>]' element should contain "<desc value>"

    # Click on column header again for desc order.
    When I click the "th.views-field-<column name>" element

    Then the xpath '//*[@id="datatable-1"]/tbody/tr[1]/td[<column>]' element should contain "<desc value>"
    And the xpath '//*[@id="datatable-1"]/tbody/tr[2]/td[<column>]' element should contain "<asc value>"

    Examples:
      | column | column name     | asc value | desc value |
      | 1      | stateIdentifier | IL1230001 | IL1230002  |
      | 2      | firstName       | Alpha     | Charlie    |
      | 4      | lastName        | Bravo     | Delta      |

  @parads-2174 @classes
  Scenario: Class tab with and without classes, no students
    Given I am logged in as "behat_testadmin"
    And I am on the homepage
    When I click "Classes"
    Then I should see the link "Tests"
    And I should see the link "Students"
    And I see the text "There are no classes assigned to you."

    When I am assigned classes "[class:his]"
    And I am on the homepage
    And I click "Classes"
    Then the xpath '//*[@id="datatable-1"]/tbody/tr[1]/td[1]' element should contain "History"

    When I click "Students"
    Then I should see the text "There are no students assigned to your classes."


  @parads-2174 @classes
  Scenario: Students tab when user has classes and students assigned to aforementioned classes
    Given I am logged in as "behat_testadmin"
    And I am assigned classes "[class:math],[class:eng]"
    When I click "Students"
    Then I should see "Math" in the "#datatable-2 caption" element
    And I should see "IL4560001" in the "#datatable-2 td.views-field-stateIdentifier" element
    And I should see "English" in the "#datatable-1 caption" element
    And I should see "IL4560002" in the "#datatable-1 td.views-field-stateIdentifier" element


  @javascript @parads-3757 @parads-4077 @classes
  Scenario Outline: Classes table on My Students page has sortable headers
    Given I am logged in as "behat_testadmin"
    And I am assigned classes "[class:math],[class:eng]"
    When I am on the homepage
    And I click "Classes"

    # Click on column header for default asc sort.
    When I click the "th.views-field-<column name>" element

    Then the xpath '//*[@id="datatable-1"]/tbody/tr[1]/td[<column>]' element should contain "<asc value>"
    And the xpath '//*[@id="datatable-1"]/tbody/tr[2]/td[<column>]' element should contain "<desc value>"

    # Click on column header again for desc order.
    When I click the "th.views-field-<column name>" element

    Then the xpath '//*[@id="datatable-1"]/tbody/tr[1]/td[<column>]' element should contain "<desc value>"
    And the xpath '//*[@id="datatable-1"]/tbody/tr[2]/td[<column>]' element should contain "<asc value>"

    Examples:
      | column | column name     | asc value | desc value |
      | 1      | classIdentifier | English   | Math       |
      | 2      | gradeLevel      | 3         | 4          |
      | 3      | sectionNumber   | 101       | 201        |

  @students @classes
  Scenario: Students tab when user has classes and students assigned to aforementioned classes
    Given I am logged in as "behat_testadmin"
    And I am assigned classes "[class:math],[class:eng]"
    And I am on the homepage
    And I click "Students"
    Then I should see "Math" in the "#datatable-2 caption" element
    And I should see "IL4560001" in the "#datatable-2 td.views-field-stateIdentifier" element
    And I should see "English" in the "#datatable-1 caption" element
    And I should see "IL4560002" in the "#datatable-1 td.views-field-stateIdentifier" element

  @parads-4329
  Scenario: Test admins with multiple Orgs see org-related data.
    Given I am logged in as "mr_hand"
    And I am assigned classes "[class:math],[class:eng],[class:sci]"

    # Home page dashboard contains separate student counts for each Org.
    When I am on the homepage
    And I reload the page
    And I select "XXD1S1" from "Organization"
    Then I should see "2" in the "#students-[org:XXD1S1]-count" element

    When I am on the homepage
    And I select "XXD1S2" from "Organization"
    And I press "Select"
    And I should see "1" in the "#students-[org:XXD1S2]-count" element

    And I select "XXD1S1" from "Organization"
    And I press "Select"
    When I click "classes-[org:XXD1S1]-count"
    Then I should be on "manage/[org:XXD1S1]/classes"
    And I should see an ".form-item-jump label" element
    #And I should see an "Showing classes for" form element
    And I should see "Math" in the ".view-pads-classes-assigned-to-users" element
    And I should not see "Science" in the ".view-pads-classes-assigned-to-users" element
    When I select "XXD1S2" from "Showing classes for"
    And I press "Go"
    Then I should be on "manage/[org:XXD1S2]/classes"
    And I should not see "Math" in the ".view-pads-classes-assigned-to-users" element
    And I should see "Science" in the ".view-pads-classes-assigned-to-users" element

    When I am on the homepage
    And I select "XXD1S2" from "Organization"
    And I press "Select"
    And I click "Students"
    Then I should be on "manage/[org:XXD1S2]/students"
    And I should see an ".form-item-jump label" element
    And I should see "Spicoli" in the ".view-pads-students-by-class" element
    And I should not see "Louis" in the ".view-pads-students-by-class" element
    And I should not see "gilbert" in the ".view-pads-students-by-class" element

    When I select "XXD1S1" from "Showing students for"
    And I press "Go"
    Then I should not see "Spicoli" in the ".view-pads-students-by-class" element
    And I should see "Louis" in the ".view-pads-students-by-class" element
    And I should see "Gilbert" in the ".view-pads-students-by-class" element

  @parads-5339
  Scenario Outline: Org admin assigned to State or Districts Orgs can not click on student count
    Given I am logged in as a user with the "organization administrator" role
    And I am assigned the organization "<user org>"
    When I visit "organizations/<view org>"

    #There is a students column
    Then the "table thead th:nth-child(6)" element should contain "Students"

    #I see the student count, verifies if user should see link
    And I should see "<student count>" in the "table tbody > tr > td:nth-child(6)" element
    And <see link> see an "table tbody > tr > td:nth-child(6) a" element
    Examples:
      | user org   | view org   | student count | see link   |
      | [org:XX]   | [org:XX]   | 3             | should not |
      | [org:XX]   | [org:XXD1] | 2             | should     |
      | [org:XXD1] | [org:XXD1] | 2             | should     |

  @parads-5339
  Scenario Outline: Non-school level organization administrator can not assign classes, view, or edit students
    Given I am logged in as a user with the "organization administrator" role
    And I am assigned the organization "<user org>"

    #Page to edit perform operations on students
    When I go to "students/[org:XXD1S1]"
    Then I should get a "<code>" HTTP response

    #View edit pages
    When I go to "student/[student:Louis Skolnick]"
    Then I should get a "<code>" HTTP response

    When I go to "student/[student:Louis Skolnick]/edit"
    Then I should get a "<code>" HTTP response
    Examples:
      | user org     | code |
      | [org:XX]     | 403  |
      | [org:XXD1]   | 200  |
      | [org:XXD1S1] | 200  |

  @ads-1380
  Scenario: PARCC District Org Admin
    # Dashboard, navigation item and student card with import action link.
    And I am logged in as a user with the "organization administrator" role
    And I am assigned the organization "[org:XXD1]"
    When I am on the homepage
    Then I should see the link "Students" in the "main_menu" region
    And I should see a "div.students .card-actions a" element

    # Students page.
    When I click "Students" in the "main_menu" region
    # Add and upload buttons available.
    Then I should see the link "Add Single Student"
    And I should see the link "Upload Students"
    # Filtering by school (default).
    And I should see the link "IL4560001"
    And I should see the link "IL4560002"
    And I should not see the link "IL4560003"
    # Filtering by school (select other option).
    When I select "XXD1S2" from "Showing students for"
    And I press "Go"
    Then I should not see the link "IL4560001"
    And I should not see the link "IL4560002"
    And I should see the link "IL4560003"
    # Able to add students.
    When I go to "student/add/4"
    Then I should get a 200 HTTP response
    # Able to view and edit all students in district.
    When I go to "student/[student:Louis Skolnick]"
    Then I should get a 200 HTTP response
    When I go to "student/[student:Louis Skolnick]/edit"
    Then I should get a 200 HTTP response
    When I go to "student/[student:Gilbert Lowell]"
    Then I should get a 200 HTTP response
    When I go to "student/[student:Gilbert Lowell]/edit"
    Then I should get a 200 HTTP response
    When I go to "student/[student:Jeff Spicoli]"
    Then I should get a 200 HTTP response
    When I go to "student/[student:Jeff Spicoli]/edit"
    Then I should get a 200 HTTP response
    # Access student import page.
    When I go to "students/import"
    Then I should get a 200 HTTP response

    # Schools Page, check that student counts are linked.
    When I click "Schools" in the "main_menu" region
    And I click 4 in the XXD1S1 row
    Then I am on "students/[org:XXD1S1]"
    When I click "Schools" in the "main_menu" region
    And I click 1 in the XXD1S2 row
    Then I am on "students/[org:XXD1S2]"

  @ads-1277 @tests @classes
  Scenario: Student pages grouped by class - section (count)
    Given LDR class orgs:
      | name               | section   | org          | grade | ref      |
      | Grand Funk Mastery | Developer | [org:XXD1S1] | 12    | dev      |
      | Grand Funk Mastery | Frontend  | [org:XXD1S1] | 12    | frontend |
      | Grand Funk Mastery | Backend   | [org:XXD1S1] | 12    | backend  |
      | Durpals 101        |           | [org:XXD1S1] | 12    | durpal   |
    And LDR students:
      | firstName | lastName | enrollOrgId  | dateOfBirth | gender | gradeLevel | stateIdentifier | classes                                      |
      | mario     | plumb    | [org:XXD1S1] | 2005-01-01  | M      | 12         | GFM0001         | [class:dev],[class:frontend],[class:backend] |
      | luigi     | plumb    | [org:XXD1S1] | 2005-01-01  | M      | 12         | GFM0002         | [class:dev],[class:frontend],[class:backend] |
      | yoshi     | boshi    | [org:XXD1S1] | 2005-01-01  | M      | 12         | GFM0003         | [class:dev],[class:frontend],[class:backend] |
      | princess  | daisy    | [org:XXD1S1] | 2005-01-01  | F      | 12         | DEV0001         | [class:dev]                                  |
      | dreis     | buyteart | [org:XXD1S1] | 2005-01-01  | F      | 12         | DEV0001         | [class:durpal]                               |

    And LDR tests:
      | name                | subject | grade | program               | security | permissions    | scoring   | description                 |
      | Certified Developer | Math    | 12    | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Test administered for Behat |
      | Backend Specialist  | Math    | 12    | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Test administered for Behat |
      | Frontend Specialist | Math    | 12    | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Test administered for Behat |
      | Durpal              | Math    | 12    | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Test administered for Behat |
    And LDR test assignments:
      | student                  | test                       | status    | ref |
      | [student:mario plumb]    | [test:Certified Developer] | Scheduled |     |
      | [student:mario plumb]    | [test:Backend Specialist]  | Scheduled |     |
      | [student:mario plumb]    | [test:Frontend Specialist] | Scheduled |     |
      | [student:luigi plumb]    | [test:Certified Developer] | Scheduled |     |
      | [student:luigi plumb]    | [test:Backend Specialist]  | Scheduled |     |
      | [student:luigi plumb]    | [test:Frontend Specialist] | Scheduled |     |
      | [student:yoshi boshi]    | [test:Certified Developer] | Scheduled |     |
      | [student:yoshi boshi]    | [test:Backend Specialist]  | Scheduled |     |
      | [student:yoshi boshi]    | [test:Frontend Specialist] | Scheduled |     |
      | [student:princess daisy] | [test:Certified Developer] | Scheduled |     |
      | [student:dreis buyteart] | [test:Durpal]              | Scheduled |     |

    Given I am logged in as "mr_hand"
    And I am assigned classes "[class:dev],[class:frontend],[class:backend],[class:durpal]"
    And I am on the homepage
    And I click "My Students"
    Then I should see 4 ".view.view-pads-students-by-class table" elements
    And the ".view.view-pads-students-by-class table:nth-child(1) caption" element should contain "Class: Durpals 101"
    And the ".view.view-pads-students-by-class table:nth-child(2) caption" element should contain "Class: Grand Funk Mastery - Backend"
    And the ".view.view-pads-students-by-class table:nth-child(3) caption" element should contain "Class: Grand Funk Mastery - Developer"
    And the ".view.view-pads-students-by-class table:nth-child(4) caption" element should contain "Class: Grand Funk Mastery - Frontend"

    #Scenario 4: Test Overview page has new class labels
    When I click "Tests" in the "main_menu" region
    Then I should see 4 ".view.view-pads-test-assignments table" elements
    And the "#datatable-1 > caption" element should contain "Class: Durpals 101 (1 Student)"
    And the "#datatable-2 > caption" element should contain "Class: Grand Funk Mastery - Backend (3 students)"
    And the "#datatable-3 > caption" element should contain "Class: Grand Funk Mastery - Developer (4 students)"
    And the "#datatable-4 > caption" element should contain "Class: Grand Funk Mastery - Frontend (3 students)"

    #test assignment page
    When I click "Tests" in the "main_menu" region
    And I click "Test Assignments"

    #Scenario 3: class filter has section number
    Then the "For class" select element should contain options "Durpals 101,Grand Funk Mastery - Backend,Grand Funk Mastery - Developer,Grand Funk Mastery - Frontend"
    #table title
    And the "#datatable-1 > caption:nth-child(1)" element should contain "Class: Durpals 101"

    When I select "Grand Funk Mastery - Backend" from "For class"
    And I press "edit-go--2"
    Then the "#datatable-1 > caption:nth-child(1)" element should contain "Class: Grand Funk Mastery - Backend"

    #Scenario 5: Print Access code page has class section number too
    When I click "Tests" in the "main_menu" region
    And I click the "#datatable-1 > tbody > tr > td.views-field.views-field-nothing-1 > a" element
    Then the "class" field should contain "Durpals 101"

    When I click "Tests" in the "main_menu" region
    And I click the "#datatable-2 > tbody > tr:nth-child(1) > td.views-field.views-field-nothing-1 > a" element
    Then the "class" field should contain "Grand Funk Mastery - Backend"
