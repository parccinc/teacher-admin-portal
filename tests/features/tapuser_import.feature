@api @tapuser @tapuser_import @import
Feature: TAP User import features.

  Background:
    Given LDR organizations:
      | name          | type     | identifier | parent              |
      | XX District 1 | district | 15515134   | XX                  |
      | School XX1a   | school   | 25515134   | [org:XX District 1] |
      | School XX1b   | school   | 25525134   | [org:XX District 1] |
    Given TAP users:
      | name                | pass  | mail                | roles                      | organizations |
      | Sir Patrick Stewart | behat | engage@makeitso.org | organization administrator | [org:XX]      |

  @javascript @parads-2445
  Scenario: Upload user file page from dashboard user box.
    Given I am logged in as "Sir Patrick Stewart"
    And I am on the homepage
    When I click "Import Users"
    Then I should be on "tapusers/import"
    And I see the heading "Upload Users"
    And I should see an "input[type='file']" element
    And I should see the link "Tap User Template"

  @ads-326
  Scenario: Tap user template
    Given I am logged in as "Sir Patrick Stewart"
    And I am on "tapusers/import"
    When I go to "/pads-import/download/tapuser_template.csv"
    Then the response should contain "First name,Last name,Email,Roles,State Code,Organization Identifiers"
    And the response should contain "Diana,Porter,dporter8@staff.a0001.edu,organization administrator,IL,a0001"
    And the response should contain "Jesse,Myers,jmyers3@staff.a0001.edu,test administrator,IL,ab0001"
    And the response should contain "Bonnie,Powell,bpowell@staff.a0001.edu,test administrator,IL,\"ab0001,ab0002\""
    And the response should not contain "PARCC user"
    And the response should not contain "TAP user"
    And the response should not contain "district test coordinator"
    And the response should not contain "non-school institution test coordinator"
    And the response should not contain "school institution test coordinator"

  @javascript
  Scenario: Import TAP user file.
    Given a file named "test_tapuser_import.csv" with:
    """
First name,Last name,Email,Roles,State Code,Organization Identifiers
Jesse,Myers,jmyers3@example.com,test administrator,XX,
Jesse,Myers,jmyers5@example.com,test administrator,XX,2
Howard,Fields,hfields4@example.com,test administrator,XX,"25515134,25525134"
Sandra,Fowler,sfowler5@example.com,lea district test coordinator,XX,"45,232,222"
Evelyn,Porter,eporter6@example.com,"school institution test coordinator,non-school institution test coordinator",XX,15515134
     """
    And I am logged in as "Sir Patrick Stewart"
    And I import the file "test_tapuser_import.csv" at "tapusers/import"
      #Then I should see the text "Total records in file: 5 :: 4 user imported successfully."
    Then I see the text "Total records in file:"
    And I see the text "Rejected Records"
    And I should see "Column organizations is required" in the "span.error" element


  @parads-3553 @parads-5199 @javascript
  Scenario: Test valid and invalid org levels for test admin
    Given a file named "test_tapuser_import.csv" with:
    """
First name,Last name,Email,Roles,State Code,Organization Identifiers
Bonnie,Kelley,testadmin1@example.com,test administrator,XX,25515134
Jesse,Myers,testadmin2@example.com,test administrator,XX,15515134
     """
    And I am logged in as "Sir Patrick Stewart"
    When I import the file "test_tapuser_import.csv" at "tapusers/import"
    Then I should not see the text "There are no rejected records to display."
    And I should see "Please select organizations at the School level for users in test administrator role." in the "span.error" element
    And I see the text "1 user imported successfully."

    # PARADS-5199, error table and CSV download headers.
    And I should see "First name" in the "#table_import_errors th:nth-of-type(1)" element
    And I should see "Last name" in the "#table_import_errors th:nth-of-type(2)" element
    And I should see "Email" in the "#table_import_errors th:nth-of-type(3)" element
    And I should see "Roles" in the "#table_import_errors th:nth-of-type(4)" element
    And I should see "State Code" in the "#table_import_errors th:nth-of-type(5)" element
    And I should see "Organization Identifiers" in the "#table_import_errors th:nth-of-type(6)" element
    And I should see "Error" in the "#table_import_errors th:nth-of-type(7)" element

    When I press "Download"
    Then my "tapuser" error export should contain:
    """
"First name","Last name",Email,Roles,"State Code","Organization Identifiers",Error
Jesse,Myers,testadmin2@example.com,"test administrator",XX,15515134,"Please select organizations at the School level for users in test administrator role."
    """

  @parads-5198 @javascript
  Scenario: Import Organization Access Validation
    Given LDR organizations:
      | name          | type     | identifier | parent              |
      | XX District 2 | district | 2          | XX                  |
      | School XX2a   | school   | 2a         | [org:XX District 2] |
      | School XX2b   | school   | 2b         | [org:XX District 2] |
    Given a file named "test_tapuser_import.csv" with:
    """
First name,Last name,Email,Roles,State Code,Organization Identifiers
Bonnie,Kelley,testadmin1@example.com,organization administrator,XX,15515134
Jesse,Myers,testadmin2@example.com,test administrator,XX,25515134
Debbie,Clayton,testadmin3@example.com,test administrator,XX,25525134
Ervin,Mccarthy,testadmin4@example.com,organization administrator,XX,2
Dennis,Yates,testadmin5@example.com,test administrator,XX,2a
Marilyn,Marsh,testadmin6@example.com,test administrator,XX,2b
     """
    And TAP users:
      | name       | mail                   | roles                      | organizations       |
      | Dist1Admin | dist1admin@example.com | organization administrator | [org:XX District 1] |
    And I am logged in as "Dist1Admin"
    When I import the file "test_tapuser_import.csv" at "tapusers/import"
    Then I see the text "3 users imported successfully."
    And I should see "is not within your associated Organizations" in the "Ervin" row
    And I should see "is not within your associated Organizations" in the "Dennis" row
    And I should see "is not within your associated Organizations" in the "Marilyn" row

  @parads-5202 @javascript
  Scenario: Creating state-level users
    Given a file named "test_tapuser_import.csv" with:
    """
First name,Last name,Email,Roles,State Code,Organization Identifiers
Bonnie,Kelley,orgadmin1@example.com,organization administrator,XX,15515134
Jesse,Myers,orgadmin2@example.com,organization administrator,XX
Palmer,Helms,orgadmin3@example.com,test administrator,XX
     """
    And I am logged in as "Sir Patrick Stewart"
    When I import the file "test_tapuser_import.csv" at "tapusers/import"
    Then I see the text "2 user imported successfully."
    And the user with email address "orgadmin1@example.com" should be assigned to organization "[org:XX District 1]"
    And the user with email address "orgadmin2@example.com" should be assigned to organization "[org:XX]"
    And I should see "Column organizations is required" in the "Palmer" row
    And I should see "Please select organizations at the School level for users in test administrator role" in the "Palmer" row

  @parads-2446 @ads-500 @javascript @sso @ads-695
  Scenario: All valid records
    Given a file named "valid_users.csv" with:
    """
First name,Last name,Email,Roles,State Code,Organization Identifiers
School,Orgadmin,school_orgadmin@example.com,organization administrator,XX,25515134
School,testadmin,school_testadmin@example.com,test administrator,XX,25515134
    """
    And I am logged in as "Sir Patrick Stewart"
    When I import the file "valid_users.csv" at "tapusers/import"
    Then I see the heading "Create User Accounts Confirmation"
    And I see the text "2 out of 2 users were created successfully."
    And I see the text "You can access the new user records."

    When I press "Back to User import"
    Then I should be on "tapusers/import"

    Then the LDAP user "school_orgadmin@example.com" should have a "parccroles" value of "organization administrator"
    Then the LDAP user "school_orgadmin@example.com" should have a "cn" value of "School Orgadmin"
    Then the LDAP user "school_orgadmin@example.com" should have a "givenname" value of "School"
    Then the LDAP user "school_orgadmin@example.com" should have a "sn" value of "Orgadmin"
    Then the LDAP user "school_orgadmin@example.com" should have a "parccorgs" value of "PARCC_XX_XX District 1_School XX1a"

  @ads-784 @javascript
  Scenario: duplicate mail in same file gets rejected
    Given a file named "test_tapuser_import.csv" with:
    """
First name,Last name,Email,Roles,State Code,Organization Identifiers
user,one,user_one@example.com,test administrator,XX,25515134
user,two,user_two@example.com,test administrator,XX,25515134
user,two,user_two@example.com,test administrator,XX,25525134
     """
    And I am logged in as "Sir Patrick Stewart"
    When I import the file "test_tapuser_import.csv" at "tapusers/import"
    Then I should see "user_two@example.com already exists in the file." in the "25515134" row
    And I should see "user_two@example.com already exists in the file." in the "25515134" row

  @ads-1336 @javascript
  Scenario: Wrong identifier does not put user at the state level!
    Given a file named "test_tapuser_import.csv" with:
    """
First name,Last name,Email,Roles,State Code,Organization Identifiers
user,one,user_one@example.com,organization administrator,XX,invalid1
user,two,user_two@example.com,test administrator,XX,invalid2
     """
    And I am logged in as "Sir Patrick Stewart"
    When I import the file "test_tapuser_import.csv" at "tapusers/import"
    Then I should see "Organization with identifier invalid1 in XX not found." in the "invalid1" row
    And I should see "Organization with identifier invalid2 in XX not found." in the "invalid2" row

  @ads-1535 @javascript
  Scenario: bad character import
    Given LDR organizations:
      | name   | type     | identifier      | parent     |
      | XXD1   | district | 050160670020000 | XX         |
      | XXD1S1 | school   | 050160670021002 | [org:XXD1] |


    And I am logged in as "Sir Patrick Stewart"
    When I import the file "tapuser_import_withinvalidchars.csv" at "tapusers/import"
    Then I should see "Malformed UTF-8 characters, possibly incorrectly encoded" in the "#table_import_errors tbody tr:nth-of-type(1) td:nth-of-type(7)" element
    And I should see "Malformed UTF-8 characters, possibly incorrectly encoded" in the "#table_import_errors tbody tr:nth-of-type(2) td:nth-of-type(7)" element
    And I should see "Malformed UTF-8 characters, possibly incorrectly encoded" in the "#table_import_errors tbody tr:nth-of-type(3) td:nth-of-type(7)" element
    And I should see "Malformed UTF-8 characters, possibly incorrectly encoded" in the "#table_import_errors tbody tr:nth-of-type(4) td:nth-of-type(7)" element
    And I should see "Organization with identifier 050160700000000 in XX not found." in the "#table_import_errors tbody tr:nth-of-type(5) td:nth-of-type(7)" element
    And I should see "Organization with identifier 050160700000000 in XX not found." in the "#table_import_errors tbody tr:nth-of-type(6) td:nth-of-type(7)" element

