@api @students
Feature: Functionality around adding a student.

  Background:
    Given LDR organizations:
      | name   | type     | identifier | parent     |
      | XXD1   | district | 1          | XX         |
      | XXD1S1 | school   | 2          | [org:XXD1] |
      | XXD1S2 | school   | 3          | [org:XXD1] |
    And TAP users:
      | name                  | pass  | mail                  | roles                      | organizations |
      | behat_testadmin       | behat | testadmin@example.com | test administrator         | [org:XXD1S1]  |
      | behat_school_orgadmin | behat | orgadmin@example.com  | organization administrator | [org:XXD1S1]  |
    And LDR class orgs:
      | name                 | section | org          | grade | ref    |
      | Discrete Mathematics | a-12345 | [org:XXD1S1] | 2     | dismat |
      | Integral Calculus    | b-12345 | [org:XXD1S1] | 2     | intcal |
      | Dinosaur Roaring     | c-12345 | [org:XXD1S1] | 1     | dinroa |
      | Hair Braiding        | d-12345 | [org:XXD1S1] | 1     | haibra |
    And LDR students:
      | firstName | lastName | enrollOrgId  | dateOfBirth | gender | gradeLevel | stateIdentifier | classes |
      | Louis     | Skolnick | [org:XXD1S1] | 2005-01-01  | M      | 3          | IL1230001       |         |
      | Gilbert   | Lowell   | [org:XXD1S1] | 2005-01-01  | M      | 3          | IL1230002       |         |


  @parads-2980 @student_add
  Scenario Outline: Add a Single Student button on my students page for Test Admins and Org Admins
    Given I am logged in as "<user name>"
    And I am on the homepage
    When I click "My Students"
    Then I should see the link "Add Single Student"
    Examples:
      | user name             |
      | behat_school_orgadmin |

  @parads-4155 @ads-1508 @student_add @ldr_cleanup
  Scenario: Add single student as an org admin
    Given I am logged in as "behat_school_orgadmin"
    And I am at "students/[org:XXD1S1]"
    When I click "Add Single Student"
    Then I should not see an "#edit-classassignment1" element

    When I fill in the following:
      | stateIdentifier   | 66649876   |
      | gradeLevel        | 7          |
      | firstName         | Tony       |
      | lastName          | Chopper    |
      | gender            | M          |
      | dateOfBirth[date] | 2004-05-16 |
    And press "Save Student Data"
    Then I should be on "students/[org:XXD1S1]"
    And I see the text "You have successfully added Tony Chopper. Click here to assign them to a class."

  @parads-4155 @parads-4356 @student_add @javascript @ldr_cleanup
  Scenario: Add single student as a test admin
    Given I am logged in as "behat_testadmin"
    And I am assigned classes "[class:dismat],[class:intcal]"
    When I am at "manage/[org:XXD1S1]/classes"
    Then the xpath '//*[@id="datatable-1"]/tbody/tr[1]/td[4]' element should contain "Add Student"
    When I click "Add Student" in the "Discrete Mathematics" row
    Then I should be on "student/add/[org:XXD1S1]"

    And I should not see an "Class" form element
    And I fill in the following:
      | stateIdentifier   | 66649876   |
      | gradeLevel        | 7          |
      | firstName         | Tony       |
      | lastName          | Chopper    |
      | gender            | M          |
      | dateOfBirth[date] | 2004-05-16 |
    And press "Save Student Data"
    Then I should be on "manage/[org:XXD1S1]/classes"
    And I see the text "You have successfully added Tony Chopper. Click here to assign them to a test"
    And student with stateIdentifier 66649876 is assigned to class "[class:dismat]"

    #PARADS-4356 Here link to create student test assignment
    When I click "here"
    And I wait for AJAX to finish
    Then I should see the text "You are assigning a test to Tony Chopper" in the "modal"

  @parads-4355 @javascript
  Scenario: As a school org admin, I want to add a student to a class after manually adding them to TAP so that test(s) can be assigned to them.
    Given I am logged in as "behat_school_orgadmin"
    And I am at "students/[org:XXD1S1]"
    And I click "Add Single Student"
    And I fill in the following:
      | stateIdentifier   | 66649876   |
      | gradeLevel        | 7          |
      | firstName         | Tony       |
      | lastName          | Chopper    |
      | gender            | M          |
      | dateOfBirth[date] | 2004-05-16 |
    And press "Save Student Data"
    When I click "here"
    Then I see the heading "Select classes to assign the following student:"
    And I should see "Tony Chopper" in the "form#views-form-pads-students-panel-pane-student-actions" element
    And I should see "Dinosaur Roaring" in the "form#views-form-pads-students-panel-pane-student-actions" element
    And I should see "Dinosaur Roaring" in the "form#views-form-pads-students-panel-pane-student-actions" element
    When I check "table[[class:dismat]]"
    And I check "table[[class:intcal]]"

    And I press "Next"
    And I wait for the batch job to finish
    Then I should see the success message "Performed Assign to Class on 1 item."

  @parads-4561
  Scenario: As a school org admin, I want to cancel adding class to newly added student
    Given I am logged in as "behat_school_orgadmin"
    And I am at "students/[org:XXD1S1]"
    And I click "Add Single Student"
    And I fill in the following:
      | stateIdentifier   | 66649876   |
      | gradeLevel        | 7          |
      | firstName         | Tony       |
      | lastName          | Chopper    |
      | gender            | M          |
      | dateOfBirth[date] | 2004-05-16 |
    And press "Save Student Data"
    And I click "here"
    When I click "Cancel"
    Then I should be on "students/[org:XXD1S1]"
