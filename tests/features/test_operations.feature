@api @tests @assignments
Feature: Test operations

  Background:
    Given LDR organizations:
      | name   | type     | identifier | parent     |
      | XXD6   | district | 6000       | XX         |
      | XXD6S1 | school   | 6001       | [org:XXD6] |
      | XXD6S2 | school   | 6002       | [org:XXD6] |
    And LDR class orgs:
      | name | section | org          | grade | ref  |
      | Math |         | [org:XXD6S1] | 3     | math |
    And LDR students:
      | firstName | lastName | enrollOrgId  | dateOfBirth | gender | gradeLevel | stateIdentifier | classes      |
      | Marnie    | Hummel   | [org:XXD6S1] | 2005-01-01  | F      | 3          | 4865MH          | [class:math] |
      | Abel      | Wade     | [org:XXD6S1] | 2005-02-02  | M      | 3          | 4865AW          | [class:math] |
      | Gordon    | Harris   | [org:XXD6S1] | 2005-03-03  | M      | 3          | 4865GH          | [class:math] |
      | Muriel    | Johnson  | [org:XXD6S1] | 2005-04-04  | F      | 3          | 4865MJ          | [class:math] |
      | Helen     | McKinney | [org:XXD6S1] | 2005-05-05  | F      | 3          | 4865HM          | [class:math] |
    And LDR tests:
      | name       | subject | grade | program               | security | permissions    | scoring   | description                 |
      | Behat test | ELA     | 3     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Test administered for Behat |
    And LDR test assignments:
      | student                  | test              | status     | ref       |
      | [student:Marnie Hummel]  | [test:Behat test] | Scheduled  | ta-marnie |
      | [student:Abel Wade]      | [test:Behat test] | InProgress | ta-abel   |
      | [student:Gordon Harris]  | [test:Behat test] | Paused     | ta-gordon |
      | [student:Muriel Johnson] | [test:Behat test] | Canceled   | ta-muriel |
      | [student:Helen McKinney] | [test:Behat test] | Submitted  | ta-helen  |
    And TAP users:
      | name        | pass  | mail                    | roles              | organizations |
      | ta_operator | behat | ta_operator@example.com | test administrator | [org:XXD6S1]  |

  @parads-5153
  Scenario: Students with a single canceled or submitted assignment shouldn't have "Generate access code"
    Given I am logged in as "ta_operator"
    And I am assigned classes "[class:math]"
    When I am on the homepage
    And I click "Students"

    And I should see "Generate access code" in the "Marnie" row
    And I should see "Generate access code" in the "Abel" row
    And I should see "Generate access code" in the "Gordon" row
    And I should not see "Generate access code" in the "Muriel" row
    And I should not see "Generate access code" in the "Helen" row

  @parads-5153 @parads-5330
  Scenario: Students with a multiple assignments and one canceled or submitted assignment shouldn't show those tests in "Generate access code"
    And LDR tests:
      | name        | subject | grade | program               | security | permissions    | scoring   | description                 |
      | Second test | ELA     | 3     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Test administered for Behat |
    And LDR test assignments:
      | student                  | test               | status     | ref       |
      | [student:Marnie Hummel]  | [test:Second test] | Scheduled  | ta-marnie |
      | [student:Abel Wade]      | [test:Second test] | InProgress | ta-abel   |
      | [student:Gordon Harris]  | [test:Second test] | Paused     | ta-gordon |
      | [student:Muriel Johnson] | [test:Second test] | InProgress | ta-muriel |
      | [student:Helen McKinney] | [test:Second test] | InProgress | ta-helen  |

    Given I am logged in as "ta_operator"
    And I am assigned classes "[class:math]"
    When I am on the homepage
    And I click "Students"

    # Check for "Generate access code" dropdowns.
    Then I should see "Generate access code" in the "Marnie" row
    And I should see "Generate access code" in the "Abel" row
    And I should see "Generate access code" in the "Gordon" row
    And I should see "Generate access code" in the "Muriel" row
    And I should see "Generate access code" in the "Helen" row
    And the "ul#ddb--5 li.first" element should contain "Behat test"
    And the "ul#ddb--5 li.last" element should contain "Second test"

    And the "ul#ddb--7 li.first" element should not contain "Behat test"
    And the "ul#ddb--7 li.last" element should contain "Second test"

    And the "ul#ddb--9 li.first" element should not contain "Behat test"
    And the "ul#ddb--9 li.last" element should contain "Second test"

    # Check for "View/Edit Test Assignments" dropdowns.
    And I should see "View/Edit Test Assignments" in the "Marnie" row
    And I should see "View/Edit Test Assignments" in the "Abel" row
    And I should see "View/Edit Test Assignments" in the "Gordon" row
    And I should see "View/Edit Test Assignments" in the "Muriel" row
    And I should see "View/Edit Test Assignments" in the "Helen" row

    And the "ul#ddb--2" element should contain "Behat Test"
    And the "ul#ddb--2" element should contain "Second Test"

    And the "ul#ddb--4" element should contain "Behat Test"
    And the "ul#ddb--4" element should contain "Second Test"

    And the "ul#ddb--6" element should contain "Behat Test"
    And the "ul#ddb--6" element should contain "Second Test"

    And the "ul#ddb--8" element should not contain "Behat Test"
    And the "ul#ddb--8" element should contain "Second Test"

    And the "ul#ddb--10" element should not contain "Behat Test"
    And the "ul#ddb--10" element should contain "Second Test"

  @parads-4865 @parads-3720 @parads-3645 @ads-1574
  Scenario: Various Test Operations Links
    # PARADS-3645: Unlock Test Status for Student - Basic
    # PARADS-3720: Cancel Test Assignment for a Student - UI
    # PARADS-4865: Unlock Test Status for Student - UI
    Given I am logged in as "ta_operator"
    And I am assigned classes "[class:math]"

    When I am on the homepage
    And I reload the page
    Then I click "tests-[org:XXD6S1]-count"
    And I should be on "manage/[org:XXD6S1]/tests/assignments/[class:math]"

    # Link visibility per test status, PARADS-3720 and PARADS-4865.
    Then I should see "Scheduled" in the "Marnie" row
    And I should see "Unlock Test" in the "Marnie" row
    And I should see "Cancel Test" in the "Marnie" row

    And I should see "InProgress" in the "Abel" row
    And I should see "Unlock Test" in the "Abel" row
    And I should see "Cancel Test" in the "Abel" row

    And I should see "Paused" in the "Gordon" row
    And I should see "Unlock Test" in the "Gordon" row
    And I should see "Cancel Test" in the "Gordon" row

    And I should see "Canceled" in the "Muriel" row
    And I should not see "Unlock Test" in the "Muriel" row

    And I should see "Submitted" in the "Helen" row
    And I should not see "Unlock Test" in the "Helen" row
    And I should not see "Cancel Test" in the "Helen" row

    # Execute the unlock test operation, PARADS-3645.
    # Popup added to "Scheduled" status, ADS-1574.
    When I click "Unlock Test" in the "Marnie" row
    Then I should see "IMPORTANT: Unlock Test should NOT be used when a student is actively taking a test. Prior to use, ensure that the student is unable to log into their test because it is locked."
    When I click "No, do not unlock test"
    Then I should see "Unlock Test" in the "Marnie" row
    When I click "Unlock Test" in the "Marnie" row
    And I click "Yes, unlock test"

    And I should see the message "The student's test was successfully unlocked."
    And I should see "Scheduled" in the "Marnie" row

    # Popup text modified, ADS-1574.
    When I click "Unlock Test" in the "Abel" row
    Then I should see "IMPORTANT: Unlock Test should NOT be used when a student is actively taking a test. Prior to use, ensure that the student is unable to log into their test because it is locked."
    When I click "No, do not unlock test"
    Then I should see "Unlock Test" in the "Abel" row
    When I click "Unlock Test" in the "Abel" row
    And I click "Yes, unlock test"

    And I should see the message "The student's test was successfully unlocked."
    And I should see "Paused" in the "Abel" row

    # Popup added to "Paused" status, ADS-1574.
    When I click "Unlock Test" in the "Gordon" row
    Then I should see "IMPORTANT: Unlock Test should NOT be used when a student is actively taking a test. Prior to use, ensure that the student is unable to log into their test because it is locked."
    When I click "No, do not unlock test"
    Then I should see "Unlock Test" in the "Gordon" row
    When I click "Unlock Test" in the "Gordon" row
    And I click "Yes, unlock test"

    And I should see the message "The student's test was successfully unlocked."
    And I should see "Paused" in the "Gordon" row

    # Ensure the unlock test operation endpoint does not work for tests at an
    # invalid status, PARADS-3645.
    When I go to "test-assignment/nojs/[asn:ta-muriel]/unlock"
    Then I should get a "403" HTTP response

    When I go to "test-assignment/nojs/[asn:ta-helen]/unlock"
    Then I should get a "403" HTTP response

  @parads-4877
  Scenario: Cancel Test Assignment for a Student - Basic
    Given I am logged in as "ta_operator"
    And I am assigned classes "[class:math]"

    When I am on the homepage
    And I reload the page
    Then I click "tests-[org:XXD6S1]-count"
    And I should be on "manage/[org:XXD6S1]/tests/assignments/[class:math]"

    When I click "Cancel Test" in the "Marnie" row
    Then I should see "Note: Please make sure that the student is logged out of the test if the status is In Progress before canceling the test. Are you sure you want to cancel Behat test for Marnie Hummel"
    When I click "No, do not cancel the test"
    And I click "Cancel Test" in the "Marnie" row
    And I click "Yes, cancel the test"
    Then I should see the message "Behat test has been canceled for Marnie Hummel"
    And I should see "Canceled" in the "Marnie" row

    When I click "Cancel Test" in the "Abel" row
    Then I should see "Note: Please make sure that the student is logged out of the test if the status is In Progress before canceling the test. Are you sure you want to cancel Behat test for Abel Wade"
    When I click "No, do not cancel the test"
    And I click "Cancel Test" in the "Abel" row
    And I click "Yes, cancel the test"
    Then I should see the message "Behat test has been canceled for Abel Wade"
    And I should see "Canceled" in the "Abel" row

    When I click "Cancel Test" in the "Gordon" row
    Then I should see "Note: Please make sure that the student is logged out of the test if the status is In Progress before canceling the test. Are you sure you want to cancel Behat test for Gordon Harris"
    When I click "No, do not cancel the test"
    And I click "Cancel Test" in the "Gordon" row
    And I click "Yes, cancel the test"
    Then I should see the message "Behat test has been canceled for Gordon Harris"
    And I should see "Canceled" in the "Gordon" row

    # Ensure the unlock test operation endpoint does not work for tests at an
    # invalid status.
    When I go to "test-assignment/nojs/[asn:ta-muriel]/cancel"
    Then I should get a "403" HTTP response

    When I go to "test-assignment/nojs/[asn:ta-helen]/cancel"
    Then I should get a "403" HTTP response

  @javascript @parads-3645 @parads-4877
  Scenario: Test operation modals
    Given I am logged in as "ta_operator"
    And I am assigned classes "[class:math]"

    When I am on the homepage
    And I reload the page
    Then I click "tests-[org:XXD6S1]-count"
    And I should be on "manage/[org:XXD6S1]/tests/assignments/[class:math]"

    # Unlock operation, cancel.
    When I click "Unlock Test" in the "Abel" row
    And I wait for AJAX to finish
    Then I should see a ".ctools-modal-content" element
    And I should see "Unlock Test" in the ".modal-header" element
    When I click "No, do not unlock test"
    Then I should not see a ".ctools-modal-content" element

    # Unlock operation, confirm.
    When I click "Unlock Test" in the "Abel" row
    And I wait for AJAX to finish
    Then I should see a ".ctools-modal-content" element
    And I should see "Unlock Test" in the ".modal-header" element
    When I click "Yes, unlock test"
    And I wait for AJAX to finish
    Then I should not see a ".ctools-modal-content" element
    And I should see the message "The student's test was successfully unlocked."

    # Cancel operation, cancel.
    When I click "Cancel Test" in the "Marnie" row
    And I wait for AJAX to finish
    And I wait 2 seconds
    Then I should see an ".ctools-modal-content" element
    And I should see "Cancel Test" in the ".modal-header" element
    When I click "No, do not cancel the test"
    And I wait 2 seconds
    Then I should not see a ".ctools-modal-content" element

    # Cancel operation, confirm.
    When I click "Cancel Test" in the "Marnie" row
    And I wait for AJAX to finish
    Then I should see a ".ctools-modal-content" element
    And I should see "Cancel Test" in the ".modal-header" element
    When I click "Yes, cancel the test"
    And I wait for AJAX to finish
    Then I should not see a ".ctools-modal-content" element
    And I should see the message "Behat test has been canceled for Marnie Hummel"

  @parads-3645 @parads-4877
  Scenario Outline: Test operation endpoint access
    Given TAP users:
      | name                | pass  | mail                            | roles              | organizations |
      | ta_operator_no_perm | behat | ta_operator_no_perm@example.com |                    | [org:XXD6S1]  |
      | ta_operator_no_org  | behat | ta_operator_no_org@example.com  | test administrator | [org:XXD6S2]  |
    And I am logged in as "<name>"

    When I go to "test-assignment/nojs/[asn:ta-marnie]/cancel"
    Then I should get a "403" HTTP response

    When I go to "test-assignment/nojs/[asn:ta-abel]/cancel"
    Then I should get a "403" HTTP response

    When I go to "test-assignment/nojs/[asn:ta-gordon]/cancel"
    Then I should get a "403" HTTP response

    When I go to "test-assignment/nojs/[asn:ta-muriel]/cancel"
    Then I should get a "403" HTTP response

    When I go to "test-assignment/nojs/[asn:ta-helen]/cancel"
    Then I should get a "403" HTTP response

    When I go to "test-assignment/nojs/[asn:ta-marnie]/unlock"
    Then I should get a "403" HTTP response

    When I go to "test-assignment/nojs/[asn:ta-abel]/unlock"
    Then I should get a "403" HTTP response

    When I go to "test-assignment/nojs/[asn:ta-gordon]/unlock"
    Then I should get a "403" HTTP response

    When I go to "test-assignment/nojs/[asn:ta-muriel]/unlock"
    Then I should get a "403" HTTP response

    When I go to "test-assignment/nojs/[asn:ta-helen]/unlock"
    Then I should get a "403" HTTP response

    Examples:
      | name                |
      | ta_operator_no_perm |
      | ta_operator_no_org  |
