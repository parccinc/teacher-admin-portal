@api @tapuser @tapuser_import @import @sso
Feature: ADS-524 User import in TAP with SSO
  As an organization administrator
  I want to roster other TAP users in ADS Teach and SSO

  Background:
    Given LDR organizations:
      | name          | type     | identifier | parent              |
      | XX District 1 | district | 15515134   | XX                  |
      | School XX1a   | school   | 25515134   | [org:XX District 1] |
      | School XX1b   | school   | 25525134   | [org:XX District 1] |
    Given TAP users:
      | name                | mail                | pass  | roles                      | organizations |
      | Sir Patrick Stewart | engage@makeitso.org | behat | organization administrator | [org:XX]      |

  @ads-524 @javascript
  Scenario: All valid records
    Given a file named "valid_users.csv" with:
    """
First name,Last name,Email,Roles,State Code,Organization Identifiers
School,Orgadmin,tapsso[timestamp]_orgadmin@example.com,organization administrator,XX,25515134
School,testadmin,tapsso[timestamp]_testadmin@example.com,test administrator,XX,25515134
    """
    And I am logged in as "Sir Patrick Stewart"
    When I import the file "valid_users.csv" at "tapusers/import"
    Then I see the heading "Create User Accounts Confirmation"
    And I see the text "2 out of 2 users were created successfully."
    And I see the text "You can access the new user records."
    When I press "Back to User import"
    Then I should be on "tapusers/import"
    And the LDAP user "tapsso[timestamp]_orgadmin@example.com" should have a "cn" value of "School Orgadmin"
    And the LDAP user "tapsso[timestamp]_orgadmin@example.com" should have a "givenname" value of "School"
    And the LDAP user "tapsso[timestamp]_orgadmin@example.com" should have a "sn" value of "Orgadmin"

    And the LDAP user "tapsso[timestamp]_testadmin@example.com" should have a "cn" value of "School testadmin"
    And the LDAP user "tapsso[timestamp]_testadmin@example.com" should have a "givenname" value of "School"
    And the LDAP user "tapsso[timestamp]_testadmin@example.com" should have a "sn" value of "testadmin"

    # Scenario 2, new TAP, existing SSO users
    # Delete the local user.
    And I delete the user "tapsso[timestamp]_orgadmin@example.com"
    Given a file named "valid_users_two.csv" with:
    """
First name,Last name,Email,Roles,State Code,Organization Identifiers
School1,Orgadmin1,tapsso[timestamp]_orgadmin@example.com,organization administrator,XX,25515134
    """
    And I am logged in as "Sir Patrick Stewart"
    # Another import with the first user.
    When I import the file "valid_users_two.csv" at "tapusers/import"
    Then I see the heading "Create User Accounts Confirmation"
    And I see the text "1 out of 1 users were created successfully."
    And I see the text "You can access the new user records."
    When I press "Back to User import"
    Then I should be on "tapusers/import"
    And the LDAP user "tapsso[timestamp]_orgadmin@example.com" should have a "cn" value of "School1 Orgadmin1"
    And the LDAP user "tapsso[timestamp]_orgadmin@example.com" should have a "givenname" value of "School1"
    And the LDAP user "tapsso[timestamp]_orgadmin@example.com" should have a "sn" value of "Orgadmin1"

  @ads-1617 @javascript
  Scenario Outline: email characters
    Given a file named "valid_users.csv" with:
    """
First name,Last name,Email,Roles,State Code,Organization Identifiers
School,Orgadmin,behat1617[timestamp]<char>1@example.com,organization administrator,XX,25515134
    """
    And I am logged in as "Sir Patrick Stewart"
    When I import the file "valid_users.csv" at "tapusers/import"
    Then I see the heading "Create User Accounts Confirmation"
    And I see the text "1 out of 1 users were created successfully."

    When I press "Back to User import"
    Then I should be on "tapusers/import"
    And the LDAP user "behat1617[timestamp]<char>1@example.com" should have a "cn" value of "School Orgadmin"
    And the LDAP user "behat1617[timestamp]<char>1@example.com" should have a "givenname" value of "School"
    And the LDAP user "behat1617[timestamp]<char>1@example.com" should have a "sn" value of "Orgadmin"

    #New user can update sso attribute
    When I visit "user/logout"
    And I am at one-time reset password page for "behat1617[timestamp]<char>1@example.com"
    And I press "Log in"

    #Sneaks in an apostrophe in the password
    And I fill in "o'brian" for "pass[pass1]"
    And I fill in "o'brian" for "pass[pass2]"
    And I press "Save"

    #Hard to query ldap for password, so let's just log in with new password
    And I visit "user/logout"
    And I log in to sso as "behat1617[timestamp]<char>1@example.com" with password "o'brian"

    Then I should be on "dashboard"

    Examples:
      | char |
      #| &    |
      #| !    |
      #|      |
      #| =    |
      #| <    |
      #| >    |
      #| ,    |
      | +    |
      #| -    |
      #| "    |
      #| '    |
      #| ;    |
      #| (    |
      #| )    |
      #| \    |
      #| *    |
      #| /    |
