@api @user_login
Feature: PARADS-543 Authentication required to access secure file page.
  As a user, I want to submit my credentials so I can access files associated with my account.

  Scenario: Anonymous user accessing file list view page.
    Given I am an anonymous user
    When I am on the homepage
    Then I see the text "Login"
    And I should see the "Log in" button

  Scenario: Authenticated user sees file list view page.
    Given I am logged in as a user with the "batch user" role
    When I am on "/files"
    Then I should see the " + Upload batch" button
    And I should see an "table" element

  @parads-5306
  Scenario: Log out on browser close
    Given I am logged in as a user with the "authenticated user" role
    When browser close cookies are expired
    And I go to the homepage
    Then I see the text "Login"
    And I should see the "Log in" button

  @parads-5306
  Scenario: Log out when activity idle
    Given the session timeout is set to "2"
    And I am logged in as a user with the "authenticated user" role
    When I sleep 3 seconds
    And I go to the homepage
    Then I see the text "Login"
    And I should see the "Log in" button

  @parads-5307 @mailsystem
  Scenario: Forgot password messaging
    Given users:
      | name  | pass  | mail              |
      | Valid | behat | valid@example.com |

    And I am an anonymous user

    When I go to "user/password"
    And I fill in "valid@example.com" for "E-mail"
    # Wait for Honeypot.
    And I sleep 5 seconds
    And I press "E-mail new password"
    Then I see the text "Password reset instructions have been sent to the email address on file."

    When I go to "user/password"
    And I fill in "invalid@example.com" for "E-mail"
    # Wait for Honeypot.
    And I sleep 5 seconds
    And I press "E-mail new password"
    Then I see the text "Password reset instructions have been sent to the email address on file."
