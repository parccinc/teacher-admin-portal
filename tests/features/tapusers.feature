@api @tapusers @ldr_orgs1
Feature: My User page and functionality.

  Background:
    Given LDR organizations:
      | name          | type     | identifier | parent              |
      | XX District 1 | district | 1          | XX                  |
      | School XX1a   | school   | 2          | [org:XX District 1] |
      | School XX1b   | school   | 3          | [org:XX District 1] |
    And TAP users:
      | name                | pass  | mail                | roles                      | organizations       |
      | Alpha               | behat | alpha@example.com   | test administrator         | [org:School XX1a]   |
      | Bravo               | behat | bravo@example.com   | organization administrator | [org:School XX1a]   |
      | Charlie             | behat | charlie@example.com | test administrator         | [org:School XX1b]   |
      | Delta               | behat | delta@example.com   | test administrator         | [org:School XX1b]   |
      | Sir Patrick Stewart | behat | makeitso@engage.net | organization administrator | [org:XX District 1] |


  @parads-2439 @javascript
  Scenario: Org admin sees elements of their My User page
    Given I am logged in as "Sir Patrick Stewart"
    And I am on the homepage

    # Wrist slap for viewing users at district level.
    When I click "user-[org:XX District 1]-count"
    And I wait 3 seconds
    Then I should see the warning message "Please click a user count link from an Organization at the school level."

    When I click "2" in the "School XX1b" row
    Then I should see the "My Users" heading in the "page title" region

    # Organization breadcrumbs - No breadcrumbs in AS
    # And I should see "XX" in the "ul.breadcrumbs li.first" element
    # And I should see "XX District 1" in the "ul.breadcrumbs li.last" element

    # School Organization dropdown.
    And I should see an "Showing users for" form element
    When I select "School XX1a" from "Showing users for"
    Then I am at "tapusers/[org:School XX1a]"

    And the xpath '//*[@id="datatable-1"]/thead/tr[1]' element should contain "Full Name"
    And the xpath '//*[@id="datatable-1"]/thead/tr[1]' element should contain "E-mail"
    And the xpath '//*[@id="datatable-1"]/thead/tr[1]' element should contain "Role"
    And the xpath '//*[@id="datatable-1"]/thead/tr[1]' element should contain "Operations"

  @parads-2439 @parads-3783 @javascript
  Scenario Outline: User table column headers are sortable
    Given I am logged in as "Sir Patrick Stewart"
    And I am on "tapusers/[org:School XX1a]"

    # Click on column header for default asc sort.
    When I click the "th.views-field-<column name>" element
    Then the xpath '//*[@id="datatable-1"]/tbody/tr[1]/td[<column>]' element should contain "<asc value>"
    And the xpath '//*[@id="datatable-1"]/tbody/tr[2]/td[<column>]' element should contain "<desc value>"

    # Click on column header again for desc order.
    When I click the "th.views-field-<column name>" element
    Then the xpath '//*[@id="datatable-1"]/tbody/tr[1]/td[<column>]' element should contain "<desc value>"
    And the xpath '//*[@id="datatable-1"]/tbody/tr[2]/td[<column>]' element should contain "<asc value>"

    Examples:
      | column | column name     | asc value                  | desc value         |
      | 2      | field-full-name | Alpha                      | Bravo              |
      | 3      | mail            | alpha@example.com          | bravo@example.com  |
      | 4      | pads-tap-role   | organization administrator | test administrator |

  @parads-5190 @user-edit
  Scenario: Sanity check - org admin can't edit users not in organization
    Given LDR organizations:
      | name          | type     | identifier | parent              |
      | XX District 2 | district | 4          | XX                  |
      | School XX2a   | school   | 5          | [org:XX District 2] |
    And TAP users:
      | name      | pass  | mail                  | roles              | organizations     |
      | Elijah    | behat | elijah@example.com    | test administrator | [org:School XX2a] |
      | SchoolOrg | behat | schoolorg@example.com | test administrator | [org:School XX2a] |
    Given users:
      | name      | pass  | mail                  | roles      |
      | batchuser | behat | batchuser@example.com | batch user |

    And I am logged in as "Sir Patrick Stewart"
    When I am on "tapusers/[org:School XX1a]"

    #I don't see Elijah from a different Org.
    Then the ".view-tap-user-actions" element should not contain "Elijah"

    #I can edit my users, but not others and definitely not the admin user.
    When I click "edit" in the "Alpha" row
    Then I see the heading "Alpha"

    #Can't edit user from a different Org
    When  I am on the edit page for user "Elijah"
    Then I should get a 403 HTTP response

    #Can't edit a non tap role user
    When I am on the edit page for user "batchuser"
    Then I should get a 403 HTTP response

    #Can't edit user 1
    When I go to "user/1/edit"
    Then I should get a 403 HTTP response

    #I can't add users either, that would be crazy
    When I go to "admin/people/create"
    Then I should get a 403 HTTP response

  @ads-1352 @ads-1550
  Scenario: User edit, user status and available user roles
    # Covers ADS-1352
    #   Scenario 2: Role radio button visibility
    #   Scenario 3: Edit user status
    #   Scenario 4: Edit user role
    #   Scenario 5: Prompt for confirmation when changing role (losing classes)
    And TAP users:
      | name          | pass  | mail                     | roles                      | organizations       |
      | me            | behat | me@example.com           | ads administrator          | 1                   |
      | nat user      | behat | natuser@example.com      | ads administrator          | 1                   |
      | state user    | behat | stateuser@example.com    | organization administrator | 2                   |
      | district user | behat | districtuser@example.com | organization administrator | [org:XX District 1] |
      | school user   | behat | schooluser@example.com   | test administrator         | [org:School XX1a]   |
    And LDR class orgs:
      | name    | section | org               | grade | ref     |
      | English | Lit 1   | [org:School XX1a] | 10    | english |
      | Math    |         | [org:School XX1a] | 6     | math    |
    And user "school user" is assigned classes "[class:math],[class:english]"

    When I am logged in as "me"
    And I am on the edit page for user "nat user"
    # Check for form inputs.
    Then I should see the "Active" radio
    And I should see the "Blocked" radio
    And I should see the "ADS Administrator" radio
    And I should see the "ADS Administrator + Content Editor" radio
    And I should see the "Content Editor only" radio
    And I should not see the "Organization Administrator" radio
    And I should not see the "Test Administrator" radio
    And I should not see the "Test Administrator (Can view restricted batteries)" radio
    # Disable.
    When I select the radio button "Blocked"
    And I press "Save"
    Then the user "natuser@example.com" should be disabled
    # Activate.
    When I am on the edit page for user "nat user"
    And I select the radio button "Active"
    And I press "Save"
    Then the user "natuser@example.com" should be activated
    # Set to "ADS Administrator + Content Editor" role.
    When I am on the edit page for user "nat user"
    And I select the radio button "ADS Administrator + Content Editor"
    And I press "Save"
    Then the user "natuser@example.com" should have the role "ads administrator"
    And the user "natuser@example.com" should have the role "ads content editor"
    And the user "natuser@example.com" should not have the role "organization administrator"
    And the user "natuser@example.com" should not have the role "test administrator"
    And the user "natuser@example.com" should not have the role "test form qc"
    # Set to "Content Editor only" role.
    When I am on the edit page for user "nat user"
    And I select the radio button "Content Editor only"
    And I press "Save"
    Then the user "natuser@example.com" should not have the role "ads administrator"
    And the user "natuser@example.com" should have the role "ads content editor"
    And the user "natuser@example.com" should not have the role "organization administrator"
    And the user "natuser@example.com" should not have the role "test administrator"
    And the user "natuser@example.com" should not have the role "test form qc"
    # Set to "ADS Administrator" role.
    When I am on the edit page for user "nat user"
    And I select the radio button "ADS Administrator"
    And I press "Save"
    Then the user "natuser@example.com" should have the role "ads administrator"
    And the user "natuser@example.com" should not have the role "ads content editor"
    And the user "natuser@example.com" should not have the role "organization administrator"
    And the user "natuser@example.com" should not have the role "test administrator"
    And the user "natuser@example.com" should not have the role "test form qc"

    And I am on the edit page for user "state user"
    Then I should see the "Active" radio
    And I should see the "Blocked" radio
    And I should not see the "ADS Administrator" radio
    And I should not see the "ADS Administrator + Content Editor" radio
    And I should not see the "Content Editor only" radio
    And I should see the "Organization Administrator" radio
    And I should not see the "Test Administrator" radio
    And I should not see the "Test Administrator (Can view restricted batteries)" radio
    # Disable.
    When I select the radio button "Blocked"
    And I press "Save"
    Then the user "stateuser@example.com" should be disabled
    # Activate.
    When I am on the edit page for user "state user"
    And I select the radio button "Active"
    And I press "Save"
    Then the user "stateuser@example.com" should be activated
    # Only org admin, no other roles to switch.

    And I am on the edit page for user "district user"
    Then I should see the "Active" radio
    And I should see the "Blocked" radio
    And I should not see the "ADS Administrator" radio
    And I should not see the "ADS Administrator + Content Editor" radio
    And I should not see the "Content Editor only" radio
    And I should see the "Organization Administrator" radio
    And I should not see the "Test Administrator" radio
    And I should not see the "Test Administrator (Can view restricted batteries)" radio
    # Disable.
    When I select the radio button "Blocked"
    And I press "Save"
    Then the user "districtuser@example.com" should be disabled
    # Activate.
    When I am on the edit page for user "district user"
    And I select the radio button "Active"
    And I press "Save"
    Then the user "districtuser@example.com" should be activated
    # Only org admin, no other roles to switch.

    And I am on the edit page for user "school user"
    Then I should see the "Active" radio
    And I should see the "Blocked" radio
    And I should not see the "ADS Administrator" radio
    And I should not see the "ADS Administrator + Content Editor" radio
    And I should not see the "Content Editor only" radio
    And I should see the "Organization Administrator" radio
    And I should see the "Test Administrator" radio
    And I should see the "Test Administrator (Can view restricted batteries)" radio
    # Disable.
    When I select the radio button "Blocked"
    And I press "Save"
    Then the user "schooluser@example.com" should be disabled
    # Activate.
    When I am on the edit page for user "school user"
    And I select the radio button "Active"
    And I press "Save"
    Then the user "schooluser@example.com" should be activated
    # Set to "Organization Administrator" role, not confirmed.
    When I am on the edit page for user "school user"
    And I select the radio button "Organization Administrator"
    And I press "Save"
    Then I should see "school user is assigned to one or more classes:"
    And I should see "English - Lit 1"
    And I should see "Math"
    And I should see "If you continue, this user will no longer have access to these classes"
    And I should see "Are you sure you want to continue?"
    And I press "No"
    Then I should not see "The changes have been saved."
    And the user "schooluser@example.com" should not have the role "ads administrator"
    And the user "schooluser@example.com" should not have the role "ads content edtior"
    And the user "schooluser@example.com" should not have the role "organization administrator"
    And the user "schooluser@example.com" should have the role "test administrator"
    And the user "schooluser@example.com" should not have the role "test form qc"
    And user "school user" should be assigned to classes "[class:math],[class:english]"
    # Set to "Organization Administrator" role, confirmed.
    And I select the radio button "Organization Administrator"
    And I press "Save"
    Then I should see "school user is assigned to one or more classes:"
    And I should see "English - Lit 1"
    And I should see "Math"
    And I should see "If you continue, this user will no longer have access to these classes"
    And I should see "Are you sure you want to continue?"
    And I press "Yes"
    Then I should see "The changes have been saved."
    And the user "schooluser@example.com" should not have the role "ads administrator"
    And the user "schooluser@example.com" should not have the role "ads content edtior"
    And the user "schooluser@example.com" should have the role "organization administrator"
    And the user "schooluser@example.com" should not have the role "test administrator"
    And the user "schooluser@example.com" should not have the role "test form qc"
    And user "school user" should not be assigned to classes "[class:math],[class:english]"
    # Set to "Test Administrator" role, no prompt.
    When I am on the edit page for user "school user"
    And I select the radio button "Test Administrator"
    And I press "Save"
    Then I should see "The changes have been saved."
    And the user "schooluser@example.com" should not have the role "ads administrator"
    And the user "schooluser@example.com" should not have the role "ads content edtior"
    And the user "schooluser@example.com" should not have the role "organization administrator"
    And the user "schooluser@example.com" should have the role "test administrator"
    And the user "schooluser@example.com" should not have the role "test form qc"
    And user "school user" should not be assigned to classes "[class:math],[class:english]"
    # Set to "Test Administrator (Can view restricted batteries)" role, no prompt.
    When user "school user" is assigned classes "[class:math],[class:english]"
    And I am on the edit page for user "school user"
    And I select the radio button "Test Administrator (Can view restricted batteries)"
    And I press "Save"
    Then I should see "The changes have been saved."
    And the user "schooluser@example.com" should not have the role "ads administrator"
    And the user "schooluser@example.com" should not have the role "ads content edtior"
    And the user "schooluser@example.com" should not have the role "organization administrator"
    And the user "schooluser@example.com" should have the role "test administrator"
    And the user "schooluser@example.com" should have the role "test form qc"
    And user "school user" should be assigned to classes "[class:math],[class:english]"

    # ADS-1550, org admin not able to see status/role.
    When I am logged in as "state user"
    And I am on the edit page for user "district user"
    Then I should not see the "Active" radio
    And I should not see the "Blocked" radio
    And I should not see the "ADS Administrator" radio
    And I should not see the "ADS Administrator + Content Editor" radio
    And I should not see the "Content Editor only" radio
    And I should not see the "Organization Administrator" radio
    And I should not see the "Test Administrator" radio
    And I should not see the "Test Administrator (Can view restricted batteries)" radio

    # ADS-1550, org admin not able to see status/role.
    When I am logged in as "district user"
    And I am on the edit page for user "school user"
    Then I should not see the "Active" radio
    And I should not see the "Blocked" radio
    And I should not see the "ADS Administrator" radio
    And I should not see the "ADS Administrator + Content Editor" radio
    And I should not see the "Content Editor only" radio
    And I should not see the "Organization Administrator" radio
    And I should not see the "Test Administrator" radio
    And I should not see the "Test Administrator (Can view restricted batteries)" radio

  @ads-1352
  Scenario Outline: Do not show User Status & Role When Viewing Own Profile
  # Covers ADS-1352
  #   SCENARIO 6: Do not show User Status & Role When Viewing Own Profile
    And TAP users:
      | name          | pass  | mail                     | roles                      | organizations       |
      | me            | behat | me@example.com           | ads administrator          | 1                   |
      | nat user      | behat | natuser@example.com      | ads administrator          | 1                   |
      | state user    | behat | stateuser@example.com    | organization administrator | 2                   |
      | district user | behat | districtuser@example.com | organization administrator | [org:XX District 1] |
      | school user   | behat | schooluser@example.com   | test administrator         | [org:School XX1a]   |
    And LDR class orgs:
      | name    | section | org               | grade | ref     |
      | English | Lit 1   | [org:School XX1a] | 10    | english |
      | Math    |         | [org:School XX1a] | 6     | math    |
    And user "school user" is assigned classes "[class:math],[class:english]"

    Given I am logged in as "<name>"
    When I am on the edit page for user "<name>"
    Then I should not see the "Active" radio
    And I should not see the "Blocked" radio
    And I should not see the "ADS Administrator" radio
    And I should not see the "ADS Administrator + Content Editor" radio
    And I should not see the "Content Editor only" radio
    And I should not see the "Organization Administrator" radio
    And I should not see the "Test Administrator" radio
    And I should not see the "Test Administrator (Can view restricted batteries)" radio
    Examples:
      | name          |
      | nat user      |
      | state user    |
      | district user |
      | school user   |