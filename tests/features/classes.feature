@api @classes
Feature: Class import and other functionality.

  Background:
    Given LDR organizations:
      | name          | type     | identifier | parent              |
      | XX District 1 | district | 1          | XX                  |
      | School XX1a   | school   | 2          | [org:XX District 1] |
      | School XX1b   | school   | 3          | [org:XX District 1] |
      | XX District 2 | district | 4          | XX                  |
      | School XX2a   | school   | 5          | [org:XX District 2] |
    And TAP users:
      | name                  | pass  | mail                        | roles                      | organizations                       |
      | behat_orgadmin        | behat | orgadmin@example.com        | organization administrator | [org:XX District 1]                 |
      | behat_tapuser         | behat | tapuser@example.com         | organization administrator | [org:School XX1a],[org:School XX1b] |
      | behat_school_orgadmin | behat | school_orgadmin@example.com | organization administrator | [org:School XX1a]                   |

  @parads-5094 @parads-5079 @parads-5094 @parads-5095 @class_import @import @javascript
  Scenario: School level orgadmin can not import class from a different school
    #The file has a blank line just to show it gets ignored (parads-5079).
    Given a file named "classes-nicetry.csv" with:
    """
    organizationIdentifier,classIdentifier,gradeLevel,sectionNumber
2,Grade 3 Spanish 1,3,201

3,Grade 3 Spanish 2,3,301
5,Grade 3 Spanish 3,3,501
    """
    And I am logged in as "behat_school_orgadmin"
    When I import the file "classes-nicetry.csv" at "class/import"
    Then I should see "Access to class Organization is required" in the "Grade 3 Spanish 2" row
    Then I should see "Access to class Organization is required" in the "Grade 3 Spanish 3" row
    And I should not see "Grade 3 Spanish 1" in the "#table_import_errors" element

  @class_import @import @parads-2423
  Scenario: PARADS-2423 Upload classes UI story.
    Given I am logged in as "behat_orgadmin"
    When I am on the homepage
    And I follow "Import Classes"
    Then I should be on "class/import"
    And I see the heading "Upload Classes"
    And I see the text "To ensure a successful upload, make sure your CSV file is in Class Template format. (Click to download sample .csv format file.)"
    And I should see the link "Class Template"
    And I should see an ".form-item.form-type-plupload.form-item-file" element
    And I see the text "Files must be less than 2 MB."
    And I see the text "Allowed file types: csv"

  @javascript @parads-2424 @class_import @import
  Scenario: PARADS-2424 Successful class import
    Given a file named "classes.csv" with:
    """
    organizationIdentifier,classIdentifier,gradeLevel,sectionNumber
2,Grade 3 Spanish 1,3,g3sp1-[timestamp]
2,Grade 3 Spanish 2,3,g3sp2-[timestamp]
    """
    And I am logged in as "behat_orgadmin"
    When I import the file "classes.csv" at "class/import"
    Then I see the heading "Upload Class Data Confirmation"
    And I see the text "2 out of 2 classes were created successfully."
    And I see the text "You can add more classes"
    #When I call the Ldr service "getClasses" with JSON args '{"organizationId":4862}'
    #Then I should find key|value pair "sectionNumber|g3sp1-[timestamp]" in the "classes" property of Ldr response
    # Click the Add More Classes button
    When I press "Add More Classes"
    Then I am on "class/import"

  @javascript @parads-2425 @parads-4833 @class_import @import
  Scenario: PARADS-2425 Class import with bad records
    Given a file named "classes-dupsection.csv" with:
    """
    organizationIdentifier,classIdentifier,gradeLevel,sectionNumber
2,Grade 3 Spanish 1,3,g3sp1-[timestamp]
2,Grade 3 Spanish 2,3,g3sp1-[timestamp]
2,,3,
    """
    And I am logged in as "behat_orgadmin"
    When I import the file "classes-dupsection.csv" at "class/import"
    Then I see the heading "Upload Class Data Confirmation with Exceptions"
    And I should see the warning message "Some records could not be uploaded. To ensure a successful upload, make sure your CSV file is in Class Template format. Please see the details below."
    And I see the text "A class for organizationId [org:School XX1a] with section number g3sp1-[timestamp] already exists"
    And I see the text "Validation errors: [classIdentifier] is a required string"
    # Click the Add More Classes button
    When I press "Add More Classes"
    Then I am on "class/import"

  @javascript @parads-2779 @import @class_import
  Scenario:
    Given I am logged in as "behat_orgadmin"
    And a file named "classes.csv" with:
    """
    organizationIdentifier,classIdentifier,gradeLevel,sectionNumber
2,Grade 3 Spanish 1,3,g3sp1-[timestamp]
2,Grade 2 Spanish,2,g2sp1-[timestamp]
3,Grade 2 Spanish,2,g2sp2-[timestamp]
    """
    When I import the file "classes.csv" at "class/import"
    And I press "View My Classes"
    Then I should be on "classes/[org:XX District 1]"

  @javascript @parads-2779 @class_page @parads-4071
  Scenario: PARADS-2779 Header and dropdown Elements on the All Classes page
    Given LDR class orgs:
      | name    | section  | org               | grade | ref     |
      | Math    | math1    | [org:School XX1a] | 3     | Math    |
      | English | english1 | [org:School XX1b] | 7     | English |

    And I am logged in as "behat_tapuser"
    When I am at "classes/[org:School XX1a]"

    #class dropdowndown
    Then I should see the heading "All Classes"
    And I should see the 'option[selected="selected"]' element with the "value" attribute set to ldr fixed "/classes/[org:School XX1a]" in the content region
    When I select "/classes/[org:School XX1b]" from "Showing classes for"
    Then I am at "classes/[org:School XX1b]"
    And I should see the 'option[selected="selected"]' element with the "value" attribute set to ldr fixed "/classes/[org:School XX1b]" in the content region

    #other elements
    And I should see "Class name" in the "Section" row
    And I should see the heading "All Classes"

  @ads-1291
  Scenario: Add Upload Classes button
    Given I am logged in as "behat_school_orgadmin"
    And I am on the homepage
    And I click "My Classes"
    When I click "Upload Classes"
    Then I should be on "class/import"
