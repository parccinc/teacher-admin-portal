@api @ads-710 @sso @dmrs @user
Feature: ADS-710 Create new User View and Menu Item for DMRS Admin
  As a State-level DMRS Admin,
  I need a page listing DMRS Users in my State only, and a menu item to get to that page.

  #If these tests are failing, it may be because of the rid value in the views
  #feature module. Make sure admin/structure/views/view/administration_dmrs_users/edit
  #has a role filter with the value 'dmrs user'.

  Background:
    Given LDR organizations:
      | name          | type     | identifier | parent              |
      | XX District 1 | district | 15515134   | XX                  |
      | School XX1a   | school   | 25515134   | [org:XX District 1] |
      | School XX1b   | school   | 25525134   | [org:XX District 1] |
    Given TAP users:
      | name                | mail                    | pass  | roles              | organizations |
      | Sir Patrick Stewart | engage@makeitso.org     | behat | dmrs administrator | [org:XX]      |
      | Sir Ian McKellen    | magneto@evilmutants.org | behat | test administrator | [org:XX]      |

  Scenario: DMRS Admins have Admin DMRS Users link
    Given I am logged in as "Sir Patrick Stewart"
    Then I should see the link "Admin DMRS Users"

  Scenario: Non-DMRS Admins have no Admin DMRS Users link
    Given I am logged in as "Sir Ian McKellen"
    Then I should not see the link "Admin DMRS Users"

  Scenario: Admin DMRS User page
    Given TAP users:
      | name    | pass  | mail                            | roles              | organizations     |
      | Alpha   | behat | alpha_[timestamp]@example.com   | dmrs administrator | [org:School XX1a] |
      | Bravo   | behat | bravo_[timestamp]@example.com   | dmrs user          | [org:School XX1a] |
      | Charlie | behat | charlie_[timestamp]@example.com | dmrs user          | [org:School XX1b] |
      | Delta   | behat | delta_[timestamp]@example.com   | dmrs administrator | [org:School XX1b] |
    And I am logged in as "Sir Patrick Stewart"
    When I click "Admin DMRS Users"
    Then I should not see the link "Sir Patrick Stewart"
    And I should not see the link "Sir Ian McKellen"
    And I should not see the link "Alpha"
    And I should not see the link "Delta"
    But I should see the link "Bravo"
    And I should see the link "Charlie"
    And I should not see a "Role" element
    And I should not see an "Operations" element
    And I should see the link "edit"
    # TODO: Determine requirements on canceling user accounts
    And I should not see the link "Cancel account"

  Scenario: Admin DMRS User can't edit user roles
    Given TAP users:
      | name    | pass  | mail                            | roles              | organizations     |
      | Alpha   | behat | alpha_[timestamp]@example.com   | test administrator | [org:School XX1a] |
      | Bravo   | behat | bravo_[timestamp]@example.com   | dmrs user          | [org:School XX1a] |
      | Charlie | behat | charlie_[timestamp]@example.com | dmrs user          | [org:School XX1b] |
      | Delta   | behat | delta_[timestamp]@example.com   | test administrator | [org:School XX1b] |
    And I am logged in as "Sir Patrick Stewart"
    When I click "Admin DMRS Users"
    And I click "edit"
    Then I should not see the text "Roles"
    And I should not see the link "Batchuser Organization"
    And I should not see the link "PADS TAP Organization"
    But I should see the link "DMRS Profile"

  Scenario: DMRS User can't edit non-DMRS users
    Given TAP users:
      | name    | pass  | mail                            | roles              | organizations     |
      | Alpha   | behat | alpha_[timestamp]@example.com   | test administrator | [org:School XX1a] |
      | Bravo   | behat | bravo_[timestamp]@example.com   | dmrs user          | [org:School XX1a] |
      | Charlie | behat | charlie_[timestamp]@example.com | dmrs user          | [org:School XX1b] |
      | Delta   | behat | delta_[timestamp]@example.com   | test administrator | [org:School XX1b] |
    And I am logged in as "Sir Patrick Stewart"
    When I am editing the "dmrs_profile" profile for the user with the email "alpha_[timestamp]@example.com"
    Then I should not see the link "Account"

  Scenario: DMRS User can edit DMRS users
    Given TAP users:
      | name    | pass  | mail                            | roles              | organizations     |
      | Alpha   | behat | alpha_[timestamp]@example.com   | test administrator | [org:School XX1a] |
      | Bravo   | behat | bravo_[timestamp]@example.com   | dmrs user          | [org:School XX1a] |
      | Charlie | behat | charlie_[timestamp]@example.com | dmrs user          | [org:School XX1b] |
      | Delta   | behat | delta_[timestamp]@example.com   | test administrator | [org:School XX1b] |
    And I am logged in as "Sir Patrick Stewart"
    When I am editing the "dmrs_profile" profile for the user with the email "bravo_[timestamp]@example.com"
    Then I should see the link "Account"

  Scenario: DMRS User can edit self
    Given I am logged in as "Sir Patrick Stewart"
    When I click "My account"
    Then I should see the link "Edit"

  @ads-741
  Scenario: DMRS User Admin only sees DMRS users from own state
    Given LDR organizations:
      | name | type  | identifier | parent |
      | WI   | state | WI         | 1      |
      | IL   | state | IL         | 1      |
    And TAP users:
      | name    | pass  | mail                            | roles              | organizations       |
      | Alpha   | behat | alpha_[timestamp]@example.com   | test administrator | [org:XX]            |
      | Bravo   | behat | bravo_[timestamp]@example.com   | dmrs user          | [org:XX]            |
      | Billie  | behat | billie_[timestamp]@example.com  | dmrs user          | [org:XX District 1] |
      | Bobby   | behat | bobby_[timestamp]@example.com   | dmrs user          | [org:School XX1a]   |
      | Charlie | behat | charlie_[timestamp]@example.com | dmrs user          | [org:WI]            |
      | Delta   | behat | delta_[timestamp]@example.com   | test administrator | [org:WI]            |
      | Echo    | behat | echo_[timestamp]@example.com    | dmrs user          | [org:IL]            |
      | Foxtrot | behat | foxtrot_[timestamp]@example.com | test administrator | [org:IL]            |
    And I am logged in as "Sir Patrick Stewart"
    When I click "Admin DMRS Users"
    Then I should not see the link "Sir Patrick Stewart"
    And I should not see the link "Sir Ian McKellen"
    And I should not see the link "Alpha"
    And I should not see the link "Delta"
    And I should not see the link "Charlie"
    And I should not see the link "Echo"
    And I should not see the link "Foxtrot"
    But I should see the link "Bravo"
    And I should see the link "Billie"
    And I should see the link "Bobby"

    @ads-767 @javascript
    Scenario: Reset button on dmrs admin view
      Given LDR organizations:
        | name | type  | identifier | parent |
        | WI   | state | WI         | 1      |
        | IL   | state | IL         | 1      |
      And TAP users:
        | name    | pass  | mail                            | roles              | organizations       |
        | Alpha   | behat | alpha_[timestamp]@example.com   | test administrator | [org:XX]            |
        | Bravo   | behat | bravo_[timestamp]@example.com   | dmrs user          | [org:XX]            |
        | Billie  | behat | billie_[timestamp]@example.com  | dmrs user          | [org:XX District 1] |
        | Bobby   | behat | bobby_[timestamp]@example.com   | dmrs user          | [org:School XX1a]   |
        | Charlie | behat | charlie_[timestamp]@example.com | dmrs user          | [org:WI]            |
        | Delta   | behat | delta_[timestamp]@example.com   | test administrator | [org:WI]            |
        | Echo    | behat | echo_[timestamp]@example.com    | dmrs user          | [org:IL]            |
        | Foxtrot | behat | foxtrot_[timestamp]@example.com | test administrator | [org:IL]            |
      And I am logged in as "Sir Patrick Stewart"
      When I click "Admin DMRS Users"
      And I fill in "E-mail" with "bravo_[timestamp]@example.com"
      And I press "Apply"
      Then I should see the link "Bravo"
      And the url should match "dmrsusers/admin"
      When I press "Reset"
      And the url should match "dmrsusers/admin"
