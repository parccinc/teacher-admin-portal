@api @dmrsuser @dmrsuser_import @import @sso @dmrs
Feature: ADS-642 A dmrs administrator imports invalid dmrs users
  As a dmrs administrator
  I want to know how many records had errors
  so that I can try 'em again.

  Background:
    Given LDR organizations:
      | name          | type     | identifier | parent              |
      | XX District 1 | district | 15515134   | XX                  |
      | School XX1a   | school   | 25515134   | [org:XX District 1] |
      | School XX1b   | school   | 25525134   | [org:XX District 1] |
    Given TAP users:
      | name                | mail                | pass  | roles              | organizations |
      | Sir Patrick Stewart | engage@makeitso.org | behat | dmrs administrator | [org:XX]      |

  @ads-641 @javascript @ads-705 @ads-741
  Scenario: A dmrs administrator imports all valid users
    Given a file named "valid_users.csv" with:
    """
First name,Last name,Email,State ID,District ID,Institution ID,General,PII,Roster,SF_Extract,RF_Extract,PF_Extract,PSRD_Extract,CDS_Extract,PII_Analytics,Staff ID
Valid1 First,State Last,valid1_[timestamp]@mailinator.com,XX,,,1,1,1,1,1,1,1,1,1,validid_1
Valid2 First,State Last,valid2_[timestamp]@mailinator.com,XX,,,0,0,1,0,0,0,0,1,1,validid_2
Valid3 First,State Last,valid3_[timestamp]@mailinator.com,XX,,,0,0,0,1,0,0,0,0,0,validid_3
Valid4 First,State Last,valid4_[timestamp]@mailinator.com,XX,,,0,0,0,0,1,0,0,1,1,validid_4
Valid5 First,State Last,valid5_[timestamp]@mailinator.com,XX,,,0,0,0,0,0,1,0,0,0,validid_5
Valid6 First,State Last,valid6_[timestamp]@mailinator.com,XX,,,0,0,0,0,0,0,1,1,1,validid_6
    """
    And I am logged in as "Sir Patrick Stewart"
    When I import the file "valid_users.csv" at "dmrsusers/import"
    Then I see the heading "Upload DMRS Users Confirmation"
    And I see the text "6 out of 6 DMRS Users were created successfully."
    And I should not see the text "You can access the new DMRS User records."
    And I should see the button "Back to DMRS User import"
    Then the user "valid1_[timestamp]@mailinator.com" should have the role "dmrs user"
    And I am editing the "dmrs_profile" profile for the user with the email "valid1_[timestamp]@mailinator.com"
    And the "State Code" field should contain "XX"
    And the "staffid" field should contain "validid_1"
    And the "edit-profile-dmrs-profile-field-memberof-und-0-value" field should contain "||GENERAL||||||XX||||||||||"
    And the "edit-profile-dmrs-profile-field-memberof-und-1-value" field should contain "||PII||||||XX||||||||||"
    And the "edit-profile-dmrs-profile-field-memberof-und-2-value" field should contain "||ROSTER||||||XX||||||||||"
    And the "edit-profile-dmrs-profile-field-memberof-und-3-value" field should contain "||SF_EXTRACT||||||XX||||||||||"
    And the "edit-profile-dmrs-profile-field-memberof-und-4-value" field should contain "||RF_EXTRACT||||||XX||||||||||"
    And the "edit-profile-dmrs-profile-field-memberof-und-5-value" field should contain "||PF_EXTRACT||||||XX||||||||||"
    And the "edit-profile-dmrs-profile-field-memberof-und-6-value" field should contain "||PSRD_EXTRACT||||||XX||||||||||"
    And the "edit-profile-dmrs-profile-field-memberof-und-7-value" field should contain "||CDS_EXTRACT||||||XX||||||||||"
    And the "edit-profile-dmrs-profile-field-memberof-und-8-value" field should contain "||PII_ANALYTICS||||||XX||||||||||"
    And I should see the text "LDAP GUID"
    # ADS-741 TAP Org should be set on this user to match the state code
    Then the LDAP user "valid1_[timestamp]@mailinator.com" should have a "parccorgs" value of "PARCC_XX"
    And the LDAP user "valid1_[timestamp]@mailinator.com" should have a "businesscategory" value of "||PII||||||XX||||||||||"

    When I am editing the "dmrs_profile" profile for the user with the email "valid1_[timestamp]@mailinator.com"
    And I fill in "edit-profile-dmrs-profile-field-memberof-und-9-value" with "||PII_ANALYTICS||||||YY||||||||||"
    And I press "Save"
    Then the LDAP user "valid1_[timestamp]@mailinator.com" should have a "businesscategory" value of "||PII_ANALYTICS||||||YY||||||||||"

  @javascript @pads-1589
  Scenario: SSO user with a '
    Given a file named "users.csv" with:
    """
First name,Last name,Email,State ID,District ID,Institution ID,General,PII,Roster,SF_Extract,RF_Extract,PF_Extract,PSRD_Extract,CDS_Extract,PII_Analytics,Staff ID
Miles,Oye,behat_oye[timestamp]@example.org,XX,,,1,0,0,0,0,0,0,0,0,
Miles,O'Brian,behat_o'brian[timestamp]@example.org,XX,,,1,0,0,0,0,0,0,0,0,
Back,Slash,back\slash[timestamp]@example.org,XX,,,1,0,0,0,0,0,0,0,0,
    """
    And I am logged in as "Sir Patrick Stewart"
    When I import the file "users.csv" at "dmrsusers/import"

    #Scenario 4 Invalid character
    Then I should see "back\slash[timestamp]@example.org" in the "#table_import_errors span.error em.placeholder" element
    And I should see "is not a valid email address." in the "#table_import_errors span.error" element

    #Scenario 1 User created in TAP
    And the user "behat_oye[timestamp]@example.org" should have the role "dmrs user"
    And the user "behat_o'brian[timestamp]@example.org" should have the role "dmrs user"

    #Scenario 2 User created in LDAP
    And the LDAP user "behat_oye[timestamp]@example.org" should have a "parccorgs" value of "PARCC_XX"
    And the LDAP user "behat_o'brian[timestamp]@example.org" should have a "parccorgs" value of "PARCC_XX"

    #Scenario 3 User can update attributes, in this case the password.
    When I visit "user/logout"
    And I am at one-time reset password page for "behat_o'brian[timestamp]@example.org"
    And I press "Log in"

    #Sneaks in an apostrophe in the password
    And I fill in "o'brian" for "pass[pass1]"
    And I fill in "o'brian" for "pass[pass2]"
    And I press "Save"

    #Hard to query ldap for password, so let's just log in with new password
    And I visit "user/logout"
    And I log in to sso as "behat_o'brian[timestamp]@example.org" with password "o'brian"

    Then I should be on "files"

