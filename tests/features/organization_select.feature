@api @organizations
Feature: TAP Organizations Selection
  As a user of the site
  In order to utilize the various pages of the site
  I want to pick which organization I'm dealing with

  Background:
    Given LDR organizations:
      | name          | type     | identifier | parent              |
      | XX District 1 | district | 1          | XX                  |
      | School XX1a   | school   | 11         | [org:XX District 1] |
      | School XX1b   | school   | 12         | [org:XX District 1] |
      | XX District 2 | district | 2          | XX                  |
      | School XX2a   | school   | 21         | [org:XX District 2] |
      | School XX2b   | school   | 22         | [org:XX District 2] |
    And TAP users:
      | name       | mail                   | roles                      | organizations                       |
      | orgadmin   | orgadmin@example.com   | organization administrator | [org:XX District 2]                 |
      | ta         | ta@example.com         | test administrator         | [org:School XX1a],[org:School XX1b] |

  @parads-4501 @javascript @menu
  Scenario: Org context selector action (multiple orgs)
    Given I am logged in as "ta"
    When I am on the homepage
    And I select "School XX1b" from "Organization"

    When I click "Students"
    Then I should be on "manage/[org:School XX1b]/students"

    When I am on the homepage
    And I click "Tests"
    Then I should be on "manage/[org:School XX1b]/tests/view"

    When I am on the homepage
    And I click "Classes"
    Then I should be on "manage/[org:School XX1b]/classes"

    When I am on the homepage
    And I select "School XX1a" from "Organization"
    And I click "Students"
    Then I should be on "manage/[org:School XX1a]/students"

    When I am on the homepage
    And I click "Tests"
    Then I should be on "manage/[org:School XX1a]/tests/view"

    When I am on the homepage
    And I click "Classes"
    Then I should be on "manage/[org:School XX1a]/classes"

  @parads-4501 @menu
  Scenario: Org context selector static text (1 org)
    Given I am logged in as "orgadmin"
    When I am on the homepage
    Then I should see "XX District 2" in the ".pads-organization-context-selector" element

    # These items are actually going to change:
    # For Org Admins they should be on `students/{%org_id}`
    # When I click "Students"
    # Then I should be on "manage/[org:School XX2a]/students"

    # When I am on the homepage

    # These items are actually going to change:
    # Not sure Org Admins should see tests at all, I need to check on this.
    # And I click "Tests"
    # Then I should be on "manage/[org:School XX2a]/tests/view"

    # When I am on the homepage

    # These items are actually going to change:
    # For Org Admins they should be on `classes/{%org_id}`
    # And I click "Classes"
    # Then I should be on "manage/[org:School XX2a]/classes"

    # TODO: Add the other menu items for org admins here and the selection change,
    # That they should get. Orgs (States/Districts/Schools) are all on the same,
    # URL, it would be nice if those were split up and shown through a really,
    # Cool access function or 3 if needed. We can change the title on these,
    # Using the level code that I have seen and used in dashboard boxes so,
    # I don't see why we cannot change the URLs to `organizations/{%org_id}`,
    # `states/{%org_id}`, `districts/{%org_id}`, and `schools/{%org_id}.
    # I think that is the way I am going to solve the menu item changes when I
    # Get back and that would really solve all of my issues with the current menu,
    # System. I also wouldn't have to use a dynamic title that way.
