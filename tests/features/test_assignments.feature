@api @tests @assignments
Feature: Scenarios around test assignment functionality
  As a Test Administrator,
  I want to be able to select accommodations for the students in my class for the test I just assigned to them.

  Background:
    Given LDR organizations:
      | name          | type     | identifier | parent              |
      | XX District 1 | district | 1          | XX                  |
      | School XX1a   | school   | 2          | [org:XX District 1] |
      | School XX2b   | school   | 3          | [org:XX District 1] |
    And LDR class orgs:
      | name     | section | org               | grade | ref      |
      | Math     | 101     | [org:School XX1a] | 3     | Math     |
      | English  | 101     | [org:School XX1a] | 7     | English  |
      | PE       | 101     | [org:School XX2b] | 7     | PE       |
      | Homeroom | 101     | [org:School XX1a] | 7     | Homeroom |
    And LDR students:
      | firstName | lastName | enrollOrgId       | dateOfBirth | gender | gradeLevel | stateIdentifier | classes                      |
      | Louis     | Skolnick | [org:School XX1a] | 2001-02-03  | M      | 3          | XXD1A001        | [class:Math]                 |
      | Lowell    | Gilbert  | [org:School XX1a] | 2004-05-06  | M      | 3          | XXD1A002        | [class:Math]                 |
      | Sloth     | Fratelli | [org:School XX1a] | 2001-02-03  | M      | 3          | XXD1A003        | [class:Math],[class:English] |
      | Mikey     | Astin    | [org:School XX1a] | 2004-05-06  | M      | 3          | XXD1A004        | [class:English]              |
      | Mouth     | Feldman  | [org:School XX1a] | 2004-05-06  | M      | 3          | XXD1A005        | [class:English]              |

    And TAP users:
      | name      | pass        | mail                  | roles                      | organizations                       |
      | testadmin | admin9test9 | testadmin@example.com | test administrator         | [org:School XX1a],[org:School XX2b] |
      | orgadmin  | behat       | orgadmin@example.com  | organization administrator | [org:School XX1a]                   |

    And LDR tests:
      | name                                | subject | grade | program               | security | permissions    | scoring   | description                 |
      | ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3  | ELA     | 8     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Test administered for Behat |
      | Math_Comp - MC.8.F.B.3 - MC.8.F.B.3 | Math    | 8     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Test administered for Behat |
    And LDR test assignments:
      | student                  | test                                       | status    | ref |
      | [student:Louis Skolnick] | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Lowell Gilbert] | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Sloth Fratelli] | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Sloth Fratelli] | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Mikey Astin]    | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Mouth Feldman]  | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |

  @parads-4940 @ads-1260
  Scenario: All classes displayed on the page (reverted)
    And I am logged in as "testadmin"
    And I am assigned classes "[class:Math],[class:English],[class:Homeroom]"
    When I am on the homepage
    And I click "tests-[org:School XX1a]-count"
    Then I should be on "manage/[org:School XX1a]/tests/assignments/[class:English]"
    And I should not see the text "There are no test assignments in this class."
    When I am on "manage/[org:School XX1a]/tests/assignments/[class:Homeroom]"
    Then I should see the text "There are no test assignments in this class."

  @parads-4940 @ads-1260
  Scenario: Classes displayed on separate pages, check default.
    # Classes displayed on separate pages, che
    And I am logged in as "testadmin"
    And I am assigned classes "[class:English],[class:Math]"
    When I am on the homepage
    And I click "tests-[org:School XX1a]-count"
    # Would be nice if we could respect the class assignment order.
    Then I should be on "manage/[org:School XX1a]/tests/assignments/[class:English]"
    And I should not see the text "There are no test assignments in this class."

  @parads-4940 @javascript
  Scenario: I select one class from the dropdown menu
    And I am logged in as "testadmin"
    And I am assigned classes "[class:Math],[class:English],[class:Homeroom]"
    When I am on the homepage
    And I reload the page
    And I click "tests-[org:School XX1a]-count"
    Then I should be on "manage/[org:School XX1a]/tests/assignments/[class:English]"
    When I select "Homeroom" from "For class"
    And I wait for AJAX to finish
    Then I should see the text "There are no test assignments in this class."
    # And I don't see it when I'm on a class with assignments
    When I select "Math" from "For class"
    And I wait for AJAX to finish
    Then I should not see the text "There are no test assignments in this class."

  @parads-4392 @ads-1260
  Scenario: Test administrator can see key link for student test assignments
    Given I am logged in as "testadmin"
    And I am assigned classes "[class:Math],[class:English]"
    And I am on the homepage
    When I click "tests-[org:School XX1a]-count"
    # Separate pages per ADS-1260.
    Then I should be on "manage/[org:School XX1a]/tests/assignments/[class:English]"
    And the "#datatable-1 caption" element should contain "Class: English"
    And the "#datatable-1" element should not contain "Skolnick"
    And the "#datatable-1" element should not contain "Gilbert"
    And the "#datatable-1" element should contain "Fratelli"
    And the "#datatable-1" element should contain "Astin"
    And the "#datatable-1" element should contain "Feldman"

    When I am on "manage/[org:School XX1a]/tests/assignments/[class:Math]"
    Then the "#datatable-1 caption" element should contain "Class: Math"
    And the "#datatable-1" element should contain "Skolnick"
    And the "#datatable-1" element should contain "Gilbert"
    And the "#datatable-1" element should contain "Fratelli"
    And the "#datatable-1" element should not contain "Astin"
    And the "#datatable-1" element should not contain "Feldman"

    #ADS-1260 scenario 4, no classes assigned in school
    When I select "School XX2b" from "Showing test assignments for"
    And I press "Go"
    Then I should not see an "For class" form element
    And I should not see "Please select accommodations for your students and their tests"
    And I should see "There are no classes assigned to you"

  @parads-4392 @javascript
  Scenario: School jump menu
    Given I am logged in as "testadmin"
    And I am assigned classes "[class:Math],[class:English],[class:PE]"
    When I am at "manage/[org:School XX1a]/tests/assignments/[class:Math]"
    And I select "School XX1a" from "Showing test assignments for"
    And I wait for AJAX to finish
    Then the "#edit-jump--2" element should contain "Math"
    And the "#edit-jump--2" element should contain "English"
    And the "#edit-jump--2" element should not contain "PE"
    When I select "School XX2b" from "Showing test assignments for"
    And I wait for AJAX to finish
    Then the "#edit-jump--2" element should not contain "Math"
    And the "#edit-jump--2" element should not contain "English"
    And the "#edit-jump--2" element should contain "PE"

  @parads-4392
  Scenario: Org admin does not see the test assessment tab
    Given I am logged in as "orgadmin"
    When I am on the homepage
    And I click "My Students"
    Then I should not see the link "Test Assignments"

  @parads-4392 @javascript
  Scenario: Making a test assignment from the UI
    Given I am logged in as "testadmin"
    And I am assigned classes "[class:Math],[class:English]"
    When I click "Classes"
    And I click "Assign Test" in the "English" row
    And I wait for AJAX to finish
    And I select "8" from "Select Grade"
    And I wait for AJAX to finish
    And I select "Math" from "Select Subject"
    And I wait for AJAX to finish
    And I select "Diagnostic Assessment Math_Comp - MC.8.F.B.3 - MC.8.F.B.3" from "Select Battery"
    And I wait for AJAX to finish
    And I press "Next"
    And I wait for AJAX to finish
    Then the url should match "tests/assignments"

  @parads-4392 @parads-4933
  Scenario: What's on the test assignment tab
    Given I am logged in as "testadmin"
    And I am assigned classes "[class:Math],[class:English]"
    And I am on the homepage
    And I reload the page
    When I click "tests-[org:School XX1a]-count"
    Then I should see "Please select accommodations for your students and their tests"

    And I should see the column headers in table "datatable-1":
      | header         |
      | Line Reader    |
      | Text to Speech |
      | Test Name      |
      | Student        |
      | Test Status    |

  @parads-4392 @javascript
  Scenario: Class filter
    Given I am logged in as "testadmin"
    And I am assigned classes "[class:Math],[class:English]"
    When I am at "manage/[org:School XX1a]/tests/assignments/[class:Math]"
    Then the "#datatable-1 caption" element should contain "Class: Math"
    And I should not see "Class: English"
    When I select "Math" from "For class"
    Then the "#datatable-1 caption" element should contain "Class: Math"
    And I should not see an "#datatable-2 caption" element
    When I select "English" from "For class"
    Then the "#datatable-1 caption" element should contain "Class: English"
    And I should not see an "#datatable-2 caption" element

  @parads-4585 @javascript
  Scenario: As a test administrator, I want to be able to assign tests to a class from the Dashboard so that I can quickly and efficiently assign tests upon sign in.

    Given I am logged in as "testadmin"
    And I am assigned classes "[class:Math],[class:English]"
    When I am on the homepage
    And I reload the page
    And I click "tests-4-action-add"
    And I wait for AJAX to finish
    And I select "English - 101" from "Class - Section Identifier"
    And I wait for AJAX to finish
    And I select "8" from "Select Grade"
    And I wait for AJAX to finish
    And I select "Math" from "Select Subject"
    And I wait for AJAX to finish
    And I select "Diagnostic Assessment Math_Comp - MC.8.F.B.3 - MC.8.F.B.3" from "Select Battery"
    And I wait for AJAX to finish
    And I press "Next"
    And I wait for AJAX to finish
    Then I should see "2 Math_Comp - MC.8.F.B.3 - MC.8.F.B.3 assignments created for class English"

  @parads-5308 @javascript
  Scenario: Assign Test to Class with No Students
    And LDR class orgs:
      | name  | section | org               | grade | ref   |
      | Empty | 101     | [org:School XX1a] | 8     | Empty |
    And I am logged in as "testadmin"
    And I am assigned classes "[class:Empty]"
    When I am on the homepage
    And I click "Classes"
    And I click "Assign Test" in the "Empty" row
    And I wait for AJAX to finish
    And I select "Math" from "Select Subject"
    And I wait for AJAX to finish
    And I select "Diagnostic Assessment Math_Comp - MC.8.F.B.3 - MC.8.F.B.3" from "Select Battery"
    And I press "Next"
    And I wait 2 seconds
    Then I should not see the error message "Math_Comp - MC.8.F.B.3 - MC.8.F.B.3 has already been assigned to the class."
    And I should see the error message "Test assignment was unsuccessful. There are no students assigned to this class."
