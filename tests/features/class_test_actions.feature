@api @classes @tests @test_assignments @ldr_cleanup
Feature: Class test operations

  Background:
    Given LDR organizations:
      | name          | type     | identifier | parent              |
      | XX District 1 | district | 1          | XX                  |
      | School XX1a   | school   | 2          | [org:XX District 1] |
    And TAP users:
      | name          | pass  | mail                | roles              | organizations     |
      | behat_tapuser | behat | tapuser@example.com | test administrator | [org:School XX1a] |
    And LDR class orgs:
      | name    | section  | org               | grade | ref     |
      | Math    | math1    | [org:School XX1a] | 3     | Math    |
      | English | english1 | [org:School XX1a] | 7     | English |
    And LDR students:
      | firstName | lastName | enrollOrgId       | dateOfBirth | gender | gradeLevel | stateIdentifier |
      | Louis     | Skolnick | [org:School XX1a] | 2001-02-03  | M      | 3          | XXD1A001        |
      | Gilbert   | Lowell   | [org:School XX1a] | 2004-05-06  | M      | 3          | XXD1A002        |

  @parads-3551 @classes
  Scenario: Assign Test link on my students page
    Given I am logged in as "behat_tapuser"
    And I am assigned classes "[class:Math],[class:English]"
    And I reload the page
    When I click "My Classes"
    Then I should see the header "Operations" in column "4" of "datatable-1"
    And I should see "Assign Test" in the "Math" row
    And I should see "Assign Test" in the "English" row
    And I should see the link "Assign Test"

  @parads-3552 @javascript
  Scenario Outline: Test Administrator and Default Grade & Subjects
    Given I am logged in as "behat_tapuser"
    And I am assigned classes "[class:Math],[class:English]"
    And students "[student:Louis Skolnick]" are assigned to "[class:Math]"
    And students "[student:Gilbert Lowell]" are assigned to "[class:English]"
    And I reload the page
    And I click "Classes"
    Then I should be on "manage/[org:<org>]/classes"
    When I click "Assign Test" in the "<class id>" row
    And I wait for AJAX to finish
    Then the "Class Identifier" field should contain "<class id>"
    And the "Section Number" field should contain "<class section>"
    And I should see an "Select Grade" form element
    And the "Select Grade" field should contain "<class grade>"
    And I should see the "Select Subject" test battery subjects for grade "<class grade>"

    # Subject changes when grade changes
    When I select "<change grade>" from "Select Grade"
    And I wait for AJAX to finish
    Then I should see the "Select Subject" test battery subjects for grade "<change grade>"
    Examples:
      | org         | class id | class section | class grade | change grade |
      | School XX1a | Math     | math1         | 3           | 4            |
      | School XX1a | English  | english1      | 7           | 8            |

  @parads-3549 @javascript
  Scenario: Test administrator selects test subject to assign after choosing grade and subject
    Given LDR tests:
      | name       | subject | grade | program               | security | permissions    | scoring   | description                      |
      | Behat Test | ELA     | 3     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Math Test administered for Behat |

    And I am logged in as "behat_tapuser"
    And I am assigned classes "[class:Math],[class:English]"

    # Get to the assign to test page.
    And I am assigned the classes "Math, English" from Organization "[org:School XX1a]"
    And students "[student:Louis Skolnick]" are assigned to "[class:Math]"
    And students "[student:Gilbert Lowell]" are assigned to "[class:English]"
    And I click "Classes"
    And I click "Assign Test" in the "Math" row
    And I wait for AJAX to finish

    # Select grade and subject.
    When I select "ELA" from "Select Subject"
    And I wait for AJAX to finish

    Then the "Select Battery" form element should contain test-battery options from JSON args '{"testBatteryGrade":3,"testBatterySubject":"ELA"}'
    And I should not see an "Next" button form element

    # Assumption is Grade 3, ELA, has option 'Diagnostic Assessment AutoIntegrationLinear2'.
    When I select "Diagnostic Assessment Behat Test" from "Select Battery"
    Then I should see the button "Next"

    # Now we change the grade.
    When I select "4" from "Select Grade"
    And I wait for AJAX to finish

    # Select battery is using element-invisible class which is not compatible
    # with the I should not see an :element form element step.
    # Then I should not see an "Select Battery" form element

    Then the "#battery-wrapper" element should have computed css value "0px" for "height"
    And I should not see an "Next" button form element

  @parads-462 @test_assignment @classes @javascript @parads-4391
  Scenario: Test page exists after assignment
    Given I am logged in as "behat_tapuser"

    And LDR tests:
      | name       | subject | grade | program               | security | permissions    | scoring   | description                      |
      | Behat Test | ELA     | 3     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Math Test administered for Behat |

    # Get to the assign to test page.
    And I am assigned classes "[class:Math],[class:English]"
    And students "[student:Louis Skolnick],[student:Gilbert Lowell]" are assigned to "[class:Math]"
    And students "[student:Gilbert Lowell]" are assigned to "[class:English]"
    And I click "Classes"
    And I click "Assign Test" in the "Math" row
    And I wait for AJAX to finish

    # Select test.
    And I select "ELA" from "Select Subject"
    And I wait for AJAX to finish
    And I select "Diagnostic Assessment Behat Test" from "Select Battery"
    When I press "Next"
    And I wait for the batch job to finish
    # Wait longer for good luck.
    And I wait 2 seconds

    Then I should be on "manage/[org:School XX1a]/tests/assignments/[class:Math]"

    # Success message
    And I should see the text "2 Behat Test assignments created for class Math"
    # The tests tab is the 3rd one with Classes, Students
    And I should see the link "Classes"
    And I should see the link "Students"
    And I should see the link "Tests Overview"
    And I should see the link "Test Assignments"

    # PARADS-462
    When I click "Tests Overview"
    Then I should see the text "Class: Math - math1 (2 students)"
    And I should see the text "Class: English - english1 (1 student)"

    #English columns
    And the xpath '//*[@id="datatable-1"]/thead/tr[1]/th[1]' element should contain "Test Name"
    And the xpath '//*[@id="datatable-1"]/thead/tr[1]/th[2]' element should contain "Grade"
    And the xpath '//*[@id="datatable-1"]/thead/tr[1]/th[3]' element should contain "Subject"
    And the xpath '//*[@id="datatable-1"]/thead/tr[1]/th[4]' element should contain "Number of Assignments"

    #Math columns
    And the xpath '//*[@id="datatable-2"]/thead/tr[1]/th[1]' element should contain "Test Name"
    And the xpath '//*[@id="datatable-2"]/thead/tr[1]/th[2]' element should contain "Grade"
    And the xpath '//*[@id="datatable-2"]/thead/tr[1]/th[3]' element should contain "Subject"
    And the xpath '//*[@id="datatable-2"]/thead/tr[1]/th[4]' element should contain "Number of Assignments"

    #English rows
    And the xpath '//*[@id="datatable-1"]/tbody/tr[1]/td[1]' element should contain "Behat Test "
    And the xpath '//*[@id="datatable-1"]/tbody/tr[1]/td[2]' element should contain "3"
    And the xpath '//*[@id="datatable-1"]/tbody/tr[1]/td[3]' element should contain "ELA"
    And the xpath '//*[@id="datatable-1"]/tbody/tr[1]/td[4]' element should contain "1"

    #Math rows
    And the xpath '//*[@id="datatable-2"]/tbody/tr[1]/td[1]' element should contain "Behat Test "
    And the xpath '//*[@id="datatable-2"]/tbody/tr[1]/td[2]' element should contain "3"
    And the xpath '//*[@id="datatable-2"]/tbody/tr[1]/td[3]' element should contain "ELA"
    And the xpath '//*[@id="datatable-2"]/tbody/tr[1]/td[4]' element should contain "2"

  @parads-4277 @javascript @parads-4391
  Scenario: Test is assigned to a class where some students already are assigned same test.
    And LDR students:
      | firstName | lastName | enrollOrgId       | dateOfBirth | gender | gradeLevel | stateIdentifier |
      | Nue       | Imma     | [org:School XX1a] | 2004-05-06  | M      | 3          | XXD1A003        |

    # Add 2 students to math class and create class test assignment.
    And students "[student:Gilbert Lowell]" are assigned to "[class:Math]"

    And LDR tests:
      | name               | subject | grade | program               | security | permissions    | scoring   | description                         |
      | Behat Test         | Math    | 7     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Math Test administered for Behat    |
      | English test Behat | ELA     | 7     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | English Test administered for Behat |

    And LDR test assignments:
      | student                  | test              | status    | ref     |
      | [student:Gilbert Lowell] | [test:Behat Test] | Scheduled | 123abc1 |

    # Add additional student to math class
    And students "[student:Nue Imma]" are assigned to "[class:Math]"

    # Do class test assignment again
    And I am logged in as "behat_tapuser"
    And I am assigned classes "[class:Math]"
    And I am on "manage/[org:School XX1a]/classes"
    And I click "Assign Test" in the "Math" row
    And I wait for AJAX to finish
    # Select test.
    And I select "7" from "Select Grade"
    And I wait for AJAX to finish
    And I select "Math" from "Select Subject"
    And I wait for AJAX to finish
    And I select "Diagnostic Assessment Behat Test" from "Select Battery"
    When I press "Next"
    And I wait for the batch job to finish
    # Wait longer for good luck.
    And I wait 2 seconds
    Then I should be on "manage/[org:School XX1a]/tests/assignments/[class:Math]"
    # And I should not see any error messages
    # Success message
    And I should see the text "1 Behat Test assignment created for class Math"
