@api @maintenance
Feature: Maintenance mode

  @ads-325
  Scenario: Maintenance message
    Given maintenance mode is on
    When I am on the homepage
    Then I see the text "The system is currently undergoing maintenance. We are sorry for the inconvenience. Please try again later."

  @ads-325
  Scenario: User is in the site and doing stuff, and maintenance mode gets enabled.
    Given LDR organizations:
      | name   | type     | identifier | parent     |
      | XXD1   | district | 1          | XX         |
      | XXD1S1 | school   | 2          | [org:XXD1] |
    And TAP users:
      | name                | pass  | mail                          | roles                      | organizations       |
      | Sir Patrick Stewart | behat | makeitso@engage.net           | organization administrator | [org:XX]            |
    And I am logged in as "Sir Patrick Stewart"
    And I am on the homepage
    And maintenance mode is on
    When I click "org-[org:XX]-count"
    Then I see the text "The system is currently undergoing maintenance. We are sorry for the inconvenience. Please try again later."

    @ads-325 @javascript
    Scenario: User is in the site and is about to do something ajaxy when maintenance mode gets enabled
      Given LDR organizations:
        | name   | type     | identifier | parent     |
        | XXD1   | district | 1          | XX         |
        | XXD1S1 | school   | 2          | [org:XXD1] |
        | XXD1S2 | school   | 3          | [org:XXD1] |
      And TAP users:
        | name          | mail                | roles              | organizations |
        | behat_tapuser | tapuser@example.com | test administrator | [org:XXD1S1]  |
      And LDR class orgs:
        | name    | section  | org          | grade | ref     |
        | Math    | math1    | [org:XXD1S1] | 3     | math    |
        | English | english1 | [org:XXD1S1] | 3     | english |
      And LDR students:
        | firstName | lastName | enrollOrgId  | dateOfBirth | gender | gradeLevel | stateIdentifier | classes         |
        | Louis     | Skolnick | [org:XXD1S1] | 2005-01-01  | M      | 3          | XXD1A001        | [class:math]    |
        | Gilbert   | Lowell   | [org:XXD1S1] | 2005-01-01  | M      | 3          | XXD1A002        | [class:english] |
        | Jeff      | Spicoli  | [org:XXD1S1] | 1980-01-01  | M      | 7          | XXD1A003        | [class:english] |
      And LDR tests:
        | name         | subject | grade | program               | security | permissions    | scoring   | description                 |
        | Behat test   | ELA     | 3     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Test administered for Behat |
        | Behat test 2 | ELA     | 5     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Test administered for Behat |
      And I am logged in as "behat_tapuser"
      And I am assigned classes "[class:math],[class:english]"
      And I am at "manage/[org:XXD1S1]/students"
      And maintenance mode is on
      When I click "Assign Test" in the "Skolnick" row
      And I wait for AJAX to finish
      Then I see the text "The system is currently undergoing maintenance. We are sorry for the inconvenience. Please try again later."