@api @students @import @student_import
Feature: Functionality around adding a student.

  Background:
    Given LDR organizations:
      | name   | type     | identifier | parent     |
      | XXD1   | district | 1          | XX         |
      | XXD1S1 | school   | 2          | [org:XXD1] |
      | XXD1S2 | school   | 3          | [org:XXD1] |

    And LDR class orgs:
      | name | section | org          | grade | ref |
      | abc  | 1       | [org:XXD1S1] | 4     | abc |
      | def  |         | [org:XXD1S1] | 8     | def |
      | ghi  | 2       | [org:XXD1S2] | 6     | ghi |
      | jkl  |         | [org:XXD1S2] | 10    | jkl |

    And TAP users:
      | name                   | pass  | mail                       | roles                      | organizations |
      | behat_dist_orgadmin    | behat | orgadmin@example.com       | organization administrator | [org:XXD1]    |
      | behat_school_orgadmin  | behat | schoolorgadmin@example.com | organization administrator | [org:XXD1S1]  |
      | behat_school_testadmin | behat | testadmin@example.com      | test administrator         | [org:XXD1S1]  |

  @parads-813 @javascript
  Scenario: Student import form
    Given I am logged in as "behat_school_testadmin"
    When I am on "students/import"
    Then I see the heading "Upload Students"
    And I see the text "(Click to download sample .csv format file.)"
    And I should see the link "Enrollment Template"
    And I should see an "input[type='file']" element
    And I see the text "Files must be less than 2 MB"
    And I see the text "Allowed file types: csv."

  @parads-1656 @javascript
  Scenario: Import success message
    Given a file named "behat1656-1.csv" with:
    """
enrollSchoolIdentifier,stateIdentifier,lastName,firstName,middleName,dateOfBirth,gender,gradeLevel,raceAA,raceAN,raceAS,raceHL,raceHP,raceWH,statusDIS,statusECO,statusELL,statusGAT,statusLEP,statusMIG,disabilityType,optionalStateData1,optionalStateData2,optionalStateData3,optionalStateData4,optionalStateData5,optionalStateData6,optionalStateData7,optionalStateData8,classAssignment1,classAssignment2,classAssignment3,classAssignment4,classAssignment5,classAssignment6,classAssignment7,classAssignment8,classAssignment9,classAssignment10
2,XX,Chopper,Toni,Nicholas,2006-08-12,F,4,Y,N,N,N,N,Y,N,N,N,N,N,N,,,,,,,,,,,,,,,,,,,

    """
    And I am logged in as "behat_school_testadmin"
    When I import the file "behat1656-1.csv" at "students/import"
    Then I see the text "Upload Student Data Confirmation"
    And I see the text "1 out of 1 students were created successfully."
    And I see the text "You can access the new student records and complete their profile."
    #And I see the button "Back to Student import"

    When I press "Back to Student import"
    Then I should be on "students/import"


  @parads-1656, @parads-1671 @parads-1672 @javascript
  Scenario: Submit student import form with invalid import
    Given a file named "behat1656-1.csv" with:

    """
enrollSchoolIdentifier,stateIdentifier,lastName,firstName,middleName,dateOfBirth,gender,gradeLevel,raceAA,raceAN,raceAS,raceHL,raceHP,raceWH,statusDIS,statusECO,statusELL,statusGAT,statusLEP,statusMIG,disabilityType,optionalStateData1,optionalStateData2,optionalStateData3,optionalStateData4,optionalStateData5,optionalStateData6,optionalStateData7,optionalStateData8,classAssignment1,classAssignment2,classAssignment3,classAssignment4,classAssignment5,classAssignment6,classAssignment7,classAssignment8,classAssignment9,classAssignment10
22,XX,Chopper,Toni,Nicholas,2006-08-12,F,4,Y,N,N,N,N,Y,N,N,N,N,N,N,,,,,,,,,,,,,,,,,,,

    """
    And I am logged in as "behat_school_testadmin"
    When I import the file "behat1656-1.csv" at "students/import"
    Then I see the heading "Upload Student Data Confirmation with Exceptions"
    And I see the text "Rejected Records: 1"
    And I see the button "Download errors (.csv)"
    And I should see the column headers in table "table_import_errors":
      | header             |
      | Student Identifier |
      | First Name         |
      | Last Name          |
      | DOB                |
      | Gender             |
      | Grade              |
      | Error              |


  @parads-2824 @javascript
  Scenario: Support Optional State Data Fields in Student Registration File
    Given I am logged in as "behat_school_testadmin"
    And there is a file named "behat3564.csv" with:
    """
enrollSchoolIdentifier,stateIdentifier,lastName,firstName,middleName,dateOfBirth,gender,gradeLevel,raceAA,raceAN,raceAS,raceHL,raceHP,raceWH,statusDIS,statusECO,statusELL,statusGAT,statusLEP,statusMIG,disabilityType,optionalStateData1,optionalStateData2,optionalStateData3,optionalStateData4,optionalStateData5,optionalStateData6,optionalStateData7,optionalStateData8,classAssignment1,classAssignment2,classAssignment3,classAssignment4,classAssignment5,classAssignment6,classAssignment7,classAssignment8,classAssignment9,classAssignment10
2,XX,Chopper,Toni,Nicholas,2006-08-12,F,4,Y,N,N,N,N,Y,N,N,N,N,N,N,,opt1,opt2,,opt4,,,opt7,opt8,,,,,,,,,,
    """
    When I import the file "behat3564.csv" at "students/import"
    Then the newest student in org "[org:XXD1S1]" contains optional state data "opt1,opt2,,opt4,,,opt7,opt8"

  @parads-4568
  Scenario: PARADS-4568 Simplified student upload template
    Given I am logged in as "behat_dist_orgadmin"
    And I am on "students/import"
    When I click "Enrollment Template"
    Then the response should contain "enrollSchoolIdentifier,stateIdentifier,lastName,firstName,middleName,dateOfBirth,gender,gradeLevel,raceAA,raceAN,raceAS,raceHL,raceHP,raceWH,statusDIS,statusECO,statusELL,statusGAT,statusLEP,statusMIG,disabilityType,optionalStateData1,optionalStateData2,optionalStateData3,optionalStateData4,optionalStateData5,optionalStateData6,optionalStateData7,optionalStateData8,classAssignment1,classAssignment2,classAssignment3,classAssignment4,classAssignment5,classAssignment6,classAssignment7,classAssignment8,classAssignment9,classAssignment10"
    And the response should contain "010010010260001,123121234,Chopper,Toni,Nicholas,2006-08-12,F,4,Y,N,N,N,N,Y,N,N,N,N,N,N,,,,,,,,,,,,,,,,,,,"

  @parads-4568 @parads-4570 @javascript
  Scenario: PARADS-4570 Simplified student upload file
    Given a file named "student_import.csv" with:
    """
enrollSchoolIdentifier,stateIdentifier,lastName,firstName,middleName,dateOfBirth,gender,gradeLevel,raceAA,raceAN,raceAS,raceHL,raceHP,raceWH,statusDIS,statusECO,statusELL,statusGAT,statusLEP,statusMIG,disabilityType,optionalStateData1,optionalStateData2,optionalStateData3,optionalStateData4,optionalStateData5,optionalStateData6,optionalStateData7,optionalStateData8,classAssignment1,classAssignment2,classAssignment3,classAssignment4,classAssignment5,classAssignment6,classAssignment7,classAssignment8,classAssignment9,classAssignment10
2,123121234,Chopper,Toni,,2006-08-12,F,4,Y,N,N,N,N,Y,N,N,N,N,N,N,,,,,,,,,,1,def,,,,,,,,
2,123121235,Chadwick,Garlow,,1997-01-01,M,12,Y,N,N,N,N,Y,N,N,N,N,N,N,,,,,,,,,,1,,,,,,,,,
2,123121236,Tiesha,Tome,,2001-02-02,F,8,N,Y,N,N,N,N,N,N,N,N,N,N,,,,,,,,,,def,,,,,,,,,
3,123121237,Alysha,Sharrock,,1999-03-03,F,10,N,N,Y,N,N,N,N,N,N,N,N,N,,,,,,,,,,2,jkl,,,,,,,,
3,123121238,Marni,Durrett,,2004-04-04,F,5,N,N,N,Y,N,N,N,N,N,N,N,N,,,,,,,,,,2,,,,,,,,,
3,123121239,Danette,Marotz,,2000-05-05,F,9,N,N,N,N,Y,N,N,N,N,N,N,N,,,,,,,,,,jkl,,,,,,,,,
     """
    And I am logged in as "behat_dist_orgadmin"
    And I import the file "student_import.csv" at "students/import"
    # Verify school assignments.
    # Currently no way to navigate here as a district-level org admin.

    When I am logged in as a user with the "organization administrator" role
    And I am assigned the organization "[org:XXD1S1]"
    And I go to "students/[org:XXD1S1]"
    Then I should see the link "123121234"
    And I should see "Toni"
    And I should see "Chopper"
    And I should see the link "123121235"
    And I should see "Chadwick"
    And I should see "Garlow"
    And I should see the link "123121236"
    And I should see "Tiesha"
    And I should see "Tome"

    When I am logged in as a user with the "organization administrator" role
    And I am assigned the organization "[org:XXD1S2]"
    And I go to "students/[org:XXD1S2]"
    Then I should see the link "123121237"
    And I should see "Alysha"
    And I should see "Sharrock"
    And I should see the link "123121238"
    And I should see "Marni"
    And I should see "Durrett"
    And I should see the link "123121239"
    And I should see "Danette"
    And I should see "Marotz"

    # Verify class assignments.
    And student with stateIdentifier "123121234" is assigned to class "[class:abc]"
    And student with stateIdentifier "123121234" is assigned to class "[class:def]"
    And student with stateIdentifier "123121235" is assigned to class "[class:abc]"
    And student with stateIdentifier "123121236" is assigned to class "[class:def]"
    And student with stateIdentifier "123121237" is assigned to class "[class:ghi]"
    And student with stateIdentifier "123121237" is assigned to class "[class:jkl]"
    And student with stateIdentifier "123121238" is assigned to class "[class:ghi]"
    And student with stateIdentifier "123121239" is assigned to class "[class:jkl]"

  @parads-5113 @javascript
  Scenario: Verify Access to Student Upload Org
    Given a file named "student_import.csv" with:
    """
enrollSchoolIdentifier,stateIdentifier,lastName,firstName,middleName,dateOfBirth,gender,gradeLevel,raceAA,raceAN,raceAS,raceHL,raceHP,raceWH,statusDIS,statusECO,statusELL,statusGAT,statusLEP,statusMIG,disabilityType,optionalStateData1,optionalStateData2,optionalStateData3,optionalStateData4,optionalStateData5,optionalStateData6,optionalStateData7,optionalStateData8,classAssignment1,classAssignment2,classAssignment3,classAssignment4,classAssignment5,classAssignment6,classAssignment7,classAssignment8,classAssignment9,classAssignment10
2,123121234,Chopper,Toni,,2006-08-12,F,4,Y,N,N,N,N,Y,N,N,N,N,N,N,,,,,,,,,,,,,,,,,,,
3,123121235,Chadwick,Garlow,,1997-01-01,M,12,Y,N,N,N,N,Y,N,N,N,N,N,N,,,,,,,,,,,,,,,,,,,
     """
    And I am logged in as "behat_school_orgadmin"
    When I import the file "student_import.csv" at "students/import"
    Then I should not see a "Toni" row
    And I should see "Access to student Organization is required" in the "Chadwick" row
    And there should be a student "123121234" in state "XX"
    And there should not be a student "123121235" in state "XX"

  @parads-5114 @javascript
  Scenario: Verify Access to Student Upload Org
    Given LDR organizations:
      | name   | type     | identifier | parent     |
      | XXD2   | district | 21         | XX         |
      | XXD2S1 | school   | 22         | [org:XXD2] |
      | XXD2S2 | school   | 23         | [org:XXD2] |
    And a file named "student_import.csv" with:
    """
enrollSchoolIdentifier,stateIdentifier,lastName,firstName,middleName,dateOfBirth,gender,gradeLevel,raceAA,raceAN,raceAS,raceHL,raceHP,raceWH,statusDIS,statusECO,statusELL,statusGAT,statusLEP,statusMIG,disabilityType,optionalStateData1,optionalStateData2,optionalStateData3,optionalStateData4,optionalStateData5,optionalStateData6,optionalStateData7,optionalStateData8,classAssignment1,classAssignment2,classAssignment3,classAssignment4,classAssignment5,classAssignment6,classAssignment7,classAssignment8,classAssignment9,classAssignment10
2,123121234,Chopper,Toni,,2006-08-12,F,4,Y,N,N,N,N,Y,N,N,N,N,N,N,,,,,,,,,,,,,,,,,,,
22,123121235,Chadwick,Garlow,,1997-01-01,M,12,Y,N,N,N,N,Y,N,N,N,N,N,N,,,,,,,,,,,,,,,,,,,
     """
    And I am logged in as "behat_dist_orgadmin"
    When I import the file "student_import.csv" at "students/import"
    Then I should not see a "Toni" row
    And I should see "Access to student Organization is required" in the "Chadwick" row
    And there should be a student "123121234" in state "XX"
    And there should not be a student "123121235" in state "XX"

  @parads-5227 @javascript
  Scenario: Test admin imports students in assigned classes
    Given a file named "student_import.csv" with:
    """
enrollSchoolIdentifier,stateIdentifier,lastName,firstName,middleName,dateOfBirth,gender,gradeLevel,raceAA,raceAN,raceAS,raceHL,raceHP,raceWH,statusDIS,statusECO,statusELL,statusGAT,statusLEP,statusMIG,disabilityType,optionalStateData1,optionalStateData2,optionalStateData3,optionalStateData4,optionalStateData5,optionalStateData6,optionalStateData7,optionalStateData8,classAssignment1,classAssignment2,classAssignment3,classAssignment4,classAssignment5,classAssignment6,classAssignment7,classAssignment8,classAssignment9,classAssignment10
2,123121234,Chopper,Toni,,2006-08-12,F,4,Y,N,N,N,N,Y,N,N,N,N,N,N,,,,,,,,,,def,,,,,,,,
"""
    And I am logged in as "behat_school_testadmin"
    And I am assigned classes "[class:def]"
    When I import the file "student_import.csv" at "students/import"
    Then I see the text "Upload Student Data Confirmation"
    And I see the text "1 out of 1 students were created successfully."

    # Verify class assignments.
    And student with stateIdentifier "123121234" is assigned to class "[class:def]"

  @parads-5227 @javascript
  Scenario: Test admin imports students not in assigned classes
    Given a file named "student_import.csv" with:
    """
enrollSchoolIdentifier,stateIdentifier,lastName,firstName,middleName,dateOfBirth,gender,gradeLevel,raceAA,raceAN,raceAS,raceHL,raceHP,raceWH,statusDIS,statusECO,statusELL,statusGAT,statusLEP,statusMIG,disabilityType,optionalStateData1,optionalStateData2,optionalStateData3,optionalStateData4,optionalStateData5,optionalStateData6,optionalStateData7,optionalStateData8,classAssignment1,classAssignment2,classAssignment3,classAssignment4,classAssignment5,classAssignment6,classAssignment7,classAssignment8,classAssignment9,classAssignment10
2,123121234,Chopper,Toni,,2006-08-12,F,4,Y,N,N,N,N,Y,N,N,N,N,N,N,,,,,,,,,,1,def,,,,,,,
2,123121236,Tiesha,Tome,,2001-02-02,F,8,N,Y,N,N,N,N,N,N,N,N,N,N,,,,,,,,,,def,,,,,,,,,
"""
    And I am logged in as "behat_school_testadmin"
    And I am assigned classes "[class:abc]"
    When I import the file "student_import.csv" at "students/import"

    Then I see the text "Upload Student Data Confirmation with Exceptions"
    And I see the text "Rejected Records: 2"

    And I should see "Cannot assign a student to a class you are not assigned to" in the "#table_import_errors" element

    # Verify class assignments.
    And student with stateIdentifier "123121234" is not assigned to class "[class:abc]"
    And student with stateIdentifier "123121234" is not assigned to class "[class:def]"
    And student with stateIdentifier "123121236" is not assigned to class "[class:abc]"
    And student with stateIdentifier "123121236" is not assigned to class "[class:def]"

  @parads-5227 @javascript
  Scenario: Test admin with multiple classes imports students to assigned classes
    Given a file named "student_import.csv" with:
    """
enrollSchoolIdentifier,stateIdentifier,lastName,firstName,middleName,dateOfBirth,gender,gradeLevel,raceAA,raceAN,raceAS,raceHL,raceHP,raceWH,statusDIS,statusECO,statusELL,statusGAT,statusLEP,statusMIG,disabilityType,optionalStateData1,optionalStateData2,optionalStateData3,optionalStateData4,optionalStateData5,optionalStateData6,optionalStateData7,optionalStateData8,classAssignment1,classAssignment2,classAssignment3,classAssignment4,classAssignment5,classAssignment6,classAssignment7,classAssignment8,classAssignment9,classAssignment10
2,123121234,Chopper,Toni,,2006-08-12,F,4,Y,N,N,N,N,Y,N,N,N,N,N,N,,,,,,,,,,1,def,,,,,,,
2,123121236,Tiesha,Tome,,2001-02-02,F,8,N,Y,N,N,N,N,N,N,N,N,N,N,,,,,,,,,,def,,,,,,,,,
"""
    And I am logged in as "behat_school_testadmin"
    And I am assigned classes "[class:abc],[class:def]"
    When I import the file "student_import.csv" at "students/import"
    Then I see the text "Upload Student Data Confirmation"
    And I see the text "2 out of 2 students were created successfully."

    # Verify class assignments.
    And student with stateIdentifier "123121234" is assigned to class "[class:abc]"
    And student with stateIdentifier "123121234" is assigned to class "[class:def]"
    And student with stateIdentifier "123121236" is not assigned to class "[class:abc]"
    And student with stateIdentifier "123121236" is assigned to class "[class:def]"

  @parads-5227 @javascript
  Scenario: Test admin imports some students in their classes and others not
    Given a file named "student_import.csv" with:
    """
enrollSchoolIdentifier,stateIdentifier,lastName,firstName,middleName,dateOfBirth,gender,gradeLevel,raceAA,raceAN,raceAS,raceHL,raceHP,raceWH,statusDIS,statusECO,statusELL,statusGAT,statusLEP,statusMIG,disabilityType,optionalStateData1,optionalStateData2,optionalStateData3,optionalStateData4,optionalStateData5,optionalStateData6,optionalStateData7,optionalStateData8,classAssignment1,classAssignment2,classAssignment3,classAssignment4,classAssignment5,classAssignment6,classAssignment7,classAssignment8,classAssignment9,classAssignment10
2,123121234,Chopper,Toni,,2006-08-12,F,4,Y,N,N,N,N,Y,N,N,N,N,N,N,,,,,,,,,,1,,,,,,,,
2,123121236,Tiesha,Tome,,2001-02-02,F,8,N,Y,N,N,N,N,N,N,N,N,N,N,,,,,,,,,,def,,,,,,,,,
"""
    And I am logged in as "behat_school_testadmin"
    And I am assigned classes "[class:abc]"
    When I import the file "student_import.csv" at "students/import"
    Then I see the text "Total records in file: 2 :: 1 student imported successfully"
    And I see the text "Rejected Records: 1"
    And I should see "Cannot assign a student to a class you are not assigned to" in the "#table_import_errors" element

    # Verify class assignments.
    And student with stateIdentifier "123121234" is assigned to class "[class:abc]"
    And student with stateIdentifier "123121236" is not assigned to class "[class:abc]"
    And student with stateIdentifier "123121234" is not assigned to class "[class:def]"
    And student with stateIdentifier "123121236" is not assigned to class "[class:def]"

  @ads-500 @javascript
  Scenario: Org admin imports students but do they see the my students button?
    Given a file named "student_import.csv" with:
    """
enrollSchoolIdentifier,stateIdentifier,lastName,firstName,middleName,dateOfBirth,gender,gradeLevel,raceAA,raceAN,raceAS,raceHL,raceHP,raceWH,statusDIS,statusECO,statusELL,statusGAT,statusLEP,statusMIG,disabilityType,optionalStateData1,optionalStateData2,optionalStateData3,optionalStateData4,optionalStateData5,optionalStateData6,optionalStateData7,optionalStateData8,classAssignment1,classAssignment2,classAssignment3,classAssignment4,classAssignment5,classAssignment6,classAssignment7,classAssignment8,classAssignment9,classAssignment10
2,123121234,Chopper,Toni,,2006-08-12,F,4,Y,N,N,N,N,Y,N,N,N,N,N,N,,,,,,,,,,def,,,,,,,,
"""
    And I am logged in as "behat_dist_orgadmin"
    When I import the file "student_import.csv" at "students/import"
    Then I should not see the text "You can access the new student records and complete their profile."
    And I see the button "Back to Student import"

  @ads-674 @javascript
  Scenario: Inaccurate Student Counts after Upload (District Org Admin/Class Name Theory)
    Given LDR organizations:
      | name   | type   | identifier | parent     |
      | XXD1S3 | school | 13         | [org:XXD1] |
    And LDR class orgs:
      | name      | section      | org          | grade | ref  |
      | name only |              | [org:XXD1S3] | 2     | no   |
      |           | section only | [org:XXD1S3] | 2     | so   |
      | name both | section both | [org:XXD1S3] | 2     | both |
    And TAP users:
      | name                    | pass  | mail                   | roles              | organizations |
      | behat_school_testadmin3 | behat | testadmin3@example.com | test administrator | [org:XXD1S3]  |
    And a file named "student_import.csv" with:
    """
enrollSchoolIdentifier,stateIdentifier,lastName,firstName,middleName,dateOfBirth,gender,gradeLevel,raceAA,raceAN,raceAS,raceHL,raceHP,raceWH,statusDIS,statusECO,statusELL,statusGAT,statusLEP,statusMIG,disabilityType,optionalStateData1,optionalStateData2,optionalStateData3,optionalStateData4,optionalStateData5,optionalStateData6,optionalStateData7,optionalStateData8,classAssignment1,classAssignment2,classAssignment3,classAssignment4,classAssignment5,classAssignment6,classAssignment7,classAssignment8,classAssignment9,classAssignment10
13,123121234,Chopper,Toni,,2006-08-12,F,2,Y,N,N,N,N,Y,N,N,N,N,N,N,,,,,,,,,,name only,,,,,,,,
13,123121235,Tiesha,Tome,,2006-09-13,F,2,Y,N,N,N,N,Y,N,N,N,N,N,N,,,,,,,,,,section only,,,,,,,,
13,123121236,Chadwick,Garlow,,2006-10-14,M,2,Y,N,N,N,N,Y,N,N,N,N,N,N,,,,,,,,,,section both,,,,,,,,
"""

    # Dist org admin.
    When I am logged in as "behat_dist_orgadmin"
    And I import the file "student_import.csv" at "students/import"
    Then I should see the text "3 out of 3 students were created successfully."
    When I am on "organizations/[org:XXD1]"
    Then I should see "3" in the "6" column of the "XXD1S3" row

    # School TA.
    When I am logged in as "behat_school_testadmin3"
    And I am assigned classes "[class:no],[class:so],[class:both]"
    And I am on "manage/[org:XXD1S3]/students"
    Then I should see "Chopper"
    And I should see "Tiesha"
    And I should see "Chadwick"

  @ads-1290
  Scenario Outline: Add Upload Students button
    Given I am logged in as "<user>"
    And I click "My Students"
    When I click "Upload Students"
    Then I should be on "students/import"
    Examples:
      | user                   |
      | behat_school_testadmin |
      | behat_school_orgadmin  |