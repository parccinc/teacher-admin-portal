@api @qc
Feature: Quality control features

  Background:
    Given LDR organizations:
      | name   | type     | identifier | parent     |
      | QCD1   | district | 6000       | XX         |
      | QCD1S1 | school   | 6001       | [org:QCD1] |
      | QCD1S2 | school   | 6002       | [org:QCD1] |
    And LDR class orgs:
      | name    | section | org          | grade | ref     |
      | QCClass |         | [org:QCD1S1] | 3     | QCClass |
    And LDR students:
      | firstName | lastName | enrollOrgId  | dateOfBirth | gender | gradeLevel | stateIdentifier | classes         |
      | Marnie    | Hummel   | [org:QCD1S1] | 2005-01-01  | F      | 3          | 4865MH          | [class:QCClass] |
      | Abel      | Wade     | [org:QCD1S1] | 2005-02-02  | M      | 3          | 4865AW          | [class:QCClass] |
      | Gordon    | Harris   | [org:QCD1S1] | 2005-03-03  | M      | 3          | 4865GH          | [class:QCClass] |
      | Muriel    | Johnson  | [org:QCD1S1] | 2005-04-04  | F      | 3          | 4865MJ          | [class:QCClass] |
      | Helen     | McKinney | [org:QCD1S1] | 2005-05-05  | F      | 3          | 4865HM          | [class:QCClass] |
    And LDR tests:
      | name                 | subject | grade | program               | security | permissions    | scoring   | description           |
      | Behat restricted     | Math    | 3     | Diagnostic Assessment | Secure   | Restricted     | Immediate | Behat restricted test |
      | Behat non-restricted | Math    | 3     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Behat non-restricted test |
    And TAP users:
      | name                | pass  | mail                  | roles                            | organizations |
      | Sir Patrick Stewart | behat | engage@makeitso.com   | test administrator, test form qc | [org:QCD1S1]  |
      | behat_testadmin     | behat | testadmin@example.com | test administrator               | [org:QCD1S1]  |

  @parads-5118 @javascript @tests @assignments
  Scenario Outline: Seeing restricted and non-restricted tests
    Given I am logged in as "<name>"
    And I am assigned classes "[class:QCClass]"
    When I am on the homepage
    And I click "Students"
    And I click "Assign Test"
    And I wait for AJAX to finish
    And I select "Math" from "Select Subject"
    And I wait for AJAX to finish
    Then the "Select Battery" select element should contain options "Diagnostic Assessment <seen battery name>"
    And the "Select Battery" select element should not contain options "Diagnostic Assessment <not seen battery name>"

    Examples:
      | name                | seen battery name    | not seen battery name |
      | Sir Patrick Stewart | Behat restricted     | Behat non-restricted  |
      | behat_testadmin     | Behat non-restricted | Behat restricted            |
