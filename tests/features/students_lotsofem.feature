@api @big
Feature: Big Data

  Background:
    Given LDR organizations:
      | name   | type     | identifier | parent     |
      | XXD1   | district | 1          | XX         |
      | XXD1S1 | school   | 2          | [org:XXD1] |
      | XXD1S2 | school   | 3          | [org:XXD1] |
    And LDR class orgs:
      | name     | section | org          | grade | ref      |
      | Math     | 101     | [org:XXD1S1] | 3     | Math     |
      | English  | 101     | [org:XXD1S1] | 7     | English  |
      | PE       | 101     | [org:XXD1S2] | 7     | PE       |
      | Homeroom | 101     | [org:XXD1S1] | 7     | Homeroom |
    And LDR students:
      | firstName | lastName   | enrollOrgId  | dateOfBirth | gender | gradeLevel | stateIdentifier | classes                      |
      | Teresa    | Ellis      | [org:XXD1S1] | 2001-08-20  | F      | 10         | XXD1S11         | [class:English]              |
      | Patrick   | Payne      | [org:XXD1S1] | 2004-12-16  | M      | 6          | XXD1S12         | [class:Math],[class:English] |
      | Christine | Wheeler    | [org:XXD1S1] | 2001-04-02  | F      | 4          | XXD1S13         | [class:Math]                 |
      | Brian     | Hart       | [org:XXD1S1] | 2002-05-04  | M      | 3          | XXD1S14         | [class:Math]                 |
      | David     | Smith      | [org:XXD1S1] | 2005-02-15  | M      | 10         | XXD1S15         | [class:Math],[class:English] |
      | John      | Burns      | [org:XXD1S1] | 2005-02-08  | M      | 12         | XXD1S16         | [class:Math]                 |
      | Nancy     | Wagner     | [org:XXD1S1] | 2000-07-22  | F      | 3          | XXD1S17         | [class:English]              |
      | Michelle  | Hunt       | [org:XXD1S1] | 2003-03-02  | F      | 2          | XXD1S18         | [class:Math],[class:English] |
      | Raymond   | Allen      | [org:XXD1S1] | 2001-03-02  | M      | 12         | XXD1S19         | [class:English]              |
      | Jessica   | Wallace    | [org:XXD1S1] | 2005-08-27  | F      | 6          | XXD1S110        | [class:Math]                 |
      | Joe       | Knight     | [org:XXD1S1] | 2005-10-03  | M      | 12         | XXD1S111        | [class:English]              |
      | Phyllis   | Simmons    | [org:XXD1S1] | 2004-01-13  | F      | 12         | XXD1S112        | [class:Math]                 |
      | Rose      | Robinson   | [org:XXD1S1] | 2003-09-29  | F      | 7          | XXD1S113        | [class:Math],[class:English] |
      | Phillip   | Shaw       | [org:XXD1S1] | 2003-10-29  | M      | 5          | XXD1S114        | [class:Math],[class:English] |
      | Ernest    | Little     | [org:XXD1S1] | 2001-06-27  | M      | 5          | XXD1S115        | [class:English]              |
      | Lori      | Rodriguez  | [org:XXD1S1] | 2004-07-23  | F      | 11         | XXD1S116        | [class:Math],[class:English] |
      | Diana     | Welch      | [org:XXD1S1] | 2002-05-06  | F      | 8          | XXD1S117        | [class:Math]                 |
      | Lawrence  | Grant      | [org:XXD1S1] | 2005-10-21  | M      | 5          | XXD1S118        | [class:English]              |
      | Randy     | Thompson   | [org:XXD1S1] | 2001-01-25  | M      | 1          | XXD1S119        | [class:Math]                 |
      | Julie     | Taylor     | [org:XXD1S1] | 2004-08-07  | F      | 11         | XXD1S120        | [class:Math]                 |
      | Ernest    | Rivera     | [org:XXD1S1] | 2002-06-04  | M      | 5          | XXD1S121        | [class:English]              |
      | Arthur    | Burns      | [org:XXD1S1] | 2000-04-03  | M      | 12         | XXD1S122        | [class:English]              |
      | Edward    | Mills      | [org:XXD1S1] | 2002-02-17  | M      | 3          | XXD1S123        | [class:Math],[class:English] |
      | Rose      | Gardner    | [org:XXD1S1] | 2002-06-18  | F      | 6          | XXD1S124        | [class:English]              |
      | George    | Daniels    | [org:XXD1S1] | 2005-08-09  | M      | 9          | XXD1S125        | [class:English]              |
      | Harry     | Thompson   | [org:XXD1S1] | 2003-09-12  | M      | 7          | XXD1S126        | [class:Math],[class:English] |
      | Joyce     | Hansen     | [org:XXD1S1] | 2002-11-10  | F      | 12         | XXD1S127        | [class:Math]                 |
      | Annie     | Martinez   | [org:XXD1S1] | 2004-02-16  | F      | 7          | XXD1S128        | [class:Math],[class:English] |
      | Donna     | Gutierrez  | [org:XXD1S1] | 2004-07-11  | F      | 6          | XXD1S129        | [class:Math],[class:English] |
      | Joseph    | Chavez     | [org:XXD1S1] | 2001-05-21  | M      | 4          | XXD1S130        | [class:Math],[class:English] |
      | Rebecca   | Brown      | [org:XXD1S1] | 2004-06-10  | F      | 10         | XXD1S131        | [class:English]              |
      | Shirley   | Powell     | [org:XXD1S1] | 2004-11-24  | F      | 11         | XXD1S132        | [class:Math]                 |
      | Virginia  | Larson     | [org:XXD1S1] | 2005-03-26  | F      | 1          | XXD1S133        | [class:English]              |
      | Raymond   | Wilson     | [org:XXD1S1] | 2004-06-11  | M      | 5          | XXD1S134        | [class:Math],[class:English] |
      | Bobby     | Austin     | [org:XXD1S1] | 2000-07-02  | M      | 6          | XXD1S135        | [class:English]              |
      | Brian     | Snyder     | [org:XXD1S1] | 2004-11-11  | M      | 2          | XXD1S136        | [class:Math],[class:English] |
      | Angela    | Larson     | [org:XXD1S1] | 2005-01-19  | F      | 12         | XXD1S137        | [class:English]              |
      | Peter     | Bryant     | [org:XXD1S1] | 2002-03-06  | M      | 3          | XXD1S138        | [class:Math],[class:English] |
      | Wanda     | Watkins    | [org:XXD1S1] | 2005-09-13  | F      | 5          | XXD1S139        | [class:Math],[class:English] |
      | Thomas    | Ford       | [org:XXD1S1] | 2003-01-16  | M      | 5          | XXD1S140        | [class:Math]                 |
      | Doris     | Ross       | [org:XXD1S1] | 2004-08-13  | F      | 5          | XXD1S141        | [class:Math],[class:English] |
      | Scott     | Bell       | [org:XXD1S1] | 2000-08-22  | M      | 4          | XXD1S142        | [class:Math],[class:English] |
      | Antonio   | White      | [org:XXD1S1] | 2001-12-04  | M      | 11         | XXD1S143        | [class:English]              |
      | Kathryn   | Alvarez    | [org:XXD1S1] | 2001-03-19  | F      | 1          | XXD1S144        | [class:English]              |
      | Benjamin  | Castillo   | [org:XXD1S1] | 2004-07-14  | M      | 7          | XXD1S145        | [class:Math]                 |
      | Phillip   | Stanley    | [org:XXD1S1] | 2002-11-13  | M      | 3          | XXD1S146        | [class:Math],[class:English] |
      | Richard   | Hicks      | [org:XXD1S1] | 2000-07-31  | M      | 4          | XXD1S147        | [class:Math],[class:English] |
      | Karen     | Carroll    | [org:XXD1S1] | 2003-11-01  | F      | 3          | XXD1S148        | [class:English]              |
      | Virginia  | Reed       | [org:XXD1S1] | 2005-03-01  | F      | 12         | XXD1S149        | [class:Math],[class:English] |
      | Henry     | Dunn       | [org:XXD1S1] | 2003-06-11  | M      | 1          | XXD1S150        | [class:English]              |
      | Nicholas  | Scott      | [org:XXD1S1] | 2005-11-24  | M      | 8          | XXD1S151        | [class:Math],[class:English] |
      | Justin    | Moreno     | [org:XXD1S1] | 2000-09-26  | M      | 10         | XXD1S152        | [class:Math]                 |
      | Adam      | Hamilton   | [org:XXD1S1] | 2004-06-26  | M      | 3          | XXD1S153        | [class:English]              |
      | Linda     | Torres     | [org:XXD1S1] | 2001-01-05  | F      | 1          | XXD1S154        | [class:English]              |
      | Gary      | Baker      | [org:XXD1S1] | 2002-06-22  | M      | 3          | XXD1S155        | [class:Math]                 |
      | Roger     | Welch      | [org:XXD1S1] | 2003-01-20  | M      | 9          | XXD1S156        | [class:English]              |
      | Daniel    | Garcia     | [org:XXD1S1] | 2001-11-16  | M      | 12         | XXD1S157        | [class:Math]                 |
      | Lois      | Fernandez  | [org:XXD1S1] | 2001-06-07  | F      | 9          | XXD1S158        | [class:Math],[class:English] |
      | Douglas   | Weaver     | [org:XXD1S1] | 2002-10-02  | M      | 3          | XXD1S159        | [class:Math]                 |
      | Deborah   | Richardson | [org:XXD1S1] | 2004-01-23  | F      | 5          | XXD1S160        | [class:English]              |
      | Janet     | Green      | [org:XXD1S1] | 2003-08-16  | F      | 7          | XXD1S161        | [class:Math],[class:English] |
      | Norma     | Dunn       | [org:XXD1S1] | 2005-07-27  | F      | 7          | XXD1S162        | [class:English]              |
      | Anthony   | Allen      | [org:XXD1S1] | 2003-05-29  | M      | 11         | XXD1S163        | [class:English]              |
      | Barbara   | Rogers     | [org:XXD1S1] | 2001-04-22  | F      | 9          | XXD1S164        | [class:Math]                 |
      | Sara      | Rodriguez  | [org:XXD1S1] | 2004-04-23  | F      | 1          | XXD1S165        | [class:Math],[class:English] |
      | William   | Harvey     | [org:XXD1S1] | 2002-06-14  | M      | 2          | XXD1S166        | [class:Math],[class:English] |
      | Phyllis   | Meyer      | [org:XXD1S1] | 2004-03-04  | F      | 9          | XXD1S167        | [class:English]              |
      | Sharon    | Burke      | [org:XXD1S1] | 2000-11-12  | F      | 3          | XXD1S168        | [class:English]              |
      | Judy      | Tucker     | [org:XXD1S1] | 2002-11-18  | F      | 8          | XXD1S169        | [class:Math]                 |
      | Beverly   | Ferguson   | [org:XXD1S1] | 2002-03-07  | F      | 11         | XXD1S170        | [class:Math]                 |
      | Sara      | Powell     | [org:XXD1S1] | 2000-09-13  | F      | 7          | XXD1S171        | [class:English]              |
      | Alan      | Hill       | [org:XXD1S1] | 2001-10-18  | M      | 12         | XXD1S172        | [class:Math],[class:English] |
      | Carolyn   | Clark      | [org:XXD1S1] | 2001-08-25  | F      | 5          | XXD1S173        | [class:Math]                 |
      | Mary      | Perry      | [org:XXD1S1] | 2001-10-07  | F      | 8          | XXD1S174        | [class:English]              |
      | Catherine | Lopez      | [org:XXD1S1] | 2003-04-05  | F      | 6          | XXD1S175        | [class:Math]                 |
      | Jose      | Fisher     | [org:XXD1S1] | 2001-12-28  | M      | 7          | XXD1S176        | [class:English]              |
      | Rachel    | Boyd       | [org:XXD1S1] | 2004-03-09  | F      | 9          | XXD1S177        | [class:English]              |
      | Julie     | Stephens   | [org:XXD1S1] | 2001-11-09  | F      | 10         | XXD1S178        | [class:English]              |
      | Justin    | Lopez      | [org:XXD1S1] | 2000-05-01  | M      | 1          | XXD1S179        | [class:English]              |
      | Carlos    | Palmer     | [org:XXD1S1] | 2004-05-10  | M      | 10         | XXD1S180        | [class:English]              |
      | Roy       | Nichols    | [org:XXD1S1] | 2005-10-18  | M      | 3          | XXD1S181        | [class:English]              |
      | Emily     | Kim        | [org:XXD1S1] | 2002-07-07  | F      | 3          | XXD1S182        | [class:Math]                 |
      | Kathleen  | Fernandez  | [org:XXD1S1] | 2005-01-15  | F      | 6          | XXD1S183        | [class:Math],[class:English] |
      | Nicole    | Butler     | [org:XXD1S1] | 2005-09-29  | F      | 12         | XXD1S184        | [class:Math],[class:English] |
      | Christina | Castillo   | [org:XXD1S1] | 2004-11-30  | F      | 2          | XXD1S185        | [class:Math],[class:English] |
      | Thomas    | Hudson     | [org:XXD1S1] | 2002-09-01  | M      | 3          | XXD1S186        | [class:Math],[class:English] |
      | Ruby      | Austin     | [org:XXD1S1] | 2004-03-25  | F      | 8          | XXD1S187        | [class:Math],[class:English] |
      | Donald    | Hanson     | [org:XXD1S1] | 2002-11-02  | M      | 7          | XXD1S188        | [class:Math]                 |
      | Lori      | Nguyen     | [org:XXD1S1] | 2005-08-09  | F      | 1          | XXD1S189        | [class:Math]                 |
      | Gerald    | Gordon     | [org:XXD1S1] | 2003-01-12  | M      | 7          | XXD1S190        | [class:English]              |
      | Cynthia   | Lane       | [org:XXD1S1] | 2003-10-15  | F      | 10         | XXD1S191        | [class:Math]                 |
      | Douglas   | Reid       | [org:XXD1S1] | 2003-03-11  | M      | 4          | XXD1S192        | [class:English]              |
      | Joan      | Green      | [org:XXD1S1] | 2000-07-31  | F      | 1          | XXD1S193        | [class:Math]                 |
      | Kevin     | Black      | [org:XXD1S1] | 2004-09-10  | M      | 6          | XXD1S194        | [class:Math]                 |
      | Craig     | Kennedy    | [org:XXD1S1] | 2005-11-17  | M      | 11         | XXD1S195        | [class:Math],[class:English] |
      | Cheryl    | Wood       | [org:XXD1S1] | 2004-01-29  | F      | 10         | XXD1S196        | [class:Math],[class:English] |
      | Daniel    | Stevens    | [org:XXD1S1] | 2005-12-13  | M      | 7          | XXD1S197        | [class:Math]                 |
      | Heather   | Arnold     | [org:XXD1S1] | 2002-11-12  | F      | 8          | XXD1S198        | [class:English]              |
      | Eric      | Rose       | [org:XXD1S1] | 2002-12-09  | M      | 5          | XXD1S199        | [class:Math],[class:English] |
      | Bruce     | Mendoza    | [org:XXD1S1] | 2001-11-09  | M      | 2          | XXD1S1100       | [class:English]              |
      | Lawrence  | Hansen     | [org:XXD1S1] | 2004-07-10  | M      | 11         | XXD1S1101       | [class:English]              |
      | Kimberly  | Dunn       | [org:XXD1S1] | 2000-10-31  | F      | 8          | XXD1S1102       | [class:English]              |
      | Ryan      | Meyer      | [org:XXD1S1] | 2004-03-23  | M      | 2          | XXD1S1103       | [class:Math],[class:English] |
      | Alan      | Mason      | [org:XXD1S1] | 2002-11-25  | M      | 4          | XXD1S1104       | [class:Math],[class:English] |
      | Maria     | Reid       | [org:XXD1S1] | 2005-09-15  | F      | 3          | XXD1S1105       | [class:Math]                 |
      | Irene     | Knight     | [org:XXD1S1] | 2004-05-03  | F      | 9          | XXD1S1106       | [class:English]              |
      | Andrea    | Dean       | [org:XXD1S1] | 2002-01-21  | F      | 10         | XXD1S1107       | [class:Math]                 |
      | Helen     | Allen      | [org:XXD1S1] | 2005-06-13  | F      | 10         | XXD1S1108       | [class:English]              |
      | Barbara   | Ruiz       | [org:XXD1S1] | 2002-05-18  | F      | 3          | XXD1S1109       | [class:English]              |
      | Jeffrey   | Perez      | [org:XXD1S1] | 2003-03-14  | M      | 10         | XXD1S1110       | [class:English]              |
      | Andrea    | Stanley    | [org:XXD1S1] | 2000-05-21  | F      | 9          | XXD1S1111       | [class:Math],[class:English] |
      | Katherine | Smith      | [org:XXD1S1] | 2005-07-31  | F      | 7          | XXD1S1112       | [class:English]              |
      | Jason     | Reynolds   | [org:XXD1S1] | 2000-01-22  | M      | 11         | XXD1S1113       | [class:English]              |
      | Jessica   | Mitchell   | [org:XXD1S1] | 2002-07-01  | F      | 11         | XXD1S1114       | [class:Math]                 |
      | Nancy     | Ward       | [org:XXD1S1] | 2005-11-11  | F      | 4          | XXD1S1115       | [class:Math]                 |
      | Nicole    | Bishop     | [org:XXD1S1] | 2000-12-16  | F      | 7          | XXD1S1116       | [class:Math]                 |
      | Mary      | Wells      | [org:XXD1S1] | 2000-07-07  | F      | 12         | XXD1S1117       | [class:Math],[class:English] |
      | Karen     | Garcia     | [org:XXD1S1] | 2000-10-07  | F      | 1          | XXD1S1118       | [class:English]              |
      | Martha    | Ramirez    | [org:XXD1S1] | 2005-11-02  | F      | 7          | XXD1S1119       | [class:English]              |
      | Deborah   | Holmes     | [org:XXD1S1] | 2001-11-09  | F      | 4          | XXD1S1120       | [class:Math],[class:English] |
      | Roger     | Patterson  | [org:XXD1S1] | 2003-05-18  | M      | 6          | XXD1S1121       | [class:English]              |
      | Walter    | Bennett    | [org:XXD1S1] | 2004-06-14  | M      | 5          | XXD1S1122       | [class:Math]                 |
      | Juan      | Miller     | [org:XXD1S1] | 2000-03-14  | M      | 7          | XXD1S1123       | [class:Math],[class:English] |
      | Roger     | Dunn       | [org:XXD1S1] | 2005-10-12  | M      | 2          | XXD1S1124       | [class:Math],[class:English] |
      | Timothy   | Ross       | [org:XXD1S1] | 2003-09-13  | M      | 11         | XXD1S1125       | [class:Math]                 |
      | Joan      | Foster     | [org:XXD1S1] | 2004-01-25  | F      | 4          | XXD1S1126       | [class:Math],[class:English] |
      | Judith    | Fox        | [org:XXD1S1] | 2000-03-26  | F      | 2          | XXD1S1127       | [class:Math],[class:English] |
      | Douglas   | Simpson    | [org:XXD1S1] | 2003-09-30  | M      | 5          | XXD1S1128       | [class:English]              |
      | Craig     | Johnston   | [org:XXD1S1] | 2000-07-21  | M      | 10         | XXD1S1129       | [class:English]              |
      | Howard    | Phillips   | [org:XXD1S1] | 2002-07-17  | M      | 2          | XXD1S1130       | [class:English]              |
      | Eugene    | Watson     | [org:XXD1S1] | 2002-11-06  | M      | 10         | XXD1S1131       | [class:Math]                 |
      | Donald    | Spencer    | [org:XXD1S1] | 2005-01-30  | M      | 12         | XXD1S1132       | [class:Math]                 |
      | Jesse     | Butler     | [org:XXD1S1] | 2001-08-21  | M      | 12         | XXD1S1133       | [class:Math],[class:English] |
      | Terry     | Sullivan   | [org:XXD1S1] | 2001-08-24  | M      | 3          | XXD1S1134       | [class:Math]                 |
      | Shirley   | Berry      | [org:XXD1S1] | 2000-02-15  | F      | 4          | XXD1S1135       | [class:Math]                 |
      | Ralph     | Tucker     | [org:XXD1S1] | 2000-01-27  | M      | 7          | XXD1S1136       | [class:Math]                 |
      | Jose      | Mitchell   | [org:XXD1S1] | 2000-01-05  | M      | 4          | XXD1S1137       | [class:English]              |
      | Michelle  | Jackson    | [org:XXD1S1] | 2001-03-25  | F      | 7          | XXD1S1138       | [class:English]              |
      | Rebecca   | Stone      | [org:XXD1S1] | 2005-08-19  | F      | 10         | XXD1S1139       | [class:English]              |
      | Judy      | Johnson    | [org:XXD1S1] | 2000-03-17  | F      | 10         | XXD1S1140       | [class:Math]                 |
      | Sara      | Ray        | [org:XXD1S1] | 2003-03-22  | F      | 7          | XXD1S1141       | [class:English]              |
      | Samuel    | Gonzalez   | [org:XXD1S1] | 2003-05-03  | M      | 7          | XXD1S1142       | [class:Math]                 |
      | Craig     | Knight     | [org:XXD1S1] | 2001-08-27  | M      | 12         | XXD1S1143       | [class:Math]                 |
      | Jeremy    | Mason      | [org:XXD1S1] | 2000-02-25  | M      | 8          | XXD1S1144       | [class:Math]                 |
      | Jeffrey   | Ward       | [org:XXD1S1] | 2002-06-06  | M      | 10         | XXD1S1145       | [class:Math],[class:English] |
      | Paul      | Duncan     | [org:XXD1S1] | 2000-11-06  | M      | 6          | XXD1S1146       | [class:English]              |
      | Ruth      | Foster     | [org:XXD1S1] | 2003-01-28  | F      | 11         | XXD1S1147       | [class:Math]                 |
      | Brandon   | Hudson     | [org:XXD1S1] | 2005-04-04  | M      | 5          | XXD1S1148       | [class:English]              |
      | Maria     | Fields     | [org:XXD1S1] | 2001-02-08  | F      | 3          | XXD1S1149       | [class:Math]                 |
      | Billy     | Shaw       | [org:XXD1S1] | 2001-02-06  | M      | 4          | XXD1S1150       | [class:Math],[class:English] |

    And TAP users:
      | name      | pass        | mail                  | roles                      | organizations             |
      | testadmin | admin9test9 | testadmin@example.com | test administrator         | [org:XXD1S1],[org:XXD1S2] |
      | orgadmin  | behat       | orgadmin@example.com  | organization administrator | [org:XXD1S1]              |

    And LDR tests:
      | name                                | subject | grade | program               | security | permissions    | scoring   | description                 |
      | ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3  | ELA     | 8     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Test administered for Behat |
      | Math_Comp - MC.8.F.B.3 - MC.8.F.B.3 | Math    | 8     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Test administered for Behat |
    And LDR test assignments:
      | student                      | test                                       | status    | ref |
      | [student:Teresa Ellis]       | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Patrick Payne]      | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Christine Wheeler]  | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Brian Hart]         | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:David Smith]        | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:John Burns]         | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Nancy Wagner]       | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Michelle Hunt]      | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Raymond Allen]      | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Jessica Wallace]    | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Joe Knight]         | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Phyllis Simmons]    | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Rose Robinson]      | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Phillip Shaw]       | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Ernest Little]      | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Lori Rodriguez]     | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Diana Welch]        | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Lawrence Grant]     | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Randy Thompson]     | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Julie Taylor]       | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Ernest Rivera]      | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Arthur Burns]       | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Edward Mills]       | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Rose Gardner]       | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:George Daniels]     | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Harry Thompson]     | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Joyce Hansen]       | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Annie Martinez]     | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Donna Gutierrez]    | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Joseph Chavez]      | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Rebecca Brown]      | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Shirley Powell]     | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Virginia Larson]    | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Raymond Wilson]     | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Bobby Austin]       | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Brian Snyder]       | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Angela Larson]      | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Peter Bryant]       | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Wanda Watkins]      | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Thomas Ford]        | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Doris Ross]         | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Scott Bell]         | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Antonio White]      | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Kathryn Alvarez]    | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Benjamin Castillo]  | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Phillip Stanley]    | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Richard Hicks]      | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Karen Carroll]      | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Virginia Reed]      | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Henry Dunn]         | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Nicholas Scott]     | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Justin Moreno]      | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Adam Hamilton]      | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Linda Torres]       | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Gary Baker]         | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Roger Welch]        | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Daniel Garcia]      | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Lois Fernandez]     | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Douglas Weaver]     | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Deborah Richardson] | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Janet Green]        | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Norma Dunn]         | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Anthony Allen]      | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Barbara Rogers]     | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Sara Rodriguez]     | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:William Harvey]     | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Phyllis Meyer]      | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Sharon Burke]       | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Judy Tucker]        | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Beverly Ferguson]   | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Sara Powell]        | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Alan Hill]          | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Carolyn Clark]      | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Mary Perry]         | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Catherine Lopez]    | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Jose Fisher]        | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Rachel Boyd]        | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Julie Stephens]     | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Justin Lopez]       | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Carlos Palmer]      | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Roy Nichols]        | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Emily Kim]          | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Kathleen Fernandez] | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Nicole Butler]      | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Christina Castillo] | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Thomas Hudson]      | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Ruby Austin]        | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Donald Hanson]      | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Lori Nguyen]        | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Gerald Gordon]      | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Cynthia Lane]       | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Douglas Reid]       | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Joan Green]         | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Kevin Black]        | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Craig Kennedy]      | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Cheryl Wood]        | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Daniel Stevens]     | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Heather Arnold]     | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Eric Rose]          | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Bruce Mendoza]      | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Lawrence Hansen]    | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Kimberly Dunn]      | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Ryan Meyer]         | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Alan Mason]         | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Maria Reid]         | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Irene Knight]       | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Andrea Dean]        | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Helen Allen]        | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Barbara Ruiz]       | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Jeffrey Perez]      | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Andrea Stanley]     | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Katherine Smith]    | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Jason Reynolds]     | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Jessica Mitchell]   | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Nancy Ward]         | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Nicole Bishop]      | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Mary Wells]         | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Karen Garcia]       | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Martha Ramirez]     | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Deborah Holmes]     | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Roger Patterson]    | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Walter Bennett]     | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Juan Miller]        | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Roger Dunn]         | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Timothy Ross]       | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Joan Foster]        | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Judith Fox]         | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Douglas Simpson]    | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Craig Johnston]     | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Howard Phillips]    | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Eugene Watson]      | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Donald Spencer]     | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Jesse Butler]       | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Terry Sullivan]     | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Shirley Berry]      | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Ralph Tucker]       | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Jose Mitchell]      | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Michelle Jackson]   | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Rebecca Stone]      | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Judy Johnson]       | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Sara Ray]           | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Samuel Gonzalez]    | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Craig Knight]       | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Jeremy Mason]       | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Jeffrey Ward]       | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Paul Duncan]        | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Ruth Foster]        | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Brandon Hudson]     | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Maria Fields]       | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Billy Shaw]         | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |


  @ads-1257 @students @javascript
  Scenario: Students page
    Given I am logged in as "testadmin"
    And I am assigned classes "[class:Math],[class:English]"
    And I visit "manage/[org:XXD1S1]/students"
    And I maximize the window
    And I page "next" on the "datatable-1" datatable
    And I wait for AJAX to finish
    When I click "Generate access code" in the "XXD1S136" row
    And I click the "#ddb--51 > li > a" element
    And I wait for AJAX to finish
    Then I should see an "#modalContent" element
