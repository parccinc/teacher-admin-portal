@api @organizations @import
Feature: As an authorized TAP user, I want to upload a CSV Organization data file

  Background:
    Given LDR organizations:
      | name          | type     | identifier | parent              |
      | XX District 1 | district | 1          | XX                  |
      | School XX1a   | school   | 2          | [org:XX District 1] |
      | XX District 2 | district | 3          | XX                  |
    And TAP users:
      | name              | pass  | mail                          | roles                      | organizations       |
      | nat_orgadmin      | behat | nat_orgadmin@example.com      | organization administrator | 1                   |
      | state_orgadmin    | behat | state_orgadmin@example.com    | organization administrator | [org:XX]            |
      | district_orgadmin | behat | district_orgadmin@example.com | organization administrator | [org:XX District 1] |

  @parads-1919 @parads-1921
  Scenario: Organization import form
    Given I am logged in as "state_orgadmin"
    And I am on the homepage
    When I click "Import Districts"
    Then I see the heading "Upload Organizations"
    And I see the text "(Click to download sample .csv format file.)"
    And I should see the link "Organization Template"
    And I should see an ".form-item.form-type-plupload.form-item-file" element
    And I see the text "Files must be less than 2 MB"
    And I see the text "Allowed file types: csv."

  @parads-1924 @parads-1927 @javascript
  Scenario: Import Organizations
    Given there is a file named "xx-dists.csv" with:
    """
      organizationName,organizationType,parentOrganizationIdentifier,organizationIdentifier
      Hansons,district,XX,24488006
Maywood,District,XX,68084205
Granby,DISTRICT,XX,60429037
Dayton,district,XX,60429215
Stone Corner,district,XX,57349913
      """
    And I am logged in as "nat_orgadmin"
    When I import the file "xx-dists.csv" at "organizations/import"
    Then I see the heading "Upload Organization Data Confirmation"
    And I see the text "5 out of 5 organizations were created successfully."
    And I see the button "Back to Organization import"
    #Let's verify the child count was updated in TAP.
    And the cache has been cleared
    When I am at "organizations/1"
    And I select "25" from "datatable-1_length"
    Then I see the "7" in the "XX" row

  @javascript @parads-5077
  Scenario: District org admin imports org from different district
    Given there is a file named "xx-dists.csv" with:
    """
organizationName,organizationType,parentOrganizationIdentifier,organizationIdentifier
School XX1B,school,1,101002
School XX2A,school,3,102001
      """
    And I am logged in as "district_orgadmin"
    When I import the file "xx-dists.csv" at "organizations/import"
    Then I should see "Access to parent Organization is required" in the "School XX2A" row
    And I should not see "School XX1B" in the "#table_import_errors" element
