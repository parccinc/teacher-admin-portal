@api @d7 @javascript @userimport
Feature:  PARADS-1191. Tests user import and access to the user import features.

  @ads-1477
  Scenario: As a PARCC user I want to be able to import users
    Given I am logged in as a user with the "ads administrator" role
    And I am at "files"
    When I click "Import Users"
    Then I should see an "input[type='file']" element

  Scenario: As a non-parcc user I can not import users
    Given I am logged in as a user with the "authenticated user" role
    And I am at "files"
    Then I should not see the text "Import users"

  @mailsystem @import @javascript @ads-727 @ads-1469 @ads-1477
  Scenario: Test email
    Given a file named "test_user_import.csv" with:
    """
      E-mail,Role,State ID,Organization
      user1a@example.com,batch user,,Foo
      """
    And I am logged in as a user with the "ads administrator" role
    When I import the file "test_user_import.csv" at "admin/user/import"
    Then the email to "user1a@example.com" should have the subject "An administrator created an account for you on the PARCC System"
    # Mail body content is wrapped by the mail system.
    And the email to "user1a@example.com" should contain "A site administrator has created your account for the PARCC System. You may"
    And the email to "user1a@example.com" should contain "now log in by clicking the link below or copying and pasting it to your"
    And the email to "user1a@example.com" should contain "browser:"
    And the email to "user1a@example.com" should contain "This link can only be used once and will lead you to a page requiring you to"
    And the email to "user1a@example.com" should contain "set your password. Once your password is set, you will be able to log in at"
    And the email to "user1a@example.com" should contain " using:"
    And the email to "user1a@example.com" should contain "username: user1a@example.com"
    And the email to "user1a@example.com" should contain "password: Your password"
    And the email to "user1a@example.com" should contain "--  PARCC team"

  @import @ads-1477
  Scenario: Test valid import
    Given LDR organizations:
      | name   | type     | identifier | parent     |
      | XXD1   | district | 1          | XX         |
      | XXD1S1 | school   | 2          | [org:XXD1] |
    And a file named "test_user_import.csv" with:
    """
      "E-mail","Role","State ID","Organization"
      "user3@example.com","organization administrator",,"parcc"
      "user1a@example.com","batch user",,"Foo"
      "user1b@example.com","batch user",,"Bar"
      "user1c@example.com","batch user",[org:XXD1],
      "user2@example.com","test administrator",[org:XX],
     """
    And I am logged in as a user with the "ads administrator" role
    And I am assigned the organization "1"
    When I import the file "test_user_import.csv" at "admin/user/import"
    Then I see the text "5 users were successfully registered."

  @import @ads-1477
  Scenario: Test an invalid import
    Given LDR organizations:
      | name   | type     | identifier | parent     |
      | XXD1   | district | 1          | XX         |
      | XXD1S1 | school   | 2          | [org:XXD1] |
    And a file named "test_user_import.csv" with:
    """
      "E-mail","Role","State ID","Organization"
      "user3example.com","ads administrator",,"parcc"
      "user1a@example.com","",,"Foo"
      "user1b@example.com","batch user",,"Bar"
      "user1c@example.com","batch user",12,
      "user2@example.com",,-5,
     """
    And I am logged in as a user with the "ads administrator" role
    When I import the file "test_user_import.csv" at "admin/user/import"
    Then I should not see the text "There are no rejected records to display."
    And I see the text "Email address: Invalid"
    And I see the text "Role: Value is required."
