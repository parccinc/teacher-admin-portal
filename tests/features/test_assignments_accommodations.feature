@api @tests @assignments @accommodations
Feature: Assign Accommodations to Students In a Class - Basic
  As a Test Administrator,
  I want to assign accommodations to the individual students in my class after I have assigned a test to a class.

  Background:
    Given LDR organizations:
      | name          | type     | identifier | parent              |
      | XX District 1 | district | 1          | XX                  |
      | School XX1a   | school   | 2          | [org:XX District 1] |
    And LDR class orgs:
      | name    | section  | org               | grade | ref     |
      | Math    | math1    | [org:School XX1a] | 3     | Math    |
      | English | english1 | [org:School XX1a] | 7     | English |
    And LDR students:
      | firstName | lastName | enrollOrgId       | dateOfBirth | gender | gradeLevel | stateIdentifier | classes                      |
      | Louis     | Skolnick | [org:School XX1a] | 2001-02-03  | M      | 3          | XXD1A001        | [class:Math]                 |
      | Gilbert   | Lowell   | [org:School XX1a] | 2004-05-06  | M      | 3          | XXD1A002        | [class:Math]                 |
      | Sloth     | Fratelli | [org:School XX1a] | 2001-02-03  | M      | 3          | XXD1A003        | [class:Math],[class:English] |
      | Mikey     | Astin    | [org:School XX1a] | 2004-05-06  | M      | 3          | XXD1A004        | [class:English]              |
      | Mouth     | Feldman  | [org:School XX1a] | 2004-05-06  | M      | 3          | XXD1A005        | [class:English]              |
    And TAP users:
      | name      | pass  | mail                  | roles                      | organizations     |
      | testadmin | behat | testadmin@example.com | test administrator         | [org:School XX1a] |
      | orgadmin  | behat | orgadmin@example.com  | organization administrator | [org:School XX1a] |

    And LDR tests:
      | name         | subject | grade | program               | security | permissions    | scoring   | description                         |
      | AICHA_TEST   | Math    | 5     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Math Test administered for Behat    |
      | English test | ELA     | 5     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | English Test administered for Behat |

    And LDR test assignments:
      | student                  | test                | status    | ref     |
      | [student:Louis Skolnick] | [test:AICHA_TEST]   | Scheduled | 123abc1 |
      | [student:Gilbert Lowell] | [test:AICHA_TEST]   | Scheduled | 123abc2 |
      | [student:Sloth Fratelli] | [test:AICHA_TEST]   | Scheduled | 123abc3 |
      | [student:Sloth Fratelli] | [test:English test] | Scheduled | 123abc3 |
      | [student:Mikey Astin]    | [test:English test] | Scheduled | 123abc4 |
      | [student:Mouth Feldman]  | [test:English test] | Scheduled | 123abc5 |

  @parads-4393 @javascript
  Scenario: Assign Accommodations to Students In a Class
    Given I am logged in as "testadmin"
    And I am assigned classes "[class:Math],[class:English]"
    When I am at "manage/[org:School XX1a]/tests/assignments/[class:Math]"
    # Start with nothing checked.
    Then the "enableLineReader" accommodation for "[asn:123abc3]" should not be set
    And the "enableTextToSpeech" accommodation for "[asn:123abc3]" should not be set
    # Set enableLineReader.
    When I set the "enableLineReader" accommodation for "[asn:123abc3]"
    And I wait for AJAX to finish
    Then I should see "Test assignment updated"
    And the "enableLineReader" accommodation for "[asn:123abc3]" should be set
    And the "enableTextToSpeech" accommodation for "[asn:123abc3]" should not be set
    # Set enableTextToSpeech.
    When I set the "enableTextToSpeech" accommodation for "[asn:123abc3]"
    And I wait for AJAX to finish
    Then I should see "Test assignment updated"
    And the "enableLineReader" accommodation for "[asn:123abc3]" should be set
    And the "enableTextToSpeech" accommodation for "[asn:123abc3]" should be set
    # Unset enableLineReader.
    When I unset the "enableLineReader" accommodation for "[asn:123abc3]"
    And I wait for AJAX to finish
    Then I should see "Test assignment updated"
    And the "enableLineReader" accommodation for "[asn:123abc3]" should not be set
    And the "enableTextToSpeech" accommodation for "[asn:123abc3]" should be set

  @parads-4901 @parads-4877 @javascript
  Scenario: Disable Accommodations and Operations For Tests with a Status of Submitted, Canceled, Completed
    Given LDR organizations:
      | name   | type     | identifier | parent     |
      | XXD6   | district | 6000       | XX         |
      | XXD6S1 | school   | 6001       | [org:XXD6] |
      | XXD6S2 | school   | 6002       | [org:XXD6] |
    And LDR class orgs:
      | name | section | org          | grade | ref      |
      | Math |         | [org:XXD6S1] | 3     | new_math |
    And LDR students:
      | firstName | lastName | enrollOrgId  | dateOfBirth | gender | gradeLevel | stateIdentifier | classes          |
      | Marnie    | Hummel   | [org:XXD6S1] | 2005-01-01  | F      | 3          | 4865MH          | [class:new_math] |
      | Abel      | Wade     | [org:XXD6S1] | 2005-02-02  | M      | 3          | 4865AW          | [class:new_math] |
      | Gordon    | Harris   | [org:XXD6S1] | 2005-03-03  | M      | 3          | 4865GH          | [class:new_math] |
      | Muriel    | Johnson  | [org:XXD6S1] | 2005-04-04  | F      | 3          | 4865MJ          | [class:new_math] |
      | Helen     | McKinney | [org:XXD6S1] | 2005-05-05  | F      | 3          | 4865HM          | [class:new_math] |
    And LDR tests:
      | name       | subject | grade | program               | security | permissions    | scoring   | description                 |
      | Behat test | ELA     | 3     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Test administered for Behat |
    And LDR test assignments:
      | student                  | test              | status     | ref    |
      | [student:Marnie Hummel]  | [test:Behat test] | Scheduled  | marnie |
      | [student:Abel Wade]      | [test:Behat test] | InProgress | abel   |
      | [student:Gordon Harris]  | [test:Behat test] | Paused     | gordon |
      | [student:Muriel Johnson] | [test:Behat test] | Canceled   | muriel |
      | [student:Helen McKinney] | [test:Behat test] | Submitted  | helen  |
    And TAP users:
      | name        | pass  | mail                    | roles              | organizations |
      | ta_operator | behat | ta_operator@example.com | test administrator | [org:XXD6S1]  |

    And I am logged in as "ta_operator"
    And I am assigned classes "[class:new_math]"

    When I am on the homepage
    And I reload the page
    And I click "tests-[org:XXD6S1]-count"
    Then I should be on "manage/[org:XXD6S1]/tests/assignments/[class:new_math]"
    And I should see "Submitted" in the "Helen" row
    And the "enableLineReader" accommodation for "[asn:helen]" should be disabled
    And the "enableTextToSpeech" accommodation for "[asn:helen]" should be disabled
    And the "enableLineReader" accommodation for "[asn:abel]" should be enabled
    And the "enableTextToSpeech" accommodation for "[asn:abel]" should be enabled
    And the "enableLineReader" accommodation for "[asn:gordon]" should be enabled
    And the "enableTextToSpeech" accommodation for "[asn:gordon]" should be enabled
    And the "enableLineReader" accommodation for "[asn:muriel]" should be disabled
    And the "enableTextToSpeech" accommodation for "[asn:muriel]" should be disabled
    And the "enableLineReader" accommodation for "[asn:marnie]" should be enabled
    And the "enableTextToSpeech" accommodation for "[asn:marnie]" should be enabled

    # Do some clicking.
    When I set the "enableLineReader" accommodation for "[asn:marnie]"
    And I wait for AJAX to finish
    Then the "enableLineReader" accommodation for "[asn:marnie]" should be set
    When I set the "enableTextToSpeech" accommodation for "[asn:marnie]"
    And I wait for AJAX to finish
    Then the "enableTextToSpeech" accommodation for "[asn:marnie]" should be set

  @parads-5185
  Scenario: Test Accommodations Editable When Applicable
    Given LDR students:
      | firstName | lastName | enrollOrgId       | dateOfBirth | gender | gradeLevel | stateIdentifier | classes         |
      | Clay      | Marsh    | [org:School XX1a] | 2004-05-06  | M      | 3          | XXD1A006        | [class:English] |
      | Saul      | Lawrence | [org:School XX1a] | 2004-05-06  | M      | 3          | XXD1A007        | [class:English] |
      | Ben       | Cortez   | [org:School XX1a] | 2004-05-06  | M      | 3          | XXD1A008        | [class:English] |
      | Marilyn   | Garza    | [org:School XX1a] | 2004-05-06  | F      | 3          | XXD1A009        | [class:English] |
      | Josefina  | Munoz    | [org:School XX1a] | 2004-05-06  | F      | 3          | XXD1A0010       | [class:English] |
    And LDR test assignments:
      | student                  | test                | status     | ref      |
      | [student:Clay Marsh]     | [test:English test] | InProgress | 123abc6  |
      | [student:Saul Lawrence]  | [test:English test] | Paused     | 123abc7  |
      | [student:Ben Cortez]     | [test:English test] | Canceled   | 123abc8  |
      | [student:Marilyn Garza]  | [test:English test] | Completed  | 123abc9  |
      | [student:Josefina Munoz] | [test:English test] | Submitted  | 123abc10 |
    And I am logged in as "testadmin"
    And I am assigned classes "[class:Math],[class:English]"

    # Scheduled
    When I go to "manage/[org:School XX1a]/students"
    And I select to view the "English test" test in the "Mouth" row
    Then I should see an "#edit-accommodations-text-to-speech" element
    And I should not see an "#edit-accommodations-text-to-speech[disabled=disabled]" element
    And I should see an "#edit-accommodations-line-reader" element
    And I should not see an "#edit-accommodations-line-reader[disabled=disabled]" element
    And I should see an "#edit-instructionstatus" element
    And I should not see an "#edit-instructionstatus[disabled=disabled]" element
    And I should see the "Save" button

    # InProgress
    When I go to "manage/[org:School XX1a]/students"
    And I select to view the "English test" test in the "Clay" row
    Then I should see an "#edit-accommodations-text-to-speech" element
    And I should not see an "#edit-accommodations-text-to-speech[disabled=disabled]" element
    And I should see an "#edit-accommodations-line-reader" element
    And I should not see an "#edit-accommodations-line-reader[disabled=disabled]" element
    And I should see an "#edit-instructionstatus" element
    And I should not see an "#edit-instructionstatus[disabled=disabled]" element
    And I should see the "Save" button

    # Paused
    When I go to "manage/[org:School XX1a]/students"
    And I select to view the "English test" test in the "Saul" row
    Then I should see an "#edit-accommodations-text-to-speech" element
    And I should not see an "#edit-accommodations-text-to-speech[disabled=disabled]" element
    And I should see an "#edit-accommodations-line-reader" element
    And I should not see an "#edit-accommodations-line-reader[disabled=disabled]" element
    And I should see an "#edit-instructionstatus" element
    And I should not see an "#edit-instructionstatus[disabled=disabled]" element
    And I should see the "Save" button

    # Canceled, Completed, and Submitted test assignments no longer shown in dropdown after PARADS-5330.

  @ads-507 @javascript
  Scenario: Test assignments do not have pagination after 10 assignments.
    #Add more tests and test assignments.
    Given LDR tests:
      | name  | subject | grade | program               | security | permissions    | scoring   | description                      |
      | math1 | Math    | 5     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Math Test administered for Behat |
      | math2 | Math    | 5     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Math Test administered for Behat |
      | math3 | Math    | 5     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Math Test administered for Behat |
    And LDR test assignments:
      | student                  | test         | status    | ref      |
      | [student:Louis Skolnick] | [test:math1] | Scheduled | 123abc6  |
      | [student:Gilbert Lowell] | [test:math1] | Scheduled | 123abc7  |
      | [student:Sloth Fratelli] | [test:math1] | Scheduled | 123abc8  |
      | [student:Louis Skolnick] | [test:math2] | Scheduled | 123abc10 |
      | [student:Gilbert Lowell] | [test:math2] | Scheduled | 123abc11 |
      | [student:Sloth Fratelli] | [test:math2] | Scheduled | 123abc12 |
      | [student:Louis Skolnick] | [test:math3] | Scheduled | 123abc13 |
      | [student:Gilbert Lowell] | [test:math3] | Scheduled | 123abc14 |
      | [student:Sloth Fratelli] | [test:math3] | Scheduled | 123abc15 |
    And I am logged in as "testadmin"
    And I am assigned classes "[class:Math]"
    And I am on "manage/[org:School XX1a]/testassignments"

    #Next button is disabled
    Then I should see an ".paginate_button.next.unavailable" element

  @ads-1260
  Scenario: Test Assignments: One Class at a Time
    Given LDR class orgs:
      | name  | section | org               | grade | ref   |
      | Empty | empty   | [org:School XX1a] | 3     | Empty |
    And I am logged in as "testadmin"
    And I am assigned classes "[class:Math]"
    And I am on the homepage
    When I click "Tests" in the "main_menu" region
    And I click "Test Assignments"

    # Scenario 2: User is assigned one class.
    Then I should be on "manage/[org:School XX1a]/tests/assignments/[class:Math]"
    And the class "[class:Math]" should be selected
    And I should see "Class: Math"
    And I should not see "Class: English"

    # Scenario 1: User is assigned multiple classes.
    When I am assigned classes "[class:English],[class:Empty]"
    And I am on the homepage
    And I click "Tests" in the "main_menu" region
    And I click "Test Assignments"
    # Defaults to Empty (alphabetical order).
    Then I should be on "manage/[org:School XX1a]/tests/assignments/[class:Empty]"
    And the class "[class:Empty]" should be selected
    And I should see "There are no test assignments in this class."

    #Navigation to Math
    When I go to "manage/[org:School XX1a]/tests/assignments/[class:Math]"
    Then the class "[class:Math]" should be selected
    And I should see "Class: Math"
    And I should not see "Class: English"
    # Navigate to English.
    When I go to "manage/[org:School XX1a]/tests/assignments/[class:English]"
    And the class "[class:English]" should be selected
    And I should see "Class: English"
    And I should not see "Class: Math"
