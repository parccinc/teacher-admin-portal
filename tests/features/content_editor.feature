@api
Feature: As a PARCC or ISBE ADS Admin
  I want to edit and add help content
  in order to manage the system's content

  @ads-1363 @help
  Scenario: Add and edit help content
    Given users:
      | name  | password | mail              | roles              |
      | usera | password | usera@example.com | ads content editor |
      | userb | password | userb@example.com | ads content editor |
    And "help" content:
      | title      | author | field_accordion_header |
      | Other help | userb  | Other help             |
    And I am logged in as "usera"
    And I am on the homepage

    # Scenario 2, create help content.
    When I click "Add content" in the "admin_menu"
    # 'content' region includes the nav menu (WAT!?!)
    And I click "Help"
    Then I should be on "node/add/help"
    And I select the radio button "No" with the id "edit-field-display-on-front-page-und-0"
    And I fill in "Help title" for "Title"
    And I fill in "Help content body" for "Body"
    And I press "Save"
    Then I should see the heading "Help title"
    And I should see "Help content body"

    # Scenario 1, edit help content (own).
    When I click "Edit"
    And I fill in "New title" for "Title"
    And I press "Save"
    Then I should see the heading "New title"
    And I should see "Help content body"

    # Scenario 1, edit help content (all).
    When I click "Content" in the "admin_menu"
    And I click "Other help"
    And I click "Edit"
    And I select the radio button "No" with the id "edit-field-display-on-front-page-und-0"
    And I fill in "HAAALP" for "Title"
    And I fill in "HAAALP content body" for "Body"
    And I press "Save"
    Then I should see the heading "HAAALP"
    And I should see "HAAALP content body"

  @ads-1474 @standards
  Scenario: Add and edit standard content
    Given users:
      | name  | password | mail              | roles              |
      | usera | password | usera@example.com | ads content editor |
      | userb | password | userb@example.com | ads content editor |
    And "pads_standards" content:
      | title   | author | field_domain | field_domain_text                 | field_cluster_number | field_cluster_name                                                  | field_evidence_statement | field_evidence_statement_text                                                                                                                                                                                                     |
      | 4.NBT.9 | userb  | NBT          | Number and Operations in Base Ten | 4.NBT.Z              | Generalize place value understanding for multi-digit whole numbers. | 4.NBT.9                  | Recognize that in a multi-digit whole number, a digit in one place represents ten times what it represents in the place to its right. For example, recognize that 700 Ä 70 = 10 by applying concepts of place value and division. |

    And I am logged in as "usera"
    And I am on the homepage

    #Scenario 1: edit standards
    When I click "Content" in the "admin_menu"
    And I click "edit" in the "4.NBT.9" row
    Then the "field_evidence_statement[und][0][value]" field should contain "4.NBT.9"

    #I can edit
    When I fill in "4.NBT.10" for "field_evidence_statement[und][0][value]"
    And I press "Save"

    #And the title changes
    Then I should see the success message "Pads Standards 4.NBT.10 has been updated."

    #Scenario 2: add standards
    When I am on the homepage
    And I click "Add content" in the "admin_menu"
    And I click "Pads Standards"
    Then I should be on "node/add/pads-standards"

    When I fill in the following:
      | field_domain[und][0][value]             | 15.NBT                                                                             |
      | field_domain_text[und][0][value]        | Number and Operations in Base Ten                                                  |
      | field_cluster_number[und][0][value]     | 15.NBT.Z                                                                           |
      | field_cluster_name[und][0][value]       | Perform operations with multi-digit whole numbers and with decimals to hundredths. |
      | field_evidence_statement[und][0][value] | 15.NBT.B.8                                                                         |
      | field_fluency_skill_area[und][0][value] | Multiply multi-digit whole numbers.                                                |
      | field_item_strand[und][0][value]        | 15. 4d by 2d product                                                               |
      | field_item_strand_text[und][0][value]   | 4 digit X 2 digit                                                                  |
    And I press "Save"

    #This also tests that item strand takes priority over evidence statement in title.
    Then I should see the success message "Pads Standards 15. 4d by 2d product has been created."

  @ads-1474 @standards
  Scenario: No ads content editor, no content permissions
    Given I am logged in as a user with the "ads administrator" role
    When I am on the homepage
    Then I should not see the link "Content"
    And I should not see an "#admin-menu" element

    When I am on "node/add/pads-standards"
    Then I should get a 403 HTTP response
