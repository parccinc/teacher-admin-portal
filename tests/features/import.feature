@api @import
Feature: Generic import functionality.

  Background:
    Given LDR organizations:
      | name   | type     | identifier | parent     |
      | XXD1   | district | 1          | XX         |
      | XXD1S1 | school   | 2          | [org:XXD1] |
      | XXD1S2 | school   | 3          | [org:XXD1] |

    And TAP users:
      | name                  | pass  | mail                 | roles                      | organizations |
      | behat_school_orgadmin | behat | orgadmin@example.com | organization administrator | [org:XXD1S1]  |

    And a file named "junk_import.csv" with:
    """
a,b,c,d,e,f
a,b,c,d,e,f
a,b,c,d,e,f
     """

  @parads-5155 @javascript
  Scenario Outline: Student import form
    Given I am logged in as "behat_school_orgadmin"
    And I am at "<page_url>"
    When I add file "junk_import.csv" to element "input[type='file']"
    And I wait for AJAX to finish
    And I wait for the batch job to finish
    And I wait 5 seconds
    Then I should see the message "Some records could not be uploaded. To ensure a successful upload, make sure your CSV file is in <link_title> format. Please see the details below."
    And I should not see the warning message "Some records could not be uploaded. Please see the details below."
    And the link "<link_title>" should point to "pads-import/download/<csv_file>"

    Examples:
      | page_url             | link_title            | csv_file                  |
      | organizations/import | Organization Template | organization_template.csv |
      | class/import         | Class Template        | class_template.csv        |
      | students/import      | Enrollment Template   | student_template.csv      |
      | tapusers/import      | Tap User Template     | tapuser_template.csv      |
