<?php
/**
 * @file
 * Contains \MenuContext
 */

use Behat\Behat\Context\SnippetAcceptingContext;
use Drupal\DrupalExtension\Context\RawDrupalContext;

class MenuContext extends RawDrupalContext implements SnippetAcceptingContext {

  /**
   * Verify menu items.
   *
   * @Then I see the parent menu item :arg1 with children :arg2
   *
   * @param string $parent
   *   Menu item label.
   *
   * @param string $children
   *   Comma separated list of exact child menu item labels.
   *
   * @throws \Exception
   */
  public function iSeeTheParentMenuItemWithChildren($parent, $children) {
    // For scenario outlines, this method is compatible with no parent.
    if (strlen(trim($parent)) == 0) {
      return;
    }

    // Verify main menu exists.
    $mainmenu = $this->assertSession()->elementExists('css', 'ul#main-menu');

    // Retrieve first level main menu links.
    $items = array();
    $mainmenu_items = $mainmenu->findAll('css', 'ul#main-menu.main-nav > li > a');
    foreach ($mainmenu_items as $item) {
      $items[] = $item->getText();
    }

    // Search for parent link.
    $parent_index = array_search($parent, $items);
    if ($parent_index !== FALSE) {
      // Found parent, make sure children match EXACTLY as given in $children.
      $nth_child = $parent_index + 1;
      // Explode the children.
      $children = strlen(trim($children)) == 0 ? array() : explode(',', $children);
      if (!empty($children)) {
        // Get the children of the parent menu item.
        $children_element = $this->assertSession()
          ->elementExists('xpath', "//*[@id='main-menu']/li[$nth_child]/ul");
        $children_labels = $children_element->findAll('css', 'li.leaf a');

        // We assume they are exact, so we first check that size is same.
        if (count($children_labels) != count($children)) {
          throw new \Exception(sprintf('Expecting %d child items of %s, found %d', count($children), $parent, count($children_labels)));
        }
        for ($index = 0; $index < count($children); $index++) {
          $child = trim($children[$index]);
          $child_label = $children_labels[$index]->getText();
          if ($child != $child_label) {
            throw new \Exception(sprintf('Expecting child menu item %s, found %s', $child, $child_label));
          }
        }
      }
    }
    else {
      throw new \Exception(sprintf('Parent menu item %s not found', $parent));
    }
  }

}
