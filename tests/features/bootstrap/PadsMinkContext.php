<?php
/**
 * @file
 * Contains \PadsMinkContext.
 */

use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Drupal\Driver\Exception\UnsupportedDriverActionException;
use Drupal\DrupalExtension\Context\MinkContext;

class PadsMinkContext extends MinkContext {

  protected $tokens;
  protected $contexts = array();

  /**
   * Initiate $timestamp.
   */
  public function __construct() {
    $this->tokens = array(
      "'''" => '"""',
      '[timestamp]' => time(),
    );
  }

  /**
   * @BeforeScenario
   */
  public function setContexts(BeforeScenarioScope $scope) {
    $this->contexts['drupal'] = $scope->getEnvironment()
      ->getContext('BtDrupalContext');
  }

  /**
   * Applies fixStepArgument to path.
   *
   * @param string $path
   *   Path.
   *
   * @return string
   *   Path with tokens replaced.
   */
  public function locatePath($path) {
    $path = $this->fixStepArgument($path);
    return parent::locatePath($path);
  }

  /**
   * Applies fixStepArgument to text.
   *
   * @param string $text
   *   Text to fix.
   */
  public function assertTextVisible($text) {
    $text = $this->fixStepArgument($text);
    parent::assertTextVisible($text);
  }

  /**
   * {@inheritdoc}
   */
  public function clickLink($link) {
    $link = $this->fixStepArgument($link);
    parent::clickLink($link);
  }

  /**
   * Applies fixStepArgument to path.
   *
   * @param string $path
   *   Path.
   *
   * @return string
   *   Path with tokens replaced.
   */
  public function assertAtPath($path) {
    $path = $this->fixStepArgument($path);
    return parent::assertAtPath($path);
  }

  /**
   * Applies fixStepArgument to page.
   *
   * @param string $page
   *   Page..
   *
   * @return string
   *   Page with tokens replaced.
   */
  public function assertPageAddress($page) {
    $page = $this->fixStepArgument($page);
    parent::assertPageAddress($page);
  }

  /**
   * {@inheritdoc}
   */
  public function assertElementContainsText($element, $text) {
    $element = $this->fixStepArgument($element);
    $text = $this->fixStepArgument($text);
    parent::assertElementContainsText($element, $text);
  }

  /**
   * {@inheritdoc}
   */
  public function assertElementNotOnPage($element) {
    $element = $this->fixStepArgument($element);
    parent::assertElementNotOnPage($element);
  }

  /**
   * {@inheritdoc}
   */
  public function assertElementOnPage($element) {
    $element = $this->fixStepArgument($element);
    parent::assertElementOnPage($element);
  }

  /**
   * Add token to be used in fixStepArgument.
   *
   * @param string $token
   *   Token to replace.
   * @param string $value
   *   Value to replace token with.
   */
  public function addToken($token, $value) {
    $this->tokens[$token] = $value;
  }

  /**
   * Replace tokens in arguments.
   *
   * @param string $argument
   *   The argument to fix.
   *
   * @return string
   *   The fixed argument.
   */
  public function fixStepArgument($argument) {
    $argument = parent::fixStepArgument($argument);
    foreach ($this->tokens as $key => $value) {
      if (strpos($argument, $key) !== FALSE) {
        $argument = str_replace($key, $value, $argument);
      }
    }
    return $argument;
  }

  /**
   * @param $arg
   * @return \Behat\Gherkin\Node\TableNode
   */
  public function fixStepArgumentTableNode($arg) {
    $newRows = array();
    $table = $arg->getTable();
    foreach ($table as $r => $row) {
      $cols = array();
      foreach ($row as $c => $col) {
        $cols[] = $this->fixStepArgument($col);
      }
      $newRows[$r] = $cols;
    }

    // * @param array $table Table in form of [$rowLineNumber => [$val1, $val2, $val3]]
    $newTable = new \Behat\Gherkin\Node\TableNode($newRows);
    return $newTable;
  }

  /**
   * @When I click xpath :xpath
   */
  public function iClickXpath($xpath) {
    $node = $this->assertSession()->elementExists('xpath', $xpath);
    $node->click();
  }

  /**
   * @When I click the :element( element)
   */
  public function iClickTheElement($element) {
    $element = $this->fixStepArgument($element);
    $node = $this->assertSession()->elementExists('css', $element);
    $node->click();
  }

  /**
   * @Given the xpath :element element should contain :value
   */
  public function theXpathElementShouldContain($element, $value) {
    $this->assertSession()
      ->elementContains('xpath', $element, $this->fixStepArgument($value));
  }

  /**
   * @Then the xpath :element element should not contain :value
   */
  public function theXpathElementShouldNotContain($element, $value) {
    $this->assertSession()
      ->elementNotContains('xpath', $element, $this->fixStepArgument($value));
  }

  /**
   * Apply fixStepArgument to link and passes to parent.
   *
   * @param string $link
   *   Link.
   *
   * @throws Exception
   */
  public function assertLinkVisible($link) {
    $link = $this->fixStepArgument($link);
    parent::assertLinkVisible($link);
  }

  /**
   * Apply fixStepArgument to link and passes to parent.
   *
   * @param string $link
   *   Link.
   *
   * @throws Exception
   */
  public function assertNotLinkVisible($link) {
    $link = $this->fixStepArgument($link);
    parent::assertNotLinkVisible($link);
  }

  /**
   * Link with exact name is not loaded on the page.
   *
   * For example, if the link is "Foo bar" then a search for "Foo" will act
   * as if the link was not found.
   *
   * @Then I should not see the exact link :link
   */
  public function assertNotExactLinkVisible($link) {
    $link = $this->fixStepArgument($link);
    $element = $this->getSession()->getPage();
    $items = $element->findAll('named_exact', array('link', $link));
    $result = count($items) ? current($items) : NULL;
    try {
      if ($result && $result->isVisible()) {
        throw new \Exception(sprintf("The link '%s' was present on the page %s and was not supposed to be", $link, $this->getSession()
          ->getCurrentUrl()));
      }
    }
    catch (UnsupportedDriverActionException $e) {
      // We catch the UnsupportedDriverActionException exception in case
      // this step is not being performed by a driver that supports javascript.
      // All other exceptions are valid.
    }

    if ($result) {
      throw new \Exception(sprintf("The link '%s' was present on the page %s and was not supposed to be", $link, $this->getSession()
        ->getCurrentUrl()));
    }
  }

  /**
   * Verify text not in a table row containing given text.
   *
   * @Then I should not see (the text ):text in the ":rowText" row
   */
  public function assertTextNotInTableRow($text, $rowText) {
    $row = $this->contexts['drupal']->getTableRow($this->getSession()
      ->getPage(), $rowText);
    if (strpos($row->getText(), $text) !== FALSE) {
      throw new \Exception(sprintf('Found a row containing "%s" also containing the text "%s".', $rowText, $text));
    }
  }

  /**
   * Verify text not in a table row containing given text.
   *
   * @Then I should not see a ":rowText" row
   */
  public function assertNoTableRow($rowText) {
    try {
      $this->contexts['drupal']->getTableRow($this->getSession()
        ->getPage(), $rowText);
      throw new \Exception(sprintf('Found a row containing "%s".', $rowText));
    } catch (Exception $e) {
      // All good.
    }
  }

  /**
   * @Then the :field select element should contain options :values
   */
  public function theFormElementShouldContainsOptions($field, $values) {
    $element = $this->assertSession()->fieldExists($field);
    $values = explode(',', $values);
    foreach ($values as $value) {
      $value = $this->fixStepArgument($value);
      $element->selectOption($value);
    }
  }

  /**
   * @Then the :field select element should not contain options :values
   */
  public function theFormElementShouldNotContainOptions($field, $values) {
    $element = $this->assertSession()->fieldExists($field);
    $values = explode(',', $values);
    foreach ($values as $value) {
      try {
        // We're hoping the NodeElement->selectOption() throws an exception.
        $element->selectOption($value);
        // No exception, fail!
        throw new Exception(sprintf('Field "%s" contains option "%s" but it should not.', $field, $value));
      } catch (Behat\Mink\Exception\ElementNotFoundException $e) {
        // Catch it here and keep iterating. Other types of exceptions bubble
        // up.
      }
    }
  }

  /**
   * @Then the link :link_title should point to :path
   */
  public function assertLinkPointsTo($link_title, $path) {
    $path = $this->locatePath($path);

    foreach ($this->getSession()->getPage()->findAll('css', 'a') as $link) {
      if ($link->getText() == $link_title) {
        $href = $this->locatePath($link->getAttribute('href'));
        if ($href == $path) {
          return;
        }
        else {
          throw new Exception(sprintf('Link "%s" found, but it pointed to "%s" not "%s".', $link_title, $href, $path));
        }
      }
    }

    throw new Exception(sprintf('No link "%s" found.', $link_title));
  }

  /**
   * @When I select to view the :test test in the :rowtext row
   */
  public function assertViewTestInRow($test, $rowtext) {
    // Find the row containing the appropriate student.
    $page = $this->getSession()->getPage();
    $row = $this->contexts['drupal']->getTableRow($page, $rowtext);

    // Look for the dropdown control link.
    foreach ($row->findAll('css', 'a') as $link) {
      if ($link->getText() == 'View/Edit Test Assignments') {
        // Find the dropdown that corresponds to the "View/Edit Test" action.
        $drop = $row->find('css', 'ul#' . $link->getAttribute('data-dropdown'));
        if (!$drop) {
          throw new Exception(sprintf('Unable to locate view test drop down.'));
        }

        // Find the link to view the test and follow it.
        foreach ($drop->findAll('css', 'a') as $viewLink) {
          if ($viewLink->getText() == $test) {
            $viewLink->click();
            return;
          }
        }

        throw new Exception(sprintf('No view test link found for "%s".', $test));
      }
    }

    throw new Exception(sprintf('Unable to find view test control.'));
  }

  /**
   * @Then I should not see the button :button
   * @Then I should not see the :button button
   */
  public function assertNoButton($button) {
    $element = $this->getSession()->getPage();
    $buttonObj = $element->findButton($button);
    if (!empty($buttonObj)) {
      throw new \Exception(sprintf("The button '%s' was found on the page %s, but it should not be there.", $button, $this->getSession()
        ->getCurrentUrl()));
    }
  }

}
