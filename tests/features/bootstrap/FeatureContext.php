<?php
/**
 * @file
 * Contains FeatureContext.
 */

use Behat\Behat\Context\Context;
use Behat\Behat\Context\Environment\InitializedContextEnvironment;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Drupal\DrupalExtension\Context\DrushContext;
use Drupal\DrupalExtension\Context\RawDrupalContext;
use Symfony\Component\Process\PhpExecutableFinder;
use Symfony\Component\Process\Process;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends RawDrupalContext implements SnippetAcceptingContext {

  protected $autologin = array();
  protected $mailSystem;
  protected $filePublicPath;
  protected $timestamp;

  protected $variables = array();

  // Stores tokens to replace in files, strings, etc.
  protected $tokens;

  /** @var InitializedContextEnvironment $env */
  protected static $env;

  /**
   * Initializes context.
   *
   * Every scenario gets its own context instance.
   * You can also pass arbitrary arguments to the
   * context constructor through behat.yml.
   */
  public function __construct() {
    $this->timestamp = time();
    $this->tokens = array(
      "'''" => '"""',
      '[timestamp]' => $this->timestamp,
    );
  }

  /**
   * Set the environment as a property of the Context.
   *
   * @BeforeScenario
   *
   * @param BeforeScenarioScope $scope
   */
  public static function setEnvironment($scope) {
    self::$env = $scope->getEnvironment();
  }

  /**
   * Utility to easily access Contexts.
   *
   * @param string $class
   *   The context to retrieve.
   *
   * @return Context
   *   The instantiated Context, NULL if not found.
   */
  public static function getContext($class) {
    foreach (self::$env->getContexts() as $context) {
      if ($context instanceof $class) {
        return $context;
      }
    }
  }

  /**
   * Replaces tokens in argument.
   *
   * @param string $argument
   *   The argument to fix.
   *
   * @return string
   *   The argument with tokens replaced.
   */
  protected function fixStepArgument($argument) {
    $mink_context = $this->getDrupal()
      ->getEnvironment()
      ->getContext('PadsMinkContext');
    return $mink_context->fixStepArgument($argument);
  }

  public function addTokens($tokens) {
    $this->tokens = array_merge($this->tokens, $tokens);
  }

  /**
   * Prepares test folders in the temporary directory.
   *
   * @BeforeScenario @mailsystem
   */
  public function setTestMailSystem($event) {
    // Save the origial mail system settings.
    $this->mailSystem = mailsystem_get();

    // Set mail system to TestingMailSystem.
    mailsystem_set(array(
        mailsystem_default_id() => 'TestingMailSystem',
      )
    );

    // Flush email buffer.
    variable_set('drupal_test_email_collector', array());
  }

  /**
   * Prepares public files path.
   *
   * @BeforeScenario @public_files
   */
  public function setFilePublicPath($event) {
    $this->filePublicPath = variable_get('file_public_path');
    //$drupal = $this->getDrupalParameter('drupal');
    //$path = drupal_realpath($drupal['drupal_root']) . DIRECTORY_SEPARATOR . $this->filePublicPath;
    $path = drupal_realpath($this->filePublicPath);
    variable_set('file_public_path', $path);
  }

  /**
   * Restores original mail settings.
   *
   * @AfterScenario @mailsystem
   */
  public function restoreTestMailSystem($event) {
    // Flush email buffer.
    variable_set('drupal_test_email_collector', array());

    // Restore original mail system.
    mailsystem_set($this->mailSystem);
  }

  /**
   * Restores public files path.
   *
   * @AfterScenario @public_files
   */
  public function restoreFilePublicPath($event) {
    variable_set('file_public_path', $this->filePublicPath);
  }

  /**
   * Copied from modules/systemtest/drupal_web_test_case.php.
   *
   * Gets an array containing all e-mails sent during this test case.
   *
   * @param array $filter
   *   An array containing key/value pairs used to filter the e-mails that are
   *   returned.
   *
   * @return array
   *   An array containing e-mail messages captured during the current test.
   */
  protected function drupalGetMails($filter = array()) {
    $filtered_emails = array();
    if ($captured_emails = db_select('variable', 'v')
      ->fields('v', array('value'))
      ->condition('name', 'drupal_test_email_collector')
      ->execute()
      ->fetchField()
    ) {
      $captured_emails = unserialize($captured_emails);
      foreach ($captured_emails as $message) {
        foreach ($filter as $key => $value) {
          if (!isset($message[$key]) || $message[$key] != $value) {
            continue 2;
          }
        }
        $filtered_emails[] = $message;
      }
    }
    return $filtered_emails;
  }

  /**
   * @Then the email to :recipient should have the subject :subject
   */
  public function assertEmailSubject($recipient, $subject) {
    $filter = array(
      'to' => $recipient,
    );
    $captured_emails = $this->drupalGetMails($filter);
    if (empty($captured_emails)) {
      throw new \Exception(sprintf('Did not find expected message to %s', $recipient));
    }
    foreach ($captured_emails as $captured_mail) {
      if ($captured_mail['subject'] == $subject) {
        return TRUE;
      }
    }
    throw new \Exception('Did not find expected content in email body.');
  }

  /**
   * @Then /^the email to "([^"]*)" should contain "([^"]*)"$/
   */
  public function theEmailToShouldContain($to, $body) {
    $filter = array(
      'to' => $to,
      // 'body' => $body,
    );
    $captured_emails = $this->drupalGetMails($filter);
    if (empty($captured_emails)) {
      throw new \Exception(sprintf('Did not find expected message to %s', $to));
    }
    foreach ($captured_emails as $captured_mail) {
      if (strpos($captured_mail['body'], $body) !== FALSE) {
        return TRUE;
      }
    }
    throw new \Exception('Did not find expected content in email body.');
  }

  /**
   * @Then I should be on the Drupal homepage
   */
  public function iShouldBeOnTheDrupalHomepage() {
    PHPUnit_Framework_Assert::assertTrue(drupal_is_front_page());
  }


  /**
   * @Given /^I add file "([^"]*)" to element "([^"]*)"$/
   */
  public function iAddFileToElement($file, $locator) {

    // Find element.
    $element = $this
      ->getSession()
      ->getPage()
      ->find('css', $locator);

    if (!$element) {
      throw new \Exception(sprintf('Did not find expected css element %s', $locator));
    }

    // Find path.
    $path = '';
    // First check for file in files_path.
    if ($files_dir = $this->getMinkParameter('files_path')) {
      $files_dir = rtrim($files_dir, DIRECTORY_SEPARATOR);
      $file_path = $files_dir . DIRECTORY_SEPARATOR . $file;

      if (is_readable($file_path) && is_file($file_path)) {
        $path = $file_path;
      }
    }

    // Check that file exists and attach file.
    if (empty($path)) {
      // Behat-created file.
      $this->fileShouldExist($file);
      $path = $this->workingDir . DIRECTORY_SEPARATOR . $file;
    }

    $element->attachFile($path);
  }

  public function getDrupalFileFromPath($filename) {
    $path = $this->workingDir . DIRECTORY_SEPARATOR . $filename;
    PHPUnit_Framework_Assert::assertFileExists($path);
    $data = trim(file_get_contents($path));
    // Normalize the line endings in the output.
    if ("\n" !== PHP_EOL) {
      $data = str_replace(PHP_EOL, "\n", $data);
    }
    return file_save_data($data);
  }

  /**
   * Wait for X seconds.
   *
   * @Given /^I wait (\d+) seconds$/
   */
  public function iWaitForXSeconds($seconds) {
    $ms = $seconds * 1000;
    $this->getSession()->wait($ms);
  }

  /**
   * @Then Element :arg1 exists but is not visible
   */
  public function elementExistsButIsNotVisible($value) {
    $assert = $this->assertSession();
    $element = $assert->elementExists('css', $value);
    PHPUnit_Framework_Assert::assertFalse($element->isVisible());
  }

  /**
   * @Then Element :arg1 exists and is visible
   */
  public function elementExistsAndIsVisible($value) {
    $assert = $this->assertSession();
    $element = $assert->elementExists('css', $value);
    PHPUnit_Framework_Assert::assertTrue($element->isVisible());
  }

  /**
   * @Then the :element element should not have computed css value :value for :property
   */
  public function theElementShouldNotHaveComputedCssValueFor($selector, $value, $property) {
    $this->theElementShouldHaveComputedCssValueFor($selector, $value, $property, TRUE);
  }

  /**
   * @Then the :element element should have computed css value :value for :property
   */
  public function theElementShouldHaveComputedCssValueFor($selector, $value, $property, $negate = FALSE) {
    // Retrieve the selenium2 driver or die.
    $driver = $this->getSession('selenium2');
    if (!$driver) {
      throw new \Exception('Selenium2 driver is required to retrieve computed css value.');
    }

    // Find element.
    $element = $driver->getDriver()
      ->getWebDriverSession()
      ->element(\WebDriver\LocatorStrategy::CSS_SELECTOR, $selector);
    if (!$element) {
      throw new ElementNotFoundException($this->getSession(), 'element', 'css', $selector);
    }

    // Check element computed css value.
    $computed = $element->css($property);
    if (!$negate && ($computed != $value)) {
      throw new \Exception(sprintf('The %s element css property %s contains %s, expecting %s', $selector, $property, $computed, $value));
    }
    if ($negate && ($computed == $value)) {
      throw new \Exception(sprintf('The %s element css property %s contains %s, when it should not', $selector, $property, $computed));
    }

  }

  /**
   * @Then I( should) see the :tag element with the :attribute attribute set to ldr fixed :value in the :region( region)
   */
  public function assertRegionElementAttribute($tag, $attribute, $value, $region) {
    $value = $this->fixStepArgument($value);
    $markup_context = $this->getDrupal()
      ->getEnvironment()
      ->getContext('Drupal\DrupalExtension\Context\MarkupContext');
    $markup_context->assertRegionElementAttribute($tag, $attribute, $value, $region);
  }

  /**
   * @Then I should see the header :value in column :col of :table
   */
  public function iShouldSeeTheHeaderInColumnOf($value, $column, $table) {
    $xpath = "//*[@id=\"$table\"]/thead/tr[1]/th[$column]";
    $this->assertSession()
      ->elementContains('xpath', $xpath, $this->fixStepArgument($value));
  }

  /**
   * @Then I should see the column headers in table :table:
   */
  public function iShouldSeeTheColumnsHeadersInTable($table, TableNode $columns) {
    $columns->getColumnsHash();
    foreach ($columns as $index => $row) {
      // We count columns at base 1
      $this->iShouldSeeTheHeaderInColumnOf($row['header'], ($index + 1), $table);
    }
  }

  /**
   * @Then the named :named :selector should contain :value
   */
  public function theNamedShouldContain($named, $selector, $value) {
    $selector = $this->getSession()
      ->getSelectorsHandler()
      ->xpathLiteral($selector);
    $this->assertSession()->elementTextContains('named', array(
      $named,
      $selector,
    ), $value);
  }

  /**
   * @Then the named :named :selector should exist
   */
  public function theNamedShouldExist($named, $selector) {
    $selector = $this->getSession()
      ->getSelectorsHandler()
      ->xpathLiteral($selector);
    $element = $this->assertSession()->elementExists('named', array(
      $named,
      $selector,
    ));
    return $element;
  }

  /**
   * @When I am editing the :type profile for the user with the email :name
   */
  public function assertEditingUserProfileByEmail($mail, $type) {
    $mail = $this->fixStepArgument($mail);
    if (!$account = user_load_by_mail($mail)) {
      throw new Exception(sprintf('Unable to find user "%s".', $mail));
    }

    $this->visitPath("/user/{$account->uid}/edit/$type");
  }

  /**
   * @When I am editing the :type profile for :name
   */
  public function assertEditingUserProfile($name, $type) {
    if (!$account = user_load_by_name($name)) {
      throw new Exception(sprintf('Unable to find user "%s".', $name));
    }

    $this->visitPath("/user/{$account->uid}/edit/$type");
  }

  /**
   * @When I am on the edit page for user :name
   */
  public function assertUserEditPageFor($name) {
    $name = $this->fixStepArgument($name);
    if (!$account = user_load_by_name($name)) {
      throw new Exception(sprintf('Unable to find user "%s".', $name));
    }
    $this->visitPath("/user/{$account->uid}/edit");
  }

  /**
   * @Then I should be on the user view page for :name
   */
  public function assertUserViewPageFor($name) {
    if (!$account = user_load_by_name($name)) {
      throw new Exception(sprintf('Unable to find user "%s".', $name));
    }

    $path = drupal_get_path_alias("user/{$account->uid}");

    $this->assertSession()
      ->addressEquals($this->locatePath($path));
  }

  /**
   * @When /^I follow meta refresh$/
   */
  public function iFollowMetaRefresh() {

    while ($refresh = $this->getSession()
      ->getPage()
      ->find('css', 'meta[http-equiv="Refresh"]')) {
      $content = $refresh->getAttribute('content');
      $url = str_replace('0; URL=', '', $content);
      $this->getSession()->visit($url);
    }
  }

  /**
   * Imports file at one of the import pages.
   *
   * @Given I import the file :file at :path
   *
   * @param string $file
   *   The filename.
   * @param string $path
   *   Path to import page.
   */
  public function iImportFileAt($file, $path) {
    $this->visitPath($path);
    $this->iAddFileToElement($file, 'input[type="file"]');
    $this->getDrupal()
      ->getEnvironment()
      ->getContext('PadsMinkContext')
      ->iWaitForAjaxToFinish();
    $this->iWaitForXSeconds(1);
    $this->getDrupal()
      ->getEnvironment()
      ->getContext('Drupal\DrupalExtension\Context\BatchContext')
      ->iWaitForTheBatchJobToFinish();
    $this->iWaitForXSeconds(1);
  }

  /**
   * Turns on maintenance mode.
   *
   * @param int $mode
   *   Value to set maintenance_mode variable. 0 or 1 expected.
   * @param DrushContext $context
   *   Type hinting is cool.
   *
   * @Given maintenance mode is on
   */
  public function maintenanceMode($mode = 1, DrushContext $context = NULL) {
    variable_set('maintenance_mode', $mode);
  }

  /**
   * Turn off maintenance mode after scenario.
   *
   * @AfterScenario @maintenance
   */
  public function maintenanceModeOff() {
    $this->maintenanceMode(0);
  }

  /**
   * @When I resize the browser to mobile
   */
  public function iResizeTheBrowserToMobile() {
    $this->getSession()->resizeWindow(320, 600, 'current');
  }

  /**
   * @When I resize the browser to tablet
   */
  public function iResizeTheBrowserToTablet() {
    $this->getSession()->resizeWindow(800, 600, 'current');
  }

  /**
   * @When I resize the browser to small desktop
   */
  public function iResizeTheBrowserToSmallDesktop() {
    $this->getSession()->resizeWindow(1048, 768, 'current');
  }

  /**
   * Maximize the window.
   *
   * @When I maximize the window
   */
  public function iMaxWindow() {
    $this->getSession()->maximizeWindow();
  }

  /**
   * @Given I page :set on the :selector datatable
   */
  public function iPageOnTheDatatable($set, $selector) {
    $script = "jQuery('#$selector').DataTable().page('$set').draw(false);";
    $this->getSession()->getDriver()->executeScript($script);
  }

  /**
   * Saves current variable value then changes to new value.
   *
   * @When the :name variable is set to :value
   *
   * @param string $name
   *   Name of the variable.
   * @param string $value
   *   Value to set the variable.
   */
  public function theVariableIsSetTo($name, $value) {
    if (!array_key_exists($name, $this->variables)) {
      $this->variables[$name] = variable_get($name);
    }
    variable_set($name, $value);

  }

  /**
   * Resets variable to values before test scenario.
   *
   * @AfterScenario
   */
  public function resetVariables() {
    foreach ($this->variables as $name => $value) {
      if (!$value) {
        variable_del($name);
      }
      else {
        variable_set($name, $value);
      }
    }
  }

  /**
   * @When I am at one-time reset password page for :mail
   */
  public function assertPasswordResetPage($mail) {
    $mail = $this->fixStepArgument($mail);
    /** @var DrushContext $drush_context */
    $drush_context = $this->getContext('Drupal\DrupalExtension\Context\DrushContext');
    $command = 'uli';
    $arguments = "--mail=\"$mail\"";
    $drush_context->assertDrushCommandWithArgument($command, $arguments);
    $output = $drush_context->readDrushOutput();
    if ($output) {
      $output = $this->fixStepArgument($output);
      $output = parse_url($output);
      if (array_key_exists('path', $output)) {
        $path = ltrim($output['path'], '/');
        $this->getSession()->visit($this->locatePath(dirname($path)));
        return;
      }
    }
    throw new \Exception('Unable to generate reset password link for "%s"', $mail);
  }

  /**
   * Logs into SSO and potentially accepts a modal dialog.
   * 
   * @param string $username
   *   Username.
   * @param string $password
   *   Password.
   *
   * @When I log in to sso as :username with password :password
   */
  public function assertSsoLogin($username, $password) {

    /** @var PadsMinkContext $mink_context */
    $mink_context = $this->getContext('PadsMinkContext');
    $username = $mink_context->fixStepArgument($username);
    $mink_context->fillField('username', $username);
    $mink_context->fillField('password', $password);
    $mink_context->pressButton('Log in');
    
    // There might be a modal dialog if local is http and IdP is https (or vice
    // versa).
    $session = $this->getSession('selenium2');
    if ($session) {
      /** @var \Behat\Mink\Driver\Selenium2Driver $driver */
      $driver = $session->getDriver();
      $webdriver_session = $driver->getWebDriverSession();
      $handles = $webdriver_session->window_handles();
      $webdriver_session->focusWindow(end($handles));
      $webdriver_session->accept_alert();
    }
  }

  // Begin copy of Behat's Feature Context.
  /**
   * @var string
   */
  private $phpBin;
  /**
   * @var Process
   */
  private $process;
  /**
   * @var string
   */
  private $workingDir;

  /**
   * Cleans test folders in the temporary directory.
   *
   * @BeforeSuite
   * @AfterSuite
   */
  public static function cleanTestFolders() {
    if (is_dir($dir = sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'behat')) {
      self::clearDirectory($dir);
    }
  }

  /**
   * Prepares test folders in the temporary directory.
   *
   * @BeforeScenario
   */
  public function prepareTestFolders() {
    $dir = sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'behat' . DIRECTORY_SEPARATOR .
      md5(microtime() * rand(0, 10000));

    mkdir($dir . '/features/bootstrap/i18n', 0777, TRUE);

    $phpFinder = new PhpExecutableFinder();
    if (FALSE === $php = $phpFinder->find()) {
      throw new \RuntimeException('Unable to find the PHP executable.');
    }
    $this->workingDir = $dir;
    $this->phpBin = $php;
    $this->process = new Process(NULL);
  }

  /**
   * Creates a file with specified name and context in current workdir.
   *
   * @Given /^(?:there is )?a file named "([^"]*)" with:$/
   *
   * @param   string $filename name of the file (relative path)
   * @param   PyStringNode $content PyString string instance
   */
  public function aFileNamedWith($filename, PyStringNode $content) {
    $content = (string) $content;
    $content = $this->fixStepArgument($content);
    $this->createFile($this->workingDir . '/' . $filename, $content);
  }

  /**
   * Moves user to the specified path.
   *
   * @Given /^I am in the "([^"]*)" path$/
   *
   * @param   string $path
   */
  public function iAmInThePath($path) {
    $this->moveToNewPath($path);
  }

  /**
   * Checks whether a file at provided path exists.
   *
   * @Given /^file "([^"]*)" should exist$/
   *
   * @param   string $path
   */
  public function fileShouldExist($path) {
    PHPUnit_Framework_Assert::assertFileExists($this->workingDir . DIRECTORY_SEPARATOR . $path);
  }

  /**
   * Sets specified ENV variable
   *
   * @When /^"BEHAT_PARAMS" environment variable is set to:$/
   *
   * @param PyStringNode $value
   */
  public function iSetEnvironmentVariable(PyStringNode $value) {
    $this->process->setEnv(array('BEHAT_PARAMS' => (string) $value));
  }

  /**
   * Runs behat command with provided parameters
   *
   * @When /^I run "behat(?: ((?:\"|[^"])*))?"$/
   *
   * @param   string $argumentsString
   */
  public function iRunBehat($argumentsString = '') {
    $argumentsString = strtr($argumentsString, array('\'' => '"'));

    $this->process->setWorkingDirectory($this->workingDir);
    $this->process->setCommandLine(
      sprintf(
        '%s %s %s %s',
        $this->phpBin,
        escapeshellarg(BEHAT_BIN_PATH),
        $argumentsString,
        strtr('--format-settings=\'{"timer": false}\'', array(
          '\'' => '"',
          '"' => '\"'
        ))
      )
    );

    // Don't reset the LANG variable on HHVM, because it breaks HHVM itself
    if (!defined('HHVM_VERSION')) {
      $env = $this->process->getEnv();
      $env['LANG'] = 'en'; // Ensures that the default language is en, whatever the OS locale is.
      $this->process->setEnv($env);
    }

    $this->process->start();
    $this->process->wait();
  }

  /**
   * Checks whether previously ran command passes|fails with provided output.
   *
   * @Then /^it should (fail|pass) with:$/
   *
   * @param   string $success "fail" or "pass"
   * @param   PyStringNode $text PyString text instance
   */
  public function itShouldPassWith($success, PyStringNode $text) {
    $this->itShouldFail($success);
    $this->theOutputShouldContain($text);
  }

  /**
   * Checks whether specified file exists and contains specified string.
   *
   * @Then /^"([^"]*)" file should contain:$/
   *
   * @param   string $path file path
   * @param   PyStringNode $text file content
   */
  public function fileShouldContain($path, PyStringNode $text) {
    $path = $this->workingDir . '/' . $path;
    $this->fileShouldExist($path);

    $fileContent = trim(file_get_contents($path));
    // Normalize the line endings in the output
    if ("\n" !== PHP_EOL) {
      $fileContent = str_replace(PHP_EOL, "\n", $fileContent);
    }

    PHPUnit_Framework_Assert::assertEquals($this->getExpectedOutput($text), $fileContent);
  }

  /**
   * Checks whether last command output contains provided string.
   *
   * @Then the output should contain:
   *
   * @param   PyStringNode $text PyString text instance
   */
  public function theOutputShouldContain(PyStringNode $text) {
    PHPUnit_Framework_Assert::assertContains($this->getExpectedOutput($text), $this->getOutput());
  }

  private function getExpectedOutput(PyStringNode $expectedText) {
    $text = strtr($expectedText, array(
      '\'\'\'' => '"""',
      '%%TMP_DIR%%' => sys_get_temp_dir() . DIRECTORY_SEPARATOR
    ));

    // windows path fix
    if ('/' !== DIRECTORY_SEPARATOR) {
      $text = preg_replace_callback(
        '/ features\/[^\n ]+/', function ($matches) {
        return str_replace('/', DIRECTORY_SEPARATOR, $matches[0]);
      }, $text
      );
      $text = preg_replace_callback(
        '/\<span class\="path"\>features\/[^\<]+/', function ($matches) {
        return str_replace('/', DIRECTORY_SEPARATOR, $matches[0]);
      }, $text
      );
      $text = preg_replace_callback(
        '/\+[fd] [^ ]+/', function ($matches) {
        return str_replace('/', DIRECTORY_SEPARATOR, $matches[0]);
      }, $text
      );
    }

    return $text;
  }

  /**
   * Checks whether previously ran command failed|passed.
   *
   * @Then /^it should (fail|pass)$/
   *
   * @param   string $success "fail" or "pass"
   */
  public function itShouldFail($success) {
    if ('fail' === $success) {
      if (0 === $this->getExitCode()) {
        echo 'Actual output:' . PHP_EOL . PHP_EOL . $this->getOutput();
      }

      PHPUnit_Framework_Assert::assertNotEquals(0, $this->getExitCode());
    }
    else {
      if (0 !== $this->getExitCode()) {
        echo 'Actual output:' . PHP_EOL . PHP_EOL . $this->getOutput();
      }

      PHPUnit_Framework_Assert::assertEquals(0, $this->getExitCode());
    }
  }

  private function getExitCode() {
    return $this->process->getExitCode();
  }

  private function getOutput() {
    $output = $this->process->getErrorOutput() . $this->process->getOutput();

    // Normalize the line endings in the output
    if ("\n" !== PHP_EOL) {
      $output = str_replace(PHP_EOL, "\n", $output);
    }

    // Replace wrong warning message of HHVM
    $output = str_replace('Notice: Undefined index: ', 'Notice: Undefined offset: ', $output);

    return trim(preg_replace("/ +$/m", '', $output));
  }

  private function createFile($filename, $content) {
    $path = dirname($filename);
    if (!is_dir($path)) {
      mkdir($path, 0777, TRUE);
    }

    file_put_contents($filename, $content);
  }

  private function moveToNewPath($path) {
    $newWorkingDir = $this->workingDir . '/' . $path;
    if (!file_exists($newWorkingDir)) {
      mkdir($newWorkingDir, 0777, TRUE);
    }

    $this->workingDir = $newWorkingDir;
  }

  private static function clearDirectory($path) {
    $files = scandir($path);
    array_shift($files);
    array_shift($files);

    foreach ($files as $file) {
      $file = $path . DIRECTORY_SEPARATOR . $file;
      if (is_dir($file)) {
        self::clearDirectory($file);
      }
      else {
        unlink($file);
      }
    }

    rmdir($path);
  }

  /**
   * @When browser close cookies are expired
   */
  public function assertExpireCookies() {
    $jar = $this->getSession()->getDriver()->getClient()->getCookieJar();

    foreach ($jar->all() as $cookie) {
      if (!$cookie->getExpiresTime()) {
        $jar->expire($cookie->getName());
      }
    }
  }

  /**
   * @Given the session timeout is set to :timeout
   */
  public function assertSessionLifetime($timeout) {
    $this->autologin['timeout'] = variable_get('autologout_timeout');
    $this->autologin['padding'] = variable_get('autologout_padding');
    variable_set('autologout_timeout', $timeout);
    variable_set('autologout_padding', 0);
  }

  /**
   * @AfterScenario
   */
  public function resetAutologinTimeout() {
    if (isset($this->autologin['timeout'])) {
      variable_set('autologout_timeout', $this->autologin['timeout']);
    }
    if (isset($this->autologin['padding'])) {
      variable_set('autologout_padding', $this->autologin['padding']);
    }
  }

  /**
   * @When I sleep :sec seconds
   */
  public function assertSleep($seconds) {
    sleep($seconds);
  }

  /**
   * @Then :type content :title exists
   */
  public function assertContentExists($type, $title) {
    if (!$this->findContent($type, $title)) {
      throw new Exception(sprintf('Content of type "%s" with title "%s" does not exist.', $type, $title));
    }
  }

  /**
   * @Then :type content :title does not exist
   */
  public function assertContentNotExists($type, $title) {
    if ($this->findContent($type, $title)) {
      throw new Exception(sprintf('Content of type "%s" with title "%s" exists.', $type, $title));
    }
  }

  /**
   * Find a piece of content by type and title.
   *
   * @param string $type
   *   Node type.
   * @param string $title
   *   Node title.
   *
   * @return stdClass
   */
  public static function findContent($type, $title) {
    $q = new EfqHelper('node');
    $q->propertyCondition('type', $type)->propertyCondition('title', $title);
    return $q->first();
  }

  /**
   * Find text in a table row containing given text.
   *
   * @Then I should see (the text ):text in the ":col" column of the ":rowText" row
   */
  public function assertTextInTableRowCol($text, $col, $rowText) {
    /** @var \Drupal\DrupalExtension\Context\DrupalContext $drupal */
    $drupal = $this->getContext('\Drupal\DrupalExtension\Context\DrupalContext');
    $row = $drupal->getTableRow($this->getSession()->getPage(), $rowText);

    $cols = $row->findAll('css', 'th, td');
    if (empty($cols[$col - 1])) {
      throw new Exception(sprintf('No column "%s" found.', $col));
    }

    if (strpos($cols[$col - 1]->getText(), $text) === FALSE) {
      throw new \Exception(sprintf('Found a row containing "%s", but column "%s" did not contain the text "%s".', $rowText, $col, $text));
    }
  }

  /**
   * Mimics core queue processing.
   *
   * Processes a queue until no more items may be extracted. It is possible that
   * that items remain in the queue if they were unable to process.
   *
   * @When the :queue queue has been processed
   *
   * @see drupal_cron_run()
   */
  public function assertQueueComplete($queue_name) {

    $queue_info = module_invoke_all('cron_queue_info');
    drupal_alter('cron_queue_info', $queue_info);

    if (empty($queue_info[$queue_name])) {
      throw new Exception(sprintf('No queue "%s" exists.', $queue_name));
    }

    $info = $queue_info[$queue_name];
    $callback = $info['worker callback'];

    /** @var DrupalQueueInterface $queue */
    $queue = DrupalQueue::get($queue_name);
    $queue->createQueue();

    // Grab items from the queue until it's empty.
    while ($item = $queue->claimItem()) {
      try {
        call_user_func($callback, $item->data);
        $queue->deleteItem($item);
      } catch (Exception $e) {
        // In case of exception log it and leave the item in the queue
        // to be processed again later.
        watchdog_exception(__FUNCTION__, $e);
      }
    }
  }

  /**
   * Verifies left nav menu labels and href.
   *
   * Example table looks like:
   * | label    | href                 |
   * | Students | students/[org:XXDD1] |
   *
   * @Then the left nav contains the links:
   *
   * @param \Behat\Gherkin\Node\TableNode $table_node
   *
   * @throws \Exception
   */
  public function assertLeftNavItems(TableNode $table_node) {
    $assert = $this->assertSession();

    // Populate the menu items in an array keyed by title with href as the
    // value. The array variable is $items.
    $nav = $assert->elementExists('css', 'ul#main-menu.main-nav.left');
    $items = array();
    $elements = $nav->findAll('css', 'li a');
    foreach ($elements as $element) {
      $href = $element->getAttribute('href');
      $label = $element->find('css', '.title') ?
        $element->find('css', '.title')->getText() : $element->getText();
      // Call out a duplicate menu item
      if (array_key_exists($label, $items)) {
        throw new \Exception(sprintf('%s is duplicated in the menu.', $label));
      }
      $items[$label] = $href;
    }

    $exceptions = array();
    // Loop through the TableNode and see if the menu has the item.
    foreach ($table_node->getHash() as $hash) {
      $label = $hash['label'];
      $href = $this->fixStepArgument($hash['href']);

      if (!array_key_exists($label, $items)) {
        $exceptions[] = sprintf('Expecting menu item %s, found none.', $label);
      }
      elseif ($items[$label] != $href) {
        $exceptions[] = sprintf('Expecting %s href to be %s, but it is %s.', $label, $href, $items[$label]);
      }
    }
    if (!empty($exceptions)) {
      throw new \Exception(implode(PHP_EOL, $exceptions));
    }

  }

}
