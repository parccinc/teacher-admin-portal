<?php

use Behat\MinkExtension\Context\RawMinkContext;
use Behat\Mink\Element\NodeElement;

class HomeboxContext extends RawMinkContext {

  /**
   * Enable "debug" mode on the JS drag and drop, which prevents the drop.
   *
   * @var bool
   */
  protected $debug = FALSE;

  /**
   * @Then I should see homebox :homebox
   */
  public function assertSeeHomebox($homebox) {
    $this->findHomeBox($homebox);
  }

  /**
   * @When I expand homebox :homebox
   */
  public function assertExpandHomebox($homebox) {
    $h = $this->findHomeBox($homebox);
    if (!$element = $h->find('css', 'a.portlet-plus')) {
      throw new Exception(sprintf('Unable to expand homebox "%s".', $homebox));
    }
    $element->click();
  }

  /**
   * @When I collapse homebox :homebox
   */
  public function assertCollapseHomebox($homebox) {
    $h = $this->findHomeBox($homebox);
    if (!$element = $h->find('css', 'a.portlet-minus')) {
      throw new Exception(sprintf('Unable to collapse homebox "%s".', $homebox));
    }
    $element->click();
  }

  /**
   * @Then I should see homebox :homebox expanded
   */
  public function assertHomeboxExpanded($homebox) {
    $h = $this->findHomeBox($homebox);
    if (!$element = $h->find('css', 'a.portlet-minus')) {
      throw new Exception(sprintf('Homebox "%s" is not expanded.', $homebox));
    }
  }

  /**
   * @Then I should see homebox :homebox collapsed
   */
  public function assertHomeboxCollapsed($homebox) {
    $h = $this->findHomeBox($homebox);
    if (!$element = $h->find('css', 'a.portlet-plus')) {
      throw new Exception(sprintf('Homebox "%s" is not collapsed.', $homebox));
    }
  }

  /**
   * @When I drag homebox :homebox to :x, :y
   */
  public function assertDragHomeBoxToCoords($homebox, $x, $y) {
    $homeboxId = $this->findHomeBoxId($homebox);

    $this->includeJs();
    $js = "Drupal.paradsHomebox.moveElementAbsolute('#{$homeboxId}', {$x}, {$y});";
    $this->getSession()->getDriver()->executeScript($js);
    $this->getSession()->wait(1000);
  }

  /**
   * @When I drag homebox :homebox to :dest
   */
  public function assertDragHomeBoxToDest($homebox, $dest) {
    $homeboxId = $this->findHomeBoxId($homebox);
    $destId = $this->findHomeBoxId($dest);

    $this->includeJs();
    $js = "Drupal.paradsHomebox.moveElementDest('#{$homeboxId}', '#{$destId}');";
    $this->getSession()->getDriver()->executeScript($js);
    $this->getSession()->wait(1000);
  }

  /**
   * @When I drag homebox :homebox by :x, :y
   */
  public function assertDragHomeBoxRelative($homebox, $x, $y) {
    $homeboxId = $this->findHomeBoxId($homebox);

    $this->includeJs();
    $js = "Drupal.paradsHomebox.moveElementRelative('#{$homeboxId}', {$x}, {$y});";
    $this->getSession()->getDriver()->executeScript($js);
    $this->getSession()->wait(1000);
  }

  /**
   * @Then homebox :homebox should appear above homebox :homebox2
   */
  public function assertHomeBoxPositioning($homebox, $homebox2) {
    $homeboxId = $this->findHomeBoxId($homebox);
    $homebox2Id = $this->findHomeBoxId($homebox2);

    $this->includeJs();
    $js = "Drupal.paradsHomebox.elementPosition('#{$homeboxId}');";
    $pos1 = $this->getSession()->getDriver()->evaluateScript($js);
    $js = "Drupal.paradsHomebox.elementPosition('#{$homebox2Id}');";
    $pos2 = $this->getSession()->getDriver()->evaluateScript($js);

    if (!isset($pos1['y'])) {
      throw new Exception(sprintf('Unable to find position of homebox "%s".', $homebox));
    }
    if (!isset($pos2['y'])) {
      throw new Exception(sprintf('Unable to find position of homebox "%s".', $homebox2));
    }

    if ($pos2['y'] <= $pos1['y']) {
      throw new Exception(sprintf('Homebox "%s" (%s) appears above homebox "%s" (%s), but it should not.', $homebox2, $pos2['y'], $homebox, $pos1['y']));
    }
  }

  /**
   * Finds the homebox element by matching the title.
   *
   * @param string $homebox
   *   The title of the homebox.
   *
   * @return NodeElement
   *   The outer homebox element.
   *
   * @throws \Exception
   */
  protected function findHomeBox($homebox) {
    $elements = $this->getSession()->getPage()->findAll('css', '.portlet-title');
    foreach ($elements as $element) {
      if (strpos($element->getText(), $homebox) !== FALSE) {
        return $element->find('xpath', '../../..');
      }
    }
    throw new Exception(sprintf('Homebox "%s" not found.', $homebox));
  }

  /**
   * Finds the ID of the named homebox element.
   *
   * @param string $homebox
   *   The title of the homebox.
   *
   * @return string
   *   The "id" attribute of the homebox.
   */
  protected function findHomeboxId($homebox) {
    $h = $this->findHomeBox($homebox);
    return $h->getAttribute('id');
  }

  /**
   * Include the homebox.js file into the
   */
  protected function includeJs() {
    $js = file_get_contents(__DIR__ . '/js/homebox.js');
    $this->getSession()->getDriver()->executeScript($js);

    if ($this->debug) {
      $js = "Drupal.paradsHomebox.debug = true;";
      $this->getSession()->getDriver()->executeScript($js);
    }

  }

  /**
   * @When when I hover over :arg1
   */
  public function whenIHoverOver($arg1) {
    $session = $this->getSession(); // get the mink session
    $element = $session->getPage()->find('css', $arg1); // runs the actual query and returns the element

    // errors must not pass silently
    if (null === $element) {
      throw new \InvalidArgumentException(sprintf('Could not evaluate CSS selector: "%s"', $arg1));
    }

    // ok, let's hover it
    $element->mouseOver();
  }

  /**
   * @Then /^I wait for css element "([^"]*)" to "([^"]*)"$/
   */
  public function iWaitForCssElement($element, $appear) {
    $xpath = $this->getSession()->getSelectorsHandler()->selectorToXpath('css', $element);
    $this->waitForXpathNode($xpath, $appear == 'appear');
  }

  /**
   * Wait for an element by its XPath to appear or disappear.
   *
   * @param string $xpath
   *   The XPath string.
   * @param bool $appear
   *   Determine if element should appear. Defaults to TRUE.
   *
   * @throws Exception
   */
  private function waitForXpathNode($xpath, $appear = TRUE, $only_check_node_existence = FALSE) {
    $this->waitFor(function($context) use ($xpath, $appear, $only_check_node_existence) {
      try {
        $nodes = $context->getSession()->getDriver()->find($xpath);
          if (count($nodes) > 0) {
            if ($only_check_node_existence) {
              return TRUE;
            }
            $visible = $nodes[0]->isVisible();
            return $appear ? $visible : !$visible;
          }
          return !$appear;
      }
      catch (WebDriver\Exception $e) {
        if ($e->getCode() == WebDriver\Exception::NO_SUCH_ELEMENT) {
          return !$appear;
        }
        throw $e;
      }
    });
  }

  /**
   * Helper function; Execute a function until it return TRUE or timeouts.
   *
   * @param $fn
   *   A callable to invoke.
   * @param int $timeout
   *   The timeout period. Defaults to 30 seconds.
   *
   * @throws Exception
   */
  private function waitFor($fn, $timeout = 30000) {
    if (empty($timeout)) {
      $timeout = 30000;
    }
    $start = microtime(true);
    $end = $start + $timeout / 1000.0;
    while (microtime(true) < $end) {
      if ($fn($this)) {
        return;
      }
    }
    throw new \Exception('waitFor timed out.');
  }

  /**
   * @When I hover over :arg1 element :arg2 is visible
   */
  public function iHoverOverElementIsVisible($arg1, $arg2) {
    // Get the mink session.
    $session = $this->getSession();
    // Runs the actual query and returns the element.
    $element = $session->getPage()->find('css', $arg1);

    // Errors must not pass silently.
    if (null === $element) {
      throw new \InvalidArgumentException(sprintf('Could not evaluate CSS selector: "%s"', $arg1));
    }

    // Hover over element and check to see element that shows.
    // This may work for now, but I'm not entirely sure that this would be,
    // Correct with the Zurb Tooltips setup. The ID matches each other in both,
    // Instances and they are both a span element. So I'm pretty sure that this,
    // Could be made to use the data-selector element or you have to make sure,
    // That your CSS matches correctly. For instance:
    // I hover over span#icon element #icon.tooltip is visible
    // Would work with Zurb Tooltips.
    $element->mouseOver();
    $csspath = $arg2;
    $this->iWaitForCssElement($csspath, TRUE);
  }

}
