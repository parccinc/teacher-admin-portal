<?php
/**
 * @file
 * Contains \DrupalFormContext.
 *
 * @see https://gist.github.com/pbuyle/7698675
 */

use Behat\Behat\Context\SnippetAcceptingContext;
use Drupal\DrupalExtension\Context\RawDrupalContext;

class DrupalFormContext extends RawDrupalContext implements SnippetAcceptingContext {
  /**
   * Checks, that form element with specified label is visible on page.
   *
   * @Then /^(?:|I )should see an? "(?P<label>[^"]*)" form element$/
   */
  public function assertFormElementOnPage($label) {
    $element = $this->getSession()->getPage();
    $nodes = $element->findAll('css', '.form-item label');
    foreach ($nodes as $node) {
      if ($node->getText() === $label) {
        if ($node->isVisible()) {
          return $node;
        }
        else {
          throw new \Exception("Form item with label \"$label\" not visible.");
        }
      }
    }
    throw new \Behat\Mink\Exception\ElementNotFoundException($this
      ->getSession(), 'form item', 'label', $label);
  }

  /**
   * Checks, that form element with specified label and type is visible on page.
   *
   * @Then /^(?:|I )should see an? "(?P<label>[^"]*)" (?P<type>[^"]*) form element$/
   */
  public function assertTypedFormElementOnPage($label, $type) {
    $container = $this->getSession()->getPage();
    $class = "form-type-{$type}";
    $label_nodes = $container->findAll('css', '.form-item label');
    foreach ($label_nodes as $label_node) {
      // Note: getText() will return an empty string when using Selenium2D. This
      // is ok since it will cause a failed step.
      if (($label_node->getText() === $label || $label_node->getText() === $label . ' *')
        && $label_node->hasClass($class)
        && $label_node->isVisible()
      ) {
        return;
      }
    }
    throw new \Behat\Mink\Exception\ElementNotFoundException($this
      ->getSession(), $type . ' form item', 'label', $label);
  }

  /**
   * Checks, that element with specified CSS is not visible on page.
   *
   * @Then /^(?:|I )should not see an? "(?P<label>[^"]*)" form element$/
   */
  public function assertFormElementNotOnPage($label) {
    $element = $this->getSession()->getPage();
    $nodes = $element->findAll('css', '.form-item label');
    foreach ($nodes as $node) {
      // Note: getText() will return an empty string when using Selenium2D. This
      // is ok since it will cause a failed step.
      if ($node->getText() === $label && $node->isVisible()) {
        throw new \Exception();
      }
    }
  }

  /**
   * Checks, that form element with specified label and type is not visible on page.
   *
   * @Then /^(?:|I )should not see an? "(?P<label>[^"]*)" (?P<type>[^"]*) form element$/
   */
  public function assertTypedFormElementNotOnPage($label, $type) {
    $container = $this->getSession()->getPage();
    $pattern = '/(^| )form-type-' . preg_quote($type) . '($| )/';
    $label_nodes = $container->findAll('css', '.form-item label');
    foreach ($label_nodes as $label_node) {
      // Note: getText() will return an empty string when using Selenium2D. This
      // is ok since it will cause a failed step.
      if ($label_node->getText() === $label
        && preg_match($pattern, $label_node->getAttribute('class'))
        && $label_node->isVisible()
      ) {
        throw new \Behat\Mink\Exception\ElementNotFoundException($this
          ->getSession(), $type . ' form item', 'label', $label);
      }
    }
  }

  /**
   * Checks that element is disabled.
   *
   * @Then /^the "(?P<label>[^"]*)" form element should be disabled$/
   */
  public function assertElementDisabled($label) {
    $field = $this->getSession()->getPage()->findField($label);
    if (!$field) {
      throw new \Exception("Form item with label \"$label\" not found.");
    }
    if ($field->hasAttribute('disabled')) {
      // What kind of disabled?
      // Leg disabled!
      return $field;
    }
    else {
      throw new \Exception("Form item with label \"$label\" not disabled.");
    }
  }

  /**
   * Checks that element is enabled.
   *
   * @Then /^the "(?P<label>[^"]*)" form element should be enabled$/
   */
  public function assertElementEnabled($label) {
    $field = $this->getSession()->getPage()->findField($label);
    if ($field->hasAttribute('disabled')) {
      // May I ask how you became disabled?
      // Acid...?
      return $field;
    }
    else {
      throw new \Exception("Form item with label \"$label\" not enabled.");
    }
  }

  /**
   * Ends the session to close the browser.
   *
   * @Then /^(?:|I )close the browser$/
   */
  public function browserClose() {
    $this->getSession()->stop();
  }

  /**
   * @Then I click :arg1 in the :arg2 element
   */
  public function iClickInTheElement($link, $css) {
    $page = $this->getSession()->getPage();
    $elements = $page->findAll('css', $css);
    if (empty($elements)) {
      throw new \Exception(sprintf('No rows found on the page %s', $this->getSession()
            ->getCurrentUrl()));
    }
    foreach ($elements as $element) {
      if ($foundlink = $element->findLink($link)) {
        // Click the link and return.
        $foundlink->click();
        return;
      }
      throw new \Exception(sprintf('Found an element "%s", but no "%s" link on the page', $css, $link));

    }
    throw new \Exception(sprintf('Failed to find an element "%s"', $css));
  }

  /**
   * @Then I should see the :name checkbox
   */
  public function assertSeeCheckbox($name) {
    if (!$checkbox = $this->findCheckbox($name)) {
      throw new Exception(sprintf('Did not find checkbox "%s" on %s.', $name, $this->getSession()->getCurrentUrl()));
    }
  }

  /**
   * @Then I should not see the :name checkbox
   */
  public function assertNotSeeCheckbox($name) {
    if ($checkbox = $this->findCheckbox($name)) {
      throw new Exception(sprintf('Found checkbox "%s" on %s.', $name, $this->getSession()->getCurrentUrl()));
    }
  }

  /**
   * Find a checkbox on the page.
   *
   * @param $name
   * @return \Behat\Mink\Element\NodeElement|null
   */
  protected function findCheckbox($name) {
    if (!$field = $this->getSession()->getPage()->find('named', array('field', $name))) {
      return;
    }

    if ($field->getTagName() == 'input' && $field->getAttribute('type') == 'checkbox') {
      return $field;
    }
  }

  /**
   * @Then I should see the :name radio
   */
  public function assertSeeRadio($name) {
    if (!$checkbox = $this->findRadio($name)) {
      throw new Exception(sprintf('Did not find radio "%s" on %s.', $name, $this->getSession()->getCurrentUrl()));
    }
  }

  /**
   * @Then I should not see the :name radio
   */
  public function assertNotSeeRadio($name) {
    if ($checkbox = $this->findRadio($name)) {
      throw new Exception(sprintf('Found radio "%s" on %s.', $name, $this->getSession()->getCurrentUrl()));
    }
  }

  /**
   * Find a radio on the page.
   *
   * @param $name
   * @return \Behat\Mink\Element\NodeElement|null
   */
  protected function findRadio($name) {
    if (!$field = $this->getSession()->getPage()->find('named', array('field', $name))) {
      return;
    }

    if ($field->getTagName() == 'input' && $field->getAttribute('type') == 'radio') {
      return $field;
    }
  }
}
