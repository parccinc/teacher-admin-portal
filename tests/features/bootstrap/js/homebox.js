(function($) {

    Drupal.paradsHomebox = Drupal.paradsHomebox || {
        debug: false,

        /**
         * Move element to absolute position within document.
         */
        moveElementAbsolute: function (selector, x, y) {
            var element = $(selector + " .portlet-title")[0];
            this.moveElement(element, x, y);
        },

        /**
         * Move element to the position of an.
         */
        moveElementDest: function (selector, destSelector) {
            var element = $(selector + " .portlet-title")[0];
            var elementCoords = findCenter(element);

            var dest = $(destSelector)[0];
            var destCenter = findCenter(dest);
            var destBottom = findBottom(dest);

            // When dragging down we have to go past the destination, then back
            // up.
            if (elementCoords.y < destCenter.y) {
                this.moveElement(element, destBottom.x, destBottom.y, true, false);
                var home = this;
                setTimeout(function () {
                    home.moveElement(element, destCenter.x, destCenter.y, false, true);
                }, 250);
            } else {
                this.moveElement(element, destCenter.x, destCenter.y);
            }

        },

        moveElementRelative: function (selector, x, y) {
            var element = $(selector + " .portlet-title")[0];
            var elementCoords = findCenter(element);
            this.moveElement(element, elementCoords.x + x, elementCoords.y + y);
        },

        moveElement: function (element, x, y, drag, drop) {
            if (typeof drag == "undefined") {
                drag = true;
            }
            if (typeof drop == "undefined") {
                drop = true;
            }

            var elementCoords = findCenter(element);

            if (drag) {
                this.mouseDown(element, elementCoords.x, elementCoords.y);
            }

            this.mouseMove(x, y);

            if (drop && !this.debug) {
                var home = this;
                setTimeout(function () {
                    home.mouseUp(element, x, y);
                }, 250);
            }
        },

        elementPosition: function (selector) {
            var element = $(selector + " .portlet-title")[0];
            return findCenter(element);
        },

        mouseDown: function(element, x, y) {
            dispatchEvent(element, 'mousedown', createEvent('mousedown', element, { clientX: x, clientY: y }));
        },
        mouseMove: function (x, y) {
            dispatchEvent(document, 'mousemove', createEvent('mousemove', document, { clientX: x, clientY: y }));
        },
        mouseUp: function (element, x, y) {
            dispatchEvent(element, 'mouseup', createEvent('mouseup', element, { clientX: x, clientY: y }));
        }
    };

    function createEvent(type, target, options) {
        var evt;
        var e = $.extend({
            target: target,
            preventDefault: function() { },
            stopImmediatePropagation: function() { },
            stopPropagation: function() { },
            isPropagationStopped: function() { return true; },
            isImmediatePropagationStopped: function() { return true; },
            isDefaultPrevented: function() { return true; },
            bubbles: true,
            cancelable: (type != "mousemove"),
            view: window,
            detail: 0,
            screenX: 0,
            screenY: 0,
            clientX: 0,
            clientY: 0,
            ctrlKey: false,
            altKey: false,
            shiftKey: false,
            metaKey: false,
            button: 0,
            relatedTarget: undefined
        }, options || {});

        if ($.isFunction(document.createEvent)) {
            evt = document.createEvent("MouseEvents");
            evt.initMouseEvent(type, e.bubbles, e.cancelable, e.view, e.detail,
                e.screenX, e.screenY, e.clientX, e.clientY,
                e.ctrlKey, e.altKey, e.shiftKey, e.metaKey,
                e.button, e.relatedTarget || document.body.parentNode);
        } else if (document.createEventObject) {
            evt = document.createEventObject();
            $.extend(evt, e);
            evt.button = { 0:1, 1:4, 2:2 }[evt.button] || evt.button;
        }
        return evt;
    }

    function dispatchEvent(el, type, evt) {
        if (el.dispatchEvent) {
            el.dispatchEvent(evt);
        } else if (el.fireEvent) {
            el.fireEvent('on' + type, evt);
        }
        return evt;
    }

    function findCenter(el) {
        var elm = $(el),
            o = elm.offset();
        return {
            x: o.left + elm.outerWidth() / 2,
            y: o.top + elm.outerHeight() / 2
        };
    }

    function findBottom(el) {
        var elm = $(el),
            o = elm.offset();
        return {
            x: o.left + elm.outerWidth() / 2,
            y: o.top + elm.outerHeight()
        };
    }

} )(jQuery);
