<?php
/**
 * @file
 * Contains \BtDrupalContext.
 */

use Drupal\DrupalExtension\Context\DrupalContext;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;

class BtDrupalContext extends DrupalContext {

  protected $contexts = array();

  /**
   * @BeforeScenario
   */
  public function setContexts(BeforeScenarioScope $scope) {
    $this->contexts['mink'] = $scope->getEnvironment()
      ->getContext('PadsMinkContext');
  }

  public function login() {
    // Check if logged in.
    if ($this->loggedIn()) {
      $this->logout();
    }

    if (!$this->user) {
      throw new \Exception('Tried to login without a user.');
    }

    if (module_exists('pads_sso_user')) {
      $login_path = '/user/local-login';
    }
    else {
      $login_path = '/user';
    }
    $this->getSession()->visit($this->locatePath($login_path));
    $element = $this->getSession()->getPage();
    $element->fillField($this->getDrupalText('username_field'), $this->user->name);
    $element->fillField($this->getDrupalText('password_field'), $this->user->pass);
    $submit = $element->findButton($this->getDrupalText('log_in'));
    if (empty($submit)) {
      throw new \Exception(sprintf("No submit button at %s", $this->getSession()
        ->getCurrentUrl()));
    }

    // Log in.
    $submit->click();

    if (!$this->loggedIn()) {
      throw new \Exception(sprintf("Failed to log in as user '%s' with role '%s'", $this->user->name, $this->user->role));
    }
  }

  public function assertLoggedInByName($name) {
    $name = $this->contexts['mink']->fixStepArgument($name);
    parent::assertLoggedInByName($name);
  }

  public function createUsers(TableNode $usersTable) {
    $newTable = $this->contexts['mink']->fixStepArgumentTableNode($usersTable);
    parent::createUsers($newTable);
  }

  /**
   * @Then I delete the user :mail
   */
  public function iDeleteTheUser($mail) {
    $mail = $this->contexts['mink']->fixStepArgument($mail);
    $account = user_load_by_mail($mail);
    if ($account) {
      $uid = $account->uid;
      user_delete($uid);
    }
    else {
      throw new \Exception(sprintf("User '%s' does not exist.", $mail));
    }
  }

  /**
   * @Then the user :mail should have the role :role
   */
  public function assertUserHasRole($mail, $role) {
    $mail = $this->contexts['mink']->fixStepArgument($mail);
    entity_get_controller('user')->resetCache();
    $account = user_load_by_mail($mail);
    if ($account) {
      $roles = user_roles();
      $rid = array_search($role, $roles);
      if (!user_has_role($rid, $account)) {
        throw new \Exception(sprintf("User '%s' does not have role '%s'.", $mail, $role));
      }

    }
    else {
      throw new \Exception(sprintf("User '%s' does not exist.", $mail));
    }
  }

  /**
   * @Then the user :mail should not have the role :role
   */
  public function assertUserNotHasRole($mail, $role) {
    $mail = $this->contexts['mink']->fixStepArgument($mail);
    entity_get_controller('user')->resetCache();
    $account = user_load_by_mail($mail);
    if ($account) {
      $roles = user_roles();
      $rid = array_search($role, $roles);
      if (user_has_role($rid, $account)) {
        throw new \Exception(sprintf("User '%s' has role '%s', but should not.", $mail, $role));
      }

    }
    else {
      throw new \Exception(sprintf("User '%s' does not exist.", $mail));
    }
  }

  /**
   * @Then the user :mail should be activated
   */
  public function assertUserActivated($mail) {
    $mail = $this->contexts['mink']->fixStepArgument($mail);
    entity_get_controller('user')->resetCache();
    if (!$account = user_load_by_mail($mail)) {
      throw new Exception(sprintf('Unbale to load user "%s".', $mail));
    }
    if (!$account->status) {
      throw new Exception(sprintf('User "%s" disabled, but should not be.', $mail));
    }
  }

  /**
   * @Then the user :mail should be disabled
   */
  public function assertUserDisabled($mail) {
    $mail = $this->contexts['mink']->fixStepArgument($mail);
    entity_get_controller('user')->resetCache();
    if (!$account = user_load_by_mail($mail)) {
      throw new Exception(sprintf('Unbale to load user "%s".', $mail));
    }
    if ($account->status) {
      throw new Exception(sprintf('User "%s" activated, but should not be.', $mail));
    }
  }

}
