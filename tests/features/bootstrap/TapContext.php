<?php

use Behat\Behat\Context\Context;
use Parcc\Tap\Driver;

class TapContext implements Context {

  /**
   * @BeforeScenario
   */
  function reset() {
    $driver = new Driver();
    $driver->reset();
  }

}
