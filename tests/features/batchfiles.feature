@api @public_files @batch
Feature: Batch file functionality

  Background:
    Given users:
      | name             | pass  | mail                   | roles             |
      | behat_parccuser  | behat | parccuser@example.com  | PARCC user        |
      | behat_parccuser2 | behat | parccuser2@example.com | PARCC user        |
      | behat_parccadmin | behat | parccadmin@example.com | ads administrator |
      | behat_batchuser  | behat | batchuser@example.com  | batch user        |

    And there is a file named "behat.csv" with:
    """
    "column1","column2","column3"
    "foo","bar",1
    """

    And secure file content:
      | title    | author           | field_file |
      | pubatch1 | behat_parccuser  | behat.csv  |
      | pubatch2 | behat_parccuser  | behat.csv  |
      | pubatch3 | behat_parccuser2 | behat.csv  |
      | pabatch1 | behat_parccadmin | behat.csv  |
      | pabatch2 | behat_parccadmin | behat.csv  |
      | bubatch1 | behat_batchuser  | behat.csv  |
      | bubatch2 | behat_batchuser  | behat.csv  |

  @javascript
  Scenario: Adding a batch file and searching for it
    Given I am logged in as a user with the "create secure_file content" permission
    And I am at "files"
    And I press the "addfile-button" button
    And I wait for AJAX to finish
    When I fill in "Batch name *" with "Test batch"
    And I fill in "Batch description *" with "Testing out batch creation"
    And I add file "behat.csv" to element "input[type='file']"
    And I wait for AJAX to finish
    And I wait 2 seconds
    And I press the "Save" button
    And I wait for AJAX to finish
    And I wait 2 seconds
    Then I should see the success message "Secure file Test batch has been created."
    And I fill in "edit-terms" with "Test batch"
    And I press the "All" button
    And the ".view-parcc-secure-files .view-content tbody" element should contain "Test batch"

  Scenario Outline: Share link is available on the SFTP server
    Given I am logged in as "<user name>"
    And I am at "files"
    Then the ".view-parcc-secure-files .view-content tbody" element should contain "<file name>"
    And I should see an "i.fi-share" element

    Examples:
      | user name        | file name |
      | behat_parccuser  | pubatch1  |
      | behat_parccadmin | pabatch1  |

  Scenario Outline: I see available recipient to share with
    Given I am logged in as "<user name>"
    When  I am at "files"
    Then the "table.views-table tbody tr:nth-child(<row>) > td.views-field.views-field-title" element should contain "<file name>"

    When I click the "table.views-table tbody tr:nth-child(<row>) > td.views-field.views-field-pads-secure-file-share > a:nth-child(1)"
    And I fill in "batchuser@example.com" for "edit-acl-view-add"
    And I press the "Add User" button
    Then I see the text "batchuser@example.com"
    And I see the button "Submit"
    Examples:
      | user name        | file name | row |
      | behat_parccuser  | pubatch1  | 1   |
      | behat_parccadmin | pabatch1  | 4   |

  @parads-1332
  Scenario Outline: PARADS-1332 I share a file
    Given I am logged in as "<user name>"
    And I am at "files"
    And I click the "table.views-table tbody tr:nth-child(<row>) > td.views-field.views-field-pads-secure-file-share > a:nth-child(1)"
    And I fill in "batchuser@example.com" for "edit-acl-view-add"
    And I press the "Add User" button
    When I press the "Submit" button
    Then the "pads_secure_file_shared" message to "batchuser@example.com" should contain the text "A file has been shared with you"


  #batchuser can see the file
    When I am logged in as "behat_batchuser"
    And I am at "files"
    Then the "table.views-table tbody" element should contain "<file name>"

    #I can view and download, but can't edit, delete or share the file shared with me.
    And I should see an "table.views-table tbody tr:nth-child(1) > td.views-field.views-field-pads-secure-file-share i.fi-eye" element
    And I should see an "table.views-table tbody tr:nth-child(1) > td.views-field.views-field-pads-secure-file-share i.fi-download" element
    And I should not see an "table.views-table tbody tr:nth-child(1) > td.views-field.views-field-pads-secure-file-share i.fi-trash" element
    And I should not see an "table.views-table tbody tr:nth-child(1) > td.views-field.views-field-pads-secure-file-share i.fi-pencil" element
    And I should not see an "table.views-table tbody tr:nth-child(1) > td.views-field.views-field-pads-secure-file-share i.fi-share" element

    Examples:
      | user name        | file name | row |
      | behat_parccuser  | pubatch1  | 1   |
      | behat_parccadmin | pabatch1  | 4   |


  Scenario Outline: Confirm Parcc roles can delete their own content
    Given I am logged in as "<user name>"
    And I am at "files"
    When I click the "#views-form-parcc-secure-files-panel-pane-1 table.views-table tbody tr:nth-child(<row>) > td.views-field.views-field-pads-secure-file-share > a:nth-child(4)"
    And I press the "Delete" button
    Then the ".view-parcc-secure-files .view-content tbody" element should not contain "<file name>"
    Examples:
      | user name        | file name | row |
      | behat_parccadmin | pabatch1  | 4   |
      | behat_parccuser  | pubatch1  | 1   |

  Scenario: Confirm that PARCC roles cannot delete the content of others
    #User 2 shares files with user 1.
    Given I am logged in as "behat_parccuser2"
    And I am at "files"
    And I click the "table.views-table tbody tr:nth-child(1) > td.views-field.views-field-pads-secure-file-share > a:nth-child(1)"
    And I fill in "parccuser@example.com" for "edit-acl-view-add"
    And I press the "Add User" button
    And I press the "Submit" button

    #User 1 logs in and does not see delete for user 2's file.
    And I am logged in as "behat_parccuser"
    And I am at "files"
    And the ".view-parcc-secure-files .view-content tbody" element should contain "pubatch3"
    #The pubatch3 is row 3
    And I should not see an ".view-parcc-secure-files .view-content tbody tr:nth-child(3) i.fi-trash" element

  Scenario: Confirm recipient can remove files they uploaded
    Given I am logged in as "behat_batchuser"
    And I am at "files"
    And the ".views-table" element should contain "bubatch1"
    When I click the "#views-form-parcc-secure-files-panel-pane-1 table.views-table tbody tr:nth-child(1) > td.views-field.views-field-pads-secure-file-share > a:nth-child(3)"
    And I press the "Delete" button
    Then I should not see "bubatch1" in the ".views-table" element

  Scenario: confirm that non-parcc roles can only see their own files
    Given I am logged in as "behat_batchuser"
    When I am at "files"
    Then I should not see "pubatch1"


  Scenario:  Share file more than once to same recipient
    Given I am logged in as "behat_parccuser"
    And I am at "files"

    #Share pubatch1 in first row.
    And I click the "table.views-table tbody tr:nth-child(1) > td.views-field.views-field-pads-secure-file-share > a:nth-child(1)"
    And I fill in "batchuser@example.com" for "edit-acl-view-add"
    And I press the "Add User" button
    And I press the "Submit" button
    And I am at "files"

    #Share pubatch1 again
    And I click the "table.views-table tbody tr:nth-child(1) > td.views-field.views-field-pads-secure-file-share > a:nth-child(1)"
    And I fill in "parccadmin@example.com" for "edit-acl-view-add"
    And I press the "Add User" button
    And I press the "Submit" button

    #I can see users I've shared pubatch1 with
    And I click the "table.views-table tbody tr:nth-child(1) > td.views-field.views-field-pads-secure-file-share > a:nth-child(1)"
    Then I should see "batchuser@example.com"
    And I should see "parccadmin@example.com"


  @batch-bulk-operations @javascript
  Scenario: As a PARCC Admin I want to be able to delete multiple files with a single operation
    Given I am logged in as "behat_parccadmin"
    And I am at "files"
    And I maximize the window
    And I should see an "div.pane-addfile" element
    When I check the box for the row containing "pabatch1"
    And I check the box for the row containing "pabatch2"
    And I select "Delete Files" from "operation"
    And I press the "Apply" button
    And I should not see an "div.pane-addfile" element
    And I press the "Confirm" button
    And I wait for the batch job to finish
    And I wait for AJAX to finish
    Then the ".view-parcc-secure-files .view-content tbody" element should not contain "pabatch1"
    And the ".view-parcc-secure-files .view-content tbody" element should not contain "pabatch2"

  @batch-bulk-operations @javascript
  Scenario: As a non-parcc user I can delete my own files but not those shared with me.
    Given I am logged in as "behat_parccuser"
    And I am at "files"
    #I share pubatch1 in the firt row
    And I click xpath '//*[@id="views-form-parcc-secure-files-panel-pane-1"]/div/table[2]/tbody/tr[1]/td[6]/a[1]'
    And I fill in "batchuser@example.com" for "edit-acl-view-add"
    And I wait for AJAX to finish
    And I press the "Add User" button
    And I wait for AJAX to finish
    And I press the "Submit" button
    And I am logged in as "behat_batchuser"
    And I am at "files"
    When I check the box for the row containing "pubatch1"
    And I check the box for the row containing "bubatch1"
    And I select "Delete Files" from "operation"
    And I press the "Apply" button
    And I press the "Confirm" button
    And I wait for the batch job to finish
    And I wait for AJAX to finish
    Then the ".view-parcc-secure-files .view-content tbody" element should contain "pubatch1"
    And the ".view-parcc-secure-files .view-content tbody" element should not contain "bubatch1"
    And I should see the message containing "You do not have access to delete pubatch1. The delete operation on this batch was skipped."

  @batch-bulk-operations
  Scenario: As a PARCC Admin I want to be able to download multiple files at the same time
    Given I am logged in as "behat_parccadmin"
    And I am at "files"
    When I check the box for the row containing "pubatch1"
    And I check the box for the row containing "pubatch2"
    And I check the box for the row containing "pabatch1"
    And I select "Download Files" from "operation"
    And I press the "Apply" button
    And I follow meta refresh
    Then I should see the success message "Your download of the files should begin shortly. If the download does not start, please click here to start download."
    And I should not see "insufficient permissions"

  @parads-4063 @javascript
  Scenario: PARADS-4063 Display message only when a search is performed.
    Given I am logged in as "behat_batchuser"
    And I am at "files"
    Then I should not see the message "No Results were found."
    When I fill in "bubatch3" for "edit-terms"
    And I wait for AJAX to finish
    And I wait 2 seconds
    Then I should see "No Results were found." in the ".view-parcc-secure-files td.views-empty" element

    When I reload the page
    Then I should not see the message "No Results were found."

  @ads-563 @ads-1612
  Scenario: Secure File Expiration
    Given secure file content:
      | title         | author           | field_file | created      | delete      |
      | newbatch      | behat_parccuser  | behat.csv  |              | now +1 week |
      | oldbatch      | behat_parccuser  | behat.csv  | now -2 weeks | now -1 week |
      | newshortbatch | behat_parccuser  | behat.csv  |              | now +1 hour |
      | oldshortbatch | behat_parccuser  | behat.csv  | now -2 hours | now -1 hour |
      | newnovalbatch | behat_parccuser  | behat.csv  |              |             |
      | oldnovalbatch | behat_parccuser  | behat.csv  | now -2 weeks |             |
    And "pads_standards" content:
      | field_evidence_statement | created      |
      | Old Standard             | now -2 weeks |
      | New Standard             |              |
    And secure file "newnovalbatch" has no delete value set
    And secure file "oldnovalbatch" has no delete value set
    When I run cron
    Then "secure_file" content "newbatch" exists
    And "secure_file" content "oldbatch" does not exist
    And "secure_file" content "newshortbatch" exists
    And "secure_file" content "oldshortbatch" does not exist
    And "secure_file" content "newnovalbatch" exists
    And "secure_file" content "oldnovalbatch" does not exist
    And "pads_standards" content "Old Standard" exists
    And "pads_standards" content "New Standard" exists
