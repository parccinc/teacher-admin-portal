@api @students @student_actions @bulk_operations
Feature: Actions for students

  Background:
    Given LDR organizations:
      | name   | type     | identifier | parent     |
      | XXD1   | district | 1          | XX         |
      | XXD1S1 | school   | 2          | [org:XXD1] |
      | XXD1S2 | school   | 3          | [org:XXD1] |
    And TAP users:
      | name                | mail                      | roles                      | organizations |
      | Sir Patrick Stewart | makeitso@engage.net       | organization administrator | [org:XXD1S1]  |
      | John Kimball        | itsnotatumor@1234.sd2.org | test administrator         | [org:XXD1S1]  |
    And LDR class orgs:
      | name                 | section | org          | grade | ref    |
      | Discrete Mathematics | a-12345 | [org:XXD1S1] | 2     | dismat |
      | Integral Calculus    | b-12345 | [org:XXD1S1] | 2     | intcal |
      | Dinosaur Roaring     | c-12345 | [org:XXD1S1] | 1     | dinroa |
      | Hair Braiding        | d-12345 | [org:XXD1S1] | 1     | haibra |
    And LDR students:
      | firstName | lastName | enrollOrgId  | dateOfBirth | gender | gradeLevel | stateIdentifier | classes |
      | Louis     | Skolnick | [org:XXD1S1] | 2005-01-01  | M      | 3          | IL1230001       |         |
      | Gilbert   | Lowell   | [org:XXD1S1] | 2005-01-01  | M      | 3          | IL1230002       |         |


  @parads-3106 @as
  Scenario: PARADS-3106 Action dropdown on student page
    Given I am logged in as "Sir Patrick Stewart"
    And I am on the homepage
    And I click "My Students"
    When I am at "students/[org:XXD1S1]"
    And I check the box for the row containing "IL1230001"
    And I check the box for the row containing "IL1230002"
    Then I select "Assign to Class" from "operation"
    And I see the button "Next"

  @parads-2609 @javascript @as
  Scenario: PARADS-2609 Config form for assign class to student operation
    Given I am logged in as "Sir Patrick Stewart"
    And I am at "students/[org:XXD1S1]"
    When I check the box for the row containing "IL1230001"
    And I check the box for the row containing "IL1230002"
    And I select "Assign to Class" from "operation"
    And I press "Next"

    # Message listing students we are assigning classes for.
    Then I should see "Select classes to assign the following 2 students"
    And I should see "Louis Skolnick"
    And I should see "Gilbert Lowell"

    # Class table should be there.
    And I should see "Class name" in the "table.tableselect-classes thead tr" element
    And I should see "Section" in the "table.tableselect-classes thead tr" element

    # JavaScript needs to be enabled to see the grouping by grade.
    And the xpath '//*[@id="datatable-2"]/tbody/tr[4]' element should contain "Grade 2"

    # Action buttons.
    And I see the button "Next"
    And I should see the link "Cancel"

  @parads-2218 @ads-1246
  Scenario: PARADS-2218 Perform student class assignment.
    Given LDR students:
      | firstName | lastName | enrollOrgId  | dateOfBirth | gender | gradeLevel | stateIdentifier | classes |
      | Carlton   | Summers  | [org:XXD1S1] | 2005-01-01  | M      | 3          | IL1230003       |         |
    And I am logged in as "Sir Patrick Stewart"

    When I am at "students/[org:XXD1S1]"
    And I check the box for the row containing "IL1230001"
    And I check the box for the row containing "IL1230002"
    And I select "Assign to Class" from "operation"
    And I press "Next"

    # Select classes.
    And I check the box for the row containing "Dinosaur Roaring"
    And I check the box for the row containing "Hair Braiding"
    And I press "Next"
    And I follow meta refresh

    # Infamous vbo message replaced with custom messaging.
    Then I should not see the text "Performed Assign to Class"
    And I should see the text "2 students successfully assigned to Class Dinosaur Roaring-c-12345."
    And I should see the text "2 students successfully assigned to Class Hair Braiding-d-12345."
    And I should not see the text "unsuccessfully assigned."
    And I should not see the text "More Details"
    # And we are at students page
    And I should be on "students/[org:XXD1S1]"

    # Run through again, should see all "unsuccessfully assigned" messages.
    When I check the box for the row containing "IL1230001"
    And I check the box for the row containing "IL1230002"
    And I select "Assign to Class" from "operation"
    And I press "Next"
    And I check the box for the row containing "Dinosaur Roaring"
    And I check the box for the row containing "Hair Braiding"
    And I press "Next"
    And I follow meta refresh

    Then I should not see the text "Performed Assign to Class"
    And I should not see the text " successfully assigned"
    And I should see the text "2 students unsuccessfully assigned to Class Dinosaur Roaring-c-12345."
    And I should see the text "2 students unsuccessfully assigned to Class Hair Braiding-d-12345."
    And I should not see the text "More Details"
    And I should be on "students/[org:XXD1S1]"

    # Run through again, mixed successes and failures.
    When I check the box for the row containing "IL1230001"
    And I check the box for the row containing "IL1230002"
    And I check the box for the row containing "IL1230003"
    And I select "Assign to Class" from "operation"
    And I press "Next"
    And I check the box for the row containing "Dinosaur Roaring"
    And I press "Next"
    And I follow meta refresh

    Then I should not see the text "Performed Assign to Class"
    And I should see the text "1 student successfully assigned to Class Dinosaur Roaring-c-12345."
    And I should see the text "2 students unsuccessfully assigned to Class Dinosaur Roaring-c-12345."
    And I should see the link "More Details"
    And I should see the text "Unable to assign Louis Skolnick to Class Dinosaur Roaring-c-12345."
    And I should see the text "Unable to assign Gilbert Lowell to Class Dinosaur Roaring-c-12345."
    And I should not see the text "Unable to assign Carlton Summers"

  @javascript @ads-1246
  Scenario: Student Class Assignment VBO "More Details" functionality
    Given LDR students:
      | firstName | lastName | enrollOrgId  | dateOfBirth | gender | gradeLevel | stateIdentifier | classes |
      | Carlton   | Summers  | [org:XXD1S1] | 2005-01-01  | M      | 3          | IL1230003       |         |
    And students "[student:Louis Skolnick],[student:Gilbert Lowell]" are assigned to "[class:dinroa]"
    And I am logged in as "Sir Patrick Stewart"

    When I am at "students/[org:XXD1S1]"
    And I check the box for the row containing "IL1230001"
    And I check the box for the row containing "IL1230002"
    And I check the box for the row containing "IL1230003"
    And I select "Assign to Class" from "operation"
    And I press "Next"
    And I check the box for the row containing "Dinosaur Roaring"
    And I press "Next"
    And I wait for AJAX to finish

    # More Details collapsed.
    Then I should see an ".more-list .element-invisible" element

    # More Details expanded.
    When I click "More Details"
    Then I should not see an ".more-list .element-invisible" element

  @javascript @ads-1623
  Scenario: Select students across multiple pages within the datatable
    Given LDR students:
      | firstName | lastName | enrollOrgId  | dateOfBirth | gender | gradeLevel | stateIdentifier | classes |
      | Lavelle   | Lage     | [org:XXD1S1] | 2005-01-29  | F      | 3          | IL1230003       |         |
      | Cherie    | Bollman  | [org:XXD1S1] | 2005-02-28  | F      | 3          | IL1230004       |         |
      | Jeffry    | Clore    | [org:XXD1S1] | 2005-03-27  | M      | 3          | IL1230005       |         |
      | Chantel   | Schacher | [org:XXD1S1] | 2005-04-26  | F      | 3          | IL1230006       |         |
      | Barton    | Sagar    | [org:XXD1S1] | 2005-05-25  | M      | 3          | IL1230007       |         |
      | Soila     | Lombardi | [org:XXD1S1] | 2005-06-24  | F      | 3          | IL1230008       |         |
      | Teisha    | Berning  | [org:XXD1S1] | 2005-07-23  | F      | 3          | IL1230009       |         |
      | Mi        | Yearout  | [org:XXD1S1] | 2005-08-22  | F      | 3          | IL1230010       |         |
      | Ervin     | Ralston  | [org:XXD1S1] | 2005-09-21  | M      | 3          | IL1230011       |         |
      | Olen      | Rohrbach | [org:XXD1S1] | 2005-10-20  | M      | 3          | IL1230012       |         |
    And I am logged in as "Sir Patrick Stewart"

    When I am at "students/[org:XXD1S1]"
    And I select "Assign to Class" from "operation"
    And I check the box for the row containing "IL1230001"
    And I check the box for the row containing "IL1230002"
    And I check the box for the row containing "IL1230003"
    And I click the ".paginate_button.next" element
    And I check the box for the row containing "IL1230011"
    And I check the box for the row containing "IL1230012"
    And I press "Next"

    Then I should see "Louis Skolnick"
    And I should see "Gilbert Lowell"
    And I should see "Lavelle Lage"
    And I should see "Ervin Ralston"
    And I should see "Olen Rohrbach"

  @javascript @ads-1268
  Scenario: Check all functionality in VBO/datatable
    Given LDR students:
      | firstName | lastName | enrollOrgId  | dateOfBirth | gender | gradeLevel | stateIdentifier | classes |
      | Lavelle   | Lage     | [org:XXD1S1] | 2005-01-29  | F      | 3          | IL1230003       |         |
      | Cherie    | Bollman  | [org:XXD1S1] | 2005-02-28  | F      | 3          | IL1230004       |         |
      | Jeffry    | Clore    | [org:XXD1S1] | 2005-03-27  | M      | 3          | IL1230005       |         |
      | Chantel   | Schacher | [org:XXD1S1] | 2005-04-26  | F      | 3          | IL1230006       |         |
      | Barton    | Sagar    | [org:XXD1S1] | 2005-05-25  | M      | 3          | IL1230007       |         |
      | Soila     | Lombardi | [org:XXD1S1] | 2005-06-24  | F      | 3          | IL1230008       |         |
      | Teisha    | Berning  | [org:XXD1S1] | 2005-07-23  | F      | 3          | IL1230009       |         |
      | Mi        | Yearout  | [org:XXD1S1] | 2005-08-22  | F      | 3          | IL1230010       |         |
      | Ervin     | Ralston  | [org:XXD1S1] | 2005-09-21  | M      | 3          | IL1230011       |         |
      | Olen      | Rohrbach | [org:XXD1S1] | 2005-10-20  | M      | 3          | IL1230012       |         |
    And I am logged in as "Sir Patrick Stewart"

    When I am at "students/[org:XXD1S1]"
    Then I should not see "Selected 10 rows in this page"

    When I click the "input.vbo-table-select-all" element
    Then I should see "Selected 10 rows in this page"

    When I select "Assign to Class" from "operation"
    And I press "Next"

    Then I should see "Louis Skolnick"
    And I should see "Gilbert Lowell"
    And I should see "Lavelle Lage"
    And I should not see "Ervin Ralston"
    And I should not see "Olen Rohrbach"

    When I am at "students/[org:XXD1S1]"
    And I click the "input.vbo-table-select-all" element
    And I click the ".views-table-row-select-all button" element
    And I select "Assign to Class" from "operation"
    And I press "Next"

    Then I should see "Louis Skolnick"
    And I should see "Gilbert Lowell"
    And I should see "Lavelle Lage"
    And I should see "Ervin Ralston"
    And I should see "Olen Rohrbach"
