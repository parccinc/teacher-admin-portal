@api @user @sso @login
Feature: ADS-342 Provide local TAP login for Drupal admins
  As a Drupal administrator to the TAP system,
  I want to a local login
  so that I can administer TAP without SSO features

  Scenario: 1 Local log in form.
    # We're not explicitly calling this step because that tries to log us out.
    # Logging us out will take us to SSO login, which we aren't certified for.
    # Given I am not logged in
    When I go to "user/local-login"
    # No redirect means we're in the right place
    Then the url should match "user/local-login"
    # I should see the Drupal login page
    And I should see the text "User account"
    And I should see the text "E-mail or username"
    And I should see the text "Password"
    # I should not see the SSO login page
    And I should not see the link "Create new account"
    And I should not see the link "Forgot Password?"

  Scenario: 2: Logging in with local log in form.
    Given users:
      | name           | pass  | mail                 | roles         |
      | behat_ssoadmin | behat | ssoadmin@example.com | administrator |
    #    And I am not a user in the SSO identity stores
    #    And I am not logged in
    When I go to "user/local-login"
    #  and input my credentials and press "Log in"
    And I fill in "E-mail or username" with "behat_ssoadmin"
    And I fill in "Password" with "behat"
    And I press "Log in"
    #    Then I should be logged in
    Then the url should match "dashboard"

  Scenario: 3: Accessing local log in form as authenticated user.
    Given users:
      | name            | pass  | mail                  |
      | behat_parccuser | behat | parccuser@example.com |
    When I go to "user/local-login"
    And I fill in "E-mail or username" with "behat_parccuser"
    And I fill in "Password" with "behat"
    And I press "Log in"
    And I go to "user/local-login"
    # Then I should be on the "View profile" page
    # It doesn't redirect, so check the contents of the page
    Then I should see "behat_parccuser"
    And I should see "History"
    And I should see "Member for"

  @ads-810
  Scenario: I see a message after I reset my password
    Given users:
      | name                      | pass  | mail                                  | roles              |
      | behat_ssoteach[timestamp] | behat | behat_ssoteach[timestamp]@example.com | test administrator |

    And I am on the homepage
    And I click "Forgot Password"
    When I fill in "name" with "behat_ssoteach[timestamp]"
    #Wait for honeypot
    And I sleep 5 seconds
    And I press "E-mail new password"
    Then I should see the success message "Password reset instructions have been sent to the email address on file"
