@api @standards
Feature: Load Comp Domain, Cluster & Evidence Statements into TAP for Immediate Score Reports

  @parads-4526
  Scenario: Load Math Comp Domain, Cluster & Evidence Statements into TAP for Immediate Score Reports

    Given the standards have been imported
    And I am not logged in

    When I am viewing the standard "4.NF.4c"
    Then I should see the text "Evidence Statement Text:"
    And I should see the text "Apply and extend previous understandings of multiplication to multiply a fraction by a whole number."

    When I am viewing the standard "2.OA.1-1"
    Then I should see the text "Evidence Statement Text:"
    And I should see the text "Use addition and subtraction within 100 to solve one-step word problems involving situations of adding to, taking from, putting together, taking apart, and comparing, with unknowns in all positions, e.g., by using drawings and equations with a symbol for the unknown number to represent the problem."
