@api
Feature: Scenarios for menu icons

  Background:
    Given LDR organizations:
      | name   | type     | identifier | parent     |
      | XXD1   | district | 1          | XX         |
      | XXD1S1 | school   | 2          | [org:XXD1] |
      | XXD1S2 | school   | 3          | [org:XXD1] |
    And LDR class orgs:
      | name     | section | org          | grade | ref      |
      | Math     | 101     | [org:XXD1S1] | 3     | Math     |
      | English  | 101     | [org:XXD1S1] | 7     | English  |
      | PE       | 101     | [org:XXD1S2] | 7     | PE       |
      | Homeroom | 101     | [org:XXD1S2] | 7     | Homeroom |
    And LDR students:
      | firstName | lastName | enrollOrgId  | dateOfBirth | gender | gradeLevel | stateIdentifier | classes                      |
      | Louis     | Skolnick | [org:XXD1S1] | 2001-02-03  | M      | 3          | XXD1A001        | [class:Math]                 |
      | Lowell    | Gilbert  | [org:XXD1S1] | 2004-05-06  | M      | 3          | XXD1A002        | [class:Math]                 |
      | Sloth     | Fratelli | [org:XXD1S1] | 2001-02-03  | M      | 3          | XXD1A003        | [class:Math],[class:English] |
      | Mikey     | Astin    | [org:XXD1S1] | 2004-05-06  | M      | 3          | XXD1A004        | [class:English]              |
      | Mouth     | Feldman  | [org:XXD1S1] | 2004-05-06  | M      | 3          | XXD1A005        | [class:English]              |

    And TAP users:
      | name            | pass  | mail                  | roles                      | organizations             |
      | testadmin       | behat | testadmin@example.com | test administrator         | [org:XXD1S1],[org:XXD1S2] |
      | school_orgadmin | behat | sorgadmin@example.com | organization administrator | [org:XXD1S1]              |

    And LDR tests:
      | name                                | subject | grade | program               | security | permissions    | scoring   | description                 |
      | ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3  | ELA     | 8     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Test administered for Behat |
      | Math_Comp - MC.8.F.B.3 - MC.8.F.B.3 | Math    | 8     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Test administered for Behat |
    And LDR test assignments:
      | student                  | test                                       | status    | ref |
      | [student:Louis Skolnick] | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Lowell Gilbert] | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Sloth Fratelli] | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Sloth Fratelli] | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Mikey Astin]    | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Mouth Feldman]  | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |

  @ads-1152
  Scenario: Left nav for school org admin
    Given I am logged in as "school_orgadmin"
    When I am on the homepage
    Then I should see 6 "ul#main-menu.main-nav.left li" elements
    And the left nav contains the links:
      | label    | href                   |
      | Students | /students/[org:XXD1S1] |
      | Classes  | /classes/[org:XXD1S1]  |
      | Users    | /tapusers/[org:XXD1S1] |
    And the "ul#main-menu.main-nav.left" element should not contain "Tests"

  @ads-1236
  Scenario Outline: org admins should not see tests link
    Given I am logged in as a user with the "organization administrator" role
    And I am assigned the organization "<org>"
    When I am on the homepage
    Then the "ul#main-menu.main-nav.left" element should not contain "Tests"
    Examples:
      | org          |
      | [org:XX]     |
      | [org:XXD1]   |
      | [org:XXD1S1] |

  @ads-1238
  Scenario Outline: school district org admin goes to right classes page
    Given I am logged in as a user with the "organization administrator" role
    And I am assigned the organization "<org>"
    When I am on the homepage
    Then the left nav contains the links:
      | label   | href                  |
      | Classes | /classes/<school org> |

    Examples:
      | org          | school org   |
      | [org:XXD1]   | [org:XXD1S1] |
      | [org:XXD1S1] | [org:XXD1S1] |

  @ads-1242 @ads-1477
  Scenario Outline: There can be only one.
    Given I am logged in as a user with the "<role>" role
    And I am assigned the organization "<org>"
    When I am on the homepage
    Then the "ul#main-menu.main-nav.left" element should contain "<level>"
    And the "ul#main-menu.main-nav.left" element should not contain "<this>"
    And the "ul#main-menu.main-nav.left" element should not contain "<or that>"
    Examples:
      | role                       | org        | level     | this      | or that   |
      | organization administrator | [org:XX]   | Districts | States    | Schools   |
      | organization administrator | [org:XXD1] | Schools   | States    | Districts |
      | ads administrator          | 1          | States    | Districts | Schools   |

  @ads-1250
  Scenario: What does our test admin menu look like
    Given I am logged in as "testadmin"
    When I am on the homepage
    Then I should see 6 "ul#main-menu.main-nav.left li" elements
    And the left nav contains the links:
      | label    | href                         |
      | Students | /students/[org:XXD1S1]       |
      | Tests    | /manage/[org:XXD1S1]/tests   |
      | Classes  | /manage/[org:XXD1S1]/classes |

    When I click "Students"
    Then I should be on "manage/[org:XXD1S1]/students"

    When I am on the homepage
    And I select "XXD1S2" from "Organization"
    And I press "Select"
    Then the left nav contains the links:
      | label    | href                         |
      | Students | /students/[org:XXD1S2]       |
      | Tests    | /manage/[org:XXD1S2]/tests   |
      | Classes  | /manage/[org:XXD1S2]/classes |

    When I click "Students"
    Then I should be on "manage/[org:XXD1S2]/students"

  @dmrs
  Scenario: Menu items for dmrs administrator
    Given I am logged in as a user with the "dmrs administrator" role
    When I am on the homepage
    Then I should see 5 "ul#main-menu.main-nav.left li" elements
    And the left nav contains the links:
      | label             | href              |
      | Upload DMRS Users | /dmrsusers/import |
      | Admin DMRS Users  | /dmrsusers/admin  |

  @dmrs
  Scenario: Menu items for dmrs user
    Given I am logged in as a user with the "dmrs user" role
    When I am on the homepage
    Then I should see 4 "ul#main-menu.main-nav.left li" elements
    And the left nav contains the links:
      | label | href               |
      | DMRS  | /dmrsusers/amplify |

