@api @students @tests
Feature: Student test features

  Background:
    Given LDR organizations:
      | name   | type     | identifier | parent     |
      | XXD1   | district | 1          | XX         |
      | XXD1S1 | school   | 2          | [org:XXD1] |
      | XXD1S2 | school   | 3          | [org:XXD1] |
    And TAP users:
      | name          | pass  | mail                | roles              | organizations |
      | behat_tapuser | behat | tapuser@example.com | test administrator | [org:XXD1S1]  |
    And LDR class orgs:
      | name    | section  | org          | grade | ref     |
      | Math    | math1    | [org:XXD1S1] | 3     | math    |
      | English | english1 | [org:XXD1S1] | 3     | english |
    And LDR students:
      | firstName | lastName | enrollOrgId  | dateOfBirth | gender | gradeLevel | stateIdentifier | classes         |
      | Louis     | Skolnick | [org:XXD1S1] | 2005-01-01  | M      | 3          | XXD1A001        | [class:math]    |
      | Gilbert   | Lowell   | [org:XXD1S1] | 2005-01-01  | M      | 3          | XXD1A002        | [class:english] |
      | Jeff      | Spicoli  | [org:XXD1S1] | 1980-01-01  | M      | 7          | XXD1A003        | [class:english] |
    And LDR tests:
      | name         | subject | grade | program               | security | permissions    | scoring   | description                 |
      | Behat test   | ELA     | 3     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Test administered for Behat |
      | Behat test 2 | ELA     | 5     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Test administered for Behat |

  @parads-3770
  Scenario: Assign Test link on my students page
    Given I am logged in as "behat_tapuser"
    And I am assigned classes "[class:math],[class:english]"
    When I am at "manage/[org:XXD1S1]/students"
    Then I should see the header "Operations" in column "6" of "datatable-1"
    And I should see "Assign Test" in the "Skolnick" row
    And I should see "Assign Test" in the "Lowell" row
    And I should see the link "Assign Test"

  @parads-3773 @javascript
  Scenario Outline: Test Administrator and Default Grade & Subjects
    Given I am logged in as "behat_tapuser"
    And I am assigned classes "[class:math],[class:english]"
    And I am at "manage/[org:<org>]/students"
    When I click "Assign Test" in the "<student>" row
    And I wait for AJAX to finish
    Then I should see the text "You are assigning a test to <full name>" in the "modal"
    And the "Class Identifier" field should contain "<class id>"
    And the "Section Number" field should contain "<class section>"
    And I should see an "Select Grade" form element
    And the "Select Grade" field should contain "<class grade>"
    And I should see the "Select Subject" test battery subjects for grade "<class grade>"

#Subject changes when grade changes
    When I select "<change grade>" from "Select Grade"
    And I wait for AJAX to finish
    Then I should see the "Select Subject" test battery subjects for grade "<change grade>"
    Examples:
      | org    | student  | full name      | class id | class section | class grade | change grade |
      | XXD1S1 | Skolnick | Louis Skolnick | Math     | math1         | 3           | 4            |
      | XXD1S1 | Gilbert  | Gilbert Lowell | English  | english1      | 3           | 8            |

  @parads-3774 @javascript
  Scenario: Test administrator selects test subject to assign after choosing grade and subject
    Given I am logged in as "behat_tapuser"
    And I am assigned classes "[class:math],[class:english]"
    And I am at "manage/[org:XXD1S1]/students"
    When I click "Assign Test" in the "Skolnick" row
    And I wait for AJAX to finish

    #Select grade and subject.
    When I select "ELA" from "Select Subject"
    And I wait for AJAX to finish

    Then the "Select Battery" form element should contain test-battery options from JSON args '{"testBatteryGrade":3,"testBatterySubject":"ELA"}'
    And I should not see an "Next" button form element

    #Assumption is Grade 3, Math, has option 'Diagnostic Assessment Math Concepts and Communication Diagnostic - Numeration'.
    When I select "Diagnostic Assessment Behat test" from "Select Battery"
    Then I should see the button "Next"

    #Now we change the grade.
    When I select "4" from "Select Grade"
    And I wait for AJAX to finish

    #Select battery is using element-invisible class which is not compatible
    #with the I should not see an :element form element step.
    #Then I should not see an "Select Battery" form element

    Then the "#battery-wrapper" element should have computed css value "0px" for "height"
    And I should not see an "Next" button form element

  @parads-3850 @test_assignment @javascript
  Scenario: Student test assignment increments test page class count
    #Adds two test assignments for english students
    Given LDR test assignments:
      | student                  | test              | status     |
      | [student:Gilbert Lowell] | [test:Behat test] | Scheduled  |
      | [student:Jeff Spicoli]   | [test:Behat test] | InProgress |

    And I am logged in as "behat_tapuser"
    And I am assigned classes "[class:math],[class:english]"
    And I am at "manage/[org:XXD1S1]/students"
    When I click "Assign Test" in the "Skolnick" row
    And I wait for AJAX to finish

  #Select test.
    And I select "ELA" from "Select Subject"
    And I wait for AJAX to finish
    And I select "Diagnostic Assessment Behat test" from "Select Battery"
    When I press "Next"
    And I wait for the batch job to finish
    And I wait for AJAX to finish
  #Wait longer for good luck.
    And I wait 2 seconds
    Then I should be on "manage/[org:XXD1S1]/tests/assignments/[class:math]"

  #Success message
    And I should see the success message "Behat test has been assigned to Louis Skolnick"
  #The tests tab is the 3rd one with Classes, Students
    And I should see the link "Classes"
    And I should see the link "Students"
    And I should see the link "Tests Overview"
    And I should see the link "Test Assignments"

    #Go to the test page to confirm correct summary test assignment counts per class.
    When I click "Tests Overview"
    Then I should see the text "Class: Math - math1 (1 student)"
    And I should see the text "Class: English - english1 (2 students)"

  #Math columns
    And the xpath '//*[@id="datatable-1"]/thead/tr[1]/th[1]' element should contain "Test Name"
    And the xpath '//*[@id="datatable-1"]/thead/tr[1]/th[2]' element should contain "Grade"
    And the xpath '//*[@id="datatable-1"]/thead/tr[1]/th[3]' element should contain "Subject"
    And the xpath '//*[@id="datatable-1"]/thead/tr[1]/th[4]' element should contain "Number of Assignments"

  #English columns
    And the xpath '//*[@id="datatable-2"]/thead/tr[1]/th[1]' element should contain "Test Name"
    And the xpath '//*[@id="datatable-2"]/thead/tr[1]/th[2]' element should contain "Grade"
    And the xpath '//*[@id="datatable-2"]/thead/tr[1]/th[3]' element should contain "Subject"
    And the xpath '//*[@id="datatable-2"]/thead/tr[1]/th[4]' element should contain "Number of Assignments"

  #English rows
    And the xpath '//*[@id="datatable-1"]/tbody/tr[1]/td[1]' element should contain "Behat test"
    And the xpath '//*[@id="datatable-1"]/tbody/tr[1]/td[2]' element should contain "3"
    And the xpath '//*[@id="datatable-1"]/tbody/tr[1]/td[3]' element should contain "ELA"
    And the xpath '//*[@id="datatable-1"]/tbody/tr[1]/td[4]' element should contain "2"

  #Math rows
    And the xpath '//*[@id="datatable-2"]/tbody/tr[1]/td[1]' element should contain "Behat test"
    And the xpath '//*[@id="datatable-2"]/tbody/tr[1]/td[2]' element should contain "3"
    And the xpath '//*[@id="datatable-2"]/tbody/tr[1]/td[3]' element should contain "ELA"
    And the xpath '//*[@id="datatable-2"]/tbody/tr[1]/td[4]' element should contain "1"

  @parads-4126 @test_assignment @javascript
  Scenario: As a Test Administrator, I want to be able to indicate whether a test assignment is occurring pre- or post-instruction in order to track student progress toward standards.
    Given I am logged in as "behat_tapuser"
    And I am assigned classes "[class:math],[class:english]"
    And I am at "manage/[org:XXD1S1]/students"
    When I click "Assign Test" in the "Skolnick" row
    And I wait for AJAX to finish
    Then I select "Pre-Instruction" from "Select Timing"
    And I select "Post-Instruction" from "Select Timing"

  @parads-4082 @test_assignment @javascript
  Scenario: Duplicate student test assignments display a user friendly message
    Given LDR test assignments:
      | student                  | test              | status    |
      | [student:Gilbert Lowell] | [test:Behat test] | Scheduled |
      | [student:Jeff Spicoli]   | [test:Behat test] | Scheduled |

    Given I am logged in as "behat_tapuser"
    And I am assigned classes "[class:english]"
    And I am at "manage/[org:XXD1S1]/students"

    When I click "Assign Test" in the "Lowell" row
    And I wait for AJAX to finish
    And I select "ELA" from "Select Subject"
    And I wait for AJAX to finish
    And I select "Diagnostic Assessment Behat test" from "Select Battery"

    When I press "Next"
    And I wait for the batch job to finish
    And I wait for AJAX to finish
    And I wait 2 seconds

    Then I should be on "manage/[org:XXD1S1]/tests/assignments/[class:english]"
    And I should see the error message "Behat test has already been assigned to Gilbert Lowell."

    #Start the Classes scenario
    When I click "Classes"
    And I am at "manage/[org:XXD1S1]/classes"
    When I click "Assign Test" in the "English" row
    And I wait for AJAX to finish
    And I select "ELA" from "Select Subject"
    And I wait for AJAX to finish
    And I select "Diagnostic Assessment Behat test" from "Select Battery"
    When I press "Next"
    And I wait for the batch job to finish
    And I wait for AJAX to finish
    And I wait 2 seconds
    Then I should be on "manage/[org:XXD1S1]/classes"
    And I should see the error message "Behat test has already been assigned to the class."

  @parads-4034 @test_assignment @javascript
  Scenario: As a Test Administrator, I want to be able to indicate whether a test assignment for a student needs PNP.
    Given I am logged in as "behat_tapuser"
    And I am assigned classes "[class:math],[class:english]"
    And I am at "manage/[org:XXD1S1]/students"
    When I click "Assign Test" in the "Skolnick" row
    And I wait for AJAX to finish
    Then I check the box "Text to Speech"
    And I check the box "Line Reader"
