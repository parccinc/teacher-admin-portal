@api @dashboard
Feature: Home dashboard by role

  Background:
    Given LDR organizations:
      | name          | type     | identifier | parent              |
      | XX District 1 | district | 1          | XX                  |
      | School XX1a   | school   | 2          | [org:XX District 1] |
      | School XX1b   | school   | 3          | [org:XX District 1] |
      | XX District 2 | district | 4          | XX                  |
      | School XX2a   | school   | 5          | [org:XX District 2] |
    And TAP users:
      | name                | pass  | mail                          | roles                      | organizations       |
      | John Kimball        | behat | itsnotatumor@sd2.org          | test administrator         | [org:School XX1a]   |
      | Ben Stein           | behat | bstein@sd2.org                | test administrator         | [org:School XX1a]   |
      | Dolores Umbridge    | behat | dumb@sd2.org                  | test administrator         | [org:School XX1a]   |
      | Pai Mei             | behat | pmei@sd2.org                  | test administrator         | [org:School XX1a]   |
      | Sir Patrick Stewart | behat | makeitso@engage.net           | organization administrator | [org:XX]            |
      | orgadmin_district   | behat | orgadmin_district@example.com | organization administrator | [org:XX District 1] |
      | orgadmin_school     | behat | orgadmin_school@example.com   | organization administrator | [org:School XX1a]   |
    And LDR class orgs:
      | name    | section  | org               | grade | ref     |
      | Math    | math1    | [org:School XX1a] | 3     | Math    |
      | English | english1 | [org:School XX1a] | 7     | English |
      | Science | sci1     | [org:School XX1a] | 7     | Science |
    And LDR students:
      | firstName | lastName | enrollOrgId       | dateOfBirth | gender | gradeLevel | stateIdentifier | classes                      |
      | Louis     | Skolnick | [org:School XX1a] | 2001-02-03  | M      | 3          | XXD1A001        | [class:Math],[class:English] |
      | Gilbert   | Lowell   | [org:School XX1a] | 2004-05-06  | M      | 3          | XXD1A002        | [class:English]              |
      | Wyatt     | Donnelly | [org:School XX1a] | 2004-05-06  | M      | 3          | XXD1A002        | [class:Science]              |

  @parads-3727 @as
  Scenario: PARADS-2737 When user logs in take them to dashboard
    Given I am an anonymous user
    And I am on the homepage
    When I fill in "username" with "Sir Patrick Stewart"
    And I fill in "pass" with "behat"
    And I press "Log in"
    Then I should be on the Drupal homepage
    And I should see the "Sir Patrick Stewart" heading in the "page title" region

  Scenario: Anonymous user accessing dashboard get login
    Given I am an anonymous user
    When I am on the homepage
    Then I should see the "Log in" button

  @batch
    Scenario: Authenticated user sees file list view page.
    Given I am logged in as a user with the "batch user" role
    When I am on the homepage
    Then I should see the " + Upload batch" button
    And I should see an "table" element

  Scenario: TAP user sees dashboard block on home page
    Given I am logged in as "Sir Patrick Stewart"
    When I am on the homepage
    Then I should see "Users" in the "div.users .card-title h6" element
    And I should see the link "Import Users"

  @parads-4513 @help @as
  Scenario: TAP user sees help dashboard help box on home page
    Given the help content has been imported
    And I am logged in as "Sir Patrick Stewart"
    When I am on the homepage
    Then I should see "Learn more about PARCC Diagnostics"
    #This is content that changes
    And I should see "Math Fluency"
    And I should see "Reading Comprehension"
    #And I should see "Math Progression"
    And I should see "ELA Vocabulary"

  @parads-4513 @javascript @help @as
  Scenario: TAP user sees help dashboard help box and clicks on Learn More Link
    Given the help content has been imported
    And I am logged in as "Sir Patrick Stewart"
    When I am on the homepage
    And I click "Learn More" in the "div.Math-Fluency-Overview" element
    And I wait 2 seconds
    # Need to fix the link but as long as the next step is passing then this,
    # is technically working but I like the check of being on the right link.
    # Then I should be on "help#panel-Node-Title"
    And the "li.active div.active" element should contain "Math Fluency"
    When I am on the homepage
    And I click "Learn More" in the "div.Reading-Comprehension-Overview" element
    And I wait 2 seconds
    # Need to fix the link but as long as the next step is passing then this,
    # is technically working but I like the check of being on the right link.
    # Then I should be on "help#panel-Node-Title"
    And the "li.active div.active" element should contain "Reading Comprehension"
    When I am on the homepage
    And I click "Learn More" in the "div.ELA-Vocabulary-Overview" element
    And I wait 2 seconds
    # Need to fix the link but as long as the next step is passing then this,
    # is technically working but I like the check of being on the right link.
    # Then I should be on "help#panel-Node-Title"
    And the "li.active div.active" element should contain "ELA Vocabulary"

  @parads-2431
  Scenario: PARADS-2431 State Org Admin Role - Districts and Users
    Given I am logged in as "Sir Patrick Stewart"
    When I am on the homepage
    Then I should see a "Districts" block for "[org:XX]" with clickable count and actions "Import Districts"
    And I should see a "Users" block for "[org:School XX1a]" with clickable count and actions "Import Users"

  @parads-2431
  Scenario: PARADS-2431 Clicking on count links takes me to expected page
    Given I am logged in as "Sir Patrick Stewart"
    And I am on the homepage

    # Organization link.
    When I click "org-[org:XX]-count"
    Then I should be on "organizations/[org:XX]"

    # User link.
    When I am on the homepage
    And I click "user-[org:XX]-count"
    Then I should be on "organizations/[org:XX]"
    And I should see the warning message containing "Please click a user count link from an Organization at the school level."

  @parads-2431
  Scenario: PARADS-2431 Org Admin clicks on Add buttons links.
    Given I am logged in as "Sir Patrick Stewart"
    And I am on the homepage

    When I click "Import Districts"
    Then I should be on "organizations/import"

    When I am on the homepage
    And I click "Import Users"
    Then I should be on "tapusers/import"

  @parads-2805
  Scenario: PARADAS-2805: District Org Admin Role - Schools and Users
    Given I am logged in as "orgadmin_district"
    When I am on the homepage
    Then I should see "Schools" in the "div.schools .card-title h6" element
    And I should see the link "org-[org:XX District 1]-count"
    And I should see the link "Import Schools"
    And I should see "Users" in the "div.users .card-title h6" element
    And I should see the link "user-[org:XX District 1]-count"
    And I should see the link "Import Users"
    # They actually should see this now for AS, and they need a link to,
    # Import Classes also b/c there is no "Upload Classes" in the menu all day.
    # And I should not see an "div.classes .card-title h6" element
    # And I should not see the link "Import Classes"
    And I should see "Students" in the "div.students .card-title h6" element
    And I should see the link "Import Students"

  @parads-2805
  Scenario: PARADAS-2805: Org Admin clicks on count links
    Given I am logged in as "orgadmin_district"
    And I am on the homepage

    # Schools Link.
    When I click "org-[org:XX District 1]-count"
    Then I should be on "organizations/[org:XX District 1]"

    # Users Link.
    When I am on the homepage
    And I click "user-[org:XX District 1]-count"
    Then I should be on "organizations/[org:XX District 1]"
    And I should see the warning message containing "Please click a user count link from an Organization at the school level."

  @parads 2805
  Scenario: PARADAS-2805: Org Admin clicks on Add buttons links
    Given I am logged in as a user with the "organization administrator" role
    And I am assigned the organization "[org:XX District 1]"
    And I am on the homepage
    When I click "Import Schools"
    Then I should be on "organizations/import"

    When I am on the homepage
    And I click "Import Users"
    Then I should be on "tapusers/import"

  @parads-2806
  Scenario: Dashboard for school level users
    Given I am logged in as "orgadmin_school"
    When I am on the homepage

    # I see the user block.
    Then I should see "Users" in the "div.users .card-title h6" element
    And I should see the link "user-[org:School XX1a]-count"
    And I should see the link "Import Users"

    # I see the student block.
    When I am on the homepage
    Then I should see "Students" in the "div.students .card-title h6" element
    And I should see the link "students-[org:School XX1a]-count"
    And I should see the link "Import Students"

    # I see the classes block.
    When I am on the homepage
    And I should see "Classes" in the "div.classes .card-title h6" element
    And I should see an "#classes-[org:School XX1a]-count" element
    And I should see the link "Import Classes"

  @parads-2806
  Scenario: PARADS-2806, Scenario 2 Test admin dashboard at school level
    Given I am logged in as "John Kimball"
    When I am on the homepage
    Then I should see "Students" in the "div.students .card-title h6" element
    And I should see the link "students-[org:School XX1a]-count"
    # Add Student temporarily removed until class dropdown is added
    #And I should see the link "Add Student"
    # Test admin sees classes block without an Import Classes button.
    And I should not see the link "#classes-[org:School XX1a]-count"
    And I should not see "Import Classes"

  @parads-2806
  Scenario: PARADS-2806, Scenario 3 Org Admin clicks on count links
    Given I am logged in as "orgadmin_school"
    And I am on the homepage

    And I click "user-[org:School XX1a]-count"
    Then I should be on "tapusers/[org:School XX1a]"

    When I am on the homepage
    And I click "students-[org:School XX1a]-count"
    Then I should be on "students/[org:School XX1a]"

    When I am on the homepage
    And I click "Import Classes"
    Then I should be on "class/import"

  @parads-2806
  Scenario: PARADS-2806, Scenario 4 test admin clicks on stuff
    Given I am logged in as "John Kimball"
    And I am on the homepage
    When I click "students-[org:School XX1a]-count"
    Then I should be on "manage/[org:School XX1a]/students"

  @parads-3742 @parads-5207 @dashboard_counts
  Scenario: Test admin student count should be only what is assigned to their classes
    Given I am logged in as "John Kimball"
    And I am assigned classes "[class:Math],[class:English]"
    When I am on the homepage
    Then I should see "2" in the "#students-[org:School XX1a]-count" element

    When I am logged in as "orgadmin_school"
    And I am on the homepage
    Then I should see "3" in the "#students-[org:School XX1a]-count" element

  @parads-5096 @javascript @help
  Scenario: Homebox functionality
    Given the help content has been imported
    And I maximize the window
    And I am logged in as "John Kimball"
    And I am assigned the classes "Math" from Organization "[org:School XX1a]"
    And I assign the students "XXD1A001" to "Math"
    When I am on the homepage

    # Default expanded.
    Then I should see homebox "Dashboard" expanded
    And I should see homebox "Learn more" expanded

    # Collapse "Dashboard" portlet, navigate away and back verify settings were
    # saved.
    When I collapse homebox "Dashboard"
    And I wait for AJAX to finish
    And I click "Classes"
    And I click "Dashboard"
    Then I should see homebox "Dashboard" collapsed
    And I should see homebox "Learn more" expanded

    # Collapse "Learn more" portlet, navigate away and back verify settings were
    # saved.
    When I expand homebox "Dashboard"
    And I wait for AJAX to finish
    And I collapse homebox "Learn more"
    And I wait for AJAX to finish
    And I click "Classes"
    And I click "Dashboard"
    Then I should see homebox "Dashboard" expanded
    And I should see homebox "Learn more" collapsed

    # Log out, and back in, verify settings were saved.
    When I am an anonymous user
    And I am logged in as "John Kimball"
    And I am on the homepage
    Then I should see homebox "Dashboard" expanded
    And I should see homebox "Learn more" collapsed

    # Adjust ordering.
    When I expand homebox "Learn more"
    And I drag homebox "Dashboard" to "Learn more"
    Then homebox "Learn more" should appear above homebox "Dashboard"

    # Navigate away and back verify settings were saved.
    When I click "Classes"
    And I click "Dashboard"
    Then homebox "Learn more" should appear above homebox "Dashboard"

    # Log out, and back in, verify settings were saved.
    When I am an anonymous user
    And I am logged in as "John Kimball"
    And I am on the homepage
    Then homebox "Learn more" should appear above homebox "Dashboard"

    # Collapse the portlets, ensure they can be re-ordered.
    When I collapse homebox "Dashboard"
    And I drag homebox "Dashboard" to "Learn more"
    Then homebox "Dashboard" should appear above homebox "Learn more"

    # Attempt to drop in invalid positions.
    When I drag homebox "Dashboard" to 0, 0
    Then homebox "Dashboard" should appear above homebox "Learn more"

    When I drag homebox "Dashboard" to 2000, 2000
    Then homebox "Dashboard" should appear above homebox "Learn more"

    When I drag homebox "Learn more" to 0, 0
    Then homebox "Dashboard" should appear above homebox "Learn more"

    When I drag homebox "Learn more" to 2000, 2000
    Then homebox "Dashboard" should appear above homebox "Learn more"

  @parads-4519 @as @javascript @help
  Scenario: Added Homebox tootltip functionality
    Given the help content has been imported
    And I am logged in as "John Kimball"
    And I am assigned the classes "Math" from Organization "[org:School XX1a]"
    And I assign the students "XXD1A001" to "Math"
    When I am on the homepage
    When I hover over "span#panels_mini_dashboard_cards.dashboard-help" element "body span.tooltip" is visible
    When I hover over "span#views_pads_dashboard_help_block-block.dashboard-help" element "body span.tooltip" is visible

  @ads-664
  Scenario: No ISBE button on PARCC for school level orgadmin
    Given I am logged in as "orgadmin_school"
    And I am on the homepage
    Then I should not see the link "ISBE Management"

  @ads-1192
  Scenario: Remove add single student button on dashboard
    Given I am logged in as "Pai Mei"
    When I am on the homepage
    Then I should not see the link "Add Student"
