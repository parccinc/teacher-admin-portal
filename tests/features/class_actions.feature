@api @classes @class_actions @bulk_operations @tapusers
Feature: Actions or operations that can be performed for classes.

  Background:
    Given LDR organizations:
      | name          | type     | identifier | parent              |
      | XX District 1 | district | 1          | XX                  |
      | School XX1a   | school   | 2          | [org:XX District 1] |

    And TAP users:
      | name                | pass  | mail                      | roles                      | organizations       |
      | John Kimball        | behat | itsnotatumor@1234.sd2.org | test administrator         | [org:School XX1a]   |
      | Ben Stein           | behat | bstein@1234.sd2.org       | test administrator         | [org:School XX1a]   |
      | Dolores Umbridge    | behat | dumb@1234.sd2.org         | test administrator         | [org:School XX1a]   |
      | Pai Mei             | behat | pmei@1234.sd2.org         | test administrator         | [org:School XX1a]   |
      | Sir Patrick Stewart | behat | makeitso@engage.net       | organization administrator | [org:XX District 1] |

    And LDR class orgs:
      | name                 | section       | org               | grade | ref      |
      | Discrete Mathematics | a-[timestamp] | [org:School XX1a] | 2     | math     |
      | Integral Calculus    | b-[timestamp] | [org:School XX1a] | 2     | calculus |
      | Dinosaur Roaring     | c-[timestamp] | [org:School XX1a] | 1     | dinosaur |
      | Hair Braiding        | d-[timestamp] | [org:School XX1a] | 1     | braiding |

  @javascript @parads-2608
  Scenario: PARADS-2608 Org admin on user page can select users and choose assign classes action
    Given I am logged in as "Sir Patrick Stewart"
    And I am at "tapusers/[org:School XX1a]"
    And I check the box for the row containing "John Kimball"
    And I check the box for the row containing "Dolores Umbridge"
    When I select "Assign to Class" from "operation"
    And I press "Next"
    Then I should see text matching "John Kimball"
    And I should see text matching "Dolores Umbridge"
    And I should not see text matching "Ben Stein"
    And I should not see text matching "Pai Mei"

      #Class table should be there
    And I should see "Class name" in the "table.tableselect-classes thead tr" element
    And I should see "Section" in the "table.tableselect-classes thead tr" element
    And I should see "Grade 1" in the "table.tableselect-classes tbody tr[role='row']" element

    When I check the box for the row containing "Dinosaur Roaring"
      #And I check the box for the row containing "Hair Braiding"

    And I should see the button "Next"

    #We need a button, this is testing a Zurb specific link styled as button.
    And I see the "form#views-form-tap-user-actions-panel-pane-user-actions a" element with the "class" attribute set to "button small round" in the "content"

  @parads-2219
  Scenario: PARADS-2219 Org Admin selects users and then classes, but chickens out
    Given I am logged in as "Sir Patrick Stewart"
    And I am at "tapusers/[org:School XX1a]"
    And I check the box for the row containing "John Kimball"
    And I check the box for the row containing "Dolores Umbridge"
    And I select "Assign to Class" from "operation"
    And I press "Next"
    Then I should see text matching "John Kimball"
    And I should see text matching "Dolores Umbridge"
    When I check the box for the row containing "Dinosaur Roaring"
    And I check the box for the row containing "Hair Braiding"
    And I click "Cancel"
    Then I should be on "tapusers/[org:School XX1a]"

  @javascript @parads-2219 @parads-5259
  Scenario: Org Admin selects users and then classes, and submits!
    Given I am logged in as "Sir Patrick Stewart"
    And I am at "tapusers/[org:School XX1a]"
    And I check the box for the row containing "John Kimball"
    And I check the box for the row containing "Dolores Umbridge"
    And I select "Assign to Class" from "operation"
    And I press "Next"
    Then I should see text matching "John Kimball"
    And I should see text matching "Dolores Umbridge"
    When I check the box for the row containing "Dinosaur Roaring"
    And I check the box for the row containing "Hair Braiding"
    And I press "Next"
    And I wait for the batch job to finish
    And I wait for AJAX to finish
    Then I see the text "Performed Assign to Class on 2 items."
    And I should be on "tapusers/[org:School XX1a]"
    # Verify happy assignments.
    And user "John Kimball" should be assigned to classes "[class:dinosaur],[class:braiding]"
    And user "Dolores Umbridge" should be assigned to classes "[class:dinosaur],[class:braiding]"
    # Verify other insanity didn't occur.
    And user "John Kimball" should not be assigned to classes "[class:math],[class:calculus]"
    And user "Dolores Umbridge" should not be assigned to classes "[class:math],[class:calculus]"
    And user "Ben Stein" should not be assigned to classes "[class:math],[class:calculus],[class:dinosaur],[class:braiding]"
    And user "Pai Mei" should not be assigned to classes "[class:math],[class:calculus],[class:dinosaur],[class:braiding]"

  @parads-5301 @ads-1473
  Scenario: Admin can unassign a users classes
    # Test admin assigned to class does not see Administer classes.
    # ADS-1473, scenario 1.
    Given I am logged in as "John Kimball"
    And I am assigned classes "[class:math]"
    When I am at "user"
    Then I should not see the link "Administer classes"

    # Access for org admins changed in ADS-1473, removing coverage from this
    # scenario.

    #Administer classes link doesn't exist if user does not have classes assigned.
    When I am logged in as a user with the "administrator" role
    And I am on the edit page for user "Ben Stein"
    Then I should not see the link "Administer classes"

    #User with administrator role can unassign user classes
    When I am on the edit page for user "John Kimball"
    And I click "Administer classes"
    And I check the box for the row containing "Discrete Mathematics"
    And I press "Unassign Class"
    And I follow meta refresh
    Then I should see the message "1 class removed for John Kimball"
    #And I should be on the user view page for "John Kimball"
    And I should be on "users/john-kimball"

  @ads-1473
  Scenario Outline: Unassign users from classes
    Given TAP users:
      | name           | pass  | mail                 | roles                      | organizations       |
      | Root admin     | behat | root@example.com     | ads administrator          | 1                   |
      | State admin    | behat | state@example.com    | organization administrator | [org:XX]            |
      | District admin | behat | district@example.com | organization administrator | [org:XX District 1] |
      | School admin   | behat | school@example.com   | organization administrator | [org:School XX1a]   |

    When I am logged in as "<name>"

    # Scenario 5, no classes assigned, link not visible.
    And I am on the edit page for user "John Kimball"
    Then I should not see the link "Administer classes"

    # Scenario 3, classes assigned, link visible.
    When user "John Kimball" is assigned classes "[class:math]"
    And I am on the edit page for user "John Kimball"
    And I click "Administer classes"
    Then I should see the heading "Administer John Kimball's classes"
    And I should see "Class name"
    And I should see "Grade level"
    And I should see "Section number"
    And I should not see "LDR Class id"

    # Scenario 4, able to remove class assignments.
    When I check the box for the row containing "Discrete Mathematics"
    And I press "Unassign Class"
    And I follow meta refresh
    Then I should be on the user view page for "John Kimball"
    And I should see the success message "1 class removed for John Kimball"
    And I should not see the success message "Performed Unassign Class on 1 item."
    And I should not see the link "Administer classes"

    Examples:
      | name           |
      | Root admin     |
      | State admin    |
      | District admin |
      | School admin   |

  @javascript @ads-1268
  Scenario: VBO datatable select all functionality
    And TAP users:
      | name                | mail              | roles              | organizations     |
      | Ruthanne Bickers    | ta1@1234.sd2.org  | test administrator | [org:School XX1a] |
      | Muriel Vannatter    | ta2@1234.sd2.org  | test administrator | [org:School XX1a] |
      | Fleta Geiser        | ta3@1234.sd2.org  | test administrator | [org:School XX1a] |
      | Ellen Beaton        | ta4@1234.sd2.org  | test administrator | [org:School XX1a] |
      | Aldo Schow          | ta5@1234.sd2.org  | test administrator | [org:School XX1a] |
      | Claudio Harrow      | ta6@1234.sd2.org  | test administrator | [org:School XX1a] |
      | Gabriela Krzeminski | ta7@1234.sd2.org  | test administrator | [org:School XX1a] |
      | Antoine Eanes       | ta8@1234.sd2.org  | test administrator | [org:School XX1a] |
      | Sybil Handler       | ta9@1234.sd2.org  | test administrator | [org:School XX1a] |
      | Gale Tunney         | ta10@1234.sd2.org | test administrator | [org:School XX1a] |
      | Syreeta Classen     | ta11@1234.sd2.org | test administrator | [org:School XX1a] |
      | Joni Vacca          | ta12@1234.sd2.org | test administrator | [org:School XX1a] |
      | Chi Hoerr           | ta13@1234.sd2.org | test administrator | [org:School XX1a] |
      | Aja Okelly          | ta14@1234.sd2.org | test administrator | [org:School XX1a] |
      | Johnnie Aldrich     | ta15@1234.sd2.org | test administrator | [org:School XX1a] |
      | Georgie Penman      | ta16@1234.sd2.org | test administrator | [org:School XX1a] |
      | Inger Loveless      | ta17@1234.sd2.org | test administrator | [org:School XX1a] |
      | Vito Welty          | ta18@1234.sd2.org | test administrator | [org:School XX1a] |
      | Wanita Googe        | ta19@1234.sd2.org | test administrator | [org:School XX1a] |
      | Lashunda Sorber     | ta20@1234.sd2.org | test administrator | [org:School XX1a] |
      | Zackary Brenes      | ta21@1234.sd2.org | test administrator | [org:School XX1a] |
      | Bruna Curnutt       | ta22@1234.sd2.org | test administrator | [org:School XX1a] |
      | Ozell Muise         | ta23@1234.sd2.org | test administrator | [org:School XX1a] |
      | Inga Mirabella      | ta24@1234.sd2.org | test administrator | [org:School XX1a] |
      | Ute Roemer          | ta25@1234.sd2.org | test administrator | [org:School XX1a] |
      | Gia Presler         | ta26@1234.sd2.org | test administrator | [org:School XX1a] |
      | Karole Blaker       | ta27@1234.sd2.org | test administrator | [org:School XX1a] |
      | Sharri Steinbeck    | ta28@1234.sd2.org | test administrator | [org:School XX1a] |
      | Alyce Shipp         | ta29@1234.sd2.org | test administrator | [org:School XX1a] |
      | Iva Oda             | ta30@1234.sd2.org | test administrator | [org:School XX1a] |
    And I am logged in as "Sir Patrick Stewart"

    When I am at "tapusers/[org:School XX1a]"
    And I click the "input.vbo-table-select-all" element
    Then I should see "Selected 25 rows in this page"

    When I select "Assign to Class" from "operation"
    And I press "Next"
    Then I should see "Select classes to assign the following 25 users"

    When I am at "tapusers/[org:School XX1a]"
    And I click the "input.vbo-table-select-all" element
    And I click the ".views-table-row-select-all button" element
    And I select "Assign to Class" from "operation"
    And I press "Next"
    Then I should see "Select classes to assign the following 34 users"

  @javascript @ads-1268
  Scenario: VBO config form tableselect select all functionality
    Given TAP users:
      | name                | mail              | roles              | organizations     |
      | Ruthanne Bickers    | ta1@1234.sd2.org  | test administrator | [org:School XX1a] |
      | Muriel Vannatter    | ta2@1234.sd2.org  | test administrator | [org:School XX1a] |
      | Fleta Geiser        | ta3@1234.sd2.org  | test administrator | [org:School XX1a] |
      | Ellen Beaton        | ta4@1234.sd2.org  | test administrator | [org:School XX1a] |
      | Aldo Schow          | ta5@1234.sd2.org  | test administrator | [org:School XX1a] |
      | Claudio Harrow      | ta6@1234.sd2.org  | test administrator | [org:School XX1a] |
      | Gabriela Krzeminski | ta7@1234.sd2.org  | test administrator | [org:School XX1a] |
      | Antoine Eanes       | ta8@1234.sd2.org  | test administrator | [org:School XX1a] |
      | Sybil Handler       | ta9@1234.sd2.org  | test administrator | [org:School XX1a] |
      | Gale Tunney         | ta10@1234.sd2.org | test administrator | [org:School XX1a] |
      | Syreeta Classen     | ta11@1234.sd2.org | test administrator | [org:School XX1a] |
      | Joni Vacca          | ta12@1234.sd2.org | test administrator | [org:School XX1a] |
      | Chi Hoerr           | ta13@1234.sd2.org | test administrator | [org:School XX1a] |
      | Aja Okelly          | ta14@1234.sd2.org | test administrator | [org:School XX1a] |
      | Johnnie Aldrich     | ta15@1234.sd2.org | test administrator | [org:School XX1a] |
      | Georgie Penman      | ta16@1234.sd2.org | test administrator | [org:School XX1a] |
      | Inger Loveless      | ta17@1234.sd2.org | test administrator | [org:School XX1a] |
      | Vito Welty          | ta18@1234.sd2.org | test administrator | [org:School XX1a] |
      | Wanita Googe        | ta19@1234.sd2.org | test administrator | [org:School XX1a] |
      | Lashunda Sorber     | ta20@1234.sd2.org | test administrator | [org:School XX1a] |
      | Zackary Brenes      | ta21@1234.sd2.org | test administrator | [org:School XX1a] |
      | Bruna Curnutt       | ta22@1234.sd2.org | test administrator | [org:School XX1a] |
      | Ozell Muise         | ta23@1234.sd2.org | test administrator | [org:School XX1a] |
      | Inga Mirabella      | ta24@1234.sd2.org | test administrator | [org:School XX1a] |
      | Ute Roemer          | ta25@1234.sd2.org | test administrator | [org:School XX1a] |
      | Gia Presler         | ta26@1234.sd2.org | test administrator | [org:School XX1a] |
      | Karole Blaker       | ta27@1234.sd2.org | test administrator | [org:School XX1a] |
      | Sharri Steinbeck    | ta28@1234.sd2.org | test administrator | [org:School XX1a] |
      | Alyce Shipp         | ta29@1234.sd2.org | test administrator | [org:School XX1a] |
      | Iva Oda             | ta30@1234.sd2.org | test administrator | [org:School XX1a] |
    And LDR class orgs:
      | name                 | section        | org               | grade | ref         |
      | Discrete Mathematics | a-[timestamp]  | [org:School XX1a] | 2     | math        |
      | Integral Calculus    | b-[timestamp]  | [org:School XX1a] | 2     | calculus    |
      | Dinosaur Roaring     | c-[timestamp]  | [org:School XX1a] | 1     | dinosaur    |
      | Hair Braiding        | d-[timestamp]  | [org:School XX1a] | 1     | braiding    |
      | Quatz                | 1-[timestamp]  | [org:School XX1a] | 2     | Quatz       |
      | Zoozzy               | 2-[timestamp]  | [org:School XX1a] | 2     | Zoozzy      |
      | Vimbo                | 3-[timestamp]  | [org:School XX1a] | 2     | Vimbo       |
      | Voonder              | 4-[timestamp]  | [org:School XX1a] | 1     | Voonder     |
      | Oyondu               | 5-[timestamp]  | [org:School XX1a] | 1     | Oyondu      |
      | Jabbertype           | 6-[timestamp]  | [org:School XX1a] | 1     | Jabbertype  |
      | Jaloo                | 7-[timestamp]  | [org:School XX1a] | 3     | Jaloo       |
      | Twitterlist          | 8-[timestamp]  | [org:School XX1a] | 3     | Twitterlist |
      | Zooveo               | 9-[timestamp]  | [org:School XX1a] | 3     | Zooveo      |
      | Ozu                  | 10-[timestamp] | [org:School XX1a] | 4     | Ozu         |
      | Kazio                | 11-[timestamp] | [org:School XX1a] | 4     | Kazio       |
      | Zava                 | 12-[timestamp] | [org:School XX1a] | 4     | Zava        |
      | Divavu               | 13-[timestamp] | [org:School XX1a] | 4     | Divavu      |
      | Eire                 | 14-[timestamp] | [org:School XX1a] | 4     | Eire        |
      | Thoughtblab          | 15-[timestamp] | [org:School XX1a] | 4     | Thoughtblab |
      | Buzzshare            | 16-[timestamp] | [org:School XX1a] | 4     | Buzzshare   |
      | Topdrive             | 17-[timestamp] | [org:School XX1a] | 4     | Topdrive    |
      | Oyoyo                | 18-[timestamp] | [org:School XX1a] | 4     | Oyoyo       |
      | Buzzbean             | 19-[timestamp] | [org:School XX1a] | 3     | Buzzbean    |
      | Edgeblab             | 20-[timestamp] | [org:School XX1a] | 2     | Edgeblab    |
    And I am logged in as "Sir Patrick Stewart"

    When I am at "tapusers/[org:School XX1a]"

  #Select John Kimball
    And I check the box for the row containing "John Kimball"
    And I select "Assign to Class" from "operation"
    And I press "Next"

  #Select all from VBO config form rendered with tableselect.
    And I click the "#datatable-2 th.select-all > input" element
    Then I should see "Selected 10 rows in this page"

  #First page of classes, and one more for good luck
    When I click "Next"
    And I check the box for the row containing "Buzzshare"
    And I press "Next"
    And I wait for the batch job to finish
    Then user "John Kimball" is assigned 11 classes

  #Select all rows in table
    When I am at "tapusers/[org:School XX1a]"
    And I check the box for the row containing "John Kimball"
    And I select "Assign to Class" from "operation"
    And I press "Next"
    And I click the "#datatable-2 th.select-all > input" element
    And I click the ".views-table-row-select-all button" element
    And I press "Next"
    And I wait for the batch job to finish
    Then user "John Kimball" is assigned 28 classes
