@api @tests
Feature: Scenarios around test functionality

  Background:
    Given LDR organizations:
      | name          | type     | identifier | parent              |
      | XX District 1 | district | 1          | XX                  |
      | School XX1a   | school   | 2          | [org:XX District 1] |
    And LDR class orgs:
      | name    | section  | org               | grade | ref     |
      | Math    | math1    | [org:School XX1a] | 3     | Math    |
      | English | english1 | [org:School XX1a] | 7     | English |
    And LDR students:
      | firstName | lastName | enrollOrgId       | dateOfBirth | gender | gradeLevel | stateIdentifier | classes                      |
      | Louis     | Skolnick | [org:School XX1a] | 2001-02-03  | M      | 3          | XXD1A001        | [class:Math]                 |
      | Gilbert   | Lowell   | [org:School XX1a] | 2004-05-06  | M      | 3          | XXD1A002        | [class:Math]                 |
      | Sloth     | Fratelli | [org:School XX1a] | 2001-02-03  | M      | 3          | XXD1A003        | [class:Math],[class:English] |
      | Mikey     | Astin    | [org:School XX1a] | 2004-05-06  | M      | 3          | XXD1A004        | [class:English]              |
      | Mouth     | Feldman  | [org:School XX1a] | 2004-05-06  | M      | 3          | XXD1A005        | [class:English]              |
    And LDR tests:
      | name                                | subject | grade | program               | security | permissions    | scoring   | description                 |
      | ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3  | ELA     | 8     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Test administered for Behat |
      | Math_Comp - MC.8.F.B.3 - MC.8.F.B.3 | Math    | 8     | Diagnostic Assessment | Secure   | Non-Restricted | Immediate | Test administered for Behat |
    And LDR test assignments:
      | student                  | test                                       | status    | ref |
      | [student:Louis Skolnick] | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Gilbert Lowell] | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Sloth Fratelli] | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |
      | [student:Sloth Fratelli] | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Mikey Astin]    | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Mouth Feldman]  | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |

    And TAP users:
      | name      | password    | mail                  | roles              | organizations     |
      | testadmin | admin9test9 | testadmin@example.com | test administrator | [org:School XX1a] |

    And I am logged in as "testadmin"

    And I am assigned classes "[class:Math],[class:English]"

  @ads-859
  Scenario: Should not see test assignment access code info if no access to student
    Given LDR organizations:
      | name          | type     | identifier | parent              |
      | School XX1b   | school   | 3          | [org:XX District 1] |
    Given TAP users:
      | name       | password    | mail                   | roles              | organizations     |
      | testadmin2 | admin9test9 | testadmin2@example.com | test administrator | [org:School XX1b] |

    And LDR class orgs:
      | name    | section  | org               | grade | ref     |
      | Algebra | algebra1 | [org:School XX1b] | 7     | Algebra |
    And LDR students:
      | firstName | lastName | enrollOrgId       | dateOfBirth | gender | gradeLevel | stateIdentifier | classes         |
      | Tony      | Chopper  | [org:School XX1b] | 2001-02-03  | M      | 3          | XXD1A006        | [class:Algebra] |
    And LDR test assignments:
      | student                | test                                       | status    | ref       |
      | [student:Tony Chopper] | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled | tony_math |

    When I am on "tests/nojs/key/student/[student:Tony Chopper]/[asn:tony_math]"
    Then I should get a 403 HTTP response

    When I am logged in as "testadmin2"
    And I am assigned classes "[class:Algebra]"
    And I am on "tests/nojs/key/student/[student:Tony Chopper]/[asn:tony_math]"
    Then I should get a 200 HTTP response

  @parads-467 @test_registration_key @javascript
  Scenario: Test administrator can see key link for student test assignments
    When I am at "manage/[org:School XX1a]/students"
    Then I should see the header "Operations" in column "6" of "datatable-1"
    #test assignment names are hidden
    And the "ul#ddb--7" element should have computed css value "-9999px" for "left"

    #the right test assignments appear for the right students
    When I click "Generate access code" in the "Skolnick" row
    Then the "ul#ddb--7" element should not have computed css value "-9999px" for "left"
    And the "ul#ddb--7 li.first" element should contain "Math_Comp - MC.8.F.B.3 - MC.8.F.B.3"
    And the "ul#ddb--7 li.last" element should not contain "ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3"

  @parads-467 @test_registration_key @javascript @print
  Scenario: Test administrator selects a student test assignment
    Given I am at "manage/[org:School XX1a]/students"
    And I click "Generate access code" in the "Skolnick" row
    When I click the "ul#ddb--7 li.first a" element
    And I wait for AJAX to finish
    #Then the "#modalContent #edit-student" element should contain "Louis Skolnick (XXD1A001)"
    #textfield is disabled so we can't get value, checking attribute instead
    Then I should see the "#edit-student" element with the "disabled" attribute set to "disabled" in the modal
    And the named "fieldset" "Test Battery Info" should exist
    #And I should see "ELA Grade 5" in the "#edit-name" element
    And I should see the "#edit-name" element with the "disabled" attribute set to "disabled" in the modal
    And I should see the link "Print access code"
    When I click "Print access code"
    And I wait for AJAX to finish
    Then I should not see an "#modalContent" element
    Then I close the browser

  @ads-1591 @print
  Scenario: Test administrator selects a student test assignment
    Given LDR students:
      | firstName | lastName     | enrollOrgId       | dateOfBirth | gender | gradeLevel | stateIdentifier | classes         |
      | Shanice   | Cortes       | [org:School XX1a] | 2001-02-03  | M      | 3          | XXD1A006        | [class:English] |
      | Elke      | Politte      | [org:School XX1a] | 2001-03-02  | M      | 3          | XXD1A007        | [class:English] |
      | Tangela   | Bordelon     | [org:School XX1a] | 2001-04-01  | M      | 3          | XXD1A008        | [class:English] |
      | Jaime     | Kupiec       | [org:School XX1a] | 2001-05-31  | M      | 3          | XXD1A009        | [class:English] |
      | Vivien    | Pease        | [org:School XX1a] | 2001-06-30  | M      | 3          | XXD1A010        | [class:English] |
      | Mack      | Brinson      | [org:School XX1a] | 2001-07-29  | M      | 3          | XXD1A011        | [class:English] |
      | Marcelle  | Ashalintubbi | [org:School XX1a] | 2001-08-28  | M      | 3          | XXD1A012        | [class:English] |
      | Queen     | Monsivais    | [org:School XX1a] | 2001-09-27  | M      | 3          | XXD1A013        | [class:English] |
      | Rema      | Browder      | [org:School XX1a] | 2001-10-26  | M      | 3          | XXD1A014        | [class:English] |
      | Sonja     | Mclennan     | [org:School XX1a] | 2001-11-25  | M      | 3          | XXD1A015        | [class:English] |
      | Edmundo   | Luse         | [org:School XX1a] | 2001-12-24  | M      | 3          | XXD1A016        | [class:English] |
      | Devona    | Newbill      | [org:School XX1a] | 2001-01-23  | M      | 3          | XXD1A017        | [class:English] |
      | Dirk      | Lokken       | [org:School XX1a] | 2001-02-22  | M      | 3          | XXD1A018        | [class:English] |
      | Lauran    | Worth        | [org:School XX1a] | 2001-03-21  | M      | 3          | XXD1A019        | [class:English] |
      | Thi       | Schwein      | [org:School XX1a] | 2001-04-20  | M      | 3          | XXD1A020        | [class:English] |
      | Delta     | Coakley      | [org:School XX1a] | 2001-05-19  | M      | 3          | XXD1A021        | [class:English] |
      | Donette   | Michener     | [org:School XX1a] | 2001-06-18  | M      | 3          | XXD1A022        | [class:English] |
      | Kattie    | Marquette    | [org:School XX1a] | 2001-07-17  | M      | 3          | XXD1A023        | [class:English] |
      | Bari      | Robins       | [org:School XX1a] | 2001-08-16  | M      | 3          | XXD1A024        | [class:English] |
      | Albertina | Stoltz       | [org:School XX1a] | 2001-09-15  | M      | 3          | XXD1A025        | [class:English] |
      | Roxanne   | Christensen  | [org:School XX1a] | 2001-10-14  | M      | 3          | XXD1A026        | [class:English] |
      | Eva       | Jones        | [org:School XX1a] | 2001-11-13  | M      | 3          | XXD1A027        | [class:English] |
      | Jay       | Briggs       | [org:School XX1a] | 2001-12-12  | M      | 3          | XXD1A028        | [class:English] |
    And LDR test assignments:
      | student                         | test                                       | status    | ref |
      | [student:Shanice Cortes]        | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Elke Politte]          | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Tangela Bordelon]      | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Jaime Kupiec]          | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Vivien Pease]          | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Mack Brinson]          | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Marcelle Ashalintubbi] | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Queen Monsivais]       | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Rema Browder]          | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Sonja Mclennan]        | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Edmundo Luse]          | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Devona Newbill]        | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Dirk Lokken]           | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Lauran Worth]          | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Thi Schwein]           | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Delta Coakley]         | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Donette Michener]      | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Kattie Marquette]      | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Bari Robins]           | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Albertina Stoltz]      | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Roxanne Christensen]   | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Eva Jones]             | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
      | [student:Jay Briggs]            | [test:ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3]  | Scheduled |     |
    And I am at "manage/[org:School XX1a]/tests/view"
    And I click "Generate Access Codes" in the "ELA_Comp" row
    And I click "Print access code"

    # Check header stuff.
    Then I should see "First Name" in the "table thead" element
    And I should see "Last Name" in the "table thead" element
    And I should see "Student ID" in the "table thead" element
    And I should see "Access Code" in the "table thead" element
    And I should not see "First Name" in the "table tbody" element
    And I should not see "Last Name" in the "table tbody" element
    And I should not see "Student ID" in the "table tbody" element
    And I should not see "Access Code" in the "table tbody" element

    #Check a few students.
    And I should see "Cortes, Shanice" in the "table tbody" element
    And I should see "XXD1A006" in the "Cortes" row
    And I should see "Luse, Edmundo" in the "table tbody" element
    And I should see "XXD1A016" in the "Luse" row
    And I should see "Briggs, Jay" in the "table tbody" element
    And I should see "XXD1A028" in the "Briggs" row

  @parads-4028 @test_registration_key @javascript @ldr_orgs @print
  Scenario: Test administrator can retrieve an access code

    #Adding a class with a student and no test assignment just for fun.
    And LDR class orgs:
      | name   | section   | org               | grade | ref    |
      | Drupal | Drupal101 | [org:School XX1a] | 3     | Drupal |
    And LDR students:
      | firstName | lastName | enrollOrgId       | dateOfBirth | gender | gradeLevel | stateIdentifier | classes      |
      | Dries     | Buytaert | [org:School XX1a] | 1978-11-19  | M      | 3          | XXD1A006        | [class:Math] |

    And LDR test assignments:
      | student                  | test                                       | status    | ref |
      | [student:Dries Buytaert] | [test:Math_Comp - MC.8.F.B.3 - MC.8.F.B.3] | Scheduled |     |

    And I am assigned classes "[class:Drupal]"
    When I am at "manage/[org:School XX1a]/tests"
    Then I should see the header "Operations" in column "5" of "datatable-1"
    And I should see the header "Operations" in column "5" of "datatable-2"
    And the xpath '//*[@id="datatable-1"]/tbody/tr[1]/td[5]' element should contain "Generate access codes"
    And the xpath '//*[@id="datatable-2"]/tbody/tr[1]/td[5]' element should contain "Generate access codes"

    #Scenario: Test administrator selects a class test assignment
    When I click "Generate Access Codes" in the "Math_Comp - MC.8.F.B.3 - MC.8.F.B.3" row
    And I wait for AJAX to finish
    #Then the "#modalContent #edit-class" element should contain "ELA Grade 5"
    #textfield is disabled so we can't get value, checking attribute instead
    Then I should see the "#edit-class" element with the "disabled" attribute set to "disabled" in the modal
    And the named "fieldset" "Test Battery Info" should exist
    And I should see the "#edit-name" element with the "disabled" attribute set to "disabled" in the modal
    And I should see the "#edit-grade" element with the "disabled" attribute set to "disabled" in the modal
    And I should see the "#edit-subject" element with the "disabled" attribute set to "disabled" in the modal
    And I should see the link "Print access code"

    #Scenario: Test administrator prints key for test assignment
    When I click "Print access code"
    And I wait for AJAX to finish

    Then I should not see an "#modalContent" element
    Then I close the browser

  @ads-1375 @ads-1440 @ads-1526 @ads-1492 @ads-1612 @test_registration_key @print
  Scenario: Print Access Code for all students in a Class
    When I click "Tests" in the "main_menu" region
    And I click "Generate Access Codes" in the "ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3" row
    And I click "Print access code"

    # Table caption.
    Then I should see "Class Name: English - english1" in the "caption" element
    And I should see "Test Name: ELA_Comp - MC.8.F.B.3 - MC.8.F.B.3" in the "caption" element
    # Table header.
    And I should see "Last Name, First Name"
    And I should see "Student Id"
    And I should see "Access Code"
    And I should not see "Student Name"
    And I should not see "Test Key"

    #ADS-1526,1492 Students ordered by last name.
    And the "tbody tr:nth-child(1) > td:nth-child(1)" element should contain "Astin, Mikey"
    And the "tbody tr:nth-child(2) > td:nth-child(1)" element should contain "Feldman, Mouth"
    And the "tbody tr:nth-child(3) > td:nth-child(1)" element should contain "Fratelli, Sloth"

  @ads-1375 @ads-1440 @ads-1526 @test_registration_key @print
  Scenario: Print Access Code for a single student test
    When I click "Students" in the "main_menu" region
    And I click "Math_Comp - MC.8.F.B.3 - MC.8.F.B.3" in the "Fratelli" row
    And I click "Print access code"
    Then I should see "Math_Comp - MC.8.F.B.3 - MC.8.F.B.3" in the "Test" row
    And I should see "Fratelli, Sloth" in the "Student Name" row
    And I should see "XXD1A003" in the "Id" row
    And I should see "Student Name"
    And I should see "Student Id"
    And I should see "Access Code"
    And I should not see "Test Key"

  @parads-4188 @test_assignments
  Scenario: View/Edit test assingment link on student page
    When I am at "manage/[org:School XX1a]/students"
    Then I should see the link "View/Edit Test Assignments"

  @parads-4189 @test_assignments @javascript
  Scenario: View/Edit test assignment actions on student page
    Given I am at "manage/[org:School XX1a]/students"
    When I click "View/Edit Test Assignments" in the "Skolnick" row
    And I click the "ul#ddb--8 li.first a" element
    And I wait for AJAX to finish

    Then I should see the "#edit-student" element with the "disabled" attribute set to "disabled" in the modal
    And the named "fieldset" "Test Battery Info" should exist
    #And I should see "ELA Grade 5" in the "#edit-name" element
    And I should see the "#edit-name" element with the "disabled" attribute set to "disabled" in the modal
    And I see the button "Save"
    And I see the button "Cancel"
    When I press "Cancel"
    And I wait for AJAX to finish
    Then I should not see an "#modalContent" element
