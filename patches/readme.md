#Patches

Patch documentation should be in the following format:

* module name
  * brief description
  * issue link (if exists)
  * patch file location

Example:

* views
    * Add CSS class to read-more link on trimmed text field
    * http://drupal.org/node/1557926
    * http://drupal.org/files/views-more_link_class-1557926.patch

---

* datatables
    * Use datatables 1.10.
    * http://drupal.org/node/2293243
    * https://www.drupal.org/files/issues/update_datatables_up_to_1.10-2293243-18.patch
    
    * Fixes datatable init problem
    * https://www.drupal.org/node/1842714
    * https://www.drupal.org/files/issues/cannot_reinitialise-1842714-8.patch

* profile2_regpath
    * Fix no profile id error message during profile install.
    * http://drupal.org/node/1510700
    * https://www.drupal.org/files/issues/1510700-broken_ctools_integration.patch

* drupal
    * Support X-Forwarded-* HTTP headers alternates
      * https://www.drupal.org/node/313145
      * https://www.drupal.org/files/issues/support-x_forwarded_proto-313145-111.patch

* filefield_sources_plupload
    * Temporary File Could Not Be Copied
    * https://www.drupal.org/node/2617526
    * https://www.drupal.org/files/issues/filefield_sources_plupload-copy_file_error-2617526-1.patch
